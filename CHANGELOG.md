<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR.MINOR.PATCH는 ## 즉, H2로 표시한다.
--->


<a name="2.1.5"></a>
# [2.1.5](https://github.com/mindslab-ai/m2u/compare/v2.1.4...v2.1.5) (2018-12-11)

### Enhancements
- map: MAP Forwarder에 eventstream 을 볼수 있도록 log 수정
- router: 무효가 된 Session의 Device ID를 제거하도록 개선
- admin: Test Chat 수행 시 directive cards 출력 및 컬러 변경
- admin: simple classifier 데이터를 excel 로 내보내고 올리는 기능 추가
- admin: simple classifier skill 별 description 추가하여 등록, 수정 기능 추가
- admin: ITF Instance 등록 시 policy 선택을 radio button으로 변경
- admin: ITF Instance test시 Result Hints 추가
- admin: sidenav style 개선
- CI: CI 환경 연동을 위한 Jenkinsfile 코드 구현 및 리펙토링


### Bug Fixes
- admin: Test Chat response 영역에 HTML 태그가 포함된 정보 깨지는 문제


<a name="2.1.4"></a>
# [2.1.4](https://github.com/mindslab-ai/m2u/compare/v2.1.3...v2.1.4) (2018-11-02)

### Enhancements
- map: MAP에서 talkRouter로 utter 전달시 공백이 없도록 수정
- admin: Pool check 실패 시 원인 오류 상세화


### Bug Fixes
- cdk: android-cdk Camera 촬영시 AutoFocus 및 "촬영" 버튼 연속으로 눌려지는 현상 수정
- cdk: android-cdk Camera 촬영 후 Crop 화면으로 이동안되는 현상 수정
- router: OpenSession 시 Talk 로그에 skill이 null로 들어가는 문제 수정
- router: Event 호출 시 Talk로그가 skill이 null로 들어가는 문제 수정


<a name="2.1.3"></a>
# [2.1.3](https://github.com/mindslab-ai/m2u/compare/v2.1.2...v2.1.3) (2018-10-23)

### Enhancements
- common: grpc channel shutdown을 위해 공통 util클래스 추가
- cdk: android-cdk Camera UI 에 Theme 설정 제거
- cdk: android-cdk Camera UI 의 닫기(X) 버튼의 터치 영역 확대
- cdk: 카메라 Preview 해상도 설정 조건 수정
- Router: ResultStatus의 detail을 DebugInfo로 전달하도록 개선
- da: DA에러 ResultStatus에 챗봇/Skill/DAI 정보 추가
- intent-finder: ITF load balancing 위한 작업 추가
- admin: m2u Admin 이전 버튼 클릭시 페이지 이동 개선
- admin-rest: M2U Rest에서 meta가 전달되지 않는 문제 수정
- logger: 디버깅용 meta 전달 기능 추가


### Bug Fixes
- cdk: android-cdk Camera UI 에 Theme 설정 제거
- cdk: 특정 단말의 android-cdk Crop 화면에서 이미지의 width, height가 바뀌는 현상 수정
- cdk: android-cdk 카메라 촬영 후 이미지 전송시 응답시간이 늦어지는 현상 수정
- da: DA서버 Shutdown시 IsReady 호출에 20초 걸리는 문제 수정
- intent-finder: itf clElapsedTimer 로그 출력 버그 수정
- intent-finder: intent finder set_is_default 설정 로직 원복
- brain-sds: CleanDisabledSession 실행 시 SDS group process 죽는 현상 수정


<a name="2.1.2"></a>
# [2.1.2](https://github.com/mindslab-ai/m2u/compare/v2.1.1...v2.1.2) (2018-10-04)

### Enhancements
- cdk: android-cdk 카메라 Preview 화면 GUI 개선
- da: discard 결과 goto이면 skill전환 기능 추가
- dai-svc: extra command 에 newline이 있는 경우 예외처리
- intent-finder: RetryIntentFinder의 동작이 IntentFinder의 개선을 반영하지 못한 부분 개선
- brain-ta: HMD not 기호 연산 범위 관련 수정사항 적용
- brain-sds: 세션 정보를 접근 시 항상 mutex lock을 하도록 처리


### Bug Fixes
- map: forward에서 secure로 ManagedChannel RuntimeException 발생하는 문제 수정
- cdk: Android 일부 단말(LG v30, v20)에서 카메라 Preview 화면 표시되지 않는 현상 수정
- intent-finder: Find Intent 분류 스킬을 찾지 못할 경우 디폴트 스킬로 채우도록 수정


<a name="2.1.1"></a>
# [2.1.1](https://github.com/mindslab-ai/m2u/compare/v2.1.0...v2.1.1) (2018-09-21)

### New Features
- common: python 개발환경 개선, 하나의 패키지에 대한 복수의 경로 지정 가능
- common:  DB Resource 암호화
- hazelcast: client timeout 설정을 m2u.conf의 hazelcast.invocation.timeout으로 추가
- map: 함수호출 시작시간, 종료시간 파일로 기록
- cdk: 카메라 preview 화면의 중간에 투명 이미지 출력 기능 추가 (prevew, crop 화면)
- cdk: cdk OpenCamera, OpenMicrophone 스펙 확대
- cdk: Preview 두번째 화면에서도 이미지 처리
- router: 에러 코드 고도화. 명시적으로 DA_ERROR, ITF_ERROR을 지정하도록 추가
- router: GRPC call에 deadline 설정을 m2u.conf의 routed.grpc.deadline로 추가
- intent-finder: custom script python 서버화
- intent-finder: policy 등록, 수정 시 dnn cl값 표시형식 수정(범위(0<coefficient< 1)
- intent-finder: 분류 결과에 hmd, pcre 규칙, dnn cut 된 cl_top 정보 detail 저장
- intent-finder: 입력 발화에 대해 NLU Analyze 처리 위한 API 추가
- intent-finder: 분류 도중 replace 된 발화에 대하여 NLU 재처리 수행
- brain-ta: Matrix 화된 결과를 기준으로 HMD 동작하도록 반영(로딩, 분류 속도개선)
- dai-svc: dialog-agent-instance-supervisor-control 프로세스 구축
- logger: Front 용 remote db 전송 추가
- m2u-map-cli: file 옵션 추가하여 TextToTalk 수행 및 로그 기록.

### Enhancements
- common: gradle wrapper 버전을 4.4.1로 정규화
- common: libmaum update, setup export 관련 기능, centos 지원
- common: spdlog에 일자별 로그에 로그 크기에 따른 rotation 지원 추가
- grpc: Python용 GRPC를 1.9.1로 원복
- map: MAP에서 grpc channel shutdown시에 awaitTermination 옵션 추가
- map: MAP grpc stub 호출 시 deadline 옵션 추가
- map: MAP Server에 max connection idle time 적용
- map: map secure interceptor에서 authToken 점검 루틴 개선
- cdk: CDK 로그아웃 처리 안정화
- router: DA 오류 시 CloseSkill 후 Invalidate 처리하도로 개선
- router: session not found, 예외 케이스에 대한 로그 추가
- intent-finder: 초기 로딩 리소스 없을 경우 관련 warning 정보 출력
- intent-finder: 의도 파악 시 최초 탐지값으로 반환하여 분류 속도 개선(HMD, PCRE)
- intent-finder: 의도 파악 관련 상세 로그 출력하도록 수정 (log level에 따라 메세지 출력)
- intent-finder: 원격 서버의 hmd 엔진을 접근할 수 있도록 반영 (단, 최대 통신 가능 데이터 크기는 4194304 byte)
- intent-finder: ITF에서 exclude 대상에서 default 스킬은 제외
- intent-finder: custom script 서버화를 위한 custom script 내용 변경
- intent-finder: custom scrpit 테스트 도구 개발
- intent-finder: itf pcre debug log가 과다하게 나오는 문제 수정
- intent-finder: 공통 에러코드 적용(Error Index 명시화)
- intent-finder: Customscript 분류 시 python script에 session, system context 전달 하도록 수정
- intent-finder: Customscript 분류 시 sync_id, session_id log 출력
- intent-finder: 분류 시간 로그 출력
- brain-ta : brain-ta 개별적인 Error Class 구현으로 ta exception 정보 전달
- brain-ta : HMD SetModel, 매트릭스화 관련 속도 개선
- da: DialogAgent Maven 빌드 가능하게 pom.xml 추가
- da-run: dialog agent runner에 -p PORT 파라미터 추가
- da-reg: m2u-darun에서 DA에 접속하기 전에 TCP connect가 가능한지 1분간 체크
- da-reg: dareg에서 dai 포트값 생성시 m2u.conf의 min, max값을 바탕으로 설정
- dai-svc: daisvc 또는 itfsvc 의 경우 hazelcast 가 정상적이지 않은 경우에 대한 처리
- sds: log를 group 별로 나오도록 개선
- admin: Admin SC Upsert 페이지에서 skill 순서 조정가능하도록 변경
- admin: intent finder Instance 등록 시 name 제약 조건 추가
- admin: Custom Script model, category 목록 가져올 수 있도록 반영.
- admin: DNN model, category 목록 service admin에서 가져오도록 수정
- admin: chatbot 설정 화면에서 intent-finder policy, intent-finder-instance 정보에대하여 링크 구조 추가
- admin: intent finder instance test 화면 style 조정
- logback: java logback의 경우 rolling 파일의 위치 변경
- build: 재설치 시점의 오류 최소화 및 불편함 해소
- doc: da-v3 proto doc 생성할 때 sds 포함

### Bug Fixes
- hazelcast: Hazelcast 서비스 재시작 시 데이터 지워지는 문제 수정
- hazelcast: 설정 백업 시 Thread가 계속 누적되는 문제 수정
- map: MAP Forwarder, Secure에서 onComplete가 정상적으로 호출되지 않는 이슈 수정
- cdk: getLocation 오류 수정
- cdk: ImageToText, SpeechToText의 경우 ByteStream이 Event보다 먼저 나가는 경우 수정
- cdk: iphone webview 호출할 때 timeout 처리
- cdk: cdk js에서 stream event를 받지 못하는 문제
- intent-finder: 부하테스트 시 발생했던 PCRE, HMD, DNN, Custom Script 분류 관련 문제 수정
- dai-svc: dai svc.conf 생성시 da 정보 획득 실패 가능성에 대한 예외 처리
- sds: group process 여러개 뜨는 문제 수정
- admin: Da 정보 Edit 버그 수정
- admin: Dai edit 시 정보 저장 관련 버그 수정
- admin: Dai 정보 수정 시 해당 dai count 정보 전달 버그 수정
- admin: intent finder instance 등록, 수정 시 ip - port에 대한 중복 체크 반영
- admin: Dai activity status 정보 전달 및 출력 버그 수정
- admin: Dialog Agent 조회 화면, 우측 패널 수정.(필드 삭제, 변경)
- admin: dai 관련 정보를 info panel에 activity status(dam name ,pid, port) 출력
- logger: 챗봇별 세션, 대화 갯수 로그가 정확히 출력 수정
- Logger: CloseSkill() 후 stub를 명시적으로 shutdown하도록 수정


<a name="2.1.0"></a>
# [2.1.0](https://github.com/mindslab-ai/m2u/compare/v2.0.0...v2.1.0) (2018-08-14)

### New Features
- common: polyglot을 이용한 m2u 프로세스 테스트 스크립트 작성
- common: m2u 재설치 시 실행 할 수 있는 스크립트 작성
- common: grpc 1.13.1로 패치
- common: 서비스 기동 시 종속관계(Hazelcast, Pool)에 따라 시작하도록 하여 전체 시스템이 원활히 동작하도록 추가
- hazelcast: 'user-info' MAP store을 별도 file DB로 분리
- hazelcast: hazelcast console 실행 shell script hzc-console 추가
- hazelcast: M2U 기본 설치에서 2개의 hzc instance를 사용하도록 변경
- hazelcast: hazelcast load order config
- map-auth: multiFactorVerify 기능 구현
- map: internal authMode 추가
- map: MAP 유량 제어를 위해 sessionManager 생성
- map: MAP에 Forwarder, Secure 추가
- map: "작업중" 공지기능 제공
- map: Launcher 관련 Event handler 기능 추가
- map: 거래로그 요건에 맞추어 MDC 설정을 추가 및 정리
- map: java 프로젝트에서 참조하는 logback.xml 파일 분리 작업
- map: 아바타 출력을 위한 장치를 정의할 수 있는 tartget 을 추가
- map: 외부에서 logback.xml을 사용하도록 수정
- map: CDK에getUserSettings, updateUserSettings 추가
- map: DA -> Router -> MAP -> CEK로 호환 가능한 Naver audio play payload 구현
- map: card.proto에 TableCard 추가
- map: 모든 card에 type 추가 및 Client LaunchApp 추가
- cdk: cdk에 Launcher 관련 콜백 추가
- cdk: 브리뷰 화면 사용자 정의할 수 있도록 라이브러리화
- cdk: 클라이언트에서 앱이나 화면을 실행할 수 있도록 기능 추가
- cdk: uglify 추가
- cdk: android cdk에서 명시적으로 onCompleted 호출
- cdk: CDK에서 event를 전송할 때, DialogAgentForwarderParam 전송
- router: itf 결과에 replace_utter 존재 시 request_utter에 적용
- router: itf 결과 DA로 전달
- router: DeviceID 로그 기록
- router: session context를 내부에 저장하면서 da에 전달하고 업데이트를 반영함.
- router: ITF를 정책으로 검색 후 round-robin 방식으로 선택하여 호출하는 기능
- router: Skill 전환 시 OpenSkill(), CloseSkill() 처리함.
- router: 에러 발생 시 에러에 따른 답변을 반환하도록 함.
- router: RDB Session, Talk 로그 Table에 x-operation-sync-id와 device Type/버전/채널 등을 기록 하도록 함.
- router: Talk 추적을 위한 x-operation-sync-id, session을 모든 로그에 추가
- router: flow에 맞도록 로그 level 조절
- router: Talk 로그에 Cards, Directives, meta 답변을 json으로 RDB에 저장한다.
- router: 스택 형식의 Skill 전환 기능
- router: Skill 전환 시 utter 교체 기능
- router: ITF를 사용하지 않고 chatbot의 default skill를 사용하는 방식 추가
- router: 공통응답 코드 적용
- intent-finder: custom script 분류 시 현재 hint에 저장되는 policy인데, skill에 저장 될 수있도록 샘플 추가
- intent-finder: itf 초기 기동 속도 개선
- intent-finder: customscript에서 replace 정보 저장
- intent-finder: customscript response proto message에 문자열, 별칭, 대체문장 정보 저장
- intent-finder: ITF 분류 결과를 RDB에 기록하는 기능 추가
- intent-finder: DNN 분류 결과를 버릴 수 있는 계수를 사용하도록 개선
- intent-finder: Custom Script에서 원문을 변경할 때, 대체 값이 복수가 되도록 개선
- intent-finder: intent-finder-cs-py-runner 와 연동 기능 추가
- intent-finder: custom script 와 연동하는 모듈 추가
- intent-finder: DNN 분류결과 버림 계수를 정의하고, 이를 활용하여 dnn 데이터 활용 보완
- intent-finder: custom script replacement 확대
- ta: HMD(C++) library 특정 모델만 로드하는 기능 추가
- dam: Admin에서 추가 원활히 하도록 함.
- dam: Java 형식의 DA 실행 시 entry class와 파라메터를 지정할 수 있도록 함.
- dam: 실행 중인 dai의 명령문에 chatbot, 지원skill, 언어등을 추가하여 디버깅, 관리를 쉽게함
- dam: 기동 시 무효한 Dialog Agent Instance 정보 삭제 기능 추가
- dam: flow에 맞도록 로그 level 조절
- sds: system_da 출력
- sds: call_log 사용 구조 추가
- sds: resolver에서 sub-sdsd 실행시 포트 범위 제한 (기본 설정: 10000~10300)
- sds: GetModelSlots API 추가
- front: Oracle 연동, DB별(oracle, mysql) 쿼리 반영
- front: flow에 맞도록 로그 level 조절
- admin: DialogMonitoring/ View Session V3 추가
- admin: 비밀번호 패턴 지정, 5개 까지 재사용 금지, 교체 시기 등 제약 처리
- admin: DialogMonitoring/ View Session V3 추가
- logger: Oracle 연동, DB별(oracle, mysql) 쿼리 반영
- logger: Appender를 통한 외부 로그(Remote DB, MQ) 연동 기능
- logger: 5분 주기로 동시 접속 세션 로를 MQ Appender로 전달 기능
- logger: 다중 서버 환경에서 각 서버별 돌아가면서 동작하도록 처리 추가
- build: centos 빌드시에 도커에 포함된 boost library를 사용하도록 변경

### Enhancements
- common: javasrc에서 불필요한 hzc 라이브러리 제거
- common: java project 로그레벨정리
- common: m2u 기본 import data set(json) 내용 추가
- common: m2u 자바 프로세스 로그 롤백 경로 수정
- common: netty 로그는 경고 수준까지만 나오도록 변경
- common: 공통응답 코드 라이브러리 수정
- common: libmaum, 데일리로그 남길때 날짜 형식을 yyyy-mm-dd로 변경
- hazelcast: file db명 변경 m2u -> maum
- hazelcast: 설정 값이 들어 있는 Map의 backup-count를 3으로 설정하여 안정성 강화
- hazelcast: 로그성 Map(Session, Talk, Classification)을 file DB로 백업하지 않도록 개선
- map: map -> router payload전송 방식 변경(필드 유효성 체크가 되도록)
- map: MAP에서 Error, Warning log 출력시 X Operation Sync Id 전달하도록 수정
- map: 최신 IDR 스펙문서에 맞게 idr.proto를 수정, CDK 수정
- map: MAP에서 외부 로깅용 Appender를 이용하여 정상적으로 LOG 파일이 출력되도록 수정
- map: 모든 연동 구간에서 x operation sync id 전달하도록 수정
- map: MAP에 Launcher 관련 Event handler 기능 추가
- map: MAP에서 log를 남길때 operationSyncId를 남기도록 수정
- map: SecureMap - MAP간 channel 생성을 InProcessChannelBuilder를 이용하도록 수정
- map: grpc channel 생성 후 사용완료 후 명시적으로 shutdown 하도록 수정
- map: MAP checkAuth에서 ThreadLocalUser를 설정하도록 수정
- map: map 전반적인 코드 정리, 반복 중복 코드 제거
- cdk: CDK에서 생성하는 uuid 에 "/" 문자 변경, RFC7515 규격을 준수
- cdk: 프리뷰 라이브러리화
- router: SKILL_TRANS_RESET 추가
- router: ITF를 Instance에서 Policy로 접근하도록 개선
- router: ResolveLastSkill에서 DA찾는 로직 보강
- router: OpenRoute 시 기존의 세션이 있으면 무효화하고 다시 생성하도록 개선
- router: last skill 사용 시 ITF Analyze만 한 nlu_result를 DA에 전달하도록 개선
- sds: 영어교육용 python sample 추가
- sds: apply_indri_score, verbose_print 옵션 conf에서 설정하도록 변경
- sds: 처음 발화자에 따라 다르게 응답하도록 open response 변경
- sds: 시스템 응답에 사용되는 제한되는 슬롯 개수 200개로 수정
- sds: generate한 외부db 정보 초기화할 수 있도록 수정
- sds: internal일때, 사용자 intent, entity 출력
- sds: 시스템 응답에 사용되는 제한되는 슬롯 개수 200개로 수
- sds: filled slot 파싱 로직 개선
- intent-finder: itf instance 이름, PID, X-Operation-Sync-id, Session-id 로그 출력 및 분류 과정 로그 기록 상세화
- intent-finder: 외부 로그 플러그인 참조 기능 추가
- intent-finder: python custom script 내부에서 grpc 호출 에러
- intent-finder: simple classifier 변경 시 즉시 ITF 적용
- intent-finder: brain-ta.conf를 main에서 한번만 로딩하도록
- intent-finder: DNN에서 복수의 dnn 모델을 쓰는 경우 안정성 향상
- intent-finder-manager: itf instance 변경 시 supervisord에 반영 후 재기동하도록 수정
- intent-finder-manager: itfm의 prefix를 itf-로 변경 하고 자기 IP인가 점검
- ta: HMD(C++) classify 시 규칙 중 괄호 짝이 맞지 않을 경우 빈 결과를 반환
- ta: HMD(C++) 문법 오류 관련 처리
- ta: wordEmbedding 형태소 태그 정보 포함한 결과 반환
- ta: NLP3 rocksdb 로그제거, 기능 안정화 반영본 적용
- dam: DA, DAI 등록/삭제 시 로그 강화
- da-v3: 샘플 DA 예제 추가 및 개선
- front: 하위 호환이 되도록 기능 적용
- admin: DA 상세화면에서 GetUserAttributes 제거
- admin: Admin Testchat 챗봇리스트 ServiceAdmin에서 가져오도록 변경
- admin: SimpleClassifier 입력,수정시 Pos,Ner 체크박스 추가
- admin: Test Chat에서 이전 버전 Front를 지원하도록 개선
- admin: SimpleClassifier 입력,수정시 Pos,Ner 체크박스 추가
- admin: 테이블 컴포넌트 통일
- admin: STT 메뉴 삭제
- logger: 이미 close되어 invalid한 세션은 closeSession()을 호출하지 않도록 개선
- logger: 하위 호환 위하여 Front 백업 추가
- logger: 대화로그를 옵션에 따라서 저장하도록 개선
- logger: 세션 상태를 남기는 로그 처리
- config: ORACLE 접속에 따른 명시적 database정보를 환경변수화
- build: centos 및 docker에서는 boost lib 복사 및 재사용
- build: cpp-netlib 제거 및 front에서 http 서비스 중지
- mapcli: 실행 시 CHATBOT 지정 필수 옵션 추가

### Breaking Changes
- repository: m2u-mobile의 cdk-android를 m2u/cdk/android로 이전
- proto: deprecated된 property 및 rpc 삭제
- front: 기존 버전 itf 제거, 2.0ver itf 반영
- ta: HMD(C++) NLP3 기준으로 변경
- sds: sds1.0 제거
- build: M2U ubuntu용 설치 라이브러리 수정(libbz2-1.0, libzmq3-dev)

### Bug Fixes
- common: svd 프로세스 자동 재시작 스크립트가 Centos기반 os에서 돌아가도록 수정
- common: netty 등의 로그 경고 수준까지만 출력 제거
- common: PropertyManager를 멤버 변수를 참조하는 문제
- map-rest: call event시 header 중첩현상 수정
- map-rest: m2u-rest에서 payload값만 나오도록 수정
- map-auth: expiredTime 버그 수정
- map: map에서 router close시에 param 오류 수정
- map: open / talk 진행 안되는 현상 수정
- map: AuthFailurePayload return되지 않는현상 수정
- map: STT 접속 이후 utter parsing 에러 수정
- map: router로 부터 speech 수신시 값이 없는 경우 TTS를 안타도록 예외처리
- map: DialogWeb에서 open,close,ttt에 대한 응답으로 view/renderspeech, view/renderclose 로 응답하도록 수정
- cdk: 안드로이드에서 MAP 호출 후 명시적으로 onCompleted()를 호출하여 연결 종료
- android-cdk: SignOut 전달시 authToken값을 전달하도록 수정
- android-cdk: SignIn 호출 시 Header에 m2u-auth-sign-in 포함하도록 수정
- android-cdk: 노트4에서 Image Capture > Crop 시 이미지 저장이 잘못되고 있는 현상 수정
- hazelcast: hazelcast Connector 원격지 ip접속 에러 수정
- hazelcast: hazelcast to fileDB 데이터 불일치현상 수정
- hazelcast: MapStoreClassificationRecord 접근 db명 오류 수정
- hazelcast: hazelcast instance에서 SQLITE_BUSY 문제 발생
- hazelcast: hazelcast 에서 lazy 모드로 실행하면 분산된 mapstore 유실되는 문제
- router: OpenSession 시 delegate meta가 전달되지 않는 문제 수정
- router: 다중 skill 지원 시 crash 발생하는 문제 수정
- router: Skill 전환 중 OpenSkill에 전환된 skill이 전달되지 않는 문제 수정
- router: TalkV3 테이블에 endTime이 기록되지 않는 문제 수정
- router: GOTO_ONCE, CANNOT_UNDERSTAND 수행 후 last_skill이 Invalidate되는 문제 수정
- router: 별개의 grpc call에서 동일한 session 변수에 접근하여 발생하는 Crash 문제 수정
- router: skill 이름이 길 때 Hazelcast exception 나는 문제 수정
- router: ITF 1차 호출 실패 후 재호출 시 crash 발생하는 문제 수정
- router: CloseSkill 시 업데이트된 Session Context을 전달하도록 수정
- intent-finder: brain-ta config를 한번만 로딩하도록 수정
- intent-finder: NLP3, NLP2를 한번만 로딩하도록 변경
- intent-finder: DNN을 마지막에 설정한 DNN을 하나만 호출하는 문제
- intent-finder: hmd 에서 nlp3, nlp2를 로딩하는 문제, Document 재사용
- intent-finder: DNN 분류기를 하나만 실행하는 문제
- intent-finder: DNN 분류기가 수행될 때, 수행이 중단되는 문제
- intent-finder: DNN에서 같은 모델을 중복해서 로딩하지 않도록
- da-v3: terminate()시 응답 없는 문제 해결
- sds: filled_entities, empty_entities parsing 오류 수정
- sds: sub-sdsd 서버가 여러개 뜨는 문제 해결
- sds: Close 호출시 sds 내부의 세션 close
- sds: sds 대화 처리 과정에서 내부 deadlock이 발생하는 문제 수정
- admin: test chat시 session이 1개만 생성되던 현상 수정
- admin: service admin의 hazelcast 변경 발생 즉시 Localdb 반영
- admin: webadmin에 Runtime Parameter 내용 출력 정상화
- admin: simple classifier 등록, 변경 시 sqlite 정상 반영
- admin: simple classifier, itf instance 등록, 변경 시 name, port 중복 방지
- admin: DialogAgentInstance 등록, 변경 시 이름 중복 방지
- admin: Admin Testchat에서 Metadata에 response 나오도록 변경
- admin: Admin Testchat에서 대화만 나오도록 수정
- admin: Admin management account 등록,수정 페이지 css 깨지는 문제
- admin: admin production 모드에서 nlp3 요청 에러 수정
- admin: Oracle 환경에서 Trigger없이 Loopback 동작하도록 개선
- admin: mobx5관련 라이브러리 제거
- admin: admin testchat에서 open시 greeting message 나오도록 수정
- admin: Oracle에서 Dialog Monitoring 동작안하는 문제 수정
- admin: DA Extra Command 안내 문구 추가
- admin: test chat UI 변경
- admin: Chatbot panel dai 10개이상 안나오는 오류 수정
- admin: dai 등록 시 da에 셋팅 안 된 dam은 선택되지 않도록 수정
- admin: DA하나에 동일한 DAM이 두개 표시되는 문제 수정
- logger: ITF 결과 저장 안되는현상 수정
- logger: TalkV3에 세션ID가 저장되지 않는 문제 수정
- logger: Appender 동작 시 freezing 문제 수정
- build: 빌드 시 admweb, router등에서 중단되는 문제 수정
- build: cdk에 포함된 OTF 파일 포맷 바이너리 처리
- build: java result status 관련 idea 오류, MAUM_ROOT가 없지 않도록 처리
- build: libmaum 내부의 빌드 오류 수정


<a name="2.0.0"></a>
# [2.0.0](https://github.com/mindslab-ai/m2u/compare/v1.2.0...v2.0.0) (2018-04-18)

### Bug Fixes
- *admin*: 헤이즐캐스트 포터블 데이터 타입 오류 정리
- *build*: 자바 앱에서 안 쓰이는 패키지 정리
- *build*: 자바에서 안 쓰이는 프로토 패키지 정리
- *grpc*: 간헐적으로 발생한 `Call dropped by load balancing policy` 에러 제거
- *admin*: SC 등록 안되는 문제 해결
- *config*: 온라인 오프라인 설치 스크립트 안정화
- *grpc*: 간헐적으로 발생한 `Call dropped by load balancing policy` 에러 제거
- *admin*: 자바 앱에서 로그 정상적으로 출력되도록 변경, 날짜 출력, 자동 로테이션

### New Features
- *dav3*: 대화 에이전트 V3 스펙 재구성
- *dav3*: 출력 대화 포맷으로 스피치, 카드, 디렉티브 구성
- *dav3*: 스킬 전이를 처리할 수 있도록 DISCARD, CANT UNDERSTAND, GOTO, GOTO_ONCE 추가
- *dav3*: 대화 API에 새로운 세션을 시작할 때 처리할 수 있는 `OpenSession` 추가
- *dav3*: 대화 API에 스킬을 시작할 때, 스킬을 종료할 때 명시적으로 호출
- *dav3*: Java 기반의 샘플 DA 앱 제작
- *dav3*: 인텐트 파인더의 결과를 전송받을 수 있는 체계 정의
- *map*: 비동기 클라이언트와 통신할 수 있는 API 체계 정비
- *map*: 기존 버전에서 누락된 데이터 구조 정비
- *map*: 인증 서비스 정의 및 `IMDG`를 통한 사용자 관리, 사용자 인증 처리 후 사용자 정보 획득
- *map*: 라우터로 대화 전송
- *map*: 샘플 인증 서버 구현 및 테스트 환경 구성
- *map*: 이미지 인식 서버와 연동하는 체계 정비
- *map*: STT 새 버전과 연동 체계 정비
- *cdk*: 안드로이드 및 iOS 단말을 위한 자바스크립트 라이브러리
- *cdk*: 안드로이드 grpc map 통신 라이브러리
- *cdk*: 안드로이드 자바스크립트 인터페이스를 통해서 카메라, 스피커, 마이크 제어
- *cdk*: 개발자용 매뉴얼 제공(타입스크립트 문서 제공)
- *map*: 웹과 같은 동기 단말을 위한 `DialogWeb` 인터페이스 추가
- *router*: 기존 front에 비해서 경량화된 처리
- *router*: 새로운 세션 체계 및 대화 저장 체계 정리, session v3, talk v3
- *router*: 새 인텐트 파인더 호출
- *router*: 스킬 전이 관련 기능
- *intent-finder*: 다양한 분류 기법의 적용 및 순서를 제어할 수 있는 규칙 처리
- *intent-finder*: 스킬 분류와 힌트 분류를 분리하여 정의
- *intent-finder*: 기존의 DFA 방식 폐기 하고 PCRE 방식으로 비교 처리
- *intent-finder*: NLP 전체를 embedding하여, HMD, DNNCL, NLP 처리 체계화
- *intent-finder*: 파이썬 언어를 이용한 커스텀 스크립트 처리
- *intent-finder*: 분류 결과 스킬이 발견되면 중지하거나 계속 진행하는 흐름 제어 기능
- *intent-finder*: 4개의 분류기에서 스킬을 발견하지 못할 경우 파이널 스킬 사용
- *intent-finder*: 스킬을 발견하지 못할 경우 힌트 콜렉팅 기능
- *intent-finder*: 인텐트 파인더 전용 테스트 클라이언트
- *intent-finder-manager*: `IMDG`의 이벤트를 받아서 실행할 ITF를 실행하도록 처리
- *intent-finder-manager*: 수퍼바이저를 통해서 서비스를 재기동하도록 구성
- *dam*: `IMDG`의 이벤트를 받아서 서비스를 재기동하도록 구성
- *dam*: 매직 라이브러리를 활용해서 `da` 폴더에 있는 실행가능한 da 추적 기능
- *dam*: 자바로 만든 DA 실행 기능, Java DA에 다양한 main 실행 추가
- *dam*: 서비스 어드민을 통해서 dam 제어하는 기능 제거
- *dam*: 하나의 DA가 여러 개의 스킬을 처리할 수 있도록 연결 제공
- *pool*: 기존의 svcadmin에 통합되어 있던 서비스를 별도의 풀 서비스로 분리함
- *rest*: map 클라이언트로 웹 인터페이스 제공, JSON 통신하도록 구성
- *rest*: 기존 DA V1과 통신하는 API 자바 기반으로 재구성
- *rest*: 인텐트 파인더 전용 테스트 클라이언트
- *admin*: 순서 편집이 가능한 인텐트 파인더 편집기 개발
- *admin*: 대시보드에 서비스 중인 챗봇 목록 나오도록 개선
- *admin*: 대시보드에 서비스 중인 챗봇의 요약 정보 출력 및 다양한 바로가기 구현
- *admin*: 기존 대시보드의 프로세스 모니터링은 별도 메뉴로 분리
- *admin*: 프로세스 모니터링은 그룹별로 재구성하여 보기 좋게 재구성
- *admin*: PCRE 편집시 NER 태깅 참고 할수 있도록 구현
- *admin*: DA V3를 위한 테스트 채팅 구성
- *admin*: 하나의 DA가 여러 개의 스킬을 처리할 수 있도록 UI 제공
- *admin*: 하젤케스트를 통한 데이터 임포트, 익스포트 기능
- *config*: 기존 고정값으로 되어 있던 값들을 `setup config`를 통해 변수로 처리 할 수 있게 변경
- *config*: 헤이즐캐스트 설정파일 정리 및 eviction 처리
- *logger*: DA V3 세션 저장 및 대화 저장, 세션종료 시 `CloseSkill` 호출
- *docs*: 2.0.0에 맞게 전체적인 매뉴얼 업데이트
- *sds*: 대화중 현재 태스크 정보 출력
- *sds*: open할 때, 슬롯 정보 초기화 가능
- *sds*: 워크벤치처럼 로그 출력할 수 있는 옵션 추가
- *build*: centos 환경에서 빌드할 수 있는 도커

### Enhancements
- *admin*: 챗봇 목록을 가져오는 기능 admin 서버에서 가져오도록 변경
- *admin*: 챗봇 생성 화면 정비하여 단순하게 재구성
- *admin*: 최신 테이블 등 적용하여 view, 스타일 정비
- *admin*: 테스트 챗팅에서 새 REST API를 바라보도록 nginx 설정 추가

### Breaking Changes
- *intent-finder*: 새 버전 인텐트 파인더를 사용함으로써 기존 버전 폐기
- *rest*: 기존 node 기반 REST API 서비스 제거, 동시에 일부 진행된 admin API 중지
- *dam*: 기존 DAM 재구성
- *admin*: 인증 제공자 기능 제거
- *dav1*: 기존 DAv1, DAv2, DAv2p1의 경우 파이썬 패키지 재설정 필요
- *grpc*: 전반적인 서비스 재구성
- *had*: HAD 기능 제거
- *sc*: 기존 DFA 방식의 simple classifier 제거


<a name="1.2.0"></a>
# [1.2.0](https://github.com/mindslab-ai/m2u/compare/v1.1.0...v1.2.0) (2018-02-3)

### Bug Fixes
 - *svcadmin*: grpc connection failed 버그 수정
 - *front*:DA v2 버그수정
 - *front*:session 저장안되는 문제 수정

### New Features
 - *admweb*: 통계기능추가
 - *admweb*: dashboard UI변경
 - *map*: map 기능추가
 - *front*: DA V3(Draft) 기능추가
 - *front*: 대화의 각 Talk별로 feedback을 저장하는 기능 추가


<a name="1.1.0"></a>
# [1.1.0](https://github.com/mindslab-ai/m2u/compare/v1.0.0...v1.1.0) (2017-12-22)
<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->

### Bug Fixes

 - *svcadmin*: grpc 연결시 grpc connection failed 버그 수정
 - *admweb*: stt 미지원시 chatbot 등록이 안되는 버그 수정
 - *admweb*: DA 중복 등록되는 버그 수정
 - *admweb*: chatbot 중복 등록되는 버그 수정
 - *admweb*: intent finder 중복 등록되는 버그 수정
 - *admweb*: Admin REST dynamic Role 설정 오류 수정
 - *front*: 설치후 front log가 안나오는 버그 수정
 - *front*: 멀티 세션 연결시 front 죽는 버그 수정

### New Features

 - **common**: 모듈별 git version 기능 추가
 - **common**: grpc 버전변경(python 1.7.0, c++ 1.7.3, nodejs 1.4.1, java 1.8.0)
 - **install**: build.sh에 있는 DEPLOY 빌드 변수 오류 수정
 - **install**: Jenkins 연동을 위한 script 추가
 - **admweb**: 과거대화 목록 화면 추가
 - **admweb**: DA Detail에서 User Attributes 보기 추가
 - **admweb**:intent finder 등록 화면 추가
 - **front**: dialog agent spec v2 추가
 - **front**: grpc 연결 timeout을 maum.conf.in으로 변경가능하도록 수정
 - **itf**: intent finder 모듈 추가

### Breaking Changes

 - **common**: 용어변경 maum -> m2u
 - **common**: 용어변경 libminds -> libmaum
 - **common**: 용어변경 minds-stt -> brain-stt
 - **common**: 용어변경 minds-ta -> brain-ta
 - **common**: 용어변경 minds-sds -> brain-sds
 - **common**: 용어변경 minds-mrc -> brain-mrc
 - **common**: 용어변경 minds-qa -> brain-qa
 - **common**: 용어변경 ServiceGroup => Chatbot



<a name="1.0"></a>
# [1.0.0](https://github.com/mindslab-ai/m2u/compare/feat/1418...v1.0.0) (2017-11-23)

### Bug Fixes
 - *front*: 일부 메타데이터 가져올 때 시스템 크래쉬 발생 문제 해결
 - *front*: 세션 생성시 불필요한 락을 사용하여 데드락 발생
 - *front*: 죽은 DA 인스턴스에 접근하는 문제, DA 인스턴스가 정상 동작하는지 점검
 - *install*: centos 및 REDHAT 폐쇄망 설치를 안정성

### New Features
 - **hazelcast**: Hazelcast를 m2u MAP을 포함하여 단일 JAR 파일로 배포
 - **hazelcast**: Hazelcast의 MapStore 기능을 활용하여 데이터 보존
 - **hazelcast**: Hazelcast의 MultiMap을 모두 Map으로 변환
 - **hazelcast**: Hazelcast의 기본 비밀번호 재생성
 - **svcadmin**: 새 Service Admin 개발
 - **svcadmin**: Front에서 Dialog Agent Pool 관리 기능 이전
 - **svcadmin**: DAM 인터페이스 기능 개선, 각 DAM에 고유 이름 지정
 - **svcadmin**: 동일한 DA Instance를 한 DAM에 여러개 실행지정 가능
 - **svcadmin**: supervisor 프로세스 모니터링 및 관리
 - **dam**: 각 DAM 별로 고유 이름을 service admin에서 지정받아서 사용, 재기동시 안정성 확보
 - **dam**: restart 시 잔존 Dialog Agent Process 제거 로직 추가
 - **admweb**: 관리화면 전반 UI/UX 개선
 - **admweb**: Dashboard : 전체 서비스 상태표시, 각 단위프로세스 상태표시
 - **admweb**: Dialog Agent Manager 관리 : DAM 관리 기능
 - **admweb**: Simple Classifier 관리 : Simple Classifier, Skill, Regex 관리 기능
 - **admweb**: DNN 분류 정보 조회 : DNN 분류 정보 조회
 - **admweb**: Intent Finder 관리 : SC kor/eng, DNN kor/eng 정보가 지정된 ITF 관리 기능
 - **admweb**: Chatbot 관리 : Chatbot, DialogAgentInstance 관리 기능
 - **dialogger**: front에서 로그기능을 분리하여 RDBMS에 적재
 - **dialogger**: 명시적인 세션 종료 처리
 - **simple-classifier**: admin에서 입력한 내용을 자동 빌드하는 규칙
 - **front**: 라우드로빈 방식으로 DA 인스턴스 로드밸런싱 기능 추가
 - **front**: HTTP/1.1로 접근할 수 있는 기능 추가

### Enhancements
 - **hazelcast**: Hazelcast 쿼리를 이용하여 전반적인 속도 향상
 - **hazelcast**: Hazelcast 데이터 저장시 put에서 set으로 변경, 속도 향상
 - **common**: Cassandra 제거
 - **common**: RDBMS(MySql) 활용
 - **setup**: systemctl 를 supervisor로 적용

### Breaking Changes
 - **common**: 용어변경 ServiceGroup => Chatbot
 - **common**: 용어변경 Domain => Skill
 - **common**: 용어변경 실행 중인 DA를 도메인이라 부름 ==> Dialog Agent Instance
 - **common**: 용어변경 Domain Classifier(폐기) => IntentFinder (확장)
