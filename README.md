# maum.ai M2U

## Overview

- Dialog Service Platform
- Dialog Service Development Platform
- Installation type: On-Premise or SaaS on AWS or azure.

## Development Environment
- server
   - C++
   - java
- hazelcast repack included
   - Java 1.7
   - hazelcast 3.8.x or later
- protocols
   - grpc based interface
- admin console web
   - angular 5
- service admin
   - java 1.8
   - grpc
- web proxy server
   - java 1.8

## Build

### Overview

- CMake (2.8.12) above, 3.1 or later is better choice.
- GCC 4.8.2 for full support c++-11, SEE issue #18, [18](https://github.com/mindslab-ai/m2u/issues/18)
- nodejs 6.10.0 above
- Java 1.7 or later

### Checkout

```bash
git clone git@github.com:mindslab-ai/m2u.git
```

### External Dependencies
It shows build time dependencies only.
All prerequisite packages are installed by `prerequisite.sh`.


### Internal Dependencies
The following dependencies are managed by submodules. (See `git submodule`)
- libmaum
  - protobuf 3.5.2 (java 3.5.1)
  - grpc 1.13.1 (python 1.9.1)
- brain-stt
- brain-ta
- brain-sds
- hazelcast-cpp-client

Above protobuf, grpc, hazelcast-cpp-client is not
managed by OS package system not yet. We manage it locally via git submodule.

### How to use `build.sh`
- All sub build tasks.

```
  libmaum: build and install libmaum
  libdb: build and install hzc.
  stt: build and install stt
  ta: build and install ta
  sds: build and install sds
  m2u: build and install m2u platform
  hzc: build and install hazelcast re-packaging
  pool: build and install m2u pool service
  map: build and install map
  mapauth: build and install map-auth
  rest: build and install rest
  da: build and install dialog-agent
  svcadm: build and install service-admin
  admweb: build and install admin-web & rest
  logger: build and install dialog-logger
  itfm: build and install intent-finder-manager
```

- Build Mode
  - It build required tasks.
  - It install all built outputs to target directory.
    Default target directory is `~/maum`.
  - It install all required resources to target directory.
  ```bash
  ./build.sh ~/maum all
  ./build.sh ~/maum m2u
  ./build.sh ~/maum libmaum stt
  ```
- Deploy Mode
  - It create an temporary directory for deploy build.
  - It build required packages.
  - It install all binaries and resources except stt, ta resources.
  - It generates `.tar.gz` file to `out` directory at source root.
  ```bash
  ./build.sh tar
  ./build.sh tar ta-only
  ./build.sh tar stt-only
  ```
- Other commnads
  ```bash
  ./build.sh clean-deploy
  ./build.sh clean-cache
  ```
#### Internal build.sh
- ~/.maum-build
  - `cache`: This directory has several prebuilt results.
     It caches previous external build output for protobuf, grpc,
     hazelcast-cpp-client, cassandra-cpp-driver by compiler version.
  - `*.done`: If a prerequisite.sh successfully install all depencencies,
     this files are generated with given sha1 hash for its script.
- `build-debug`: `cmake` create this directory for develop mode.
- `build-deploy-debug`: `cmake` create this directory for deploy mode.
- Generated tar file has version number from `git describe`.

#### cmake
- Use 2.8.12 above.
- In centos, use `cmake3` not `cmake`.

It use the following options.

- DCMAKE_INSTALL_PREFIX=${MAUM_ROOT}
- DCMAKE_BUILD_TYPE=Debug
- DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++-4.8
- DCMAKE_C_COMPILER:FILEPATH=/usr/bin/gcc-4.8

In centos, `/usr/bin/g++` version is 4.8.5.

### Custom build for special options.
M2U has several preprocessors to build special outputs.

- for audio file storing
  ```bash
  mkdir ~/git/m2u/build-debug-record
  cd ~/git/m2u/build-debug-record
  cmake \
    -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++-4.8 \
    -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/gcc-4.8 \
    -DDEFINE_RECORD_AUDIO=ON \
    ..
  ```

- Use `alias.root`

```bash
git config --global --add alias.root '!pwd'
make -C $(git root)/build-debug install
```

- CLion create debug build directory. Use it.
  - Goto `Settings > Build, Execution, Deployment > CMake` in settings.
  - Add cmake options.
  ```bash
    -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT}
    -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++-4.8
    -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/gcc-4.8
  ```

```bash
make -C $(git root)/cmake-build-debug install
```

## Development tools, Testing

In development, `~/maum` is the default installation directory.
It is more convenient to handle built binaries and libraries.

### Settings
```bash
export MAUM_ROOT=~/maum
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${MAUM_ROOT}/lib
export PATH=$PATH:${MAUM_ROOT}/bin:
CDPATH=~/maum:~/git/m2u:..:. # maum path is can be changed by personal setting.
```

```bash
cd logs # it will drop you at ~/maum/logs
cd bin
```

### Use `setup`

```bash
setup config
```

It generates all configuration files always.

### Use supervisord

### Scripts to run server

```bash
sudo ${MAUM_ROOT}/bin/setup config

${MAUM_ROOT}/bin/svd
${MAUM_ROOT}/bin/svctl
```
