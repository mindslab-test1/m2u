package ai.maum.m2u.cdk.grpclib.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 이 클래스는 문자열 관련하여 유용하게 사용할 수 있는 함수들로 구성된 클래스 입니다.
 * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
 */

public class StringUtil {

  private static final String TAG = StringUtil.class.getSimpleName();

  /**
   * 클래스 생성자 입니다.
   */
  private StringUtil() {
  }

  /**
   * 문자열이 empty 또는 null 인지 확인하는 함수입니다.
   *
   * @param str 확인할 문자열
   * @return 문자열이 empty 또는 null 인 경우 true
   */
  public static boolean isEmptyString(String str) {
    return ("".equals(str) || str == null);
  }

  /**
   * 입력문자열이 null 또는 "null" 인지 확인 후 해당되는 경우
   * "" 의 값을 반환하는 함수입니다.
   *
   * @param str 확인할 문자열
   * @return String null 또는 "null" 인 경우 "" 를 return
   */
  public static String nullToBlank(String str) {
    if (str == null || str.equals("null")) {
      return "";
    }
    return str;
  }

  /**
   * 입력문자열이 null 또는 "null" 일 경우 "" 의 값을 반환하되,
   * 문자열 앞뒤 공백을 제거한 후(trim) 반환하는 함수입니다.
   *
   * @param str 공백을 제거시킬 문자열
   * @return String 공백이 제거된 문자열
   */
  public static String nullToBlankTrim(String str) {
    String temp = nullToBlank(str);
    return temp.trim();
  }

  /**
   * 입력문자열이 빈 문자열인지 확인하는 함수입니다.
   *
   * @param str 확인할 문자열
   * @return boolean 빈 문자열인 경우 true
   */
  public static boolean isBlank(String str) {
    String temp = nullToBlankTrim(str);
    temp.replaceAll("\r", "");
    temp.replaceAll("\n", "");
    temp.replaceAll("&nbsp;", "");
    if (!temp.equals("")) {
      return false;
    }
    return true;
  }

  /**
   * Object 객체를 byte[] 로 변환하는 함수입니다.
   *
   * @param obj byte[] 로 변환할 객체
   * @return byte[] byte[]로 변환된 값
   */
  public static byte[] toByteArray(Object obj) {
    byte[] bytes = null;
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    try {
      ObjectOutputStream oos = new ObjectOutputStream(bos);
      oos.writeObject(obj);
      oos.flush();
      oos.close();
      bos.close();
      bytes = bos.toByteArray();
    } catch (IOException ex) {
      LogUtil.e(TAG, ex.getMessage());
    }
    return bytes;
  }

  /**
   * byte[] 를 Object 로 변환하는 함수입니다.
   *
   * @param bytes Object 로 변환할 byte[]
   * @return Object 로 변환된 값
   */
  public static Object toObject(byte[] bytes) {
    Object obj = null;
    try {
      ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
      ObjectInputStream ois = new ObjectInputStream(bis);
      obj = ois.readObject();
    } catch (IOException ex) {
      LogUtil.e(TAG, ex.getMessage());
    } catch (ClassNotFoundException ex) {
      LogUtil.e(TAG, ex.getMessage());
    }
    return obj;
  }
}