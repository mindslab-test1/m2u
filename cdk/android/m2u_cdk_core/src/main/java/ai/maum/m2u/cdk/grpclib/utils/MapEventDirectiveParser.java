package ai.maum.m2u.cdk.grpclib.utils;

import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO.DirectiveStreamInfo;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO.MapDirectiveInfo;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO.MapEventInfo;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.MapDirective;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * JS 로 부터 전달받은 eventStream, MAP 으로 부터 전달받은 MapDirective 를 파싱하는 클래스 입니다.
 * 앱에서 사용할 수 있는 Key 들에 대해서 파싱하여 객체로 변환해 줍니다.
 * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
 */

public class MapEventDirectiveParser {

  private static final String TAG = MapEventDirectiveParser.class
      .getSimpleName();

  /**
   * JS 로 부터 전달받은 JSON type의 eventStream 데이터를 MapEventInfo 객체에 채워주는 함수입니다.
   *
   * @param eventStreamJson JS 로 부터 전달받은 JSON type의 eventStream
   * @return MapEventInfo 객체
   */
  public static MapEventInfo getMapEventInfo(String eventStreamJson) {
    MapEventInfo mapEventInfo = new MapEventInfo();
    try {
      JSONObject eventValueObject = new JSONObject(eventStreamJson);
      String streamId = eventValueObject.getString("streamId");
      String operationSyncId = eventValueObject.getString("operationSyncId");
      String interfaceValue = eventValueObject.getString("interface");
      JSONObject interfaceValueObject = new JSONObject(interfaceValue);
      String interfaceName = interfaceValueObject.getString("interface");
      String operationName = interfaceValueObject.getString("operation");
      boolean streaming = false;
      if (interfaceValueObject.has("streaming")) {
        streaming = interfaceValueObject.getBoolean("streaming");
      }

      // contexts의 location정보가 있는 경우 mapEventInfo에 설정합니다.
      JSONArray jsonArray = eventValueObject.getJSONArray("contexts");
      if (jsonArray != null && jsonArray.length() > 0) {
        for (int i = 0; i < jsonArray.length(); i++) {
          JSONObject jsonObject = jsonArray.getJSONObject(i);
          String location = jsonObject.optString("location");
          if (!StringUtil.isEmptyString(location)) {
            JSONObject locationObject = new JSONObject(location);
            mapEventInfo.setHasLocation(true);
            mapEventInfo.setLocation(locationObject.getString("location"));
            mapEventInfo.setLatitude((float)locationObject.getDouble("latitude"));
            mapEventInfo.setLongitude((float)locationObject.getDouble("longitude"));
            break;
          }
        }
      }

      mapEventInfo.setEventStreamJson(eventStreamJson);
      mapEventInfo.setStreamId(streamId);
      mapEventInfo.setOperationSyncId(operationSyncId);
      mapEventInfo.setInterfaceName(interfaceName);
      mapEventInfo.setOperationName(operationName);
      mapEventInfo.setStreaming(streaming);
    } catch (JSONException e) {
      LogUtil.d(TAG, "[getMapEventInfo]\n" + e.getMessage());
      return null;
    }
    return mapEventInfo;
  }

  /**
   * MAP 으로 부터 전달받은 MapDirective 데이터를 파싱하여 MapDirectiveInfo 객체에 채워주는 함수입니다.
   *
   * @param mapDirective MAP 으로 부터 전달받은 MapDirective 데이터
   * @return MapDirectiveInfo 객체
   */
  public static MapDirectiveInfo getMapDirectiveInfo(
      MapDirective mapDirective) {
    MapDirectiveInfo mapDirectiveInfo = new MapDirectiveInfo();
    mapDirectiveInfo.setDirectiveStream(mapDirective.getDirective());
    mapDirectiveInfo.setMapException(mapDirective.getException());
    mapDirectiveInfo.setByteStream(mapDirective.getBytes());
    mapDirectiveInfo.setByteTextStream(mapDirective.getTextBytes());
    mapDirectiveInfo.setStreamEnd(mapDirective.getStreamEnd());
    mapDirectiveInfo.setStreamBreak(mapDirective.getStreamBreak());
    mapDirectiveInfo.setDirectiveText(mapDirective.getText());
    mapDirectiveInfo.setStreamMeta(mapDirective.getMeta());
    if (!mapDirectiveInfo.getDirectiveStream().toString().isEmpty()) {
      mapDirectiveInfo.setDirectiveStreamInfo(
          getDirectiveStreamInfo(mapDirectiveInfo.getDirectiveStream()));
      try {
        String directiveStreamJson = JsonFormat.printer()
            .print(mapDirectiveInfo.getDirectiveStream());
        mapDirectiveInfo.setDirectiveStreamJson(directiveStreamJson);
      } catch (InvalidProtocolBufferException e) {
        e.printStackTrace();
        LogUtil.d(TAG, "[getMapDirectiveInfo]\n" + e.getMessage());
        return null;
      }
    }
    if (!mapDirectiveInfo.getMapException().toString().isEmpty()) {
      try {
        String mapExceptionJson = JsonFormat.printer()
            .print(mapDirectiveInfo.getMapException());
        mapDirectiveInfo.setMapExceptionJson(mapExceptionJson);
      } catch (InvalidProtocolBufferException e) {
        e.printStackTrace();
        LogUtil.d(TAG, "[getMapDirectiveInfo]\n" + e.getMessage());
        return null;
      }
    }
    return mapDirectiveInfo;
  }

  /**
   * MAP 으로 부터 전달받은 MapDirecive 데이터에서 interface, streamId, operationSyncId 관련
   * 데이터만 파싱하여 리턴해 주는 함수입니다.
   *
   * @param directiveStream MapDirective 의 directiveStream 데이터
   * @return DirectiveStreamInfo 객체
   */
  public static DirectiveStreamInfo getDirectiveStreamInfo(
      DirectiveStream directiveStream) {
    DirectiveStreamInfo streamInfo = new DirectiveStreamInfo();
    streamInfo.setInterfaceName(directiveStream.getInterface().getInterface());
    streamInfo.setOperationName(directiveStream.getInterface().getOperation());
    streamInfo.setStreamId(directiveStream.getStreamId());
    streamInfo.setOperationSyncId(directiveStream.getOperationSyncId());
    return streamInfo;
  }

  /**
   * contexts 정보를 삭제 합니다.
   *
   * @param eventStreamJson
   * @return
   */
  public static String getContextsJsonRemoved(String eventStreamJson) {
    try {
      JSONObject eventValueObject = new JSONObject(eventStreamJson);
      eventValueObject.put("contexts", null);
      eventStreamJson = eventValueObject.toString();

    } catch (JSONException e) {
      LogUtil.d(TAG, "[getContextsJsonRemoved]\n" + e.getMessage());
    }
    return eventStreamJson;
  }
}