package ai.maum.m2u.cdk.grpclib.chatbot.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 앱에서 사용될 데이터를 저장하고 로드하기 위한 Preference agent 클래스 입니다.
 */

public class PreferenceAgent {

  private static final String TAG = PreferenceAgent.class.getSimpleName();
  // Preference File Name
  private static final String SHARED_FILE_TITLE = "pref_ai_maum_m2u_cdk";

  /**
   * 문자열 타입의 데이터를 로드한다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 가져올 값에 대한 키값
   * @return 키값에 대한 값
   */
  public static String getStringSharedData(Context context, String key) {
    if (null == context) {
      return null;
    }
    SharedPreferences pref = context.getSharedPreferences(SHARED_FILE_TITLE,
        Context.MODE_PRIVATE);
    return pref.getString(key, null);
  }

  /**
   * 문자열 타입의 데이터를 저장하는 함수입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 저장될 값에 대한 키값
   * @param data 저장될 값
   */

  public static void setStringSharedData(Context context,
      String key, String data) {
    SharedPreferences pref = context.getSharedPreferences(SHARED_FILE_TITLE,
        Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString(key, data);
    editor.apply();
  }

  /**
   * 정수형 타입의 데이터를 로드한다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 가져올 값에 대한 키값
   * @return 키값에 대한 값
   */
  public static int getIntSharedData(Context context, String key) {
    if (null == context) {
      return -1;
    }
    SharedPreferences pref = context
        .getSharedPreferences(SHARED_FILE_TITLE, Context.MODE_PRIVATE);
    return pref.getInt(key, -1);
  }

  /**
   * 정수형 타입의 데이터를 저장하는 함수입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 저장될 값에 대한 키값
   * @param data 저장될 값
   */

  public static void setIntSharedData(Context context, String key, int data) {
    SharedPreferences pref = context
        .getSharedPreferences(SHARED_FILE_TITLE, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putInt(key, data);
    editor.apply();
  }

  /**
   * 롱형 타입의 데이터를 로드한다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 가져올 값에 대한 키값
   * @return 키값에 대한 값
   */
  public static long getLongSharedData(Context context, String key) {
    if (null == context) {
      return -1;
    }
    SharedPreferences pref = context
        .getSharedPreferences(SHARED_FILE_TITLE, Context.MODE_PRIVATE);
    return pref.getLong(key, -1);
  }

  /**
   * 롱형 타입의 데이터를 저장하는 함수입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 저장될 값에 대한 키값
   * @param data 저장될 값
   */

  public static void setlongSharedData(Context context, String key, long data) {
    SharedPreferences pref = context
        .getSharedPreferences(SHARED_FILE_TITLE, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putLong(key, data);
    editor.apply();
  }

  /**
   * float형 타입의 데이터를 로드한다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 가져올 값에 대한 키값
   * @return 키값에 대한 값
   */
  public static float getFloatSharedData(Context context, String key) {
    if (null == context) {
      return -1;
    }
    SharedPreferences pref = context
        .getSharedPreferences(SHARED_FILE_TITLE, Context.MODE_PRIVATE);
    return pref.getFloat(key, 0.0f);
  }

  /**
   * float형 타입의 데이터를 저장하는 함수입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 저장될 값에 대한 키값
   * @param data 저장될 값
   */

  public static void setFloatSharedData(Context context, String key, float
      data) {
    SharedPreferences pref = context
        .getSharedPreferences(SHARED_FILE_TITLE, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putFloat(key, data);
    editor.apply();
  }

  /**
   * 불린형 타입의 데이터를 로드한다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 가져올 값에 대한 키값
   * @return 키값에 대한 값
   */
  public static boolean getBooleanSharedData(Context context, String key) {
    if (null == context) {
      return false;
    }
    SharedPreferences pref = context
        .getSharedPreferences(SHARED_FILE_TITLE, Context.MODE_PRIVATE);
    return pref.getBoolean(key, false);
  }

  /**
   * 불린형 타입의 데이터를 저장하는 함수입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @param key 저장될 값에 대한 키값
   * @param flag 저장될 값
   */
  public static void setBooleanSharedData(Context context,
      String key, boolean flag) {
    SharedPreferences pref = context.getSharedPreferences(SHARED_FILE_TITLE,
        Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putBoolean(key, flag);
    editor.apply();
  }
}
