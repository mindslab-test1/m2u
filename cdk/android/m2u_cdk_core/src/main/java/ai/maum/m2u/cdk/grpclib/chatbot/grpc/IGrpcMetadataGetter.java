package ai.maum.m2u.cdk.grpclib.chatbot.grpc;

import io.grpc.Metadata;

/**
 * GrpcHeaderClientInterceptor 객체에 포함될 MetaDataGetter 에 대한 인터페이스 입니다.
 */

public interface IGrpcMetadataGetter {

  /**
   * 인터페이스를 구현한 곳에 메타 데이터를 전달하는 함수입니다.
   *
   * @param md Metadata
   */
  void onMetaData(Metadata md);
}
