package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;

/**
 * gRPC 상태 정보를 JS 에게 전달하기 위해 사용되는 데이터 전송 객체에 대한 클래스입니다.
 * receiveError()에 대한 제공, error detail 전달 방법 추후 제공 #107
 * <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */

public class GrpcStatusDTO {

  private int errorCode = 0;
  private String errorMessage = Const.EMPTY_STRING;
  // 바이너리
  private String errorDetail = Const.EMPTY_STRING;
  // 바이너리를 복원하면, object, json
  // 다음버전
  // 예제 제공..
  private Object errorObject;

  public GrpcStatusDTO() {
  }

  public int getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(int errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getErrorDetail() {
    return errorDetail;
  }

  public void setErrorDetail(String errorDetail) {
    this.errorDetail = errorDetail;
  }

  public Object getErrorObject() {
    return errorObject;
  }

  public void setErrorObject(Object errorObject) {
    this.errorObject = errorObject;
  }
}
