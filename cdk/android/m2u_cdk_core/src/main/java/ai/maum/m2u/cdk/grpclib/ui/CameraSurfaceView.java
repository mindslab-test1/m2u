package ai.maum.m2u.cdk.grpclib.ui;

import ai.maum.m2u.cdk.grpclib.ui.dto.CameraDTO.CameraParam;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import android.content.Context;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CameraSurfaceView extends ViewGroup implements SurfaceHolder
    .Callback {

  private static final String TAG = CameraSurfaceView.class.getSimpleName();
  /**
   * Max preview width that is guaranteed by Camera2 API
   */
  private static final int MAX_PREVIEW_WIDTH = 1024;
  /**
   * Max preview height that is guaranteed by Camera2 API
   */
  private static final int MAX_PREVIEW_HEIGHT = 768;

  SurfaceView mSurfaceView;
  SurfaceHolder mHolder;
  Size mPreviewSize;
  List<Size> mSupportedPreviewSizes;
  Camera mCamera;
  CameraParam mCameraParam;
  int mWidth, mHeight;

  CameraSurfaceView(Context context, SurfaceView sv, CameraParam cameraParam,
      int width, int height) {
    super(context);
    mWidth = width;
    mHeight = height;
    mSurfaceView = sv;
    mCameraParam = cameraParam;
    mHolder = mSurfaceView.getHolder();
    mHolder.addCallback(this);
    mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    // We purposely disregard child measurements because act as a
    // wrapper to a SurfaceView that centers the camera preview instead
    // of stretching it.
    final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
    final int height = resolveSize(getSuggestedMinimumHeight(),
        heightMeasureSpec);
    setMeasuredDimension(width, height);
    if (mSupportedPreviewSizes != null) {
      mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width,
          height);
    }
  }

  @Override
  protected void onLayout(boolean changed, int l, int t, int r, int b) {
    if (changed && getChildCount() > 0) {
      final View child = getChildAt(0);
      final int width = r - l;
      final int height = b - t;
      int previewWidth = width;
      int previewHeight = height;
      if (mPreviewSize != null) {
        previewWidth = mPreviewSize.width;
        previewHeight = mPreviewSize.height;
      }

      // Center the child SurfaceView within the parent.
      if (width * previewHeight > height * previewWidth) {
        final int scaledChildWidth = previewWidth * height / previewHeight;
        child.layout((width - scaledChildWidth) / 2, 0,
            (width + scaledChildWidth) / 2, height);
      } else {
        final int scaledChildHeight = previewHeight * width / previewWidth;
        child.layout(0, (height - scaledChildHeight) / 2,
            width, (height + scaledChildHeight) / 2);
      }
    }
  }

  @Override
  public void surfaceCreated(SurfaceHolder holder) {
    //Toast.makeText(getContext(), "surfaceCreated", Toast.LENGTH_LONG).show();
    Log.d("@@@", "surfaceCreated");
    // The Surface has been created, acquire the camera and tell it where
    // to draw.
    try {
      if (mCamera != null) {
        mCamera.setPreviewDisplay(holder);
      }
      setWillNotDraw(false);
    } catch (IOException exception) {
      Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
    }
  }

  @Override
  protected void onDraw(Canvas canvas) {
    // Do nothing.
  }

  public float getRatio(int w, int h) {
    if (h == 0) {
      return 0;
    }
    return (float) w / (float) h;
  }

  @Override
  public void surfaceChanged(SurfaceHolder holder, int format, int width,
      int height) {
    if (mCamera != null) {
      float bRatio = 0;
      float cRatio = (float) width / (float) height;
      Camera.Parameters parameters = mCamera.getParameters();
      List<Size> allSizes = parameters.getSupportedPreviewSizes();
      Size size = allSizes.get(0); // get top size

      // Camera.Size 확인
      LogUtil.e("mmcamera",
          "Check Camera.Size : " + size.width + "," + size.height);

      List<Size> findSizes = new ArrayList<Size>();
      // Surface View (화면)과 가장 가까운 카메라 뷰의 종횡비를 찾는다.
      for (int i = 0; i < allSizes.size(); i++) {
        if (Math.abs(cRatio - bRatio) > Math.abs(cRatio - this
            .getRatio(allSizes.get(i).width, allSizes.get(i).height))) {
          bRatio = this.getRatio(allSizes.get(i).width, allSizes.get(i).height);
          size = allSizes.get(i);
          findSizes.add(size);

          // mmcamera size 확인
          LogUtil.e("mmcamera",
              "Check for loop size : [index] " + i + ", " + size.width + ","
                  + size.height);
        }
      }

      // 위의 로직에서 마지막의 해상도로 설정되면 저해상도로 Preview 가 설정될 수 있다.
      // 저해상도로 설정되는 경우 Crop된 이미지의 해상도가 현저히 떨어지기 때문에 유효한 해상도중에 1920에 근접한 해상도를
      // 선택한다.
      if (findSizes.size() > 0) {
        int sizeX = 0;
        int findIndex = -1;
        for (int nLoop = 0 ; nLoop < findSizes.size() ; nLoop++) {
          int findSizeX = findSizes.get(nLoop).width;
          if (findSizeX > sizeX && findSizeX <= 1920) {
            sizeX = findSizeX;
            findIndex = nLoop;
          }
        }
        if (findIndex > -1) {
          size = findSizes.get(findIndex);
        }
      }

      // 2018.09.27 특정단말 (V20, V30)에서 최종 width:1280, height:768 일 경우
      // Surface setParameters failed 에러가 발생하여 아래와 같이 예외 추가
      // 2018.08.16 특정단말 (G7)에서 최종 width:1440, height:720 일 경우 Surface setParameters failed 에러가 발생하여
      // 예외처리를 아래와 같이 추가
      if ((size.width == 1440 && size.height == 720)
          || (size.width == 1280 && size.height == 768)) {
        size.width = 1920;
        size.height = 1080;
      }

      //set max Preview Size
      LogUtil.d("mmcamera",
          "Result Size : " + size.width + "," + size.height + " " + width + ","
              + height);
      parameters.setPreviewSize(size.width, size.height);
      parameters.setPictureSize(size.width, size.height);
      // Important: Call startPreview() to start updating the preview surface.
      // Preview must be started before you can take a picture.
      mCamera.setParameters(parameters);
      mCamera.startPreview();
    }
  }

  @Override
  public void surfaceDestroyed(SurfaceHolder holder) {
    // Surface will be destroyed when we return, so stop the preview.
    if (mCamera != null) {
      mCamera.stopPreview();
    }
  }

  public void setCamera(Camera camera) {
    if (mCamera != null) {
      // Call stopPreview() to stop updating the preview surface.
      mCamera.stopPreview();
      // Important: Call release() to release the camera for use by other
      // applications. Applications should release the camera immediately
      // during onPause() and re-open() it during onResume()).
      mCamera.release();
      mCamera = null;
    }
    mCamera = camera;
    if (mCamera != null) {
      List<Size> localSizes = mCamera.getParameters()
          .getSupportedPreviewSizes();
      mSupportedPreviewSizes = localSizes;
      requestLayout();
      // get Camera parameters
      Camera.Parameters params = mCamera.getParameters();
      List<String> focusModes = params.getSupportedFocusModes();
      /*if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
        // set the focus mode
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        // set Camera parameters
        mCamera.setParameters(params);
      }*/
      if (focusModes
          .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        mCamera.setParameters(params);
      }

      Size size = localSizes.get(0); // get top size
      float cRatio = mWidth / mHeight;
      float bRatio = 0.0f;
      // Surface View (화면)과 가장 가까운 카메라 뷰의 종횡비를 찾는다.
      for (int i = 0; i < localSizes.size(); i++) {
        if (Math.abs(cRatio - bRatio) > Math.abs(cRatio - this
            .getRatio(localSizes.get(i).width, localSizes.get(i).height))) {
          bRatio = this
              .getRatio(localSizes.get(i).width, localSizes.get(i).height);
          size = localSizes.get(i);
        }
      }
      //set max Preview Size
      LogUtil.d("mmcamera",
          "Size 3 : " + size.width + "," + size.height + " " + mWidth + ","
              + mHeight);
      //params.setPreviewSize(size.width, size.height);
      mCamera.setParameters(params);
      try {
        mCamera.setPreviewDisplay(mHolder);
      } catch (IOException e) {
        LogUtil.e(TAG, e.getMessage());
      }
      // Important: Call startPreview() to start updating the preview
      // surface. Preview must be started before you can take a picture.
      mCamera.startPreview();
    }
  }

  private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
    final double ASPECT_TOLERANCE = 0.1;
    double targetRatio = (double) w / h;
    if (sizes == null) {
      return null;
    }

    Size optimalSize = null;
    double minDiff = Double.MAX_VALUE;
    int targetHeight = h;
    int width = MAX_PREVIEW_WIDTH;
    int height = MAX_PREVIEW_HEIGHT;
    if (mCameraParam.getRes().getMax_x() <= 0) {
      width = MAX_PREVIEW_WIDTH;
    }
    if (mCameraParam.getRes().getMax_y() <= 0) {
      height = MAX_PREVIEW_HEIGHT;
    }
    if (mCamera != null) {
      double ratio = (double) width / height;
      if (Math.abs(ratio - targetRatio) <= ASPECT_TOLERANCE) {
        if (Math.abs(height - targetHeight) < minDiff) {
          optimalSize = mCamera.new Size(0, 0);
          optimalSize.width = width;
          optimalSize.height = height;
          minDiff = Math.abs(height - targetHeight);
        }
      }
      // Cannot find the one match the aspect ratio, ignore the requirement
      if (optimalSize == null) {
        minDiff = Double.MAX_VALUE;
        if (Math.abs(height - targetHeight) < minDiff) {
          optimalSize = mCamera.new Size(0, 0);
          optimalSize.width = width;
          optimalSize.height = height;
          minDiff = Math.abs(height - targetHeight);
        }
      }
    }
    return optimalSize;
  }
}