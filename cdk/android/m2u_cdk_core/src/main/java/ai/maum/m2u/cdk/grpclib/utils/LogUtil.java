package ai.maum.m2u.cdk.grpclib.utils;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Device Log 를 Logcat 으로 출력 또는 파일로 저장하는 함수들로 구성된 클래스 입니다.
 * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
 */

public class LogUtil {

  /**
   * Logcat 로그를 파일에 저장할 때 사용되는 파일 저장 경로입니다.
   */
  private static final String LOG_FOLDER = "/cdk/logs";
  /**
   * Logcat 로그를 파일에 저장할 때 사용되는 파일 이름입니다.
   */
  private static final String LOG_FILE = "log.txt";
  /**
   * Logcat에 로그를 출력할지 여부 설정하는 변수입니다.
   */
  private static boolean mIsPrintLog = false;
  /**
   * Logcat의 로그를 파일에 저장할지 여부 설정하는 변수입니다.
   */
  private static boolean mIsSaveLogToFile = false;

  /**
   * 클래스 생성자 입니다.
   */
  private LogUtil() {
  }

  /**
   * Logcat 에 로그를 출력할지 여부를 설정하는 함수입니다.
   *
   * @param isPrint Logcat 에 로그를 출력하는 경우 true
   */
  public static void setIsPrintLog(boolean isPrint) {
    mIsPrintLog = isPrint;
  }

  /**
   * 로그를 Logcat 에 출력되도록 설정되어 있는지 확인하는 함수입니다.
   *
   * @return Logcat 에 출력되도록 설정된 경우 true
   */
  public static boolean isPrintLog() {
    return mIsPrintLog;
  }

  /**
   * Logcat 에 출력되는 로그를 파일에 저장할지 여부를 설정하는 함수입니다.
   *
   * @param isSave 파일에 저장하는 경우 true
   */
  public static void setIsSaveLogToFile(boolean isSave) {
    mIsSaveLogToFile = isSave;
  }

  /**
   * Logcat 에 출력되는 로그를 파일에 저장하도록 설정되어 있는지 확인하는 함수입니다.
   *
   * @return 파일에 저장되도록 설정된 경우 true
   */
  public static boolean isSaveLogToFile() {
    return mIsSaveLogToFile;
  }

  /**
   * Debug 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   */
  public static void d(String tag, String msg) {
    if (mIsPrintLog) {
      Log.d(tag, StringUtil.nullToBlankTrim(msg));
    }
    if (mIsSaveLogToFile) {
      saveLog(tag, StringUtil.nullToBlankTrim(msg));
    }
  }

  /**
   * Throwable 메시지를 포함하여 Debug 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   * @param tr 출력될 Throwable 메시지
   */
  public static void d(String tag, String msg, Throwable tr) {
    if (mIsPrintLog) {
      StringBuffer buf = new StringBuffer();
      buf.append(StringUtil.nullToBlankTrim(msg));
      buf.append('\n');
      buf.append(getStackTraceString(tr));
      d(tag, buf.toString());
    }
  }

  /**
   * Error 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   */
  public static void e(String tag, String msg) {
    if (mIsPrintLog) {
      Log.e(tag, StringUtil.nullToBlankTrim(msg));
    }

    if (mIsSaveLogToFile) {
      saveLog(tag, StringUtil.nullToBlankTrim(msg));
    }
  }

  /**
   * Throwable 메시지를 포함하여 Error 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   * @param tr 출력될 Throwable 메시지
   */
  public static void e(String tag, String msg, Throwable tr) {
    if (mIsPrintLog) {
      StringBuffer buf = new StringBuffer();
      buf.append(StringUtil.nullToBlankTrim(msg));
      buf.append('\n');
      buf.append(getStackTraceString(tr));
      e(tag, buf.toString());
    }
  }

  /**
   * Info 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   */
  public static void i(String tag, String msg) {
    if (mIsPrintLog) {
      Log.i(tag, StringUtil.nullToBlankTrim(msg));
    }
    if (mIsSaveLogToFile) {
      saveLog(tag, StringUtil.nullToBlankTrim(msg));
    }
  }

  /**
   * Throwable 메시지를 포함하여 Info 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   * @param tr 출력될 Throwable 메시지
   */
  public static void i(String tag, String msg, Throwable tr) {
    if (mIsPrintLog) {
      StringBuffer buf = new StringBuffer();
      buf.append(StringUtil.nullToBlankTrim(msg));
      buf.append('\n');
      buf.append(getStackTraceString(tr));
      i(tag, buf.toString());
    }
  }

  /**
   * Verbose 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   */
  public static void v(String tag, String msg) {
    if (mIsPrintLog) {
      Log.v(tag, StringUtil.nullToBlankTrim(msg));
    }
    if (mIsSaveLogToFile) {
      saveLog(tag, StringUtil.nullToBlankTrim(msg));
    }
  }

  /**
   * Throwable 메시지를 포함하여 Verbose 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   * @param tr 출력될 Throwable 메시지
   */
  public static void v(String tag, String msg, Throwable tr) {
    if (mIsPrintLog) {
      StringBuffer buf = new StringBuffer();
      buf.append(StringUtil.nullToBlankTrim(msg));
      buf.append('\n');
      buf.append(getStackTraceString(tr));
      v(tag, buf.toString());
    }
  }

  /**
   * Warn 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   */
  public static void w(String tag, String msg) {
    if (mIsPrintLog) {
      Log.w(tag, StringUtil.nullToBlankTrim(msg));
    }
    if (mIsSaveLogToFile) {
      saveLog(tag, StringUtil.nullToBlankTrim(msg));
    }
  }

  /**
   * Throwable 메시지를 포함하여 Warn 형식으로 로그를 출력하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   * @param tr 출력될 Throwable 메시지
   */
  public static void w(String tag, String msg, Throwable tr) {
    if (mIsPrintLog) {
      StringBuffer buf = new StringBuffer();
      buf.append(StringUtil.nullToBlankTrim(msg));
      buf.append('\n');
      buf.append(getStackTraceString(tr));
      w(tag, buf.toString());
    }
  }

  /**
   * Throwable 로 부터 Stack Trace 메시지를 추출하여 문자열로 만드는 함수입니다.
   *
   * @param tr 출력될 Throwable 메시지
   */
  public static String getStackTraceString(Throwable tr) {
    if (tr == null) {
      return "";
    }
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    tr.printStackTrace(pw);
    return sw.toString();
  }

  /**
   * 로그가 저장되어 있는 로그파일의 경로를 얻을 수 있는 함수입니다.
   *
   * @return 로그가 저장되어 있는 로그파일의 경로
   */
  private static String getLogFilePath() {
    // 외부 저장 장치의 상태를 얻어온다.
    String ess = Environment.getExternalStorageState();
    String sdCardPath = null;
    String folderPath = null;
    String logFilePath = null;
    if (ess.equals(Environment.MEDIA_MOUNTED)) {
      sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
      if (!StringUtil.isBlank(sdCardPath)) {
        folderPath = sdCardPath + LOG_FOLDER;
        File folder = new File(folderPath);
        if (!folder.exists()) {
          folder.mkdirs();
        }
        logFilePath = folderPath + "/" + LOG_FILE;
        return logFilePath;
      }
    }
    return "";
  }

  /**
   * Logcat 에 출력된 로그를 파일에 저장하는 함수입니다.
   *
   * @param tag 클래스 tag 이름
   * @param msg 출력될 로그 메시지
   */
  private static void saveLog(String tag, String msg) {
    String logFilePath = getLogFilePath();
    if (!StringUtil.isBlank(logFilePath)) {
      String log = String.format("%s : [%s] %s\r\n", DateUtil
          .getKorCurrentFormattedDate("HH:mm:ss.SSS"), tag, msg);
      BufferedWriter out = null;
      try {
        File logFile = new File(logFilePath);
        out = new BufferedWriter(new FileWriter(logFile, true), 1024);
        out.write(log);
      } catch (IOException e) {
        LogUtil.e(TAG, e.getMessage());
      } finally {
        if (out != null) {
          try {
            out.close();
          } catch (IOException e) {
            LogUtil.e(TAG, e.getMessage());
          }
        }
      }
    }
  }
}
