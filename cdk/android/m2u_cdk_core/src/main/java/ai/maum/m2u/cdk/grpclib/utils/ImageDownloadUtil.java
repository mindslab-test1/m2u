package ai.maum.m2u.cdk.grpclib.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import java.io.InputStream;
import java.net.URL;

/**
 * Http url 로 부터 이미지를 다운받아서 ImageView 에 set 하는 클래스입니다.
 * 인자로 함께 넘겨 받은 opacity 값으로 투명처리를 합니다.
 */

public class ImageDownloadUtil extends AsyncTask<String, String, Bitmap> {

  private static final String TAG = ImageDownloadUtil.class
      .getSimpleName();

  private Bitmap downloadBitmap;
  private ImageView downloadImage;
  private float opacity;

  public ImageDownloadUtil(ImageView downloadImage, float opacity) {
    this.downloadImage = downloadImage;
    this.opacity = opacity;
  }

  @Override
  protected Bitmap doInBackground(String... strings) {
    try {
      downloadBitmap = BitmapFactory.decodeStream((InputStream)new URL(strings[0])
          .getContent());
      LogUtil.d(TAG, "DownloadImage : url = " + strings[0]);
    } catch (Exception e) {
      LogUtil.e(TAG, "DownloadImageTask download error");
    }
    return downloadBitmap;
  }

  @Override
  protected void onPostExecute(Bitmap image) {
    if (image != null) {
      downloadImage.setVisibility(View.VISIBLE);
      downloadImage.setImageBitmap(image);
      int rate = (int)(opacity * 100);
      int settingAlphaValue = (int)(255 * rate) / 100;
      if (Build.VERSION.SDK_INT < 16) {
        downloadImage.setAlpha(settingAlphaValue);
      }
      else {
        downloadImage.setImageAlpha(settingAlphaValue);
      }
      LogUtil.d(TAG, "DownloadImage : set to imageview");
    } else {
      // do nothing
    }
  }
}
