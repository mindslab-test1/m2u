package ai.maum.m2u.cdk.grpclib.chatbot.map;

/**
 * MAP 의 Event, Directive 에서 사용되는 Interface Name 을 정의한 Constant 클래스 입니다.
 */

public class MapInterfaceName {

  public static final String INTERFACE_SPEECHRECOGNIZER = "SpeechRecognizer";
  public static final String INTERFACE_DIALOG = "Dialog";
  public static final String INTERFACE_SPEECHSYNTHESIZER = "SpeechSynthesizer";
  public static final String INTERFACE_AUDIOPLAY = "AudioPlayer";
  public static final String INTERFACE_VIEW = "View";
  public static final String INTERFACE_MICROPHONE = "Microphone";
  public static final String INTERFACE_IMAGERECOGNIZER = "ImageRecognizer";
  public static final String INTERFACE_AVATAR = "Avatar";
  public static final String INTERFACE_AUTHENTICATION = "Authentication";
}
