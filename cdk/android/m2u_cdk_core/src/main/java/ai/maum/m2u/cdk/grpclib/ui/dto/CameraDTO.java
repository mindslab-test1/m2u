package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * camera 설정 정보를 JS 로 부터 전달받거나 camera 상태를 JS 에게 전달하기 위해 사용되는 데이터 전송 객체에
 * 대한 클래스입니다.
 * <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */

public class CameraDTO {

  /**
   * camera open, close 시 실패 종류를 정의한 클래스입니다.
   */
  public static class CameraFailure {

    public static final String CAMERA_ERROR_UNKNOWN = "CAMERA_ERROR_UNKNOWN";
    public static final String CAMERA_OPEN_ERROR = "CAMERA_OPEN_ERROR";
    public static final String CAMERA_CLOSE_ERROR = "CAMERA_CLOSE_ERROR";
    public static final String CAMERA_CAPTURE_CANCELLED = "CAMERA_CAPTURE_CANCELLED";
    public static final String CAMERA_INSUFFICIENT_MEMORY = "CAMERA_INSUFFICIENT_MEMORY";
    public static final String CAMERA_STORE_FAILED = "CAMERA_STORE_FAILED";
    public static final String CAMERA_TIMER_EXPIRED = "CAMERA_TIMER_EXPIRED";
    public static final String CAMERA_PERMISSION_DENIED = "CAMERA_PERMISSION_DENIED";
  }

  /**
   * camera 이벤트를 정의한 클래스입니다.
   */
  public static class CameraEvent {

    public static final String UNKNOWN = "UNKNOWN";
    public static final String OPEN_SUCCESS = "OPEN_SUCCESS";
    public static final String OPEN_FAILURE = "OPEN_FAILURE";
    public static final String IMAGE_CAPTURED = "IMAGE_CAPTURED";
    public static final String CLOSED = "CLOSED";
  }

  /**
   * camera open 시 파라미터 구성에 대한 클래스 입니다.
   */
  public static class CameraParam implements Parcelable {

    public static final Parcelable.Creator<CameraParam> CREATOR = new Parcelable.Creator<CameraParam>() {

      @Override
      public CameraParam[] newArray(int size) {
        return new CameraParam[size];
      }

      @Override
      public CameraParam createFromParcel(Parcel source) {
        return new CameraParam(source);
      }
    };
    // image capture 해상도 정보
    public static final EtcDTO.Resolution res = new EtcDTO.Resolution();
    // rear Camera = true, front Camera: false
    private boolean rearCamera = false;
    // camera, crop 화면 보여질 시간이며 타이머를 구동할 때 사용됩니다.
    private int openInterval = 10;

    // 워터마트 이미지 URL
    private String watermarkImageUrl = "";

    // 출력 Preview, 높이에서 워터마크가 차지할 비율
    private float watermarkResizeRateOfHeight = 0.0f;

    // watermark image 를 보여줄 때 투명도(0에서 1사이의 값)
    private float opacity = 0.0f;

    // 카메라 촬영 후 Crop 화면에 보여질 워터마크 이미지 URL
    private String confirmImageUrl = "";

    // 카메라 촬영 후 Crop 화면, 높이에서 워터마크가 차지할 비율
    private float confirmResizeRateOfHeight = 0.0f;

    // 안내 문자열
    private String guideText = "";

    public CameraParam() {
    }

    public CameraParam(Parcel in) {
      readFromParcel(in);
    }

    public EtcDTO.Resolution getRes() {
      return res;
    }

    public void setRes(EtcDTO.Resolution res) {
      this.res = res;
    }

    public boolean isRearCamera() {
      return rearCamera;
    }

    public void setRearCamera(boolean rearCamera) {
      this.rearCamera = rearCamera;
    }

    public int getOpenInterval() {
      return openInterval;
    }

    public void setOpenInterval(int openInterval) {
      this.openInterval = openInterval;
    }

    public String getWatermarkImageUrl() {
      return watermarkImageUrl;
    }

    public void setWatermarkImageUrl(String watermarkImageUrl) {
      this.watermarkImageUrl = watermarkImageUrl;
    }

    public float getWatermarkResizeRateOfHeight() {
      return watermarkResizeRateOfHeight;
    }

    public void setWatermarkResizeRateOfHeight(
        float watermarkResizeRateOfHeight) {
      this.watermarkResizeRateOfHeight = watermarkResizeRateOfHeight;
    }

    public float getOpacity() {
      return opacity;
    }

    public void setOpacity(float opacity) {
      this.opacity = opacity;
    }

    public String getGuideText() {
      return guideText;
    }

    public void setGuideText(String guideText) {
      this.guideText = guideText;
    }

    public String getConfirmImageUrl() {
      return confirmImageUrl;
    }

    public void setConfirmImageUrl(String confirmImageUrl) {
      this.confirmImageUrl = confirmImageUrl;
    }

    public float getConfirmResizeRateOfHeight() {
      return confirmResizeRateOfHeight;
    }

    public void setConfirmResizeRateOfHeight(float confirmResizeRateOfHeight) {
      this.confirmResizeRateOfHeight = confirmResizeRateOfHeight;
    }

    @Override
    public int describeContents() {
      return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      dest.writeInt(this.rearCamera ? 1 : 0);
      dest.writeInt(this.openInterval);
      dest.writeParcelable(this.res, 0);
      dest.writeString(this.watermarkImageUrl);
      dest.writeFloat(this.watermarkResizeRateOfHeight);
      dest.writeFloat(this.opacity);
      dest.writeString(this.confirmImageUrl);
      dest.writeFloat(this.confirmResizeRateOfHeight);
      dest.writeString(this.guideText);
    }

    private void readFromParcel(Parcel in) {
      this.rearCamera = (in.readInt() == 1) ? true : false;
      this.openInterval = in.readInt();
      this.res = in.readParcelable(EtcDTO.Resolution.class.getClassLoader());
      this.watermarkImageUrl = in.readString();
      this.watermarkResizeRateOfHeight = in.readFloat();
      this.opacity = in.readFloat();
      this.confirmImageUrl = in.readString();
      this.confirmResizeRateOfHeight = in.readFloat();
      this.guideText = in.readString();
    }
  }

  /**
   * camera 의 상태 정보를 구성하는 클래스 입니다.
   */
  public static class CameraStatus {

    // thumbnail image data, image url 정보를 구성하는 클래스의 객체
    public static final EtcDTO.ImageData imageData = new EtcDTO.ImageData();
    // camera 의 event 종류
    private String eventType = CameraEvent.UNKNOWN;
    // 추가적인 메시지 여부
    private boolean hasReason = false;
    // hasReason이 true인 경우 reason
    private String failure = CameraFailure.CAMERA_ERROR_UNKNOWN;
    // 상세한 에러 메시지
    private String message = Const.EMPTY_STRING;

    public CameraStatus() {
    }

    public EtcDTO.ImageData getImageData() {
      return imageData;
    }

    public void setImageData(EtcDTO.ImageData imageData) {
      this.imageData = imageData;
    }

    public String getEventType() {
      return eventType;
    }

    public void setEventType(String eventType) {
      this.eventType = eventType;
    }

    public boolean isHasReason() {
      return hasReason;
    }

    public void setHasReason(boolean hasReason) {
      this.hasReason = hasReason;
    }

    public String getFailure() {
      return failure;
    }

    public void setFailure(String failure) {
      this.failure = failure;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
}
