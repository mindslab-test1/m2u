package ai.maum.m2u.cdk.grpclib.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.TextureView;

/**
 * <p>A TextureView can be used to display a content stream. Such a content
 * stream can for instance be a video or an OpenGL scene. The content stream
 * can come from the application's process as well as a remote process.</p>
 *
 * <p>TextureView can only be used in a hardware accelerated window. When
 * rendered in software, TextureView will draw nothing.</p>
 *
 * <p>Unlike SurfaceView, TextureView does not create a separate
 * window but behaves as a regular View. This key difference allows a
 * TextureView to be moved, transformed, animated, etc. For instance, you
 * can make a TextureView semi-translucent by calling
 * <code>myView.setAlpha(0.5f)</code>.</p>
 *
 * <p>Using a TextureView is simple: all you need to do is get its
 * SurfaceTexture. The SurfaceTexture can then be used to
 * render content. The following example demonstrates how to render the
 * camera preview into a TextureView:</p>
 *
 * <pre>
 *  public class LiveCameraActivity extends Activity implements TextureView.SurfaceTextureListener {
 *      private Camera mCamera;
 *      private TextureView mTextureView;
 *
 *      protected void onCreate(Bundle savedInstanceState) {
 *          super.onCreate(savedInstanceState);
 *
 *          mTextureView = new TextureView(this);
 *          mTextureView.setSurfaceTextureListener(this);
 *
 *          setContentView(mTextureView);
 *      }
 *
 *      public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
 *          mCamera = Camera.open();
 *
 *          try {
 *              mCamera.setPreviewTexture(surface);
 *              mCamera.startPreview();
 *          } catch (IOException ioe) {
 *              // Something bad happened
 *          }
 *      }
 *
 *      public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
 *          // Ignored, Camera does all the work for us
 *      }
 *
 *      public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
 *          mCamera.stopPreview();
 *          mCamera.release();
 *          return true;
 *      }
 *
 *      public void onSurfaceTextureUpdated(SurfaceTexture surface) {
 *          // Invoked every time there's a new Camera preview frame
 *      }
 *  }
 * </pre>
 *
 * <p>A TextureView's SurfaceTexture can be obtained either by invoking
 * {@link #getSurfaceTexture()} or by using a {@link SurfaceTextureListener}.
 * It is important to know that a SurfaceTexture is available only after the
 * TextureView is attached to a window (and {@link #onAttachedToWindow()} has
 * been invoked.) It is therefore highly recommended you use a listener to
 * be notified when the SurfaceTexture becomes available.</p>
 *
 * <p>It is important to note that only one producer can use the TextureView.
 * For instance, if you use a TextureView to display the camera preview, you
 * cannot use {@link #lockCanvas()} to draw onto the TextureView at the same
 * time.</p>
 */

public class CameraTextureView extends TextureView {

  private int mRatioWidth = 0;
  private int mRatioHeight = 0;

  /**
   * Creates a new TextureView.
   *
   * @param context The context to associate this view with.
   */
  public CameraTextureView(Context context) {
    this(context, null);
  }

  /**
   * Creates a new TextureView.
   *
   * @param context The context to associate this view with.
   * @param attrs The attributes of the XML tag that is inflating the view.
   */
  public CameraTextureView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * Creates a new TextureView.
   *
   * @param context The context to associate this view with.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyle An attribute in the current theme that contains a
   * reference to a style resource that supplies default values for
   * the view. Can be 0 to not look for defaults.
   */
  public CameraTextureView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  /**
   * Sets the aspect ratio for this view. The size of the view will be measured
   * based on the ratio calculated from the parameters. Note that the actual
   * sizes of parameters don't matter, that is, calling setAspectRatio(2, 3) and
   * setAspectRatio(4, 6) make the same result.
   *
   * @param width Relative horizontal size
   * @param height Relative vertical size
   */
  public void setAspectRatio(int width, int height) {
    if (width < 0 || height < 0) {
      throw new IllegalArgumentException("Size cannot be negative.");
    }
    mRatioWidth = width;
    mRatioHeight = height;
    requestLayout();
  }

  /**
   * <p>
   * Measure the view and its content to determine the measured width and the
   * measured height. This method is invoked by {@link #measure(int, int)} and
   * should be overridden by subclasses to provide accurate and efficient
   * measurement of their contents.
   * </p>
   *
   * <p>
   * <strong>CONTRACT:</strong> When overriding this method, you
   * <em>must</em> call {@link #setMeasuredDimension(int, int)} to store the
   * measured width and height of this view. Failure to do so will trigger an
   * <code>IllegalStateException</code>, thrown by
   * {@link #measure(int, int)}. Calling the superclass'
   * {@link #onMeasure(int, int)} is a valid use.
   * </p>
   *
   * <p>
   * The base class implementation of measure defaults to the background size,
   * unless a larger size is allowed by the MeasureSpec. Subclasses should
   * override {@link #onMeasure(int, int)} to provide better measurements of
   * their content.
   * </p>
   *
   * <p>
   * If this method is overridden, it is the subclass's responsibility to make
   * sure the measured height and width are at least the view's minimum height
   * and width ({@link #getSuggestedMinimumHeight()} and
   * {@link #getSuggestedMinimumWidth()}).
   * </p>
   *
   * @param widthMeasureSpec horizontal space requirements as imposed by the parent.
   * The requirements are encoded with
   * {@link android.view.View.MeasureSpec}.
   * @param heightMeasureSpec vertical space requirements as imposed by the parent.
   * The requirements are encoded with
   * {@link android.view.View.MeasureSpec}.
   */
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    int width = MeasureSpec.getSize(widthMeasureSpec);
    int height = MeasureSpec.getSize(heightMeasureSpec);
    if (0 == mRatioWidth || 0 == mRatioHeight) {
      setMeasuredDimension(width, height);
    } else {
      if (width < height * mRatioWidth / mRatioHeight) {
        setMeasuredDimension(width, width * mRatioHeight / mRatioWidth);
      } else {
        setMeasuredDimension(height * mRatioWidth / mRatioHeight, height);
      }
    }
  }
}
