package ai.maum.m2u.cdk.grpclib.chatbot.grpc;

import ai.maum.m2u.cdk.grpclib.chatbot.common.PreferenceAgent;
import ai.maum.m2u.cdk.grpclib.chatbot.map.MapEventFactory;
import ai.maum.m2u.cdk.grpclib.chatbot.map.MapOperationName;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst.GrpcMetadataKeys;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst.MapKeys;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO.MapEventInfo;
import ai.maum.m2u.cdk.grpclib.ui.updater.ExecuteUpdater;
import ai.maum.m2u.cdk.grpclib.utils.GsonUtil;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import ai.maum.m2u.cdk.grpclib.utils.MapEventDirectiveParser;
import ai.maum.m2u.cdk.grpclib.utils.StringUtil;
import android.content.Context;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.okhttp.OkHttpChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.Map.PingRequest;
import maum.m2u.map.Map.PongResponse;
import maum.m2u.map.MaumToYouProxyServiceGrpc;
import maum.m2u.map.MaumToYouProxyServiceGrpc.MaumToYouProxyServiceStub;

/**
 * MAP 과의 통신을 담당하고 있는 클래스 입니다. channel 초기화, stub 구성, MAP 으로 MapEvent 및 ping 전송, MAP 으로부터 받은
 * MapDirective 를 UI 에 전송하는 기능을 합니다.
 */

public class GrpcAgent {

  private static final String TAG = GrpcAgent.class.getSimpleName();

  // 주기적으로 MAP에 Ping을 전송하기 위한 TimerTask
  public static final TimerTaskForPing mTimerTaskForPing;
  private Context mContext;
  private IGrpcMetadataGetter iGcrpMetadataGetter;
  // MAP 과 통신하기 위한 Client Stub
  private MaumToYouProxyServiceStub maumToYouProxyServiceStub;
  // Channel 클래스의 객체
  private Channel mapFinalChannel;
  // IP, PORT 를 빌드해서 생성되는 Channel 을 상속받은 클래스의 객체
  private ManagedChannel mapChannel;
  private String connectUrl;
  private int connectPort;
  // 스트림 메시지를 MAP 에게 전송, MAP으로부터 수신을 하는 Stub 을 사용하는 클래스 변수
  private StreamObserver<MapEvent> mapEventStreamObserver;
  private Timer mTimer;
  private long currentPingIntervalMilli = 0;
  // gRPC로 수신받은 Metadata 정보를 저장하는 멤버변수
  private HashMap<String, String> mMetadataMap = new HashMap<>();

  /**
   * 클래스 생성자 입니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @param mg 메타 데이터를 받을 수 있는 인터페이스
   */
  public GrpcAgent(Context context, IGrpcMetadataGetter mg) {
    mContext = context;
    iGcrpMetadataGetter = mg;
    init();
  }

  /**
   * Preference 에서 ip, port 값을 로드하여 변수에 할당하는 함수입니다.
   */
  public void init() {
    connectUrl = PreferenceAgent
        .getStringSharedData(mContext, BaseConst.MapKeys.SERVER_IP);
    connectPort = PreferenceAgent
        .getIntSharedData(mContext, BaseConst.MapKeys.SERVER_PORT);
    LogUtil.e(TAG, "grpc init : " + connectUrl + " / " + connectPort);
  }

  /**
   * gRPC 통신을 위해 초기화 하는 함수입니다. 언어 설정이 없는 경우 기본값인 kor 을 사용합니다.
   */
  public boolean open() {
    return open(ChatLanguage.kor);
  }

  /**
   * gRPC 통신을 위해 channel, stub 을 생성하는 함수 입니다. stub 에 authToken metadata 도 추가됩니다.
   *
   * @param lang 설정된 언어
   * @return 성공한 경우 true
   */
  public boolean open(final ChatLanguage lang) {
    boolean isReturn = true;
    if (isGrpcOpen()) {
      return isReturn;
    }
    GrpcHeaderClientInterceptor headerClientInterceptor =
        new GrpcHeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(iGcrpMetadataGetter);

    if (StringUtil.isEmptyString(connectUrl)) {
      init();
    }

    try {
      LogUtil.d(TAG, "grpc open : " + connectUrl + " / " + connectPort);

      boolean useTls = PreferenceAgent.getBooleanSharedData(mContext, MapKeys.USE_TLS);

      // tls를 사용하는 경우 sslSocketFactory를 사용한다
      if (useTls) {
        mapChannel = OkHttpChannelBuilder.forAddress(connectUrl, connectPort).sslSocketFactory(null)
            .build();
      } else {
        mapChannel = OkHttpChannelBuilder.forAddress(connectUrl, connectPort).usePlaintext(true)
            .build();
      }

      mapFinalChannel = ClientInterceptors
          .intercept(mapChannel, headerClientInterceptor);
      maumToYouProxyServiceStub = MaumToYouProxyServiceGrpc.newStub(mapFinalChannel);

      startPingProcess();
    } catch (final NullPointerException e) {
      LogUtil.e(TAG, e.getMessage());
      if (PreferenceAgent.getBooleanSharedData(mContext, MapKeys
          .IS_ERROR_TOAST)) {
        runUiUpdater(HandlerConst.UIUpdater.UI_FROM_MAP_ERROR,
            e.getMessage(), null, 0, false,
            null, null, null);
      }
      isReturn = false;
    }
    return isReturn;
  }

  /**
   * 현재 gRPC 가 연결된 상태인지 확인하는 함수이다.
   *
   * @return grpc open된 경우 true
   */
  public boolean isGrpcOpen() {
    boolean isOpened = false;
    if (mapChannel != null && maumToYouProxyServiceStub != null) {
      isOpened = true;
    }
    return isOpened;
  }

  /**
   * MAP 에게 주기적으로 ping 을 요청을 시작하는 함수입니다. JS 로부터 전달받은 Setting 데이터 중에 pingInterval 값이 유효한 경우에만
   * pingInterval 시간 주기로 Ping 을 요청합니다.
   */
  public void startPingProcess() {
    int pingInterval = PreferenceAgent.getIntSharedData(mContext,
        BaseConst.MapKeys.PING_INTERVAL);
    if (pingInterval > 0) {
      long pingIntervalMilli = pingInterval * 1000;
      if (pingIntervalMilli == getCurrentPingIntervalMilli()) {
        // ping 주기가 같고 현재 동작되고 있는 TimerTask 가 있으면 return;
        if (mTimerTaskForPing != null) {
          return;
        }
      }
      setCurrentPingIntervalMilli(pingIntervalMilli);
      // ping 주기가 같고 TimerTask 가 없으면 TimerTask 구동
      // ping 주기가 같지 않고 TimerTask 가 있던지, 없던지 TimerTask 구동
      stopPingProcess();
      mTimerTaskForPing = new TimerTaskForPing();
      mTimer = new Timer();
      mTimer.schedule(mTimerTaskForPing, pingIntervalMilli, pingIntervalMilli);
    } else {
      stopPingProcess();
    }
    LogUtil.e(TAG, "startPingProcess...");
  }

  private long getCurrentPingIntervalMilli() {
    return currentPingIntervalMilli;
  }

  private void setCurrentPingIntervalMilli(long currentPingIntervalMilli) {
    this.currentPingIntervalMilli = currentPingIntervalMilli;
  }

  /**
   * Ping timertask 가 동작하고 있는 경우 Ping 요청을 종료시키는 함수입니다.
   */
  public void stopPingProcess() {
    if (mTimer != null) {
      mTimer.cancel();
      mTimer = null;
    }
    if (mTimerTaskForPing != null) {
      mTimerTaskForPing.cancel();
      mTimerTaskForPing = null;
    }
    LogUtil.e(TAG, "stopPingProcess...");
  }

  /**
   * MAP 에 MapEvent 을 전송한 후 MAP 으로부터 응답을 받는 함수입니다. 응답으로 받은 MapDirective 데이터를 JS 로 전달하기 위해 UI 로
   * 전달합니다. 정상 수신, 완료, 에러에 대한 MapDirective 데이터를 UI 로 전달합니다.
   */
  private void getEventStream() {
    mapEventStreamObserver = maumToYouProxyServiceStub
        .eventStream(new StreamObserver<MapDirective>() {
          @Override
          public void onNext(MapDirective value) {
            DirectiveStream directiveStream = value.getDirective();
            runUiUpdater(HandlerConst.UIUpdater.UI_FROM_MAP_ONNEXT,
                null, null, 0, false,
                null, value, null);
          }

          @Override
          public void onCompleted() {
            LogUtil.e(TAG, "#@ Event Stream Completed");
            mapEventStreamObserver = null;
            runUiUpdater(HandlerConst.UIUpdater.UI_FROM_MAP_ONCOMPLETED,
                "Event Stream Completed", null, 0, false,
                null, null, null);
          }

          @Override
          public void onError(final Throwable t) {
            LogUtil.e(TAG, "#@ Event Stream Error : " + t.toString());
            stopPingProcess();
            mapEventStreamObserver = null;
            runUiUpdater(HandlerConst.UIUpdater.UI_FROM_MAP_ONERROR,
                t.toString(), null, 0, false,
                null, null, null);
          }
        });
  }

  /**
   * 주기적으로 Ping 을 요청하는 함수입니다. Deivce 객체를 포함한 PingRequest 객체를 생성하여 MAP 에 전송합니다. MAP에서는 Ping 이 오지 않은
   * 디바이스는 Active 하지 않은 것으로 판단하고 관련 DialogAgent들을 모두 정리합니다. MAP 으로부터 받은 수신 데이터 또한 JS 로 전달하기 위해 UI 로
   * 전달합니다. 정상 수신에 대한 MapDirective 데이터를 UI 로 전달합니다.
   */
  private void requestPing() {
    // MaumToYouProxyServiceStub
    int pingInterval = PreferenceAgent.getIntSharedData(mContext,
        BaseConst.MapKeys.PING_INTERVAL);
    if (pingInterval <= 0) {
      stopPingProcess();
      return;
    }
    PingRequest pingRequest = MapEventFactory.getPingRequest(mContext);
    LogUtil.e(TAG, "[REQUEST PING]\n" + pingRequest);
    maumToYouProxyServiceStub.ping(pingRequest,
        new StreamObserver<PongResponse>() {
          @Override
          public void onNext(PongResponse value) {
            GsonUtil.gsonLog(value);
            try {
              String pongJson = JsonFormat.printer().print(value);
              runUiUpdater(HandlerConst.UIUpdater.UI_FROM_MAP_PONG, pongJson,
                  null, 0, false, null, value, null);
            } catch (InvalidProtocolBufferException e) {
              e.printStackTrace();
              LogUtil.e(TAG, e.getMessage());
            }
          }

          @Override
          public void onError(Throwable t) {
            LogUtil.e(TAG, "PongResponse Error : " + t.toString());
            runUiUpdater(HandlerConst.UIUpdater.UI_FROM_MAP_ONERROR,
                t.toString(), null, 0, false, null,
                null, null);
          }

          @Override
          public void onCompleted() {
            LogUtil.e(TAG, "PongResponse End");
          }
        });
  }

  /**
   * JS 로 부터 받은 eventStream 데이터를 Device 정보를 추가하여 MapEvent를 생성하고 MAP 에 전송하는 함수입니다.
   *
   * @param eventStreamJson JS 로 부터 받은 MAP 에 전송될 eventStream 데이터
   */
  public boolean requestGrpc(String eventStreamJson) {
    boolean sendOnComplete = false;

    // #@ SignIn, MultiFactorVerify 인 경우 m2u-sign-in header 를 attach
    // 그 외 OperationName 인 경우 authToken attach.
    final MapEventInfo mapEventInfo = MapEventDirectiveParser
        .getMapEventInfo(eventStreamJson);
    // 현재 요청받은 함수가 signIn 또는 MultiFactorVerify 인지 확인
    boolean isSignInMultiFactorVerify = false;

    if (mapEventInfo.getOperationName().equalsIgnoreCase(MapOperationName.OPERATION_SIGNIN)
        || mapEventInfo.getOperationName()
        .equalsIgnoreCase(MapOperationName.OPERATION_MULTIFACTORVERIFY)) {
      isSignInMultiFactorVerify = true;
    }

    // location 정보가 있는 경우 eventStreamJson의 contexts 정보를 모두 제거하고 셋팅
    if (mapEventInfo.isHasLocation()) {
      eventStreamJson = MapEventDirectiveParser.getContextsJsonRemoved(eventStreamJson);
    }

    attachHeader(isSignInMultiFactorVerify);
    getEventStream();
    startPingProcess();

    MapEvent mapEvent = null;
    try {
      EventStream.Builder eventStreamBuilder = EventStream.newBuilder();
      JsonFormat.parser().merge(eventStreamJson, eventStreamBuilder);

      EventStream eventStream = null;
      if (!mapEventInfo.isHasLocation()) {
        // 기본적으로 context에 device 정보를 추가
        eventStream = eventStreamBuilder
            .addContexts(0, MapEventFactory.getDevice(mContext))
            .build();
      } else {
        // location 정보가 있는 경우 추가 적으로 contexts에 location 정보를 추가
        eventStream = eventStreamBuilder
            .addContexts(0, MapEventFactory.getDevice(mContext))
            .addContexts(1, MapEventFactory.getLocationMeta(mapEventInfo))
            .build();
      }

      MapEvent.Builder mapEventBuilder = MapEvent.newBuilder();
      mapEvent = mapEventBuilder.setEvent(eventStream).build();

      boolean isStreaming = eventStream.getInterface().getStreaming();

      // getStreaming이 false 이면 onCompleted(더이상 보낼 stream event가 없음)를 명시적으로 호출 합니다
      LogUtil.d(TAG, "#@ isStreaming :" + isStreaming);
      if (!isStreaming) {
        sendOnComplete = true;
      }
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
      runUiUpdater(HandlerConst.UIUpdater.UI_FROM_MAP_ERROR, e.getMessage(),
          null, 0, false,
          null, null, null);
      return false;
    }

    LogUtil.e(TAG, "[requestGrpc] : mapEventJson = \n" + mapEvent);

    if (mapEvent != null) {
      try {
        mapEventStreamObserver.onNext(mapEvent);
        if (sendOnComplete) {
          mapEventStreamObserver.onCompleted();
          LogUtil.d(TAG, "#@ Call requestGrpc onCompleted requestGrpc");
        }
      } catch (Exception e) {
        LogUtil.e(TAG, e.getMessage());
      }
    }

    return true;
  }

  /**
   * Microphone 을 통해 발화한 byteStream 데이터를 MAP 에 전송하는 함수입니다.
   *
   * @param voice Microphone 을 통해 발화한 byte[] 값
   */
  public void requestVoiceResult(byte[] voice) {
    if (mapEventStreamObserver == null) {
      getEventStream();
    }
    try {
      mapEventStreamObserver.onNext(MapEventFactory.getByteParam(voice));
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
    }
  }

  /**
   * Camera, Gallery 를 통해 취득한 Iamge 의 byteStream 데이터를 MAP 에 전송하는 함수입니다.
   *
   * @param iamge Image 에 대한  byte[] 값
   */
  public void requestImageResult(byte[] iamge) {
    if (mapEventStreamObserver == null) {
      getEventStream();
    }
    try {
      mapEventStreamObserver.onNext(MapEventFactory.getByteParam(iamge));
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
    }
  }

  /**
   * byteStream 을 MAP 에 모두 전송한 후에 streamEnd 를 MAP 에 호출하는 함수입니다.
   *
   * @param streamId MAP 에 전송한 byteStream 에 대한 streamId 값
   */
  public void requestStreamEnd(String streamId) {
    if (mapEventStreamObserver == null) {
      getEventStream();
    }
    LogUtil.e(TAG, "requestStreamEnd run : " + streamId);
    try {
      mapEventStreamObserver.onNext(MapEventFactory.getStreamEndParam(streamId));
      mapEventStreamObserver.onCompleted();
      LogUtil.d(TAG, "#@ Call requestGrpc onCompleted requestStreamEnd");
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
    }
  }

  /**
   * EventStream 을 MAP 서버에 모두 전송한 후 onCompleted 함수를 호출하는 함수입니다.
   */
  private void requestOnCompleted() {
    try {
      if (mapEventStreamObserver != null) {
        mapEventStreamObserver.onCompleted();
        LogUtil.d(TAG, "#@ Call requestGrpc onCompleted requestOnCompleted");
      }
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
    }
  }

  /**
   * MAP 과의 통신을 재연결하기 위해 닫은 후 초기화하고 다시 오픈하는 함수입니다.
   *
   * @return 재오픈 성공시 true
   */
  public boolean reOpen() {
    close();
    init();
    return open();
  }

  /**
   * gRPC 의 세션을 shutdown 시키는 함수입니다.
   */
  public void shutdown() throws InterruptedException {
    if (mapChannel != null) {
      mapChannel.shutdown().awaitTermination(1, TimeUnit.SECONDS);
      mapChannel = null;
    }
    if (mapEventStreamObserver != null) {
      mapEventStreamObserver.onCompleted();
      LogUtil.d(TAG, "#@ Call requestGrpc onCompleted shutdown");
      mapEventStreamObserver = null;
    }
    stopPingProcess();
  }

  /**
   * gRPC 통신에 대한 연결을 종료시키는 함수입니다. 내부적으로 gRPC 를 suhtdown 시킵니다.
   */
  public void close() {
    LogUtil.d(TAG, "======== MAP(GrpcAgent) close called ========");
    try {
      shutdown();
    } catch (InterruptedException e) {
      LogUtil.e(TAG, e.getMessage());
    }
  }

  /**
   * UI 에게 값을 전달하기 위해 Observer pattern을 통해서 값을 전달하는 함수입니다.
   */
  private void runUiUpdater(int updateCase,
      String arg1, String arg2, int arg3, boolean arg4,
      byte[] arg5, Object obj1, Object obj2) {
    ExecuteUpdater updater = ExecuteUpdater.getInstance();
    updater.updateRun(updateCase, arg1, arg2, arg3, arg4, arg5, obj1, obj2);
  }

  // Language enum type
  public enum ChatLanguage {
    kor,
    eng
  }

  /**
   * operation에 따라 Header 추가를 설정하는 함수
   */
  private void attachHeader(boolean isSignInMultiFactorVerify) {
    LogUtil.d(TAG, "#@ attachHeader isSignInMultiFactorVerify :" + isSignInMultiFactorVerify);
    // MetaData에서 조회할때 필요한 변수 설정
    String authSignInMetadataKey = GrpcMetadataKeys.M2U_AUTH_SIGN_IN;
    String authTokenMetadataKey = GrpcMetadataKeys.M2U_AUTH_TOKEN;

    // AuthToken 값 조회
    String authToken = PreferenceAgent.getStringSharedData(mContext, MapKeys.AUTH_TOKEN);
    LogUtil.d(TAG, "#@ attachHeader authToken :" + authToken);

    // SignIn 또는 MultiFactorVerify 인 경우
    if (isSignInMultiFactorVerify) {
      // 이미 m2u-auth-token 값이 설정되었는지 확인 변수
      boolean isExistAuthToken = mMetadataMap.containsKey(authTokenMetadataKey);
      LogUtil.d(TAG, "#@ SignIn Process isExistAuthToken :" + isExistAuthToken);

      if (isExistAuthToken) {
        // 이미 authToken 이 있으면 header 초기화후 재설정
        LogUtil.d(TAG, "#@ SignIn Process resetClientInterceptor1");
        resetClientInterceptor();

        // Header에 m2u-auth-sign-in 설정
        Metadata fixedHeaders = new Metadata();
        Metadata.Key<String> key = Metadata.Key
            .of(authSignInMetadataKey, Metadata.ASCII_STRING_MARSHALLER);
        fixedHeaders.put(key, "");

        maumToYouProxyServiceStub = MetadataUtils
            .attachHeaders(maumToYouProxyServiceStub, fixedHeaders);
        LogUtil.d(TAG, "#@ 1.SignIn Process authSignInMetadataKey");
      } else {
        // 이미 m2u-auth-sign-in 값이 설정되었는지 확인 변수
        boolean isExist = mMetadataMap.containsKey(authSignInMetadataKey);
        LogUtil.d(TAG, "#@ SignIn Process isExist :" + isExist);

        if (isExist) {
          // header 에 signin 이미 있는 경우엔 초기화 이후 attach 한다
          LogUtil.d(TAG, "#@ SignIn Process resetClientInterceptor2");
          resetClientInterceptor();
        }
        // SignIn에는 m2u-auth-sign-in가 필수
        Metadata fixedHeaders = new Metadata();
        Metadata.Key<String> key = Metadata.Key
            .of(authSignInMetadataKey, Metadata.ASCII_STRING_MARSHALLER);
        fixedHeaders.put(key, "");

        maumToYouProxyServiceStub = MetadataUtils
            .attachHeaders(maumToYouProxyServiceStub, fixedHeaders);
        LogUtil.d(TAG, "#@ 2.SignIn Process authSignInMetadataKey");
      }
      LogUtil.e(TAG, "#@ SignIn Process");
    } else {  // SignIn 이 아닌 경우
      // 이미 m2u-auth-token 값이 설정되었는지 확인 변수
      boolean isExist = mMetadataMap.containsKey(authTokenMetadataKey);
      LogUtil.d(TAG, "#@ Another Process isExist :" + isExist);
      // header 에 authToken 이 attach 되어 있는 경우.
      if (isExist) {
        String value = mMetadataMap.get(authTokenMetadataKey);
        LogUtil.e(TAG, "#@ Another Process : currentAuthToken = " + value);

        // header의 authToken값과 preference 의 authToken 값이 다르므로 header 를 변경한다.
        if (!authToken.equalsIgnoreCase(value)) {
          // header 초기화후 재설정
          LogUtil.d(TAG, "#@ Another Process resetClientInterceptor");
          resetClientInterceptor();

          Metadata fixedHeaders = new Metadata();
          Metadata.Key<String> key = Metadata.Key
              .of(authTokenMetadataKey, Metadata.ASCII_STRING_MARSHALLER);
          fixedHeaders.put(key, authToken);

          maumToYouProxyServiceStub = MetadataUtils.attachHeaders
              (maumToYouProxyServiceStub, fixedHeaders);
          LogUtil.d(TAG, "#@ 1.Another Process authTokenMetadataKey");
        }
      } else {  // header 에 authToken 이 attach 되어 있지 않은 경우.
        Metadata fixedHeaders = new Metadata();
        Metadata.Key<String> key = Metadata.Key.of(authTokenMetadataKey, Metadata
            .ASCII_STRING_MARSHALLER);
        fixedHeaders.put(key, authToken);

        maumToYouProxyServiceStub = MetadataUtils.attachHeaders
            (maumToYouProxyServiceStub, fixedHeaders);
        LogUtil.d(TAG, "#@ 2.Another Process authTokenMetadataKey");
      }
      LogUtil.e(TAG, "#@ Another Process");
    }
  }

  /**
   * interceptCall 을 통해 Metadata 값을 전달받는다.
   *
   * @param metadata interceptCall metadata
   */
  public void getCurrentMetaData(Metadata metadata) {
    // HashMap 초기화
    mMetadataMap.clear();

    for (String key : metadata.keys()) {
      // Metadata 값을 추출하여 HashMap에 저장
      String value = metadata.get(Metadata.Key.of(key, Metadata.ASCII_STRING_MARSHALLER));
      mMetadataMap.put(key, value);
      LogUtil.e(TAG, "#@ MetaData : key = " + key + ", value = " + value);
    }
  }

  /**
   * Header 정보가 중첩되는 것을 막기 위해 maumToYouProxyServiceStub 객체를 초기화 하는 함수
   */
  private void resetClientInterceptor() {
    LogUtil.d(TAG, "#@ START resetClientInterceptor");
    GrpcHeaderClientInterceptor headerClientInterceptor = new GrpcHeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(iGcrpMetadataGetter);
    // mapChannel 정보 설정
    mapFinalChannel = ClientInterceptors.intercept(mapChannel, headerClientInterceptor);

    LogUtil.d(TAG, "#@ mMetadataMap.clear()");
    mMetadataMap.clear();
    // maumToYouProxyServiceStub 객체를 초기화 (Header 정보 재설정)
    maumToYouProxyServiceStub = MaumToYouProxyServiceGrpc.newStub(mapFinalChannel);
  }

  /**
   * 주기적으로 MAP 에게 Ping 을 요청하기 위한 TimerTask 클래스 입니다.
   */
  public class TimerTaskForPing extends TimerTask {

    public TimerTaskForPing() {
    }

    @Override
    public void run() {
      LogUtil.e(TAG, "PING TIMER...");
      requestPing();
    }
  }
}
