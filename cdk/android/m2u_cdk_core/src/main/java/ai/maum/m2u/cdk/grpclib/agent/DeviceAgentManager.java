package ai.maum.m2u.cdk.grpclib.agent;

import android.content.Context;
import android.os.AsyncTask;

/**
 * microphone, speaker 에 대한 초기화, 해제, byteStream 전달을 하는 함수를 호출하는 함수들로 구성된
 * 클래스 입니다.
 * <br>
 * 이 클래스를 통해서 microphone, speaker 에 접근할 수 있습니다.
 */

public class DeviceAgentManager {

  private static final String TAG = DeviceAgentManager.class.getSimpleName();
  private static Context mContext;
  public static final boolean mIsReturn = false;
  private VoiceRecorderAgent voiceRecorderAgent;
  private SoundPlayAgent soundPlayAgent;

  /**
   * 클래스 생성자 입니다.
   */
  private DeviceAgentManager() {
  }

  /**
   * microphone, speaker 객체에 접근하기 위해 Singletone instance 를 리턴 해주는 함수입니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @return Singletone instance
   */
  public static DeviceAgentManager getInstance(Context context) {
    mContext = context;
    return SingletonHolder.INSTANCE;
  }

  /**
   * microphone 을 초기화 하는 함수를 호출합니다.
   *
   * @param sampleRate sampleRate : 16000
   * @param encoding encoding : LINEAR16
   * @param model model : baseline
   * @return microphone 이 정상적으로 초기화 된 경우 true
   */
  public boolean callCreateVoiceRecorderInit(int sampleRate, String encoding,
      String model) {
    boolean isResult = true;
    try {
      voiceRecorderAgent = new VoiceRecorderAgent(mContext, sampleRate,
          encoding, model);
    } catch (Exception e) {
      isResult = false;
    }
    return isResult;
  }

  /**
   * microphone 으로 발화 출력을 하는 함수를 호출하는 함수입니다.
   *
   * @param streamId SpeechToText, SpeechToSpeech 전달시의 streamId
   * @param operationSyncId SpeechToText, SpeechToSpeech 전달시의 operationSyncId
   */
  public void callStartVoiceRecorderAgent(String streamId,
      String operationSyncId) {
    if (voiceRecorderAgent != null) {
      voiceRecorderAgent.setStreamOperationIds(streamId, operationSyncId);
      voiceRecorderAgent.execute();
    }
  }

  /**
   * microphone 을 해제 시키는 함수를 호출하는 함수입니다.
   *
   * @return 정상적으로 해제된 경우 true
   */
  public boolean callStopVoiceRecorderAgent() {
    boolean isResult = false;
    if (isExistVoiceRecorderAgent()) {
      if (voiceRecorderAgent.getStatus() == AsyncTask.Status.RUNNING) {
        voiceRecorderAgent.cancel(true);
        voiceRecorderAgent.onPostExecute(null);
        voiceRecorderAgent = null;
        isResult = true;
      }
    }
    return isResult;
  }

  /**
   * microphone 관련 제어를 담당하는 클래스의 객체가 생성되었는지 확인하는 함수입니다.
   *
   * @return 클래스의 객체가 생성된 경우 true
   */
  public boolean isExistVoiceRecorderAgent() {
    return voiceRecorderAgent != null ? true : false;
  }

  /**
   * microphone 의 객체가 생성되었는지 확인하는 함수입니다.
   *
   * @return microphone 의 객체가 생성된 경우 true
   */
  public boolean isAudioRecorderObject() {
    if (isExistVoiceRecorderAgent()) {
      return voiceRecorderAgent.isAudioRecordExist();
    } else {
      return false;
    }
  }

  /**
   * 스피커로 PCM 을 출력하기 위해 오디오트랙을 준비한다.
   */
  /**
   * speaker 를 초기화 시켜주는 함수를 호출하는 함수입니다.
   *
   * @param sampleRate sampleRate : 16000
   * @param encoding encoding : LINEAR16 또는 MULAW
   */
  public void callCreateAudioPlayAgent(int sampleRate, String encoding) {
    SoundPlayAgent.getInstance(mContext).init(sampleRate, encoding);
  }

  /**
   * speaker 로 출력 될 byteStream 데이터를 전달해 주는 함수를 호출하는 함수입니다.
   *
   * @param data speaker 로 출력 될 byteStream 데이터
   */
  public void callPlayAudioPlayAgent(byte[] data) {
    SoundPlayAgent.getInstance(mContext).addPlayData(data);
  }

  /**
   * speaker 객체를 해제하는 함수를 호출하는 함수입니다.
   */
  public void callReleaseAudioPlayAgent() {
    SoundPlayAgent.getInstance(mContext).release();
  }

  /**
   * speaker 객체가 생성되었는지 확인하는 함수입니다.
   *
   * @return speaker 객체가 생성된 경우 true
   */
  public boolean isExistAudioPlayAgent() {
    return SoundPlayAgent.getInstance(mContext).isExistAudioTrackInstance();
  }

  /**
   * Singletone instance 를 생성하는 클래스 입니다.
   */
  private static final class SingletonHolder {

    static final DeviceAgentManager INSTANCE = new DeviceAgentManager();
  }
}
