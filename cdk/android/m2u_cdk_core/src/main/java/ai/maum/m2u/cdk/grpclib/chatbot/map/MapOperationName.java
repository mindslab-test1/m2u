package ai.maum.m2u.cdk.grpclib.chatbot.map;

/**
 * MAP 의 Event, Directive 에서 사용되는 Operation Name 을 정의한 Constant 클래스 입니다.
 */

public class MapOperationName {

  public static final String OPERATION_OPEN = "Open";
  public static final String OPERATION_CLOSE = "Close";
  public static final String OPERATION_SPEECHTOTEXTTALK = "SpeechToTextTalk";
  public static final String OPERATION_TEXTTOSPEECHTALK = "TextToSpeechTalk";
  public static final String OPERATION_TEXTTOTEXTTALK = "TextToTextTalk";
  public static final String OPERATION_IMAGETOSPEECHTALK = "ImageToSpeechTalk";
  public static final String OPERATION_IMAGETOTEXTTALK = "ImageToTextTalk";
  public static final String OPERATION_RECOGNIZING = "Recognizing";
  public static final String OPERATION_RECOGNIZED = "Recognized";
  public static final String OPERATION_ENDPOINTDETECTED = "EndpointDetected";
  public static final String OPERATION_PROCESSING = "Processing";
  public static final String OPERATION_RENDERTEXT = "RenderText";
  public static final String OPERATION_RENDERCARD = "RenderCard";
  public static final String OPERATION_RENDERHIDDEN = "RenderHidden";
  public static final String OPERATION_EXPECTSPEECH = "ExpectSpeech";
  public static final String OPERATION_EXPECTSPEECHSTOP = "ExpectSpeechStop";
  public static final String OPERATION_PLAY = "Play";
  public static final String OPERATION_PLAYFINISHED = "PlayFinished";
  public static final String OPERATION_PLAYPAUSED = "PlayPaused";
  public static final String OPERATION_PLAYRESUMED = "PlayResumed";
  public static final String OPERATION_PLAYSTARTED = "PlayStarted";
  public static final String OPERATION_PLAYSTOPPED = "PlayStopped";
  public static final String OPERATION_STREAMDELIVER = "StreamDeliver";
  public static final String OPERATION_STREAMREQUESTED = "StreamRequested";
  public static final String OPERATION_PROGRESSREPORTDELAYPASSED =
      "ProgressReportDelayPassed";
  public static final String OPERATION_PROGRESSREPORTINTERVALPASSED =
      "ProgressReportIntervalPassed";
  public static final String OPERATION_PROGRESSREPORTPOSITIONPASSED =
      "ProgressReportPositionPassed";
  public static final String OPERATION_EXPECTSPEECHTIMEDOUT =
      "ExpectSpeechTimedOut";
  public static final String OPERATION_SPEECHTOSPEECHTALK =
      "SpeechToSpeechTalk";
  public static final String OPERATION_SETEXPRESSION = "SetExpression";
  public static final String OPERATION_SIGNIN = "SignIn";
  public static final String OPERATION_MULTIFACTORVERIFY = "MultiFactorVerify";
}