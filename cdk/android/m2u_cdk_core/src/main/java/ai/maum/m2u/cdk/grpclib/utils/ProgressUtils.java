package ai.maum.m2u.cdk.grpclib.utils;

import ai.maum.m2u.cdk.grpclib.R;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.ProgressBar;

/**
 * Progress bar 를 보여줄 때 사용되는 클래스 입니다.
 */

public class ProgressUtils {

  Context context;
  private ProgressDialog progress_dialog;

  /**
   * 클래스 생성자 입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   */
  public ProgressUtils(Context context) {
    this.context = context;
  }

  /**
   * Progress bar 의 다이얼로그를 생성하는 함수입니다.
   *
   * @param mContext 함수를 호출한 클래스의 context
   * @return 생성된 Progress bar
   */
  public static ProgressDialog createProgressDialog(Context mContext) {
    ProgressDialog dialog = new ProgressDialog(mContext);
    dialog.setCancelable(false);
    dialog.getWindow()
        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.setContentView(R.layout.progressdialog);
    // dialog.setMessage(Message);
    return dialog;
  }

  /**
   * Progress bar 를 보여주는 함수입니다.
   *
   * @param isCancelable 취소 기능 추가할 경우 true
   */
  public void show_dialog(boolean isCancelable) {
    progress_dialog = ProgressDialog
        .show(context, null, null, true, isCancelable);
    progress_dialog.getWindow()
        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    progress_dialog.setContentView(R.layout.progressdialog);
    ProgressBar progressbar = (ProgressBar) progress_dialog
        .findViewById(R.id.progressBar1);
    progress_dialog.setCancelable(isCancelable);
    progressbar.getIndeterminateDrawable()
        .setColorFilter(Color.parseColor("#3F51B5"),
            android.graphics.PorterDuff.Mode.SRC_IN);
  }

  /**
   * Progress bar 를 종료시키는 함수입니다.
   */
  public void dismiss_dialog() {
    if (progress_dialog != null && progress_dialog.isShowing()) {
      progress_dialog.dismiss();
    }
  }
}
