package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import com.google.gson.JsonObject;

/**
 * MAP 과의 통신을 위한 설정 정보를 JS 로 부터 전달받기 위해 사용되는 데이터 전송 객체에 대한 클래스입니다.
 * <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */

public class MapSettingsDTO {

  // 기본 단말 정보에 대한 객체입니다.
  public static final Device device = new Device();
  // 서버 주소 정보입니다.
  private String serverIp = Const.EMPTY_STRING;
  // 서버 포트 정보입니다.
  private int serverPort = 0;
  // TLS 사용 여부에 대한 정보입니다.
  private boolean useTls = false;
  // LogUtil 사용 여부에 대한 정보입니다.
  private boolean useLog = true;

  /**
   * notify를 호출할 때 사용할 prefix
   * 만일, prefix가 "webviewMapClient" 라면
   * "webviewMapClient.receiveDirective( + string  + )"
   * 형식으로 호출해주시면 됩니다.
   */
  private String notifyObjectPrefix = Const.EMPTY_STRING;
  /**
   * 주기적인 PING 전송할지 여부
   */
  private int pingInterval = 60;
  /**
   * 인증 토큰
   */
  private String authToken = "";

  public MapSettingsDTO() {
  }

  public Device getDevice() {
    return device;
  }

  public void setDevice(Device device) {
    this.device = device;
  }

  public String getServerIp() {
    return serverIp;
  }

  public void setServerIp(String serverIp) {
    this.serverIp = serverIp;
  }

  public int getServerPort() {
    return serverPort;
  }

  public void setServerPort(int serverPort) {
    this.serverPort = serverPort;
  }

  public boolean isUseTls() {
    return useTls;
  }

  public void setUseTls(boolean useTls) {
    this.useTls = useTls;
  }

  public boolean isUseLog() {
    return useLog;
  }

  public void setUseLog(boolean useLog) {
    this.useLog = useLog;
  }

  public int getPingInterval() {
    return pingInterval;
  }

  public void setPingInterval(int pingInterval) {
    this.pingInterval = pingInterval;
  }

  public String getAuthToken() {
    return authToken;
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }

  public String getNotifyObjectPrefix() {
    return notifyObjectPrefix;
  }

  public void setNotifyObjectPrefix(String notifyObjectPrefix) {
    this.notifyObjectPrefix = notifyObjectPrefix;
  }

  /**
   * 기본 단말 정보이며 단말 정보가 갱신되면 계속해서 setMapSetting() 함수를 통해서 JS 로 부터 전달받습니다.
   */
  public static class Device {

    // 장치에 대한 메타 데이터 입니다.
    public static final JsonObject meta = null;
    // Capability 클래스의 객체입니다.
    public static final Device.Capability support = new Device.Capability();
    public static final String id = Const.EMPTY_STRING;
    public static final String type = Const.EMPTY_STRING;
    public static final String version = Const.EMPTY_STRING;
    // 서비스 접근 경로이며 내부적인 업무 구분 용도로 사용될 수 있다.
    public static final String channel = Const.EMPTY_STRING;
    // 기본 접근 챗봇 정보 입니다.
    public static final String preferredChatbot = Const.EMPTY_STRING;
    public static final String timestamp = Const.EMPTY_STRING;

    public Device() {
    }

    public Capability getSupport() {
      return support;
    }

    public void setSupport(Capability support) {
      this.support = support;
    }

    public String getTimestamp() {
      return timestamp;
    }

    public void setTimestamp(String timestamp) {
      this.timestamp = timestamp;
    }


    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getChannel() {
      return channel;
    }

    public void setChannel(String channel) {
      this.channel = channel;
    }

    public String getPreferredChatbot() {
      return preferredChatbot;
    }

    public void setPreferredChatbot(String preferredChatbot) {
      this.preferredChatbot = preferredChatbot;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getVersion() {
      return version;
    }

    public void setVersion(String version) {
      this.version = version;
    }

    public String getMeta() {
      return meta.toString();
    }

    public void setMeta(JsonObject meta) {
      this.meta = meta;
    }

    /**
     * 각 기능들에 대한 지원 여부에 대한 정보를 설정하는 클래스 입니다.
     */
    public static class Capability {

      // OUTPUT: 텍스트를 출력할 수 있다.
      public static final boolean supportRenderText = false;
      // OUTPUT: 카드를 처리할 수 있다.
      public static final boolean supportRenderCard = false;
      // OUTPUT: 음성합성을 처리할 수 있다.
      public static final boolean supportSpeechSynthesizer = false;
      // OUTPUT: 오디오를 출력할 수 있다.
      public static final boolean supportPlayAudio = false;
      // OUTPUT: 비디오를 출력할 수 있다.
      public static final boolean supportPlayVideo = false;
      // OUTPUT: 행위를 처리할 수 있다.
      public static final boolean supportAction = false;
      // OUTPUT: 소스를 움직일 수 있다.
      public static final boolean supportMove = false;
      // INPUT: 음성입력 대기를 처리할 수 있다.
      public static final boolean supportExpectSpeech = false;

      public Capability() {
      }

      public boolean isSupportRenderText() {
        return supportRenderText;
      }

      public void setSupportRenderText(boolean supportRenderText) {
        this.supportRenderText = supportRenderText;
      }

      public boolean isSupportRenderCard() {
        return supportRenderCard;
      }

      public void setSupportRenderCard(boolean supportRenderCard) {
        this.supportRenderCard = supportRenderCard;
      }

      public boolean isSupportSpeechSynthesizer() {
        return supportSpeechSynthesizer;
      }

      public void setSupportSpeechSynthesizer(
          boolean supportSpeechSynthesizer) {
        this.supportSpeechSynthesizer = supportSpeechSynthesizer;
      }

      public boolean isSupportPlayAudio() {
        return supportPlayAudio;
      }

      public void setSupportPlayAudio(boolean supportPlayAudio) {
        this.supportPlayAudio = supportPlayAudio;
      }

      public boolean isSupportPlayVideo() {
        return supportPlayVideo;
      }

      public void setSupportPlayVideo(boolean supportPlayVideo) {
        this.supportPlayVideo = supportPlayVideo;
      }

      public boolean isSupportAction() {
        return supportAction;
      }

      public void setSupportAction(boolean supportAction) {
        this.supportAction = supportAction;
      }

      public boolean isSupportMove() {
        return supportMove;
      }

      public void setSupportMove(boolean supportMove) {
        this.supportMove = supportMove;
      }

      public boolean isSupportExpectSpeech() {
        return supportExpectSpeech;
      }

      public void setSupportExpectSpeech(boolean supportExpectSpeech) {
        this.supportExpectSpeech = supportExpectSpeech;
      }
    }
  }
}
