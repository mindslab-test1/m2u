package ai.maum.m2u.cdk.grpclib.ui.view;

import ai.maum.m2u.cdk.grpclib.R;
import android.content.Context;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 토스트 UI 가 중복되지 않고 한개만 표시되도록 하기 위한 클래스 입니다.
 */

public class ToastView {

  // ToastView Flag 변수
  static ToastView instance;
  // Context
  Context mContext;
  // custom Toast
  Toast customToast;
  // custom Toast에 필요한 TextView
  TextView tv_toast;
  // inflate에 사용할 view
  View view;

  /**
   * 클래스 생성자 입니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   */
  private ToastView(Context context) {
    mContext = context;
    init();
  }

  /**
   * 클래스의 생성자 인스턴스를 리턴하는 함수입니다.
   * Singletone pattern 으로 구성되어 있습니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @return toastView 인스턴스
   */
  public static ToastView getInstance(Context context) {
    if (instance == null) {
      instance = new ToastView(context);
    }
    return instance;
  }

  /**
   * 클래스의 view를 초기화 하는 함수입니다.
   */
  private void init() {
    LayoutInflater mInflater = (LayoutInflater) mContext
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = mInflater.inflate(R.layout.view_toast, null);
    tv_toast = (TextView) view.findViewById(R.id.tv_toast);
  }

  /**
   * ToastView UI 를 보여주는 함수입니다.
   * customToast가 null일 때 Toast를 생성해주고
   * ToastView의 duration을 설정한 후 view를 set 해서 msg를 보여줍니다.
   *
   * @param msg ToastView 에 보여질 메시지
   */
  public void setToastMsg(final String msg) {
    new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        if (customToast == null) {
          customToast = new Toast(mContext);
          customToast.setDuration(Toast.LENGTH_SHORT);
          customToast.setView(view);
        }
        tv_toast.setVisibility(View.GONE);
        tv_toast.setText(msg);
        tv_toast.setVisibility(View.VISIBLE);
      }
    });
  }
}