package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.ui.dto.EtcDTO.Streamingtype;

/**
 * microphone 을 통해 발화한 데이터 정보를 JS 에게 전달할 때 사용되는 데이터 전송 객체에 대한 클래스입니다.
 * <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */

public class StreamingStatusDTO {

  // 스트림 ID
  private String streamId = Const.EMPTY_STRING;
  // 응답 동기화 ID
  private String operationSyncId = Const.EMPTY_STRING;
  // 이벤트인가 디렉티브인가
  private boolean isEvent = true;
  // 이벤트가 강제로 중지되었는가?
  private boolean isBreak = false;
  // 이벤트의 끝인가?
  private boolean isEnd = false;
  // 스트리밍 데이터의 구체적인 형식
  private String type = Streamingtype.BYTES;
  // 스트리밍의 크기
  private int size = 0;

  public StreamingStatusDTO() {
  }

  public String getStreamId() {
    return streamId;
  }

  public void setStreamId(String streamId) {
    this.streamId = streamId;
  }

  public String getOperationSyncId() {
    return operationSyncId;
  }

  public void setOperationSyncId(String operationSyncId) {
    this.operationSyncId = operationSyncId;
  }

  public boolean isEvent() {
    return isEvent;
  }

  public void setEvent(boolean event) {
    isEvent = event;
  }

  public boolean isBreak() {
    return isBreak;
  }

  public void setBreak(boolean aBreak) {
    isBreak = aBreak;
  }

  public boolean isEnd() {
    return isEnd;
  }

  public void setEnd(boolean end) {
    isEnd = end;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }
}
