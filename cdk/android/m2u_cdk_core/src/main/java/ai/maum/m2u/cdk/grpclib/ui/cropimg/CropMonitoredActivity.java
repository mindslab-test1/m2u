package ai.maum.m2u.cdk.grpclib.ui.cropimg;

import android.app.Activity;
import android.os.Bundle;
import java.util.ArrayList;

/**
 * Activity 모니터링
 */

public abstract class CropMonitoredActivity extends Activity {

  private final ArrayList<LifeCycleListener> mListeners = new ArrayList<LifeCycleListener>();

  public void addLifeCycleListener(LifeCycleListener listener) {
    if (mListeners.contains(listener)) {
      return;
    }
    mListeners.add(listener);
  }

  public void removeLifeCycleListener(LifeCycleListener listener) {
    mListeners.remove(listener);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    for (LifeCycleListener listener : mListeners) {
      listener.onActivityCreated(this);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    for (LifeCycleListener listener : mListeners) {
      listener.onActivityDestroyed(this);
    }
  }

  @Override
  protected void onStart() {

    super.onStart();
    for (LifeCycleListener listener : mListeners) {
      listener.onActivityStarted(this);
    }
  }

  @Override
  protected void onStop() {

    super.onStop();
    for (LifeCycleListener listener : mListeners) {
      listener.onActivityStopped(this);
    }
  }

  /**
   * Activity Life Cycle Listener
   *
   * @author Administrator
   */
  public static interface LifeCycleListener {

    public void onActivityCreated(CropMonitoredActivity activity);

    public void onActivityDestroyed(CropMonitoredActivity activity);

    public void onActivityPaused(CropMonitoredActivity activity);

    public void onActivityResumed(CropMonitoredActivity activity);

    public void onActivityStarted(CropMonitoredActivity activity);

    public void onActivityStopped(CropMonitoredActivity activity);
  }

  public static class LifeCycleAdapter implements LifeCycleListener {

    @Override
    public void onActivityCreated(CropMonitoredActivity activity) {
    }

    @Override
    public void onActivityDestroyed(CropMonitoredActivity activity) {
    }

    @Override
    public void onActivityPaused(CropMonitoredActivity activity) {
    }

    @Override
    public void onActivityResumed(CropMonitoredActivity activity) {
    }

    @Override
    public void onActivityStarted(CropMonitoredActivity activity) {
    }

    @Override
    public void onActivityStopped(CropMonitoredActivity activity) {
    }
  }
}
