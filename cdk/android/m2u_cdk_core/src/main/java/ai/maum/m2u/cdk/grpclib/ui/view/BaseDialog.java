package ai.maum.m2u.cdk.grpclib.ui.view;


import ai.maum.m2u.cdk.grpclib.R;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created on 2017. 12. 27..
 */

public abstract class BaseDialog extends Dialog {

  protected TextView tv_title;
  protected LinearLayout ll_container;
  protected View view_contents;
  protected Button btn_ok, btn_cancel;

  protected LayoutInflater layoutInflater;

  public BaseDialog(@NonNull Context context) {
    super(context);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.dialog_base);
    setCancelable(true);
    init();
  }

  private void init() {
    layoutInflater = (LayoutInflater) getContext()
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    tv_title = (TextView) findViewById(R.id.tv_title);
    ll_container = (LinearLayout) findViewById(R.id.ll_container);
    btn_ok = (Button) findViewById(R.id.btn_ok);
    btn_cancel = (Button) findViewById(R.id.btn_cancel);
  }

  protected void setContainerView(int layout) {
    view_contents = layoutInflater.inflate(layout, null);
    ll_container.addView(view_contents);
  }
}
