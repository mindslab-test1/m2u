package ai.maum.m2u.cdk.grpclib.ui.updater;

/**
 * UI 를 업데이트 하기 위해 사용되는 Observer pattern 클래스에서 참조하는 클래스 입니다.
 * {@link BaseUIUpdater} 클래스를 상속받았습니다.
 */

public class UIUpdater extends BaseUIUpdater {

  private static final String TAG = UIUpdater.class.getSimpleName();
  /**
   * 현재 업데이트 중인지에 대한 여부를 저장하는 변수입니다.
   */
  private boolean mIsUpdate = false;

  /**
   * 현재 업데이트 중인지에 대한 여부를 확인하는 함수입니다.
   * {@link BaseUIUpdater} 클래스의 추상 메소드를 구현한 함수입니다.
   *
   * @return 현재 업데이트 중이면 true
   */
  @Override
  public boolean getUpdateStatus() {
    return mIsUpdate;
  }

  /**
   * UI 에 업데이트 될 정보를 Observer pattern 에게 전달해 주는 함수입니다.
   * {@link BaseUIUpdater} 클래스의 추상 메소드를 구현한 함수입니다.
   *
   * @param updateCase 업데이트를 구분하는 case값
   * @param arg1 전달될 String
   * @param arg2 전달될 String
   * @param arg3 전달될 Integer
   * @param arg4 전달될 Boolean
   * @param arg5 전달될 byte[]
   * @param obj1 전달될 Object
   * @param obj2 전달될 Object
   */
  @Override
  public void execute(int updateCase, String arg1, String arg2, int arg3,
      boolean arg4, byte[] arg5, Object obj1, Object obj2) {
    mIsUpdate = true;
    notifyObserver(updateCase, arg1, arg2, arg3, arg4, arg5, obj1, obj2);
  }
}
