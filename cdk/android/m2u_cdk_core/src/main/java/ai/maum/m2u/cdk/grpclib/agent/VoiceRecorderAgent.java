package ai.maum.m2u.cdk.grpclib.agent;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst;
import ai.maum.m2u.cdk.grpclib.ui.updater.ExecuteUpdater;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder.AudioSource;
import android.media.audiofx.NoiseSuppressor;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import com.google.protobuf.ByteString;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * microphone 으로 발화된 byteStream 데이터를 MAP 에게 전달하기 위해 AudioRecord 을 초기화
 * 또는 해제하고, 출력해주는 함수들로 구성된 클래스 입니다.
 */

public class VoiceRecorderAgent extends AsyncTask<Void, Void, Void> {

  private static final String TAG = VoiceRecorderAgent.class.getSimpleName();
  public static final int sampleRate = 16000;
  public static final short channels = 1;
  public static final short bitDepth = 16;
  public static final AudioRecord audioRecord;
  private NoiseSuppressor noiseSuppressor = null;
  Context mContext;
  byte[] buffer;
  String operationSyncId = "";
  String streamId = "";
  String encoding = "LINEAR16";
  String model = "baseline";

  /**
   * 클래스 생성자 입니다.
   * <br>
   * 파라미터로 전달받은 데이터를 set 합니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @param sampleRate sampleRate : 16000
   * @param encoding encoding : LINEAR16
   * @param model baseline
   */
  public VoiceRecorderAgent(Context context, int sampleRate, String encoding,
      String model) {
    super();
    mContext = context;
    this.sampleRate = sampleRate;
    this.encoding = encoding;
    this.model = model;
  }

  /**
   * microphone 으로 발화 후 MAP 에 전송할 때 필요한 streamId, operationSyncId 를 설정하는 함수입니다.
   */
  public void setStreamOperationIds(String streamId, String operationSyncId) {
    this.streamId = streamId;
    this.operationSyncId = operationSyncId;
  }

  /**
   * audioRecord 의 객체가 생성 되었는지 체크하는 함수입니다.
   *
   * @return audioRecord 의 객체가 생성 된 경우 true
   */
  public boolean isAudioRecordExist() {
    return audioRecord != null ? true : false;
  }

  /**
   * audioRecord 의 객체를 생성하는 함수입니다.
   */
  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    // 실제 발화한 데이터가 정상인지 확인하려면 파일로 저장해서 확인할 수 있다.
    // createFile();
    audioRecord = createAudioRecord();
    LogUtil.d(TAG, "onPreExecute : audioRecord = " + audioRecord);
    if (audioRecord == null) {
      runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_MICROPHONE_RECORDINGSTATUS,
          null, null, 0, false, null, null, null);
      return;
    }
  }

  /**
   * microphone 으로 발화된 byteStream 데이터를 MAP 에 전송하기 위해 UI 에 전달해 주는 함수입니다.
   *
   * @param params void
   * @return void
   */
  @Override
  protected Void doInBackground(Void... params) {
    if (audioRecord == null) {
      LogUtil.d(TAG, "audioRecord is null");
      runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_MICROPHONE_RECORDINGSTATUS,
          null, null, 0, false,
          null, null, null);
      return null;
    }
    LogUtil.d(TAG, "audioRecord doInBackground");
    try {
      audioRecord.startRecording();
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
      return null;
    }
    // 최초 Recocding 이 시작될 때 JS 에 알림 : notifyMicrophoneStatus()
    runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_MICROPHONE_RECORDINGSTATUS,
        null, null, 0, true,
        null, null, null);

    // 발화데이터를 pcm 파일로 저장하는 코드입니다. 테스트용입니다. mIsSaveAudioRecord 값에 따라 enable 또는
    // disable 됩니다.
    BufferedOutputStream bos = null;
    DataOutputStream dos = null;
    if (Const.mIsSaveAudioRecord) {
      File file = new
          File(Environment.getExternalStorageDirectory(), "cdk.pcm");
      if (file.exists())
        file.delete();
      OutputStream os = null;
      try {
        os = new FileOutputStream(file);
      } catch (FileNotFoundException e) {
        LogUtil.e(TAG, e.getMessage());
      }
      bos = new BufferedOutputStream(os);
      dos = new DataOutputStream(bos);
    }

    while (audioRecord.getRecordingState() == AudioRecord
        .RECORDSTATE_RECORDING) {
      if (this.isCancelled()) {
        // 이벤트가 강제로 중지되었는가? obj1 : isBreak(boolean),
        // 이벤트의 끝인가? obj2 : isEnd(boolean)
        runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_STREAMSENT, streamId,
            operationSyncId, 0, false, null, true, false);
        break;
      }
      int size = audioRecord.read(buffer, 0, buffer.length);
      LogUtil.e(TAG, "VoiceRecorderAgent : " + ByteString.copyFrom(buffer)
          .toString());
      // 이벤트가 강제로 중지되었는가? obj1 : isBreak(boolean),
      // 이벤트의 끝인가? obj2 : isEnd(boolean)
      runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_STREAMSENT, streamId,
          operationSyncId, 0, false, buffer, false, false);

      if (Const.mIsSaveAudioRecord) {
        try {
          if (dos != null) {
            dos.write(buffer);
          }
        } catch (IOException e) {
          LogUtil.e(TAG, e.getMessage());
        }
      }
    }

    if (Const.mIsSaveAudioRecord) {
      try {
        if (dos != null) {
          dos.close();
        }
      } catch (IOException e) {
        LogUtil.e(TAG, e.getMessage());
      }
    }

    return null;
  }

  /**
   * microphone 으로 발화가 끝난 후 audioRecord 객체를 해제하는 함수입니다.
   *
   * @param aVoid void
   */
  @Override
  public void onPostExecute(Void aVoid) {
    super.onPostExecute(aVoid);
    LogUtil.d(TAG, "Recoder onPostExecute");
    if (audioRecord != null) {
      if(noiseSuppressor != null) {
        noiseSuppressor.release();
      }
      audioRecord.stop();
      audioRecord = null;
    }
    // 이벤트가 강제로 중지되었는가? obj1 : isBreak(boolean),
    // 이벤트의 끝인가? obj2 : isEnd(boolean)
    runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_STREAMSENT, streamId,
        operationSyncId, 0, false, null, false, true);
  }

  /**
   * audioRecord 객체를 생성하는 함수입니다.
   *
   * @return 생성된 audioRecord 객체
   */
  private AudioRecord createAudioRecord() {
    // audioFormat defulat : AudioFormat.ENCODING_PCM_16BIT
    int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
    if (!encoding.equals("LINEAR16")) {
      audioFormat = AudioFormat.ENCODING_PCM_8BIT;
    }
    int sizeInBytes = AudioRecord.getMinBufferSize(sampleRate, AudioFormat
        .CHANNEL_IN_MONO, audioFormat);
    if (sizeInBytes == AudioRecord.ERROR_BAD_VALUE || sizeInBytes ==
        AudioRecord.ERROR) {
      return null;
    }
    AudioRecord instance = new AudioRecord(AudioSource.VOICE_RECOGNITION,
        sampleRate, AudioFormat.CHANNEL_IN_MONO, audioFormat, sizeInBytes);
    if (instance.getState() == AudioRecord.STATE_INITIALIZED) {
      if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
        if (isNoiseSuppressorAvailable()) {
          noiseSuppressor = NoiseSuppressor.create(instance.getAudioSessionId());
          if (noiseSuppressor == null) {
            LogUtil.d(TAG, "NoiseSuppressor: failed");
          } else {
            LogUtil.d(TAG, "NoiseSuppressor: ON");
          }
        }
      }
      buffer = new byte[sizeInBytes];
      LogUtil.d(TAG, "instance.getState() = INITIALIZED");
    } else {
      instance.release();
      LogUtil.d(TAG, "instance.getState() = UNINITIALIZED");
    }
    return instance;
  }

  /**
   * NoiseSuppressor 를 지원하는지 여부를 판단하는 함수입니다.
   *
   * @return 지원하는 경우 true, 지원하지 않으면 false
   */
  private boolean isNoiseSuppressorAvailable() {
    if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
      return NoiseSuppressor.isAvailable();
    }
    return false;
  }

  /**
   * UI 에게 값을 전달하기 위해 Observer pattern을 통해서 값을 전달하는 함수입니다.
   */
  private void runUiUpdater(int updateCase, String arg1, String arg2, int
      arg3, boolean arg4, byte[] arg5, Object obj1, Object obj2) {
    ExecuteUpdater updater = ExecuteUpdater.getInstance();
    updater.updateRun(updateCase, arg1, arg2, arg3, arg4, arg5, obj1, obj2);
  }
}