package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;

/**
 * microphone 설정 정보를 JS 로 부터 전달받거나 microphone 상태를 JS 에게 전달하기 위해 사용되는 데이터 전송 객체에
 * 대한 클래스입니다.
 * <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */
public class MicrophoneDTO {

  /**
   * microphone 이벤트를 정의한 클래스입니다.
   */
  public static class MicrophoneEvent {

    public static final String UNKNOWN = "UNKNOWN";
    public static final String OPEN_SUCCESS = "OPEN_SUCCESS";
    public static final String OPEN_FAILURE = "OPEN_FAILURE";
    public static final String STARTING_RECORD = "STARTING_RECORD";
    public static final String CLOSE_SUCCESS = "CLOSE_SUCCESS";
    public static final String CLOSE_FAILURE = "CLOSE_FAILURE";
  }

  /**
   * microphone open, close 시 실패 종류를 정의한 클래스입니다.
   */
  public static class MicrophoneFailure {

    public static final String MICROPHONE_ERROR_UNKNOWN = "MICROPHONE_ERROR_UNKNOWN";
    /* open failure */
    public static final String MICROPHONE_OPEN_ERROR = "MICROPHONE_OPEN_ERROR";
    /* close failure */
    public static final String MICROPHONE_CLOSE_ERROR = "MICROPHONE_CLOSE_ERROR";
    /* open failure */
    public static final String MICROPHONE_UNDER_USE = "MICROPHONE_UNDER_USE";
    /* closed success */
    public static final String MICROPHONE_TIMER_EXPIRED = "MICROPHONE_TIMER_EXPIRED";
    public static final String MICROPHONE_PERMISSION_DENIED = "MICROPHONE_PERMISSION_DENIED";
  }

  // 언어와 로케일을 함께 포함한 구조
  public static class Lang {

    public static final String ko_KR = "ko_KR";
    public static final String en_US = "en_US";
  }

  /**
   * microphone open 시 파라미터 구성에 대한 클래스 입니다.
   */
  public static class MicrophoneParam {

    // microphone 설정을 위한 파라미터 구성에 대한 클래스 객체입니다.
    public static final SpeechRecognitionParam speechParam
        = new SpeechRecognitionParam();
    // microphone 대기 모드 유무에 대한 변수입니다.
    private boolean expectMode = false;
    /**
     * microphone 유지 시간(milli sec)에 대한 변수입니다.
     * MAP 으로 부터 recognizing 을 수신하면 해당 시간으로 타이머를 재시작 합니다.
     */
    private int timoeoutInMilliseconds = 10000;
    /**
     * mic voice gain 값에 대한 변수입니다.
     * 이 값을 기준으로 mic의 input값을 설정합니다.
     * 범위 : 0.0f ~ 1.0f
     */
    private float miceGain = 0.5f;

    public MicrophoneParam() {
    }

    public SpeechRecognitionParam getSpeechParam() {
      return speechParam;
    }

    public void setSpeechParam(
        SpeechRecognitionParam speechParam) {
      this.speechParam = speechParam;
    }

    public boolean isExpectMode() {
      return expectMode;
    }

    public void setExpectMode(boolean expectMode) {
      this.expectMode = expectMode;
    }

    public int getTimoeoutInMilliseconds() {
      return timoeoutInMilliseconds;
    }

    public void setTimoeoutInMilliseconds(int timoeoutInMilliseconds) {
      this.timoeoutInMilliseconds = timoeoutInMilliseconds;
    }

    public float getMiceGain() {
      return miceGain;
    }

    public void setMiceGain(float miceGain) {
      this.miceGain = miceGain;
    }

    /**
     * microphone 설정을 위한 파라미터 구성에 대한 클래스 입니다.
     * <br>
     * JS 로 부터 전달받는 데이터 입니다.
     */
    public static class SpeechRecognitionParam {

      // 현재는 PCM만 지원한다. ("LINEAR16")
      public static final String encoding = "LINEAR16";
      // 8K, 16K를 지원한다. 기본값으로 16K를 사용한다.(16000)
      public static final int sampleRate = 16000;
      // 현재 언어는 kor, eng 만 사용가능하다.
      public static final String lang = Lang.ko_KR;
      // STT 학습 모델 정보입니다.
      public static final String model = "baseline";
      // JS 로 부터 전달
      public static final boolean singleUtterance = false;

      public SpeechRecognitionParam() {
      }

      public String getEncoding() {
        return encoding;
      }

      public void setEncoding(String encoding) {
        this.encoding = encoding;
      }

      public int getSampleRate() {
        return sampleRate;
      }

      public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
      }

      public String getLang() {
        return lang;
      }

      public void setLang(String lang) {
        this.lang = lang;
      }

      public String getModel() {
        return model;
      }

      public void setModel(String model) {
        this.model = model;
      }

      public boolean getSingleUtterance() {
        return singleUtterance;
      }

      public void setSingleUtterance(boolean singleUtterance) {
        this.singleUtterance = singleUtterance;
      }
    }
  }

  /**
   * microphone 의 상태 정보를 구성하는 클래스 입니다.
   */
  public static class MicrophoneStatus {

    // microphone 의 event 종류
    private String eventType = MicrophoneEvent.UNKNOWN;
    // 현재가 export mode 인가?
    private boolean expectMode = false;
    // 추가적인 메시지 여부
    private boolean hasReason = false;
    // hasReason이 true인 경우 reason
    private String failure = MicrophoneFailure.MICROPHONE_ERROR_UNKNOWN;
    // 상세한 에러 메시지
    private String message = Const.EMPTY_STRING;

    public MicrophoneStatus() {
    }

    public String getEventType() {
      return eventType;
    }

    public void setEventType(String eventType) {
      this.eventType = eventType;
    }

    public boolean isExpectMode() {
      return expectMode;
    }

    public void setExpectMode(boolean expectMode) {
      this.expectMode = expectMode;
    }

    public boolean isHasReason() {
      return hasReason;
    }

    public void setHasReason(boolean hasReason) {
      this.hasReason = hasReason;
    }

    public String getFailure() {
      return failure;
    }

    public void setFailure(String failure) {
      this.failure = failure;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
}
