package ai.maum.m2u.cdk.grpclib.utils;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.opengl.GLES10;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

/**
 * Bitmap 관련 처리를 하는 함수 및 서브 클래스로 구성된 클래스 입니다.
 */

public class BitmapUtil {

  public static final String TAG = BitmapUtil.class.getSimpleName();

  /**
   * Bitmap 을 회전시키기 위한 함수들로 구성된 서브 클래스 입니다.
   * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
   */
  public static class RotateBitmap {

    private Bitmap mBitmap;
    private int mRotation;

    /**
     * 클래스 생성자 입니다.
     *
     * @param bitmap 회전시킬 대상 이미지에 대한 bitmap 데이터
     */
    public RotateBitmap(Bitmap bitmap) {
      mBitmap = bitmap;
      mRotation = 0;
    }

    /**
     * 클래스 생성자 입니다.
     *
     * @param bitmap 회전시킬 대상 이미지에 대한 bitmap 데이터
     * @param rotation 회전시킬 각도
     */
    public RotateBitmap(Bitmap bitmap, int rotation) {
      mBitmap = bitmap;
      mRotation = rotation % 360;
    }

    /**
     * 현재 설정된 회전시킬 각도를 리턴해주는 함수입니다.
     *
     * @return 현재 설정된 회전시킬 각도
     */
    public int getRotation() {
      return mRotation;
    }

    /**
     * Bitmap 을 회전시킬 각도를 설정하는 함수입니다.
     *
     * @param rotation 회전시킬 각도
     */
    public void setRotation(int rotation) {
      mRotation = rotation;
    }

    /**
     * 현재 설정된 회전시킬 Bitmap 을 리턴해 주는 함수입니다.
     *
     * @return 현재 설정된 회전시킬 Bitmap
     */
    public Bitmap getBitmap() {
      return mBitmap;
    }

    /**
     * 회전시킬 Bitmap 을 설정하는 함수입니다.
     */
    public void setBitmap(Bitmap bitmap) {
      mBitmap = bitmap;
    }

    /**
     * 회전된 Matrix 정보를 리턴하는 함수입니다.
     *
     * @return 회전된 Matrix 정보
     */
    public Matrix getRotateMatrix() {
      // By default this is an identity matrix.
      Matrix matrix = new Matrix();
      if (mRotation != 0) {
        // We want to do the rotation at origin, but since the bounding
        // rectangle will be changed after rotation, so the delta values
        // are based on old & new width/height respectively.
        int cx = mBitmap.getWidth() / 2;
        int cy = mBitmap.getHeight() / 2;
        matrix.preTranslate(-cx, -cy);
        matrix.postRotate(mRotation);
        matrix.postTranslate(getWidth() / 2, getHeight() / 2);
      }
      return matrix;
    }

    /**
     * 현재 Orientation 이 변경되었는지 확인하는 함수입니다.
     *
     * @return Orientation 이 변경된 경우 true
     */
    public boolean isOrientationChanged() {
      return (mRotation / 90) % 2 != 0;
    }

    /**
     * 변경된 Orientation 에 의한 Bitmap 의 Height 를 리턴하는 함수입니다.
     *
     * @return Bitmap 의 Height
     */
    public int getHeight() {
      if (isOrientationChanged()) {
        return mBitmap.getWidth();
      } else {
        return mBitmap.getHeight();
      }
    }

    /**
     * 변경된 Orientation 에 의한 Bitmap 의 Height 를 리턴하는 함수입니다.
     *
     * @return Bitmap 의 Width
     */
    public int getWidth() {
      if (isOrientationChanged()) {
        return mBitmap.getHeight();
      } else {
        return mBitmap.getWidth();
      }
    }

    /**
     * Bitmap 회전에 사용되었던 리소스를 해제하는 함수입니다.
     */
    public void recycle() {
      if (mBitmap != null) {
        mBitmap.recycle();
        mBitmap = null;
      }
    }
  }

  /**
   * 메인 UI 에서 Image 관련 처리를 할때 사용되는 클래스입니다.
   */
  public static class ProcessImage {

    /**
     * 특정 뷰에 표시될 이미지의 제한 크기를 알아내기 위한 함수입니다.
     *
     * @return 특정 뷰에 표시될 크기의 최대값
     */
    public static int[] getMaxTextureSize() {
      EGL10 egl = (EGL10) EGLContext.getEGL();
      EGLDisplay dpy = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
      int[] vers = new int[2];
      egl.eglInitialize(dpy, vers);
      int[] configAttr = {
          EGL10.EGL_COLOR_BUFFER_TYPE, EGL10.EGL_RGB_BUFFER,
          EGL10.EGL_LEVEL, 0,
          EGL10.EGL_SURFACE_TYPE, EGL10.EGL_PBUFFER_BIT,
          EGL10.EGL_NONE
      };
      EGLConfig[] configs = new EGLConfig[1];
      int[] numConfig = new int[1];
      egl.eglChooseConfig(dpy, configAttr, configs, 1, numConfig);
      if (numConfig[0] == 0) {
        // what to do here?
      }
      EGLConfig config = configs[0];
      int[] surfAttr = {
          EGL10.EGL_WIDTH, 64,
          EGL10.EGL_HEIGHT, 64,
          EGL10.EGL_NONE
      };
      EGLSurface surf = egl.eglCreatePbufferSurface(dpy, config, surfAttr);
      final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;  // missing in EGL10
      int[] ctxAttrib = {
          EGL_CONTEXT_CLIENT_VERSION, 1,
          EGL10.EGL_NONE
      };
      EGLContext ctx = egl.eglCreateContext(dpy, config, EGL10.EGL_NO_CONTEXT,
          ctxAttrib);
      egl.eglMakeCurrent(dpy, surf, surf, ctx);
      int[] maxSize = new int[1];
      GLES10.glGetIntegerv(GLES10.GL_MAX_TEXTURE_SIZE, maxSize, 0);
      egl.eglMakeCurrent(dpy, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE,
          EGL10.EGL_NO_CONTEXT);
      egl.eglDestroySurface(dpy, surf);
      egl.eglDestroyContext(dpy, ctx);
      egl.eglTerminate(dpy);
      return maxSize;
    }

    /**
     * Uri에 대한 이미지 경로 가져오는 함수 입니다.
     *
     * @param context 함수를 호출하는 클래스의 context
     * @param contentUri 이미지의 경로를 가져오기 위한 uri
     * @return uri 에 해당되는 경로
     */
    public static String getRealPathFromURI(Context context, Uri contentUri) {
      int column_index = 0;
      String[] proj = {MediaStore.Images.Media.DATA};
      Cursor cursor = context.getContentResolver()
          .query(contentUri, proj, null, null, null);
      if (cursor.moveToFirst()) {
        column_index = cursor
            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
      }
      return cursor.getString(column_index);
    }

    /**
     * imagePath 의 이미지를 로딩하여 base64로 encoding 한 String 타입의 데이터를
     * 리턴하는 함수입니다.
     * 이 데이터는 JS 에게 전달될 Thumbnail 크기의 데이터 입니다.
     *
     * @param context 함수를 호출하는 클래스의 context
     * @param imagePath 이미지의 경로
     * @return base64 로 encoding 된 String 타입의 이미지 데이터
     */
    public static String convertImageToString(Context context, String
        imagePath) {
      BitmapFactory.Options options = new BitmapFactory.Options();
      Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
      ExifInterface originalExif = null;
      try {
        originalExif = new ExifInterface(imagePath);
      } catch (IOException e) {
        LogUtil.e(TAG, "IOException", e);
      }
      int degrees = CommUtil.getExifOrientation(originalExif);
      Bitmap resized = resizeBitmapImage(context, CommUtil.rotate(bitmap,
          degrees));
      //encode image(from image path) to base64 string
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      resized.compress(Bitmap.CompressFormat.JPEG, 100, baos);
      byte[] imageBytes = baos.toByteArray();
      String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
      imageString = imageString.replaceAll("\n", "");
      imageString = "data:image/jpg;base64," + imageString;
      if (resized != null) {
        resized.recycle();
      }
      return imageString;
    }

    /**
     * 원래 이미지 크기의 절반 크기로 리사이징된 Bitmap 데이터를 리턴하는 함수입니다.
     *
     * @param context 함수를 호출하는 클래스의 context
     * @param source 원래 이미지에 대한 Bitmap
     * @return 리사이징 된 Bitmap 데이터
     */
    public static Bitmap resizeBitmapImage(Context context, Bitmap source) {
      int[] maxTextureSize = new int[1];
      maxTextureSize = getMaxTextureSize();
      DisplayMetrics metrics = context.getResources().getDisplayMetrics();
      int scrW = metrics.widthPixels / 4;
      int scrH = metrics.heightPixels / 4;
      // Web 에서 표시될 이미지 영역의 WIDTH 값
      int webViewImageW = Const.IMAGEVIEW_AREA_WIDTH;
      int imgW = source.getWidth();
      int imgH = source.getHeight();
      float rate = 0.0f;
      int newW = imgW;
      int newH = imgH;
      if (imgW <= webViewImageW) {
        // DO nothing
      } else if (imgW > webViewImageW && imgW < scrW) {
        rate = webViewImageW / (float) imgW;
        newW = (int) (imgW * rate);
        newH = (int) (imgH * rate);
      } else {
        if (scrW < scrH) {
          // 화면 세로
          if (imgW < imgH) {
            // 이미지 세로
            rate = scrW / (float) imgW;
            newW = scrW;
            newH = (int) (imgH * rate);
            if (maxTextureSize[0] != 0 && newH > maxTextureSize[0]) {
              newH = maxTextureSize[0] - 50;
            }
          } else {
            // 이미지 가로
            rate = scrH / (float) imgH;
            newW = (int) (imgW * rate);
            newH = scrH;
            if (maxTextureSize[0] != 0 && newW > maxTextureSize[0]) {
              newW = maxTextureSize[0] - 50;
            }
          }
        } else {
          // 화면 가로
          if (imgW < imgH) {
            // 이미지 세로
            rate = scrH / (float) imgH;
            newW = (int) (imgW * rate);
            newH = scrH;
          } else {
            // 이미지 가로
            rate = scrW / (float) imgW;
            newW = scrW;
            newH = (int) (imgH * rate);
          }
        }
      }
      return Bitmap.createScaledBitmap(source, newW, newH, true);
    }
  }
}