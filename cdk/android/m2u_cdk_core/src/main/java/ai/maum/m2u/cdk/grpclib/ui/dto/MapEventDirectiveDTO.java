package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.utils.StringUtil;
import com.google.protobuf.ByteString;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.MapException;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;

/**
 * eventStreamJson, MapDirective 를 파싱한 후 객체로 관리하기 위한 클래스 입니다.
 */

public class MapEventDirectiveDTO {

  /**
   * 파싱된 eventStreamJson 을 객체로 관리하기 위한 클래스 입니다.
   */
  public static class MapEventInfo {

    // 파싱할 eventStreamJson
    private String eventStreamJson = Const.EMPTY_STRING;
    // streamId
    private String streamId = Const.EMPTY_STRING;
    // operationSyncId
    private String operationSyncId = Const.EMPTY_STRING;
    // interfaceName
    private String interfaceName = Const.EMPTY_STRING;
    // operationName
    private String operationName = Const.EMPTY_STRING;
    // streaming 유무
    private boolean streaming = false;
    // location context 유무
    private boolean hasLocation = false;
    // location latitude 정보
    private float latitude = 0.f;
    // location longitude 정보
    private float longitude = 0.f;
    // 위치 정보
    private String location = "";

    public MapEventInfo() {

    }

    public String getEventStreamJson() {
      return eventStreamJson;
    }

    public void setEventStreamJson(String eventStreamJson) {
      this.eventStreamJson = eventStreamJson;
    }

    public String getStreamId() {
      return streamId;
    }

    public void setStreamId(String streamId) {
      this.streamId = streamId;
    }

    public String getOperationSyncId() {
      return operationSyncId;
    }

    public void setOperationSyncId(String operationSyncId) {
      this.operationSyncId = operationSyncId;
    }

    public String getInterfaceName() {
      return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
      this.interfaceName = interfaceName;
    }

    public String getOperationName() {
      return operationName;
    }

    public void setOperationName(String operationName) {
      this.operationName = operationName;
    }

    public boolean isStreaming() {
      return streaming;
    }

    public void setStreaming(boolean streaming) {
      this.streaming = streaming;
    }

    public boolean isHasLocation() {
      return hasLocation;
    }

    public void setHasLocation(boolean hasLocation) {
      this.hasLocation = hasLocation;
    }

    public float getLatitude() {
      return latitude;
    }

    public void setLatitude(float latitude) {
      this.latitude = latitude;
    }

    public float getLongitude() {
      return longitude;
    }

    public void setLongitude(float longitude) {
      this.longitude = longitude;
    }

    public String getLocation() {
      return location;
    }

    public void setLocation(String location) {
      this.location = location;
    }
  }

  /**
   * 파싱된 directiveStream 을 객체로 관리하기 위한 클래스 입니다.
   */
  public static class MapDirectiveInfo {

    // 파싱할 directiveStream
    public static final DirectiveStreamInfo directiveStreamInfo;
    // MapException 객체
    private MapException mapException;
    // DirectiveStream 객체
    private DirectiveStream directiveStream;
    // ByteString 객체
    private ByteString byteStream;
    // ByteString 객체
    private ByteString byteTextStream;
    // StreamEnd 객체
    private StreamEnd streamEnd;
    // StreamBreak 객체
    private StreamBreak streamBreak;
    // directiveText 문자열
    private String directiveText;
    // streamMeta 객체
    private StreamMeta streamMeta;
    // directiveStreamJson 문자열
    private String directiveStreamJson;
    // mapExceptionJson 문자열
    private String mapExceptionJson;

    public MapDirectiveInfo() {

    }

    public DirectiveStreamInfo getDirectiveStreamInfo() {
      return directiveStreamInfo;
    }

    public void setDirectiveStreamInfo(
        DirectiveStreamInfo directiveStreamInfo) {
      this.directiveStreamInfo = directiveStreamInfo;
    }

    public MapException getMapException() {
      return mapException;
    }

    public DirectiveStream getDirectiveStream() {
      return directiveStream;
    }

    public ByteString getByteStream() {
      return byteStream;
    }

    public ByteString getByteTextStream() {
      return byteTextStream;
    }

    public StreamEnd getStreamEnd() {
      return streamEnd;
    }

    public StreamBreak getStreamBreak() {
      return streamBreak;
    }

    public String getDirectiveText() {
      return directiveText;
    }

    public StreamMeta getStreamMeta() {
      return streamMeta;
    }

    public String getDirectiveStreamJson() {
      return directiveStreamJson;
    }

    public void setDirectiveStreamJson(String directiveStreamJson) {
      this.directiveStreamJson = directiveStreamJson;
    }

    public String getMapExceptionJson() {
      return mapExceptionJson;
    }

    public void setMapExceptionJson(String mapExceptionJson) {
      this.mapExceptionJson = mapExceptionJson;
    }

    public boolean isMapException() {
      boolean isReturn = true;
      if (mapException == null || StringUtil.isEmptyString(mapException
          .toString())) {
        isReturn = false;
      }
      return isReturn;
    }

    public void setMapException(MapException mapException) {
      this.mapException = mapException;
    }

    public boolean isDirectiveStream() {
      boolean isReturn = true;
      if (directiveStream == null || StringUtil.isEmptyString(directiveStream
          .toString())) {
        isReturn = false;
      }
      return isReturn;
    }

    public void setDirectiveStream(DirectiveStream directiveStream) {
      this.directiveStream = directiveStream;
    }

    public boolean isByteStream() {
      boolean isReturn = true;
      if (byteStream == null || byteStream.size() == 0) {
        isReturn = false;
      }
      return isReturn;
    }

    public void setByteStream(ByteString byteStream) {
      this.byteStream = byteStream;
    }

    public boolean isByteTextStream() {
      boolean isReturn = true;
      if (byteTextStream == null || byteTextStream.size() == 0) {
        isReturn = false;
      }
      return isReturn;
    }

    public void setByteTextStream(ByteString byteTextStream) {
      this.byteTextStream = byteTextStream;
    }

    public boolean isStreamEnd() {
      boolean isReturn = true;
      if (streamEnd == null || StringUtil.isEmptyString(streamEnd
          .toString())) {
        isReturn = false;
      }
      return isReturn;
    }

    public void setStreamEnd(StreamEnd streamEnd) {
      this.streamEnd = streamEnd;
    }

    public boolean isStreamBreak() {
      boolean isReturn = true;
      if (streamBreak == null || StringUtil.isEmptyString(streamBreak
          .toString())) {
        isReturn = false;
      }
      return isReturn;
    }

    public void setStreamBreak(StreamBreak streamBreak) {
      this.streamBreak = streamBreak;
    }

    public boolean isDirectiveText() {
      boolean isReturn = true;
      if (directiveText == null || StringUtil.isEmptyString(directiveText
          .toString())) {
        isReturn = false;
      }
      return isReturn;
    }

    public void setDirectiveText(String directiveText) {
      this.directiveText = directiveText;
    }

    public boolean isStreamMeta() {
      boolean isReturn = true;
      if (streamMeta == null || StringUtil.isEmptyString(streamMeta
          .toString())) {
        isReturn = false;
      }
      return isReturn;
    }

    public void setStreamMeta(StreamMeta streamMeta) {
      this.streamMeta = streamMeta;
    }
  }

  /**
   * MAP 으로 부터 전달받은 MapDirecive 데이터에서 interface, streamId, operationSyncId 관련
   * 데이터만 파싱한 후 객체로 관리하기 위한 클래스 입니다.
   */
  public static class DirectiveStreamInfo {

    // interfaceName
    private String interfaceName = Const.EMPTY_STRING;
    // operationName
    private String operationName = Const.EMPTY_STRING;
    // streamId
    private String streamId = Const.EMPTY_STRING;
    // operationSyncId
    private String operationSyncId = Const.EMPTY_STRING;
    // streamInfoJson
    private String streamInfoJson = Const.EMPTY_STRING;

    public DirectiveStreamInfo() {
    }

    public String getInterfaceName() {
      return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
      this.interfaceName = interfaceName;
    }

    public String getOperationName() {
      return operationName;
    }

    public void setOperationName(String operationName) {
      this.operationName = operationName;
    }

    public String getStreamId() {
      return streamId;
    }

    public void setStreamId(String streamId) {
      this.streamId = streamId;
    }

    public String getOperationSyncId() {
      return operationSyncId;
    }

    public void setOperationSyncId(String operationSyncId) {
      this.operationSyncId = operationSyncId;
    }

    public String getStreamInfoJson() {
      return streamInfoJson;
    }

    public void setStreamInfoJson(String streamInfoJson) {
      this.streamInfoJson = streamInfoJson;
    }

    public boolean isStreamInfoExist() {
      return streamInfoJson.isEmpty() ? false : true;
    }
  }
}
