package ai.maum.m2u.cdk.grpclib.chatbot;

import ai.maum.m2u.cdk.grpclib.agent.DeviceAgentManager;
import ai.maum.m2u.cdk.grpclib.chatbot.common.PreferenceAgent;
import ai.maum.m2u.cdk.grpclib.chatbot.grpc.GrpcAgent;
import ai.maum.m2u.cdk.grpclib.chatbot.grpc.IGrpcMetadataGetter;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst.MapKeys;
import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.constants.Const.MultiMediaType;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst.UIUpdater;
import ai.maum.m2u.cdk.grpclib.ui.CameraAllPreviewActivity;
import ai.maum.m2u.cdk.grpclib.ui.CameraPreviewActivity;
import ai.maum.m2u.cdk.grpclib.ui.cropimg.CropImageActivity;
import ai.maum.m2u.cdk.grpclib.ui.dto.CameraDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.CameraDTO.CameraEvent;
import ai.maum.m2u.cdk.grpclib.ui.dto.CameraDTO.CameraFailure;
import ai.maum.m2u.cdk.grpclib.ui.dto.EtcDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.EtcDTO.Streamingtype;
import ai.maum.m2u.cdk.grpclib.ui.dto.GalleryDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.GalleryDTO.GalleryEvent;
import ai.maum.m2u.cdk.grpclib.ui.dto.GalleryDTO.GalleryFailure;
import ai.maum.m2u.cdk.grpclib.ui.dto.LocationDTO.LocationFailure;
import ai.maum.m2u.cdk.grpclib.ui.dto.LocationDTO.LocationStatus;
import ai.maum.m2u.cdk.grpclib.ui.dto.LocationDTO.LocationStatus.Location;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapSettingsDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.MicrophoneDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.MicrophoneDTO.MicrophoneEvent;
import ai.maum.m2u.cdk.grpclib.ui.dto.MicrophoneDTO.MicrophoneFailure;
import ai.maum.m2u.cdk.grpclib.ui.dto.SpeakerDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.SpeakerDTO.SpeakerEvent;
import ai.maum.m2u.cdk.grpclib.ui.dto.StreamingStatusDTO;
import ai.maum.m2u.cdk.grpclib.ui.updater.ExecuteUpdater;
import ai.maum.m2u.cdk.grpclib.utils.CommUtil;
import ai.maum.m2u.cdk.grpclib.utils.FileUtil;
import ai.maum.m2u.cdk.grpclib.utils.GsonUtil;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import ai.maum.m2u.cdk.grpclib.utils.StringUtil;
import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import com.google.gson.Gson;
import io.grpc.Metadata;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 메인 앱과 라이브러리간 인터페이스 역할을 하는 클래스 입니다.
 * 메인 앱에서 라이브러리의 함수를 호출할 때 사용됩니다.
 * Singletone pattern 으로 동작합니다.
 */

public class GrpcClient {

  private static final String TAG = GrpcClient.class.getSimpleName();
  private static Context mContext;
  private static Activity mActivity;
  public static final boolean mIsReturn = false;
  public static final TimerTaskForAction mTimerTaskForAction;
  public static final TimerTaskForMic mTimerTaskForMic;
  public static final TimerTaskForLocation mTimerTaskForLocation;

  private GrpcAgent grpcAgent;
  // 사용자 위치 수신기
  private LocationManager locationManager = null;
  private int mMultimediaType = Const.MultiMediaType.NONE;
  private int mGalleryRequestCode = -1;
  private int mPreviewRequestCode = -1;
  private int mCropRequestCode = -1;
  private Timer mTimerAction, mTimerMic, mTimerLocation;
  private String mCurrentSpeechStreamId, mCurrentSpeechOperationSyncId;
  private long mMicTimeout = Const.MULTIMEDIA_TIMEOUT;
  private long mLocationTimeout = Const.LOCATION_TIMEOUT;

  /**
   * MAP과 통신을 하는 gRPC 클래스인 GrpcAgent 를 생성하는 함수입니다.
   */
  private GrpcClient() {
    LogUtil.setIsPrintLog(PreferenceAgent.getBooleanSharedData(mContext,
        MapKeys.IS_DDMS_LOG));
    LogUtil.setIsSaveLogToFile(PreferenceAgent.getBooleanSharedData(mContext,
        MapKeys.IS_FILE_LOG));
    if (grpcAgent == null) {
      grpcAgent = new GrpcAgent(mContext, new IGrpcMetadataGetter() {
        @Override
        public void onMetaData(Metadata md) {
          // 수신된 gRPC 에서 MetaData 정보를 조회한다
          grpcAgent.getCurrentMetaData(md);
        }
      });
    }
  }

  /**
   * Singletone 인스턴스를 리턴 해주는 함수입니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @return 클래스의 Singletone 인스턴스
   */
  public static GrpcClient getInstance(Context context) {
    mContext = context;
    mActivity = (Activity) context;
    return SingletonHolder.INSTANCE;
  }

  /**
   * 라이브러리에서 사용될 값들을 Preference 에 저장하는 함수입니다.
   * JS 에서 전달받은 데이터 또는 MAP 으로 부터 수신한 데이터 중 일부가  저장될 수 있습니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @param map <키, 값> 으로 구성된 Preference에 저장될 항목의 table
   */
  public static void setGrpcSettingData(Context context, Hashtable map) {
    Iterator iterator = map.keySet().iterator();
    while (iterator.hasNext()) {
      String key = (String) iterator.next();
      if (MapKeys.SERVER_SELECTED_IP.equals(key)
          || MapKeys.SERVER_IP.equals(key)
          || MapKeys.SERVER_DEV_IP.equals(key)
          || MapKeys.AUTH_TOKEN.equals(key)
          || MapKeys.NOTIFYOBJECT_PREFIX.equals(key)
          || MapKeys.USER_ID.equals(key)
          || MapKeys.USER_NAME.equals(key)
          || MapKeys.USER_ADDRESS.equals(key)
          || MapKeys.DEVICE_TYPE.equals(key)
          || MapKeys.DEVICE_ID.equals(key)
          || MapKeys.DEVICE_CHANNEL.equals(key)
          || MapKeys.DEVICE_PREFERREDCHATBOT.equals(key)
          || MapKeys.DEVICE_VERSION.equals(key)
          || MapKeys.DEVICE_META.equals(key)
          || MapKeys.SPEECH_ENCODING.equals(key)
          || MapKeys.SPEECH_LANG.equals(key)) {
        PreferenceAgent
            .setStringSharedData(context, key, (String) map.get(key));
      } else if (MapKeys.SERVER_PORT.equals(key)
          || MapKeys.SERVER_DEV_PORT.equals(key)
          || MapKeys.PING_INTERVAL.equals(key)
          || MapKeys.SPEECH_SAMPLERATE.equals(key)
          || MapKeys.DEVICE_TIMESTAMP_INT_NANOS.equals(key)) {
        PreferenceAgent.setIntSharedData(context, key, (Integer) map.get(key));
      } else if (MapKeys.DEVICE_TIMESTAMP_LONG_SECONDS.equals(key)) {
        PreferenceAgent.setlongSharedData(context, key, (Long) map.get(key));
      } else if (MapKeys.IS_DDMS_LOG.equals(key)
          || MapKeys.IS_FILE_LOG.equals(key)
          || MapKeys.IS_ERROR_TOAST.equals(key)
          || MapKeys.IS_LOOPBACK_DATA.equals(key)
          || MapKeys.USE_TLS.equals(key)
          || MapKeys.DEVICE_SUPPORT_RENDER_TEXT.equals(key)
          || MapKeys.DEVICE_SUPPORT_RENDER_CARD.equals(key)
          || MapKeys.DEVICE_SUPPORT_SPEECH_SYNTHESIZER.equals(key)
          || MapKeys.DEVICE_SUPPORT_PLAY_AUDIO.equals(key)
          || MapKeys.DEVICE_SUPPORT_PLAY_VIDEO.equals(key)
          || MapKeys.DEVICE_SUPPORT_ACTION.equals(key)
          || MapKeys.DEVICE_SUPPORT_MOVE.equals(key)
          || MapKeys.DEVICE_SUPPORT_EXPECT_SPEECH.equals(key)) {
        PreferenceAgent
            .setBooleanSharedData(context, key, (boolean) map.get(key));
      }
    }
    LogUtil.setIsPrintLog(PreferenceAgent.getBooleanSharedData(mContext,
        MapKeys.IS_DDMS_LOG));
    LogUtil.setIsSaveLogToFile(PreferenceAgent.getBooleanSharedData(mContext,
        MapKeys.IS_FILE_LOG));
  }

  /**
   * MAP 과의 통신 연결을 위한 필요한 값들을 Preference 에 저장하는 함수를 호출하는 함수입니다.
   * <br>
   * JS 로 부터 setMapSetting() 함수 호출을 통해 전달받은 데이터를 Preference 에 저장합니다.
   *
   * @param mapSettingJson JS 로 부터 setMapSetting() 함수 호출을 통해 전달받은 데이터
   */
  public void setGrpcSettingData(String mapSettingJson) {
    Gson gson = new Gson();
    try {
      MapSettingsDTO mapSetting = gson.fromJson(mapSettingJson,
          MapSettingsDTO.class);
      Hashtable<String, Serializable> map = new Hashtable<>();
      String serverIp = mapSetting.getServerIp();
      int serverPort = mapSetting.getServerPort();
      // MAP 과 통신을 위한 IP, PORT 설정
      map.put(MapKeys.SERVER_IP, serverIp);
      map.put(MapKeys.SERVER_PORT, serverPort);
      map.put(MapKeys.USE_TLS, mapSetting.isUseTls());
      map.put(MapKeys.PING_INTERVAL, mapSetting.getPingInterval());
      map.put(MapKeys.AUTH_TOKEN, mapSetting.getAuthToken());
      map.put(MapKeys.NOTIFYOBJECT_PREFIX, mapSetting.getNotifyObjectPrefix());
      map.put(MapKeys.DEVICE_TYPE, mapSetting.getDevice().getType());
      map.put(MapKeys.DEVICE_ID, mapSetting.getDevice().getId());
      map.put(MapKeys.DEVICE_CHANNEL, mapSetting.getDevice().getChannel());
      map.put(MapKeys.DEVICE_PREFERREDCHATBOT, mapSetting.getDevice()
          .getPreferredChatbot());
      map.put(MapKeys.DEVICE_VERSION, mapSetting.getDevice().getVersion());
      map.put(MapKeys.DEVICE_SUPPORT_RENDER_TEXT, mapSetting.getDevice()
          .getSupport().isSupportRenderText());
      map.put(MapKeys.DEVICE_SUPPORT_RENDER_CARD, mapSetting.getDevice()
          .getSupport().isSupportRenderCard());
      map.put(MapKeys.DEVICE_SUPPORT_SPEECH_SYNTHESIZER, mapSetting.getDevice
          ().getSupport().isSupportSpeechSynthesizer());
      map.put(MapKeys.DEVICE_SUPPORT_PLAY_AUDIO, mapSetting.getDevice()
          .getSupport().isSupportPlayAudio());
      map.put(MapKeys.DEVICE_SUPPORT_PLAY_VIDEO, mapSetting.getDevice()
          .getSupport().isSupportPlayVideo());
      map.put(MapKeys.DEVICE_SUPPORT_ACTION, mapSetting.getDevice()
          .getSupport().isSupportAction());
      map.put(MapKeys.DEVICE_SUPPORT_MOVE, mapSetting.getDevice().getSupport
          ().isSupportMove());
      map.put(MapKeys.DEVICE_SUPPORT_EXPECT_SPEECH, mapSetting.getDevice()
          .getSupport().isSupportExpectSpeech());
      map.put(MapKeys.DEVICE_TIMESTAMP_STRING, mapSetting.getDevice()
          .getTimestamp());
      map.put(MapKeys.DEVICE_META, mapSetting.getDevice().getMeta());
      // DDMS 로그 출력 유무
      map.put(MapKeys.IS_DDMS_LOG, mapSetting.isUseLog());
      setGrpcSettingData(mContext, map);
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
    }
  }

  /**
   * sendEvent 함수로 받은 json 형식의 eventStream data 를  MAP 으로 전달하는 함수입니다.
   * <br>
   * eventStream 을 MapEvent 형식으로 변환은 GrpcAgent 에서 처리합니다.
   *
   * @param mapEventJson json string 형식의 eventStream data
   */
  public boolean grpcSendEvent(String mapEventJson) {

    boolean isCallRequestGrpc;
    boolean isSuccess = callOpen(mContext);
    LogUtil.d(TAG, "openGrpc : isSuccess = " + isSuccess);
    if (isSuccess) {
      isCallRequestGrpc = callRequestGrpc(mapEventJson);
    } else {
      isCallRequestGrpc = false;
    }

    return isCallRequestGrpc;
  }

  /**
   * MAP 과의 통신을 종료시키는 함수를 호출해 주는 함수입니다.
   */
  public void shutDownGrcp() {
    callClose();
  }

  /**
   * MAP 과의 통신을 재연결하는 함수를 호출해 주는 함수입니다.
   */
  public boolean callReopenGrcp() {
    return callReOpen(mContext);
  }

  /**
   * MAP과 통신을 하는 gRPC 클래스인 GrpcAgent 를 초기화 하는 함수를 호출해주는 함수입니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @return 정상적으로 초기화된 경우 true
   */
  public boolean callOpen(Context context) {
    GrpcAgent.ChatLanguage lang = GrpcAgent.ChatLanguage.kor;
    if (PreferenceAgent.getIntSharedData(context,
        MapKeys.KEY_SELECT_LANGUAGE) == 1) {
      lang = GrpcAgent.ChatLanguage.eng;
    }
    return grpcAgent.open(lang);
  }

  /**
   * MAP과 통신을 하는 gRPC 클래스인 GrpcAgent 를 재 초기화 하는 함수입니다.
   * 이미 생성되어 있다면 GrpcAgent의 channel을 shutdown 후 초기화 합니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @return 정상적으로 초기화된 경우 true
   */
  public boolean callReOpen(final Context context) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        grpcAgent.close();
        grpcAgent.init();
        mIsReturn = callOpen(context);
      }
    }).start();
    return mIsReturn;
  }

  /**
   * JS 로 부터 전달받은 JSON 형식의 eventStream 을 MAP 에 전달할 때 호출하는 함수입니다.
   *
   * @param eventStreamJson JS 로 부터 전달받은 JSON 형식의 eventStream
   */
  public boolean callRequestGrpc(final String eventStreamJson) {
    boolean isRequestGrpc;

    if (callOpen(mContext)) {
      isRequestGrpc = grpcAgent.requestGrpc(eventStreamJson);
    } else {
      isRequestGrpc = false;
    }

    return isRequestGrpc;
  }

  /**
   * MAP 과의 통신을 종료시키기 위해 GrpcAgent의 channel을 shutdown 시키는 함수입니다.
   */
  public void callClose() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        if (grpcAgent != null) {
          grpcAgent.close();
        }
      }
    }).start();
  }

  /**
   * Microphone 을 통해 발화된 Voice data 를 MAP 에 전달하는 함수입니다.
   *
   * @param voice Microphone 을 통해 취득된 Voice data
   */
  public void callRequestVoiceResult(byte[] voice) {
    grpcAgent.requestVoiceResult(voice);
  }

  /**
   * Camera, Gallery 를 통해 취득된 Image data 를 MAP 에 전달하는 함수입니다.
   *
   * @param imageData Camera, Gallery 를 통해 취득된 Image data
   */
  public void callRequestImageResult(byte[] imageData) {
    grpcAgent.requestImageResult(imageData);
  }

  /**
   * Voice data, Image data 의 streamData 를 모두 전송한 후 MAP 으로 부터 Recognized
   * operation 을 수신하면 전송시의 streamId 에 대하여 streamEnd 를 MAP 에게 전송하기 위한 함수입니다.
   *
   * @param streamId streamData 전송시의 Id 값
   */
  public void callRequestStreamEnd(String streamId) {
    grpcAgent.requestStreamEnd(streamId);
  }

  /**
   * MAP 에 Ping 을 요청하는 타이머를 start 시킵니다.
   */
  public void startPingProcess() {
    if (grpcAgent.isGrpcOpen()) {
      grpcAgent.startPingProcess();
    }
  }

  /**
   * MAP 에 Ping 을 요청하는 타이머를 stop 시킵니다.
   */
  public void stopPingProcess() {
    grpcAgent.stopPingProcess();
  }

  /**
   * 현재 camera 의 상태를 JS 에게 전달하기 위해 호출되는 함수입니다.
   *
   * @param cameraEvent camera 의 event 값
   * @param hasReason 원인에 대한 값
   * @param cameraFailure 실패에 대한 값
   * @param message 기타 메시지
   * @param imageData thumbnail image data 와 image 의 url
   */
  public void processNotifyCameraStatus(String cameraEvent, boolean
      hasReason, String cameraFailure, String message, EtcDTO.ImageData
      imageData, int timerInterval) {

    // 카메라 관련 동작 실패인 경우이므로 저장된 이미지 파일이 있으면 삭제한다.
    if (hasReason == Const.HAS_REASON) {
      FileUtil.removeCameraFile();
    }

    if (cameraEvent == CameraEvent.CLOSED) {
      stopTimerForAction();
    } else if (cameraEvent == CameraEvent.OPEN_SUCCESS) {
      startTimerForAction(timerInterval * 1000);
    }
    // dto 생성 하여 JSON 변환 후 UI_TO_JS_CAMERASTATUS 로 전달
    CameraDTO.CameraStatus cameraStatus = new CameraDTO.CameraStatus();
    cameraStatus.imageData = new EtcDTO.ImageData();
    cameraStatus.setEventType(cameraEvent);
    cameraStatus.setHasReason(hasReason);
    cameraStatus.setFailure(cameraFailure);
    cameraStatus.setMessage(message);
    if (imageData != null) {
      float refVertexX = PreferenceAgent.getFloatSharedData(mContext, MapKeys
          .IMAGE_REFVERTEX_X);
      float refVertexY = PreferenceAgent.getFloatSharedData(mContext, MapKeys
          .IMAGE_REFVERTEX_Y);
      int width = PreferenceAgent.getIntSharedData(mContext, MapKeys
          .IMAGE_RESOLUTION_X);
      int height = PreferenceAgent.getIntSharedData(mContext, MapKeys
          .IMAGE_RESOLUTION_Y);
      if (width > 0 && height > 0) {
        imageData.setWidth(width);
        imageData.setHeight(height);
        imageData.setRefVertexX(refVertexX);
        imageData.setRefVertexY(refVertexY);
      }
      cameraStatus.setImageData(imageData);
    }
    String cameraStatusJson = GsonUtil.jsonStringFromObject(cameraStatus);
    runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_CAMERASTATUS,
        cameraStatusJson, null, 0, false, null, null, cameraStatus);
  }

  /**
   * 현재 gallery 의 상태를 JS 에게 전달하기 위해 호출되는 함수입니다.
   *
   * @param galleryEvent gallery 의 event 값
   * @param hasReason 원인에 대한 값
   * @param galleryFailure 실패에 대한 값
   * @param message 기타 메시지
   * @param imageData thumbnail image data 와 image 의 url
   */
  public void processNotifyGalleryStatus(String galleryEvent, boolean
      hasReason, String galleryFailure, String message, EtcDTO.ImageData
      imageData, int timerInterval) {
    if (galleryEvent == GalleryEvent.CLOSED) {
      stopTimerForAction();
    } else if (galleryEvent == GalleryEvent.OPEN_SUCCESS) {
      startTimerForAction(timerInterval * 1000);
    }
    // dto 생성 하여 JSON 변환 후 UI_TO_JS_GALLERYSTATUS 로 전달
    GalleryDTO.GalleryStatus galleryStatus = new GalleryDTO.GalleryStatus();
    galleryStatus.imageData = new EtcDTO.ImageData();
    galleryStatus.setEventType(galleryEvent);
    galleryStatus.setHasReason(hasReason);
    galleryStatus.setFailure(galleryFailure);
    galleryStatus.setMessage(message);
    if (imageData != null) {
      galleryStatus.setImageData(imageData);
    }
    String galleryStatusJson = GsonUtil.jsonStringFromObject(galleryStatus);
    runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_GALLERYSTATUS,
        galleryStatusJson, null, 0, false, null, null, galleryStatus);
  }

  /**
   * 현재 speaker 의 상태를 JS 에게 전달하기 위해 호출되는 함수입니다.
   *
   * @param speakerEvent speaker 의 event 값
   * @param speechSynthesizing 음성인식중인 경우 true
   * @param hasReason 원인에 대한 값
   * @param speakerFailure 실패에 대한 값
   * @param message 기타 메시지
   */
  public void processNotifySpeakerStatus(String speakerEvent, boolean
      speechSynthesizing, boolean hasReason, String speakerFailure, String
      message) {
    SpeakerDTO.SpeakerStatus speakerStatusDto = new SpeakerDTO.SpeakerStatus();
    speakerStatusDto.setEventType(speakerEvent);
    speakerStatusDto.setSpeechSynthesizing(speechSynthesizing);
    speakerStatusDto.setHasReason(hasReason);
    speakerStatusDto.setFailure(speakerFailure);
    speakerStatusDto.setMessage(message);
    String speakerStatusDtoJson = GsonUtil
        .jsonStringFromObject(speakerStatusDto);
    runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_SPEAKERSTATUS,
        speakerStatusDtoJson, null, 0, false, null, null, null);
  }

  /**
   * 현재 microphone 의 상태를 JS 에게 전달하기 위해 호출되는 함수입니다.
   *
   * @param microphoneEevent microphone 의 event 값
   * @param expectMode expect mode 인 경우 true
   * @param hasReason 원인에 대한 값
   * @param microphoneFailure 실패에 대한 값
   * @param message 기타 메시지
   */
  public void processNotifyMicrophoneStatus(String microphoneEevent, boolean
      expectMode, boolean hasReason, String microphoneFailure, String
      message) {
    // dto 생성 하여 JSON 변환 후 UI_TO_JS_MICROPHONESTATUS 로 전달
    MicrophoneDTO.MicrophoneStatus microphoneStatus =
        new MicrophoneDTO.MicrophoneStatus();
    microphoneStatus.setEventType(microphoneEevent);
    microphoneStatus.setExpectMode(expectMode);
    microphoneStatus.setHasReason(hasReason);
    microphoneStatus.setFailure(microphoneFailure);
    microphoneStatus.setMessage(message);
    String macrophoneStatusJson = GsonUtil
        .jsonStringFromObject(microphoneStatus);
    runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_MICROPHONESTATUS,
        macrophoneStatusJson, null, 0, false, null, null, null);
  }

  /**
   * 마이크 시작을 위해 초기화하는 함수를 호출하는 함수입니다.
   * <br>
   * JS 로 부터 openMicrophone() 함수를 통해 전달받은 MicrophoneParam 값으로 초기화 합니다.
   *
   * @param microphoneParam JS 로 부터 openMicrophone() 함수를 통해 전달받은 MicrophoneParam 값
   * @return microphone 이 초기화 된 경우 true
   */
  public boolean processMicrophoneInit(MicrophoneDTO.MicrophoneParam
      microphoneParam) {
    setMicTimeout(microphoneParam.getTimoeoutInMilliseconds());
    String encoding = microphoneParam.getSpeechParam().getEncoding();
    int sampleRate = microphoneParam.getSpeechParam().getSampleRate();
    String lang = microphoneParam.getSpeechParam().getLang();
    String model = microphoneParam.getSpeechParam().getModel();
    // model, encoding 값이 없으면 마이크 사용을 하지 않는다.
    if (StringUtil.isEmptyString(model) || StringUtil.isEmptyString(encoding)) {
      return false;
    }
    Hashtable<String, Serializable> map = new Hashtable<>();
    map.put(BaseConst.MapKeys.SPEECH_ENCODING, encoding);
    map.put(BaseConst.MapKeys.SPEECH_SAMPLERATE, sampleRate);
    map.put(BaseConst.MapKeys.SPEECH_LANG, lang);
    map.put(BaseConst.MapKeys.SPEECH_MODEL, model);
    setGrpcSettingData(mContext, map);
    DeviceAgentManager.getInstance(mContext).callStopVoiceRecorderAgent();
    return DeviceAgentManager.getInstance(mContext)
        .callCreateVoiceRecorderInit(sampleRate, encoding, model);
  }

  /**
   * microphone 으로 사용자 발화를 시작시키는 함수를 호출하는 함수입니다.
   *
   * @param streamId JS 로 부터 sendEvent() 함수로 전달받은 eventStream 의 streamId
   * @param operationSyncId JS 로 부터 sendEvent() 함수로 전달받은 eventStream 의
   * operationSyncId
   */
  public void processMicrophoneStart(String streamId, String operationSyncId) {
    mCurrentSpeechStreamId = streamId;
    mCurrentSpeechOperationSyncId = operationSyncId;
    DeviceAgentManager.getInstance(mContext).callStartVoiceRecorderAgent
        (streamId, operationSyncId);
  }

  /**
   * microphone 을 멈추고 해제하는 함수입니다.
   * <br>
   * microphone 이 동작중에 강제로 중지된 경우 JS 에게 streamingStatus 를 전달합니다.
   * <br>
   * 현재 microphone 의 상태를 JS 에게 전달합니다.
   *
   * @param microphoneEvent microphone 의 event
   * @param microphoneFailure 실패한 값
   */
  public void processMicrophoneStop(String microphoneEvent,
      String microphoneFailure) {
    // Mic Stop 처리
    boolean isRunning = DeviceAgentManager.getInstance(mContext)
        .callStopVoiceRecorderAgent();
    if (isRunning && !StringUtil.isEmptyString(mCurrentSpeechStreamId)) {
      // 이벤트가 강제로 중지되었는가? obj1 : isBreak(boolean),
      // 이벤트의 끝인가? obj2 : isEnd(boolean)
      // 강제로 중지되는 경우이다.
      runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_STREAMSENT,
          mCurrentSpeechStreamId,
          mCurrentSpeechOperationSyncId, 0, false, null, true, false);
    }
    // JS 에게 현재 microphone 상태를 전달한다.
    processNotifyMicrophoneStatus(microphoneEvent, Const.EXPECT_MDOE,
        microphoneFailure != null ? Const.HAS_REASON : Const.HAS_NOT_REASON,
        microphoneFailure, Const.EMPTY_STRING);
  }

  /**
   * microphone 으로 발화한 byteStream 데이터를 MAP 에 전송하는 함수를 호출합니다.
   *
   * @param utterVoice microphone 으로 입력된 사용자 발화 데이터
   */
  public void processMicrophoneUtterData(byte[] utterVoice) {
    callRequestVoiceResult(utterVoice);
  }

  /**
   * MAP 으로 부터 byteStream 데이터를 수신한 경우 JS 에게 정보를 전달하기 위해 호출되는 함수입니다.
   *
   * @param receiveData MAP 으로 부터 수신한 byteStream 데이터
   * @param streamId MAP 에 요청했던 streamId
   * @param operationSyncId MAP 에 요청했던 operationSyncId
   * @param isBreak 강제로 중지된 경우 true
   * @param isEnd 이벤트의 끝인 경우 true
   */
  public void processReceiveStreamDirective(byte[] receiveData, String
      streamId, String operationSyncId, boolean isBreak, boolean isEnd) {
    // sendStreamingEvent() 함수로 StreamingObject 데이터 전송
    StreamingStatusDTO streamStatusDto = new StreamingStatusDTO();
    // 스트림 ID
    streamStatusDto.setStreamId(streamId);
    // 응답 동기화 ID
    streamStatusDto.setOperationSyncId(operationSyncId);
    // 이벤트가 강제로 중지되었는가?
    streamStatusDto.setBreak(isBreak);
    // 이벤트의 끝인가?
    streamStatusDto.setEnd(isEnd);
    // 스트리밍 데이터의 구체적인 형식
    streamStatusDto.setType(Streamingtype.BYTES);
    // 스트리밍의 크기
    streamStatusDto.setSize(receiveData == null ? 0 : receiveData.length);
    String receiveStreamJson = GsonUtil.jsonStringFromObject(streamStatusDto);
    runUiUpdater(UIUpdater.UI_TO_JS_RECEIVESTREAM, receiveStreamJson, null,
        0, false, null, null, null);
  }

  /**
   * camera, gallery 를 통해 캡춰된 image 의 byteStream 데이터를 MAP 에 전송하는 함수를
   * 호출합니다.
   *
   * @param imageData 캡춰된 이미지의 데이터
   */
  public void processImageData(byte[] imageData) {
    callRequestImageResult(imageData);
  }

  /**
   * TelephonyManager를 이용해서 Phone Number를 리턴합니다.
   */
  public String processGetPhoneNumber() {
    String phoneNumber = "";

    TelephonyManager mgr = (TelephonyManager) mContext
        .getSystemService(Context.TELEPHONY_SERVICE);
    try {

      @SuppressLint("MissingPermission") String tmpPhoneNumber = mgr
          .getLine1Number();
      phoneNumber = tmpPhoneNumber.replace("+82", "0");

    } catch (Exception e) {
      phoneNumber = "";
    }

    return phoneNumber;
  }

  public void processNotifyLocation(boolean hasReason, String failure,
      String message, Location locaiton) {
    LocationStatus locationStatus = new LocationStatus();
    locationStatus.setHasReason(hasReason);
    locationStatus.setFailure(failure);
    locationStatus.setMessage(message);
    if (locaiton != null) {
      locationStatus.setLocation(locaiton);
    }
    processNotifyLocation(locationStatus);
  }

  /**
   * JS 로 부터 호출된 getLocation() 함수를 통해 GPS 정보를 취득하고 이 데이터를 JS 에게 전달하는 함수입니다.
   *
   * @param locationStatus GPS 정보 객체
   */
  public void processNotifyLocation(LocationStatus locationStatus) {
    if (locationManager != null) {
      locationManager.removeUpdates(mLocationListener);
    }
    stopTimerForLocation();
    String locationStatusJson = GsonUtil.jsonStringFromObject(locationStatus);
    runUiUpdater(HandlerConst.UIUpdater.UI_TO_JS_LOCATIONSTATUS,
        locationStatusJson, null, 0, false, null, null, null);
  }

  /**
   * JS 로 부터 getLocation 함수를 호출받고 위치 정보를 수신하기 위해 GPS, NETWORK provider를 등록하는
   * 함수입니다. GPS 관련 권한이 없는 경우 JS 에게 관련된 응답을 전달하고 권한이 있는 경우 위치 정보를 수신하기 위한 매니저에
   * 리스너를 설정하고 리스너를 유지하기 위한 타이머를 구동합니다.
   */
  public void processLocation() {
    if (ActivityCompat
        .checkSelfPermission(mContext, permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED && ActivityCompat
        .checkSelfPermission(mContext, permission.ACCESS_COARSE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      LocationStatus locationStatus = new LocationStatus();
      locationStatus.setHasReason(Const.HAS_REASON);
      locationStatus.setFailure(LocationFailure.LOCATION_PERMISSION_DENIED);
      locationStatus.setMessage(Const.EMPTY_STRING);
      processNotifyLocation(locationStatus);
      return;
    }

    startTimerForLocation();
    if (locationManager == null) {
      locationManager = (LocationManager) mContext
          .getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * minTime : 전력소비를 줄이기 위해 시간을 설정합니다.
     * 위치 업데이트 시간이 minTime보다 짧지는 않겠지만,
     * 주어진 Provider의 구현과 실행중인 다른 app에서 요청한 업데이트 간격의 영향을 받기 때문에 더 커질수도 있습니다.
     *
     * minDistance : 값이 0인 경우 distance 기능은 해제됩니다.
     * 0보다 큰값으로 설정된 경우 Provider는 최소한 지정된 거리만큼 위치가 변경된 경우에만 app 에 위치정보 업데이트를
     * 보냅니다. minTime 과 같은 절전 기능은 아니지만, 지정된 값에 해당되는 거리마다 업데이트가 되므로 절전을 염두해 두어야
     * 합니다.
     */
    locationManager
        .requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0.1f,
            mLocationListener);
    locationManager
        .requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0.1f,
            mLocationListener);
  }

  public void getManualLocationInfo() {
    LogUtil.e(TAG, "getManualLocationInfo...");
    boolean getGpsLocation = false;
    double gpsLng = 0.0, gpsLat = 0.0;
    android.location.Location currentGpsLocation = null;
    try {
      currentGpsLocation = locationManager
          .getLastKnownLocation(LocationManager.GPS_PROVIDER);
    } catch (SecurityException e) {
      LogUtil.e(TAG, e.getMessage());
    }
    if (currentGpsLocation != null) {
      getGpsLocation = true;
      gpsLng = currentGpsLocation.getLongitude();
      gpsLat = currentGpsLocation.getLatitude();
      LogUtil.d(TAG, "[getLocation : GPS_PROVIDER]longitude=" + gpsLng +
          ", " + "latitude=" + gpsLat);
    }

    boolean getNetworkLocation = false;
    double networkLng = 0.0, networkLat = 0.0;
    android.location.Location currentNetworkLocation = null;
    try {
      currentNetworkLocation = locationManager
          .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    } catch (SecurityException e) {
      LogUtil.e(TAG, e.getMessage());
    }
    if (currentNetworkLocation != null) {
      getNetworkLocation = true;
      networkLng = currentNetworkLocation.getLongitude();
      networkLat = currentNetworkLocation.getLatitude();
      LogUtil.e(TAG, "[getLocation : NETWORK_PROVIDER]longitude=" + networkLng +
          ", latitude=" + networkLat);
    }

    LocationStatus locationStatus = new LocationStatus();
    locationStatus.setHasReason(Const.HAS_NOT_REASON);
    locationStatus.setFailure(Const.EMPTY_STRING);
    locationStatus.setMessage(Const.EMPTY_STRING);
    if (getGpsLocation) {
      locationStatus.getLocation().setLatitude((float) gpsLat);
      locationStatus.getLocation().setLongitude((float) gpsLng);
    } else if (getNetworkLocation) {
      locationStatus.getLocation().setLatitude((float) networkLat);
      locationStatus.getLocation().setLongitude((float) networkLng);
    } else {
      locationStatus.setHasReason(Const.HAS_REASON);
      locationStatus.setFailure(LocationFailure.LOCATION_TIMEOUT);
      locationStatus.setMessage(Const.EMPTY_STRING);
    }
    processNotifyLocation(locationStatus);
  }

  /**
   * 오디오의 현재 볼륨 상태(mute 또는 unmute)를 리턴합니다.
   *
   * @return 현재 볼륨이 0 보타 크면 false
   */
  public boolean getCurrentVolumeState() {
    boolean isMute = true;
    int currentVolume = ((AudioManager) mContext.getSystemService(Context
        .AUDIO_SERVICE)).getStreamVolume(AudioManager.STREAM_MUSIC);
    if (currentVolume > 0) {
      isMute = false;
    }
    return isMute;
  }

  /*
   * MAP 으로 부터 Play 수신 시 player 를 준비 시키고 이후에 directiveStream 으로
   * byteStream 이 수신되면 이 데이터를 AudioTrack 으로 재생합니다.
   */
  public void speakerPlayInit() {
    if (!DeviceAgentManager.getInstance(mContext).isExistAudioPlayAgent()) {
      // Audio play 준비 (PCM)
      boolean isMute = getCurrentVolumeState();
      processNotifySpeakerStatus(isMute ? SpeakerEvent.MUTE : SpeakerEvent.UNMUTE,
          false, Const.HAS_NOT_REASON, Const.EMPTY_STRING, Const.EMPTY_STRING);
        DeviceAgentManager.getInstance(mContext).callCreateAudioPlayAgent(
            PreferenceAgent.getIntSharedData(mContext, MapKeys.SPEECH_SAMPLERATE),
            PreferenceAgent
                .getStringSharedData(mContext, MapKeys.SPEECH_ENCODING));
    }
  }

  /**
   * 스피커가 초기화되어 있지 않으면 초기화를 하고 MAP 으로 부터 directiveStream 으로 받은 byteStream 을
   * 재생합니다.
   *
   * @param byteData 스피커로 재생될 Audio byteStream data
   */
  public void speakerPlay(byte[] byteData) {
    speakerPlayInit();
    DeviceAgentManager.getInstance(mContext).callPlayAudioPlayAgent(byteData);
  }

  /**
   * JS 에서 closeSpeaker() 함수를 호출하면 speaker 을 닫습니다.
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 speaker 을 닫을때 speakerClose() 함수가 호출될
   * 수 있습니다.
   */
  public void speakerClose() {
    // Speaker close
    DeviceAgentManager.getInstance(mContext).callReleaseAudioPlayAgent();
    processNotifySpeakerStatus(SpeakerEvent.CLOSED, false,
        Const.HAS_NOT_REASON, Const.EMPTY_STRING, Const.EMPTY_STRING);
  }

  /**
   * Camera 를 통해 캡춰된 이미지를 로드하여 MAP 에 전송하는 함수입니다.
   * <br>
   * Thread 로 동작되며 1회 전송 시 byteStream으로 10240 bytes 씩 전송합니다.
   * <br>
   * 이미지 데이터 전송 후 MAP 에 streamEnd 를 전송합니다.
   *
   * @param imagePath Camera 로 캡춰한 이미지의 경로
   * @param currentImageStreamId 이미지 데이터 전송 후 streamEnd 로 보내어질 streamId
   */
  public void loadImageForSend(String imagePath,
      final String currentImageStreamId) {
    Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
    if (bitmap == null) {
      LogUtil.e(TAG, "[loadImageForSend] : imagePath = " + imagePath);
      return;
    }
    ExifInterface originalExif = null;
    try {
      originalExif = new ExifInterface(imagePath);
    } catch (IOException e) {
      LogUtil.e(TAG, "IOException", e);
    }
    int degrees = CommUtil.getExifOrientation(originalExif);
    final Bitmap rotatedBitmap = CommUtil.rotate(bitmap, degrees);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
    final byte[] imageBytes = baos.toByteArray();
    /**
     * String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
     * imageString = imageString.replaceAll("\n", "");
     * imageString = "data:image/jpg;base64," + imageString;
     */
    new Thread(new Runnable() {
      @Override
      public void run() {
        boolean done = false;
        int loopCount = imageBytes.length / Const.IMAGE_TRANSFER_SIZE;
        int remainder = imageBytes.length % Const.IMAGE_TRANSFER_SIZE;
        if (remainder > 0) {
          loopCount += 1;
        }
        int index = 0;
        int startPosition = 0;
        int getLength = Const.IMAGE_TRANSFER_SIZE;
        int totalLength = imageBytes.length;
        File photo = null;
        if (Const.mIsMapImageSaveTest) {
          photo = new File(Environment.getExternalStorageDirectory(), "photo"
              + ".jpg");
          if (photo.exists()) {
            photo.delete();
          }
        }
        try {
          // 실제 전송한 byteArray 를 file 로 저장해서 확인하기 위함.
          FileOutputStream fos = null;
          byte[] savedBytes = null;
          if (Const.mIsMapImageSaveTest) {
            fos = new FileOutputStream(photo.getPath());
            savedBytes = new byte[totalLength];
          }
          while (index < loopCount) {
            startPosition = index * Const.IMAGE_TRANSFER_SIZE;
            if ((totalLength - startPosition) < Const.IMAGE_TRANSFER_SIZE) {
              getLength = totalLength - startPosition;
            }
            byte[] sendData = new byte[getLength];
            System.arraycopy(imageBytes, startPosition, sendData, 0,
                getLength);
            LogUtil.e(TAG, "[IMAGE SEND : Runnable]\n" + index + " : "
                + "position " + "= " + startPosition + " " + "/ " +
                totalLength + ", length = " + getLength + ", getSize = " +
                sendData.length);
            GrpcClient.getInstance(mContext).processImageData(sendData);
            if (Const.mIsMapImageSaveTest) {
              if (savedBytes != null) {
                System.arraycopy(sendData, 0, savedBytes, startPosition,
                    getLength);
              }
            }
            index++;
            if (index >= loopCount) {
              LogUtil.e(TAG, "[IMAGE SEND : STREAM END]\n" + "streamId = " +
                  currentImageStreamId);
              // 이미지 전송의 경우 byteStream 전송 끝나면 바로 streamEnd 전송한다.
              GrpcClient.getInstance(mContext).callRequestStreamEnd
                  (currentImageStreamId);
              if (rotatedBitmap != null) {
                rotatedBitmap.recycle();
              }
              break;
            }
          }
          if (Const.mIsMapImageSaveTest) {
            if (fos != null && savedBytes != null) {
              fos.write(savedBytes);
              fos.close();
            }
          }
        } catch (IOException e) {
        }
        // 이미지데이터를 MAP에 모두 전송하거나 Exception 발생했으면 이미지를 삭제한다.
        FileUtil.removeCameraFile();
      }
    }).start();
  }

  /**
   * gallery 를 호출하는 함수입니다.
   *
   * @param requestCode gallery 화면을 요청한 코드
   * @return gallery 화면 open 성공한 경우 true
   */
  public boolean callGalleryOpen(int requestCode) {
    boolean isResult = true;
    try {
      Intent intent = new Intent(Intent.ACTION_PICK);
      intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
      intent.setType("image/*");
      mActivity.startActivityForResult(intent, requestCode);
      mMultimediaType = MultiMediaType.GALLERY;
      mGalleryRequestCode = requestCode;
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
      isResult = false;
      mMultimediaType = MultiMediaType.NONE;
      mGalleryRequestCode = -1;
    }
    return isResult;
  }

  /**
   * requestCode 로 구분하여 gallery, camera, crop 화면을 닫는다.
   *
   * @param requestCode gallery, camera, crop 화면을 요청한 코드
   */
  public void callOpenedActivityClose(int requestCode) {
    mActivity.finishActivity(requestCode);
  }

  /**
   * camera preview 화면을 호출하는 함수입니다.
   *
   * @param requestCode preview 화면을 요청한 코드
   * @param cameraParam preview 화면을 초기화 하는데 사용될 파라미터값
   * @param frameResId 설정될 프레임 이미지. 0 인 경우 aar 의 디폴트 이미지
   * @param isApi21Over API Level 21 이상인 경우 true, 모든 API Level 에 호환되는 경우 false
   * @return preview 화면 open 시 true
   */
  public boolean callPreviewOpen(int requestCode, CameraDTO.CameraParam
      cameraParam, int frameResId, boolean isApi21Over) {
    boolean isResult = true;
    try {
      PreferenceAgent.setStringSharedData(mContext, MapKeys
          .IMAGE_CONFIRM_URL, cameraParam.getConfirmImageUrl());
      PreferenceAgent.setFloatSharedData(mContext,
          MapKeys.IMAGE_CONFIRM_RESIZE_RATE, cameraParam
              .getConfirmResizeRateOfHeight());
      PreferenceAgent.setFloatSharedData(mContext,
          MapKeys.IMAGE_OPACITY_RATE, cameraParam.getOpacity());

      mMultimediaType = MultiMediaType.CAMERA;
      mPreviewRequestCode = requestCode;
      Intent intent = null;
      // api level 에 따른 분기를 고려했으나, isApi21Over = false 인 경우 모든 api 에 대해 커버되므로
      // false 로 강제 설정한다.
      isApi21Over = false;
      if (isApi21Over) {
        intent = new Intent(mContext, CameraPreviewActivity.class);
      } else {
        intent = new Intent(mContext, CameraAllPreviewActivity.class);
      }
      intent.putExtra(Const.CAMERA_EXTRA_INFO, cameraParam);
      intent.putExtra(Const.CAMERA_EXTRA_FRAMEDRAW, frameResId);
      mActivity.startActivityForResult(intent, requestCode);
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
      isResult = false;
      mMultimediaType = MultiMediaType.NONE;
      mPreviewRequestCode = -1;
    }
    return isResult;
  }

  /**
   * capture 된 이미지를 crop 하는 화면을 호출하는 함수입니다.
   *
   * @param data preview 화면으로 부터 받은 intent 값.
   * @param requestCode crop 화면을 요청한 코드
   * @return crop 화면 open 시 true
   */
  public boolean callCropOpen(Intent data, int requestCode) {
    boolean isResult = true;
    try {
      mMultimediaType = MultiMediaType.CAMERA;
      mCropRequestCode = requestCode;
      data.setClass(mContext, CropImageActivity.class);
      mActivity.startActivityForResult(data, requestCode);
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
      isResult = false;
      mMultimediaType = MultiMediaType.NONE;
      mCropRequestCode = -1;
    }
    return isResult;
  }

  public void stopTimerTask(TIMER_KIND timer_Kind) {
    if (timer_Kind == TIMER_KIND.TIMER_ALL) {
      stopTimerForAction();
      stopTimerForMic();
    } else if (timer_Kind == TIMER_KIND.TIMER_MULTIMEDIA) {
      stopTimerForAction();
    } else {
      stopTimerForMic();
    }
  }

  /**
   * OpenCamera, OpenGallery 의 함수 호출로 전달받은 openInterval 값 * 1000 으로 타이머
   * Task 를 구동시키는 함수입니다.
   *
   * @param milliSecond CameraParam, GalleryParam 의 openInterval 값 * 1000
   */
  private void startTimerForAction(long milliSecond) {
    stopTimerForAction();
    if (milliSecond <= 0) {
      return;
    }
    mTimerTaskForAction = new TimerTaskForAction();
    mTimerAction = new Timer();
    mTimerAction.schedule(mTimerTaskForAction, milliSecond);
  }

  /**
   * Camera, Gallery 를 구동 시켰을 때 사용된 Timer, TimerTask 를 해제 시키는 함수입니다.
   */
  private void stopTimerForAction() {
    if (mTimerAction != null) {
      mTimerAction.cancel();
      mTimerAction = null;
    }
    if (mTimerTaskForAction != null) {
      mTimerTaskForAction.cancel();
      mTimerTaskForAction = null;
    }
  }

  /**
   * OpenMicrophone 함수로 부터 전달 받은 timeoutInMilliseconds 값을 가져오는 함수입니다.
   *
   * @return microphone 구동 타이머 시간
   */
  private long getMicTimeout() {
    return mMicTimeout;
  }

  /**
   * openMicrophone 함수로 부터 전달 받은 timeoutInMilliseconds 값을 로컬변수에 저장하는 함수입니다.
   *
   * @param milliSecond timeInMilliseconds 값
   */
  private void setMicTimeout(long milliSecond) {
    mMicTimeout = milliSecond;
  }

  /**
   * 위치 정보를 수신하는 리스너를 타이머로 구동 시키는 함수입니다.
   */
  public void startTimerForLocation() {
    mLocationTimeout = getLocationTimeout();
    stopTimerForLocation();
    mTimerTaskForLocation = new TimerTaskForLocation();
    mTimerLocation = new Timer();
    mTimerLocation.schedule(mTimerTaskForLocation, mLocationTimeout);
  }

  /**
   * 위치 정보를 수신하는 리스너를 구동 시 사용되었던 타이머, 타이머 Task 를 해제시키는 함수입니다.
   */
  private void stopTimerForLocation() {
    if (mTimerLocation != null) {
      mTimerLocation.cancel();
      mTimerLocation = null;
    }
    if (mTimerTaskForLocation != null) {
      mTimerTaskForLocation.cancel();
      mTimerTaskForLocation = null;
    }
  }

  /**
   * 위치 정보를 수신하는 리스너를 유지하는 타이머 시간을 가져오는 함수입니다.
   *
   * @return microphone 리스너 유지 타이머 시간
   */
  private long getLocationTimeout() {
    return mLocationTimeout;
  }

  /**
   * openMicrophone 의 함수 호출로 전달받은 MicrophoneParam 의 timeoutInMilliseconds 값으로
   * 타이머, 타이머 Task 를 구동시키는 함수입니다.
   */
  public void startTimerForMic() {
    mMicTimeout = getMicTimeout();
    stopTimerForMic();
    mTimerTaskForMic = new TimerTaskForMic();
    mTimerMic = new Timer();
    mTimerMic.schedule(mTimerTaskForMic, mMicTimeout);
  }

  /**
   * microphone 구동 시 사용되었던 타이머, 타이머 Task 를 해제시키는 함수입니다.
   */
  private void stopTimerForMic() {
    if (mTimerMic != null) {
      mTimerMic.cancel();
      mTimerMic = null;
    }
    if (mTimerTaskForMic != null) {
      mTimerTaskForMic.cancel();
      mTimerTaskForMic = null;
    }
  }

  /**
   * microphone 을 멈추고 해제하는 함수입니다.
   * <br>JS 에게 현재 microphone 의 상태를 전달해 줍니다.
   *
   * @param timerStop 타이머를 종료시켜야 되는 경우 true
   * @param event microphone 의 현재 이벤트
   * @param failure microphone 의 상태에 대한 메시지가 있으면 not null
   */
  public void microphoneRelese(boolean timerStop, String event, String
      failure) {
    if (timerStop) {
      stopTimerTask(TIMER_KIND.TIMER_MICROPHONE);
    }
    processMicrophoneStop(event, failure);
  }

  /**
   * UI 에 데이터를 전송하기 위해서 호출하는  함수입니다.
   *
   * @param updateCase 업데이트 종류
   * @param arg1 String variable
   * @param arg2 String variable
   * @param arg3 Integer variable
   * @param arg4 boolean variable
   * @param arg5 byte[] variable
   * @param obj1 Object variable
   * @param obj2 Object variable
   */
  private void runUiUpdater(int updateCase, String arg1, String arg2, int
      arg3, boolean arg4, byte[] arg5, Object obj1, Object obj2) {
    ExecuteUpdater updater = ExecuteUpdater.getInstance();
    updater.updateRun(updateCase, arg1, arg2, arg3, arg4, arg5, obj1, obj2);
  }

  public enum TIMER_KIND {
    TIMER_MULTIMEDIA,
    TIMER_MICROPHONE,
    TIMER_ALL
  }

  /**
   * Singletone instance 를 생성하는 클래스 입니다.
   */
  private static final class SingletonHolder {

    static final GrpcClient INSTANCE = new GrpcClient();
  }

  /**
   * Gallery, Camera Preview 호출된 후 동작되는 타이머 Task 클래스 입니다.
   */
  public class TimerTaskForAction extends TimerTask {

    public TimerTaskForAction() {
    }

    /**
     * Action 타이머가 Expired 되면 notifyMicrophoneStatus() 함수 호출합니다.
     * <p>
     * Camera 의 경우 notifyCameraStatus 함수를 호출하고, Gallery 의 경우
     * notifyGalleryStatus 함수를 호출하여 JS 에게 현재 상태를 알려줍니다.<br>
     * event : CLOSED<br>
     * reason : XXXX_TIMER_EXPIRED<br>
     * 또한, 연동된 다른 Activity들이 보여지고 있으면 해당 Activity 를 종료 시켜줍니다.
     * </p>
     */
    @Override
    public void run() {
      // Camera Preview 가 구동중
      if (mMultimediaType == MultiMediaType.CAMERA) {
        mActivity.finishActivity(mPreviewRequestCode);
        mActivity.finishActivity(mCropRequestCode);
        GrpcClient.getInstance(mContext)
            .processNotifyCameraStatus(CameraEvent.CLOSED,
                Const.HAS_REASON, CameraFailure.CAMERA_TIMER_EXPIRED, Const
                    .EMPTY_STRING, null, 0);
      }
      // Gallery 가 구동중
      else if (mMultimediaType == MultiMediaType.GALLERY) {
        GrpcClient.getInstance(mContext)
            .callOpenedActivityClose(mGalleryRequestCode);
        GrpcClient.getInstance(mContext)
            .processNotifyGalleryStatus(GalleryEvent.CLOSED,
                Const.HAS_REASON, GalleryFailure.GALLERY_TIMER_EXPIRED, Const
                    .EMPTY_STRING, null, 0);
      }
    }
  }

  /**
   * Microphone open 호출된 후 동작되는 타이머 Task 클래스 입니다.
   */
  public class TimerTaskForMic extends TimerTask {

    public TimerTaskForMic() {
    }

    /**
     * Microphone 타이머가 Expired 되면 notifyMicrophoneStatus() 함수 호출합니다.<br>
     * event : CLOSE_SUCCESS<br>
     * reason : MICROPHONE_TIMER_EXPIRED
     */
    @Override
    public void run() {
      // 발화 타이머 Expire 시 Microphone을 닫고 notifyMicrophoneStatus() 함수 호출하여 JS에게
      // 현재 Microphone 의 상태를 알려줍니다.
      // reason 에는 MICROPHONE_TIMER_EXPIRED
      microphoneRelese(false, MicrophoneEvent.CLOSE_SUCCESS,
          MicrophoneFailure.MICROPHONE_TIMER_EXPIRED);
    }
  }

  /**
   * 위치 정보를 수신하는 리스너를 유지시키는 타이머 Task 클래스 입니다. Timeout 시 JS 에게 TIMEOUT 메시지를
   * 전달합니다.
   */
  public class TimerTaskForLocation extends TimerTask {

    public TimerTaskForLocation() {

    }

    @Override
    public void run() {
      LogUtil.d(TAG, "* TimerTaskForLocation...timeout : 5sec.");
      // TIMEOUT 시 수동으로 위치 구하도록 한다.
      getManualLocationInfo();
    }
  }

  /**
   * 위치 정보를 수신할 수 있는 리스너입니다. 위치 정보를 수신하면 JS 에게 수신받은 위치 정보를 전달합니다.
   */
  public final LocationListener mLocationListener = new LocationListener() {

    @Override
    public void onLocationChanged(android.location.Location location) {

      double longitude = location.getLongitude(); //경도
      double latitude = location.getLatitude();   //위도
      double altitude = location.getAltitude();   //고도
      float accuracy = location.getAccuracy();    //정확도
      String provider = location.getProvider();   //위치제공자

      String info = "\nprovider : " + provider + "\nlongitude : " + longitude +
          "\nlatitude : " + latitude + "\n고도 : " + altitude + "\n정확도 : "
          + accuracy;
      LogUtil.d(TAG, info);

      LocationStatus locationStatus = new LocationStatus();
      locationStatus.setHasReason(Const.HAS_NOT_REASON);
      locationStatus.setFailure(Const.EMPTY_STRING);
      locationStatus.setMessage(Const.EMPTY_STRING);
      locationStatus.getLocation().setLatitude((float) latitude);
      locationStatus.getLocation().setLongitude((float) longitude);
      // time of fix, in milliseconds since January 1, 1970.
      // 윤기현 전무님 요청 : latitude, longitude 값만 채워서 전달
      // RefreshAt 은 채우지 않음
      processNotifyLocation(locationStatus);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
      // 변경시
      LogUtil.d(TAG, "onStatusChanged, provider:" + provider + ", "
          + "status:" + status + " ,Bundle:" + extras);
    }

    @Override
    public void onProviderEnabled(String provider) {
      // Enabled시
      LogUtil.e(TAG, "onProviderEnabled, provider:" + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
      // Disabled시
      LogUtil.e(TAG, "onProviderDisabled, provider:" + provider);
    }
  };

}
