package ai.maum.m2u.cdk.grpclib.chatbot.grpc;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall;
import io.grpc.ForwardingClientCallListener;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import java.util.logging.Logger;

/**
 * io.grpc 라이브러리의 인터페이스를 구현한 클래스 입니다. gRPC channel 의 intercept 에 추가되는 객체를 만드는데 사용됩니다. *
 */

public class GrpcHeaderClientInterceptor implements ClientInterceptor {

  private static final String TAG =
      GrpcHeaderClientInterceptor.class.getSimpleName();
  private static final Logger logger =
      Logger.getLogger(GrpcHeaderClientInterceptor.class.getName());
  private static Metadata.Key<String> sessionKey =
      Metadata.Key.of("session", Metadata.ASCII_STRING_MARSHALLER);
  IGrpcMetadataGetter mg_;

  /**
   * ClientCall 객체를 생성하여 리턴시키는 함수 입니다. gRPC 가이드를 따릅니다.
   *
   * @param method MmethodDescriptor
   * @param callOptions CallOptions
   * @param next Channel
   * @param <ReqT> type parameter
   * @param <RespT> type parameter
   * @return ClientCall object
   */
  @Override
  public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(
      MethodDescriptor<ReqT, RespT> method,
      CallOptions callOptions, Channel next) {
    return new ForwardingClientCall
        .SimpleForwardingClientCall<ReqT, RespT>
        (next.newCall(method, callOptions)) {
      @Override
      public void start(ClientCall.Listener<RespT> responseListener,
          Metadata headers) {

        if (mg_ != null) {
          mg_.onMetaData(headers);
        }

        super.start(new ForwardingClientCallListener
            .SimpleForwardingClientCallListener<RespT>(responseListener) {
          @Override
          public void onHeaders(Metadata headers) {
            /**
             * if you don't need receive header from server,
             * you can use {@link io.grpc.stub.MetadataUtils attachHeaders}
             * directly to send header
             */
            //logger.info("header received from server:" + headers);
            super.onHeaders(headers);
          }
        }, headers);
      }
    };
  }

  /**
   * GrpcMetadataGetter에 대한 인터페이스를 set 하는 함수입니다.
   *
   * @param mg GrpcMetadataGetter에 대한 인터페이스
   */
  public void setMetadataGetter(IGrpcMetadataGetter mg) {
    mg_ = mg;
  }
}
