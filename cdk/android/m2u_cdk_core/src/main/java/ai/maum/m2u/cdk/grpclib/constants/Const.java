package ai.maum.m2u.cdk.grpclib.constants;

import android.os.Environment;

/**
 * 다른 클래스에서 공통으로 사용하고 있는 상수값을 정의한 클래스 입니다.
 */

public class Const {

  // WebView 의 디버깅 설정 유무
  public static final boolean IS_WEBVIEW_DEBUGGING = true;
  // microphone 의 Timer 구동시 timeout 디폴트 시간
  public static final int MULTIMEDIA_TIMEOUT = 10 * 1000; // sec
  // Location 의 Timer 구동시 timeout 디폴트 시간
  public static final int LOCATION_TIMEOUT = 5 * 1000; // sec
  // JS 에게 전달 될 thumbnail 이미지의 최대 크기
  public static final int IMAGEVIEW_AREA_WIDTH = 270;
  // MAP 에 image data 전송 시 패킷 크기 입니다.
  public static final int IMAGE_TRANSFER_SIZE = 10240;
  // camera preview/crop 화면에서 사용되는 intent extra name
  public static final String CAMERA_EXTRA_INFO = "camera_extra_name";
  public static final String CAMERA_EXTRA_FILE = "camera_extra_file";
  public static final String CAMERA_EXTRA_MESSAGE = "camera_extra_message";
  public static final String CAMERA_EXTRA_FAILURE = "camera_extra_failure";
  public static final String CAMERA_EXTRA_RESULT = "camera_extra_result";
  public static final String CAMERA_EXTRA_FRAMEDRAW = "camera_extra_framedraw";
  public static final String CAMERA_IMAGE_SAVE_PATH = Environment.getExternalStorageDirectory()
      .getAbsoluteFile() + "/path/";
  public static final String CAMERA_IMAGE_SAVE_FILE = "pic.jpg";

  // onActivityResult 함수에서 체크하는 result 값
  public static final int RESULT_SUCCESS = 1;
  public static final int RESULT_FAILURE = 0;
  public static final int RESULT_RETRY = 2;
  /**
   * ITT, ITS 인 경우 Image data(byteArray) 전송 후
   * 전송된 Image data 를 이미지 파일로 저장하여 정상적으로
   * 보여지는지 확인하기 위한 flag 입니다.
   */
  public static final boolean mIsMapImageSaveTest = false;
  /**
   * Cropped image를 상,하,좌,우 확장 시키는 flag 입니다.
   */
  public static final boolean mIsCropImageExtend = true;
  /**
   * Audio record data : for save test
   */
  public static final boolean mIsSaveAudioRecord = false;
  public static final String EMPTY_STRING = "";
  public static final String JAVA_SCRIPT_START = "javaScript:";

  public static final boolean HAS_REASON = true;
  public static final boolean HAS_NOT_REASON = false;

  public static final boolean RESULT_BOOL_SUCCESS = true;
  public static final boolean RESULT_BOOL_FAIL = false;

  public static final boolean EXPECT_MDOE = true;
  public static final boolean EXPECT_NOT_MDOE = false;

  /**
   * 현재 camera, gallery 등이 실행되고 있는지 체크하기 위한 식별자들로 구선된 클래스 입니다.
   */
  public static class MultiMediaType {

    public static final int NONE = 0;
    public static final int NORMAL = 1;
    public static final int CAMERA = 2;
    public static final int GALLERY = 3;
  }

  /**
   * 기타 상수값들을 정의한 클래스 입니다.
   */
  public static class EtcInfo {

    // LogUtil 로 로그 출력 여부를 결정하는 변수입니다. 로그 출력 시 true
    public static final boolean isDDMSLog = true;
    // LogUtil 로 출력된 로그를 파일에 저장할지 여부를 결정하는 변수입니다. 로그 저장 시 true
    public static final boolean isFileLog = false;
  }
}
