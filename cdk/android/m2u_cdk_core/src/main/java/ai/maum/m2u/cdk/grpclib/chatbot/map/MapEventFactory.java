package ai.maum.m2u.cdk.grpclib.chatbot.map;

import ai.maum.m2u.cdk.grpclib.chatbot.common.PreferenceAgent;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst.DefaultValue;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst.MapKeys;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO.MapEventInfo;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import ai.maum.m2u.cdk.grpclib.utils.StringUtil;
import android.content.Context;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Struct;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import maum.m2u.common.DeviceOuterClass.Device;
import maum.m2u.common.DeviceOuterClass.Device.Capability;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.EventStream.EventContext;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.Map.PingRequest;
import maum.m2u.map.Map.StreamEnd;

/**
 * MAP 에 전송될 MapEvent에 필요한 객체들을 생성하는 함수들로 구성된 클래스 입니다.
 * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
 */

public class MapEventFactory {

  private static final String TAG = MapEventFactory.class.getSimpleName();

  /**
   * Preference 에 저장된 JS 로 부터 전달받은 Device 정보를 로드해서
   * Device 객체를 생성하는 함수입니다.
   * Timestamp는 현재 단말 시간을 기준으로 생성합니다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @return 생성된 Device 객체
   */
  public static Device getDeviceMeta(Context context) {
    String deviceId = PreferenceAgent
        .getStringSharedData(context, MapKeys.DEVICE_ID);
    String deviceType = PreferenceAgent
        .getStringSharedData(context, MapKeys.DEVICE_TYPE);
    String deviceVersion = PreferenceAgent
        .getStringSharedData(context, MapKeys.DEVICE_VERSION);
    String deviceChannel = PreferenceAgent
        .getStringSharedData(context, MapKeys.DEVICE_CHANNEL);
    String devicePreferredChatbot = PreferenceAgent
        .getStringSharedData(context, MapKeys.DEVICE_PREFERREDCHATBOT);
    boolean supportRenderText = PreferenceAgent
        .getBooleanSharedData(context, MapKeys.DEVICE_SUPPORT_RENDER_TEXT);
    boolean supportRenderCard = PreferenceAgent
        .getBooleanSharedData(context, MapKeys.DEVICE_SUPPORT_RENDER_CARD);
    boolean supportSpeechSynthesizer = PreferenceAgent
        .getBooleanSharedData(context,
            MapKeys.DEVICE_SUPPORT_SPEECH_SYNTHESIZER);
    boolean supportPlayAudio = PreferenceAgent
        .getBooleanSharedData(context, MapKeys.DEVICE_SUPPORT_PLAY_AUDIO);
    boolean supportPlayVideo = PreferenceAgent
        .getBooleanSharedData(context, MapKeys.DEVICE_SUPPORT_PLAY_VIDEO);
    boolean supportAction = PreferenceAgent
        .getBooleanSharedData(context, MapKeys.DEVICE_SUPPORT_ACTION);
    boolean supportMove = PreferenceAgent
        .getBooleanSharedData(context, MapKeys.DEVICE_SUPPORT_MOVE);
    boolean supportExpectSpeech = PreferenceAgent
        .getBooleanSharedData(context, MapKeys.DEVICE_SUPPORT_EXPECT_SPEECH);
    String meta = PreferenceAgent
        .getStringSharedData(context, MapKeys.DEVICE_META);
    // KST+9 : +9 만 의미가 있습니다. KST+9, KST 는 같은 의미입니다.
    String timezone = DefaultValue.DEFAULT_DEVICE_TIMEZONE;
    Struct metaValue = null;
    if (!StringUtil.isEmptyString(meta)) {
      try {
        Struct.Builder builder = Struct.newBuilder();
        JsonFormat.parser().merge(meta, builder);
        metaValue = builder.build();
      } catch (InvalidProtocolBufferException e) {
        e.printStackTrace();
      }
    }
    // .setMeta(metaValue) - meta : Struct 에 대한 처리 필요할 수 있음.
    Device deviceMeta = Device.newBuilder()
        .setId(deviceId)
        .setType(deviceType)
        .setVersion(deviceVersion)
        .setChannel(deviceChannel)
        .setSupport(Capability.newBuilder()
            .setSupportRenderText(supportRenderText)
            .setSupportRenderCard(supportRenderCard)
            .setSupportSpeechSynthesizer(supportSpeechSynthesizer)
            .setSupportPlayAudio(supportPlayAudio)
            .setSupportPlayVideo(supportPlayVideo)
            .setSupportAction(supportAction)
            .setSupportMove(supportMove)
            .setSupportExpectSpeech(supportExpectSpeech)
            .build())
        .setMeta(metaValue)
        // 현재 단말 시간에 대한 timestamp.
        .setTimestamp(getCurrentTimeStamp())
        .setTimezone(timezone)
        .build();
    try {
      String deviceMetaJson = JsonFormat.printer().print(deviceMeta);
      LogUtil.e(TAG, "[getDeviceMeta] : deviceJson = \n" + deviceMetaJson);
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
      LogUtil.e(TAG, e.getMessage());
    }
    return deviceMeta;
  }

  /**
   * Device 객체를 생성한 후 EventContext 로 빌드하여 빌드된 결과를 리턴해주는 함수입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   */
  public static EventContext getDevice(Context context) {
    return EventContext.newBuilder().setDevice(getDeviceMeta(context)).build();
  }

  /**
   * 현재 단말 시간을 기준으로 TImestamp 객체를 생성하는 함수입니다.
   *
   * @return Timestamp 객체
   */
  public static Timestamp getCurrentTimeStamp() {
    long currentTimeMillis = System.currentTimeMillis();
    return Timestamp.newBuilder()
        .setSeconds(currentTimeMillis / 1000)
        .setNanos((int) (currentTimeMillis % 1000 * 1000000))
        .build();
  }

  /**
   * MAP 에 전송될 Ping 객체를 생성하는 함수입니다.
   * Device, Timestamp 객체를 포함하고 있습니다.
   *
   * @param context 함수를 호출한 클래스의 context
   */
  // 새버전 proto 에서는 user 객체가 제거됨
  public static PingRequest getPingRequest(Context context) {
    PingRequest pingRequest = PingRequest.newBuilder()
        .setDevice(getDeviceMeta(context))
        .setPingAt(getCurrentTimeStamp())
        .build();
    return pingRequest;
  }

  /**
   * Microphone을 통한 voice data 또는 Camera를 통한 Image data 를 MAP 에 전송하기 위해
   * MapEvent를 생성하는 함수입니다.
   *
   * @param byteData MAP 에 전송될 byteStream data
   * @return MapEvent 객체
   */
  public static MapEvent getByteParam(byte[] byteData) {
    MapEvent mapEvent = MapEvent.newBuilder()
        .setBytes(ByteString.copyFrom(byteData)).build();
    LogUtil.e(TAG, "getByteParam : " + mapEvent.toString());
    return mapEvent;
  }

  /**
   * MAp 에 전송될 StreamEnd 객체를 생성하는 함수입니다.
   * Timestamp 객체를 포함하고 있습니다.
   *
   * @param currentStreamId StreamEnd 에 해당되는 StreamId
   * @return MapEvent 객체
   */
  public static MapEvent getStreamEndParam(String currentStreamId) {
    StreamEnd streamEnd = StreamEnd.newBuilder()
        .setStreamId(currentStreamId)
        .setEndAt(getCurrentTimeStamp())
        .build();
    MapEvent mapEvent = MapEvent.newBuilder().setStreamEnd(streamEnd).build();
    LogUtil.e(TAG, "getStreamEndParam : " + mapEvent.toString());
    return mapEvent;
  }

  /**
   * mapEventInfo를 이용하여 Location 객체를 생성하는 함수입니다.
   *
   * @param mapEventInfo
   * @return
   */
  public static EventContext getLocationMeta(MapEventInfo mapEventInfo) {
    Location locationMeta = Location.newBuilder()
        .setLatitude(mapEventInfo.getLatitude())
        .setLongitude(mapEventInfo.getLongitude())
        .setLocation(mapEventInfo.getLocation())
        .setRefreshAt(getCurrentTimeStamp())
        .build();
    try {
      String locationMetaJson = JsonFormat.printer().print(locationMeta);
      LogUtil.e(TAG, "[getLocationMeta] : locationMetaJson = \n" + locationMetaJson);
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
      LogUtil.e(TAG, e.getMessage());
    }
    return EventContext.newBuilder().setLocation(locationMeta).build();
  }
}
