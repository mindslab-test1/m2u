package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * 기타 데이터 전송 객체들에 대한 클래스 입니다.
 * <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */

public class EtcDTO {

  /**
   * streamingtype 의 종류를 구성하고 있는 클래스 입니다.
   */
  public static class Streamingtype {

    public static final String UNKNOWN = "UNKNOWN";
    public static final String BYTES = "BYTES";
    public static final String TEXT = "TEXT";
    public static final String STRUCT = "STRUCT";
  }

  /**
   * timestamp 정보를 구성하고 있는 클래스 입니다.
   */
  public static class Timestamp {

    // Represents seconds of UTC time since Unix epoch
    // 1970-01-01T00:00:00Z. Must be from 0001-01-01T00:00:00Z to
    // 9999-12-31T23:59:59Z inclusive.
    public static final long seconds = 0;
    public static final int nanos = 0;

    public Timestamp() {
    }

    public long getSeconds() {
      return seconds;
    }

    public void setSeconds(long seconds) {
      this.seconds = seconds;
    }

    public int getNanos() {
      return nanos;
    }

    public void setNanos(int nanos) {
      this.nanos = nanos;
    }
  }

  /**
   * image 해상도 정보를 구성하고 있는 클래스 입니다.
   */
  public static class Resolution implements Parcelable {

    public static final Parcelable.Creator<Resolution> CREATOR = new Parcelable.Creator<Resolution>() {

      @Override
      public Resolution[] newArray(int size) {
        return new Resolution[size];
      }

      @Override
      public Resolution createFromParcel(Parcel source) {
        return new Resolution(source);
      }
    };
    private int min_x = 0;
    private int min_y = 0;
    private int max_x = 0;
    private int max_y = 0;

    public Resolution() {
    }

    public Resolution(Parcel in) {
      readFromParcel(in);
    }

    public int getMin_x() {
      return min_x;
    }

    public void setMin_x(int min_x) {
      this.min_x = min_x;
    }

    public int getMin_y() {
      return min_y;
    }

    public void setMin_y(int min_y) {
      this.min_y = min_y;
    }

    public int getMax_x() {
      return max_x;
    }

    public void setMax_x(int max_x) {
      this.max_x = max_x;
    }

    public int getMax_y() {
      return max_y;
    }

    public void setMax_y(int max_y) {
      this.max_y = max_y;
    }

    @Override
    public int describeContents() {
      return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      dest.writeInt(this.min_x);
      dest.writeInt(this.min_y);
      dest.writeInt(this.max_x);
      dest.writeInt(this.max_y);
    }

    private void readFromParcel(Parcel in) {
      this.min_x = in.readInt();
      this.min_y = in.readInt();
      this.max_x = in.readInt();
      this.max_y = in.readInt();
    }
  }

  /**
   * image 의 정보를 구성하고 있는 클래스 입니다.
   */
  public static class ImageData {

    // 이미지의 해상도 정보에 대한 클래스의 객체
    public static final Resolution res = new Resolution();
    // 이미지 URL 정보
    private String imageUrl = Const.EMPTY_STRING;
    // capture 된 image의 Thumbnail data(base64 Encoded)
    private String thumbnail = Const.EMPTY_STRING;
    // 이미지 전체 가로 (px)
    private int width = 0;
    // 이미지 전체 세로 (px)
    private int height = 0;
    // App 에서 보내줄 guide 비율의 가로 값
    private float ref_vertex_x = 0.0f;
    // App 에서 보내줄 guide 비율의 세로 값
    private float ref_vertex_y = 0.0f;

    public ImageData() {
    }

    public Resolution getRes() {
      return res;
    }

    public void setRes(Resolution res) {
      this.res = res;
    }

    public String getImageUrl() {
      return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
      this.imageUrl = imageUrl;
    }

    public String getThumbnail() {
      return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
      this.thumbnail = thumbnail;
    }

    public int getWidth() {
      return width;
    }

    public void setWidth(int width) {
      this.width = width;
    }

    public int getHeight() {
      return height;
    }

    public void setHeight(int height) {
      this.height = height;
    }

    public float getRefVertexX() {
      return ref_vertex_x;
    }

    public void setRefVertexX(float ref_vertex_x) {
      this.ref_vertex_x = ref_vertex_x;
    }

    public float getRefVertexY() {
      return ref_vertex_y;
    }

    public void setRefVertexY(float ref_vertex_y) {
      this.ref_vertex_y = ref_vertex_y;
    }
  }
}
