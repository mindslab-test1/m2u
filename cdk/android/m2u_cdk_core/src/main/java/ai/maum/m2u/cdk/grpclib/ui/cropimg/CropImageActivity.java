package ai.maum.m2u.cdk.grpclib.ui.cropimg;

import ai.maum.m2u.cdk.grpclib.R;
import ai.maum.m2u.cdk.grpclib.chatbot.common.PreferenceAgent;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst.MapKeys;
import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.ui.cropimg.component.CropUtil;
import ai.maum.m2u.cdk.grpclib.ui.cropimg.component.impl.IImage;
import ai.maum.m2u.cdk.grpclib.ui.cropimg.component.impl.IImageList;
import ai.maum.m2u.cdk.grpclib.ui.cropimg.component.manager.ImageManager;
import ai.maum.m2u.cdk.grpclib.utils.FileUtil;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.support.v4.content.PermissionChecker;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * camera 를 통해 취득된 image 를 crop 처리하는 클래스 입니다.
 */

public class CropImageActivity extends CropMonitoredActivity {

  /**
   * 디버깅 태그
   */
  public static final String TAG = CropImageActivity.class
      .getSimpleName();

  public static final String RETURN_DATA = "return-data";
  public static final String RETURN_DATA_AS_BITMAP = "data";
  public static final String ACTION_INLINE_DATA = "inline-data";
  public static final int NO_STORAGE_ERROR = -1;
  public static final int CANNOT_STAT_ERROR = -2;

  private final Handler mHandler = new Handler();
  private boolean mSaving;  // Whether the "save" button is already clicked.
  // These are various options can be specified in the intent.
  private Bitmap.CompressFormat mOutputFormat = Bitmap.CompressFormat.JPEG;
  // only used with mSaveUri
  private Uri mSaveUri = null;
  private ImageView mImageView;
  private ContentResolver mContentResolver;
  private Bitmap mBitmap;
  private IImageList mAllImages;
  private IImage mImage;
  private File mFile;
  private Typeface mFontNanum;

  private int mWidth, mHeight;
  private int mFrameLeft, mFrameTop, mFrameRight, mFrameWidth, mFrameHeight;
  private Activity mActivity;
  private LinearLayout mLL_left_edge;
  private CropImageActivity.GuideLineBlinkThread
      mGuideLineBlinkThread = null;

  public static void showStorageToast(Activity activity) {
    showStorageToast(activity, calculatePicturesRemaining());
  }

  public static void showStorageToast(Activity activity, int remaining) {
    String noStorageText = null;
    if (remaining == NO_STORAGE_ERROR) {
      String state = Environment.getExternalStorageState();
      if (state.equals(Environment.MEDIA_CHECKING)) {
        noStorageText = activity.getString(R.string.preparing_sd);
      } else {
        noStorageText = activity.getString(R.string.no_storage);
      }
    } else if (remaining < 1) {
      noStorageText = activity.getString(R.string.not_enough_space);
    }
    if (noStorageText != null) {
      Toast.makeText(activity, noStorageText, Toast.LENGTH_SHORT).show();
    }
  }

  @SuppressWarnings("deprecation")
  public static int calculatePicturesRemaining() {
    try {
      String storageDirectory =
          Environment.getExternalStorageDirectory().toString();
      StatFs stat = new StatFs(storageDirectory);
      float remaining = ((float) stat.getAvailableBlocks()
          * (float) stat.getBlockSize()) / 400000F;
      return (int) remaining;
    } catch (Exception ex) {
      // if we can't stat the filesystem then we don't know how many
      // pictures are remaining.  it might be zero but just leave it
      // blank since we really don't know.
      return CANNOT_STAT_ERROR;
    }
  }

  private void loadImage() {
    Bitmap dest = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(dest);
    canvas.drawBitmap(mBitmap,
        new Rect(0, 0, mBitmap.getWidth(), mBitmap.getHeight()),
        new Rect(0, 0, mWidth, mHeight), null);
    mImageView.setImageDrawable(new BitmapDrawable(getResources(), dest));
  }

  void initFont() {
    mFontNanum = Typeface.createFromAsset(this.getAssets(), "fonts/notosans"
        + ".ttf");
  }

  void setFont(TextView textView) {
    textView.setTypeface(mFontNanum);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mActivity = this;

    mContentResolver = getContentResolver();
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.crop_image_activity);
    initFont();

    /**
     * 화면 해상도를 구해서 해상도별 대응을 위한 PreviewParam Objects 를 생성한다
     */
    mFile = new File(
        Const.CAMERA_IMAGE_SAVE_PATH + Const.CAMERA_IMAGE_SAVE_FILE);

    WindowManager windowManager = (WindowManager) mActivity.getSystemService(
        Context.WINDOW_SERVICE);
    Display display = windowManager.getDefaultDisplay();

    Point size = new Point();
    display.getSize(size);
    int deviceX = size.x;
    int deviceY = size.y;
    LogUtil.d(TAG, "Device Size On Crop" + deviceX + "," + deviceY);

    int rotation = display.getRotation();
    if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) {
      mWidth = deviceY;
      mHeight = deviceX;
    }
    else {
      mWidth = deviceX;
      mHeight = deviceY;
    }

    mImageView = (ImageView) findViewById(R.id.picture);

    mFrameLeft = (int) getResources().getDimension(R.dimen.preview_left);
    mFrameTop = (int) getResources().getDimension(R.dimen.preview_top);

    LogUtil.e(TAG, "> mFrameLeft : " + mFrameLeft);
    LogUtil.e(TAG, "> mFrameTop : " + mFrameTop);

    mFrameRight = (int) getResources()
        .getDimension(R.dimen.preview_btn_area_width);
    mFrameWidth = mWidth - mFrameLeft - mFrameRight;
    mFrameHeight = mHeight - (mFrameTop * 2);

    mLL_left_edge = (LinearLayout) findViewById(R.id.ll_left_edge);

    TextView tv_title_left = (TextView) findViewById(R.id.tv_title_left);
    setFont(tv_title_left);
    TextView tv_title_right = (TextView) findViewById(R.id.tv_title_right);
    setFont(tv_title_right);

    TextView tv_send_dummy = (TextView) findViewById(R.id.tv_send_dummy);
    setFont(tv_send_dummy);
    TextView tv_send = (TextView) findViewById(R.id.tv_send);
    setFont(tv_send);

    TextView tv_reshoot = (TextView) findViewById(R.id.tv_reshoot);
    setFont(tv_reshoot);

    LinearLayout ll_close = (LinearLayout) findViewById(R.id.ll_close);
    ll_close.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        setResult(RESULT_CANCELED);
        mFile.delete();
        finish();
      }
    });

    LinearLayout ll_send = (LinearLayout) findViewById(R.id.ll_send);
    ll_send.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {
          onSaveClicked();
        } catch (Exception e) {
          finish();
        }
      }
    });

    LinearLayout ll_reshoot = (LinearLayout) findViewById(R.id.ll_reshoot);
    ll_reshoot.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        setResult(Const.RESULT_RETRY, getIntent());
        mFile.delete();
        finish();
      }
    });

    showStorageToast(this);

    if (PermissionChecker
        .checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        != PackageManager.PERMISSION_GRANTED) {
      // 토스트
      Toast.makeText(this, getString(R.string.message_toast_deny_permission),
          Toast.LENGTH_SHORT).show();
      mFile.delete();
      finish();
      return;
    }

    Uri target = getIntent().getData();
    if (mBitmap == null) {

      LogUtil.d(TAG, "Uri Data : " + target.toString());
      mAllImages = ImageManager.makeImageList(mContentResolver, target,
          ImageManager.SORT_ASCENDING);
      mImage = mAllImages.getImageForUri(target);

      mBitmap = mImage.fullSizeBitmap(mWidth, mWidth * mHeight);
    }

    if (mBitmap == null) {
      setResult(RESULT_CANCELED, getIntent());
      finish();
      return;
    }
    // Make UI fullscreen.
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    loadImage();
    startGuideLineBlinkThread();
  }

  public float getRatio(int numerator, int denominator) {
    if (denominator == 0) {
      return 0;
    }
    return (float) numerator / (float) denominator;
  }

  private void onSaveClicked() throws Exception {
    // step api so that we don't require that the whole (possibly large)
    // bitmap doesn't have to be read into memory
    if (mSaving) {
      return;
    }
    if (mBitmap == null) {
      return;
    }
    mSaving = true;

    LogUtil.e(TAG, "> mWidth : " + mWidth + ", mHeight : " + mHeight);
    LogUtil.e(TAG, "> mBitmap.getWidth() : " + mBitmap.getWidth());
    LogUtil.e(TAG, "> mBitmap.getHeight() : " + mBitmap.getHeight());

    // 스크린의 Width 와 Bitmap 의 Width 와의 비율
    float widthRatio = getRatio(mBitmap.getWidth(), mWidth);
    LogUtil.e(TAG, "> widthRatio : " + widthRatio);
    if (mWidth < mBitmap.getWidth()) {
      widthRatio = getRatio(mWidth, mBitmap.getWidth());
    }

    // 스크린의 Height 와 Bitmap 의 Height 와의 비율
    float heightRatio = getRatio(mBitmap.getHeight(), mHeight);
    LogUtil.e(TAG, "> heightRatio : " + heightRatio);
    if (mHeight < mBitmap.getHeight()) {
      heightRatio = getRatio(mHeight, mBitmap.getHeight());
    }

    mFrameWidth = (int) (mFrameWidth * widthRatio);
    mFrameHeight = (int) (mFrameHeight * heightRatio);
    mFrameLeft = (int) (mFrameLeft * widthRatio);
    mFrameTop = (int) (mFrameTop * heightRatio);

    // 상, 하, 좌, 우 확장
    if (Const.mIsCropImageExtend) {
      mFrameWidth += (mFrameLeft * 2);
      mFrameTop -= mFrameLeft;
      mFrameHeight += (mFrameLeft * 2);
      setReferenceVertexToPreference(mFrameLeft, mFrameLeft, mFrameWidth, mFrameHeight);
    }

    int width = mFrameWidth;
    int height = mFrameHeight;

    // If we are circle cropping, we want alpha channel, which is the
    // third param here.
    Bitmap croppedImage;
    try {
      croppedImage = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
    } catch (Exception e) {
      throw e;
    }

    if (croppedImage == null) {
      return;
    }

    Rect r = new Rect(0, mFrameTop, mFrameWidth, mFrameTop+mFrameHeight);

    Canvas croppedCanvas = new Canvas(croppedImage);
    Rect croppedDstRect = new Rect(0, 0, width, height);
    croppedCanvas.drawBitmap(mBitmap, r, croppedDstRect, null);

    // Return the cropped image directly or save it to the specified URI.
    Bundle myExtras = getIntent().getExtras();
    if (myExtras != null && (myExtras.getParcelable("data") != null
        || myExtras.getBoolean(RETURN_DATA))) {
      Bundle extras = new Bundle();
      extras.putParcelable(RETURN_DATA_AS_BITMAP, croppedImage);
      setResult(RESULT_OK,
          (new Intent()).setAction(ACTION_INLINE_DATA).putExtras(extras));
      finish();
    } else {
      final Bitmap b = croppedImage;
      CropUtil.startBackgroundJob(this, null, getString(R.string.savingImage),
          new Runnable() {
            public void run() {
              saveOutput(b);
            }
          }, mHandler);
    }
  }

  private void saveOutput(Bitmap croppedImage) {
    if (mSaveUri != null) {
      OutputStream outputStream = null;
      try {
        outputStream = mContentResolver.openOutputStream(mSaveUri);
        if (outputStream != null) {
          croppedImage.compress(mOutputFormat, 100, outputStream);
        }
      } catch (IOException ex) {
        LogUtil.e(TAG, "Cannot open file: " + mSaveUri, ex);
      } finally {
        CropUtil.closeSilently(outputStream);
      }
      Bundle extras = new Bundle();
      setResult(RESULT_OK, new Intent(mSaveUri.toString())
          .putExtras(extras));
    } else {
      try {
        int[] degree = new int[1];
        String filePath = getIntent().getStringExtra(Const.CAMERA_EXTRA_FILE);
        File file = new File(filePath);
        if (file.exists()) {
          file.delete();
        }

        // 편집된 이미지를 DB에 저장하지 않음
        LogUtil.e(TAG, "> saveOutput : croppedImage.getByteCount = " +
            croppedImage.getByteCount());
        ImageManager.addImage(mImage.getTitle(), filePath, croppedImage,
            null, degree);

        setResult(RESULT_OK, getIntent());
      } catch (Exception ex) {
        // basically ignore this or put up
        // some ui saying we failed
        LogUtil.e(TAG, "store image fail, continue anyway", ex);
      }
    }
    final Bitmap b = croppedImage;
    mHandler.post(new Runnable() {
      public void run() {
        b.recycle();
      }
    });
    finish();
  }

  /**
   * ITT, ITS의 eventStream에 채워질 값을 Preference에 저장한다.
   *
   * @param frameLeft frame 의 left 위치 값
   * @param frameTop frame  의 Top 위 치 값
   * @param frameWidth 원본 이미지의 width(해상도)
   * @param frameHeight 원본 이미지의 height(해상도)
   */
  // TODO
  private void setReferenceVertexToPreference(int frameLeft, int frameTop,
      int frameWidth, int frameHeight) {
    float ref_vertex_x = (float) frameLeft / frameWidth;
    float ref_vertex_y = (float) frameTop / frameHeight;
    PreferenceAgent.setFloatSharedData(this, MapKeys
        .IMAGE_REFVERTEX_X, ref_vertex_x);
    PreferenceAgent.setFloatSharedData(this, MapKeys
        .IMAGE_REFVERTEX_Y, ref_vertex_y);
    PreferenceAgent.setIntSharedData(this, MapKeys
        .IMAGE_RESOLUTION_X, frameWidth);
    PreferenceAgent.setIntSharedData(this, MapKeys
        .IMAGE_RESOLUTION_Y, frameHeight);

    LogUtil.e(TAG, "> frameLeft : " + frameLeft);
    LogUtil.e(TAG, "> frameTop : " + frameTop);
    LogUtil.e(TAG, "> frameWidth : " + frameWidth);
    LogUtil.e(TAG, "> frameHeight : " + frameHeight);
    LogUtil.e(TAG, "> ref_vertex_x : " + ref_vertex_x);
    LogUtil.e(TAG, "> ref_vertex_y : " + ref_vertex_y);
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  @Override
  protected void onDestroy() {
    if (mAllImages != null) {
      mAllImages.close();
    }
    // "전송"을 누르지 않고 화면종료하는 경우 저장된 파일을 삭제한다.
    if (!mSaving) {
      FileUtil.removeCameraFile();
    }
    super.onDestroy();
  }

  private void setEdgeShow(boolean isShow) {
    mLL_left_edge.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }

  private void startGuideLineBlinkThread() {
    if (mGuideLineBlinkThread == null) {
      mGuideLineBlinkThread = new CropImageActivity
          .GuideLineBlinkThread();
      mGuideLineBlinkThread.startThread();
    }
  }

  private void stopGuideLineBlinkThread() {
    if (mGuideLineBlinkThread != null) {
      mGuideLineBlinkThread.stopThread();
      mGuideLineBlinkThread = null;
    }
    if (mActivity != null) {
      mActivity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
          setEdgeShow(false);
        }
      });
    }
  }

  public class GuideLineBlinkThread extends Thread {

    private static final int SLEEP_DURATION = 500;
    private final int BLINK_DELAY = 5000;
    private boolean mIsRunning = false;
    private int mDuration = 0;
    private boolean mIsShow = false;

    public void startThread() {
      mIsRunning = true;
      mDuration = 0;
      this.start();
    }

    public void stopThread() {
      mIsRunning = false;
      this.interrupt();
    }

    public void run() {
      while (mIsRunning) {
        try {
          Thread.sleep(SLEEP_DURATION);
        } catch (InterruptedException e) {
        }

        mDuration += SLEEP_DURATION;

        mIsShow = !mIsShow;
        if (mActivity != null) {
          mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
              setEdgeShow(mIsShow);
            }
          });
        }

        if (mDuration >= BLINK_DELAY && mIsRunning) {
          stopGuideLineBlinkThread();
        }
      }
    }
  }
}
