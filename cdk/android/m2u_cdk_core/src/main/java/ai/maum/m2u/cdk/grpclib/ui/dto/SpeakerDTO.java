package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;

/**
 * speaker 설정 정보를 JS 로 부터 전달받거나 speaker 상태를 JS 에게 전달하기 위해 사용되는 데이터 전송 객체에 대한 클래스입니다.
 * <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */

public class SpeakerDTO {

  public static class SpeakerEvent {

    public static final String MUTE = "MUTE";
    public static final String UNMUTE = "UNMUTE";
    public static final String CLOSED = "CLOSED";
  }

  public static class SpeakerFailure {

    public static final String UNKNOWN = "UNKNOWN";
    public static final String SPAEKER_CLOSE_FAILED
        = "SPAEKER_CLOSE_FAILED";
    public static final String SPAEKER_PERMISSION_DENIED =
        "SPAEKER_PERMISSION_DENIED";
  }

  /**
   * speaker 의 상태 정보를 구성하는 클래스 입니다.
   */
  public static class SpeakerStatus {

    // speaker 의 상태
    private String eventType = SpeakerEvent.MUTE;
    // 음성 인식중인지 여부
    private boolean speechSynthesizing = false;
    // 추가적인 메시지 여부
    private boolean hasReason = false;
    // event 가 실패인 경우 reason
    private String failure = SpeakerFailure.UNKNOWN;
    // 상세한 에러 메시지
    private String message = Const.EMPTY_STRING;

    public SpeakerStatus() {
    }

    public String getEventType() {
      return eventType;
    }

    public void setEventType(String eventType) {
      this.eventType = eventType;
    }

    public boolean isSpeechSynthesizing() {
      return speechSynthesizing;
    }

    public void setSpeechSynthesizing(boolean speechSynthesizing) {
      this.speechSynthesizing = speechSynthesizing;
    }

    public boolean isHasReason() {
      return hasReason;
    }

    public void setHasReason(boolean hasReason) {
      this.hasReason = hasReason;
    }

    public String getFailure() {
      return failure;
    }

    public void setFailure(String failure) {
      this.failure = failure;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
}
