package ai.maum.m2u.cdk.grpclib.utils;

import android.content.Context;
import android.graphics.Point;
import android.os.Environment;
import android.os.StatFs;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

/**
 * Device 관련 정보를 확인하는 함수들로 구성된 클래스 입니다.
 */

public class DeviceUtil {

  private static final String TAG = DeviceUtil.class.getSimpleName();

  /**
   * 클래스 생성자 입니다.
   */
  private DeviceUtil() {
  }

  /**
   * SD card 의 사용가능 용량을 반환하는 함수입니다.
   * SD card 가 마운트 되지 않았으면 -1 을 리턴합니다.
   *
   * @return 사용가능한 byte 수
   */
  public static long getExternalStorageFreeSpace() {
    String mountPath = StringUtil.nullToBlankTrim(
        Environment.getExternalStorageDirectory().getAbsolutePath());
    if (!StringUtil.isBlank(mountPath)) {
      StatFs stat = new StatFs(mountPath);
      // 남은 공간 확인용 (getFreeBlocks()를 사용할 경우 사용하지 못하는 공간도 포함됨
      long bytesAvailable = (long) stat.getBlockSize() * (long) stat
          .getAvailableBlocks();
      LogUtil.i(TAG, "Available Size(Byte) = " + bytesAvailable);
      return bytesAvailable;
    } else {
      return -1;
    }
  }

  /**
   * SD card 의 저장 공간이 저장할 내용보다 큰지를 판단하는 함수입니다.
   *
   * @param fileSize 저장할 파일의 크기
   * @return SD card 의 저장 공간이 저장할 파일의 크기보다 큰 경우 true
   */
  public static boolean isAvailableSaveSpace(long fileSize) {
    if (fileSize > 0 && getExternalStorageFreeSpace() > 0
        && getExternalStorageFreeSpace() > fileSize) {
      return true;
    }
    return false;
  }

  /**
   * 현재 단말이 테블릿 스크린 타입인지 폰 스크린 타입인지 구분하는 함수입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @return 단말이 폰 스크린 타입인 경우 true
   */
  public static boolean isPhoneScreenType(Context context) {
    boolean isPhoneScreenType = true;

    WindowManager windowManager = (WindowManager) context.getSystemService(
        Context.WINDOW_SERVICE);
    Display display = windowManager.getDefaultDisplay();
    int rotation = display.getRotation();

    Point size = new Point();
    display.getSize(size);

    if (Surface.ROTATION_0 == rotation || Surface.ROTATION_180 == rotation) {
      if (size.x > size.y) {
        // This is a Tablet and it is in Landscape orientation
        isPhoneScreenType = false;
      } else {
        // This is a Phone and it is in Portrait orientation
        isPhoneScreenType = true;
      }
    } else {
      if (size.x > size.y) {
        // This is a Phone and it is in Landscape orientation
        isPhoneScreenType = true;
      } else {
        // This is a Tablet and it is in Portrait orientation
        isPhoneScreenType = false;
      }
    }
    return isPhoneScreenType;
  }
}
