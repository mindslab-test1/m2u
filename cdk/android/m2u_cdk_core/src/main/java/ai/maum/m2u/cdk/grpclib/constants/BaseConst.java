package ai.maum.m2u.cdk.grpclib.constants;


/**
 * 각 클래스들에서 공통으로 사용될 수 있는 Constant 들을 정의한 클래스입니다.
 */

public class BaseConst {

  /**
   * MetaData 구성 시 사용되는 Constant sub 클래스 입니다.
   */
  public static class GrpcMetadataKeys {

    // AuthToken Key 에 대한 Constant 입니다.
    public static final String M2U_AUTH_TOKEN = "m2u-auth-token";
    public static final String M2U_AUTH_SIGN_IN = "m2u-auth-sign-in";
  }

  /**
   * Preference 에 참조하기 위해 사용되는 Key 값 들에 대한 Constnat sub 클래스 입니다.
   */
  public static class MapKeys {

    public static final String SERVER_SELECTED_IP = "serverSelectedIP";
    public static final String SERVER_IP = "serverIP";
    public static final String SERVER_PORT = "serverPORT";
    public static final String USE_TLS = "useTls";
    public static final String PING_INTERVAL = "pingInterval";
    public static final String AUTH_TOKEN = "authToken";
    public static final String NOTIFYOBJECT_PREFIX = "notifyObjectPrefix";
    public static final String SERVER_DEV_IP = "serverDevIP";
    public static final String SERVER_DEV_PORT = "serverDevPORT";
    public static final String USER_ID = "userID";
    public static final String USER_NAME = "userName";
    public static final String USER_ADDRESS = "userAddress";
    public static final String IS_DDMS_LOG = "isDDMSLog";
    public static final String IS_FILE_LOG = "isFileLog";
    public static final String IS_ERROR_TOAST = "isErrorToast";
    public static final String IS_LOOPBACK_DATA = "isLoopbackData";
    public static final String DEVICE_ID = "deviceId";
    public static final String DEVICE_CHANNEL = "deviceChannel";
    public static final String DEVICE_PREFERREDCHATBOT =
        "devicePreferredChatbot";
    public static final String DEVICE_TYPE = "deviceType";
    public static final String DEVICE_VERSION = "deviceVersion";
    public static final String DEVICE_SUPPORT_RENDER_TEXT =
        "deviceSupportRenderText";
    public static final String DEVICE_SUPPORT_RENDER_CARD =
        "deviceSupportRenderCard";
    public static final String DEVICE_SUPPORT_SPEECH_SYNTHESIZER =
        "deviceSupportSpeechSynthesizer";
    public static final String DEVICE_SUPPORT_PLAY_AUDIO =
        "deviceSupportPlayAudio";
    public static final String DEVICE_SUPPORT_PLAY_VIDEO =
        "deviceSupportPlayVideo";
    public static final String DEVICE_SUPPORT_ACTION = "deviceSupportAction";
    public static final String DEVICE_SUPPORT_MOVE = "deviceSupportMove";
    public static final String DEVICE_SUPPORT_EXPECT_SPEECH =
        "deviceSupportExpectSpeech";
    public static final String DEVICE_TIMESTAMP_LONG_SECONDS =
        "deviceTimestampLongSeconds";
    public static final String DEVICE_TIMESTAMP_INT_NANOS =
        "deviceTimestampIntNamos";
    public static final String DEVICE_TIMESTAMP_STRING =
        "deviceTimestampString";
    public static final String DEVICE_META = "deviceMeta";
    public static final String SPEECH_ENCODING = "speechEncoding";
    public static final String SPEECH_SAMPLERATE = "speechSampleRate";
    public static final String SPEECH_LANG = "speechLang";
    public static final String SPEECH_MODEL = "speechModel";
    // 0 : 한국어, 1 : 영어
    public static final String KEY_SELECT_LANGUAGE = "select_language";

    public static final String IMAGE_REFVERTEX_X = "image_refvertex_x";
    public static final String IMAGE_REFVERTEX_Y = "image_refvertex_y";
    public static final String IMAGE_RESOLUTION_X = "image_resolution_x";
    public static final String IMAGE_RESOLUTION_Y = "image_resolution_y";
    public static final String IMAGE_CONFIRM_URL = "image_confirm_url";
    public static final String IMAGE_CONFIRM_RESIZE_RATE =
        "image_confirm_resize_rate";
    public static final String IMAGE_OPACITY_RATE = "image_opacity_reate";
  }

  /**
   * 디폴트값을 정의한 Constant sub 클래스 입니다.
   */
  public static class DefaultValue {

    public static final String DEFAULT_DEVICE_TIMEZONE = "KST+9";
  }
}
