package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.ui.dto.EtcDTO.Timestamp;

/**
 * GPS 정보를 취득한 후 객체로 만들어서 JS 에게 전달할 떄 json string 으로 변환하기 위해 사용되는 클래스입니다.
 */

public class LocationDTO {

  /**
   * GPS open, 취득 시 실패 종류를 정의한 클래스
   */
  public static class LocationFailure {

    public static final String LOCATION_UNKONWN
        = "LOCATION_UNKONWN";
    public static final String LOCATION_PERMISSION_DENIED
        = "LOCATION_PERMISSION_DENIED";
    public static final String LOCATION_TIMEOUT
        = "LOCATION_TIMEOUT";
  }

  /**
   * GPS 정보를 구성하는 클래스 입니다.
   */
  public static class LocationStatus {

    // 위도, 경도등이 정의된 클래스의 객체
    public static final Location location = new Location();
    // 추가적인 메시지 여부
    private boolean hasReason = false;
    // hasReason이 true인 경우 reason
    private String failure = LocationFailure.LOCATION_UNKONWN;
    // 상세한 에러 메시지
    private String message = Const.EMPTY_STRING;

    public LocationStatus() {
    }

    public Location getLocation() {
      return location;
    }

    public void setLocation(
        Location location) {
      this.location = location;
    }

    public boolean isHasReason() {
      return hasReason;
    }

    public void setHasReason(boolean hasReason) {
      this.hasReason = hasReason;
    }

    public String getFailure() {
      return failure;
    }

    public void setFailure(String failure) {
      this.failure = failure;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }

    public static class Location {

      // 현재 위치가 채집된 시간, 위치는 시간에 따라서 바뀔 수 있다.
      public static final EtcDTO.Timestamp refresh_at = null;
      // 위도
      public static final float latitude = 0.0f;
      // 경도
      public static final float longitude = 0.0f;
      // 기타 문자열로 표현된 장소 정보, "경복궁", "경포대"
      // *optional*
      // 통상적인 경우는 비어있습니다.
      public static final String location = Const.EMPTY_STRING;

      public Timestamp getRefresh_at() {
        return refresh_at;
      }

      public void setRefresh_at(Timestamp refresh_at) {
        this.refresh_at = refresh_at;
      }

      public float getLatitude() {
        return latitude;
      }

      public void setLatitude(float latitude) {
        this.latitude = latitude;
      }

      public float getLongitude() {
        return longitude;
      }

      public void setLongitude(float longitude) {
        this.longitude = longitude;
      }

      public String getLocation() {
        return location;
      }

      public void setLocation(String location) {
        this.location = location;
      }
    }
  }
}
