package ai.maum.m2u.cdk.grpclib.ui.updater;

/**
 * UI 업데이트를 실행하기 위한 클래스 입니다.
 */

public class ExecuteUpdater {

  /**
   * UI 업데이트를 하기 위한 클래스의 객체입니다.
   */
  private BaseUIUpdater updater = new UIUpdater();

  /**
   * Singleton 객체를 리턴해 주는 함수입니다.
   *
   * @return Singleton 객체
   */
  public static ExecuteUpdater getInstance() {
    return SingletonHolder.instance;
  }

  /**
   * UI 클래스에게 데이터 전달을 실행하는 함수입니다.
   *
   * @param updateCase 업데이트를 구분하는 case값
   * @param arg1 전달될 String
   * @param arg2 전달될 String
   * @param arg3 전달될 Integer
   * @param arg4 전달될 Boolean
   * @param arg5 전달될 byte[]
   * @param obj1 전달될 Object
   * @param obj2 전달될 Object
   */
  public void updateRun(int updateCase,
      String arg1, String arg2, int arg3, boolean arg4,
      byte[] arg5, Object obj1, Object obj2) {
    updater.execute(updateCase, arg1, arg2, arg3, arg4, arg5, obj1, obj2);
  }

  /**
   * Observer pattern 을 통해 업데이트가 될 UI 클래스들을 추가하는 함수입니다.
   *
   * @param observer 업데이트에 대한 notification을 받을 UI 클래스
   */
  public void addObserver(ObserverForUpdate observer) {
    updater.addObserver(observer);
  }

  /**
   * Observer pattern 을 더이상 사용하지 않을 때 호출되는 함수입니다.
   *
   * @param observer 업데이트를 더이상 필요로 하지 않는 UI 클래스
   */
  public void removeObserver(ObserverForUpdate observer) {
    updater.removeObserver(observer);
  }

  /**
   * UI 업데이트를 위한 클래스를 Singleton 으로 생성하는 클래스 입니다.
   */
  private static final class SingletonHolder {

    static final ExecuteUpdater instance = new ExecuteUpdater();
  }
}
