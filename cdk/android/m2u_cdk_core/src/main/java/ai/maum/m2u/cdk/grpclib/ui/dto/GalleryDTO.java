package ai.maum.m2u.cdk.grpclib.ui.dto;

import ai.maum.m2u.cdk.grpclib.constants.Const;

/**
 * gallery 설정 정보를 JS 로 부터 전달받거나 camera 상태를 JS 에게 전달하기 위해 사용되는 데이터 전송 객체에
 * 대한 클래스입니다.
 * <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */

public class GalleryDTO {

  /**
   * gallery open, close 시 실패 종류를 정의한 클래스입니다.
   */
  public static class GalleryFailure {

    public static final String GALLERY_ERROR_UNKNOWN = "GALLERY_ERROR_UNKNOWN";
    public static final String GALLERY_OPEN_ERROR = "GALLERY_OPEN_ERROR";
    public static final String GALLERY_CLOSE_ERROR = "GALLERY_CLOSE_ERROR";
    public static final String GALLERY_SELECT_CANCELLED = "GALLERY_SELECT_CANCELLED";
    public static final String GALLERY_TIMER_EXPIRED = "GALLERY_TIMER_EXPIRED";
    public static final String GALLERY_PERMISSION_DENIED = "GALLERY_TIMER_EXPIRED";
  }

  /**
   * gallery 이벤트를 정의한 클래스입니다.
   */
  public static class GalleryEvent {

    public static final String UNKNOWN = "UNKNOWN";
    public static final String OPEN_SUCCESS = "OPEN_SUCCESS";
    public static final String OPEN_FAILURE = "OPEN_FAILURE";
    public static final String IMAGE_CAPTURED = "IMAGE_CAPTURED";
    public static final String CLOSED = "CLOSED";
  }

  /**
   * gallery open 시 파라미터 구성에 대한 클래스 입니다.
   */
  public static class GalleryParam {

    // image capture 해상도 정보
    public static final EtcDTO.Resolution res = new EtcDTO.Resolution();
    // gallery 화면 보여질 시간이며 타이머를 구동할 때 사용됩니다.
    private int openInterval = 10;

    public GalleryParam() {
    }

    public EtcDTO.Resolution getRes() {
      return res;
    }

    public void setRes(EtcDTO.Resolution res) {
      this.res = res;
    }

    public int getOpenInterval() {
      return openInterval;
    }

    public void setOpenInterval(int openInterval) {
      this.openInterval = openInterval;
    }
  }

  /**
   * gallery 의 상태 정보를 구성하는 클래스 입니다.
   */
  public static class GalleryStatus {

    // thumbnail image data, image url 정보를 구성하는 클래스의 객체
    public static final EtcDTO.ImageData imageData = new EtcDTO.ImageData();
    // gallery 의 event 종류
    private String eventType = GalleryEvent.UNKNOWN;
    // 추가적인 메시지 여부
    private boolean hasReason = false;
    // hasReason이 true인 경우 reason
    private String failure = GalleryFailure.GALLERY_ERROR_UNKNOWN;
    // 상세한 에러 메시지
    private String message = Const.EMPTY_STRING;

    public GalleryStatus() {
    }

    public EtcDTO.ImageData getImageData() {
      return imageData;
    }

    public void setImageData(EtcDTO.ImageData imageData) {
      this.imageData = imageData;
    }

    public String getEventType() {
      return eventType;
    }

    public void setEventType(String eventType) {
      this.eventType = eventType;
    }

    public boolean isHasReason() {
      return hasReason;
    }

    public void setHasReason(boolean hasReason) {
      this.hasReason = hasReason;
    }

    public String getFailure() {
      return failure;
    }

    public void setFailure(String failure) {
      this.failure = failure;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
}
