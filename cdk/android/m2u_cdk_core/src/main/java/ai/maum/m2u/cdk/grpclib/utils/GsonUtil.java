package ai.maum.m2u.cdk.grpclib.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Object, Json String 사이에 변환을 할때 사용되는 함수들로 구성된 클래스 입니다.
 * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
 */

public class GsonUtil {

  @SuppressWarnings("unused")
  private static final String TAG = GsonUtil.class.getSimpleName();

  /**
   * Gson 객체에 대한 변수입니다.
   */
  private static Gson mGson;

  /**
   * Gson 객체를 빌드하여 리턴해주는 함수입니다. 이 함수를 통해서 Gson을 활용할 수 있습니다.
   *
   * @return Gson 객체
   */
  public static Gson getGson() {
    if (null == mGson) {
      mGson = new GsonBuilder().create();
    }
    return mGson;
  }

  /**
   * Object 를 JsonObject 로 변환하여 Logcat 에 출력하는 함수입니다.
   *
   * @param obj Logcat 에 출력될 Object
   */
  public static void gsonLog(Object obj) {
    JSONObject jsonObject = null;
    try {
      jsonObject = new JSONObject(getGson().toJson(obj));
//      LogUtil.e(TAG, jsonObject.toString(2));
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  /**
   * Object 를 Json String 으로 변환하여 리턴해주는 함수입니다.
   *
   * @param obj Json String 으로 변환될 Object
   * @return 변환된 Json String
   */
  public static String jsonStringFromObject(Object obj) {
    String jsonString = "";
    try {
      JSONObject jsonObject = new JSONObject(getGson().toJson(obj));
      jsonString = jsonObject.toString(2);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return jsonString;
  }
}
