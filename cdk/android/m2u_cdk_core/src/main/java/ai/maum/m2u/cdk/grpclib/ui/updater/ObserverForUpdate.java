package ai.maum.m2u.cdk.grpclib.ui.updater;

/**
 * UI 업데이를 위한 Observer pattern 인터페이스 입니다.
 */
public interface ObserverForUpdate {

  /**
   * UI 클래스에게 업데이트 될 데이터를 전달하는 함수입니다.
   * 이 인터페이스를 구현한 클래스에서 함수를 정의합니다.
   *
   * @param updateCase 업데이트를 구분하는 case값
   * @param arg1 전달될 String
   * @param arg2 전달될 String
   * @param arg3 전달될 Integer
   * @param arg4 전달될 Boolean
   * @param arg5 전달될 byte[]
   * @param obj1 전달될 Object
   * @param obj2 전달될 Object
   */
  public abstract void update(int updateCase,
      String arg1, String arg2, int arg3, boolean arg4,
      byte[] arg5, Object obj1, Object obj2);
}
