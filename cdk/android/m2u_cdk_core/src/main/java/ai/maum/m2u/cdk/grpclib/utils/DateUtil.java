package ai.maum.m2u.cdk.grpclib.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * 날짜 관련 처리를 하는 함수들로 구성된 클래스 입니다.
 * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
 */

public class DateUtil {

  /**
   * 클래스 생성자 입니다.
   */
  private DateUtil() {
  }

  /**
   * 현재 한국 날짜를 특정 format String 으로 변환하는 함수입니다.
   *
   * @param format 날짜 패턴 (yyyyMMddHHmmssSSS)
   * @return 인자로 받은 format으로 변환된 날짜를 리턴
   */
  public static String getKorCurrentFormattedDate(String format) {
    Date dt = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
    String strResult = sdf.format(dt);
    return strResult;
  }
}
