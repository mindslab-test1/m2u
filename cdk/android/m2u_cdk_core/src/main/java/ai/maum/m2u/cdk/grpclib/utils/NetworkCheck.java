package ai.maum.m2u.cdk.grpclib.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * 현재 단말의 네트워크 상태를 체크할 때 사용하는 함수로 구성된 클래스 입니다.
 * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
 */

public class NetworkCheck {

  /**
   * 현재 단말의 네트워크 상태가 유효한지 체크하여 결과를 리턴해주는 함수입니다.
   *
   * @param context 함수를 호출한 클래스의 context
   * @return 네트워크 상태가 유효한 경우 true
   */
  public static boolean isNetworkAvailable(Context context) {
    boolean networkavailable = false;

    try {
      ConnectivityManager connectivityManager =
          (ConnectivityManager) context
              .getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo activeNetworkInfo =
          connectivityManager.getActiveNetworkInfo();

      if (connectivityManager != null && activeNetworkInfo != null
          && activeNetworkInfo.isConnectedOrConnecting()) {
        networkavailable = true;
      } else {
        networkavailable = false;
        Toast.makeText((Activity) context,
            "Network connection failure.", Toast.LENGTH_LONG)
            .show();
      }
    } catch (Exception e) {
      LogUtil.e(TAG, e.getMessage());
    }
    return networkavailable;
  }
}
