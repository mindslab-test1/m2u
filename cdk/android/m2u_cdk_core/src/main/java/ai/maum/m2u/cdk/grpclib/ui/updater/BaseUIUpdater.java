package ai.maum.m2u.cdk.grpclib.ui.updater;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * UI 를 업데이트 하기 위해 사용되는 Observer pattern 클래스에서 참조하는 클래스 입니다.
 */
public abstract class BaseUIUpdater {

  /**
   * Observer pattern 을 생성합니다.
   */
  @SuppressWarnings("rawtypes")
  private CopyOnWriteArrayList observers = new CopyOnWriteArrayList();

  /**
   * Observer pattern 을 통해 업데이트가 될 UI 클래스들을 추가하는 함수입니다.
   *
   * @param observer 업데이트에 대한 notification을 받을 UI 클래스
   */
  @SuppressWarnings("unchecked")
  public void addObserver(ObserverForUpdate observer) {
    observers.add(observer);
  }

  /**
   * Observer pattern 을 더이상 사용하지 않을 때 호출되는 함수입니다.
   *
   * @param observer 업데이트를 더이상 필요로 하지 않는 UI 클래스
   */
  public void removeObserver(ObserverForUpdate observer) {
    observers.remove(observer);
  }

  /**
   * Observer pattern 에게 데이터를 전달하는 함수입니다.
   * addObserver 함수를 통해 추가된 UI 클래스들에게 모두 전달됩니다.
   *
   * @param updateCase 업데이트를 구분하는 case값
   * @param arg1 전달될 String
   * @param arg2 전달될 String
   * @param arg3 전달될 Integer
   * @param arg4 전달될 Boolean
   * @param arg5 전달될 byte[]
   * @param obj1 전달될 Object
   * @param obj2 전달될 Object
   */
  @SuppressWarnings("rawtypes")
  public void notifyObserver(int updateCase,
      String arg1, String arg2, int arg3, boolean arg4,
      byte[] arg5, Object obj1, Object obj2) {

    Iterator iter = observers.iterator();
    while (iter.hasNext()) {
      ObserverForUpdate observerUpdater = (ObserverForUpdate) iter.next();
      observerUpdater.update(updateCase,
          arg1, arg2, arg3, arg4,
          arg5, obj1, obj2);
    }
  }

  /**
   * 현재 업데이트 중인지에 대한 여부를 확인하는 함수입니다.
   * 추상 메소드이므로 상속받은 클래스에서 해당 함수를 구현하고 있습니다.
   *
   * @return 현재 업데이트 중이면 true
   */
  public abstract boolean getUpdateStatus();

  /**
   * UI 에 업데이트 될 정보를 Observer pattern 에게 전달해 주는 함수입니다.
   * 추상 메소드이므로 상속받은 클래스에서 해당 함수를 구현하고 있습니다.
   *
   * @param updateCase 업데이트를 구분하는 case값
   * @param arg1 전달될 String
   * @param arg2 전달될 String
   * @param arg3 전달될 Integer
   * @param arg4 전달될 Boolean
   * @param arg5 전달될 byte[]
   * @param obj1 전달될 Object
   * @param obj2 전달될 Object
   */
  public abstract void execute(int updateCase,
      String arg1, String arg2, int arg3, boolean arg4,
      byte[] arg5, Object obj1, Object obj2);
}
