package ai.maum.m2u.cdk.grpclib.ui;

import ai.maum.m2u.cdk.grpclib.R;
import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.ui.dto.CameraDTO;
import ai.maum.m2u.cdk.grpclib.utils.FileUtil;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * 카메라 프리뷰 화면을 제공하는 클래스입니다. 아래 샘플 소스를 참고하였습니다. http://webnautes.tistory.com/822
 */

public class CameraAllPreviewActivity extends CoreBaseActivity implements
    SurfaceHolder.Callback {

  private static final String TAG = CameraAllPreviewActivity.class
      .getSimpleName();

  private final static int PERMISSIONS_REQUEST_CODE = 100;
  ShutterCallback shutterCallback = new ShutterCallback() {
    public void onShutter() {
      LogUtil.d(TAG, "onShutter'd");
    }
  };
  PictureCallback rawCallback = new PictureCallback() {
    public void onPictureTaken(byte[] data, Camera camera) {
      LogUtil.d(TAG, "onPictureTaken - raw");
    }
  };
  private CameraSurfaceView mPreview;
  private SurfaceHolder surfaceHolder;
  private Camera mCamera;
  private int mWidth;
  private int mHeight;
  private Typeface mFontNanum;
  private Activity mActivity;
  private int mFacing = CameraInfo.CAMERA_FACING_BACK;

  // 촬영버튼이 중복으로 눌리지 않도록 체크하기 위한 변수
  private boolean mIsCaptureUse = false;
  /**
   * This is the output file for our picture.
   */
  private File mFile;
  /**
   * 카메라 파라미터
   */
  private CameraDTO.CameraParam mCameraParam;
  private PreviewParams.PreviewParam mPreviewParam;
  private LinearLayout mLL_left_edge;
  private LayoutInflater mInflater;
  private GuideLineBlinkThread mGuideLineBlinkThread = null;
  //참고 : http://stackoverflow.com/q/37135675
  private PictureCallback jpegCallback = new PictureCallback() {
    public void onPictureTaken(byte[] data, Camera camera) {

      //이미지의 너비와 높이 결정
      int pw = camera.getParameters().getPreviewSize().width;
      int ph = camera.getParameters().getPreviewSize().height;
      int w = camera.getParameters().getPictureSize().width;
      int h = camera.getParameters().getPictureSize().height;

      int orientation = setCameraDisplayOrientation(
          CameraAllPreviewActivity.this,
          mFacing, camera);

      //byte array를 bitmap으로 변환
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inPreferredConfig = Bitmap.Config.ARGB_8888;
      Bitmap bitmap = BitmapFactory
          .decodeByteArray(data, 0, data.length, options);

      //이미지를 디바이스 방향으로 회전
      Matrix matrix = new Matrix();
      matrix.postRotate(orientation);
      bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);

      //bitmap을 byte array로 변환
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
      byte[] currentData = stream.toByteArray();
      LogUtil.d("mmcamera",
          "" + currentData[0] + currentData[1] + currentData[2]
              + currentData[3]);

      //파일로 저장
      new SaveImageTask().execute(currentData);
      LogUtil.d(TAG, "onPictureTaken - jpeg");
    }
  };

  public static void doRestart(Context c) {
    //http://stackoverflow.com/a/22345538
    try {
      //check if the context is given
      if (c != null) {
        //fetch the packagemanager so we can get the default launch activity
        // (you can replace this intent with any other activity if you want
        PackageManager pm = c.getPackageManager();
        //check if we got the PackageManager
        if (pm != null) {
          //create the intent with the default start activity for your application
          Intent mStartActivity = pm.getLaunchIntentForPackage(
              c.getPackageName()
          );
          if (mStartActivity != null) {
            mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //create a pending intent so the application is restarted
            // after System.exit(0) was called.
            // We use an AlarmManager to call this intent in 100ms
            int mPendingIntentId = 223344;
            PendingIntent mPendingIntent = PendingIntent
                .getActivity(c, mPendingIntentId, mStartActivity,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager mgr =
                (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100,
                mPendingIntent);
            //kill the application
            System.exit(0);
          } else {
            LogUtil.e(TAG, "Was not able to restart application, " +
                "mStartActivity null");
          }
        } else {
          LogUtil.e(TAG, "Was not able to restart application, PM null");
        }
      } else {
        LogUtil.e(TAG, "Was not able to restart application, Context null");
      }
    } catch (Exception ex) {
      LogUtil.e(TAG, "Was not able to restart application");
    }
  }

  public static int setCameraDisplayOrientation(Activity activity,
      int cameraId, Camera camera) {
    CameraInfo info =
        new CameraInfo();
    Camera.getCameraInfo(cameraId, info);
    int rotation = activity.getWindowManager().getDefaultDisplay()
        .getRotation();
    int degrees = 0;
    switch (rotation) {
      case Surface.ROTATION_0:
        degrees = 0;
        break;
      case Surface.ROTATION_90:
        degrees = 90;
        break;
      case Surface.ROTATION_180:
        degrees = 180;
        break;
      case Surface.ROTATION_270:
        degrees = 270;
        break;
    }

    int result;
    if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
      result = (info.orientation + degrees) % 360;
      result = (360 - result) % 360;  // compensate the mirror
    } else {  // back-facing
      result = (info.orientation - degrees + 360) % 360;
    }

    return result;
  }

  void initFont() {
    mFontNanum = Typeface
        .createFromAsset(this.getAssets(), "fonts/notosans.ttf");
  }

  void setFont(TextView textView) {
    textView.setTypeface(mFontNanum);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mContext = this;
    mActivity = this;

    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.activity_camera_all);

    initFont();

    // Preview화면 시작시 저장된 이미지 파일을 삭제한다.
    FileUtil.removeCameraFile();

    /**
     * 화면 해상도를 구해서 해상도별 대응을 위한 PreviewParam Objects 를 생성한다
     */

    WindowManager windowManager = (WindowManager) mContext.getSystemService(
        Context.WINDOW_SERVICE);
    Display display = windowManager.getDefaultDisplay();

    Point size = new Point();
    display.getSize(size);
    int deviceX = size.x;
    int deviceY = size.y;

    int rotation = display.getRotation();
    if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) {
      mWidth = deviceY;
      mHeight = deviceX;
    }
    else {
      mWidth = deviceX;
      mHeight = deviceY;
    }

    mCameraParam = getIntent().getParcelableExtra(Const.CAMERA_EXTRA_INFO);
    mPreviewParam = new PreviewParams.PreviewParam(this);

    mLL_left_edge = (LinearLayout) findViewById(R.id.ll_left_edge);

    TextView tv_title_left = (TextView) findViewById(R.id.tv_title_left);
    setFont(tv_title_left);
    TextView tv_title_right = (TextView) findViewById(R.id.tv_title_right);
    setFont(tv_title_right);

    TextView tv_shoot_dummy = (TextView) findViewById(R.id.tv_shoot_dummy);
    setFont(tv_shoot_dummy);
    TextView tv_shoot = (TextView) findViewById(R.id.tv_shoot);
    setFont(tv_shoot);

    LinearLayout ll_close = (LinearLayout) findViewById(R.id.ll_close);
    ll_close.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

    LinearLayout ll_capture = (LinearLayout) findViewById(R.id.ll_capture);
    ll_capture.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (getCameraCaptureUse()) {
          LogUtil.e(TAG, "> Capture return...");
          return;
        }
        setCameraCaptureUse(true);
        try {
          LogUtil.e(TAG, "> Capture do...");
          mCamera.autoFocus(new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {
              LogUtil.e(TAG, "> Capture is success ? : " + success);
              camera.takePicture(shutterCallback, rawCallback, jpegCallback);
            }
          });
        } catch (Exception e) {
          LogUtil.e(TAG, "ll_capture onClick : " + e.getMessage());
          setCameraCaptureUse(false);
        }
      }
    });

    if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //API 23 이상이면
        // 런타임 퍼미션 처리 필요

        int hasCameraPermission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.CAMERA);
        int hasWriteExternalStoragePermission =
            ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (hasCameraPermission == PackageManager.PERMISSION_GRANTED
            && hasWriteExternalStoragePermission
            == PackageManager.PERMISSION_GRANTED) {
          //이미 퍼미션을 가지고 있음
        } else {
          //퍼미션 요청
          ActivityCompat.requestPermissions(this,
              new String[]{Manifest.permission.CAMERA,
                  Manifest.permission.WRITE_EXTERNAL_STORAGE},
              PERMISSIONS_REQUEST_CODE);
        }
      } else {
        // do nothing
      }
    } else {
      Toast.makeText(CameraAllPreviewActivity.this, "Camera not supported",
          Toast.LENGTH_LONG).show();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    startCamera();
  }

  @Override
  protected void onPause() {
    super.onPause();
    // Surface will be destroyed when we return, so stop the preview.
    if (mCamera != null) {
      // Call stopPreview() to stop updating the preview surface
      mCamera.stopPreview();
      mPreview.setCamera(null);
      mCamera.release();
      mCamera = null;
    }
    ((RelativeLayout) findViewById(R.id.ll_main)).removeView(mPreview);
    mPreview = null;
  }

  public void startCamera() {
    setCameraCaptureUse(false);
    if (mPreview == null) {
      mPreview = new CameraSurfaceView(this, (SurfaceView) findViewById(R.id
          .surfaceview), mCameraParam, mWidth, mHeight);
      mPreview.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
          LayoutParams.MATCH_PARENT));
      ((RelativeLayout) findViewById(R.id.ll_main)).addView(mPreview, 2);
      mPreview.setKeepScreenOn(true);

      surfaceHolder = mPreview.mSurfaceView.getHolder();
      surfaceHolder.addCallback(this);
    }

    mPreview.setCamera(null);
    if (mCamera != null) {
      mCamera.release();
      mCamera = null;
    }

    int numCams = Camera.getNumberOfCameras();
    if (numCams > 0) {
      try {
        mFacing = mCameraParam.isRearCamera() ? CameraInfo
            .CAMERA_FACING_BACK : CameraInfo.CAMERA_FACING_FRONT;
        mCamera = Camera.open(mFacing);
        // camera orientation
        mCamera.setDisplayOrientation(setCameraDisplayOrientation(this, mFacing,
            mCamera));
        // get Camera parameters
        Parameters params = mCamera.getParameters();
        List<Camera.Size> allSizes = params.getSupportedPreviewSizes();
        // picture image orientation
        params.setRotation(setCameraDisplayOrientation(this, mFacing, mCamera));
        if (mFacing == CameraInfo.CAMERA_FACING_BACK) {
          params.setFocusMode(Parameters.FOCUS_MODE_AUTO);
          params.setFlashMode(Parameters.FLASH_MODE_AUTO);
        }

        Camera.Size size = allSizes.get(0); // get top size
        float cRatio = mWidth / mHeight;
        float bRatio = 0.0f;
        // Surface View (화면)과 가장 가까운 카메라 뷰의 종횡비를 찾는다.
        for (int i = 0; i < allSizes.size(); i++) {
          if (Math.abs(cRatio - bRatio) > Math.abs(
              cRatio - getRatio(allSizes.get(i).width,
                  allSizes.get(i).height))) {
            bRatio = getRatio(allSizes.get(i).width, allSizes.get(i).height);
            size = allSizes.get(i);
          }
        }
        //set max Preview Size
        //params.setPreviewSize(size.width, size.height);
        LogUtil.d("mmcamera",
            "Size 1 : " + size.width + "," + size.height + " " + mWidth + ","
                + mHeight);
        mCamera.setParameters(params);
        mCamera.setPreviewDisplay(surfaceHolder);
        mCamera.startPreview();
      } catch (IOException e) {
        LogUtil.d(TAG, "surface exception : " + e.getMessage().toString());
      } catch (RuntimeException ex) {
        Toast.makeText(mContext,
            "camera_not_found " + ex.getMessage().toString(),
            Toast.LENGTH_LONG).show();
        LogUtil.d(TAG, "camera_not_found " + ex.getMessage().toString());
      }
    }
    mPreview.setCamera(mCamera);
    startGuideLineBlinkThread();
  }

  public float getRatio(int w, int h) {
    if (h == 0) {
      return 0;
    }
    return (float) w / (float) h;
  }

  private void resetCam() {
    LogUtil.e(TAG, "> Capture is fail...so, resetCam...");
    startCamera();
  }

  private void setCameraCaptureUse(boolean isCaptureUse) {
    mIsCaptureUse = isCaptureUse;
  }

  private boolean getCameraCaptureUse() {
    return mIsCaptureUse;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
      @NonNull String[] permissions,
      @NonNull int[] grandResults) {

    if (requestCode == PERMISSIONS_REQUEST_CODE && grandResults.length > 0) {

      int hasCameraPermission = ContextCompat.checkSelfPermission(this,
          Manifest.permission.CAMERA);
      int hasWriteExternalStoragePermission =
          ContextCompat.checkSelfPermission(this,
              Manifest.permission.WRITE_EXTERNAL_STORAGE);

      if (hasCameraPermission == PackageManager.PERMISSION_GRANTED
          && hasWriteExternalStoragePermission
          == PackageManager.PERMISSION_GRANTED) {

        //이미 퍼미션을 가지고 있음
        doRestart(this);
      } else {
        checkPermissions();
      }
    }
  }

  @TargetApi(Build.VERSION_CODES.M)
  private void checkPermissions() {
    int hasCameraPermission = ContextCompat.checkSelfPermission(this,
        Manifest.permission.CAMERA);
    int hasWriteExternalStoragePermission =
        ContextCompat.checkSelfPermission(this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE);

    boolean cameraRationale = ActivityCompat
        .shouldShowRequestPermissionRationale(this,
            Manifest.permission.CAMERA);
    boolean writeExternalStorageRationale =
        ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE);

    if ((hasCameraPermission == PackageManager.PERMISSION_DENIED
        && cameraRationale)
        || (
        hasWriteExternalStoragePermission == PackageManager.PERMISSION_DENIED
            && writeExternalStorageRationale)) {
      showDialogForPermission(getResources().getString(R.string
          .message_dialog_deny_permission));
    } else if ((hasCameraPermission == PackageManager.PERMISSION_DENIED
        && !cameraRationale)
        || (
        hasWriteExternalStoragePermission == PackageManager.PERMISSION_DENIED
            && !writeExternalStorageRationale)) {
      showDialogForPermissionSetting(getResources().getString(R.string
          .message_dialog_deny_permission));
    } else if (hasCameraPermission == PackageManager.PERMISSION_GRANTED
        || hasWriteExternalStoragePermission
        == PackageManager.PERMISSION_GRANTED) {
      doRestart(this);
    }
  }

  @TargetApi(Build.VERSION_CODES.M)
  private void showDialogForPermission(String msg) {

    AlertDialog.Builder builder = new AlertDialog.Builder(
        CameraAllPreviewActivity.this);
    builder.setTitle(getResources().getString(R.string.inform));
    builder.setMessage(msg);
    builder.setCancelable(false);
    builder.setPositiveButton(getResources().getString(R.string.yes), new
        DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            //퍼미션 요청
            ActivityCompat.requestPermissions(CameraAllPreviewActivity.this,
                new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSIONS_REQUEST_CODE);
          }
        });

    builder.setNegativeButton(getResources().getString(R.string.no), new
        DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            finish();
          }
        });
    builder.create().show();
  }

  private void showDialogForPermissionSetting(String msg) {

    AlertDialog.Builder builder = new AlertDialog.Builder(
        CameraAllPreviewActivity.this);
    builder.setTitle(getResources().getString(R.string.inform));
    builder.setMessage(msg);
    builder.setCancelable(true);
    builder.setPositiveButton(getResources().getString(R.string.yes),
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            Intent myAppSettings = new Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + mActivity.getPackageName()));
            myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
            myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mActivity.startActivity(myAppSettings);
          }
        });
    builder.setNegativeButton(getResources().getString(R.string.no), new
        DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            finish();
          }
        });
    builder.create().show();
  }

  @Override
  public void surfaceCreated(SurfaceHolder holder) {

  }

  @Override
  public void surfaceChanged(SurfaceHolder holder, int format, int width,
      int height) {

  }

  @Override
  public void surfaceDestroyed(SurfaceHolder holder) {

  }

  private void setEdgeShow(boolean isShow) {
    mLL_left_edge.setVisibility(isShow ? View.VISIBLE : View.GONE);
  }

  private void startGuideLineBlinkThread() {
    if (mGuideLineBlinkThread == null) {
      mGuideLineBlinkThread = new GuideLineBlinkThread();
      mGuideLineBlinkThread.startThread();
    }
  }

  private void stopGuideLineBlinkThread() {
    if (mGuideLineBlinkThread != null) {
      mGuideLineBlinkThread.stopThread();
      mGuideLineBlinkThread = null;
    }
    if (mActivity != null) {
      mActivity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
          setEdgeShow(false);
        }
      });
    }
  }

  private class SaveImageTask extends AsyncTask<byte[], Void, Void> {

    @Override
    protected Void doInBackground(byte[]... data) {
      FileOutputStream outStream = null;

      // Write to SD Card
      try {
        File dir = new File(Const.CAMERA_IMAGE_SAVE_PATH);
        if (!dir.exists()) {
          dir.mkdirs();
        }
        mFile = new File(
            Const.CAMERA_IMAGE_SAVE_PATH + Const.CAMERA_IMAGE_SAVE_FILE);

        outStream = new FileOutputStream(mFile);
        outStream.write(data[0]);
        outStream.flush();
        outStream.close();

        LogUtil.d(TAG, mFile.toString());

        final String packageNameAuthorities = String.format(getResources()
            .getString(R.string.authorities_appid), getPackageName());

        LogUtil.e(TAG, ">>> packageNameAuthorities : " +
            packageNameAuthorities);

        Uri photoUri = FileProvider
            .getUriForFile(mContext, packageNameAuthorities, mFile);

        Intent intent = getIntent();
        intent.setData(photoUri);
        intent.putExtra(Const.CAMERA_EXTRA_FILE, mFile.getAbsolutePath());
        intent.putExtra(Const.CAMERA_EXTRA_RESULT, Const.RESULT_SUCCESS);

        setResult(RESULT_OK, intent);
        finish();
      } catch (FileNotFoundException e) {
        LogUtil.e("mmcamera", e.getMessage());
      } catch (IOException e) {
        LogUtil.e("mmcamera", e.getMessage());
      } catch (Exception e) {
        LogUtil.e("mmcamera", e.getMessage());
      } finally {
      }
      return null;
    }
  }

  public class GuideLineBlinkThread extends Thread {

    private static final int SLEEP_DURATION = 500;
    private final int BLINK_DELAY = 5000;
    private boolean mIsRunning = false;
    private int mDuration = 0;
    private boolean mIsShow = false;

    public void startThread() {
      mIsRunning = true;
      mDuration = 0;
      this.start();
    }

    public void stopThread() {
      mIsRunning = false;
      this.interrupt();
    }

    public void run() {
      while (mIsRunning) {
        try {
          Thread.sleep(SLEEP_DURATION);
        } catch (InterruptedException e) {
        }

        mDuration += SLEEP_DURATION;

        mIsShow = !mIsShow;
        if (mActivity != null) {
          mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
              setEdgeShow(mIsShow);
            }
          });
        }

        if (mDuration >= BLINK_DELAY && mIsRunning) {
          stopGuideLineBlinkThread();
        }
      }
    }
  }
}
