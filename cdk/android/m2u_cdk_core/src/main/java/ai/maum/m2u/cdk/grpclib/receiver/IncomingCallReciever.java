package ai.maum.m2u.cdk.grpclib.receiver;

import ai.maum.m2u.cdk.grpclib.constants.HandlerConst;
import ai.maum.m2u.cdk.grpclib.ui.updater.ExecuteUpdater;
import ai.maum.m2u.cdk.grpclib.ui.view.ToastView;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import ai.maum.m2u.cdk.grpclib.utils.StringUtil;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/**
 * 단말기의 현재 Call 상태를 전달받는 Receiver 클래스 입니다.
 */

public class IncomingCallReciever extends BroadcastReceiver {

  private static final String TAG = IncomingCallReciever.class.getSimpleName();
  private Context mContext;
  private Intent mIntent;
  private final PhoneStateListener phoneStateListener = new PhoneStateListener() {
    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
      String callState = "UNKNOWN";
      switch (state) {
        case TelephonyManager.CALL_STATE_IDLE:
          callState = "IDLE";
          break;
        case TelephonyManager.CALL_STATE_RINGING:
          // -- check international call or not.
          if (!StringUtil.isEmptyString(incomingNumber) && incomingNumber
              .startsWith("00")) {
            ToastView.getInstance(mContext).setToastMsg("International Call- "
                + incomingNumber);
            callState = "International - Ringing (" + incomingNumber + ")";
          } else {
            ToastView.getInstance(mContext).setToastMsg("Local Call - " +
                incomingNumber);
            callState = "Local - Ringing (" + incomingNumber + ")";
          }
          break;
        case TelephonyManager.CALL_STATE_OFFHOOK:
          String dialingNumber = mIntent
              .getStringExtra(Intent.EXTRA_PHONE_NUMBER);
          if (StringUtil.isEmptyString(dialingNumber)) {
            return;
          }
          if (!StringUtil.isEmptyString(dialingNumber) && dialingNumber
              .startsWith("00")) {
            ToastView.getInstance(mContext).setToastMsg("International - "
                + dialingNumber);
            callState = "International - Dialing (" + dialingNumber + ")";
          } else {
            ToastView.getInstance(mContext).setToastMsg("International - "
                + dialingNumber);
            callState = "Local - Dialing (" + dialingNumber + ")";
          }
          break;
      }
      LogUtil.e(TAG, "onCallStateChanged " + callState);
      runUiUpdater(HandlerConst.UIUpdater.UI_FROM_ETC_CALLSTATUS,
          null, null, state, false,
          null, null, null);
      super.onCallStateChanged(state, incomingNumber);
    }
  };

  @Override
  public void onReceive(Context context, Intent intent) {
    mContext = context;
    mIntent = intent;
    TelephonyManager telephonyManager = (TelephonyManager) context
        .getSystemService(Context.TELEPHONY_SERVICE);
    int events = PhoneStateListener.LISTEN_CALL_STATE;
    telephonyManager.listen(phoneStateListener, events);
  }

  /**
   * UI 에 데이터를 전송하기 위해서 호출하는  함수입니다.
   */
  private void runUiUpdater(int updateCase,
      String arg1, String arg2, int arg3, boolean arg4, byte[] arg5, Object
      obj1, Object obj2) {
    ExecuteUpdater updater = ExecuteUpdater.getInstance();
    updater.updateRun(updateCase, arg1, arg2, arg3, arg4, arg5, obj1, obj2);
  }
}
