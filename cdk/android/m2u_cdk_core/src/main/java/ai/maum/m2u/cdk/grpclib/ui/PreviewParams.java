package ai.maum.m2u.cdk.grpclib.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.WindowManager;

import ai.maum.m2u.cdk.grpclib.R;

/**
 * camera 설정 정보를 JS 로 부터 전달받거나 camera 상태를 JS 에게 전달하기 위해 사용되는 데이터 전송 객체에 대한 클래스입니다. <br>
 * json string, 객체간 변환을 할 때에도 사용합니다.
 */

public class PreviewParams {

  /**
   * 프리뷰 화면에 사용되는 텍스트뷰의 화면구성 클래스
   */

  public static class PreviewText implements Parcelable {
    public static final int fontName;
    public static final int fontSize;
    public static final int foreColor, backColor;
    public static final String text;

    protected PreviewText(Parcel in) {
      fontName = in.readInt();
      fontSize = in.readInt();
      foreColor = in.readInt();
      backColor = in.readInt();
      text = in.readString();
    }

    public static final Creator<PreviewText> CREATOR = new Creator<PreviewText>() {
      @Override
      public PreviewText createFromParcel(Parcel in) {
        return new PreviewText(in);
      }

      @Override
      public PreviewText[] newArray(int size) {
        return new PreviewText[size];
      }
    };

    public PreviewText() {

    }

    @Override
    public int describeContents() {
      return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
      parcel.writeInt(fontName);
      parcel.writeInt(fontSize);
      parcel.writeInt(foreColor);
      parcel.writeInt(backColor);
      parcel.writeString(text);
    }
  }

  /**
   * preview 화면을 구성하는 파라미터 클래스 입니다.
   */
  public static class PreviewParam implements Parcelable {

    public static final Creator<PreviewParam> CREATOR =
        new Creator<PreviewParam>() {

          @Override
          public PreviewParam[] newArray(int size) {
            return new PreviewParam[size];
          }

          @Override
          public PreviewParam createFromParcel(Parcel source) {
            return new PreviewParam(source);
          }
        };

    public PreviewParam(Context context) {
      mContext = context;
      WindowManager windowManager = (WindowManager) mContext.getSystemService(
          Context.WINDOW_SERVICE);
      Display display = windowManager.getDefaultDisplay();

      Point size = new Point();
      display.getSize(size);
      int deviceX = size.x;
      int deviceY = size.y;

      int displayIndex = 0;
      if (deviceY >= 1080) displayIndex = 1;
      else if (deviceY >= 1440) displayIndex = 2;

      int titleFontSizeSet[] = new int[]{36, 54, 72};
      int buttonFontSizeSet[] = new int[]{26, 38, 52};
      int frameLineWidthSet[] = new int[]{10, 17, 20};

      // Default 지정
      this.setTitleLayout(Gravity.LEFT);
      PreviewText title = new PreviewText();
      title.fontSize = titleFontSizeSet[displayIndex];
      title.foreColor = Color.parseColor("#FFFFFF");
      title.backColor = Color.parseColor("#00000000");
      title.text = context.getResources().getString(R.string.image_send_noti);
      this.setTitleText(title);

      this.setButtonLayout(Gravity.CENTER);

      PreviewText btn1 = new PreviewText();
      btn1.fontSize = buttonFontSizeSet[displayIndex];
      btn1.foreColor = Color.parseColor("#FFFFFF");
      btn1.backColor = Color.parseColor("#00000000");
      btn1.text = context.getResources().getString(R.string.image_capture);
      this.setButton1Text(btn1);

      PreviewText btn2 = new PreviewText();
      btn2.fontSize = buttonFontSizeSet[displayIndex];
      btn2.foreColor = Color.parseColor("#FFFFFF");
      btn2.backColor = Color.parseColor("#00000000");
      btn2.text = context.getResources().getString(R.string.image_resend);
      this.setButton2Text(btn2);

      PreviewText btnX = new PreviewText();
      btnX.fontSize = buttonFontSizeSet[displayIndex];
      btnX.foreColor = Color.parseColor("#FFFFFF");
      btnX.backColor = Color.parseColor("#00000000");
      btnX.text = context.getResources().getString(R.string.image_close);
      this.setButtonXText(btnX);

      this.setPreviewFrameColor(Color.parseColor("#fcff00"));
      this.setPreviewFrameLineWidth(frameLineWidthSet[displayIndex]);
      this.setPreviewFrameRatio(1.9f);

      this.xButtonDrawable = R.drawable.icon_x720;
      this.button1Drawable = R.drawable.btn_camera720;
      this.button2Drawable = R.drawable.btn_sendpng720;

      int titleMarginLeftSet[] = new int[]{50,75, 100};
      int titleMarginTopSet[] = new int[]{63,95, 126};
      int titleMarginBottomSet[] = new int[]{63,95, 126};
      int titleHeightSet[] = new int[]{34,60, 68};

      this.titleMarginLeft = titleMarginLeftSet[displayIndex];
      this.titleMarginTop = titleMarginTopSet[displayIndex];
      this.titleMarginBottom = titleMarginBottomSet[displayIndex];
      this.titleHeight = titleHeightSet[displayIndex];

      int frameMarginLeftSet[] = new int[]{50,75,100};
      int frameMarginBottomSet[] = new int[]{50,100,100};

      this.frameMarginLeft = frameMarginLeftSet[displayIndex];
      this.frameMarginBottom = frameMarginBottomSet[displayIndex];

      int buttonImageWidthSet[] = new int[]{113,170,228};
      int buttonMarginTop1Set[] = new int[]{220,330,440};
      int buttonMarginTop2Set[] = new int[]{340,510,690};
      int buttonMarginRightSet[] = new int[]{20,30,40 };
      int buttonSpaceSet[] = new int[]{90,135,180};
      int buttonHalfSpaceSet[] = new int[]{45,62,90};
      int buttonTextMarginTopSet[] = new int[]{15,24,30};

      this.buttonImageWidth = buttonImageWidthSet[displayIndex];
      this.buttonMarginTop1 = buttonMarginTop1Set[displayIndex];
      this.buttonMarginTop2 = buttonMarginTop2Set[displayIndex];
      this.buttonMarginRight = buttonMarginRightSet[displayIndex];
      this.buttonSpace = buttonSpaceSet[displayIndex];
      this.buttonHalfSpace = buttonHalfSpaceSet[displayIndex];
      this.buttonTextMarginTop = buttonTextMarginTopSet[displayIndex];
    }

    public PreviewParam(Parcel in) {
      readFromParcel(in);
    }

    @Override
    public int describeContents() {
      return 0;
    }

    private int xButtonDrawable;

    public void setxButtonDrawable(int res) {
      this.xButtonDrawable = res;
    }

    public int getxButtonDrawable() {
      return this.xButtonDrawable;
    }

    private int titleLayout;
    private PreviewText titleText;

    public void setTitleLayout(int layout) {
      this.titleLayout = layout;
    }

    public void setTitleText(PreviewText text) {
      this.titleText = text;
    }

    public int getTitleLayout() {
      return this.titleLayout;
    }

    public PreviewText getTitleText() {
      return this.titleText;
    }

    private float previewFrameRatio;

    public void setPreviewFrameRatio(float ratio) {
      this.previewFrameRatio = ratio;
    }

    public float getPreviewFrameRatio() {
      return this.previewFrameRatio;
    }

    private int previewFrameLineWidth;

    public void setPreviewFrameLineWidth(int width) {
      this.previewFrameLineWidth = width;
    }

    public int getPreviewFrameLineWidth() {
      return this.previewFrameLineWidth;
    }

    private int previewFrameColor;

    public void setPreviewFrameColor(int color) {
      this.previewFrameColor = color;
    }

    public int getPreviewFrameColor() {
      return this.previewFrameColor;
    }

    private int buttonLayout;

    public void setButtonLayout(int layout) {
      this.buttonLayout = layout;
    }

    public int getButtonLayout() {
      return this.buttonLayout;
    }

    private PreviewText button1Text;

    public void setButton1Text(PreviewText text) {
      this.button1Text = text;
    }

    public PreviewText getButton1Text() {
      return this.button1Text;
    }

    private PreviewText button2Text;

    public void setButton2Text(PreviewText text) {
      this.button2Text = text;
    }

    public PreviewText getButton2Text() {
      return this.button2Text;
    }

    private PreviewText buttonXText;

    public void setButtonXText(PreviewText text) {
      this.buttonXText = text;
    }

    public PreviewText getButtonXText() {
      return this.buttonXText;
    }

    private int buttonImageWidth;
    public void setButtonImageWidth(int v) { this.buttonImageWidth = v; }
    public int getButtonImageWidth() {return this.convertPixelsToDp(this.buttonImageWidth);}

    private int button1Drawable;
    private int button2Drawable;

    public int getButton1Drawable() {
      return this.button1Drawable;
    }

    public int getButton2Drawable() {
      return this.button2Drawable;
    }

    public void setButton1Drawable(int d) {
      this.button1Drawable = d;
    }

    public void setButton2Drawable(int d) {
      this.button2Drawable = d;
    }

    private int titleMarginLeft;
    private int titleMarginTop;
    private int titleMarginBottom;
    private int titleHeight;

    public void setTitleMarginLeft(int v) { this.titleMarginLeft = v;}
    public void setTitleMarginTop(int v) { this.titleMarginTop = v;}
    public void setTitleMarginBottom(int v) { this.titleMarginBottom = v;}
    public void setTitleMarginHeight(int v) { this.titleHeight = v;}

    public int getTitleMarginLeft() { return this.convertPixelsToDp(this.titleMarginLeft); }
    public int getTitleMarginTop() { return this.convertPixelsToDp(this.titleMarginTop); }
    public int getTitleMarginBottom() { return this.convertPixelsToDp(this.titleMarginBottom); }
    public int getTitleMarginHeight() { return this.convertPixelsToDp(this.titleHeight); }

    private int frameMarginLeft;
    private int frameMarginBottom;

    public void setFrameMarginLeft(int v) { this.frameMarginLeft = v; }
    public void setFrameMarginBottom(int v) { this.frameMarginBottom = v; }

    public int getFrameMarginLeft() { return this.convertPixelsToDp(this.frameMarginLeft); }
    public int getFrameMarginBottom() { return this.convertPixelsToDp(this.frameMarginBottom); }

    private int buttonMarginTop1, buttonMarginTop2;
    private int buttonMarginRight;
    private int buttonSpace;
    private int buttonHalfSpace;
    private int buttonTextMarginTop;

    public void setButtonMarginTop1(int v) { this.buttonMarginTop1 = v; }
    public void setButtonMarginTop2(int v) { this.buttonMarginTop2 = v; }
    public void setButtonMarginRight(int v) { this.buttonMarginRight = v; }
    public void setButtonSpace(int v) { this.buttonSpace = v; }
    public void setButtonHalfSpace(int v) { this.buttonHalfSpace = v; }
    public void setButtonTextMarginTop(int v) { this.buttonTextMarginTop = v; }

    public int getButtonMarginTop1() { return this.convertPixelsToDp(this.buttonMarginTop1); }
    public int getButtonMarginTop2() { return this.convertPixelsToDp(this.buttonMarginTop2); }
    public int getButtonMarginRight() { return this.convertPixelsToDp(this.buttonMarginRight); }
    public int getButtonSpace() { return this.convertPixelsToDp(this.buttonSpace); }
    public int getButtonHalfSpace() { return this.convertPixelsToDp(this
        .buttonHalfSpace); }
    public int getButtonTextMarginTop() { return this.convertPixelsToDp(this.buttonTextMarginTop); }

    private static Context mContext;

    public static int convertPixelsToDp(int px){
      return px;
      /*Resources resources = mContext.getResources();
      DisplayMetrics metrics = resources.getDisplayMetrics();
      float dp = (float)px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
      return (int)dp;*/
    }

    public int pxToDp(int px) {
      Resources resources = mContext.getResources();
      DisplayMetrics metrics = resources.getDisplayMetrics();
      float dp = (float)px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
      return (int)dp;
    }

    public static int spToPx(int sp) {
      return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, mContext.getResources().getDisplayMetrics());
    }

    public int spToDp(int sp) {
      return this.convertPixelsToDp(this.spToPx(sp));
    }

    public int pxToSp(int px) {
      float sp = px / mContext.getResources().getDisplayMetrics().scaledDensity;
      return (int) sp;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      dest.writeInt(this.titleLayout);
      dest.writeParcelable((Parcelable) this.titleText, 0);
      dest.writeInt(this.xButtonDrawable);

      dest.writeFloat(this.previewFrameRatio);
      dest.writeInt(this.previewFrameLineWidth);
      dest.writeInt(this.previewFrameColor);

      dest.writeInt(this.buttonLayout);
      dest.writeParcelable((Parcelable) this.button1Text, 0);

      dest.writeParcelable((Parcelable) this.button2Text, 0);

      dest.writeInt(this.button1Drawable);
      dest.writeInt(this.button2Drawable);

      dest.writeInt(this.titleMarginLeft);
      dest.writeInt(this.titleMarginTop);
      dest.writeInt(this.titleMarginBottom);
      dest.writeInt(this.titleHeight);

      dest.writeInt(this.frameMarginLeft);
      dest.writeInt(this.frameMarginBottom);

      dest.writeInt(this.buttonMarginTop1);
      dest.writeInt(this.buttonMarginTop2);
      dest.writeInt(this.buttonMarginRight);
      dest.writeInt(this.buttonSpace);
      dest.writeInt(this.buttonTextMarginTop);

      dest.writeInt(this.buttonImageWidth);
    }

    private void readFromParcel(Parcel in) {
      this.titleLayout = in.readInt();
      this.titleText = in.readParcelable(PreviewText.class.getClassLoader());
      this.xButtonDrawable = in.readInt();

      this.previewFrameRatio = in.readFloat();
      this.previewFrameLineWidth = in.readInt();
      this.previewFrameColor = in.readInt();

      this.buttonLayout = in.readInt();
      this.button1Text = in.readParcelable(PreviewText.class.getClassLoader());

      this.button2Text = in.readParcelable(PreviewText.class.getClassLoader());

      this.button1Drawable = in.readInt();
      this.button2Drawable = in.readInt();

      this.titleMarginLeft = in.readInt();
      this.titleMarginTop = in.readInt();
      this.titleMarginBottom = in.readInt();
      this.titleHeight = in.readInt();

      this.frameMarginLeft = in.readInt();
      this.frameMarginBottom = in.readInt();

      this.buttonMarginTop1 = in.readInt();
      this.buttonMarginTop2 = in.readInt();
      this.buttonMarginRight = in.readInt();
      this.buttonSpace = in.readInt();
      this.buttonTextMarginTop = in.readInt();
      this.buttonImageWidth = in.readInt();
    }
  }

}
