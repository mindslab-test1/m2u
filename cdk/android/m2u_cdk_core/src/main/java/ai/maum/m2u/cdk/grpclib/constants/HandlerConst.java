package ai.maum.m2u.cdk.grpclib.constants;

/**
 * 핸들러에서 사용하는 Constant 들을 선언한 클래스 입니다.
 */

public class HandlerConst {

  /**
   * Observer pattern 에서 사용하는 Update case 들을 선언한 Constant sub 클래스 입니다.
   */

  public static class UIUpdater {

    /**
     * MAP(jar) -> Native
     */
    // MAP 으로부터 수신받은 경우에 대한 updatecase
    public static final int UI_FROM_MAP_ONNEXT = 0x5001;
    // MAP 으로부터 수신받은 이후 completed 함수 호출된 경우에 대한 udpatecase
    public static final int UI_FROM_MAP_ONCOMPLETED = 0x5002;
    // grpc 에러 발생에 대한 udpatecase (GrpcStatus)
    public static final int UI_FROM_MAP_ONERROR = 0x5003;
    public static final int UI_FROM_MAP_ERROR = 0x5004;
    // MAP 으로부터 수신받은 MapException에 대한 updatecase(receiveException)
    public static final int UI_FROM_MAPEXCEPTION_ERROR = 0x5005;
    public static final int UI_FROM_MAP_PONG = 0x5006;
    /**
     * JS -> Native
     */
    // JS 로 부터 setMapSetting 호출된 경우에 대한 updatecase
    public static final int UI_FROM_JS_SETMAP_SETTING = 0x6001;
    // JS 로 부터 sendEvent 가 호출된 경우에 대한 updatecase
    public static final int UI_FROM_JS_SENDEVENT = 0x6002;
    public static final int UI_FROM_JS_SENDEVENTURL = 0x6003;
    // JS 로 부터 toast 를 보여줘야 될 경우에 대한 updatecase
    public static final int UI_FROM_JS_SHOWTOAST = 0x6004;
    // JS 로 부터 카메라 오픈 명령을 받은 경우에 대한 updatecase
    public static final int UI_FROM_JS_OPENCAMERA = 0x6005;
    // JS 로 부터 카메라 닫기 명령을 받은 경우에 대한 updatecase
    public static final int UI_FROM_JS_CLOSECAMERA = 0x6006;
    // JS 로 부터 겔러리 오픈 명령을 받은 경우에 대한 updatecase
    public static final int UI_FROM_JS_OPENGALLERY = 0x6007;
    // JS 로 부터 겔러리 닫기 명령을 받은 경우에 대한 updatecase
    public static final int UI_FROM_JS_CLOSEGALLERY = 0x6008;
    // JS 로 부터 마이크 오픈 명령을 받은 경우에 대한 updatecase
    public static final int UI_FROM_JS_OPENMICROPHONE = 0x6009;
    // JS 로 부터 마이크 닫기 명령을 받은 경우에 대한 updatecase
    public static final int UI_FROM_JS_CLOSEMICROPHONE = 0x6010;
    // JS 로 부터 스피커 닫기 명령을 받은 경우에 대한 updatecase
    public static final int UI_FROM_JS_CLOSESPEAKER = 0x6011;
    // JS 로 부터 위치 정보 요청 명령을 받은 경우에 대한 updatecase
    public static final int UI_FROM_JS_GETLOCATION = 0x6012;
    // JS 에게 카메라 상태 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_TO_JS_CAMERASTATUS = 0x7001;
    // JS 에게 겔러리 상태 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_TO_JS_GALLERYSTATUS = 0x7002;
    // JS 에게 마이크 상태 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_TO_JS_MICROPHONESTATUS = 0x7003;
    // JS 에게 스피커 상태 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_TO_JS_SPEAKERSTATUS = 0x7004;
    // JS 에게 마이크 레코딩 시작 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_TO_JS_MICROPHONE_RECORDINGSTATUS = 0x7005;
    // JS 에게 streamSend 에 대한 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_TO_JS_STREAMSENT = 0x7006;
    // JS 에게 streamReceive 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_TO_JS_RECEIVESTREAM = 0x7007;
    // JS 에게 위치 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_TO_JS_LOCATIONSTATUS = 0x7008;
    // JS 에게 마이크 상태 정보를 전달하는 경우에 대한 updatecase
    public static final int UI_FROM_ETC_CALLSTATUS = 0x8001;
  }
}
