package ai.maum.m2u.cdk.grpclib.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import java.lang.reflect.Field;

/**
 * Bitmap 또는 Image 의 Rotation 등을 처리하기 위한 함수들로 구성된 클래스 입니다.
 * 함수들은 외부에서 쉽게 접근 가능하도록 static 으로 정의되어 있습니다.
 */
public class CommUtil {

  private static final String TAG = CommUtil.class.getSimpleName();

  /**
   * Image 의 Exif 에 대한 Rotation 정보를 리턴하는 함수입니다.
   *
   * @param exif Image 에 대한 Exif interface
   * @return Image의 exif 에 대한 rotation degree 를 리턴
   */
  public static int getExifOrientation(ExifInterface exif) {
    int degree = 0;
    if (exif != null) {
      for (Field f : ExifInterface.class.getFields()) {
        String name = f.getName();
        if (!name.startsWith("TAG_")) {
          continue;
        }
        String key = null;
        try {
          key = (String) f.get(null);
        } catch (NullPointerException e) {
          System.out.println("예외발생");
          continue;
        } catch (IllegalArgumentException e) {
          System.out.println("예외발생");
          continue;
        } catch (IllegalAccessException e) {
          System.out.println("예외발생");
          continue;
        }
        if (key == null) {
          continue;
        }
      }
      int orientation = exif
          .getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

      if (orientation != -1) {
        // We only recognize a subset of orientation tag values.
        switch (orientation) {
          case ExifInterface.ORIENTATION_ROTATE_90:
            degree = 90;
            break;
          case ExifInterface.ORIENTATION_ROTATE_180:
            degree = 180;
            break;
          case ExifInterface.ORIENTATION_ROTATE_270:
            degree = 270;
            break;
          default:
            LogUtil.e(TAG, "default orientation : " + orientation);
            break;
        }
      }
    }
    return degree;
  }

  /**
   * Bitmap 이미지를 특정 degree 로 rotation 시키는 함수입니다.
   *
   * @param b Bitmap image 데이터
   * @param degrees Rotation 시킬 각
   * @return Rotation 된 Bitmap image 데이터
   */
  public static Bitmap rotate(Bitmap b, int degrees) {
    if (degrees != 0 && b != null) {
      Matrix m = new Matrix();
      m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight()
          / 2);
      try {
        Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(),
            b.getHeight(), m, true);
        if (b != b2) {
          b.recycle();
          b = b2;
        }
      } catch (OutOfMemoryError ex) {
        // We have no memory to rotate. Return the original bitmap.
        LogUtil.e(TAG, ex.getMessage());
      }
    }
    return b;
  }
}
