package ai.maum.m2u.cdk.grpclib.agent;

import ai.maum.m2u.cdk.grpclib.constants.HandlerConst;
import ai.maum.m2u.cdk.grpclib.ui.updater.ExecuteUpdater;
import ai.maum.m2u.cdk.grpclib.utils.G711;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import java.util.ArrayList;
import android.util.Log;

/**
 * speaker 로 byteStream 을 출력하기 위해 AudioTrack 을 초기화 또는 해제하고, 출력해 주는 함수들로 구성된
 * 클래스 입니다.
 */

public class SoundPlayAgent {

  private static final String TAG = SoundPlayAgent.class.getSimpleName();
  private static Context mContext;
  public static final AudioTrack audioTrack;
  public static final ENCODING_TYPE mEncodingType = ENCODING_TYPE.LINEAR8;
  private int sampleRate = 16000;
  private String encoding = "LINEAR16";
  private int bufferSize;
  private boolean isPlaying = false;
  private int writePosition_ = 0;
  private short[] ab_short = null;
  private byte[] ab_byte = null;
  private ArrayList<short[]> replies_short = null;
  private ArrayList<byte[]> replies_byte = null;
  private Thread playingThread = null;
  private int bufferSizeShort = 0;
  private int prevHeadPosition_ = 0;

  /**
   * 클래스 생성자 입니다.
   */
  private SoundPlayAgent() {
    // singleton
  }

  /**
   * microphone 객체에 접근하기 위해 Singletone instance 를 리턴 해주는 함수입니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   * @return Singletone instance
   */
  public static SoundPlayAgent getInstance(Context context) {
    mContext = context;
    return SingletonHolder.INSTANCE;
  }

  /**
   * speaker 를 초기화 시켜주는 함수입니다.
   *
   * @param sampleRate sampleRate : 16000
   * @param encoding encoding : LINEAR16 또는 MULAW
   */
  public void init(int sampleRate, String encoding) {
    release();

    this.encoding = encoding;
    this.sampleRate = sampleRate;

    int audioFormat = AudioFormat.ENCODING_PCM_8BIT;
    if (encoding.equalsIgnoreCase("LINEAR16") || encoding
        .equalsIgnoreCase("MULAW")) {
      if (encoding.equalsIgnoreCase("LINEAR16")) {
        mEncodingType = ENCODING_TYPE.LINEAR16;
      } else if (encoding.equalsIgnoreCase("MULAW")) {
        mEncodingType = ENCODING_TYPE.MULAW;
      } else if (encoding.equalsIgnoreCase("MALAW")) {
        mEncodingType = ENCODING_TYPE.MALAW;
      }
      audioFormat = AudioFormat.ENCODING_PCM_16BIT;
    }
    bufferSize = AudioTrack.getMinBufferSize(sampleRate,
        AudioFormat.CHANNEL_OUT_MONO,
        audioFormat);
    LogUtil.e(TAG, "> init : buffersize = " + bufferSize);
    if (bufferSize < 8000) {
      bufferSize = 8000;
    }
    bufferSizeShort = bufferSize / 2;
    audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate,
        AudioFormat.CHANNEL_OUT_MONO, audioFormat, bufferSize * 4, AudioTrack
        .MODE_STREAM);
    audioTrack.play();

    replies_byte = new ArrayList<byte[]>();
    ab_byte = new byte[bufferSize];
    replies_short = new ArrayList<short[]>();
    ab_short = new short[bufferSizeShort];
    isPlaying = true;

    playingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        playLoop();
      }
    }, "AudioPlay Thread");
    playingThread.start();
  }

  /**
   * speaker 로 출력 될 byteStream 데이터 출력해 주는 함수입니다.
   */
  private void playLoop() {
    boolean waiting = true;
    if (mEncodingType == ENCODING_TYPE.LINEAR16) {
      while (playingThread != null && replies_byte.size() == 0) {
        sleep(10);
      }
      while (isPlaying) {
        if ((replies_byte.size() > 0 && !waiting) || replies_byte.size() > 1) {
          waiting = false;
          byte[] data = replies_byte.get(0);
          audioTrack.write(data, 0, data.length);
          replies_byte.remove(0);
        } else {
          sleep(200);
          if (replies_byte.size() == 0) { // Audio list is empty
            waiting = true;
            if (prevHeadPosition_ == audioTrack.getPlaybackHeadPosition()) {
              break;
            }
            prevHeadPosition_ = audioTrack.getPlaybackHeadPosition();
          }
        }
      }
    } else {
      while (playingThread != null && replies_short.size() == 0) {
        sleep(10);
      }
      while (isPlaying) {
        if ((replies_short.size() > 0 && !waiting)
            || replies_short.size() > 1) {
          waiting = false;
          short[] data = replies_short.get(0);
          audioTrack.write(data, 0, data.length);
          replies_short.remove(0);
        } else {
          sleep(200);
          if (replies_short.size() == 0) { // Audio list is empty
            waiting = true;
            if (prevHeadPosition_ == audioTrack.getPlaybackHeadPosition()) {
              break;
            }
            prevHeadPosition_ = audioTrack.getPlaybackHeadPosition();
          }
        }
      }
    }
    audioTrack.stop();
    audioTrack.release();
    audioTrack = null;
    replies_short.clear();
    replies_byte.clear();
    sleep(100);
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_CLOSESPEAKER, null,
        null, 0, false, null, null, null);
  }

  /**
   * speaker 로 출력 될 byteStream 데이터를 byte[] 의 array 에 add 해 주는 함수입니다.
   *
   * @param bytes speaker 로 출력 될 byteStream 데이터
   */
  public void addPlayData(byte[] bytes) {
    if (mEncodingType == ENCODING_TYPE.LINEAR16) {
      LogUtil.e(TAG, "#@ addPlayData mEncodingType LINEAR16");

      for (int i = 0; i < bytes.length; ++i) {
        ab_byte[writePosition_++] = bytes[i];
        if (writePosition_ == bufferSize) {
          writePosition_ = 0;
          replies_byte.add(ab_byte);
          ab_byte = new byte[bufferSize];
        }
      }
    } else {
      short data[] = new short[bytes.length];
      for (int i = 0; i < bytes.length; ++i) {
        if (mEncodingType == ENCODING_TYPE.MULAW) {
          data[i] = (short) G711.ulaw2linear(bytes[i]);
        } else {
          data[i] = (short) G711.alaw2linear(bytes[i]);
        }
        ab_short[writePosition_++] = data[i];
        if (writePosition_ == bufferSizeShort) {
          writePosition_ = 0;
          replies_short.add(ab_short);
          ab_short = new short[bufferSizeShort];
        }
      }
    }
  }

  void sleep(long interval) {
    try {
      Thread.sleep(interval);
    } catch (InterruptedException e) {
      Log.e(TAG, "addPlayData e", e);
    }
  }

  /**
   * speaker 로 출력 될 byteStream 데이터 출력해 주는 함수입니다.
   *
   * @param data speaker 로 출력 될 byteStream 데이터
   * @param size byteStream 의 크기
   */
  public boolean play(byte[] data, int size) {

    if (audioTrack != null) {
      try {
        int ret = 0;
        if (mEncodingType == ENCODING_TYPE.LINEAR16) {
          ret = audioTrack.write(data, 0, size);
        } else {
          short dataShort[] = new short[data.length];
          if (mEncodingType == ENCODING_TYPE.MULAW) {
            for (int i = 0; i < data.length; ++i) {
              dataShort[i] = (short) G711.ulaw2linear(data[i]);
              if (i == data.length - 1) {
                ret = audioTrack.write(dataShort, 0, dataShort.length);
              }
            }
          } else {
            for (int i = 0; i < data.length; ++i) {
              dataShort[i] = (short) G711.alaw2linear(data[i]);
              if (i == data.length - 1) {
                ret = audioTrack.write(dataShort, 0, dataShort.length);
              }
            }
          }
        }

        switch (ret) {
          case AudioTrack.ERROR_INVALID_OPERATION:
            LogUtil.e(TAG, "play fail: ERROR_INVALID_OPERATION");
            return false;
          case AudioTrack.ERROR_BAD_VALUE:
            LogUtil.e(TAG, "play fail: ERROR_BAD_VALUE");
            return false;
          case AudioManager.ERROR_DEAD_OBJECT:
            LogUtil.e(TAG, "play fail: ERROR_DEAD_OBJECT");
            return false;
          default:
            return true;
        }
      } catch (IllegalStateException e) {
        LogUtil.e(TAG, "play fail: " + e.getMessage());
        return false;
      }
    }
    LogUtil.e(TAG, "play fail: null audioTrack");
    return false;
  }

  /**
   * playThread 가 동작중이면 멈추고 speaker 객체를 해제시키는 함수입니다.
   */
  public void release() {
    isPlaying = false;
    playingThread = null;
  }

  /**
   * speaker 객체가 생성되었는지 확인하는 함수입니다.
   *
   * @return speaker 객체가 생성된 경우 true
   */
  public boolean isExistAudioTrackInstance() {
    return audioTrack == null ? false : true;
  }

  /**
   * UI 에 데이터를 전송하기 위해서 호출하는  함수입니다.
   *
   * @param updateCase 업데이트 종류
   * @param arg1 String variable
   * @param arg2 String variable
   * @param arg3 Integer variable
   * @param arg4 boolean variable
   * @param arg5 byte[] variable
   * @param obj1 Object variable
   * @param obj2 Object variable
   */
  private void runUiUpdater(int updateCase, String arg1, String arg2, int
      arg3, boolean arg4, byte[] arg5, Object obj1, Object obj2) {
    ExecuteUpdater updater = ExecuteUpdater.getInstance();
    updater.updateRun(updateCase, arg1, arg2, arg3, arg4, arg5, obj1, obj2);
  }

  /**
   * encoding type 에 대한 enum 입니다.
   */
  public enum ENCODING_TYPE {
    LINEAR8,
    LINEAR16,
    MULAW,
    MALAW
  }

  /**
   * Singletone instance 를 생성하는 클래스 입니다.
   */
  private static final class SingletonHolder {

    static final SoundPlayAgent INSTANCE = new SoundPlayAgent();
  }
}