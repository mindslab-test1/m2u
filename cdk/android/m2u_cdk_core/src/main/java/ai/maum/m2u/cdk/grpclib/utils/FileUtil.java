package ai.maum.m2u.cdk.grpclib.utils;

import ai.maum.m2u.cdk.grpclib.constants.Const;
import java.io.File;

/**
 * file 관련된 처리를 하는 클래스 입니다.
 */

public class FileUtil {
  private static final String TAG = FileUtil.class.getSimpleName();

  /**
   * path + fileName 이 존재하는지 체크하는 함수입니다.
   *
   * @param path 체크대상이 되는 path
   * @param fileName 체크대상이 되는 fileName
   * @return 존재하는 경우 true, 존재하지 않는 경우 false
   */
  public static boolean isFileExist(String path, String fileName) {
    File fileInfo = new File(path, fileName);
    if (!fileInfo.exists()) {
      return false;
    }
    return true;
  }

  /**
   * filePath 가 존재하는지 체크하는 함수입니다.
   *
   * @param filePath 체크대상이 되는 filePath
   * @return 존재하는 경우 true, 존재하지 않는 경우 false
   */
  public static boolean isFileExist(String filePath) {
    File fileInfo = new File(filePath);
    if (!fileInfo.exists()) {
      return false;
    }
    return true;
  }

  /**
   * path + filename 을 삭제하는 함수입니다.
   *
   * @param path file 이 속해 있는 path
   * @param fileName 삭제될 fileName
   */
  public static void delFile(String path, String fileName) {
    try {
      if (isFileExist(path, fileName)) {
        File fileInfo = new File(path, fileName);
        fileInfo.delete();
      }
    }
    catch(Exception e) {
    }
  }

  /**
   * file path 를 삭제하는 함수입니다.
   *
   * @param filePath 삭제될 file path
   */
  public static void delFile(String filePath) {
    try {
      if (isFileExist(filePath)) {
        File fileInfo = new File(filePath);
        fileInfo.delete();
      }
    }
    catch(Exception e) {
    }
  }

  public static void removeCameraFile() {
    delFile(Const.CAMERA_IMAGE_SAVE_PATH + Const.CAMERA_IMAGE_SAVE_FILE);
  }
}
