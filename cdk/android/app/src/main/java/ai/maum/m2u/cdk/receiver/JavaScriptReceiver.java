package ai.maum.m2u.cdk.receiver;

import ai.maum.m2u.cdk.grpclib.chatbot.GrpcClient;
import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst.UIUpdater;
import ai.maum.m2u.cdk.grpclib.ui.updater.ExecuteUpdater;
import ai.maum.m2u.cdk.grpclib.utils.StringUtil;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

/**
 * 이 클래스에는 아래의 함수들로 구성되어 있습니다.
 * <p>
 * 1. JS 에서 호출해 주는 함수 : Observer pattern을 사용해서 메인 화면으로 전달합니다.<br>
 * 2. Jar 의 함수를 호출해 주는 함수 : GrpcClient 클래스의 Singletone 을 사용합니다.<br>
 * 3. microphone, camera, gallery, gps 등을 제어하거나 상태값을 전달하기 위한 함수를 호출해주는 함수
 * </p>
 */

public class JavaScriptReceiver {

  private static final String TAG = JavaScriptReceiver.class.getSimpleName();
  Context mContext;
  Activity activity;

  /**
   * 클래스 생성자 입니다.
   *
   * @param context 함수를 호출하는 클래스의 context
   */
  public JavaScriptReceiver(Context context) {
    mContext = context;
    activity = (Activity) mContext;
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * MAP 서버와 통신하기 위해 필요한 정보 및 기타 정보를 전달 받습니다.
   *
   * @param setMapSettingJson MapSetting 객체의 json string
   */
  @JavascriptInterface
  public void setMapSetting(String setMapSettingJson) {
    Log.e(TAG, "setMapSetting : " + setMapSettingJson);
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_SETMAP_SETTING,
        setMapSettingJson, null, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * MAP 에 전달 될 json 형식의 eventStream 데이터를 전달 받습니다.
   *
   * @param eventStreamJson MAP 에 전달 될 json 형식의 eventStream
   */
  @JavascriptInterface
  public void sendEvent(String eventStreamJson) {
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_SENDEVENT, eventStreamJson,
        Const.EMPTY_STRING, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * MAP 에 전달 될 json 형식의 eventStream, Image 를 로드하기 위한 데이터를 전달 받습니다.
   *
   * @param eventStreamJson MAP 에 전달 될 json 형식의 eventStream
   * @param imageUrl 이미지를 전송해야 되는 경우 로드하기 위한 image 의 경로
   */
  @JavascriptInterface
  public void sendEvent(String eventStreamJson, String imageUrl) {
    String imageurl = imageUrl;
    if (StringUtil.isEmptyString(imageUrl) || imageUrl
        .equalsIgnoreCase("undefined")) {
      imageurl = Const.EMPTY_STRING;
    }
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_SENDEVENT,
        eventStreamJson, imageurl, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * microphone 을 통한 발화가 필요한 경우 microphone 을 open 하기 위해 호출되며
   * open 에 필요한 정보를 전달받습니다.
   *
   * @param microphoneParamJson MicrophoneParam 객체의 json string
   */
  @JavascriptInterface
  public void openMicrophone(String microphoneParamJson) {
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_OPENMICROPHONE,
        microphoneParamJson, null, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * microphone 을 통한 발화가 필요한 경우 호출되며 open 에 필요한 정보를 전달받습니다.
   */
  @JavascriptInterface
  public void closeMicrophone() {
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_CLOSEMICROPHONE, null,
        null, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * speaker 를 close 시킬 때 호출됩니다.
   */
  @JavascriptInterface
  public void closeSpeaker() {
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_CLOSESPEAKER, null,
        null, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * MAP 에 image 를 전송하기 위해 camera 를 open 해야 되는 경우 호출됩니다.
   *
   * @param cameraParamJson CameraParam 객체의 json string
   */
  @JavascriptInterface
  public void openCamera(String cameraParamJson) {
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_OPENCAMERA, cameraParamJson,
        null, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * camera 를 close 시킬 때 호출됩니다. camera preview/crop 화면이 보여지고 있는경우
   * 화면을 종료시킵니다.
   */
  @JavascriptInterface
  public void closeCamera() {
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_CLOSECAMERA, null, null,
        0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * MAP 에 image 를 전송하기 위해 gallery 화면을 open 해야 되는 경우 호출됩니다.
   *
   * @param galleryParamJson GalleryParam 객체의 json string
   */
  @JavascriptInterface
  public void openGallery(String galleryParamJson) {
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_OPENGALLERY,
        galleryParamJson, null, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * gallery 를 close 시킬 때 호출됩니다. gallery 화면이 보여지고 있는경우 화면을 종료시킵니다.
   */
  @JavascriptInterface
  public void closeGallery() {
    runUiUpdater(HandlerConst.UIUpdater.UI_FROM_JS_CLOSEGALLERY, null,
        null, 0, false, null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * GPS 정보를 요청 하는 경우 호출됩니다.
   */
  @JavascriptInterface
  public void getLocation() {
    runUiUpdater(UIUpdater.UI_FROM_JS_GETLOCATION, null, null, 0, false,
        null, null, null);
  }

  /**
   * JS 에서 호출해 주는 함수입니다.
   * <br>
   * Phone Number 정보를 요청 하는 경우 호출됩니다.
   * getPhoneNumber 호출을 호출 하면 바로 String 형태로 전화번호가 전달됩니다.
   * Phone Number를 획득하지 못한 경우 ""를 전달합니다.
   */
  @JavascriptInterface
  public String getPhoneNumber() {
    String tempPhoneNum = GrpcClient.getInstance(mContext).processGetPhoneNumber();
    Log.e(TAG, "getPhoneNumber :" + tempPhoneNum);

    return tempPhoneNum;
  }

  /**
   * UI 에 데이터를 전송하기 위해서 호출하는  함수입니다.
   *
   * @param updateCase 업데이트 종류
   * @param arg1 String variable
   * @param arg2 String variable
   * @param arg3 Integer variable
   * @param arg4 boolean variable
   * @param arg5 byte[] variable
   * @param obj1 Object variable
   * @param obj2 Object variable
   */
  private void runUiUpdater(int updateCase, String arg1, String arg2, int
      arg3, boolean arg4, byte[] arg5, Object obj1, Object obj2) {
    ExecuteUpdater updater = ExecuteUpdater.getInstance();
    updater.updateRun(updateCase, arg1, arg2, arg3, arg4, arg5, obj1, obj2);
  }
}
