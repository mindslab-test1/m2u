package ai.maum.m2u.cdk.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import ai.maum.m2u.cdk.R;
import ai.maum.m2u.cdk.grpclib.agent.DeviceAgentManager;
import ai.maum.m2u.cdk.grpclib.chatbot.GrpcClient;
import ai.maum.m2u.cdk.grpclib.chatbot.GrpcClient.TIMER_KIND;
import ai.maum.m2u.cdk.grpclib.chatbot.common.PreferenceAgent;
import ai.maum.m2u.cdk.grpclib.chatbot.map.MapInterfaceName;
import ai.maum.m2u.cdk.grpclib.chatbot.map.MapOperationName;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst.MapKeys;
import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.constants.Const.MultiMediaType;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst.UIUpdater;
import ai.maum.m2u.cdk.grpclib.ui.dto.CameraDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.CameraDTO.CameraEvent;
import ai.maum.m2u.cdk.grpclib.ui.dto.CameraDTO.CameraFailure;
import ai.maum.m2u.cdk.grpclib.ui.dto.EtcDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.EtcDTO.Streamingtype;
import ai.maum.m2u.cdk.grpclib.ui.dto.GalleryDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.GalleryDTO.GalleryEvent;
import ai.maum.m2u.cdk.grpclib.ui.dto.GalleryDTO.GalleryFailure;
import ai.maum.m2u.cdk.grpclib.ui.dto.LocationDTO.LocationFailure;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO.DirectiveStreamInfo;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO.MapDirectiveInfo;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO.MapEventInfo;
import ai.maum.m2u.cdk.grpclib.ui.dto.MicrophoneDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.MicrophoneDTO.MicrophoneEvent;
import ai.maum.m2u.cdk.grpclib.ui.dto.MicrophoneDTO.MicrophoneFailure;
import ai.maum.m2u.cdk.grpclib.ui.dto.StreamingStatusDTO;
import ai.maum.m2u.cdk.grpclib.ui.updater.ExecuteUpdater;
import ai.maum.m2u.cdk.grpclib.ui.updater.ObserverForUpdate;
import ai.maum.m2u.cdk.grpclib.ui.view.ToastView;
import ai.maum.m2u.cdk.grpclib.utils.BitmapUtil;
import ai.maum.m2u.cdk.grpclib.utils.GsonUtil;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import ai.maum.m2u.cdk.grpclib.utils.MapEventDirectiveParser;
import ai.maum.m2u.cdk.grpclib.utils.NetworkCheck;
import ai.maum.m2u.cdk.grpclib.utils.StringUtil;
import ai.maum.m2u.cdk.receiver.JavaScriptReceiver;
import maum.m2u.map.Map.MapDirective;

public class MainActivity extends BaseActivity implements ObserverForUpdate {

  private static final String TAG = MainActivity.class.getSimpleName();
  private final int REQUEST_ATTACH_IMAGE = 1113;
  private final int REQUEST_ATTACH_CAPTURE = 1115;
  private final int REQUEST_ATTACH_CROP = 1116;

  private WebView webView;
  private String embed_link = "file:///android_asset/index.html";
  private JavaScriptReceiver javaScriptReceiver;
  private boolean mWebViewLoadFinished = false;
  private Uri mImageCaptureUri = null;
  private String mCurrentPhotoPath;//실제 사진 파일 경로
  private String mCurrentStreamId = "";
  private String mCurrentSpeechStreamId = "";
  private String mCurrentSpeechOperationSyncId = "";
  private String mCurrentImageStreamId = "";
  private String mCurrentOperationSyncId = "";
  private String mCurrentOperationName = "";
  private int mMultimediaType = Const.MultiMediaType.NONE;

  @SuppressLint("AddJavascriptInterface")
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mContext = this;
    mMyPackageName = mContext.getPackageName();
    webView = (WebView) findViewById(R.id.webView);
    /**
     * 화면 업데이트 및 데이터 전송을 위해 Observer pattern 에 등록
     */
    ExecuteUpdater updater = ExecuteUpdater.getInstance();
    updater.addObserver(this);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    final CharSequence title = toolbar.getTitle();
    final ArrayList<View> outViews = new ArrayList<>(1);
    toolbar.findViewsWithText(outViews, title, View.FIND_VIEWS_WITH_TEXT);
    if (!outViews.isEmpty()) {
      final TextView titleView = (TextView) outViews.get(0);
      titleView.setGravity(Gravity.CENTER_HORIZONTAL);
      final Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) titleView
          .getLayoutParams();
      layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
      layoutParams.setMargins(0, 0, 60, 0);
      toolbar.requestLayout();
    }
    getSupportActionBar().setTitle(getString(R.string.chatbot_mtoucdk));
  }

  /**
   * WebView 를 초기화 하고 WebPage 를 로딩하는 함수입니다.
   *
   * @param webUrl WebPage url
   */
  private void startWebView(String webUrl) {

    if (webView == null) {
      return;
    }

    webView.setWebChromeClient(new WebChromeClient() {
    });
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      webView.setWebContentsDebuggingEnabled(Const.IS_WEBVIEW_DEBUGGING);
    }
    webView.getSettings().setDomStorageEnabled(true);
    webView.getSettings().setJavaScriptEnabled(true);
    webView.getSettings().setTextZoom(100);
    webView.getSettings().setAllowFileAccess(true);
    webView.getSettings().setAllowContentAccess(true);
    if (NetworkCheck.isNetworkAvailable(this)) {
      if (javaScriptReceiver == null) {
        javaScriptReceiver = new JavaScriptReceiver(this);
      }
      webView.addJavascriptInterface(javaScriptReceiver, "m2uWebViewNative");
      webView.loadUrl(webUrl);
    }
    webView.setWebViewClient(new WebViewClient() {
      @SuppressWarnings("deprecation")
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, final String
          requestUrl) {
        return true;
      }

      @TargetApi(android.os.Build.VERSION_CODES.M)
      @Override
      public boolean shouldOverrideUrlLoading(WebView view,
          WebResourceRequest req) {
        // Redirect to deprecated method, so you can use it in all SDK versions
        shouldOverrideUrlLoading(view, req.getUrl().toString());
        return true;
      }

      @Override
      public void onPageStarted(WebView view, String url, Bitmap facIcon) {

      }

      public void onPageFinished(WebView view, String url) {
        mWebViewLoadFinished = true;
      }

      @SuppressWarnings("deprecation")
      @Override
      public void onReceivedError(WebView view, int errorCode, String
          description, String failingUrl) {
      }

      @TargetApi(android.os.Build.VERSION_CODES.M)
      @Override
      public void onReceivedError(WebView view, WebResourceRequest req,
          WebResourceError rerr) {
        // Redirect to deprecated method, so you can use it in all SDK versions
        onReceivedError(view, rerr.getErrorCode(), rerr.getDescription()
            .toString(), req.getUrl().toString());
      }
    });
  }

  /**
   * WebView 를 해제하는 함수입니다.
   */
  private void closeWebView() {
    if (webView != null) {
      webView.clearHistory();
      webView.clearCache(true);
      webView.clearView();
    }
  }

  /**
   * 화면 종료 시 사용했던 리소스를 해제 하는 함수입니다.
   * <p>
   * 1. WebView 해제 <br>
   * 2. 사용했던 TimerTask들 해제<br>
   * 3. gRPC shutdown<br>
   * 4. 화면 업데이트 및 데이터 전송을 위해 Observer pattern 에 등록된 observer 해제
   * </p>
   */
  @Override
  protected void onDestroy() {
    super.onDestroy();
    closeWebView();
    GrpcClient.getInstance(mContext).stopTimerTask(TIMER_KIND.TIMER_ALL);
    // MAP 과의 통신을 종료시킵니다.
    GrpcClient.getInstance(mContext).shutDownGrcp();
    ExecuteUpdater updater = ExecuteUpdater.getInstance();
    if (updater != null) {
      updater.removeObserver(this);
      updater = null;
    }
  }

  /**
   * 화면이 아래로 내려가는 경우 처리하는 함수입니다.
   * <p>
   * 1. microphone 이 open 상태일 수 있으므로 open 상태이면 close 시킵니다.<br>
   * 2. Camera, Gallery 의 화면과 관련이 없는 경우 MAP 에 Ping 을 요청하는 타이머를 start 시킵니다.
   * </p>
   */
  @Override
  protected void onPause() {
    super.onPause();
    GrpcClient.getInstance(mContext).microphoneRelese(true, MicrophoneEvent
        .CLOSE_SUCCESS, null);

    // 앱을 종료하거나 화면 전환시 스피커 출력을 닫는다
    GrpcClient.getInstance(mContext).speakerClose();

    if (mMultimediaType == Const.MultiMediaType.NORMAL
        || mMultimediaType == Const.MultiMediaType.NONE) {
      GrpcClient.getInstance(mContext).stopPingProcess();
    }
  }

  /**
   * 화면이 보여지는 경우 처리하는 함수입니다.
   * <p>
   * 1. Camera, Gallery 등의 화면과 관련이 없는 경우에 MAP 에 Ping 을 요청하는 타이머를 start 시킵니다.
   * <br>
   * 2. WebPage 가 로딩되지 않았으면 로딩합니다.
   * </p>
   */
  @Override
  protected void onResume() {
    super.onResume();
    if (javaScriptReceiver != null && mWebViewLoadFinished) {
      /**
       * Camera, Gallery 가 표였다가 사라질 때에는
       * MAP 에 Ping 을 요청하는 타이머를 start 시킵니다.
       */
      if (mMultimediaType == Const.MultiMediaType.NORMAL
          || mMultimediaType == Const.MultiMediaType.NONE) {
        GrpcClient.getInstance(mContext).startPingProcess();
      }
    } else {
      /**
       * Permission 체크 후 granted 되었을 때 WebView 설정 및 로딩 시작
       * denied 시 앱 종료
       */
      checkPermissions(PERMISSIONS_ALL, new PermissionEventListener() {
        @Override
        public void onGrantedPermission() {
          startWebView(embed_link);
        }

        @Override
        public void onDeniedPermission() {
          finish();
        }
      });
    }
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  protected void onStop() {
    super.onStop();

  }

  /**
   * ObserverForUpdate 인터페이스를 구현한 함수입니다.
   * <p>
   * updater.addObserver 함수를 호출하여 Observer 에 클래스 instance 를 추가하면 타 클래스에서
   * updateRun 함수로 호출할 때 전달한 arg 들을 인터페이스 함수를 통하여 받을 수 있습니다.<br>
   * updateCase 별로 구분하여 해당 case 에 해당하는 작업을 처리할 수 있습니다.
   * </p>
   * <br>
   * [UI_FROM_JS_SETMAP_SETTING]
   * <br>
   * JS 에서 setMapSetting() 함수를 호출하여 전달한 MapSetting 데이터를
   * Preference 에 저장합니다.
   * <br>
   * <br>
   * [UI_FROM_JS_SENDEVENT]
   * <br>
   * JS 에서 sendEvent() 함수를 호출하여 전달한 eventStream 데이터를
   * MAP 에 전송합니다.
   * <br>
   * - arg2 : null
   * <br>
   * STT, STS 의 경우 openMicrophone > sendEvent 순서로 호출되므로 openMicrophone 처리하는
   * 함수에서 Microphone 을 준비하고 이곳에서 Microphone 을 실행시킵니다.
   * <br>
   * - arg2 : not null
   * <br>
   * Camera, Gallery 에서 캡춰한 이미지의 url 을 받은 경우 이 Url 에 대한 image data 를 MAP 에
   * 전달합니다.
   * <br>
   * loadImageForSend() 함수에서 getByteParam() 함수를 호출하여 image data 를 byteStream
   * 으로 전송한 후 전송이 끝나면 streamEnd 함수를 호출합니다.
   * <br>
   * <br>
   * [UI_FROM_MAP_PONG]
   * <br>
   * JS 에서 setMapSetting() 함수를 호출하여 전달한 MapSetting 데이터에 pingInterval 값이 있으면 이
   * 주기마다 MAP 에 pingRequest 를 호출하고 MAP 으로부터 응답으로 받은 pongResponse 값을 JS 에게 전달해
   * 줍니다.
   * <br>
   * <br>
   * [UI_FROM_JS_OPENMICROPHONE]
   * <br>
   * JS 에서 openMicrophone() 함수를 호출하여 전달한 MicrophoneDTO.MicrophoneParam 데이터의
   * 설정값을 참고하여 microphone 을 open 시킵니다.
   * <br>
   * 실제 microphone 을 통한 발화는 JS 에서 sendEvent 함수를 호출하여 전달받은 SpeechToTextTalk,
   * SpeechToSpeechTalk eventStream 인 경우에 하게 됩니다.
   * <br>
   * <br>
   * [UI_FROM_JS_CLOSEMICROPHONE]
   * <br>
   * JS 에서 closeMicrophone() 함수를 호출하면 microphone 을 닫습니다.
   * <br>
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 microphone 을 닫을때 closeMicrophone() 함수가
   * 호출될 수 있습니다.
   * <br>
   * <br>
   * [UI_TO_JS_MICROPHONESTATUS]
   * <br>
   * 현재 microphone 의 상태를 JS 에게 전달합니다.
   * <br>
   * MicrophoneDTO.MicrophoneStatus 객체로 구성하여 JSON String 으로 전달합니다.
   * <br>
   * <br>
   * [UI_TO_JS_MICROPHONE_RECORDINGSTATUS]
   * <br>
   * microphone 으로 발화가 시작된 경우(recoding) STARTING_RECORD 이벤트를 JS 에게 전달합니다.
   * <br>
   * MicrophoneDTO.MicrophoneStatus 객체로 구성하여 JSON String 으로 전달합니다.
   * <br>
   * <br>
   * [UI_TO_JS_STREAMSENT]
   * <br>
   * microphone 을 통해 발화한 데이터를 MAP 에 전송하고, JS 에게 관련 정보를 전달합니다.
   * <br>
   * StreamingStatusDTO 객체로 구성하여 JSON String 으로 전달합니다.
   * <br>
   * microphone이 릴리즈된 경우에는 버퍼에 남아있는 byteStream이 있지만 streamEnd를 보낸 경우이므로 전송하지
   * 않는다.
   * <br>
   * <br>
   * [UI_TO_JS_RECEIVESTREAM]
   * <br>
   * MAP 으로 부터 byteStream, streamEnd 을 수신한 경우 JS 에게 byteStream 에 대한 정보를 전달해 줍니다.
   * <br>
   * StreamingStatusDTO 객체로 구성하여 JSON String 으로 전달합니다.
   * <br>
   * <br>
   * [UI_FROM_JS_OPENCAMERA]
   * <br>
   * JS 로 부터 openCamera() 함수가 호출된 경우 전달받은 CameraParam 데이터를 사용하여 camera 프리뷰
   * 화면을 실행합니다.
   * <br>
   * <br>
   * [UI_FROM_JS_CLOSECAMERA]
   * <br>
   * JS 로 부터 closeCamera() 함수가 호출된 경우 camera 프리뷰화면 또는 image crop 화면을
   * 종료시킵니다.
   * <br>
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 camera를 닫을때
   * closeCamera() 함수가 호출될 수 있습니다.
   * <br>
   * <br>
   * [UI_TO_JS_CAMERASTATUS]
   * <br>
   * 현재 camera 의 상태를 JS 에게 전달합니다.
   * <br>
   * CameraDTO.CameraStatus 객체로 구성하여 JSON String 으로 전달합니다.
   * <br>
   * <br>
   * [UI_FROM_JS_OPENGALLERY]
   * <br>
   * JS 로 부터 openGallery() 함수가 호출된 경우 전달받은 GalleryParam 데이터를 사용하여 Gallery 화면을
   * 실행합니다.
   * <br>
   * <br>
   * [UI_FROM_JS_CLOSEGALLERY]
   * <br>
   * JS 로 부터 closeGallery() 함수가 호출된 경우 Gallery 화면을 종료시킵니다.
   * <br>
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 Gallery 화면을 닫을때 closeGallery() 함수가 호출될 수
   * 있습니다.
   * <br>
   * <br>
   * [UI_TO_JS_GALLERYSTATUS]
   * <br>
   * 현재 Gallery 화면의 상태를 JS 에게 전달합니다.
   * <br>
   * GalleryDTO.GalleryStatus 객체로 구성하여 JSON String 으로 전달합니다.
   * <br>
   * <br>
   * [UI_FROM_JS_CLOSESPEAKER]
   * <br>
   * JS 에서 closeSpeaker() 함수를 호출하면 speaker 을 닫습니다.
   * <br>
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 speaker 을 닫을때 closeSpeaker() 함수가 호출될 수
   * 있습니다.
   * <br>
   * <br>
   * [UI_TO_JS_SPEAKERSTATUS]
   * <br>
   * 현재 speaker 의 상태를 JS 에게 전달합니다.
   * <br>
   * SpeakerDTO.SpeakerStatus 객체로 구성하여 JSON String 으로 전달합니다.
   * <br>
   * <br>
   * [UI_TO_JS_LOCATIONSTATUS]
   * <br>
   * 현재 GPS 정보를 JS 에게 전달합니다.
   * <br>
   * LocationDTO.LocationStatus 객체로 구성하여 JSON String 으로 전달합니다.
   * <br>
   * <br>
   * [UI_FROM_JS_GETLOCATION]
   * <br>
   * JS 에서 getLocation() 함수를 호출하면 현재 GPS 정보를 취득합니다.
   * <br>
   * JS 에서 GPS 정보가 필요할 때 비주기적으로 호출될 수 있습니다.
   * <br>
   * <br>
   * [UI_FROM_MAP_ONCOMPLETED]
   * <br>
   * MAP 에게 eventStream 을 전송하고 MAP 으로 부터 수신이 완료된 경우 JS 에게 전송된 operationSyncId
   * 값을 전달합니다.
   * <br>
   * <br>
   * [UI_FROM_MAP_ONERROR]
   * <br>
   * [UI_FROM_MAP_ERROR]
   * <br>
   * MAP 과 통신중에 gRPC 에러를 수신한 경우에 에러에 대한 정보를 GrpcStatusDTO 객체로 구성하여 JSON
   * String 으로 전달합니다.
   * <br>
   * 현재는 Throwable 메시지를 전달하고 있고, 상세 에러 내역을 추출하는 방법을 제공받은 후 그에 대한 수정이 필요합니다.
   * <br>
   * <br>
   * [UI_FROM_MAP_ONNEXT]
   * <br>
   * JS 에서 호출된 sendEvent() 함수를 통해 전달받은 eventStream 데이터를 MAP 에게 요청하고 이 요청에 대한
   * 응답(MapDirective)을 받은 경우 directive 종류에 따라 처리를 하고 directiveStream 인 경우 JS
   * 에게 전달해 줍니다.
   * <br>
   * - MapException <br>
   * Directive 로 MapException 이 들어오면 receiveException() 함수를 호출하여 MapException
   * 정보를 전달해 줍니다.<br>
   * - ByteStream <br>
   * MAP 으로 부터 byteStream 를 수신하면 이 데이터의 정보를 JS 에게 전달하고 AudioTrack으로 수신한
   * byteStream 을 재생합니다.<br>
   * - StreamEnd <br>
   * MAP 으로 부터 streamEnd 를 수신하면 streamEnd 에 대한 정보를 StreamingStatusDTO 객체로 구성한
   * JSON String 을 JS 에게 전달합니다.<br>
   * - DirectiveStream <br>
   * MAP 으로 부터 받은 MapDirective 가 directiveStream 인 경우 JS 에게 전달합니다.<br>
   * - DirectiveStream : "SpeechRecognizer" / "Recognized",
   * "EndpointDetected" <br>
   * MAP 으로 부터 Recognized 수신 시 또는 MAP 으로 부터 EndpointDetected 수신 시
   * VoiceRecorderAgent 를 stop 시키고 streamEnd 호출합니다.<br>
   * - DirectiveStream : "SpeechRecognizer" / "Recognizing"<br>
   * MicrophoneParam : timeoutInMillisecond 시간만큼 Timer를 연장합니다.
   * (startTimerForMic)<br>
   * - DirectiveStream : "SpeechRecognizer" / "Play"<br>
   * param / speechSynthesizerParam 에 있는 encoding, sampleRate 를 preference에
   * 저장합니다.<br>
   * AudioTrack을 준비 시키고 이후에 directiveStream 으로 byteStream 이 수신되면 이 데이터를
   * AudioTrack 으로 재생합니다.<br>
   * - DirectiveStream : "ExpectSpeech"<br>
   * operationName으로 ExpectSpeech를 받은 경우 directStream을 JS 에게 전달해 주고 JS 는
   * openMicrophone(param) 을 호출해서 음성인식 처리를 유도합니다.<br>
   * - DirectiveStream : "ExpectSpeechTimedOut"<br>
   * MAP 으로부터 수신한 directiveStream 이 ExpectSpeechTimedOut 이면 음성인식 실패한 경우이며, 이때
   * 마이크가 열려있으면 닫습니다.
   * <br>
   * <br>
   * [UI_FROM_ETC_CALLSTATUS]
   * <br>
   * broadcast receiver 에서 수신한 call status 에 대한 처리를 합니다.
   * <br>
   *
   * @param updateCase 업데이트를 구분하는 case값
   * @param arg1 전달받은 String
   * @param arg2 전달받은 String
   * @param arg3 전달받은 Integer
   * @param arg4 전달방은 Boolean
   * @param arg5 전달받은 byte[]
   * @param obj1 전달받은 Object
   * @param obj2 전달받은 Object
   */
  @Override
  public void update(int updateCase, final String arg1, final String arg2,
      final int arg3, boolean arg4, final byte[] arg5, final Object obj1,
      final Object obj2) {

    if (isFinishing()) {
      return;
    }

    final String getNotifyObjectPrefix = PreferenceAgent
        .getStringSharedData(mContext, MapKeys.NOTIFYOBJECT_PREFIX);
    final String notifyObjectPrefix = StringUtil
        .isEmptyString(getNotifyObjectPrefix) ? Const.JAVA_SCRIPT_START :
        Const.JAVA_SCRIPT_START + getNotifyObjectPrefix + ".";

    switch (updateCase) {
      /**
       * JS 에서 setMapSetting() 함수를 호출하여 전달한 MapSetting 데이터를
       * Preference 에 저장합니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_SETMAP_SETTING: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String setMapSettingJson = arg1;
        GrpcClient.getInstance(mContext).setGrpcSettingData(setMapSettingJson);
        LogUtil.e(TAG, "[SETMAP_SETTING]\n" + setMapSettingJson);
      }
      break;
      /**
       * JS 에서 sendEvent() 함수를 호출하여 전달한 eventStream 데이터를
       * MAP 에 전송합니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_SENDEVENT: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String mapEventJson = arg1;
        final String imageUrl = arg2;
        LogUtil.e(TAG, "[UI_FROM_JS_SENDEVENT]\n" + mapEventJson + "\n "
            + "[imageUrl]\n" + imageUrl);
        // mapEventInfo = mapEventJson 을 parsing 한 데이터
        final MapEventInfo mapEventInfo = MapEventDirectiveParser
            .getMapEventInfo(mapEventJson);
        if (mapEventInfo == null) {
          return;
        }
        mCurrentStreamId = mapEventInfo.getStreamId();
        mCurrentOperationSyncId = mapEventInfo.getOperationSyncId();
        mCurrentOperationName = mapEventInfo.getOperationName();
        if (mCurrentOperationName.equalsIgnoreCase(MapOperationName
            .OPERATION_SPEECHTOTEXTTALK)
            || mCurrentOperationName.equalsIgnoreCase(MapOperationName
            .OPERATION_SPEECHTOSPEECHTALK)
            || mCurrentOperationName.equalsIgnoreCase(MapOperationName
            .OPERATION_TEXTTOSPEECHTALK)) {
          mCurrentSpeechStreamId = mapEventInfo.getStreamId();
          mCurrentSpeechOperationSyncId = mapEventInfo.getOperationSyncId();
        } else if (mCurrentOperationName.equalsIgnoreCase(MapOperationName
            .OPERATION_IMAGETOTEXTTALK)
            || mCurrentOperationName.equalsIgnoreCase(MapOperationName
            .OPERATION_IMAGETOSPEECHTALK)) {
          mCurrentImageStreamId = mapEventInfo.getStreamId();
        }
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            boolean isGrpcSendEvent = GrpcClient.getInstance(mContext).grpcSendEvent(mapEventJson);
            if (!isGrpcSendEvent) {
              LogUtil.e(TAG, "grpcSendEvent fail");
              return;
            }

            LogUtil.e(TAG, "[SEND-EVENT]\n" + mapEventJson);
            if (!StringUtil.isEmptyString(imageUrl)) {
              /*
               * Camera, Gallery 에서 캡춰한 이미지의 url 을 받은 경우 이 Url 에 대한 image
               * data 를 MAP 에 전달합니다.
               * loadImageForSend() 함수에서 getByteParam() 함수를 호출하여 image data 를
               * byteStream 으로 전송한 후 전송이 끝나면 streamEnd 함수를 호출합니다.
               */
              GrpcClient.getInstance(mContext)
                  .loadImageForSend(imageUrl, mCurrentImageStreamId);
            } else {
              /*
               * STT, STS 의 경우 openMicrophone > sendEvent 순서로 호출되므로
               * openMicrophone 처리하는 함수에서 Microphone 을 준비하고
               * 이곳에서 Microphone 을 실행시킵니다.
               */
              if (mapEventInfo.getOperationName().equalsIgnoreCase(
                  MapOperationName.OPERATION_SPEECHTOTEXTTALK)
                  || mapEventInfo.getOperationName().equalsIgnoreCase(
                  MapOperationName.OPERATION_SPEECHTOSPEECHTALK)) {
                GrpcClient.getInstance(mContext).processMicrophoneStart
                    (mCurrentSpeechStreamId, mCurrentSpeechOperationSyncId);
                GrpcClient.getInstance(mContext).startTimerForMic();
              }
            }
          }
        });
      }
      break;
      /**
       * JS 에서 setMapSetting() 함수를 호출하여 전달한 MapSetting 데이터에 pingInterval 값이
       * 있으면 이 주기마다 MAP 에 pingRequest 를 호출하고 MAP 으로부터 응답으로 받은 pongResponse 값을
       * JS 에게 전달해 줍니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_MAP_PONG: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String messageJson = arg1;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            webView.loadUrl(
                notifyObjectPrefix + "notifyPong('" + messageJson + "')");
            LogUtil.e(TAG, "[PONG-UI_FROM_MAP_PONG]\n" + messageJson);
          }
        });
      }
      break;
      /**
       * JS 에서 openMicrophone() 함수를 호출하여 전달한 MicrophoneDTO.MicrophoneParam
       * 데이터의 설정값을 참고하여 microphone 을 open 시킵니다.
       * 실제 microphone 을 통한 발화는 JS 에서 sendEvent 함수를 호출하여 전달받은
       * SpeechToTextTalk, SpeechToSpeechTalk eventStream 인 경우에 하게 됩니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_OPENMICROPHONE: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String microphoneParamJson = arg1;
        LogUtil.e(TAG, "[OPENMICROPHONE]\n" + microphoneParamJson);
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            Gson gson = new Gson();
            MicrophoneDTO.MicrophoneParam microPhoneParam = gson.fromJson
                (microphoneParamJson, MicrophoneDTO.MicrophoneParam.class);
            processMicrophone(true, microPhoneParam);
          }
        });
      }
      break;
      /**
       * JS 에서 closeMicrophone() 함수를 호출하면 microphone 을 닫습니다.
       * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 microphone 을 닫을때 closeMicrophone()
       * 함수가 호출될 수 있습니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_CLOSEMICROPHONE: {
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            processMicrophone(false, null);
          }
        });
      }
      break;
      /**
       * 현재 microphone 의 상태를 JS 에게 전달합니다.
       * MicrophoneDTO.MicrophoneStatus 객체로 구성하여 JSON String 으로 전달합니다.
       */
      case HandlerConst.UIUpdater.UI_TO_JS_MICROPHONESTATUS: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String microphoneStatusJson = arg1;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            webView.loadUrl(notifyObjectPrefix + "notifyMicrophoneStatus('"
                + microphoneStatusJson + "')");
            LogUtil.e(TAG, "[MICROPHONE-NOTIFY]\n" + microphoneStatusJson);
          }
        });
      }
      break;
      /**
       * microphone 으로 발화가 시작된 경우(recoding) STARTING_RECORD 이벤트를 JS 에게 전달합니다.
       * MicrophoneDTO.MicrophoneStatus 객체로 구성하여 JSON String 으로 전달합니다.
       */
      case HandlerConst.UIUpdater.UI_TO_JS_MICROPHONE_RECORDINGSTATUS: {
        final boolean recordingStart = arg4;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            if (recordingStart) {
              LogUtil.e(TAG, "[MICROPHONE-RECORDING START]\n");
            } else {
              LogUtil.e(TAG, "[MICROPHONE-RECORDING STOP]\n");
            }
            boolean isResultStatus = Const.RESULT_BOOL_SUCCESS;
            if (!recordingStart) {
              isResultStatus = Const.RESULT_BOOL_FAIL;
            }
            String strReason = Const.EMPTY_STRING;
            if (!recordingStart) {
              strReason = MicrophoneDTO.MicrophoneFailure.MICROPHONE_ERROR_UNKNOWN;
            }
            // 레코딩 시작 상태인 경우
            GrpcClient.getInstance(mContext).processNotifyMicrophoneStatus(
                MicrophoneEvent.STARTING_RECORD, Const.EXPECT_MDOE,
                Const.HAS_NOT_REASON, Const.EMPTY_STRING,
                Const.EMPTY_STRING);
          }
        });
      }
      break;
      /**
       * microphone 을 통해 발화한 데이터를 MAP 에 전송하고, JS 에게 관련 정보를 전달한다.
       * StreamingStatusDTO 객체로 구성하여 JSON String 으로 전달합니다.
       */
      case HandlerConst.UIUpdater.UI_TO_JS_STREAMSENT: {
        if (StringUtil.isEmptyString(arg1) || StringUtil.isEmptyString(arg2)) {
          return;
        }

        final String streamId = arg1;
        final String operationSyncId = arg2;
        final boolean isBreak = (Boolean) obj1;
        final boolean isEnd = (Boolean) obj2;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            // 마이크가 릴리즈된 경우 버퍼에 남아있는 byteStream이 있지만
            // streamEnd를 보낸 경우이므로 전송하지 않는다.
            if (!DeviceAgentManager.getInstance(mContext)
                .isAudioRecorderObject()) {
              LogUtil.e(TAG, "UI_TO_JS_STREAMSENT : "
                  + "Voice Recorder Agent is null...so, not send stream");
              return;
            } else {
              LogUtil.e(TAG, "UI_TO_JS_STREAMSENT : "
                  + "Voice Recorder Agent is not null...so, send stream");
            }
            // sendStreamingEvent() 함수로 StreamingObject 데이터 전송
            StreamingStatusDTO streamStatusDto = new StreamingStatusDTO();
            // 스트림 ID
            streamStatusDto.setStreamId(streamId);
            // 응답 동기화 ID
            streamStatusDto.setOperationSyncId(operationSyncId);
            // 이벤트가 강제로 중지되었는가?
            streamStatusDto.setBreak(isBreak);
            // 이벤트의 끝인가?
            streamStatusDto.setEnd(isEnd);
            // 스트리밍 데이터의 구체적인 형식
            streamStatusDto.setType(Streamingtype.BYTES);
            // 스트리밍의 크기
            final byte[] utterVoice = (arg5 != null) ? arg5.clone() : null;
            streamStatusDto.setSize(utterVoice != null ? utterVoice.length : 0);
            String streamObjectJson = GsonUtil
                .jsonStringFromObject(streamStatusDto);
            LogUtil.e(TAG, "[MICROPHONE_STREAMSENT]\n" + streamObjectJson);
            // StreamingStatus
            webView.loadUrl(notifyObjectPrefix + "sendStreamingEvent('" +
                streamObjectJson + "')");
            // Microphone으로 발화된 음성을 MAP 에 전달
            if (utterVoice != null) {
              GrpcClient.getInstance(mContext)
                  .processMicrophoneUtterData(utterVoice);
            }
          }
        });
      }
      break;
      /**
       * MAP 으로 부터 byteStream, streamEnd 을 수신한 경우
       * JS 에게 byteStream 에 대한 정보를 전달해 줍니다.
       * StreamingStatusDTO 객체로 구성하여 JSON String 으로 전달합니다.
       */
      case HandlerConst.UIUpdater.UI_TO_JS_RECEIVESTREAM: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String receiveStreamObjectJson = arg1;
        LogUtil.e(TAG, "[RECEIVESTREAM_DIRECTIVE]\n" + receiveStreamObjectJson);
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            webView.loadUrl(notifyObjectPrefix + "receiveStreamingDirective('"
                + receiveStreamObjectJson + "')");
          }
        });
      }
      break;
      /**
       * JS 로 부터 openCamera() 함수가 호출된 경우 전달받은 CameraParam 데이터를 사용하여
       * camera 프리뷰 화면을 실행합니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_OPENCAMERA: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String messageJson = arg1;
        LogUtil.e(TAG, "[OPENCAMERA]\n" + messageJson);
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            Gson gson = new Gson();
            final CameraDTO.CameraParam cameraParam = gson.fromJson(messageJson,
                CameraDTO.CameraParam.class);
            processCamera(true, cameraParam);
          }
        });
      }
      break;
      /**
       * JS 로 부터 closeCamera() 함수가 호출된 경우 camera 프리뷰화면 또는 image crop 화면을
       * 종료시킵니다.
       * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 camera를 닫을때
       * closeCamera() 함수가 호출될 수 있습니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_CLOSECAMERA: {
        LogUtil.e(TAG, "[CLOSECAMERA]\n");
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            processCamera(false, null);
          }
        });
      }
      break;
      /**
       * 현재 camera 의 상태를 JS 에게 전달합니다.
       * CameraDTO.CameraStatus 객체로 구성하여 JSON String 으로 전달합니다.
       */
      case HandlerConst.UIUpdater.UI_TO_JS_CAMERASTATUS: {
        if (StringUtil.isEmptyString(arg1) || obj2 == null) {
          return;
        }
        final String cameraStatusJson = arg1;
        final CameraDTO.CameraStatus cameraStatus =
            (CameraDTO.CameraStatus) obj2;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            webView.loadUrl(notifyObjectPrefix + "notifyCameraStatus('" +
                cameraStatusJson + "')");
            LogUtil.e(TAG, "[CAMERA-NOTIFY]\n" + cameraStatusJson);
          }
        });
      }
      break;
      /**
       * JS 로 부터 openGallery() 함수가 호출된 경우 전달받은 GalleryParam 데이터를 사용하여
       * Gallery 화면을 실행합니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_OPENGALLERY: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String galleryParamJson = arg1;
        LogUtil.e(TAG, "[OPENGALLERY]\n" + galleryParamJson);
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            Gson gson = new Gson();
            GalleryDTO.GalleryParam galleryParam = gson.fromJson
                (galleryParamJson, GalleryDTO.GalleryParam.class);
            processGallery(true, galleryParam);
          }
        });
      }
      break;
      /**
       * JS 로 부터 closeGallery() 함수가 호출된 경우 Gallery 화면을 종료시킵니다.
       * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 Gallery 화면을 닫을때
       * closeGallery() 함수가 호출될 수 있습니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_CLOSEGALLERY: {
        LogUtil.e(TAG, "[CLOSEGALLERY]\n");
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            processGallery(false, null);
          }
        });
      }
      break;
      /**
       * 현재 Gallery 화면의 상태를 JS 에게 전달합니다.
       * GalleryDTO.GalleryStatus 객체로 구성하여 JSON String 으로 전달합니다.
       */
      case HandlerConst.UIUpdater.UI_TO_JS_GALLERYSTATUS: {
        if (StringUtil.isEmptyString(arg1) || obj2 == null) {
          return;
        }
        final String galleryStatusJson = arg1;
        final GalleryDTO.GalleryStatus galleryStatus = (GalleryDTO
            .GalleryStatus) obj2;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            webView.loadUrl(notifyObjectPrefix + "notifyGalleryStatus('" +
                galleryStatusJson + "')");
            LogUtil.e(TAG, "[GALLERY-NOTIFY]\n" + galleryStatusJson);
          }
        });
      }
      break;
      /**
       * JS 에서 closeSpeaker() 함수를 호출하면 speaker 을 닫습니다.
       * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 speaker 을 닫을때 closeSpeaker() 함수가 호출될
       * 수 있습니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_JS_CLOSESPEAKER: {
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            GrpcClient.getInstance(mContext).speakerClose();
          }
        });
      }
      break;
      /**
       * 현재 speaker 의 상태를 JS 에게 전달합니다.
       * SpeakerDTO.SpeakerStatus 객체로 구성하여 JSON String 으로 전달합니다.
       */
      case HandlerConst.UIUpdater.UI_TO_JS_SPEAKERSTATUS: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String speakerStatusJson = arg1;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            webView.loadUrl(notifyObjectPrefix + "notifySpeakerStatus('" +
                speakerStatusJson + "')");
            LogUtil.e(TAG, "[SPEAKER-NOTIFY]\n" + speakerStatusJson);
          }
        });
      }
      break;
      /**
       * 현재 GPS 정보를 JS 에게 전달합니다.
       * LocationDTO.LocationStatus 객체로 구성하여 JSON String 으로 전달합니다.
       */
      case UIUpdater.UI_TO_JS_LOCATIONSTATUS: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String locationStatus = arg1;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            webView.loadUrl(notifyObjectPrefix + "notifyLocation('" +
                locationStatus + "')");
            LogUtil.e(TAG, "[LOCATION-NOTIFY]\n" + locationStatus);
          }
        });
      }
      break;
      /**
       * JS 에서 getLocation() 함수를 호출하면 현재 GPS 정보를 취득합니다.
       * JS 에서 GPS 정보가 필요할 때 비주기적으로 호출될 수 있습니다.
       */
      case UIUpdater.UI_FROM_JS_GETLOCATION: {
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            processLocation();
          }
        });
      }
      break;
      /**
       * MAP 에게 eventStream 을 전송하고 MAP 으로 부터 수신이 완료된 경우
       * JS 에게 전송된 operationSyncId 값을 전달한다.
       */
      case HandlerConst.UIUpdater.UI_FROM_MAP_ONCOMPLETED: {
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            LogUtil.e(TAG, "[MAP_ONCOMPLETED]\n\n" + mCurrentOperationSyncId);
            webView.loadUrl(notifyObjectPrefix + "receiveCompleted('" +
                mCurrentOperationSyncId + "')");
          }
        });
      }
      break;
      /**
       * MAP 과 통신중에 gRPC 에러를 수신한 경우에 에러에 대한 정보를
       * GrpcStatusDTO 객체로 구성하여 JSON String 으로 전달합니다.
       * 현재는 Throwable 메시지를 전달하고 있고,
       * 상세 에러 내역을 추출하는 방법을 제공받은 후 그에 대한 수정이 필요합니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_MAP_ONERROR:
      case HandlerConst.UIUpdater.UI_FROM_MAP_ERROR: {
        if (StringUtil.isEmptyString(arg1)) {
          return;
        }
        final String errorMessageJson = arg1;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            webView.loadUrl(notifyObjectPrefix + "receiveError('" +
                errorMessageJson + "')");
            ToastView.getInstance(MainActivity.this)
                .setToastMsg("[ERROR]\n" + errorMessageJson);
          }
        });
      }
      break;
      /**
       * JS 에서 호출된 sendEvent() 함수를 통해 전달받은 eventStream 데이터를 MAP 에게 요청하고
       * 이 요청에 대한 응답(MapDirective)을 받은 경우 directive 종류에 따라 처리를 하고
       * directiveStream 인 경우 JS 에게 전달해 줍니다.
       */
      case HandlerConst.UIUpdater.UI_FROM_MAP_ONNEXT: {
        if (obj1 == null) {
          return;
        }
        final MapDirective mapDirective = (MapDirective) obj1;
        final MapDirectiveInfo directiveInfo = MapEventDirectiveParser
            .getMapDirectiveInfo(mapDirective);
        if (directiveInfo == null) {
          return;
        }
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            /**
             * Directive 로 MapException 이 들어오면 receiveException() 함수를 호출하여
             * MapException 정보를 전달해 줍니다.
             */
            if (directiveInfo.isMapException()) {
              webView.loadUrl(notifyObjectPrefix + "receiveException('" +
                  directiveInfo.getMapExceptionJson() + "')");
              LogUtil.e(TAG, "[MAPEXCEPTION_ERROR]\n" + directiveInfo
                  .getMapExceptionJson());
              return;
            }
            /**
             * MAP 으로 부터 byteStream 를 수신하면 이 데이터의 정보를 JS 에게 전달하고
             * AudioTrack으로 수신한 byteStream 을 재생합니다.
             */
            else if (directiveInfo.isByteStream() || directiveInfo
                .isByteTextStream()) {
              byte[] byteData = null;
              if (directiveInfo.isByteStream()) {
                byteData = directiveInfo.getByteStream().toByteArray();
              } else {
                byteData = directiveInfo.getByteTextStream().toByteArray();
              }
              GrpcClient.getInstance(mContext)
                  .processReceiveStreamDirective(byteData,
                      mCurrentStreamId, mCurrentOperationSyncId, false, false);
              // Audio play (PCM)
              GrpcClient.getInstance(mContext).speakerPlay(byteData);
              return;
            }
            /**
             * MAP 으로 부터 streamEnd 를 수신하면 streamEnd 에 대한 정보를
             * StreamingStatusDTO 객체로 구성한 JSON String 을 JS 에게 전달합니다.
             */
            else if (directiveInfo.isStreamEnd()) {
              // Audio stop (PCM)
              GrpcClient.getInstance(mContext)
                  .processReceiveStreamDirective(null,
                      mCurrentStreamId, mCurrentOperationSyncId, false, true);
              return;
            }
            /**
             * MAP 으로 부터 받은 MapDirective 가 directiveStream 인 경우 JS 에게 전달한다.
             */
            else if (directiveInfo.isDirectiveStream()) {
              DirectiveStreamInfo dsInfo = directiveInfo
                  .getDirectiveStreamInfo();
              // dsJson : directiveStream of JSON type string
              String dsJson = directiveInfo.getDirectiveStreamJson();
              if (dsJson.isEmpty() || dsInfo == null) {
                LogUtil.e(TAG, "[ONNEXT] directiveStreamInfo is null.");
                return;
              } else {
                LogUtil.e(TAG, "[RECEIVE_DIRECTIVE]\n" + dsJson);
              }
              String interfaceName = dsInfo.getInterfaceName();
              String operationName = dsInfo.getOperationName();
              String streamId = dsInfo.getStreamId();
              String operationSyncId = dsInfo.getOperationSyncId();
              if (interfaceName.equalsIgnoreCase((MapInterfaceName
                  .INTERFACE_SPEECHRECOGNIZER))) {
                if (operationName.equalsIgnoreCase(MapOperationName
                    .OPERATION_RECOGNIZED)
                    || operationName.equalsIgnoreCase(MapOperationName
                    .OPERATION_ENDPOINTDETECTED)) {
                  /*
                   * MAP 으로 부터 Recognized 수신 시 또는 MAP 으로 부터 EndpointDetected
                   * 수신 시 VoiceRecorderAgent 를 stop 시키고 streamEnd 호출합니다.
                   */
                  GrpcClient.getInstance(mContext)
                      .microphoneRelese(true, MicrophoneEvent.CLOSE_SUCCESS,
                          null);
                  LogUtil.e(TAG, "[RECEIVE_DIRECTIVE : STREAMEND]\n" +
                      "streamId = " + mCurrentSpeechStreamId);
                  // Recognized, EndpointDetected 수신 시 streamEnd 호출한다.
                  GrpcClient.getInstance(mContext).callRequestStreamEnd
                      (mCurrentSpeechStreamId);
                } else if (operationName.equalsIgnoreCase(MapOperationName
                    .OPERATION_RECOGNIZING)) {
                  /**
                   * MicrophoneParam : timeoutInMillisecond 시간만큼 Timer를 연장합니다.
                   * (startTimerForMic)
                   */
                  GrpcClient.getInstance(mContext).startTimerForMic();
                }
              } else if (operationName.equalsIgnoreCase(MapOperationName
                  .OPERATION_PLAY)) {
                if (interfaceName.equalsIgnoreCase(MapInterfaceName
                    .INTERFACE_SPEECHSYNTHESIZER)) {
                  /*
                   * SpeechSynthesizer, AudioPlay / Play 인 경우 param /
                   * speechSynthesizerParam 에 있는 encoding, sampleRate 를
                   * preference에 저장합니다.
                   */
                  try {
                    JSONObject directiveJsonObject = new JSONObject(dsJson);
                    String paramValue = directiveJsonObject
                        .getString("param");
                    if (!StringUtil.isEmptyString(paramValue)) {
                      JSONObject paramValueObject = new JSONObject(paramValue);
                      String speechSynthesizerParamValue = paramValueObject
                          .getString("speechSynthesizerParam");
                      JSONObject speechSynthesizerParamObject = new JSONObject
                          (speechSynthesizerParamValue);
                      String encoding = speechSynthesizerParamObject
                          .getString("encoding");
                      int sampleRate = speechSynthesizerParamObject.getInt
                          ("sampleRate");
                      PreferenceAgent.setStringSharedData(mContext, MapKeys
                          .SPEECH_ENCODING, encoding);
                      PreferenceAgent.setIntSharedData(mContext, MapKeys
                          .SPEECH_SAMPLERATE, sampleRate);
                    }
                  } catch (JSONException e) {
                    LogUtil.d(TAG, "[UI_FROM_JS_SENDEVENT]\n" + e.getMessage());
                    return;
                  }
                }
                /*
                 * MAP 으로 부터 Play 수신 시 player 를 준비 시키고 이후에 directiveStream 으로
                 * byteStream 이 수신되면 이 데이터를 AudioTrack 으로 재생합니다.
                 */
                GrpcClient.getInstance(mContext).speakerPlayInit();
              }
              /**
               * ExpectSpeech를 받은 경우 directStream을 JS 에게 전달해 주고 JS 는
               * openMicrophone(param) 을 호출해서 음성인식 처리를 유도합니다.
               * 여기에서는 따로 처리하지 않습니다.
               */
              else if (operationName.equalsIgnoreCase(MapOperationName
                  .OPERATION_EXPECTSPEECH)) {
                mCurrentSpeechStreamId = streamId;
                mCurrentSpeechOperationSyncId = operationSyncId;
                // 여기에서는 따로 처리하지 않습니다.
              }
              /**
               * MAP 으로부터 수신한 directiveStream 이 ExpectSpeechTimedOut 이면 음성인식
               * 실패한 경우이며, 이때 마이크가 열려있으면 닫습니다.
               */
              else if (operationName.equalsIgnoreCase(MapOperationName
                  .OPERATION_EXPECTSPEECHTIMEDOUT)) {
                processMicrophone(false, null);
              }

              // directiveStream 인 경우 JS 에게 전달한다.
              String url = notifyObjectPrefix + "receiveDirective('" + dsJson
                  + "')";
              webView.loadUrl(url);
            }
          }
        });
      }
      break;
      // arg3 : broadcast receiver 에서 수신한 call status 에 대한 처리를 합니다.
      case HandlerConst.UIUpdater.UI_FROM_ETC_CALLSTATUS: {
        final int callState = arg3;
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            if (callState == TelephonyManager.CALL_STATE_RINGING) {
              GrpcClient.getInstance(mContext)
                  .processMicrophoneStop(MicrophoneEvent.CLOSE_SUCCESS,
                      MicrophoneFailure.MICROPHONE_UNDER_USE);
            }
          }
        });
      }
      break;
    }
  }


  /**
   * Camera preview, crop 화면 또는 Gallery 화면을 실행시킨 후 결과값을 받아서 처리하는 함수입니다.
   * <p>
   * Camera crop, Gallery 화면에 대한 결과값이 RESULT_OK 인 경우 동작되고 있는 Timer 를 종료시키고,
   * 캡춰된 이미지에 대한 thumbnail data, image path 정보를 notifyCameraStatus,
   * notifyGalleryStatus 함수를 호출하여 JS 로 전달합니다.<br>
   * RESULT_OK 가 아닌 경우에도 notifyCameraStatus, notifyGalleryStatus 함수를 호출하여 실패에
   * 대한 상태값을 JS 로 전달합니다.
   * </p>
   *
   * @param requestCode 화면 실행 시 사용되었던 요청 코드
   * @param resultCode 성공이면 RESULT_OK, 실패이면 RESULT_CANCELED
   * @param data Camera crop, Gallery 화면으로 부터 전달받은 데이터
   */
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent
      data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == REQUEST_ATTACH_IMAGE) {
      mMultimediaType = Const.MultiMediaType.NORMAL;
      // 1. Gallery 닫힘 상태를 알려준다.
      GrpcClient.getInstance(mContext)
          .processNotifyGalleryStatus(GalleryEvent.CLOSED,
              Const.HAS_NOT_REASON, Const.EMPTY_STRING, Const.EMPTY_STRING,
              null, 0);
      if (resultCode == RESULT_OK) {
        mImageCaptureUri = data.getData();
        String imagePath = BitmapUtil.ProcessImage.getRealPathFromURI
            (mContext, mImageCaptureUri); //
        // path 경로
        String imageData = BitmapUtil.ProcessImage.convertImageToString
            (mContext, imagePath);
        // 2. Native -> JS : Gallery 에서 선택한 이미지를 전달한다.
        EtcDTO.ImageData imageDataDto = new EtcDTO.ImageData();
        imageDataDto.setImageUrl(imagePath);
        imageDataDto.setThumbnail(imageData);
        GrpcClient.getInstance(mContext).processNotifyGalleryStatus(GalleryEvent
            .IMAGE_CAPTURED, Const.HAS_NOT_REASON, Const.EMPTY_STRING, Const
            .EMPTY_STRING, imageDataDto, 0);
      } else {
        GrpcClient.getInstance(mContext).processNotifyGalleryStatus(GalleryEvent
            .IMAGE_CAPTURED, Const.HAS_REASON, GalleryFailure
            .GALLERY_SELECT_CANCELLED, Const.EMPTY_STRING, null, 0);
      }
    } else if (requestCode == REQUEST_ATTACH_CROP) {
      mMultimediaType = Const.MultiMediaType.NORMAL;
      // Crop 갔다가 온 후에 CLOSED
      GrpcClient.getInstance(mContext).processNotifyCameraStatus(CameraEvent
              .CLOSED,
          Const.HAS_NOT_REASON, Const.EMPTY_STRING, Const.EMPTY_STRING,
          null, 0);
      if (data != null) {
        CameraDTO.CameraParam cameraParam = data.getParcelableExtra(Const
            .CAMERA_EXTRA_INFO);

        mCurrentPhotoPath = data.getStringExtra(Const.CAMERA_EXTRA_FILE);

        String failure = data.getStringExtra(Const.CAMERA_EXTRA_FAILURE);
        String message = data.getStringExtra(Const.CAMERA_EXTRA_MESSAGE);
        if (resultCode == RESULT_OK) {
          //  CameraEvent.IMAGE_CAPTURED
          String imageData = BitmapUtil.ProcessImage.convertImageToString
              (mContext, mCurrentPhotoPath);

          // 2. Native -> JS : Camera 에서 Capture한 이미지를 전달한다.
          EtcDTO.ImageData imageDataDto = new EtcDTO.ImageData();
          imageDataDto.setImageUrl(mCurrentPhotoPath);
          imageDataDto.setThumbnail(imageData);
          GrpcClient.getInstance(mContext).processNotifyCameraStatus(CameraEvent
                  .IMAGE_CAPTURED, Const.HAS_NOT_REASON, Const.EMPTY_STRING,
              Const.EMPTY_STRING, imageDataDto, 0);
        } else {
          //  CameraEvent.IMAGE_CAPTURED
          GrpcClient.getInstance(mContext)
              .processNotifyCameraStatus(CameraEvent.IMAGE_CAPTURED,
                  Const.HAS_REASON, CameraFailure.CAMERA_CAPTURE_CANCELLED,
                  Const.EMPTY_STRING, null, 0);
          if (resultCode == Const.RESULT_RETRY) {
            // 카메라를 다시연다
            LogUtil.d("mmcamera", "Retry");
            processCamera(true, cameraParam);
          }
        }
      } else {
        //  CameraEvent.IMAGE_CAPTURED
        GrpcClient.getInstance(mContext)
            .processNotifyCameraStatus(CameraEvent.IMAGE_CAPTURED,
                Const.HAS_REASON, CameraFailure.CAMERA_CAPTURE_CANCELLED,
                Const.EMPTY_STRING, null, 0);
        LogUtil.d("mmcamera", "Back");
      }
    } else if (requestCode == REQUEST_ATTACH_CAPTURE) {
      // 1. Camera 닫힘 상태를 알려준다.
      if (data != null) {
        CameraDTO.CameraParam cameraParam = data.getParcelableExtra(Const
            .CAMERA_EXTRA_INFO);

        mCurrentPhotoPath = data.getStringExtra(Const.CAMERA_EXTRA_FILE);
        String failure = data.getStringExtra(Const.CAMERA_EXTRA_FAILURE);
        String message = data.getStringExtra(Const.CAMERA_EXTRA_MESSAGE);
        int resultStatus = data.getIntExtra(Const.CAMERA_EXTRA_RESULT,
            Const.RESULT_SUCCESS);
        LogUtil.e(TAG, "> PhothPath = " + mCurrentPhotoPath);
        if (resultCode == RESULT_OK) {
          if (resultStatus == Const.RESULT_SUCCESS) {
            // Crop 화면 호출. CAMERA 구동의 연장 처리로 간주한다.
            mMultimediaType = Const.MultiMediaType.CAMERA;
            GrpcClient.getInstance(mContext).callCropOpen
                (data, REQUEST_ATTACH_CROP);
          } else {
            // 카메라 실패 하여 CLOSED
            GrpcClient.getInstance(mContext)
                .processNotifyCameraStatus(CameraEvent.CLOSED,
                    Const.HAS_REASON, failure, message, null, 0);
          }
        } else {
          // 카메라 실패 하여 CLOSED
          GrpcClient.getInstance(mContext)
              .processNotifyCameraStatus(CameraEvent.CLOSED,
                  Const.HAS_REASON, failure, message, null, 0);
        }
      } else {
        // 카메라 실패 하여 CLOSED
        GrpcClient.getInstance(mContext)
            .processNotifyCameraStatus(CameraEvent.CLOSED,
                Const.HAS_REASON, CameraFailure.CAMERA_CAPTURE_CANCELLED,
                Const.EMPTY_STRING, null, 0);
      }
    }
  }

  /**
   * JS 로 부터 openCamera 함수가 호출된 경우 Camera 프리뷰 화면을 실행하고,
   * JS 로 부터 closeCamera 함수가 호출된 경우 Camera 프리뷰 또는 Crop 화면을 종료시키는 함수입니다.
   * <p>
   * open 액션인 경우 CameraParam 의 openInterval*1000 에 대한 타이머를 구동하고
   * 타이머가 Expired 되면 실행된 화면들을 종료시킵니다.<br>
   * open, close 에 대한 상태를 notifyCameraStatus 함수를 통해서 JS 에게 전달해 줍니다.
   * </p>
   *
   * @param isOpen true 인 경우 open, false 인 경우 close
   * @param cameraParam true 인 경우 JS 로 부터 전달 받은 CameraParam 값
   */
  private void processCamera(boolean isOpen, final CameraDTO.CameraParam
      cameraParam) {
    mMultimediaType = Const.MultiMediaType.NONE;
    // Camera 열기
    if (isOpen) {
      if (cameraParam != null) {
        String errorMessage = "";
        try {
          /**
           * 1. Camera preview
           * 2. 10초 Timer
           */
          String state = Environment.getExternalStorageState();
          if (Environment.MEDIA_MOUNTED.equals(state)) {
            /**
             * cameraParam 을 카메라 프리뷰에 넘겨준다.
             */
            GrpcClient.getInstance(mContext).callPreviewOpen
                (REQUEST_ATTACH_CAPTURE, cameraParam, R
                    .drawable.camera_corners_normal, false);
            GrpcClient.getInstance(mContext)
                .processNotifyCameraStatus(CameraEvent.OPEN_SUCCESS,
                    Const.HAS_NOT_REASON, Const.EMPTY_STRING,
                    Const.EMPTY_STRING, null, cameraParam.getOpenInterval());
            mMultimediaType = Const.MultiMediaType.CAMERA;
          }
        } catch (Exception e) {
          LogUtil.e(TAG, e.getMessage());
          errorMessage = e.getMessage();
          GrpcClient.getInstance(mContext)
              .processNotifyCameraStatus(CameraEvent.OPEN_FAILURE,
                  Const.HAS_REASON, CameraFailure.CAMERA_ERROR_UNKNOWN,
                  errorMessage, null, 0);
        }
      }
      // open 일때 cameraParam 가 null 인 경우
      else {
        // CameraStatus : Open 실패
        GrpcClient.getInstance(mContext)
            .processNotifyCameraStatus(CameraEvent.OPEN_FAILURE,
                Const.HAS_REASON, CameraFailure.CAMERA_ERROR_UNKNOWN, Const
                    .EMPTY_STRING, null, 0);
      }
    }
    // close 일때 Camera 닫기
    else {
      // CameraStatus : Close 성공
      GrpcClient.getInstance(mContext)
          .callOpenedActivityClose(REQUEST_ATTACH_CAPTURE);
      GrpcClient.getInstance(mContext)
          .callOpenedActivityClose(REQUEST_ATTACH_CROP);
      GrpcClient.getInstance(mContext)
          .processNotifyCameraStatus(CameraEvent.CLOSED,
              Const.HAS_NOT_REASON, Const.EMPTY_STRING, Const.EMPTY_STRING,
              null, 0);
    }
  }

  /**
   * JS 로 부터 openGallery 함수가 호출된 경우 단말의 겔러리 앱을 실행하고,
   * JS 로 부터 closeGallery 함수가 호출된 경우 겔러리 앱을 종료시키는 함수입니다.
   * <p>
   * open 액션인 경우 GalleryParam 의 openInterval*1000 에 대한 타이머를 구동하고
   * 타이머가 Expired 되면 실행된 겔러리 앱을 종료시킵니다.<br>
   * open, close 에 대한 상태를 notifyGalleryStatus 함수를 통해서 JS 에게 전달해 줍니다.
   * </p>
   *
   * @param isOpen true 인 경우 open, false 인 경우 close
   * @param galleryParam true 인 경우 JS 로 부터 전달 받은 GalleryParam 값
   */
  private void processGallery(boolean isOpen, final GalleryDTO.GalleryParam
      galleryParam) {
    mMultimediaType = Const.MultiMediaType.NONE;
    // Gallery 열기
    if (isOpen) {
      Gson gson = new Gson();
      if (galleryParam != null) {
        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
            new PermissionEventListener() {
              @Override
              public void onGrantedPermission() {
                String errorMessage = "";
                /**
                 * 1. Gallery 호출
                 * 2. openInterval*1000 시간만큼 Timer 구동
                 * 3. Open 성공인 경우 JS 에게 OPEN_SUCCESS 를 알려준다.
                 * 4. Open 실패인 경우 JS 에게 OPEN_FAILURE 를 알려준다.
                 */
                boolean isResult = GrpcClient.getInstance(mContext)
                    .callGalleryOpen(REQUEST_ATTACH_IMAGE);
                mMultimediaType = isResult ? Const.MultiMediaType
                    .GALLERY : MultiMediaType.NONE;
                if (isResult) {
                  GrpcClient.getInstance(mContext)
                      .processNotifyGalleryStatus(GalleryEvent.OPEN_SUCCESS,
                          Const.HAS_NOT_REASON, Const.EMPTY_STRING,
                          Const.EMPTY_STRING, null,
                          galleryParam.getOpenInterval());
                } else {
                  // GalleryStatus : Open 실패
                  GrpcClient.getInstance(mContext)
                      .processNotifyGalleryStatus(GalleryEvent
                              .OPEN_FAILURE, Const.HAS_REASON, GalleryFailure
                              .GALLERY_PERMISSION_DENIED, Const.EMPTY_STRING,
                          null, 0);
                }
              }

              @Override
              public void onDeniedPermission() {
                // GalleryStatus : Open 실패
                GrpcClient.getInstance(mContext)
                    .processNotifyGalleryStatus(GalleryEvent
                            .OPEN_FAILURE, Const.HAS_REASON, GalleryFailure
                            .GALLERY_PERMISSION_DENIED, Const.EMPTY_STRING,
                        null, 0);
              }
            });
      }
      // open 일때 galleryParam 이 null 인 경우
      else {
        // GalleryStatus : Open 실패
        GrpcClient.getInstance(mContext)
            .processNotifyGalleryStatus(GalleryEvent.OPEN_FAILURE,
                Const.HAS_REASON, GalleryFailure.GALLERY_OPEN_ERROR, Const
                    .EMPTY_STRING, null, 0);
      }
    }
    // close 일때 Gallery 닫기
    else {
      GrpcClient.getInstance(mContext)
          .callOpenedActivityClose(REQUEST_ATTACH_IMAGE);
      GrpcClient.getInstance(mContext)
          .processNotifyGalleryStatus(GalleryEvent.CLOSED,
              Const.HAS_NOT_REASON, Const.EMPTY_STRING, Const.EMPTY_STRING,
              null, 0);
    }
  }

  /**
   * JS 로 부터 openMicrophone 함수가 호출된 경우 Microphone 을 open 하고, JS 로 부터
   * closeMicrophone 함수가 호출되었거나 MAP 으로 부터 "ExpectSpeechTimedOut"을 수신한 경우
   * Microphone 을 close 하는 함수입니다.
   * <p>
   * open 액션인 경우 MicrophoneParam 값으로 Microphone 을 초기화 한 후
   * notifyMicrophoneStatus 함수를 통해서 JS 에게 현재 상태를 전달해 줍니다.<br>
   * open 실패나 close 에 대한 상태도 JS 에게 전달해 줍니다.
   * </p>
   *
   * @param isOpen true 인 경우 open, false 인 경우 close
   * @param microPhoneParam true 인 경우 JS 로 부터 전달 받은 MicrophoneParam 값
   */
  private void processMicrophone(boolean isOpen,
      final MicrophoneDTO.MicrophoneParam microPhoneParam) {
    // Microphone 열기
    if (isOpen) {
      if (microPhoneParam != null) {
        String errorMessage = "";
        checkPermission(Manifest.permission.RECORD_AUDIO,
            new PermissionEventListener() {
              @Override
              public void onGrantedPermission() {
                boolean isResult = GrpcClient.getInstance(mContext)
                    .processMicrophoneInit(microPhoneParam);
                if (isResult) {
                  // 마이크 Open 성공
                  GrpcClient.getInstance(mContext).processNotifyMicrophoneStatus
                      (MicrophoneEvent.OPEN_SUCCESS, Const.EXPECT_MDOE,
                          Const.HAS_NOT_REASON, Const.EMPTY_STRING,
                          Const.EMPTY_STRING);
                } else {
                  // 마이크 Open 실패
                  GrpcClient.getInstance(mContext).processNotifyMicrophoneStatus
                      (MicrophoneEvent.OPEN_FAILURE, Const.EXPECT_MDOE,
                          Const.HAS_REASON, MicrophoneFailure
                              .MICROPHONE_ERROR_UNKNOWN, Const.EMPTY_STRING);
                }
              }

              @Override
              public void onDeniedPermission() {
                // 마이크 Open 실패
                GrpcClient.getInstance(mContext).processNotifyMicrophoneStatus
                    (MicrophoneEvent.OPEN_FAILURE, Const.EXPECT_MDOE,
                        Const.HAS_REASON, MicrophoneFailure
                            .MICROPHONE_PERMISSION_DENIED, Const.EMPTY_STRING);
              }
            });
      } else {
        GrpcClient.getInstance(mContext)
            .microphoneRelese(false, MicrophoneEvent.OPEN_FAILURE,
                MicrophoneFailure.MICROPHONE_OPEN_ERROR);
      }
    }
    // Microphone 명시적으로 닫기
    else {
      GrpcClient.getInstance(mContext)
          .microphoneRelese(true, MicrophoneEvent.CLOSE_SUCCESS, null);
    }
  }

  /**
   * JS 로 부터 getLocation 함수를 호출받고 GPS 정보를 수신한 후 notifyLocation 함수 호출로 JS 에게
   * GPS 정보를 전달해 주는 함수입니다.
   */
  private void processLocation() {
    checkPermissions(PERMISSIONS_GPS, new PermissionEventListener() {
      @Override
      public void onGrantedPermission() {
        GrpcClient.getInstance(mContext).processLocation();
      }

      @Override
      public void onDeniedPermission() {
        GrpcClient.getInstance(mContext).processNotifyLocation(true,
            LocationFailure.LOCATION_PERMISSION_DENIED, Const.EMPTY_STRING,
            null);
      }
    });
  }
}
