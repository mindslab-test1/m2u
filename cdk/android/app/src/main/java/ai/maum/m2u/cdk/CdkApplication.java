package ai.maum.m2u.cdk;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

/**
 * cdk-android 프로젝트의 Application 클래스 입니다.
 */

public class CdkApplication extends MultiDexApplication {

  private static Context mContext;

  public static Context getAppContext() {
    return mContext;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mContext = getApplicationContext();
  }
}
