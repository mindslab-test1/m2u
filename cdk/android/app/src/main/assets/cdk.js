var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
define("uuid", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * Fast UUID generator, RFC4122 version 4 compliant.
     * @author Jeff Ward (jcward.com).
     * @license MIT license
     * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
     **/
    var UUID = /** @class */ (function () {
        function UUID() {
        }
        /**
         * UUID initialize
         */
        UUID.initialize = function () {
            // Initialization
            for (var i = 0; i < 256; i++) {
                UUID.sLut[i] = (i < 16 ? '0' : '') + (i).toString(16);
            }
            // BASE64 URL 스펙, RFC7515 를 기준으로 사용한다.
            // 여기서 만들어진 결과를 web url에서 쓰이고 파일이름을 생성할 때에도 쓰이기 때문이다.
            var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
            for (var i = 0, len = code.length; i < len; ++i) {
                UUID.lookup[i] = code[i];
            }
        };
        /**
         * Base64 encode
         *
         * @param {number} number를 Base64 인코딩
         * @returns {string}
         */
        UUID.tripletToBase64 = function (num) {
            return UUID.lookup[num >> 18 & 0x3F]
                + UUID.lookup[num >> 12 & 0x3F]
                + UUID.lookup[num >> 6 & 0x3F]
                + UUID.lookup[num & 0x3F];
        };
        /**
         * Combine the three bytes into a single integer
         *
         * @param {Uint8Array} uint8 입력 Byte Array
         * @param {number} start 시작 위치
         * @param {number} end 종료 위치
         * @returns {string}
         */
        UUID.encodeChunk = function (uint8, start, end) {
            var tmp;
            var output = [];
            for (var i = start; i < end; i += 3) {
                tmp = ((uint8[i] << 16) & 0xFF0000) + ((uint8[i + 1] << 8) & 0xFF00) + (uint8[i + 2] & 0xFF);
                output.push(this.tripletToBase64(tmp));
            }
            return output.join('');
        };
        /**
         * Takes a byte array and returns string
         *
         * @param {Uint8Array} uint8 String으로 변환할 Byte Array
         * @returns {string}
         */
        UUID.fromByteArray = function (uint8) {
            var tmp;
            var len = uint8.length;
            var extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes
            var output = '';
            var parts = [];
            var maxChunkLength = 16383; // must be multiple of 3
            //go through the array every three bytes, we'll deal with trailing stuff later
            for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
                parts.push(this.encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)));
            }
            //pad the end with zeros, but make sure to not forget the extra bytes
            if (extraBytes === 1) {
                tmp = uint8[len - 1];
                output += UUID.lookup[tmp >> 2];
                output += UUID.lookup[(tmp << 4) & 0x3F];
                output += '==';
            }
            else if (extraBytes === 2) {
                tmp = (uint8[len - 2] << 8) + (uint8[len - 1]);
                output += UUID.lookup[tmp >> 10];
                output += UUID.lookup[(tmp >> 4) & 0x3F];
                output += UUID.lookup[(tmp << 2) & 0x3F];
                output += '=';
            }
            parts.push(output);
            return parts.join('');
        };
        /**
         * Generate a Base64 to string
         *
         * @returns {string} Base64 인코딩된 값을 String으로 전달
         */
        UUID.generateAsBase64 = function () {
            var d0 = Math.random() * 0xffffffff | 0;
            var d1 = Math.random() * 0xffffffff | 0;
            var d2 = Math.random() * 0xffffffff | 0;
            var d3 = Math.random() * 0xffffffff | 0;
            var raw = new Uint8Array(16);
            raw[0] = d0 & 0xff;
            raw[1] = d0 >> 8 & 0xff;
            raw[2] = d0 >> 16 & 0xff;
            raw[3] = d0 >> 24 & 0xff;
            raw[4] = d1 & 0xff;
            raw[5] = d1 >> 8 & 0xff;
            raw[6] = d1 >> 16 & 0x0f | 0x40;
            raw[7] = d1 >> 24 & 0xff;
            raw[8] = d2 & 0x3f | 0x80;
            raw[9] = d2 >> 8 & 0xff;
            raw[10] = d2 >> 16 & 0xff;
            raw[11] = d2 >> 24 & 0xff;
            raw[12] = d3 & 0xff;
            raw[13] = d3 >> 8 & 0xff;
            raw[14] = d3 >> 16 & 0xff;
            raw[15] = d3 >> 24 & 0xff;
            return UUID.fromByteArray(raw);
        };
        /**
         * Generate a UUID to string
         *
         * @returns {string} UUID 값 생성
         */
        UUID.generate = function () {
            var lut = UUID.sLut;
            var d0 = Math.random() * 0xffffffff | 0;
            var d1 = Math.random() * 0xffffffff | 0;
            var d2 = Math.random() * 0xffffffff | 0;
            var d3 = Math.random() * 0xffffffff | 0;
            return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
                lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
                lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
                lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff];
        };
        UUID.sLut = [];
        UUID.lookup = [];
        return UUID;
    }());
    exports.UUID = UUID;
    UUID.initialize();
});
// ------------------------------------------------------------------------
// GOOGLE/PROTOBUF
// ------------------------------------------------------------------------
define("proto", ["require", "exports", "uuid"], function (require, exports, uuid_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * 빈 오브젝트 형태, VOID
     */
    var Empty = /** @class */ (function () {
        function Empty() {
        }
        return Empty;
    }());
    exports.Empty = Empty;
    /**
     * 기간
     */
    var Duration = /** @class */ (function () {
        /**
         * 생성자
         */
        function Duration() {
            this.seconds = 0;
            this.nanos = 0;
        }
        return Duration;
    }());
    exports.Duration = Duration;
    /**
     * NOT USED, used as Date()
     */
    var Timestamp = /** @class */ (function () {
        /**
         * 생성자,
         * 자동으로 현재의 시간을 구한다.
         */
        function Timestamp() {
            this.getCurrentTimestamp();
        }
        /**
         * 현재의 시간을 구한다.
         */
        Timestamp.prototype.getCurrentTimestamp = function () {
            var ts = Date.now();
            this.seconds = Math.floor(ts / 1000);
            this.nanos = Math.floor(ts % 1000) * 1000000;
        };
        return Timestamp;
    }());
    exports.Timestamp = Timestamp;
    /**
     * 일반적인 비정형 데이터를 담을 수 있는 그릇입니다.
     */
    var Struct = /** @class */ (function () {
        /**
         * 생성자
         */
        function Struct() {
        }
        return Struct;
    }());
    exports.Struct = Struct;
    // ------------------------------------------------------------------------
    // COMMON/M2U/DIALOG
    // ------------------------------------------------------------------------
    // ----------------------------------------------------------------------
    // 사용자 발화: Utter
    // ----------------------------------------------------------------------
    /**
     * 입력 유형
     */
    var InputType;
    (function (InputType) {
        /**
         * STT를 통한 입력
         */
        InputType["SPEECH"] = "SPEECH";
        /**
         * 타이핑에 의한 입력
         */
        InputType["KEYBOARD"] = "KEYBOARD";
        /**
         * CARD 등에 지정된 텍스트를 보내는 경우
         */
        InputType["TOUTCH"] = "TOUTCH";
        /**
         * 이미지 인식의 결과를 보내는 경우, 이미지 애노테이션을 통한 처리
         */
        InputType["IMAGE"] = "IMAGE";
        /**
         *  OCR 이미지 인식의 결과를 보내는 경우
         */
        InputType["IMAGE_DOCUMENT"] = "IMAGE_DOCUMENT";
        /**
         * 이미지 인식의 결과를 보내는 경우
         */
        InputType["VIDEO"] = "VIDEO";
        /**
         * 세션을 생성하는 등의 최초 이벤트, 프로그램 등의 명시적인 이벤트로 처리하는 방식
         */
        InputType["OPEN_EVENT"] = "OPEN_EVENT";
    })(InputType = exports.InputType || (exports.InputType = {}));
    /**
     * 사용자의 입력 데이터
     * 입력의 유형은 매우 다양할 수 있습니다.
     */
    var Utter = /** @class */ (function () {
        /**
         * 생성자
         */
        function Utter() {
            this.utter = '';
            this.inputType = InputType.KEYBOARD;
            this.lang = Lang.ko_KR;
            this.altUtters = [];
            this.meta = new Struct();
        }
        return Utter;
    }());
    exports.Utter = Utter;
    /**
     * 명시적으로 새로운 챗봇을 엽니다. 새로운 대화 세션을 생성합니다.
     */
    var OpenUtter = /** @class */ (function () {
        /**
         * 생성자
         */
        function OpenUtter() {
            this.chatbot = '';
            this.skill = '';
            this.utter = '';
            this.meta = new Struct();
        }
        return OpenUtter;
    }());
    exports.OpenUtter = OpenUtter;
    var Event = /** @class */ (function () {
        function Event() {
            this.interface = '';
            this.operation = '';
            this.daForwardParam = new DialogAgentForwarderParam();
            this.eventPayload = new EventPayload();
        }
        return Event;
    }());
    exports.Event = Event;
    /**
     * Event 관련 payload
     */
    var EventPayload = /** @class */ (function () {
        // EventPayload 생성자
        function EventPayload() {
            this.launcherAuthorized = undefined;
            this.launcherAuthorizeFailed = undefined;
            this.launcherSlotsFilled = undefined;
            this.audioPlayerPlay = undefined;
            this.launcherDocumentRead = undefined;
        }
        Object.defineProperty(EventPayload.prototype, "_launcherAuthorized", {
            // EventPayload getter, setter 정의
            get: function () {
                return this.launcherAuthorized;
            },
            set: function (value) {
                this.launcherAuthorizeFailed = undefined;
                this.launcherSlotsFilled = undefined;
                this.audioPlayerPlay = undefined;
                this.launcherDocumentRead = undefined;
                this.launcherAuthorized = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventPayload.prototype, "_launcherAuthorizeFailed", {
            get: function () {
                return this.launcherAuthorizeFailed;
            },
            set: function (value) {
                this.launcherAuthorized = undefined;
                this.launcherSlotsFilled = undefined;
                this.audioPlayerPlay = undefined;
                this.launcherDocumentRead = undefined;
                this.launcherAuthorizeFailed = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventPayload.prototype, "_launcherSlotsFilled", {
            get: function () {
                return this.launcherSlotsFilled;
            },
            set: function (value) {
                this.launcherAuthorized = undefined;
                this.launcherAuthorizeFailed = undefined;
                this.audioPlayerPlay = undefined;
                this.launcherDocumentRead = undefined;
                this.launcherSlotsFilled = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventPayload.prototype, "_audioPlayerPlay", {
            get: function () {
                return this.audioPlayerPlay;
            },
            set: function (value) {
                this.launcherAuthorized = undefined;
                this.launcherAuthorizeFailed = undefined;
                this.launcherSlotsFilled = undefined;
                this.launcherDocumentRead = undefined;
                this.audioPlayerPlay = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventPayload.prototype, "_launcherDocumentRead", {
            get: function () {
                return this.launcherDocumentRead;
            },
            set: function (value) {
                this.launcherAuthorized = undefined;
                this.launcherAuthorizeFailed = undefined;
                this.launcherSlotsFilled = undefined;
                this.audioPlayerPlay = undefined;
                this.launcherDocumentRead = value;
            },
            enumerable: true,
            configurable: true
        });
        return EventPayload;
    }());
    exports.EventPayload = EventPayload;
    // ------------------------------------------------------------------------
    // M2U/COMMON/DEVICE
    // ------------------------------------------------------------------------
    /**
     * 단말의 처리 능력
     */
    var Capability = /** @class */ (function () {
        /**
         * 생성자
         */
        function Capability() {
            /**
             * OUTPUT: 텍스트를 출력할 수 있다.
             */
            this.supportRenderText = false;
            this.supportRenderText = true;
            this.supportRenderCard = true;
            this.supportSpeechSynthesizer = true;
            this.supportPlayAudio = false;
            this.supportAction = true;
            this.supportMove = false;
            this.supportExpectSpeech = true;
        }
        return Capability;
    }());
    exports.Capability = Capability;
    /**
     * 장치
     */
    var Device = /** @class */ (function () {
        /**
         * 생성자
         */
        function Device() {
            this.id = uuid_1.UUID.generate();
            this.type = '';
            this.version = '';
            this.channel = '';
            this.support = new Capability();
            this.timestamp = new Date();
            this.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            this.meta = new Struct();
        }
        return Device;
    }());
    exports.Device = Device;
    // ------------------------------------------------------------------------
    // M2U/COMMON/DIRECTIVE
    // ------------------------------------------------------------------------
    // ----------------------------------------------------------------------
    // 디렉티브
    // ----------------------------------------------------------------------
    /**
     * DA에서 단말로 내려보내주는 PAYLOAD
     * 아바타 출력을 위한 페이로드
     */
    var AvatarSetExpressionPayload = /** @class */ (function () {
        /**
         * 생성자
         */
        function AvatarSetExpressionPayload() {
            this.target = '';
            this.expression = '';
            this.desciption = '';
        }
        return AvatarSetExpressionPayload;
    }());
    exports.AvatarSetExpressionPayload = AvatarSetExpressionPayload;
    /**
     * 오디오 출력을 위한 페이로드
     * 사용자에게 오디오 스트림또는 오디오 파일을 스피커로 출력하도록 지시한다.
     */
    var AudioPlayPayload = /** @class */ (function () {
        function AudioPlayPayload() {
        }
        return AudioPlayPayload;
    }());
    exports.AudioPlayPayload = AudioPlayPayload;
    /**
     * 오디오 스트림의 정보
     */
    var Stream = /** @class */ (function () {
        function Stream() {
            this.id = '';
            this.url = '';
            this.playTime = new Duration();
        }
        return Stream;
    }());
    exports.Stream = Stream;
    /**
     * 오디오 스트림의 정보
     */
    var Metadata = /** @class */ (function () {
        function Metadata() {
            this.title = '';
            this.subtitle = '';
        }
        return Metadata;
    }());
    exports.Metadata = Metadata;
    /**
     * 부가 정보의 위치
     */
    var Sources = /** @class */ (function () {
        function Sources() {
            this.urls = [];
        }
        return Sources;
    }());
    exports.Sources = Sources;
    /**
     * 오디오 출력의 중지
     */
    var AudioStopPayload = /** @class */ (function () {
        function AudioStopPayload() {
            this.id = '';
            this.reason = '';
        }
        return AudioStopPayload;
    }());
    exports.AudioStopPayload = AudioStopPayload;
    /**
     * 비디오 출력을 위한 페이로드
     */
    var VideoPlayPayload = /** @class */ (function () {
        function VideoPlayPayload() {
            this.id = '';
            this.title = '';
            this.url = '';
            this.playTime = new Duration();
        }
        return VideoPlayPayload;
    }());
    exports.VideoPlayPayload = VideoPlayPayload;
    /**
     * 지도표시를 위한 페이로드
     */
    var LauncherViewMapPayload = /** @class */ (function () {
        /**
         * 생성자
         */
        function LauncherViewMapPayload() {
            this.searchRange = '';
            this.searchObject = '';
            this.searchKeyword = '';
        }
        return LauncherViewMapPayload;
    }());
    exports.LauncherViewMapPayload = LauncherViewMapPayload;
    // ------------------------------------------------------------------------
    // M2U/COMMON/EVENT
    // ------------------------------------------------------------------------
    /**
     * 단말에 대화 처리에 대한 일부 데이터 위임을 하는 경우에 대한 처리
     * 모든 처리를 SKIP하기 위해서 최대한 상세한 정보를 넘겨주도록 한다.
     */
    /**
     * 인증 처리를 위한 페이로드
     */
    var LauncherAuthorizePayload = /** @class */ (function () {
        /**
         * 생성자
         */
        function LauncherAuthorizePayload() {
            this.provider = '';
            this.method = '';
            this.meta = new Struct();
        }
        return LauncherAuthorizePayload;
    }());
    exports.LauncherAuthorizePayload = LauncherAuthorizePayload;
    /**
     * 단말 정보 획득 위한 페이로드
     */
    var LauncherFillSlotsPayload = /** @class */ (function () {
        function LauncherFillSlotsPayload() {
            this.filledSlots = new Struct();
            this.requestingSlots = [];
        }
        return LauncherFillSlotsPayload;
    }());
    exports.LauncherFillSlotsPayload = LauncherFillSlotsPayload;
    /**
     * 문서를 읽은 결과 처리하기 위한 페이로드
     */
    var LauncherReadDocumentPayload = /** @class */ (function () {
        /**
         * 생성자
         */
        function LauncherReadDocumentPayload() {
            this.id = '';
            this.documentUrl = '';
            this.title = '';
            this.subTitle = '';
        }
        return LauncherReadDocumentPayload;
    }());
    exports.LauncherReadDocumentPayload = LauncherReadDocumentPayload;
    /**
     * 권한 부여 성공
     */
    var LauncherAuthorized = /** @class */ (function () {
        /**
         * 생성자
         */
        function LauncherAuthorized() {
            this.accessToken = '';
            this.authorizedAt = new Date();
        }
        return LauncherAuthorized;
    }());
    exports.LauncherAuthorized = LauncherAuthorized;
    /**
     * 권한 부여 실패
     */
    var LauncherAuthorizeFailed = /** @class */ (function () {
        /**
         * 생성자
         */
        function LauncherAuthorizeFailed() {
            this.accessToken = '';
            this.authorizedAt = new Date();
        }
        return LauncherAuthorizeFailed;
    }());
    exports.LauncherAuthorizeFailed = LauncherAuthorizeFailed;
    /**
     * 슬롯이 채워진 경우에 그 값을 정해준다.
     */
    var LauncherSlotsFilled = /** @class */ (function () {
        /**
         * 생성자
         */
        function LauncherSlotsFilled() {
            this.filledSlots = new Struct();
            this.unfilledSlots = [];
        }
        return LauncherSlotsFilled;
    }());
    exports.LauncherSlotsFilled = LauncherSlotsFilled;
    /**
     * AudioPlayerPlay 관련 payload 정보
     */
    var AudioPlayerPlay = /** @class */ (function () {
        /**
         * 생성자
         */
        function AudioPlayerPlay() {
            this.id = '';
            this.reason = '';
        }
        return AudioPlayerPlay;
    }());
    exports.AudioPlayerPlay = AudioPlayerPlay;
    /**
     * 문서 읽기 유/무를 처리하기 위한 정보
     */
    var LauncherDocumentRead = /** @class */ (function () {
        /**
         * 생성자
         */
        function LauncherDocumentRead() {
            this.id = '';
            this.read = false;
        }
        return LauncherDocumentRead;
    }());
    exports.LauncherDocumentRead = LauncherDocumentRead;
    /**
     * 클라이언트에게 LaunchApp을 실행하는 경우
     * interface: Client
     * operation: LaunchApp
     */
    var LauncherLaunchAppPayload = /** @class */ (function () {
        function LauncherLaunchAppPayload() {
            this.target = '';
            this.uri = '';
            this.meta = {};
            this.runtimeEnv = '';
            this.description = '';
        }
        return LauncherLaunchAppPayload;
    }());
    exports.LauncherLaunchAppPayload = LauncherLaunchAppPayload;
    /**
     * 클라이언트에게 LaunchPage을 실행하는 경우
     * 주로 웹 브라우저인 M2U 단말이 실행을 가능성이 높습니다.
     *
     * interface: Client
     * operation: LaunchPage
     */
    var LauncherLaunchPagePayload = /** @class */ (function () {
        function LauncherLaunchPagePayload() {
            this.target = '';
            this.uri = '';
            this.meta = {};
            this.browserEnv = '';
            this.description = '';
        }
        return LauncherLaunchPagePayload;
    }());
    exports.LauncherLaunchPagePayload = LauncherLaunchPagePayload;
    /**
     * DA에서 단말로 내려보나는 경우에는 현재의 세션, SKILL, INENT를 모두 담아서 내려보내고
     * 이를 바로 받아볼 수 있도록 처리한다.
     */
    var DialogAgentForwarderParam = /** @class */ (function () {
        /**
         * 생성자
         */
        function DialogAgentForwarderParam() {
            this.chatbot = '';
            this.skill = '';
            this.intent = '';
            this.sessionId = 0;
        }
        return DialogAgentForwarderParam;
    }());
    exports.DialogAgentForwarderParam = DialogAgentForwarderParam;
    // ------------------------------------------------------------------------
    // M2U/COMMON/LOCATION
    // ------------------------------------------------------------------------
    /**
     * 위경도를 포함한 위치 정보
     */
    var Location = /** @class */ (function () {
        /**
         * 생성자
         */
        function Location() {
            this.latitude = 0;
            this.longitude = 0;
            this.location = '';
            this.refreshAt = new Date();
        }
        return Location;
    }());
    exports.Location = Location;
    // ------------------------------------------------------------------------
    // COMMON/LANG
    // ------------------------------------------------------------------------
    /**
     * 언어와 로캘을 함께 포함한 구조
     */
    var Lang;
    (function (Lang) {
        /**
         * 한국어, 한국
         */
        Lang["ko_KR"] = "ko_KR";
        /**
         * 영어 미국
         */
        Lang["en_US"] = "en_US";
    })(Lang = exports.Lang || (exports.Lang = {}));
    // ------------------------------------------------------------------------
    // BRAIN/IR
    // ------------------------------------------------------------------------
    /**
     * 이미지 포맷
     */
    var ImageFormat;
    (function (ImageFormat) {
        /** JPEG */
        ImageFormat["JPEG"] = "JPEG";
        /** PNG */
        ImageFormat["PNG"] = "PNG";
        /** bitmap */
        ImageFormat["BMP"] = "BMP";
        /** TIFF format */
        ImageFormat["TIFF"] = "TIFF";
        /** pdf format */
        ImageFormat["PDF"] = "PDF";
    })(ImageFormat = exports.ImageFormat || (exports.ImageFormat = {}));
    /**
     * Image Recognition Param 에 사용될 수 있는 파라미터.
     */
    var ImageRecognitionParam = /** @class */ (function () {
        /** 생성자 */
        function ImageRecognitionParam(init) {
            /**
             * 출력 인코딩 포맷
             */
            this.imageFormat = ImageFormat.JPEG;
            /**
             * 출력 언어
             */
            this.lang = Lang.ko_KR;
            /**
             * guide 비율의 가로 값
             */
            this.refVertexX = 0.104167;
            /**
             * guide 비율의 세로 값
             */
            this.refVertexY = 0.138889;
            /**
             * 너비
             */
            this.width = 1024;
            /**
             * 높이
             */
            this.height = 768;
            __assign(this, init);
        }
        return ImageRecognitionParam;
    }());
    exports.ImageRecognitionParam = ImageRecognitionParam;
    /**
     * 주의: Result로 표현되는 경우는 하나의 요청에 대한 응답으로만 동작하는 것이 아니라
     * 다른 곳에 재전송할 수 있는 데이터 유형일 때이다.
     */
    /**
     * 이미지 인식 결과물
     *
     */
    var ImageRecognitionResult = /** @class */ (function () {
        /**
         * 생성자
         */
        function ImageRecognitionResult() {
            this.imageClass = '';
            this.annotatedTexts = [];
            this.meta = new Struct();
        }
        return ImageRecognitionResult;
    }());
    exports.ImageRecognitionResult = ImageRecognitionResult;
    // ------------------------------------------------------------------------
    // BRAIN/IR
    // ------------------------------------------------------------------------
    /**
     * 음성인식을 위한 파라미터
     *
     * AudioEncoding, sample_rate 등의 옵션을 가지고 있다.
     */
    var SpeechRecognitionParam = /** @class */ (function () {
        /**
         * 생성자
         */
        function SpeechRecognitionParam() {
            this.encoding = AudioEncoding.LINEAR16;
            this.sampleRate = 16000;
            this.lang = Lang.ko_KR;
            this.model = 'baseline';
            this.singleUtterance = true;
        }
        return SpeechRecognitionParam;
    }());
    exports.SpeechRecognitionParam = SpeechRecognitionParam;
    /**
     * Indicates the type of speech event.
     */
    var SpeechEventType;
    (function (SpeechEventType) {
        /** No speech event specified. */
        SpeechEventType["SPEECH_EVENT_UNSPECIFIED"] = "SPEECH_EVENT_UNSPECIFIED";
        /**
         * This event indicates that the server has detected the end of the user's
         * speech utterance and expects no additional speech. Therefore, the server
         * will not process additional audio (although it may subsequently return
         * additional results). The client should stop sending additional audio
         * data, half-close the gRPC connection, and wait for any additional results
         * until the server closes the gRPC connection. This event is only sent if
         * `single_utterance` was set to `true`, and is not used otherwise.
         */
        SpeechEventType["END_OF_SINGLE_UTTERANCE"] = "END_OF_SINGLE_UTTERANCE";
    })(SpeechEventType = exports.SpeechEventType || (exports.SpeechEventType = {}));
    /**
     * 음성인식 응답
     *
     * `StreamingRecognizeResponse` is the only message returned to the client by
     * `StreamingRecognize`. A series of zero or more `StreamingRecognizeResponse`
     * messages are streamed back to the client. If there is no recognizable
     * audio, and `single_utterance` is set to false, then no messages are streamed
     * back to the client.
     * - Only two of the above responses #4 and #7 contain final results; they are
     * indicated by `is_final: true`. Concatenating these together generates the
     * full transcript: "to be or not to be that is the question".
     *
     * - The others contain interim `results`. #3 and #6 contain two interim
     * `results`: the first portion has a high stability and is less likely to
     * change; the second portion has a low stability and is very likely to
     * change. A UI designer might choose to show only high stability `results`.
     *
     * - The specific `stability` and `confidence` values shown above are only for
     * illustrative purposes. Actual values may vary.
     *
     * - In each response, only one of these fields will be set:
     *`error`,
     * `speech_event_type`, or
     * one or more (repeated) `results`.
     */
    var StreamingRecognizeResponse = /** @class */ (function () {
        function StreamingRecognizeResponse() {
            this.results = undefined;
            this.speechEventType = SpeechEventType.END_OF_SINGLE_UTTERANCE;
        }
        return StreamingRecognizeResponse;
    }());
    exports.StreamingRecognizeResponse = StreamingRecognizeResponse;
    /**
     * STT Result를 위한 타입
     * 단어 정보
     */
    var WordInfo = /** @class */ (function () {
        function WordInfo() {
            this.startTime = new Duration();
            this.endTime = new Duration();
            this.word = '';
        }
        return WordInfo;
    }());
    exports.WordInfo = WordInfo;
    /**
     * 음성인식 결과
     */
    var SpeechRecognitionResult = /** @class */ (function () {
        function SpeechRecognitionResult() {
            this.transcript = '';
            this.final = false;
            this.words = [];
        }
        return SpeechRecognitionResult;
    }());
    exports.SpeechRecognitionResult = SpeechRecognitionResult;
    // ------------------------------------------------------------------------
    // BRAIN/TTS
    // ------------------------------------------------------------------------
    /**
     * TTS 관련 AsyncInterface를 호출할 때 사용될 수 있는 파라미터.
     */
    var SpeechSynthesizerParam = /** @class */ (function () {
        function SpeechSynthesizerParam() {
            this.encoding = AudioEncoding.LINEAR16;
            this.lang = Lang.ko_KR;
            this.sampleRate = 16000;
        }
        return SpeechSynthesizerParam;
    }());
    exports.SpeechSynthesizerParam = SpeechSynthesizerParam;
    // ------------------------------------------------------------------------
    // BRAIN/IR
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // M2U/COMMON/CARD
    // ------------------------------------------------------------------------
    /**
     *
     * 카드 형태의 데이터
     * 카드 형태의 데이터를 나타나도록 지시한다.
     * CHART
     *    type: 그래프 종류..
     *    options : 출력형태
     *    data: [] , 내부적으로 대시 배열 2차 3차 , object 로 동작한다
     * SELECT CARD
     *  title, 요약 사진 , selected utter,
     * NEWS LIST
     *  title, 요약, 사진 .. 링크 ...
     */
    /**
     * 차트 형태의 데이터
     * google charts 형식을 지원할 수 있도록 정의한다.
     */
    var ChartCard = /** @class */ (function () {
        function ChartCard() {
            this.type = '';
            this.data = [];
        }
        return ChartCard;
    }());
    exports.ChartCard = ChartCard;
    /**
     * Grid 형태의 데이터
     */
    var GridCard = /** @class */ (function () {
        function GridCard() {
            this.type = '';
            this.columns = {};
            this.rows = [];
        }
        return GridCard;
    }());
    exports.GridCard = GridCard;
    /**
     * 테이블 형태의 데이터
     */
    var TableCard = /** @class */ (function () {
        function TableCard() {
            this.type = '';
        }
        return TableCard;
    }());
    exports.TableCard = TableCard;
    /**
     * 테이블 Card 내 ITEM
     */
    var TableItem = /** @class */ (function () {
        function TableItem() {
            this.key = '';
            this.value = '';
            this.style = '';
        }
        return TableItem;
    }());
    exports.TableItem = TableItem;
    /**
     * 리스트 형태의 테이블 형태의 데이터
     */
    var TableListCard = /** @class */ (function () {
        function TableListCard() {
            this.cards = [];
            this.horizontal = true;
        }
        return TableListCard;
    }());
    exports.TableListCard = TableListCard;
    /**
     * 선택형 카드
     *
     * 값을 선택할 수 있는 선택형 카드가 필요하다.
     */
    var SelectCard = /** @class */ (function () {
        function SelectCard() {
            this.title = '';
            this.header = '';
            this.type = '';
        }
        return SelectCard;
    }());
    exports.SelectCard = SelectCard;
    /**
     * 카드내 ITEM
     */
    var Item = /** @class */ (function () {
        function Item() {
            this.title = '';
            this.summary = '';
            this.imageUrl = '';
            this.selectedUtter = '';
            this.style = '';
        }
        return Item;
    }());
    exports.Item = Item;
    /**
     * 링크 형태의 카드
     */
    var LinkCard = /** @class */ (function () {
        function LinkCard() {
            this.title = '';
            this.summary = '';
            this.imageUrl = '';
            this.imageHref = '';
            this.type = '';
        }
        return LinkCard;
    }());
    exports.LinkCard = LinkCard;
    /**
     * 리스트 형태의 카드들
     */
    var ListCard = /** @class */ (function () {
        function ListCard() {
            this.cards = [];
            this.horizontal = true;
        }
        return ListCard;
    }());
    exports.ListCard = ListCard;
    /**
     * 사용자 지정 카드
     */
    var CustomCard = /** @class */ (function () {
        function CustomCard() {
            this.type = '';
            this.cardData = new Struct();
        }
        return CustomCard;
    }());
    exports.CustomCard = CustomCard;
    /**
     * 사용자 지정 카드 목록
     */
    var CustomListCard = /** @class */ (function () {
        function CustomListCard() {
            this.cards = [];
            this.horizontal = true;
        }
        return CustomListCard;
    }());
    exports.CustomListCard = CustomListCard;
    /**
     * 메시지 카드
     */
    var Card = /** @class */ (function () {
        function Card(init) {
            this.chart = undefined;
            this.table = undefined;
            this.grid = undefined;
            this.select = undefined;
            this.link = undefined;
            this.custom = undefined;
            this.linkList = undefined;
            this.customList = undefined;
            this.tableList = undefined;
            this.raw = undefined;
            this.chart = undefined;
            this.table = undefined;
            this.grid = undefined;
            this.select = undefined;
            this.link = undefined;
            this.custom = undefined;
            this.linkList = undefined;
            this.customList = undefined;
            this.tableList = undefined;
            this.raw = undefined;
            __assign(this, init);
        }
        Object.defineProperty(Card.prototype, "_chart", {
            get: function () {
                return this.chart;
            },
            set: function (v) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this.chart = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_table", {
            get: function () {
                return this.table;
            },
            set: function (v) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this.table = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_grid", {
            get: function () {
                return this.grid;
            },
            set: function (v) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this.grid = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_select", {
            get: function () {
                return this.select;
            },
            set: function (v) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this._select = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_link", {
            get: function () {
                return this.link;
            },
            set: function (c) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this.link = c;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_custom", {
            get: function () {
                return this.custom;
            },
            set: function (c) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this.custom = c;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_linkList", {
            get: function () {
                return this.linkList;
            },
            set: function (c) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this.linkList = c;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_customList", {
            get: function () {
                return this.customList;
            },
            set: function (c) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this.customList = c;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_tableList", {
            get: function () {
                return this.tableList;
            },
            set: function (c) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = undefined;
                this.tableList = c;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Card.prototype, "_raw", {
            get: function () {
                return this.raw;
            },
            set: function (c) {
                this.chart = undefined;
                this.table = undefined;
                this.grid = undefined;
                this.select = undefined;
                this.link = undefined;
                this.custom = undefined;
                this.linkList = undefined;
                this.customList = undefined;
                this.tableList = undefined;
                this.raw = c;
            },
            enumerable: true,
            configurable: true
        });
        return Card;
    }());
    exports.Card = Card;
    // ------------------------------------------------------------------------
    // COMMON/AUDIOENCODING
    // ------------------------------------------------------------------------
    var AudioEncoding;
    (function (AudioEncoding) {
        /**
         * Not specified. Will return result [google.rpc.Code.INVALID_ARGUMENT][google.rpc.Code.INVALID_ARGUMENT].
         */
        AudioEncoding["ENCODING_UNSPECIFIED"] = "ENCODING_UNSPECIFIED";
        /**
         * Uncompressed 16-bit signed little-endian samples (Linear PCM).
         */
        AudioEncoding["LINEAR16"] = "LINEAR16";
        /**
         * [`FLAC`](https://xiph.org/flac/documentation.html) (Free Lossless Audio
         * Codec) is the recommended encoding because it is
         * lossless--therefore recognition is not compromised--and
         * requires only about half the bandwidth of `LINEAR16`. `FLAC` stream
         * encoding supports 16-bit and 24-bit samples, however, not all fields in
         * `STREAMINFO` are supported.
         */
        AudioEncoding["FLAC"] = "FLAC";
        /**
         * 8-bit samples that compand 14-bit audio samples using G.711 PCMU/mu-law.
         */
        AudioEncoding["MULAW"] = "MULAW";
        /**
         * Adaptive Multi-Rate Narrowband codec. `sample_rate_hertz` must be 8000.
         */
        AudioEncoding["AMR"] = "AMR";
        /**
         * Adaptive Multi-Rate Wideband codec. `sample_rate_hertz` must be 16000.
         */
        AudioEncoding["AMR_WB"] = "AMR_WB";
        /**
         * Opus encoded audio frames in Ogg container
         * ([OggOpus](https://wiki.xiph.org/OggOpus)).
         * `sample_rate_hertz` must be 16000.
         */
        AudioEncoding["OGG_OPUS"] = "OGG_OPUS";
        /**
         * Although the use of lossy encodings is not recommended, if a very low
         * bitrate encoding is required, `OGG_OPUS` is highly preferred over
         * Speex encoding. The [Speex](https://speex.org/)  encoding supported by
         * Cloud Speech API has a header byte in each block, as in MIME type
         * `audio/x-speex-with-header-byte`.
         * It is a variant of the RTP Speex encoding defined in
         * [RFC 5574](https://tools.ietf.org/html/rfc5574).
         * The stream is a sequence of blocks, one block per RTP packet. Each block
         * starts with a byte containing the length of the block, in bytes, followed
         * by one or more frames of Speex data, padded to an integral number of
         * bytes (octets) as specified in RFC 5574. In other words, each RTP header
         * is replaced with a single byte containing the block length. Only Speex
         * wideband is supported. `sample_rate_hertz` must be 16000.
         */
        AudioEncoding["SPEEX_WITH_HEADER_BYTE"] = "SPEEX_WITH_HEADER_BYTE";
    })(AudioEncoding = exports.AudioEncoding || (exports.AudioEncoding = {}));
    // ------------------------------------------------------------------------
    // M2U/MAP/AUTHENTICATION
    // ------------------------------------------------------------------------
    var AuthenticationParam = /** @class */ (function () {
        function AuthenticationParam(method, value) {
            this.method = method;
            this.value = value;
        }
        return AuthenticationParam;
    }());
    exports.AuthenticationParam = AuthenticationParam;
    /**
     * 각 인증 방법에 따른 인증 파라미터들
     * 인증 페이로드
     */
    var SignInPayload = /** @class */ (function () {
        function SignInPayload(userkey, passphrase) {
            this.userkey = userkey;
            this.passphrase = passphrase;
            this.authParams = [];
            this.device = new Device();
        }
        return SignInPayload;
    }());
    exports.SignInPayload = SignInPayload;
    /**
     * 인증 결과 PAYLOAD
     */
    var SignInResultPayload = /** @class */ (function () {
        function SignInResultPayload() {
            this.authFailure = undefined;
            this.authSuccess = undefined;
            this.multiFactorAuthRequest = undefined;
        }
        return SignInResultPayload;
    }());
    exports.SignInResultPayload = SignInResultPayload;
    /**
     * 인증 성공 데이터
     *
     * 인증 성공 시점에 단말에 전달되는 데이터
     * 단말은 인증 토콘을 저장해야 한다.
     */
    var AuthTokenPayload = /** @class */ (function () {
        function AuthTokenPayload() {
            this.authToken = '';
            this.multiFactor = false;
            this.meta = new Struct();
        }
        return AuthTokenPayload;
    }());
    exports.AuthTokenPayload = AuthTokenPayload;
    /**
     * 인증 실패 페이로드
     * 인증 싶패의 이유를 명시적으로 적시해준다.
     */
    var AuthFailurePayload = /** @class */ (function () {
        function AuthFailurePayload() {
            this.resCode = '';
            this.message = '';
            this.detailMessage = '';
        }
        return AuthFailurePayload;
    }());
    exports.AuthFailurePayload = AuthFailurePayload;
    var MultiFactorAuthMethod = /** @class */ (function () {
        function MultiFactorAuthMethod() {
            this.method = '';
            this.param = new Struct();
        }
        return MultiFactorAuthMethod;
    }());
    exports.MultiFactorAuthMethod = MultiFactorAuthMethod;
    /**
     * MULTI FACTOR 인증을 위한 추가적인 인증 밥업과
     * 인증 방식을 정의합니다.
     */
    var MultiFactorAuthRequestPayload = /** @class */ (function () {
        function MultiFactorAuthRequestPayload() {
            this.tempAuthToken = '';
            this.multiFactorAuthMethods = [];
        }
        return MultiFactorAuthRequestPayload;
    }());
    exports.MultiFactorAuthRequestPayload = MultiFactorAuthRequestPayload;
    var MultiFactorAuthResult = /** @class */ (function () {
        function MultiFactorAuthResult() {
            this.method = '';
            this.value = '';
            this.meta = new Struct();
        }
        return MultiFactorAuthResult;
    }());
    exports.MultiFactorAuthResult = MultiFactorAuthResult;
    /**
     * 다중 팩터 요청에 대한 상세 데이터
     */
    var MultiFactorVerifyPayload = /** @class */ (function () {
        function MultiFactorVerifyPayload() {
            this.tempAuthToken = '';
            this.authParams = [];
            this.multiFactorAuthResults = [];
            this.device = new Device();
        }
        return MultiFactorVerifyPayload;
    }());
    exports.MultiFactorVerifyPayload = MultiFactorVerifyPayload;
    /**
     * SignOut 페이로드
     */
    var SignOutPayload = /** @class */ (function () {
        function SignOutPayload(authtoken) {
            this.authToken = authtoken;
            this.userKey = '';
        }
        return SignOutPayload;
    }());
    exports.SignOutPayload = SignOutPayload;
    /**
     * 응답 메시지
     */
    var SignOutResultPayload = /** @class */ (function () {
        function SignOutResultPayload() {
            this.message = '';
        }
        return SignOutResultPayload;
    }());
    exports.SignOutResultPayload = SignOutResultPayload;
    /**
     * 사용자 속성 정의 데이터
     */
    var UserSettings = /** @class */ (function () {
        function UserSettings() {
            this.authToken = '';
            this.settings = new Struct();
        }
        return UserSettings;
    }());
    exports.UserSettings = UserSettings;
    /**
     * 사용자 속성 조회를 위한 키
     */
    var UserKey = /** @class */ (function () {
        function UserKey() {
            this.authToken = '';
        }
        return UserKey;
    }());
    exports.UserKey = UserKey;
    // ------------------------------------------------------------------------
    // M2U/MAP/MAP
    // ------------------------------------------------------------------------
    //
    // PING
    //
    /**
     * 주기적인 Ping 요청
     */
    var PingRequest = /** @class */ (function () {
        function PingRequest() {
        }
        return PingRequest;
    }());
    exports.PingRequest = PingRequest;
    /**
     * PONG을 통한 클라이언트 상태 표시
     */
    var PongClientState;
    (function (PongClientState) {
        /**
         * 기존에 이미 존재한 클라이언트 지속
         */
        PongClientState["PONG_CLIENT_CONTINUE"] = "PONG_CLIENT_CONTINUE";
        /**
         * 새로운 클라이언트가 발견되었고 이를 등록처리
         */
        PongClientState["PONG_NEW_CLIENT_FOUND"] = "PONG_NEW_CLIENT_FOUND";
        /**
         * 기존의 클라이언트가 재시작 되었음을 인지했음.
         */
        PongClientState["PONG_IDLE_CLIENT_RESTART"] = "PONG_IDLE_CLIENT_RESTART";
        /**
         * 접근이 거부되었음
         */
        PongClientState["PONG_ACCESS_DENIED"] = "PONG_ACCESS_DENIED";
    })(PongClientState = exports.PongClientState || (exports.PongClientState = {}));
    /**
     * PONG 응답
     */
    var PongResponse = /** @class */ (function () {
        function PongResponse() {
            this.clientState = PongClientState.PONG_NEW_CLIENT_FOUND;
            this.m2uId = new MaumToYouIdentifier();
            this.requireEventStream = false;
            this.dirState = new DirectiveState();
            this.version = "0.1";
            this.pongAt = new Date();
        }
        return PongResponse;
    }());
    exports.PongResponse = PongResponse;
    /**
     * M2U 서버에 대한 구분
     */
    var MaumToYouIdentifier = /** @class */ (function () {
        function MaumToYouIdentifier() {
            this.uuid = '';
            this.version = '';
            this.licensedTo = '';
        }
        return MaumToYouIdentifier;
    }());
    exports.MaumToYouIdentifier = MaumToYouIdentifier;
    /**
     * 디렉티브 상태
     */
    var DirectiveState = /** @class */ (function () {
        function DirectiveState() {
            this.pending = false;
            this.pendingCount = 0;
            this.discarded = false;
            this.discardedCount = 0;
        }
        return DirectiveState;
    }());
    exports.DirectiveState = DirectiveState;
    //
    // EVENTS
    //
    var EventContext = /** @class */ (function () {
        function EventContext() {
            this.device = null;
            this.location = null;
        }
        EventContext.prototype.setDevice = function (device) {
            this.location = undefined;
            this.device = device;
        };
        EventContext.prototype.setLocation = function (loc) {
            this.location = loc;
            this.device = undefined;
        };
        return EventContext;
    }());
    exports.EventContext = EventContext;
    var EventParam = /** @class */ (function () {
        function EventParam() {
            this.speechRecognitionParam = undefined;
            this.imageRecognitionParam = undefined;
            this.videoParam = undefined;
            this.gestureParam = undefined;
            this.keyboardParam = undefined;
            this.emptyParam = undefined;
            this.dialogAgentForwarderParam = undefined;
        }
        Object.defineProperty(EventParam.prototype, "_speechRecognitionParam", {
            get: function () {
                return this.speechRecognitionParam;
            },
            set: function (value) {
                this.imageRecognitionParam = undefined;
                this.videoParam = undefined;
                this.gestureParam = undefined;
                this.keyboardParam = undefined;
                this.emptyParam = undefined;
                this.dialogAgentForwarderParam = undefined;
                this.speechRecognitionParam = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventParam.prototype, "_imageRecognitionParam", {
            get: function () {
                return this.imageRecognitionParam;
            },
            set: function (value) {
                this.speechRecognitionParam = undefined;
                this.videoParam = undefined;
                this.gestureParam = undefined;
                this.keyboardParam = undefined;
                this.emptyParam = undefined;
                this.dialogAgentForwarderParam = undefined;
                this.imageRecognitionParam = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventParam.prototype, "_videoParam", {
            get: function () {
                return this.videoParam;
            },
            set: function (value) {
                this.speechRecognitionParam = undefined;
                this.imageRecognitionParam = undefined;
                this.gestureParam = undefined;
                this.keyboardParam = undefined;
                this.emptyParam = undefined;
                this.dialogAgentForwarderParam = undefined;
                this.videoParam = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventParam.prototype, "_gestureParam", {
            get: function () {
                return this.gestureParam;
            },
            set: function (value) {
                this.speechRecognitionParam = undefined;
                this.imageRecognitionParam = undefined;
                this.keyboardParam = undefined;
                this.emptyParam = undefined;
                this.videoParam = undefined;
                this.dialogAgentForwarderParam = undefined;
                this.gestureParam = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventParam.prototype, "_keyboardParam", {
            get: function () {
                return this.keyboardParam;
            },
            set: function (value) {
                this.speechRecognitionParam = undefined;
                this.imageRecognitionParam = undefined;
                this.gestureParam = undefined;
                this.emptyParam = undefined;
                this.videoParam = undefined;
                this.dialogAgentForwarderParam = undefined;
                this.keyboardParam = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventParam.prototype, "_emptyParam", {
            get: function () {
                return this.emptyParam;
            },
            set: function (value) {
                this.speechRecognitionParam = undefined;
                this.imageRecognitionParam = undefined;
                this.gestureParam = undefined;
                this.keyboardParam = undefined;
                this.videoParam = undefined;
                this.dialogAgentForwarderParam = undefined;
                this.emptyParam = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventParam.prototype, "_dialogAgentForwarderParam", {
            get: function () {
                return this.dialogAgentForwarderParam;
            },
            set: function (value) {
                this.speechRecognitionParam = undefined;
                this.imageRecognitionParam = undefined;
                this.gestureParam = undefined;
                this.keyboardParam = undefined;
                this.videoParam = undefined;
                this.dialogAgentForwarderParam = value;
                this.emptyParam = undefined;
            },
            enumerable: true,
            configurable: true
        });
        return EventParam;
    }());
    exports.EventParam = EventParam;
    var kkk = new EventParam();
    kkk._speechRecognitionParam = new SpeechRecognitionParam();
    var p = AudioEncoding.LINEAR16;
    console.log('LEANER16', p.toString(), p.valueOf());
    /**
     * 이벤트 형식의 FRAME
     * 단일 이벤트이거나 연속된 스트림 이벤트의 시작일 수 있다.
     */
    var EventStream = /** @class */ (function () {
        function EventStream() {
            this.interface = undefined;
            this.streamId = uuid_1.UUID.generate();
            this.operationSyncId = '';
            this.contexts = [];
            this.param = new EventParam();
            this.payload = new Struct();
            this.beginAt = new Date();
        }
        return EventStream;
    }());
    exports.EventStream = EventStream;
    //
    // DIRECTIVES
    //
    var DirectiveParam = /** @class */ (function () {
        function DirectiveParam() {
            this.daForwardParam = undefined;
            this.speechSynthesizerParam = undefined;
            this.videoParam = undefined;
            this.emptyParam = undefined;
        }
        return DirectiveParam;
    }());
    exports.DirectiveParam = DirectiveParam;
    /**
     * 디렉티브 형식의 프레임
     * 단일한 디렉티브 이거나 연속된 스트림 디렉티브의 시작일 수 있다.
     */
    var DirectiveStream = /** @class */ (function () {
        function DirectiveStream() {
            this.interface = new AsyncInterface();
            this.streamId = '';
            this.operationSyncId = '';
            this.param = new DirectiveParam();
            this.payload = '';
            this.beginAt = new Date();
        }
        return DirectiveStream;
    }());
    exports.DirectiveStream = DirectiveStream;
    //
    // EXCEPTION
    //
    var StatusCode;
    (function (StatusCode) {
        StatusCode["STATUS_NOT_SPECIFIED"] = "STATUS_NOT_SPECIFIED";
        StatusCode["GRPC_STT_ERROR"] = "GRPC_STT_ERROR";
        StatusCode["GRPC_IDR_ERROR"] = "GRPC_IDR_ERROR";
        StatusCode["GRPC_TTS_ERROR"] = "GRPC_TTS_ERROR";
        StatusCode["GRPC_AUTH_SIGN_IN_ERROR"] = "GRPC_AUTH_SIGN_IN_ERROR";
        StatusCode["GRPC_AUTH_SIGN_OUT_ERROR"] = "GRPC_AUTH_SIGN_OUT_ERROR";
        StatusCode["GRPC_AUTH_MULTIFACTOR_VERIFY_ERROR"] = "GRPC_AUTH_MULTIFACTOR_VERIFY_ERROR";
        StatusCode["GRPC_AUTH_IS_VALID_ERROR"] = "GRPC_AUTH_IS_VALID_ERROR";
        StatusCode["GRPC_AUTH_GET_USER_INFO_ERROR"] = "GRPC_AUTH_GET_USER_INFO_ERROR";
        StatusCode["GRPC_AUTH_UPDATE_USER_SETTINGS_ERROR"] = "GRPC_AUTH_UPDATE_USER_SETTINGS_ERROR";
        StatusCode["GRPC_AUTH_GET_USER_SETTINGS_ERROR"] = "GRPC_AUTH_GET_USER_SETTINGS_ERROR";
        StatusCode["GRPC_ROUTER_OPEN_ERROR"] = "GRPC_ROUTER_OPEN_ERROR";
        StatusCode["GRPC_ROUTER_TALK_ERROR"] = "GRPC_ROUTER_TALK_ERROR";
        StatusCode["GRPC_ROUTER_EVENT_ERROR"] = "GRPC_ROUTER_EVENT_ERROR";
        StatusCode["GRPC_ROUTER_CLOSE_ERROR"] = "GRPC_ROUTER_CLOSE_ERROR";
        StatusCode["GRPC_ROUTER_FEEDBACK_ERROR"] = "GRPC_ROUTER_FEEDBACK_ERROR";
        StatusCode["GRPC_STT_TRANSCRIPT_NULL_ERROR"] = "GRPC_STT_TRANSCRIPT_NULL_ERROR";
        StatusCode["AUTH_IS_VAILD_FAILED"] = "AUTH_IS_VAILD_FAILED";
        StatusCode["AUTH_INVALID_AUTH_TOKEN"] = "AUTH_INVALID_AUTH_TOKEN";
        StatusCode["AUTH_FAILED"] = "AUTH_FAILED";
        StatusCode["AUTH_INVALID_HEADER"] = "AUTH_INVALID_HEADER";
        StatusCode["AUTH_CHECK_AUTH_FAILED"] = "AUTH_CHECK_AUTH_FAILED";
        StatusCode["MAP_NO_STREAM_PARAM"] = "MAP_NO_STREAM_PARAM";
        StatusCode["MAP_IF_NOT_FOUND"] = "MAP_IF_NOT_FOUND";
        StatusCode["MAP_IF_STREAMING_NOT_MATCH"] = "MAP_IF_STREAMING_NOT_MATCH";
        StatusCode["MAP_IF_DUPLICATED_STREAMING"] = "MAP_IF_DUPLICATED_STREAMING";
        StatusCode["MAP_EVENT_CASE_NOT_SET"] = "MAP_EVENT_CASE_NOT_SET";
        StatusCode["MAP_CURRENTLY_HAS_NO_STREAM"] = "MAP_CURRENTLY_HAS_NO_STREAM";
        StatusCode["MAP_STREAM_ID_NOT_MATCH"] = "MAP_STREAM_ID_NOT_MATCH";
        StatusCode["MAP_PAYLOAD_ERROR"] = "MAP_PAYLOAD_ERROR";
        StatusCode["MAP_CLASS_NOT_FOUND"] = "MAP_CLASS_NOT_FOUND";
        StatusCode["ROUTER_SESSION_NOT_FOUND"] = "ROUTER_SESSION_NOT_FOUND";
        StatusCode["ROUTER_SESSION_INVALID"] = "ROUTER_SESSION_INVALID";
        StatusCode["ROUTER_CHATBOT_NOT_FOUND"] = "ROUTER_CHATBOT_NOT_FOUND";
        StatusCode["ROUTER_DA_NOT_FOUND"] = "ROUTER_DA_NOT_FOUND";
        StatusCode["ROUTER_DA_ERROR"] = "ROUTER_DA_ERROR";
        StatusCode["ROUTER_ITF_NOT_FOUND"] = "ROUTER_ITF_NOT_FOUND";
        StatusCode["ROUTER_ITF_ERROR"] = "ROUTER_ITF_ERROR";
        StatusCode["MAP_TOTAL_SESSION_COUNT_EXCEEDED"] = "MAP_TOTAL_SESSION_COUNT_EXCEEDED";
        StatusCode["MAP_SYSTEM_MAINTENANCE"] = "MAP_SYSTEM_MAINTENANCE";
    })(StatusCode = exports.StatusCode || (exports.StatusCode = {}));
    /**
     * 예외는 Event나 Directive에 대응하여 호출시 문제가 생기면 이를 내보내도록 한다.
     */
    var MapException = /** @class */ (function () {
        function MapException() {
            this.exceptionId = '';
            this.statusCode = StatusCode.STATUS_NOT_SPECIFIED;
            this.exIndex = 0;
            this.exMessage = '';
            this.payload = new Struct();
            this.thrownAt = new Date();
            this.streamId = '';
            this.operationSyncId = '';
            this.calledInterface = new AsyncInterface();
        }
        return MapException;
    }());
    exports.MapException = MapException;
    /**
     * `StreamMeta`을 통해서
     * Event나 Directive에 추가적인 스트림 데이터를 전송할 수 있다.
     * 기본적인 스트림 데이터 형식인 문자열이나 바이트 배열 이외에 다른 메시지를
     * 포함하려고 할 때 사용할 수 있다.
     */
    var StreamMeta = /** @class */ (function () {
        function StreamMeta() {
            this.objectType = '';
            this.meta = new Struct();
        }
        return StreamMeta;
    }());
    exports.StreamMeta = StreamMeta;
    /**
     * 스트림 형식의 Event나 Directive의 끝을 표시해준다.
     */
    var StreamEnd = /** @class */ (function () {
        function StreamEnd() {
            this.streamId = '';
            this.endAt = new Date();
        }
        return StreamEnd;
    }());
    exports.StreamEnd = StreamEnd;
    /**
     * 중지를 발생시키는 타입
     */
    var StreamBreaker;
    (function (StreamBreaker) {
        /**
         * 원인을 알 수 없는 중지
         */
        StreamBreaker["BREAK_BY_UNSPECIFIED"] = "BREAK_BY_UNSPECIFIED";
        /**
         * 사용자에 의한 명시적인 중지
         */
        StreamBreaker["BREAK_BY_USER"] = "BREAK_BY_USER";
        /**
         * 클라이언트나 서버의 내부 구조에 의한 중지
         */
        StreamBreaker["BREAK_BY_SYSTEM"] = "BREAK_BY_SYSTEM";
        /**
         * Stream 전송 규칙 과정에서 발생하는 논리적인 오류
         */
        StreamBreaker["BREAK_BY_LOGICAL"] = "BREAK_BY_LOGICAL";
        /**
         * 예외에 의한 중지
         * 예를 들어서 음성 데이터를 받아오는 네트워크의 중지에 의한 예외 등
         */
        StreamBreaker["BREAK_BY_EXCEPTION"] = "BREAK_BY_EXCEPTION";
    })(StreamBreaker = exports.StreamBreaker || (exports.StreamBreaker = {}));
    var StreamBreak = /** @class */ (function () {
        function StreamBreak() {
            this.reason = '';
            this.breaker = StreamBreaker.BREAK_BY_UNSPECIFIED;
            this.streamId = '';
            this.brokenAt = new Date();
        }
        return StreamBreak;
    }());
    exports.StreamBreak = StreamBreak;
    // ###############################################
    // ASYNC INTERFACE 들을 위한 공통 규격
    /**
     * operation 타입
     */
    var OperationType;
    (function (OperationType) {
        /**
         * EVENT 유형, `클라이언트`에서 `MAP`으로 전송된다.
         */
        OperationType["OP_EVENT"] = "OP_EVENT";
        /**
         * DIRECTIVE 유형, `MAP`에서 `클라이언트`로 전송된다.
         */
        OperationType["OP_DIRECTIVE"] = "OP_DIRECTIVE";
    })(OperationType = exports.OperationType || (exports.OperationType = {}));
    /** 호출되는 대상에 대한 정의
     * 호출되는 대상은 interface와 operation으로 정의된다.
     * operation은 단순히 메소드가 아니라 이벤트 메시지이기도 하고
     * 각 상대방이 처리해야할 메시지이기도 하다.
     *
     * 예를 들어, interface는 VideoPlayer 이고 operation 은 stop 이다.
     */
    var AsyncInterface = /** @class */ (function () {
        function AsyncInterface(init) {
            /**
             * 인터페이스 이름
             * 통상적으로 변수 규칙에 따르고 대문자로 시작하고 Camel Case로 작성한다.
             * Map과 통신하는 구조에서는 기본으로 제공하는 인터페이스 외에 추가적인 인터페이스가
             * 존재할 수 있다.
             * 추가적인 인터페이스에 대해서는 플러그인 형태로 개발할 수 있도록 되어 있다.
             */
            this.interface = '';
            /**
             * 오퍼레이션 이름
             * 각 상대방에서 수행해야 할 작업의 이름이고 인터페이스에 종속적이다.
             * 이 이름은 동사이거나 발생하는 이벤트의 이름이어야 한다.
             */
            this.operation = '';
            /**
             * 메시지의 타입은 EVENT 이거나 DIRECTIVE 임.
             */
            this.type = OperationType.OP_EVENT;
            /**
             * 스트리밍 여부
             * operation은 스트리밍 형식이거나 단일 메시지 형식이다.
             * 스트리밍 형식의 operation은 EventStream, DirectiveStream과 StreamEnd 사이에
             * 반복적인 BODY 메시지를 전송할 수 있다.
             * 반복적인 BODY 형식은 string, byte array, meta 형식으로 전송될 수 있다.
             * EventStream, DirectiveStream과 StreamEnd은 같은 stream_id를 공유한다.
             * EventEnd를 만나면 해당 스트림은 종료된다.
             * 스트리밍이 아닌 AsyncInterface 형식을 가진 EventStream이나 DirectiveStream이
             * 중간에 끼여서 전송될 수 있다. 하지만, 스트리밍 이벤트 중간에 다른 스트리밍 이벤트는
             * 전송될 수 없다. 만일, 동시에 스트리밍 이벤트는 여러 개 전송해야 하는 경우에는
             * 새로운 grpc 연결을 사용해서 처리하면 된다.
             * 이 규칙을 어기고 스트림 형식의 새로운 EventStream이나 DirectiveStream이 들어오면
             * 현재의 스트림은 자동으로 종료 처리된다.
             * 스트리밍 프레임들은 StreamBreak 메시지를 통해서 중지될 수 있다.
             * 이 경우, 스트리밍 프레임은 중지되고, stream_id는 더 이상 유효하지 않게 된다.
             * 추가적인 스트리밍 BODY는 모두 버려지게 된다.
             */
            this.streaming = false;
            __assign(this, init);
        }
        return AsyncInterface;
    }());
    exports.AsyncInterface = AsyncInterface;
    /**
     * 비동기인터페이스의 목록
     */
    var AsyncInterfaceList = /** @class */ (function () {
        function AsyncInterfaceList() {
            this.interfaces = [];
        }
        return AsyncInterfaceList;
    }());
    exports.AsyncInterfaceList = AsyncInterfaceList;
    //
    // STREAM PARAMETERS
    //
    /**
     * Video 관련 AsyncInterface를 호출할 때 사용될 수 있는 파라미터.
     */
    var VideoParam = /** @class */ (function () {
        function VideoParam() {
            this.videoFormat = '';
            this.codec = '';
            this.todo = '';
        }
        return VideoParam;
    }());
    exports.VideoParam = VideoParam;
    var GestureParam = /** @class */ (function () {
        function GestureParam() {
            this.todo = '';
        }
        return GestureParam;
    }());
    exports.GestureParam = GestureParam;
    /**
     * 키보드 파라미터
     */
    var KeyboardParam = /** @class */ (function () {
        function KeyboardParam() {
            this.typedLength = 0;
            this.duration = new Duration();
        }
        return KeyboardParam;
    }());
    exports.KeyboardParam = KeyboardParam;
    // ------------------------------------------------------------------------
    // M2U/MAP/PAYLOAD
    // ------------------------------------------------------------------------
    /**
     * RenderText 호출에 따르는 Payload
     */
    var RenderTextPayload = /** @class */ (function () {
        function RenderTextPayload() {
            this.text = '';
        }
        return RenderTextPayload;
    }());
    exports.RenderTextPayload = RenderTextPayload;
    /**
     * 기존 V1, V2 호환을 위해서 또는 디버그 목적으로 DA에서 내려주는 다양한 정보들을
     * 사용하도록 내려준다.
     * 이때 meta가 그대로 내려간다.
     */
    var RenderHiddenPayload = /** @class */ (function () {
        function RenderHiddenPayload() {
            this.meta = new Struct();
        }
        return RenderHiddenPayload;
    }());
    exports.RenderHiddenPayload = RenderHiddenPayload;
    /**
     * 음성입력 대기에 필요한 추가적인 Payload 데이터
     */
    var ExpectSpeechPayload = /** @class */ (function () {
        function ExpectSpeechPayload() {
            this.timeoutInMilliseconds = 0;
        }
        return ExpectSpeechPayload;
    }());
    exports.ExpectSpeechPayload = ExpectSpeechPayload;
});
define("native", ["require", "exports", "proto"], function (require, exports, proto_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * MapSetting은 초기 데이터로 NATIVE INTERFACE를 생성하는 역할을 한다.
     * 이 설정은 주기적으로 새롭게 정의할 수 있다.
     */
    var MapSetting = /** @class */ (function () {
        /**
         * 생성자
         * @param {string} serverIp 서버 주소
         * @param {number} serverPort 서버 포트
         * @param {boolean} useTls TLS 사용 여부
         */
        function MapSetting(serverIp, serverPort, useTls) {
            this.serverIp = serverIp;
            this.serverPort = serverPort;
            this.useTls = useTls;
            this.authToken = '';
            this.device = new proto_1.Device();
        }
        /**
         * 인증 토콘을 설정한다.
         * @param {string} token
         */
        MapSetting.prototype.setAuthToken = function (token) {
            this.authToken = token;
        };
        /**
         * 핑 주기를 설정한다.
         * @param {number} interval
         */
        MapSetting.prototype.setPingInterval = function (interval) {
            this.pingInterval = interval;
        };
        return MapSetting;
    }());
    exports.MapSetting = MapSetting;
    // ---------------------------------------------------
    // MAP
    // ---------------------------------------------------
    /**
     * MAP과 통신할 때 발생할 수 있는 에러 메시지
     */
    var GrpcStatus = /** @class */ (function () {
        function GrpcStatus() {
            this.errorCode = 0;
            this.errorMessage = undefined;
            this.errorDetail = undefined;
            this.errorObject = undefined;
        }
        return GrpcStatus;
    }());
    exports.GrpcStatus = GrpcStatus;
    /**
     * 스트리밍 유형
     *
     * map에 스트리밍 형태로 데이터를 전송할 때
     * 계속되는 데이터의 유형, 크게 3가지가 있다.
     */
    var StreamingType;
    (function (StreamingType) {
        /**
         * 타임 없음
         */
        StreamingType["UNKONWN"] = "UNKONWN";
        /**
         * 바이트 형태
         */
        StreamingType["BYTES"] = "BYTES";
        /**
         * 텍스트 형태
         */
        StreamingType["TEXT"] = "TEXT";
        /**
         * 사용자 정의 데이터 형태
         */
        StreamingType["STRUCT"] = "STRUCT";
    })(StreamingType = exports.StreamingType || (exports.StreamingType = {}));
    /**
     * 스트리밍 전송 상태를 알려주는 데이터 형식
     */
    var StreamingStatus = /** @class */ (function () {
        /**
         * 생성자
         */
        function StreamingStatus() {
            this.streamId = '';
            this.operationSyncId = '';
            this.isBreak = false;
            this.isEvent = false;
            this.isEnd = false;
            this.type = StreamingType.UNKONWN;
            this.size = 0;
            this.interface = undefined;
        }
        return StreamingStatus;
    }());
    exports.StreamingStatus = StreamingStatus;
    // ---------------------------------------------------
    // MIC
    // ---------------------------------------------------
    /**
     * 마이크 개방 파라미터
     */
    var MicrophoneParam = /** @class */ (function () {
        /**
         * 생성자
         */
        function MicrophoneParam() {
            this.expectMode = false;
            this.timoeoutInMilliseconds = 0;
            this.speechParam = new proto_1.SpeechRecognitionParam();
        }
        return MicrophoneParam;
    }());
    exports.MicrophoneParam = MicrophoneParam;
    /**
     * 마이크 상태 이벤트
     */
    var MicrophoneEvent;
    (function (MicrophoneEvent) {
        /**
         * UNKNOWN
         */
        MicrophoneEvent["UNKNOWN"] = "UNKNOWN";
        /**
         * 마이크 열기 성공
         */
        MicrophoneEvent["OPEN_SUCCESS"] = "OPEN_SUCCESS";
        /**
         * 마이크 열기 실패
         */
        MicrophoneEvent["OPEN_FAILURE"] = "OPEN_FAILURE";
        /**
         * 마이크 데이터 캡춰 시작
         */
        MicrophoneEvent["STARTING_RECORD"] = "STARTING_RECORD";
        /**
         * 닫기 성공
         */
        MicrophoneEvent["CLOSE_SUCCESS"] = "CLOSE_SUCCESS";
        /**
         * 닫기 실패
         */
        MicrophoneEvent["CLOSE_FAILURE"] = "CLOSE_FAILURE";
    })(MicrophoneEvent = exports.MicrophoneEvent || (exports.MicrophoneEvent = {}));
    /**
     * 마이크 실패에 대한 상세 유형
     */
    var MicrophoneFailure;
    (function (MicrophoneFailure) {
        /**
         * 열기 실패 알수 없는 실패 또는 성공
         */
        MicrophoneFailure["MICROPHONE_ERROR_UNKNOWN"] = "MICROPHONE_ERROR_UNKNOWN";
        /**
         * 열기 에러
         */
        MicrophoneFailure["MICROPHONE_OPEN_ERROR"] = "MICROPHONE_OPEN_ERROR";
        /**
         * 닫기 에러
         */
        MicrophoneFailure["MICROPHONE_CLOSE_ERROR"] = "MICROPHONE_CLOSE_ERROR";
        /**
         * 사용 중
         */
        MicrophoneFailure["MICROPHONE_UNDER_USE"] = "MICROPHONE_UNDER_USE";
        /**
         * 대기 타이머 초과
         */
        MicrophoneFailure["MICROPHONE_TIMER_EXPIRED"] = "MICROPHONE_TIMER_EXPIRED";
        /**
         * 장치 접근이 거부됨
         */
        MicrophoneFailure["MICROPHONE_PERMISSION_DENIED"] = "MICROPHONE_PERMISSION_DENIED";
    })(MicrophoneFailure = exports.MicrophoneFailure || (exports.MicrophoneFailure = {}));
    /**
     * 마이크 상태
     */
    var MicrophoneStatus = /** @class */ (function () {
        /**
         * 생성자
         */
        function MicrophoneStatus() {
            this.eventType = MicrophoneEvent.UNKNOWN;
            this.expectMode = false;
            this.hasReason = false;
            this.failure = MicrophoneFailure.MICROPHONE_ERROR_UNKNOWN;
            this.message = '';
        }
        return MicrophoneStatus;
    }());
    exports.MicrophoneStatus = MicrophoneStatus;
    // ---------------------------------------------------
    // SPEAKER
    // ---------------------------------------------------
    /**
     * 스피커 이벤트
     */
    var SpeakerEvent;
    (function (SpeakerEvent) {
        /**
         * 스피커 데이터 출력 중
         */
        SpeakerEvent["MUTE"] = "MUTE";
        /**
         * 스피커 데이터 없음
         */
        SpeakerEvent["UNMUTE"] = "UNMUTE";
        /**
         * 스피커 닫힘
         */
        SpeakerEvent["CLOSED"] = "CLOSED";
    })(SpeakerEvent = exports.SpeakerEvent || (exports.SpeakerEvent = {}));
    /**
     * 스피커 에러 유형
     */
    var SpeakerFailure;
    (function (SpeakerFailure) {
        /**
         * 알수 없는 에러 또는 성공
         */
        SpeakerFailure["UNKONWN"] = "UNKONWN";
        /**
         * 닫기 실패
         */
        SpeakerFailure["SPAEKER_CLOSE_FAILED"] = "SPAEKER_CLOSE_FAILED";
        /**
         * 장치 접근이 거부됨
         */
        SpeakerFailure["SPAEKER_PERMISSION_DENIED"] = "SPAEKER_PERMISSION_DENIED";
    })(SpeakerFailure = exports.SpeakerFailure || (exports.SpeakerFailure = {}));
    /**
     * 스피커 상태
     */
    var SpeakerStatus = /** @class */ (function () {
        /**
         * 생성자
         */
        function SpeakerStatus() {
            this.eventType = SpeakerEvent.MUTE;
            this.speechSynthesizing = false;
            this.hasReason = false;
            this.failure = SpeakerFailure.UNKONWN;
            this.message = '';
        }
        return SpeakerStatus;
    }());
    exports.SpeakerStatus = SpeakerStatus;
    // ---------------------------------------------------
    // CAMERA
    // ---------------------------------------------------
    /**
     * 카메라 해상도
     */
    var Resolution = /** @class */ (function () {
        /**
         * 생성자
         */
        function Resolution() {
            this.min_x = 0;
            this.min_y = 0;
            this.max_x = 0;
            this.max_y = 0;
        }
        return Resolution;
    }());
    exports.Resolution = Resolution;
    /**
     * 카메라 파라미터
     */
    var CameraParam = /** @class */ (function () {
        /**
         * 생성자
         */
        function CameraParam() {
            this.rearCamera = false;
            this.openInterval = 0;
            this.res = new Resolution();
        }
        return CameraParam;
    }());
    exports.CameraParam = CameraParam;
    /**
     * 카메라 이벤트
     */
    var CameraEvent;
    (function (CameraEvent) {
        /**
         * 알수 없는 이벤트
         */
        CameraEvent["UNKNOWN"] = "UNKNOWN";
        /**
         * 열기 성공
         */
        CameraEvent["OPEN_SUCCESS"] = "OPEN_SUCCESS";
        /**
         * 열기 닫기
         */
        CameraEvent["OPEN_FAILURE"] = "OPEN_FAILURE";
        /**
         * 이미지 캡춰됨
         */
        CameraEvent["IMAGE_CAPTURED"] = "IMAGE_CAPTURED";
        /**
         * 카메라 닫힘
         */
        CameraEvent["CLOSED"] = "CLOSED";
    })(CameraEvent = exports.CameraEvent || (exports.CameraEvent = {}));
    /**
     * 카메라 실패 유형
     */
    var CameraFailure;
    (function (CameraFailure) {
        /**
         * 알수 없는 에러 또는 성공
         */
        CameraFailure["CAMERA_ERROR_UNKNOWN"] = "CAMERA_ERROR_UNKNOWN";
        /**
         * 열기 실패
         */
        CameraFailure["CAMERA_OPEN_ERROR"] = "CAMERA_OPEN_ERROR";
        /**
         * 닫기 실패
         */
        CameraFailure["CAMERA_CLOSE_ERROR"] = "CAMERA_CLOSE_ERROR";
        /**
         * 캡춰 취소
         * 카메라에서 명시적으로 사용자에 의한 취소
         */
        CameraFailure["CAMERA_CAPTURE_CANCELLED"] = "CAMERA_CAPTURE_CANCELLED";
        /**
         * 메모리 부족
         */
        CameraFailure["CAMERA_INSUFFICIENT_MEMORY"] = "CAMERA_INSUFFICIENT_MEMORY";
        /**
         * 저장 실패
         */
        CameraFailure["CAMERA_STORE_FAILED"] = "CAMERA_STORE_FAILED";
        /**
         * 카메라 타이머 지남
         */
        CameraFailure["CAMERA_TIMER_EXPIRED"] = "CAMERA_TIMER_EXPIRED";
        /**
        * 장치 접근이 거부됨
        */
        CameraFailure["CAMERA_PERMISSION_DENIED"] = "CAMERA_PERMISSION_DENIED";
    })(CameraFailure = exports.CameraFailure || (exports.CameraFailure = {}));
    /**
     * 이미지 데이터
     */
    var ImageData = /** @class */ (function () {
        /**
         * 생성자
         */
        function ImageData() {
            this.imageUrl = '';
            this.thumbnail = '';
            this.res = new Resolution();
        }
        return ImageData;
    }());
    exports.ImageData = ImageData;
    /**
     * 카머라 상태 정보
     */
    var CameraStatus = /** @class */ (function () {
        /**
         * 생성자
         */
        function CameraStatus() {
            this.eventType = CameraEvent.UNKNOWN;
            this.imageData = new ImageData();
            this.hasReason = false;
            this.failure = CameraFailure.CAMERA_ERROR_UNKNOWN;
            this.message = '';
        }
        return CameraStatus;
    }());
    exports.CameraStatus = CameraStatus;
    // ---------------------------------------------------
    // GALLERY
    // ---------------------------------------------------
    /**
     * 갤러리 열기 파라미터
     */
    var GalleryParam = /** @class */ (function () {
        /**
         * 생성자
         */
        function GalleryParam() {
            this.openInterval = 0;
            this.res = new Resolution();
        }
        return GalleryParam;
    }());
    exports.GalleryParam = GalleryParam;
    /**
     * 갤러리에서 발생하는 이벤트
     */
    var GalleryEvent;
    (function (GalleryEvent) {
        /**
         * 알수 없는 이벤트
         */
        GalleryEvent["UNKNOWN"] = "UNKNOWN";
        /**
         * 열기 성공
         */
        GalleryEvent["OPEN_SUCCESS"] = "OPEN_SUCCESS";
        /**
         * 열기 실패
         */
        GalleryEvent["OPEN_FAILURE"] = "OPEN_FAILURE";
        /**
         * 이미지 선택 완료
         */
        GalleryEvent["IMAGE_CAPTURED"] = "IMAGE_CAPTURED";
        /**
         * 갤러리 닫힘
         */
        GalleryEvent["CLOSED"] = "CLOSED";
    })(GalleryEvent = exports.GalleryEvent || (exports.GalleryEvent = {}));
    /**
     * 갤러리 실패 유형
     */
    var GalleryFailure;
    (function (GalleryFailure) {
        /**
         * 갤러리 모르는 에러 및 성공
         */
        GalleryFailure["GALLERY_ERROR_UNKNOWN"] = "GALLERY_ERROR_UNKNOWN";
        /**
         * 열기 실패
         */
        GalleryFailure["GALLERY_OPEN_ERROR"] = "GALLERY_OPEN_ERROR";
        /**
         * 닫기 실패
         */
        GalleryFailure["GALLERY_CLOSE_ERROR"] = "GALLERY_CLOSE_ERROR";
        /**
         * 사용자에 의한 선태 취소
         */
        GalleryFailure["GALLERY_SELECT_CANCELLED"] = "GALLERY_SELECT_CANCELLED";
        /**
         * 시간이 지나서 자동으로 닫힘
         */
        GalleryFailure["GALLERY_TIMER_EXPIRED"] = "GALLERY_TIMER_EXPIRED";
        /**
         * 장치 접근이 거부됨
         */
        GalleryFailure["GALLERY_PERMISSION_DENIED"] = "GALLERY_TIMER_EXPIRED";
    })(GalleryFailure = exports.GalleryFailure || (exports.GalleryFailure = {}));
    /**
     * 갤러리 상태 정보
     */
    var GalleryStatus = /** @class */ (function () {
        /**
         * 생성자
         */
        function GalleryStatus() {
            this.eventType = GalleryEvent.UNKNOWN;
            this.hasReason = false;
            this.failure = GalleryFailure.GALLERY_ERROR_UNKNOWN;
            this.imageData = new ImageData();
            this.message = '';
        }
        return GalleryStatus;
    }());
    exports.GalleryStatus = GalleryStatus;
    // ---------------------------------------------------
    // LOCATION
    // ---------------------------------------------------
    /**
     * 위치 정보 획득 실패 이유
     */
    var LocationFailure;
    (function (LocationFailure) {
        LocationFailure["LOCATION_UNKONWN"] = "LOCATION_UNKONWN";
        /**
         * 장치 접근이 거부됨
         */
        LocationFailure["LOCATION_PERMISSION_DENIED"] = "LOCATION_PERMISSION_DENIED";
    })(LocationFailure = exports.LocationFailure || (exports.LocationFailure = {}));
    /**
     * 위치 정보 획득 상태
     *
     */
    var LocationStatus = /** @class */ (function () {
        /**
         * 생성자
         */
        function LocationStatus() {
            this.hasReason = false;
            this.location = new proto_1.Location();
            this.failure = LocationFailure.LOCATION_UNKONWN;
        }
        return LocationStatus;
    }());
    exports.LocationStatus = LocationStatus;
    var AppleM2uWebViewClientNative = /** @class */ (function () {
        function AppleM2uWebViewClientNative() {
        }
        /**
         * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
         *  - TLS 사용여부
         *  - 서버의 주소 및 포트
         *  - Ping 주기
         *
         * @param {string} setting, MapSetting 값의 json string 버전
         * @returns {boolean}
         * @desc Js to Native
         */
        AppleM2uWebViewClientNative.prototype.setMapSetting = function (setting) {
            window.location.href = "appName://setMapSetting?type=" + setting;
            return true;
        };
        /**
         * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
         *
         * - streamid
         * - operationid
         * - Param
         * - Payload 데이터는 함께 묶어서 전송한다.
         *
         *
         *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
         *
         *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
         *    - STT 음성의 전송
         *    - 이미지 서버로 이미지 전송
         *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
         *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
         *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
         *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
         *
         * @param {string} es 서버로 보낼 이벤트 메시지, EventStream 객체의 string 버전
         * @param {string | null} opt 이미지 URL
         * @desc Js to Native
         */
        AppleM2uWebViewClientNative.prototype.sendEvent = function (es, opt) {
            var optstr = '';
            if (opt)
                optstr = '&imageUrl=' + opt;
            window.location.href = "appName://sendEvent?type=" + es + optstr;
        };
        /**
         * 마이크를 열어 달라고 하는 이벤트
         *
         * @param {string} param 개방 옵션, MicrophoneParam 객체를 json 문자열로 변환한 것
         * @desc JS to Native
         */
        AppleM2uWebViewClientNative.prototype.openMicrophone = function (param) {
            window.location.href = "appName://openMicrophone?type=" + param;
        };
        /**
         * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
         *
         * @desc JS To Native
         */
        AppleM2uWebViewClientNative.prototype.closeMicrophone = function () {
            window.location.href = "appName://closeMicrophone?";
        };
        /**
         * 명시적으로 스피커를 닫는다.
         * @desc JS To Native
         */
        AppleM2uWebViewClientNative.prototype.closeSpeaker = function () {
            window.location.href = "appName://closeSpeaker?";
        };
        /**
         * 카메라를 엽니다.
         *
         * @param {string} param 문자열로 변환된 CameraParam 객체
         * @desc JS To Native
         */
        AppleM2uWebViewClientNative.prototype.openCamera = function (param) {
            window.location.href = "appName://openCamera?type=" + param;
        };
        /**
         * 명시적으로 카메라를 닫는다.
         *
         * @desc JS To Native
         */
        AppleM2uWebViewClientNative.prototype.closeCamera = function () {
            window.location.href = "appName://closeCamera?";
        };
        /**
         * 갤러리를 오픈한다.
         * call to native
         * @param {string} param
         */
        AppleM2uWebViewClientNative.prototype.openGallery = function (param) {
            window.location.href = "appName://openGallery?type=" + param;
        };
        /**
         * 명시적으로 Gallery를 닫는다.
         */
        AppleM2uWebViewClientNative.prototype.closeGallery = function () {
            window.location.href = "appName://closeGallery?";
        };
        /**
         *
         * 매번 현재 단말의 위치를 구해와라.
         *
         * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
         * EventStream의 context 정보에 적재하여 전송한다.
         *
         * @desc JS to Native
         */
        AppleM2uWebViewClientNative.prototype.getLocation = function () {
            window.location.href = "appName://getLocation?";
        };
        /**
         *
         * Phone Number 정보는 JS에서 필요시 호출한다.
         *
         * @desc JS to Native
         */
        AppleM2uWebViewClientNative.prototype.getPhoneNumber = function () {
            window.location.href = "appName://getPhoneNumber?";
        };
        return AppleM2uWebViewClientNative;
    }());
    exports.AppleM2uWebViewClientNative = AppleM2uWebViewClientNative;
    /**
     * 디버깅 용도로 사용되는 Local 구현
     */
    var LocalM2uWebViewClientNative = /** @class */ (function () {
        function LocalM2uWebViewClientNative() {
        }
        /**
         * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
         *  - TLS 사용여부
         *  - 서버의 주소 및 포트
         *  - Ping 주기
         *
         * @param {string} setting, MapSetting 값의 json string 버전
         * @returns {boolean}
         * @desc Js to Native
         */
        LocalM2uWebViewClientNative.prototype.setMapSetting = function (setting) {
            console.log('setMapSetting', setting);
            return true;
        };
        /**
         * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
         *
         * - streamid
         * - operationid
         * - Param
         * - Payload 데이터는 함께 묶어서 전송한다.
         *
         *
         *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
         *
         *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
         *    - STT 음성의 전송
         *    - 이미지 서버로 이미지 전송
         *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
         *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
         *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
         *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
         *
         * @param {string} es 서버로 보낼 이벤트 메시지, EventStream 객체의 string 버전
         * @param {string | null} opt 이미지 URL
         * @desc Js to Native
         */
        LocalM2uWebViewClientNative.prototype.sendEvent = function (es, opt) {
            console.log('sendEvent', es, opt);
        };
        /**
         * 마이크를 열어 달라고 하는 이벤트
         *
         * @param {string} param 개방 옵션, MicrophoneParam 객체를 json 문자열로 변환한 것
         * @desc JS to Native
         */
        LocalM2uWebViewClientNative.prototype.openMicrophone = function (param) {
            console.log('openMicrophone', param);
        };
        /**
         * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
         *
         * @desc JS To Native
         */
        LocalM2uWebViewClientNative.prototype.closeMicrophone = function () {
            console.log('closeMicrophone');
        };
        /**
         * 명시적으로 스피커를 닫는다.
         * @desc JS To Native
         */
        LocalM2uWebViewClientNative.prototype.closeSpeaker = function () {
            console.log('closeSpeaker');
        };
        /**
         * 카메라를 엽니다.
         *
         * @param {string} param 문자열로 변환된 CameraParam 객체
         * @desc JS To Native
         */
        LocalM2uWebViewClientNative.prototype.openCamera = function (param) {
            console.log('openCamera', param);
        };
        /**
         * 명시적으로 카메라를 닫는다.
         *
         * @desc JS To Native
         */
        LocalM2uWebViewClientNative.prototype.closeCamera = function () {
            console.log('closeCamera');
        };
        LocalM2uWebViewClientNative.prototype.openGallery = function (param) {
            console.log('openGallery', param);
        };
        /**
         * 명시적으로 Gallery를 닫는다.
         */
        LocalM2uWebViewClientNative.prototype.closeGallery = function () {
            console.log('closeGallery');
        };
        /**
         *
         * 매번 현재 단말의 위치를 구해와라.
         *
         * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
         * EventStream의 context 정보에 적재하여 전송한다.
         *
         * @desc JS to Native
         */
        LocalM2uWebViewClientNative.prototype.getLocation = function () {
            console.log('getLocation');
        };
        /**
         *
         * Phone Number 정보는 JS에서 필요시 호출한다.
         *
         * @desc JS to Native
         */
        LocalM2uWebViewClientNative.prototype.getPhoneNumber = function () {
            console.log('getPhoneNumber');
        };
        return LocalM2uWebViewClientNative;
    }());
    exports.LocalM2uWebViewClientNative = LocalM2uWebViewClientNative;
});
define("main", ["require", "exports", "proto", "native", "uuid"], function (require, exports, proto_2, native_1, uuid_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * Native에서 MAPClient 쪽으로 보내는 메시지를 해석해서 내부적으로 이벤트를 발생시켜주는 클래스
     *
     * 이벤트의 유형에 따라서 다양한 메시지를 전달해준다.
     */
    var NativeEventForward = /** @class */ (function () {
        /**
         * NativeEventForward 생성자
         * @param {MapClient} mapClient `mapClient`로부터 받은 이벤트를 처리하도록 한다.
         */
        function NativeEventForward(mapClient) {
            this.mapClient = undefined;
            this.mapClient = mapClient;
        }
        // ---------------------------------------------------
        // MAP
        // ---------------------------------------------------
        /**
         * MAP 서버로부터 응답을 받은다.
         * - opid에 대한 싱크 관리는 Web에서 처리하도록 한다.
         *
         * @param {string} directive 응답받은 메시지 데이터
         * @desc Native to JS
         */
        NativeEventForward.prototype.receiveDirective = function (directive) {
            var obj = JSON.parse(directive);
            var dir = new proto_2.DirectiveStream();
            dir = __assign({}, dir, obj);
            var ifname = dir.interface.interface + '.' + dir.interface.operation;
            // 스트리밍 메시지를 수신한 경우에는 streaming 에 값을 넣어둡니다.
            if (dir.interface.streaming) {
                this.mapClient._streams[dir.streamId] = dir.interface;
            }
            var handler = this.mapClient.handler;
            // 핸들러가 정의되어 있지 않으면 중지 한다.
            if (!handler) {
                console.log('no handler assigned!');
                return;
            }
            // 각 인터페이스별로 분기하여 이를 클라이언트의 콜백으로 호출하도록 한다.
            switch (ifname) {
                case 'View.RenderText': {
                    var o1 = new proto_2.RenderTextPayload();
                    o1 = __assign({}, o1, dir.payload);
                    if (handler.onViewRenderText)
                        handler.onViewRenderText(o1);
                    break;
                }
                case 'View.RenderCard': {
                    var o2 = new proto_2.Card();
                    o2 = __assign({}, o2, dir.payload);
                    if (handler.onViewRenderCard)
                        handler.onViewRenderCard(o2);
                    break;
                }
                case 'View.RenderHidden': {
                    var o3 = new proto_2.Struct();
                    o3 = __assign({}, o3, dir.payload);
                    if (handler.onViewRenderHidden)
                        handler.onViewRenderHidden(o3);
                    break;
                }
                case 'SpeechRecognizer.Recognizing': {
                    var o4 = new proto_2.StreamingRecognizeResponse();
                    o4 = __assign({}, o4, dir.payload);
                    if (handler.onSpeechRecognizerRecognizing)
                        handler.onSpeechRecognizerRecognizing(o4);
                    break;
                }
                case 'SpeechRecognizer.Recognized': {
                    var o5 = new proto_2.StreamingRecognizeResponse();
                    o5 = __assign({}, o5, dir.payload);
                    if (handler.onSpeechRecognizerRecognized)
                        handler.onSpeechRecognizerRecognized(o5);
                    break;
                }
                case 'Dialog.Processing': {
                    if (handler.onDialogProcessing)
                        handler.onDialogProcessing();
                    break;
                }
                case 'SpeechSynthesizer.Play': {
                    if (handler.onSpeechSynthesizerPlayStarted)
                        handler.onSpeechSynthesizerPlayStarted();
                    break;
                }
                case 'Microphone.ExpectSpeech': {
                    var o6 = new proto_2.ExpectSpeechPayload();
                    o6 = __assign({}, o6, dir.payload);
                    if (handler.onMicrophoneExpectSpeech)
                        handler.onMicrophoneExpectSpeech(o6);
                    break;
                }
                case 'Avatar.SetExpression': {
                    var o7 = new proto_2.AvatarSetExpressionPayload();
                    o7 = __assign({}, o7, dir.payload);
                    if (handler.onAvatarSetExpression)
                        handler.onAvatarSetExpression(o7);
                    break;
                }
                case 'ImageDocument.Recognized': {
                    if (handler.onImageDocumentRecognized)
                        handler.onImageDocumentRecognized();
                    break;
                }
                case 'Authentication.SignInResult': {
                    var o8 = new proto_2.SignInResultPayload();
                    o8 = __assign({}, o8, dir.payload);
                    // 강제로 `authToken`을 지정합니다.
                    if (o8.authSuccess && o8.authSuccess.authToken)
                        this.mapClient.authToken = o8.authSuccess.authToken;
                    if (handler.onAuthenticationSignInResult)
                        handler.onAuthenticationSignInResult(o8);
                    break;
                }
                case 'Authentication.SignOutResult': {
                    var o9 = new proto_2.SignOutResultPayload();
                    o9 = __assign({}, o9, dir.payload);
                    if (handler.onAuthenticationSignOutResult)
                        handler.onAuthenticationSignOutResult(o9);
                    this.mapClient.authToken = null;
                    break;
                }
                case 'Authentication.MultiFactorVerifyResult': {
                    var p0 = new proto_2.SignInResultPayload();
                    p0 = __assign({}, p0, dir.payload);
                    // 강제로 `authToken`을 지정합니다.
                    if (p0.authSuccess && p0.authSuccess.authToken)
                        this.mapClient.authToken = p0.authSuccess.authToken;
                    if (handler.onAuthenticationMultiFactorVerifyResult)
                        handler.onAuthenticationMultiFactorVerifyResult(p0);
                    break;
                }
                case 'Authentication.UpdateUserSettingsResult': {
                    var p11 = new proto_2.UserSettings();
                    p11 = __assign({}, p11, dir.payload);
                    if (handler.onAuthenticationUpdateUserSettingsResult)
                        handler.onAuthenticationUpdateUserSettingsResult(p11);
                    break;
                }
                case 'Authentication.GetUserSettingsResult': {
                    var p12 = new proto_2.UserSettings();
                    p12 = __assign({}, p12, dir.payload);
                    if (handler.onAuthenticationGetUserSettingsResult)
                        handler.onAuthenticationGetUserSettingsResult(p12);
                    break;
                }
                case 'Launcher.Authorize': {
                    var p13 = new proto_2.LauncherAuthorizePayload();
                    var a1 = new proto_2.DialogAgentForwarderParam();
                    a1 = __assign({}, a1, dir.param.daForwardParam);
                    p13 = __assign({}, p13, dir.payload);
                    if (handler.onLauncherAuthorize)
                        handler.onLauncherAuthorize(p13, a1);
                    break;
                }
                case 'Launcher.FillSlots': {
                    var p14 = new proto_2.LauncherFillSlotsPayload();
                    var a2 = new proto_2.DialogAgentForwarderParam();
                    a2 = __assign({}, a2, dir.param.daForwardParam);
                    p14 = __assign({}, p14, dir.payload);
                    if (handler.onLauncherFillSlots)
                        handler.onLauncherFillSlots(p14, a2);
                    break;
                }
                case 'Launcher.LaunchApp': {
                    var p15 = new proto_2.LauncherLaunchAppPayload();
                    p15 = __assign({}, p15, dir.payload);
                    if (handler.onLauncherLaunchApp)
                        handler.onLauncherLaunchApp(p15);
                    break;
                }
                case 'Launcher.LaunchPage': {
                    var p16 = new proto_2.LauncherLaunchPagePayload();
                    p16 = __assign({}, p16, dir.payload);
                    if (handler.onLauncherLaunchPage)
                        handler.onLauncherLaunchPage(p16);
                    break;
                }
                case 'Launcher.ViewMap': {
                    var p17 = new proto_2.LauncherViewMapPayload();
                    p17 = __assign({}, p17, dir.payload);
                    if (handler.onLauncherViewMap)
                        handler.onLauncherViewMap(p17);
                    break;
                }
            }
            if (this.mapClient._debug && this.mapClient._debug.onDirective)
                this.mapClient._debug.onDirective(JSON.stringify(dir, replacer, 2));
        };
        /**
         * map 통신 중에 수신한 exception
         *
         * mapdirective의 내용을 json 형태로 전송한다.
         *
         * @param {string} exception MapException MAP 통신 중에서 Exception이 발생한 경우
         * @desc Native to JS
         */
        NativeEventForward.prototype.receiveException = function (exception) {
            var obj = JSON.parse(exception);
            var mape = new proto_2.MapException();
            mape = __assign({}, mape, obj);
            var handler = this.mapClient.handler;
            if (handler && handler.onException)
                handler.onException(mape);
        };
        /**
         * 해당 `OPERATION`이 완료되면, 발생시킨다.
         *
         * Java의 경우, `OnComplete()` 호출의 결과 전송하도록 한다.
         *
         * @param {string} opid operation id
         */
        NativeEventForward.prototype.receiveCompleted = function (opid) {
            this.mapClient.clearOperationId(opid);
            var handler = this.mapClient.handler;
            if (handler && handler.onOperationCompleted)
                handler.onOperationCompleted(opid);
        };
        /**
         * 에러를 수신한다.
         * grpc Error를 수신한 경우에 이를 담아서 보내준다
         *
         * @param {string} status GrpcStatus GRPC 에러 생태 값 및 상세 정보
         * @desc Native to JS
         */
        NativeEventForward.prototype.receiveError = function (status) {
            var obj = JSON.parse(status);
            var s = new native_1.GrpcStatus();
            s = __assign({}, s, obj);
            var handler = this.mapClient.handler;
            if (handler && handler.onError)
                handler.onError(s);
        };
        /**
         * SendEvent에 딸린 스트림을 전송하는 경우에는 Web에 알려준다.
         * 스트림을 전송하고 있는 동안에 화면에 정보를 표시할 수 있도록 처리한다.
         *
         * 스트림의 끝, 스트림 중지의 경우에도 보내준다
         *
         * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
         *
         * @param {string} st StreamEventStatus 타입
         * @desc Native To JS
         */
        NativeEventForward.prototype.sendStreamingEvent = function (st) {
            var obj = JSON.parse(status);
            var s = new native_1.StreamingStatus();
            s = __assign({}, s, obj);
            var aif = this.mapClient.getAsyncInterface(s.streamId);
            if (!aif) {
                console.error('cannot find async interface for streamEvent', JSON.stringify(s));
            }
            else {
                s = __assign({}, s, { interface: aif });
                var ifname = aif.interface + '.' + aif.operation;
                switch (ifname) {
                    case 'Dialog.SpeechToSpeechTalk': {
                        console.log(ifname, s);
                        break;
                    }
                    case 'Dialog.SpeechToTextTalk': {
                        console.log(ifname, s);
                        break;
                    }
                    case 'Dialog.ImageToSpeechTalk': {
                        console.log(ifname, s);
                        break;
                    }
                    case 'Dialog.ImageToTextTalk': {
                        console.log(ifname, s);
                        break;
                    }
                }
                // 스트림이 끝났으면 이를 버립니다.
                if (s.isEnd && s.isBreak) {
                    this.mapClient.clearStreamId(s.streamId);
                }
                var handler = this.mapClient.handler;
                if (handler && handler.onStreamingEvent) {
                    handler.onStreamingEvent(s);
                }
            }
        };
        /**
         * receiveDirective에 딸린 스트림을 수신하는 경우에는 Web에 알려준다.
         * 이것은 TTS 출력 중인 상태와 같이 스트림을 수신하고 있는 동아네
         * 화면에 정보를 표시할 수 있도록 처리한다.
         *
         * 스트림의 끝, 스트림 중지의 경우에도 보내준다
         *
         * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
         *
         * @param {string} st StreamEventStatus
         * @desc Native To JS
         */
        NativeEventForward.prototype.receiveStreamingDirective = function (st) {
            var obj = JSON.parse(status);
            var s = new native_1.StreamingStatus();
            s = __assign({}, s, obj);
            var aif = this.mapClient.getAsyncInterface(s.streamId);
            if (!aif) {
                console.error('cannot find async interface for streamEvent', JSON.stringify(s));
            }
            else {
                s = __assign({}, s, { interface: aif });
                var ifname = aif.interface + '.' + aif.operation;
                switch (ifname) {
                    case 'SpeechSynthesizer.Play': {
                        console.log(ifname, s);
                        break;
                    }
                }
                // 스트림이 끝났으면 이를 버립니다.
                if (s.isEnd && s.isBreak) {
                    this.mapClient.clearStreamId(s.streamId);
                }
                var handler = this.mapClient.handler;
                if (handler && handler.onStreamingEvent) {
                    handler.onStreamingEvent(s);
                }
            }
        };
        /**
         * Ping의 결과가 있으면 이를 반환한다.
         *
         * iOS의 경우에 백그라운드 앱은 네트워크를 사용할 수 없으므로 깨어나면 바로 자동으로 시작하도록 한다.
         *
         * setMapSetting() 에 의해서 pingEnabled = false로 주어지면, ping은 내부적으로 중지될 수 있다.
         *
         * @param {string} pong  PongResponse 수신받든 Pong 객체
         * @desc Native To JS
         */
        NativeEventForward.prototype.notifyPong = function (pong) {
            console.log(pong);
        };
        // ---------------------------------------------------
        // MIC
        // ---------------------------------------------------
        /**
         * 마이크 상태를 반환한다.
         *
         * 1. 마이크 열림 성공, 또는 실패
         * 2. 최초 음성 레코딩이 시작될 때 알림
         * 3. 마이크 닫는 호출에 대한 응답을 반환한다.
         *
         * @param {string } st MicrophoneStatus type
         * @desc Native To JS
         */
        NativeEventForward.prototype.notifyMicrophoneStatus = function (st) {
            var obj = JSON.parse(st);
            var s = new native_1.MicrophoneStatus();
            s = __assign({}, s, obj);
            var handler = this.mapClient.handler;
            if (handler && handler.onMicrophoneStatus) {
                handler.onMicrophoneStatus(s);
            }
        };
        // ---------------------------------------------------
        // SPEAKER
        // ---------------------------------------------------
        /**
         * 마이크 개방 후 상태를 알려줍니다.
         *
         * @param {string } ss SpeakerStatus 마이크 개방 상태
         * @desc Native To JS
         */
        NativeEventForward.prototype.notifySpeakerStatus = function (ss) {
            var obj = JSON.parse(ss);
            var s = new native_1.SpeakerStatus();
            s = __assign({}, s, obj);
            var handler = this.mapClient.handler;
            if (handler && handler.onSpeakerStatus) {
                handler.onSpeakerStatus(s);
            }
        };
        // ---------------------------------------------------
        // CAMERA
        // ---------------------------------------------------
        /**
         * 성공, 실패, 데이터 수집 등 카메라 상태를 반환한다.
         *
         * @param {string} cs CameraStatus type 카메라 상태
         * @desc Native to JS
         */
        NativeEventForward.prototype.notifyCameraStatus = function (cs) {
            var obj = JSON.parse(cs);
            var s = new native_1.CameraStatus();
            s = __assign({}, s, obj);
            var handler = this.mapClient.handler;
            if (handler && handler.onCameraStatus) {
                handler.onCameraStatus(s);
            }
        };
        // ---------------------------------------------------
        // GALLERY
        // ---------------------------------------------------
        /**
         * 갤러리 상태를 반환한다.
         *
         * @param {string} st GalleryStatus type
         */
        NativeEventForward.prototype.notifyGalleryStatus = function (st) {
            var obj = JSON.parse(st);
            var s = new native_1.GalleryStatus();
            s = __assign({}, s, obj);
            var handler = this.mapClient.handler;
            if (handler && handler.onGalleryStatus) {
                handler.onGalleryStatus(s);
            }
        };
        // ---------------------------------------------------
        // LOCATION
        // ---------------------------------------------------
        /**
         * 위치정보가 구해지면 이를 저장합니다.
         *
         * @param {LocationStatus} st 구해진 위치 정보를 반환한다.
         */
        NativeEventForward.prototype.notifyLocation = function (st) {
            var obj = JSON.parse(st);
            var s = new native_1.LocationStatus();
            s = __assign({}, s, obj);
            var handler = this.mapClient.handler;
            if (!s.hasReason) {
                if (this.mapClient.location == null) {
                    // 최초 위치 정보를 얻어온 경우 예외처리
                    this.mapClient.location = s.location;
                }
                else {
                    // 이후 위치 정보값이 변경된 경우에만 값을 대입
                    if (this.mapClient.location.longitude !== s.location.longitude
                        || this.mapClient.location.latitude !== s.location.latitude) {
                        this.mapClient.location = s.location;
                    }
                }
                // 위치 정보를 onLocationChanged 리스너로 전달
                if (handler && handler.onLocationChanged) {
                    handler.onLocationChanged(s);
                }
            }
            else {
                if (handler && handler.onLocationChanged) {
                    handler.onLocationChanged(s);
                }
            }
        };
        return NativeEventForward;
    }());
    exports.NativeEventForward = NativeEventForward;
    /**
     * 랜던 문자열을 생성한다.
     *
     * @param {number} length 길이
     * @param {string} chars 쓰일 문자열
     * @return {string} 생성된 문자열
     */
    function randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i)
            result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }
    /**
     * CDK main class
     *
     * 이 클래스를 통해서 모든 함수 호출이 처리된다.
     */
    var MapClient = /** @class */ (function () {
        /**
         * `MapClient`로 새로 만듭니다.
         *
         * @param {MapSetting} settings Map 서버 접속 주소 등의 정보를 가지고 있습니다.
         * @param {string} deviceSuffix 호출을 구분하는 OPERATION ID를 구분하는 정보
         */
        function MapClient(settings, deviceSuffix) {
            /**
             * Native로 보낸 EventStream이 streaming 형식일 때 기억하기 위한 변수
             * native로 보내고 난후, `notifyEventStream` 호출을 통해서 정보가 오는데, 이때는 streamId 정보만 온다.
             * 어떤 event에 대한 정보인지를 알수가 없으므로 이를 기억한다.
             *
             * @type {Object}
             * @private
             */
            this._streams = {};
            /**
             * Native로 보낸 EventStream에 대해서 기억하는 변수
             * 각각 이벤트에 대한 응답이 오면 지운다.
             * @type {Object}
             * @private
             */
            this._operations = undefined;
            /**
             * 실지로 native로 메시지를 전송하는 객체
             * @type {M2uWebViewClientNativeMobile}
             */
            this.native = null;
            /**
             * native의 이벤트를 받아들여서 이를 local WEB javascript callback 함수를 호출해준다.
             * @type {NativeEventForward}
             */
            this.forwader = null;
            /**
             * 현재 위치를 기억하고 있습니다.
             * @type {Location}
             * @private
             */
            this._location = null;
            /**
             * 등록된 콜백 함수들
             * @type {MapDirectiveHandler}
             * @private
             */
            this._handler = undefined;
            /**
             * 등록된 디버깅 함수들
             * @type {MapClientDebugger}
             * @private
             */
            this._debug = undefined;
            this.deviceSuffix = '0000000000';
            // ANDROID
            if (window.m2uWebViewNative) {
                this.native = window.m2uWebViewNative;
            }
            // IOS
            else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                this.native = new native_1.AppleM2uWebViewClientNative();
            }
            // 아무것도 아니면...
            else {
                this.native = new native_1.LocalM2uWebViewClientNative();
            }
            this.deviceSuffix = deviceSuffix.substr(0, MapClient.suffixLength);
            // operations들을 관리하는 map
            this._operations = {};
            //
            // 새로운 Forwader를 설치한다.
            //
            this.forwader = new NativeEventForward(this);
            // 늘 문자로 시작하도록 하고 길이는 20자로 한다.
            this._forwarderName =
                randomString(5, 'abcdefghijklmnopqrstuvwxyz') +
                    randomString(15, '1234567890abcdefghijklmnopqrstuvwxyz');
            if (window.m2uNativeForward === undefined)
                window.m2uNativeForward = {};
            window.m2uNativeForward[this._forwarderName] = this.forwader;
            if (window.mySecret === undefined)
                window.mySecret = {};
            window.mySecret = this._forwarderName;
            this._settings = settings;
        }
        Object.defineProperty(MapClient.prototype, "settings", {
            /**
             * 설정을 가져온다.
             * @returns {MapSetting}
             */
            get: function () {
                return this._settings;
            },
            /**
             * 설정을 바꾼다.
             * @param {MapSetting} value
             */
            set: function (value) {
                this._settings = value;
                this._settings.notifyObjectPrefix = 'window.m2uNativeForward.' + this._forwarderName;
                this.setMapSetting(this._settings);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MapClient.prototype, "device", {
            /**
             * 현재 디바이스 상태를 반환한다.
             *
             * @returns {Device} 현재 디바이스 상태
             */
            get: function () {
                return this._settings.device;
            },
            /**
             * 디바디스 정보를 설정한다.
             * @param {Device} value 변경할 디바이스 정보
             */
            set: function (value) {
                this._settings.device = value;
                this.setMapSetting(this._settings);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MapClient.prototype, "authToken", {
            /**
             * 인증 토큰을 반환한다.
             *
             * @returns {string}
             */
            get: function () {
                return this._settings.authToken;
            },
            /**
             * 인증 토큰을 변경한다.
             * @param {string} value
             */
            set: function (value) {
                this._settings.setAuthToken(value);
                this.setMapSetting(this._settings);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MapClient.prototype, "location", {
            /**
             * 현재 위치
             * @returns {Location}
             */
            get: function () {
                return this._location;
            },
            /**
             * 로케이션 지정
             * @param {Location} value
             */
            set: function (value) {
                this._location = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MapClient.prototype, "handler", {
            /**
             * 현재 설정된 핸들러 반환
             * @returns {MapDirectiveHandler}
             */
            get: function () {
                return this._handler;
            },
            /**
             * 핸들러 지정
             * @param {MapDirectiveHandler} handler
             */
            set: function (handler) {
                this._handler = handler;
            },
            enumerable: true,
            configurable: true
        });
        //
        // 인증에 대한 호출
        //
        /**
         * 인증을 호출합니다.
         *
         * @param {SignInPayload} payload 인증 처리 요청 데이터
         */
        MapClient.prototype.signIn = function (payload) {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Authentication',
                operation: 'SignIn'
            });
            // 인증 시점에는 반드시 이 토큰으로 사용해야 합니다.
            //this.authToken = '1602a950-e651-11e1-84be-00145e76c700';
            this.authToken = '';
            // 페이로드 지정
            if (payload) {
                // 사용자의 device 정보를 업데이트 합니다.
                payload.device = this.settings.device;
                es.payload = __assign({}, es.payload, payload);
            }
            console.log(JSON.stringify(es));
            this.sendEvent(es);
        };
        /**
         * SignOut을 호출합니다.
         * 내부에 들고 있는 authToken 정리합니다.
         * @returns {string} operation id
         */
        MapClient.prototype.signOut = function () {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Authentication',
                operation: 'SignOut'
            });
            // 현재 authToken 값을 전달하여 SignOut을 진행합니다
            // TODO CDK에서 사용자 키 (optional)값은 전달해야 할까요?
            var payload = new proto_2.SignOutPayload(this.authToken);
            es.payload = __assign({}, es.payload, payload);
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * 다중 팩터 인증을 처리합니다.
         *
         * @param {MultiFactorVerifyPayload} payload 다중 팩터 인증을 위한 처리 데이터
         * @returns {string} operation id
         */
        MapClient.prototype.multiFactorVerify = function (payload) {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Authentication',
                operation: 'MultiFactorVerify'
            });
            // 페이로드 지정
            if (payload)
                es.payload = __assign({}, es.payload, payload);
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * 사용자 속성 정의
         * 인증된 사용자만 이 호출을 진행할 수 있다.
         * 바꾸고 싶은 값만 넣을 수 있다.
         * 응답으로는 모든 설정정보를 다 반환한다.
         *
         * @param {UserSettings} userSettings 사용자 속성 정의 데이터
         * @returns {string}
         * @constructor
         */
        MapClient.prototype.updateUserSettings = function (payload) {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Authentication',
                operation: 'UpdateUserSettings'
            });
            // payload 지정
            if (payload)
                es.payload = __assign({}, es.payload, payload);
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * 사용자 속성 조회
         * 인증된 사용자만 이 호출을 진행할 수 있다.
         * 설정정보를 조회한다.
         *
         * @param {UserKey} userKey 사용자 속성 조회를 위한 키
         * @returns {string}
         * @constructor
         */
        MapClient.prototype.getUserSettings = function (payload) {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Authentication',
                operation: 'GetUserSettings'
            });
            // payload 지정
            if (payload)
                es.payload = __assign({}, es.payload, payload);
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        //
        // 대화에 대한 호출
        //
        /**
         * 새로운 세션을 엽니다.
         *
         * M2U 시스템은 세션이 열리자 마자 새로운 발화를 내보내개 됩니다.
         *
         * @param {OpenUtter} openUtter
         *   `openUtter.chatbot`이 비어있으면 Device.
         *   `openUtter.utter`는 기본값이 비어있습니다.
         *   `openUtter.meta`에는 원하는 추가적인 정보를 넣어줄 수도 있습니다.
         * @returns {string} operation id
         */
        MapClient.prototype.open = function (openUtter) {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Dialog',
                operation: 'Open'
            });
            // OpenUtter 값을 지정합니다.
            if (openUtter)
                es.payload = __assign({}, es.payload, openUtter);
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * 음성으로 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * - SpeechRecognizer.Recognizing
         * - SpeechRecognizer.Recognized
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         * - Microphone.ExpectSpeech
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
         * @returns {string} operation id
         */
        MapClient.prototype.speechToSpeechTalk = function (param) {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Dialog',
                operation: 'SpeechToSpeechTalk',
                streaming: true
            });
            // param
            if (!param) {
                var srParam = new proto_2.SpeechRecognitionParam();
                srParam.lang = proto_2.Lang.ko_KR;
                srParam.model = 'baseline';
                srParam.encoding = proto_2.AudioEncoding.LINEAR16;
                srParam.sampleRate = 16000;
                srParam.singleUtterance = true;
                es.param._speechRecognitionParam = srParam;
            }
            else {
                es.param._speechRecognitionParam = param;
            }
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * 음성으로 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * - SpeechRecognizer.Recognizing
         * - SpeechRecognizer.Recognized
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
         * @returns {string} operation id
         */
        MapClient.prototype.speechToTextTalk = function (param) {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Dialog',
                operation: 'SpeechToTextTalk',
                streaming: true
            });
            // param
            if (!param) {
                var srParam = new proto_2.SpeechRecognitionParam();
                srParam.lang = proto_2.Lang.ko_KR;
                srParam.model = 'baseline';
                srParam.encoding = proto_2.AudioEncoding.LINEAR16;
                srParam.sampleRate = 16000;
                srParam.singleUtterance = true;
                es.param._speechRecognitionParam = srParam;
            }
            else {
                es.param._speechRecognitionParam = param;
            }
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * 텍스트로 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {Utter} utter 이 값은 필수입니다.
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        MapClient.prototype.textToTextTalk = function (utter) {
            if (utter === null)
                throw new Error('invalid utter');
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Dialog',
                operation: 'TextToTextTalk'
            });
            // OpenUtter 값을 지정합니다.
            if (utter)
                es.payload = __assign({}, es.payload, utter);
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * Client가 명시적으로 interface, operation 명을 지정할 수 있는 디렉티브 생성 함수
         *
         * 예1) MAP으로 부터 Launcher.Authorize 를 수신 받았다면
         * Client는 interface를 Launcher, operation은 Authorized로 설정하여 해당 함수를 호출한다
         * 또는 interface를 Launcher, operation은 AuthorizeFailed로 설정하여 해당 함수를 호출한다
         *
         * 예2) MAP으로 부터 Launcher.FillSlots 를 수신 받았다면
         * Client는 interface를 Launcher, operation은 SlotstFilled로 설정하여 해당 함수를 호출한다
         *
         * @param {string} interfaceName
         * @param {string} operationName
         * @param {EventPayload} eventPayload
         * @param {DialogAgentForwarderParam} param dialog agent forwarder param
         * @returns {string}
         */
        MapClient.prototype.event = function (interfaceName, operationName, eventPayload, param) {
            if (eventPayload === null) {
                throw new Error('invalid eventPayload');
            }
            if (param === null) {
                throw new Error('invalid param');
            }
            var evParam = new proto_2.EventParam();
            evParam._dialogAgentForwarderParam = param;
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: interfaceName,
                operation: operationName,
            });
            if (eventPayload) {
                es.payload = __assign({}, es.payload, eventPayload);
            }
            es.param = evParam;
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * 텍스트로 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {Utter} utter 이 값은 필수입니다.
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        MapClient.prototype.textToSpeechTalk = function (utter) {
            if (utter === null)
                throw new Error('invalid utter');
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Dialog',
                operation: 'TextToSpeechTalk'
            });
            // OpenUtter 값을 지정합니다.
            if (utter)
                es.payload = __assign({}, es.payload, utter);
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        /**
         * 이미지를 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - ImageDocument.Recognized
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
         *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
         * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
      
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        MapClient.prototype.imageToTextTalk = function (imageUrl, param) {
            if (imageUrl === null)
                throw new Error('invalid utter');
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Dialog',
                operation: 'ImageToTextTalk',
                streaming: true
            });
            // param
            if (!param) {
                es.param._imageRecognitionParam = new proto_2.ImageRecognitionParam({
                    lang: proto_2.Lang.ko_KR,
                    imageFormat: proto_2.ImageFormat.JPEG,
                    width: 1024,
                    height: 768,
                    refVertexX: 0.104167,
                    refVertexY: 0.138889,
                    symmCrop: true
                });
            }
            else {
                es.param._imageRecognitionParam = param;
            }
            console.log(JSON.stringify(es));
            this.sendEvent(es, imageUrl);
            return es.operationSyncId;
        };
        /**
         * 이미지를 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - ImageDocument.Recognized
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         * - Microphone.ExpectSpeech
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
         *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
         * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
      
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        MapClient.prototype.imageToSpeechTalk = function (imageUrl, param) {
            if (imageUrl === null)
                throw new Error('invalid utter');
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Dialog',
                operation: 'ImageToSpeechTalk',
                streaming: true
            });
            // param
            if (!param) {
                es.param._imageRecognitionParam = new proto_2.ImageRecognitionParam({
                    lang: proto_2.Lang.ko_KR,
                    imageFormat: proto_2.ImageFormat.JPEG,
                    width: 1024,
                    height: 768,
                    refVertexX: 0.104167,
                    refVertexY: 0.138889,
                    symmCrop: true
                });
            }
            else {
                es.param._imageRecognitionParam = param;
            }
            console.log(JSON.stringify(es));
            this.sendEvent(es, imageUrl);
            return es.operationSyncId;
        };
        /**
         * 명시적으로 세션을 닫습니다.
         * @returns {string} operation id
         */
        MapClient.prototype.close = function () {
            var es = new proto_2.EventStream();
            es.interface = __assign({}, es.interface, {
                interface: 'Dialog',
                operation: 'Close'
            });
            console.log(JSON.stringify(es));
            this.sendEvent(es);
            return es.operationSyncId;
        };
        //
        // CALL NATIVE DEVICES
        //
        /**
         * map setting 정보를 넣어준다.
         *
         * @param {MapSetting} setting 변경할 map setting 정보
         * @returns {boolean} 새로운 map setting을 넣어준다.
         */
        MapClient.prototype.setMapSetting = function (setting) {
            // 혹시라도 누락되면 꼬이므로 갱신해준다.
            setting.notifyObjectPrefix = 'window.m2uNativeForward.' + this._forwarderName;
            var temp = JSON.stringify(setting);
            return this.native.setMapSetting(temp);
        };
        /**
         * 스트립 ID를 정리합니다.
         */
        MapClient.prototype.clearStreamId = function (strId) {
            console.log('clearing opid ', strId);
            delete this._streams[strId];
        };
        /**
         * operation에 대한 정보 삭제
         * @param {string} opid operationId
         */
        MapClient.prototype.clearOperationId = function (opid) {
            console.log('clearing opid ', opid);
            delete this._operations[opid];
        };
        /**
         * operation에 대한 호출 인터페이스 정보를 가져옵니다.
         * @param {string} streamId 인터페이스 ID를 streamID입니다.
         * @return {AsyncInterface}
         */
        MapClient.prototype.getAsyncInterface = function (streamId) {
            return this._streams[streamId];
        };
        /**
         * 내부적으로 이벤트를 전송합니다.
         *
         * 이 메소드는 직접 호출할 수도 있지만, 다른 wrapper 함수를 통해서 호출하는 것이
         * 바람직합니다.
         *
         * 매 전송시마다 새로운 streamId, operationSyncId를 생성해서 전달합니다.
         * `operationSyncId`는 UUID + deviceSuffix를 사용하여 전달합니다.
         *
         * @param {EventStream} es 전송할 이벤트
         * @param {string} imageUrl 옵셔널 이미지 URL
         */
        MapClient.prototype.sendEvent = function (es, imageUrl) {
            // uuid
            es.streamId = uuid_2.UUID.generate();
            es.operationSyncId = uuid_2.UUID.generateAsBase64().substr(0, 22) +
                this.deviceSuffix;
            // 보낸 메시지 ..
            this._operations[es.operationSyncId] = es.interface;
            if (es.interface.streaming) {
                this._streams[es.streamId] = es.interface;
            }
            if (this._location) {
                var ec = new proto_2.EventContext();
                ec.setLocation(this._location);
                es.contexts.push(ec);
            }
            var temp = JSON.stringify(es);
            this.native.sendEvent(temp, imageUrl);
            if (this._debug && this._debug.onEvent)
                this._debug.onEvent(JSON.stringify(es, replacer, 2));
        };
        /**
         * 마이크를 열어 달라고 하는 이벤트
         *
         * @param {MicrophoneParam} param 개방 옵션
         * @desc JS to Native
         */
        MapClient.prototype.openMicrophone = function (param) {
            var temp = JSON.stringify(param);
            this.native.openMicrophone(temp);
        };
        /**
         * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
         *
         * @desc JS To Native
         */
        MapClient.prototype.closeMicrophone = function () {
            this.native.closeMicrophone();
        };
        /**
         * 명시적으로 스피커를 닫는다.
         * @desc JS To Native
         */
        MapClient.prototype.closeSpeaker = function () {
            this.native.closeSpeaker();
        };
        /**
         * 카메라를 엽니다.
         *
         * @param {CameraParam} param
         * @desc JS To Native
         */
        MapClient.prototype.openCamera = function (param) {
            var temp = JSON.stringify(param);
            this.native.openCamera(temp);
        };
        /**
         * 명시적으로 카메라를 닫는다.
         *
         * @desc JS To Native
         */
        MapClient.prototype.closeCamera = function () {
            this.native.closeCamera();
        };
        /**
         * 갤러리를 오픈한다.
         *
         * @param {GalleryParam} param
         */
        MapClient.prototype.openGallery = function (param) {
            var temp = JSON.stringify(param);
            this.native.openGallery(temp);
        };
        /**
         * 명시적으로 Gallery를 닫는다.
         */
        MapClient.prototype.closeGallery = function () {
            this.native.closeGallery();
        };
        /**
         *
         * 매번 현재 단말의 위치를 구해와라.
         *
         * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
         * EventStream의 context 정보에 적재하여 전송한다.
         *
         * @desc JS to Native
         */
        MapClient.prototype.getLocation = function () {
            this.native.getLocation();
        };
        /**
         *
         * Phone Number 정보는 JS에서 필요시 호출한다.
         *
         * @desc JS to Native
         */
        MapClient.prototype.getPhoneNumber = function () {
            console.log('call getPhoneNumber');
            var tempPhoneNum = this.native.getPhoneNumber();
            console.log('tempPhoneNum :' + tempPhoneNum);
            return tempPhoneNum;
        };
        // 8바이트 추가적인 문자열의 길이
        MapClient.suffixLength = 10;
        return MapClient;
    }());
    exports.MapClient = MapClient;
    var seen = [];
    /**
     * JSON.stringify() circular 참조를 호출할 때 오동작을 제거하는 함수
     */
    function clearReplacer() {
        seen = [];
    }
    exports.clearReplacer = clearReplacer;
    /**
     * JSON.stringify()를 호출할 때, circular 참조를 무시하도록 하는 함수
     *
     * @param key 키
     * @param value 값
     * @return {any} 응답 데이터
     */
    function replacer(key, value) {
        if (value != null && typeof value == "object") {
            if (seen.indexOf(value) >= 0) {
                return;
            }
            seen.push(value);
        }
        return value;
    }
    exports.replacer = replacer;
});
//# sourceMappingURL=cdk.js.map