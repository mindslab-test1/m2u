# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-verbose
-keepattributes SourceFile,LineNumberTable
-printusage unused.txt
-dontoptimize
-dontshrink
-keepattributes *Annotation*, EnclosingMethod
-keepattributes Signature
-keepattributes InnerClasses

-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-dontwarn com.google.**
-keep class com.google.** { *; }
-keep interface com.google.** { *; }

-dontwarn io.grpc.**
-keep class io.grpc.** { *; }
-keep interface io.grpc.** { *; }

-dontwarn ai.maum.m2u.cdk.constants.**
-keep class ai.maum.m2u.cdk.constants.** { *; }
-keep interface ai.maum.m2u.cdk.constants.** { *; }

-dontwarn ai.maum.m2u.cdk.ui.dto.**
-keep class ai.maum.m2u.cdk.ui.dto.** { *; }
-keep interface ai.maum.m2u.cdk.ui.dto.** { *; }

-dontwarn com.mindslab.grpclib.utils.**
-keep class com.mindslab.grpclib.utils.** { *; }
-keep interface com.mindslab.grpclib.utils.** { *; }

-dontwarn com.mindslab.grpclib.chatbot.GrpcClient
-keep class com.mindslab.grpclib.chatbot.GrpcClient { *; }
-keep interface com.mindslab.grpclib.chatbot.GrpcClient { *; }

-dontwarn com.mindslab.grpclib.constants.**
-keep class com.mindslab.grpclib.constants.** { *; }
-keep interface com.mindslab.grpclib.constants.** { *; }

-dontwarn com.mindslab.grpclib.chatbot.common.**
-keep class com.mindslab.grpclib.chatbot.common.** { *; }
-keep interface com.mindslab.grpclib.chatbot.common.** { *; }

-dontwarn com.mindslab.grpclib.chatbot.map.**
-keep class com.mindslab.grpclib.chatbot.map.** { *; }
-keep interface com.mindslab.grpclib.chatbot.map.** { *; }

-dontwarn maum.m2u.**
-keep class maum.m2u.** { *; }
-keep interface maum.m2u.** { *; }

-dontwarn okhttp3.**
-dontwarn okio.**
