import {
  AsyncInterface,
  Device,
  DirectiveStream,
  EventStream,
  Location,
  MapException,
  PongResponse,
  SpeechRecognitionParam
} from "./proto";


/**
 * MapSetting은 초기 데이터로 NATIVE INTERFACE를 생성하는 역할을 한다.
 * 이 설정은 주기적으로 새롭게 정의할 수 있다.
 */
export class MapSetting {
  /**
   * 서버 주소
   */
  serverIp: string;
  /**
   * 서버 포트
   */
  serverPort: number;
  /**
   * TLS 사용 여부
   */
  useTls: boolean;

  /**
   * notify를 호출할 때 사용할 prefix
   * 만일, prefix가 "webviewMapClient" 라면
   * "webviewMapClient.receiveDirective( + string  + )"
   * 형식으로 호출해주시면 됩니다.
   */
  notifyObjectPrefix: string;

  /**
   * 주기적인 PING 전송할지 여부
   */
  pingInterval: number;

  /**
   * 인증 토큰
   */
  authToken: string;

  /**
   * 기본 단말 정보
   * 단말 정보가 갱신되면 계속해서 MapSetting을 통해서
   * 추가적인 정보를 저장하도록 한다.
   * location은 제외..
   */
  device: Device;

  /**
   * 생성자
   * @param {string} serverIp 서버 주소
   * @param {number} serverPort 서버 포트
   * @param {boolean} useTls TLS 사용 여부
   */
  constructor(serverIp: string,
              serverPort: number,
              useTls: boolean) {
    this.serverIp = serverIp;
    this.serverPort = serverPort;
    this.useTls = useTls;
    this.authToken = '';
    this.device = new Device();
  }

  /**
   * 인증 토콘을 설정한다.
   * @param {string} token
   */
  public setAuthToken(token: string): void {
    this.authToken = token;
  }

  /**
   * 핑 주기를 설정한다.
   * @param {number} interval
   */
  public setPingInterval(interval: number): void {
    this.pingInterval = interval;
  }
}


// ---------------------------------------------------
// MAP
// ---------------------------------------------------

/**
 * MAP과 통신할 때 발생할 수 있는 에러 메시지
 */
export class GrpcStatus {
  /**
   * 에러코드
   */
  errorCode: number;
  /**
   * 에러 메시지
   */
  errorMessage: string;
  // 바이너리
  /**
   * 에러 상세 정보
   */
  errorDetail: string;
  /**
   * 바이너리를 복원하면, object, json
   *
   */
  errorObject: Object;

  constructor() {
    this.errorCode = 0;
    this.errorMessage = undefined;
    this.errorDetail = undefined;
    this.errorObject = undefined;
  }
}

/**
 * 스트리밍 유형
 *
 * map에 스트리밍 형태로 데이터를 전송할 때
 * 계속되는 데이터의 유형, 크게 3가지가 있다.
 */
export enum StreamingType {
  /**
   * 타임 없음
   */
  UNKONWN = "UNKONWN",
  /**
   * 바이트 형태
   */
  BYTES = "BYTES",
  /**
   * 텍스트 형태
   */
  TEXT = "TEXT",
  /**
   * 사용자 정의 데이터 형태
   */
  STRUCT = "STRUCT"
}

/**
 * 스트리밍 전송 상태를 알려주는 데이터 형식
 */
export class StreamingStatus {
  /**
   * 스트림 ID
   */
  streamId: string;
  /**
   * 응답 동기화 ID
   */
  operationSyncId: string;
  /**
   * 이벤트인가 디렉티브인가
   */
  isEvent: boolean;
  /**
   * 이벤트가 강제로 중지되었는가?
   */
  isBreak: boolean;
  /**
   * 이벤트의 끝인가?
   */
  isEnd: boolean;
  /**
   * 스트리밍 데이터의 구체적인 형식
   */
  type: StreamingType;
  /**
   * 스트리밍의 크기
   */
  size: number;
  /**
   * 호출한 인터페이스의 이름
   */
  interface: AsyncInterface;

  /**
   * 생성자
   */
  constructor() {
    this.streamId = '';
    this.operationSyncId = '';
    this.isBreak = false;
    this.isEvent = false;
    this.isEnd = false;
    this.type = StreamingType.UNKONWN;
    this.size = 0;
    this.interface = undefined;
  }
}


// ---------------------------------------------------
// MIC
// ---------------------------------------------------

/**
 * 마이크 개방 파라미터
 */
export class MicrophoneParam {
  /**
   * 아이크 대기 모드인가?
   */
  expectMode: boolean;
  /**
   * Microphone ON 유지 시간 (milli sec)
   * 밀리시간 단위로 내보내는 대기 시간
   * recognizing 이면 timer restart
   * default: 10,000
   */
  timoeoutInMilliseconds: number;
  /**
   * STT 인식을 위한 파라미터
   */
  speechParam: SpeechRecognitionParam;

  /**
   * 마이크에서 얻어낼 값
   */
  miceGain: number;

  /**
   * 생성자
   */
  constructor() {
    this.expectMode = false;
    this.timoeoutInMilliseconds = 0;
    this.speechParam = new SpeechRecognitionParam();
    this.miceGain = 0.5;
  }
}

/**
 * 마이크 상태 이벤트
 */
export enum MicrophoneEvent {
  /**
   * UNKNOWN
   */
  UNKNOWN = "UNKNOWN",
  /**
   * 마이크 열기 성공
   */
  OPEN_SUCCESS = "OPEN_SUCCESS",
  /**
   * 마이크 열기 실패
   */
  OPEN_FAILURE = "OPEN_FAILURE",
  /**
   * 마이크 데이터 캡춰 시작
   */
  STARTING_RECORD = "STARTING_RECORD",
  /**
   * 닫기 성공
   */
  CLOSE_SUCCESS = "CLOSE_SUCCESS",
  /**
   * 닫기 실패
   */
  CLOSE_FAILURE = "CLOSE_FAILURE"
}

/**
 * 마이크 실패에 대한 상세 유형
 */
export enum MicrophoneFailure {
  /**
   * 열기 실패 알수 없는 실패 또는 성공
   */
  MICROPHONE_ERROR_UNKNOWN = "MICROPHONE_ERROR_UNKNOWN",
  /**
   * 열기 에러
   */
  MICROPHONE_OPEN_ERROR = "MICROPHONE_OPEN_ERROR", /* open failure */
  /**
   * 닫기 에러
   */
  MICROPHONE_CLOSE_ERROR = "MICROPHONE_CLOSE_ERROR", /* close failure */
  /**
   * 사용 중
   */
  MICROPHONE_UNDER_USE = "MICROPHONE_UNDER_USE", /* open failure */
  /**
   * 대기 타이머 초과
   */
  MICROPHONE_TIMER_EXPIRED = "MICROPHONE_TIMER_EXPIRED", /* closed success */
  /**
   * 장치 접근이 거부됨
   */
  MICROPHONE_PERMISSION_DENIED = "MICROPHONE_PERMISSION_DENIED"
}

/**
 * 마이크 상태
 */
export class MicrophoneStatus {
  /**
   * 이벤트 유형
   */
  eventType: MicrophoneEvent;
  /**
   * 현재가 export mode 인가?
   */
  expectMode: boolean;
  /**
   * 추가적인 메시지 여부
   */
  hasReason: boolean;
  /**
   * 에러 유형
   */
  failure: MicrophoneFailure;
  /**
   * 상세한 에러 메시지
   */
  message: string;

  /**
   * 생성자
   */
  constructor() {
    this.eventType = MicrophoneEvent.UNKNOWN;
    this.expectMode = false;
    this.hasReason = false;
    this.failure = MicrophoneFailure.MICROPHONE_ERROR_UNKNOWN;
    this.message = '';
  }
}

// ---------------------------------------------------
// SPEAKER
// ---------------------------------------------------

/**
 * 스피커 이벤트
 */
export enum SpeakerEvent {
  /**
   * 스피커 데이터 출력 중
   */
  MUTE = "MUTE",
  /**
   * 스피커 데이터 없음
   */
  UNMUTE = "UNMUTE",
  /**
   * 스피커 닫힘
   */
  CLOSED = "CLOSED"
}

/**
 * 스피커 에러 유형
 */
export enum SpeakerFailure {
  /**
   * 알수 없는 에러 또는 성공
   */
  UNKONWN = "UNKONWN",
  /**
   * 닫기 실패
   */
  SPAEKER_CLOSE_FAILED = "SPAEKER_CLOSE_FAILED",
  /**
   * 장치 접근이 거부됨
   */
  SPAEKER_PERMISSION_DENIED = "SPAEKER_PERMISSION_DENIED"
}

/**
 * 스피커 상태
 */
export class SpeakerStatus {
  /**
   * 스피커 이벤트
   */
  eventType: SpeakerEvent;
  /**
   * 현재 음성 출력 중인가?
   * 노래 출력 중일 경우에는 FALSE입니다.
   */
  speechSynthesizing: boolean;
  /**
   * 추가적인 메시지 여부
   */
  hasReason: boolean;
  /**
   * 스피키 여러 상세 유형
   */
  failure: SpeakerFailure;
  /**
   * 상세한 에러 메시지
   */
  message: string;

  /**
   * 생성자
   */
  constructor() {
    this.eventType = SpeakerEvent.MUTE;
    this.speechSynthesizing = false;
    this.hasReason = false;
    this.failure = SpeakerFailure.UNKONWN;
    this.message = '';
  }
}


// ---------------------------------------------------
// CAMERA
// ---------------------------------------------------

/**
 * 카메라 해상도
 */
export class Resolution {
  /**
   * 최소 너비
   */
  min_x: number;
  /**
   * 최소 높이
   */
  min_y: number;
  /**
   * 최대 너비
   */
  max_x: number;
  /**
   * 최대 높이
   */
  max_y: number;

  /**
   * 생성자
   */
  constructor() {
    this.min_x = 0;
    this.min_y = 0;
    this.max_x = 0;
    this.max_y = 0;
  }
}

/**
 * 카메라 파라미터
 */
export class CameraParam {
  /**
   * rear Camera = true, front Camera: false
   */
  rearCamera: boolean;
  /**
   * 확인결과 preview 상태일 때 App이 Background이므로 보통의 경우
   * Timer 를 구동시켜서 닫는 방법을 사용합니다.
   * 따라서 CameraDTO 객체에 timer interval을 추가하였습니다.
   *
   */
  openInterval: number;

  /**
   * 해상도
   */
  res: Resolution;

  /**
   * 워터마트 이미지 URL
   */
  watermarkImageUrl: string;


  /**
   * 출력 브리뷰, 높이에서 워터마크가 차지할 비율
   */
  watermarkResizeRateOfHeight : number;


  /**
   * 촬영 후 확인 이미지 URL
   */
  confirmImageUrl: string;


  /**
   * 촬영 후 확인 이미지, 높이에서 워터마크가 차지할 비율
   */
  confirmResizeRateOfHeight : number;


  /**
   * 0에서 1사이의 값
   */
  opacity: number;

  /**
   * 안내 문자열
   */
  guideText: string;

  /**
   * 생성자
   */
  constructor() {
    this.rearCamera = false;
    this.openInterval = 0;
    this.res = new Resolution();
    this.watermarkImageUrl = null;
    this.watermarkResizeRateOfHeight = null;
    this.confirmImageUrl = null;
    this.confirmResizeRateOfHeight = null;
    this.opacity = 0.0;
    this.guideText = '안내문을 출력하세요';
  }
}

/**
 * 카메라 이벤트
 */
export enum CameraEvent {
  /**
   * 알수 없는 이벤트
   */
  UNKNOWN = "UNKNOWN",
  /**
   * 열기 성공
   */
  OPEN_SUCCESS = "OPEN_SUCCESS",
  /**
   * 열기 닫기
   */
  OPEN_FAILURE = "OPEN_FAILURE",
  /**
   * 이미지 캡춰됨
   */
  IMAGE_CAPTURED = "IMAGE_CAPTURED",
  /**
   * 카메라 닫힘
   */
  CLOSED = "CLOSED"
}

/**
 * 카메라 실패 유형
 */
export enum CameraFailure {
  /**
   * 알수 없는 에러 또는 성공
   */
  CAMERA_ERROR_UNKNOWN = "CAMERA_ERROR_UNKNOWN",
  /**
   * 열기 실패
   */
  CAMERA_OPEN_ERROR = "CAMERA_OPEN_ERROR",
  /**
   * 닫기 실패
   */
  CAMERA_CLOSE_ERROR = "CAMERA_CLOSE_ERROR",
  /**
   * 캡춰 취소
   * 카메라에서 명시적으로 사용자에 의한 취소
   */
  CAMERA_CAPTURE_CANCELLED = "CAMERA_CAPTURE_CANCELLED",
  /**
   * 메모리 부족
   */
  CAMERA_INSUFFICIENT_MEMORY = "CAMERA_INSUFFICIENT_MEMORY",
  /**
   * 저장 실패
   */
  CAMERA_STORE_FAILED = "CAMERA_STORE_FAILED",
  /**
   * 카메라 타이머 지남
   */
  CAMERA_TIMER_EXPIRED = "CAMERA_TIMER_EXPIRED",
   /**
   * 장치 접근이 거부됨
   */
  CAMERA_PERMISSION_DENIED = "CAMERA_PERMISSION_DENIED"
}

/**
 * 이미지 데이터
 */
export class ImageData {
  /**
   * 로컬 장치 내부에 유지되는 이미지를 구분하는 리소스 경로
   */
  imageUrl: string;
  /**
   * capture 된 image의 Thumbnail data(base64 Encoded)
   */
  thumbnail: string;
  /**
   * 캡춰된 해상도
   */
  res: Resolution;
  /**
   * 이미지 전체 가로 (px)
   */
  width: number;
  /**
   * 이미지 전체 세로 (px)
   */
  height: number;
  /**
   * App 에서 보내줄 guide 비율의 가로 값
   */
  refVertexX: number;
  /**
   * App 에서 보내줄 guide 비율의 세로 값
   */
  refVertexY: number;

  /**
   * 생성자
   */
  constructor() {
    this.imageUrl = '';
    this.thumbnail = '';
    this.res = new Resolution();
    this.width = 0;
    this.height = 0;
    this.refVertexX = 0;
    this.refVertexY = 0;
  }
}

/**
 * 카머라 상태 정보
 */
export class CameraStatus {
  /**
   * 카메라 이벤트
   */
  eventType: CameraEvent;
  /**
   * 이미지 데이터
   */
  imageData: ImageData;
  /**
   * 추가적인 정보 여부
   */
  hasReason: boolean;
  /**
   * 카메라 에러 유형
   */
  failure: CameraFailure;
  /**
   * 상세한 에러 메시지
   */
  message: string;

  /**
   * 생성자
   */
  constructor() {
    this.eventType = CameraEvent.UNKNOWN;
    this.imageData = new ImageData();
    this.hasReason = false;
    this.failure = CameraFailure.CAMERA_ERROR_UNKNOWN;
    this.message = '';
  }
}

// ---------------------------------------------------
// GALLERY
// ---------------------------------------------------

/**
 * 갤러리 열기 파라미터
 */
export class GalleryParam {

  /**
   * 열기 대기 시간
   *
   * Gallery Open Interval(10의 경우 10초 동안 갤러리 이미지 선택까지 하지 않는 경우
   * 자동 종료되고 close/GALLERY_TIMER_EXPIRED가 JS에 전송됨)
   */
  openInterval: number;

  /**
   * 원하는 해상도 크기
   */
  res: Resolution;

  /**
   * 생성자
   */
  constructor() {
    this.openInterval = 0;
    this.res = new Resolution();
  }
}

/**
 * 갤러리에서 발생하는 이벤트
 */
export enum GalleryEvent {
  /**
   * 알수 없는 이벤트
   */
  UNKNOWN = "UNKNOWN",
  /**
   * 열기 성공
   */
  OPEN_SUCCESS = "OPEN_SUCCESS",
  /**
   * 열기 실패
   */
  OPEN_FAILURE = "OPEN_FAILURE",
  /**
   * 이미지 선택 완료
   */
  IMAGE_CAPTURED = "IMAGE_CAPTURED",
  /**
   * 갤러리 닫힘
   */
  CLOSED = "CLOSED"
}

/**
 * 갤러리 실패 유형
 */
export enum GalleryFailure {
  /**
   * 갤러리 모르는 에러 및 성공
   */
  GALLERY_ERROR_UNKNOWN = "GALLERY_ERROR_UNKNOWN",
  /**
   * 열기 실패
   */
  GALLERY_OPEN_ERROR = "GALLERY_OPEN_ERROR",
  /**
   * 닫기 실패
   */
  GALLERY_CLOSE_ERROR = "GALLERY_CLOSE_ERROR",
  /**
   * 사용자에 의한 선태 취소
   */
  GALLERY_SELECT_CANCELLED = "GALLERY_SELECT_CANCELLED",
  /**
   * 시간이 지나서 자동으로 닫힘
   */
  GALLERY_TIMER_EXPIRED = "GALLERY_TIMER_EXPIRED",
  /**
   * 장치 접근이 거부됨
   */
  GALLERY_PERMISSION_DENIED = "GALLERY_TIMER_EXPIRED"
}

/**
 * 갤러리 상태 정보
 */
export class GalleryStatus {
  /**
   * 갤러리 이벤트 유형
   */
  eventType: GalleryEvent;
  /**
   * 선택된 이미지 데이터
   */
  imageData: ImageData;
  /**
   * 추가적인 메시지 여부
   */
  hasReason: boolean;
  /**
   * 갤러리 에러의 상세한 정보
   */
  failure: GalleryFailure;
  /**
   * 추가적인 메시지
   */
  message: string;

  /**
   * 생성자
   */
  constructor() {
    this.eventType = GalleryEvent.UNKNOWN;
    this.hasReason = false;
    this.failure = GalleryFailure.GALLERY_ERROR_UNKNOWN;
    this.imageData = new ImageData();
    this.message = '';
  }
}

// ---------------------------------------------------
// LOCATION
// ---------------------------------------------------

/**
 * 위치 정보 획득 실패 이유
 */
export enum LocationFailure {
  LOCATION_UNKONWN = "LOCATION_UNKONWN",
  /**
   * 장치 접근이 거부됨
   */
  LOCATION_PERMISSION_DENIED = "LOCATION_PERMISSION_DENIED"
}

/**
 * 위치 정보 획득 상태
 *
 */
export class LocationStatus {
  /**
   * 구해 놓은 위치
   */
  location: Location;
  /**
   * 실패 이유가 있음
   */
  hasReason: boolean;
  /**
   * 실패 이유
   */
  failure: LocationFailure;
  /**
   * 상세한 메시지
   */
  message: string;

  /**
   * 생성자
   */
  constructor() {
    this.hasReason = false;
    this.location = new Location();
    this.failure = LocationFailure.LOCATION_UNKONWN;
  }
}


/**
 * 웹뷰가 구현하고 있는 코크들에 대한 정의
 *
 * 웹뷰에서 네이티브로 호출하는 함수들에 대한 규격을 정의한다.
 */
export interface M2uWebViewClientNative {

  // ---------------------------------------------------
  // SETTINGS
  // ---------------------------------------------------

  /**
   * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
   *  - TLS 사용여부
   *  - 서버의 주소 및 포트
   *  - Ping 주기
   *
   * @param {MapSetting} setting
   * @returns {boolean}
   * @desc Js to Native
   */
  setMapSetting(setting: MapSetting): boolean;

  // ---------------------------------------------------
  // MAP
  // ---------------------------------------------------
  /**
   * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
   *
   * - streamid
   * - operationid
   * - Param
   * - Payload 데이터는 함께 묶어서 전송한다.
   *
   *
   *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
   *
   *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
   *    - STT 음성의 전송
   *    - 이미지 서버로 이미지 전송
   *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
   *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
   *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
   *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
   *
   * @param {EventStream} es 서버로 보낼 이벤트 메시지
   * @param {string | null} opt 이미지 URL
   * @desc Js to Native
   */
  sendEvent(es: EventStream, opt: string | null): void;

  // ---------------------------------------------------
  // MIC
  // ---------------------------------------------------

  /**
   * 마이크를 열어 달라고 하는 이벤트
   *
   * @param {MicrophoneParam} param 개방 옵션
   * @desc JS to Native
   */
  openMicrophone(param: MicrophoneParam): void;


  /**
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
   *
   * @desc JS To Native
   */
  closeMicrophone(): void;


  // ---------------------------------------------------
  // SPEAKER
  // ---------------------------------------------------
  /**
   * 명시적으로 스피커를 닫는다.
   * @desc JS To Native
   */
  closeSpeaker(): void;

  // ---------------------------------------------------
  // CAMERA
  // ---------------------------------------------------

  /**
   * 카메라를 엽니다.
   *
   * @param {CameraParam} param
   * @desc JS To Native
   */
  openCamera(param: CameraParam): void;


  /**
   * 명시적으로 카메라를 닫는다.
   *
   * @desc JS To Native
   */
  closeCamera(): void;


  // ---------------------------------------------------
  // GALLERY
  // ---------------------------------------------------
  /**
   * 갤러리를 오픈한다.
   *
   * @param {GalleryParam} param
   */
  openGallery(param: GalleryParam): void;

  /**
   * 명시적으로 Gallery를 닫는다.
   */
  closeGallery(): void;


  // ---------------------------------------------------
  // LOCATION
  // ---------------------------------------------------
  /**
   *
   * 매번 현재 단말의 위치를 구해와라.
   *
   * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
   * EventStream의 context 정보에 적재하여 전송한다.
   *
   * @desc JS to Native
   */
  getLocation(): void;

  // ---------------------------------------------------
  // PHONE NUMBER
  // ---------------------------------------------------
  /**
   *
   * Phone Number 정보는 JS에서 필요시 호출한다.
   *
   * @desc JS to Native
   */
  getPhoneNumber(): any;
}


/**
 * 웹뷰가 구현하고 있는 코크들에 대한 정의
 *
 * 웹뷰에서 네이티브로 호출하는 함수들에 대한 규격을 정의한다.
 */
export interface M2uWebViewClientNativeMobile {

  // ---------------------------------------------------
  // SETTINGS
  // ---------------------------------------------------

  /**
   * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
   *  - TLS 사용여부
   *  - 서버의 주소 및 포트
   *  - Ping 주기
   *
   * @param {string} setting, MapSetting 값의 json string 버전
   * @returns {boolean}
   * @desc Js to Native
   */
  setMapSetting(setting: string): boolean;

  // ---------------------------------------------------
  // MAP
  // ---------------------------------------------------
  /**
   * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
   *
   * - streamid
   * - operationid
   * - Param
   * - Payload 데이터는 함께 묶어서 전송한다.
   *
   *
   *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
   *
   *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
   *    - STT 음성의 전송
   *    - 이미지 서버로 이미지 전송
   *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
   *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
   *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
   *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
   *
   * @param {string} es 서버로 보낼 이벤트 메시지, EventStream 객체의 string 버전
   * @param {string} imageUrl 이미지 URL
   * @desc Js to Native
   */
  sendEvent(es: string, imageUrl?: string): void;

  // ---------------------------------------------------
  // MIC
  // ---------------------------------------------------

  /**
   * 마이크를 열어 달라고 하는 이벤트
   *
   * @param {string} param 개방 옵션, MicrophoneParam 객체를 json 문자열로 변환한 것
   * @desc JS to Native
   */
  openMicrophone(param: string): void;


  /**
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
   *
   * @desc JS To Native
   */
  closeMicrophone(): void;


  // ---------------------------------------------------
  // SPEAKER
  // ---------------------------------------------------
  /**
   * 명시적으로 스피커를 닫는다.
   * @desc JS To Native
   */
  closeSpeaker(): void;

  // ---------------------------------------------------
  // CAMERA
  // ---------------------------------------------------

  /**
   * 카메라를 엽니다.
   *
   * @param {string} param 문자열로 변환된 CameraParam 객체
   * @desc JS To Native
   */
  openCamera(param: string): void;


  /**
   * 명시적으로 카메라를 닫는다.
   *
   * @desc JS To Native
   */
  closeCamera(): void;


  // ---------------------------------------------------
  // GALLERY
  // ---------------------------------------------------
  /**
   * 갤러리를 오픈한다.
   *
   * @param {GalleryParam} param
   */
  openGallery(param: string): void;

  /**
   * 명시적으로 Gallery를 닫는다.
   */
  closeGallery(): void;


  // ---------------------------------------------------
  // LOCATION
  // ---------------------------------------------------
  /**
   *
   * 매번 현재 단말의 위치를 구해와라.
   * @desc JS to Native
   */
  getLocation(): void;

  // ---------------------------------------------------
  // PHONE NUMBER
  // ---------------------------------------------------
  /**
   *
   * Phone Number 정보는 JS에서 필요시 호출한다.
   * @desc JS to Native
   */
  getPhoneNumber(): any;
}


/**
 * 네이티브에서 JAVASCRIPT를 호출할 때 사용하는 정보
 *
 * 아래와 같은 인터페이스 구조로 동작합니다.
 */
export interface M2uWebViewClientNativeEventForward {
  // ---------------------------------------------------
  // MAP
  // ---------------------------------------------------
  /**
   * MAP 서버로부터 응답을 받은다.
   * - opid에 대한 싱크 관리는 Web에서 처리하도록 한다.
   *
   * @param {string} dir 응답받은 메시지 데이터
   * @desc Native to JS
   */
  receiveDirective(dir: string): void;

  /**
   * MAP directive 수신 완료
   * @param {string} end  operation sync id를 문자욜로 보낸다.
   * JSON으로 포장하지 않고 syncid만 그대로 전송한다.
   */
  receiveCompleted(end: string): void;


  /**
   * map 통신 중에 수신한 exception
   *
   * mapdirective의 내용을 json 형태로 전송한다.
   *
   * @param {string} ex MapException MAP 통신 중에서 Exception이 발생한 경우
   * @desc Native to JS
   */
  receiveException(ex: string): void;


  /**
   * 에러를 수신한다.
   * grpc Error를 수신한 경우에 이를 담아서 보내준다
   *
   * @param {string} status GrpcStatus GRPC 에러 생태 값 및 상세 정보
   * @desc Native to JS
   */
  receiveError(status: string): void;


  /**
   * SendEvent에 딸린 스트림을 전송하는 경우에는 Web에 알려준다.
   * 스트림을 전송하고 있는 동안에 화면에 정보를 표시할 수 있도록 처리한다.
   *
   * 스트림의 끝, 스트림 중지의 경우에도 보내준다
   *
   * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
   *
   * @param {string} st StreamEventStatus 타입
   * @desc Native To JS
   */
  sendStreamingEvent(st: string): void;


  /**
   * receiveDirective에 딸린 스트림을 수신하는 경우에는 Web에 알려준다.
   * 이것은 TTS 출력 중인 상태와 같이 스트림을 수신하고 있는 동아네
   * 화면에 정보를 표시할 수 있도록 처리한다.
   *
   * 스트림의 끝, 스트림 중지의 경우에도 보내준다
   *
   * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
   *
   * @param {string} st StreamEventStatus
   * @desc Native To JS
   */
  receiveStreamingDirective(st: string): void;

  /**
   * Ping의 결과가 있으면 이를 반환한다.
   *
   * iOS의 경우에 백그라운드 앱은 네트워크를 사용할 수 없으므로 깨어나면 바로 자동으로 시작하도록 한다.
   *
   * setMapSetting() 에 의해서 pingEnabled = false로 주어지면, ping은 내부적으로 중지될 수 있다.
   *
   * @param {string} pong  PongResponse 수신받든 Pong 객체
   * @desc Native To JS
   */
  notifyPong(pong: string): void;

  // ---------------------------------------------------
  // MIC
  // ---------------------------------------------------

  /**
   * 마이크 상태를 반환한다.
   *
   * 1. 마이크 열림 성공, 또는 실패
   * 2. 최초 음성 레코딩이 시작될 때 알림
   * 3. 마이크 닫는 호출에 대한 응답을 반환한다.
   *
   * @param {string } st MicrophoneStatus type
   * @desc Native To JS
   */
  notifyMicrophoneStatus(st: string): void;


  // ---------------------------------------------------
  // SPEAKER
  // ---------------------------------------------------
  /**
   * 마이크 개방 후 상태를 알려줍니다.
   *
   * @param {string } ss SpeakerStatus 마이크 개방 상태
   * @desc Native To JS
   */
  notifySpeakerStatus(ss: string): void;


  // ---------------------------------------------------
  // CAMERA
  // ---------------------------------------------------

  /**
   * 성공, 실패, 데이터 수집 등 카메라 상태를 반환한다.
   *
   * @param {string} cs CameraStatus type 카메라 상태
   * @desc Native to JS
   */
  notifyCameraStatus(cs: string): void;


  // ---------------------------------------------------
  // GALLERY
  // ---------------------------------------------------
  /**
   * 갤러리 상태를 반환한다.
   *
   * @param {string} st GalleryStatus type
   */
  notifyGalleryStatus(st: string): void;


  // ---------------------------------------------------
  // LOCATION
  // ---------------------------------------------------
  /**
   *
   * @param {LocationStatus} st 구해진 위치 정보를 반환한다.
   */
  notifyLocation(st: string): void;
}


/**
 * 네이티브에서 JAVASCRIPT를 호출할 때 사용하는 정보
 *
 * 아래와 같은 인터페이스 구조로 동작합니다.
 */
export interface M2uWebViewClientCallable {
  // ---------------------------------------------------
  // MAP
  // ---------------------------------------------------
  /**
   * MAP 서버로부터 응답을 받은다.
   * - opid에 대한 싱크 관리는 Web에서 처리하도록 한다.
   *
   * @param {DirectiveStream} dir 응답받은 메시지 데이터
   * @desc Native to JS
   */
  receiveDirective(dir: DirectiveStream): void;


  /**
   * map 통신 중에 수신한 exception
   *
   * mapdirective의 내용을 json 형태로 전송한다.
   *
   * @param {MapException} ex MAP 통신 중에서 Exception이 발생한 경우
   * @desc Native to JS
   */
  receiveException(ex: MapException): void;


  /**
   * 에러를 수신한다.
   * grpc Error를 수신한 경우에 이를 담아서 보내준다
   *
   * @param {GrpcStatus} status GRPC 에러 생태 값 및 상세 정보
   * @desc Native to JS
   */
  receiveError(status: GrpcStatus): void;


  /**
   * SendEvent에 딸린 스트림을 전송하는 경우에는 Web에 알려준다.
   * 스트림을 전송하고 있는 동안에 화면에 정보를 표시할 수 있도록 처리한다.
   *
   * 스트림의 끝, 스트림 중지의 경우에도 보내준다
   *
   * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
   *
   * @param {StreamingStatus} st
   * @desc Native To JS
   */
  sendStreamingEvent(st: StreamingStatus): void;


  /**
   * receiveDirective에 딸린 스트림을 수신하는 경우에는 Web에 알려준다.
   * 이것은 TTS 출력 중인 상태와 같이 스트림을 수신하고 있는 동아네
   * 화면에 정보를 표시할 수 있도록 처리한다.
   *
   * 스트림의 끝, 스트림 중지의 경우에도 보내준다
   *
   * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
   *
   * @param {StreamingStatus} st
   * @desc Native To JS
   */
  receiveStreamingDirective(st: StreamingStatus): void;

  /**
   * Ping의 결과가 있으면 이를 반환한다.
   *
   * iOS의 경우에 백그라운드 앱은 네트워크를 사용할 수 없으므로 깨어나면 바로 자동으로 시작하도록 한다.
   *
   * setMapSetting() 에 의해서 pingEnabled = false로 주어지면, ping은 내부적으로 중지될 수 있다.
   *
   * @param {PongResponse} pong 수신받든 Pong 객체
   * @desc Native To JS
   */
  notifyPong(pong: PongResponse): void;

  // ---------------------------------------------------
  // MIC
  // ---------------------------------------------------

  /**
   * 마이크 상태를 반환한다.
   *
   * 1. 마이크 열림 성공, 또는 실패
   * 2. 최초 음성 레코딩이 시작될 때 알림
   * 3. 마이크 닫는 호출에 대한 응답을 반환한다.
   *
   * @param {MicrophoneStatus} st
   * @desc Native To JS
   */
  notifyMicrophoneStatus(st: MicrophoneStatus): void;


  // ---------------------------------------------------
  // SPEAKER
  // ---------------------------------------------------
  /**
   * 마이크 개방 후 상태를 알려줍니다.
   *
   * @param {SpeakerStatus} ss 마이크 개방 상태
   * @desc Native To JS
   */
  notifySpeakerStatus(ss: SpeakerStatus): void;


  // ---------------------------------------------------
  // CAMERA
  // ---------------------------------------------------

  /**
   * 성공, 실패, 데이터 수집 등 카메라 상태를 반환한다.
   *
   * @param {CameraStatus} cs 카메라 상태
   * @desc Native to JS
   */
  notifyCameraStatus(cs: CameraStatus): void;


  // ---------------------------------------------------
  // GALLERY
  // ---------------------------------------------------
  /**
   * 갤러리 상태를 반환한다.
   *
   * @param {GalleryStatus} st
   */
  notifyGalleryStatus(st: GalleryStatus): void;


  // ---------------------------------------------------
  // LOCATION
  // ---------------------------------------------------
  /**
   *
   * @param {LocationStatus} st 구해진 위치 정보를 반환한다.
   */
  notifyLocation(st: LocationStatus): void;
}

export class AppleM2uWebViewClientNative implements M2uWebViewClientNativeMobile {

  /**
   * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
   *  - TLS 사용여부
   *  - 서버의 주소 및 포트
   *  - Ping 주기
   *
   * @param {string} setting, MapSetting 값의 json string 버전
   * @returns {boolean}
   * @desc Js to Native
   */
  setMapSetting(setting: string): boolean {
    var msg= {
      "name" : "setMapSetting",
      "params" : setting
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );

    return true;
  }

  /**
   * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
   *
   * - streamid
   * - operationid
   * - Param
   * - Payload 데이터는 함께 묶어서 전송한다.
   *
   *
   *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
   *
   *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
   *    - STT 음성의 전송
   *    - 이미지 서버로 이미지 전송
   *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
   *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
   *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
   *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
   *
   * @param {string} es 서버로 보낼 이벤트 메시지, EventStream 객체의 string 버전
   * @param {string | null} opt 이미지 URL
   * @desc Js to Native
   */
  sendEvent(es: string, opt: string | null): void {
    var msg= {
      "name" : "sendEvent",
      "params" : es ,
      "imageUrl" : opt
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   * 마이크를 열어 달라고 하는 이벤트
   *
   * @param {string} param 개방 옵션, MicrophoneParam 객체를 json 문자열로 변환한 것
   * @desc JS to Native
   */
  openMicrophone(param: string): void {
    var msg = {
      "name" : "openMicrophone",
      "params" : param
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
   *
   * @desc JS To Native
   */
  closeMicrophone(): void {
    var msg = {
      "name" : "closeMicrophone",
      "params" : ""
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   * 명시적으로 스피커를 닫는다.
   * @desc JS To Native
   */
  closeSpeaker(): void {
    var msg = {
      "name" : "closeSpeaker",
      "params" : ""
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   * 카메라를 엽니다.
   *
   * @param {string} param 문자열로 변환된 CameraParam 객체
   * @desc JS To Native
   */
  openCamera(param: string): void {
    var msg = {
      "name" : "openCamera",
      "params" : param
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   * 명시적으로 카메라를 닫는다.
   *
   * @desc JS To Native
   */
  closeCamera(): void {
    var msg = {
      "name" : "closeCamera",
      "params" : ""
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   * 갤러리를 오픈한다.
   * call to native
   * @param {string} param
   */
  openGallery(param: string): void {
    var msg = {
      "name" : "openGallery",
      "params" : param
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   * 명시적으로 Gallery를 닫는다.
   */
  closeGallery(): void {
    var msg = {
      "name" : "closeGallery",
      "params" : ""
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   *
   * 매번 현재 단말의 위치를 구해와라.
   *
   * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
   * EventStream의 context 정보에 적재하여 전송한다.
   *
   * @desc JS to Native
   */
  getLocation(): void {
    var msg = {
      "name" : "getLocation",
      "params" : ""
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  /**
   *
   * Phone Number 정보는 JS에서 필요시 호출한다.
   *
   * @desc JS to Native
   */
  getPhoneNumber(): any {
    var msg = {
      "name" : "getPhoneNumber",
      "params" : ""
    };
    window.webkit.messageHandlers.m2uWebViewNative.postMessage( msg );
  }

  constructor() {
  }
}


/**
 * 디버깅 용도로 사용되는 Local 구현
 */
export class LocalM2uWebViewClientNative implements M2uWebViewClientNativeMobile {

  /**
   * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
   *  - TLS 사용여부
   *  - 서버의 주소 및 포트
   *  - Ping 주기
   *
   * @param {string} setting, MapSetting 값의 json string 버전
   * @returns {boolean}
   * @desc Js to Native
   */
  setMapSetting(setting: string): boolean {
    console.log('setMapSetting', setting);
    return true;
  }

  /**
   * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
   *
   * - streamid
   * - operationid
   * - Param
   * - Payload 데이터는 함께 묶어서 전송한다.
   *
   *
   *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
   *
   *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
   *    - STT 음성의 전송
   *    - 이미지 서버로 이미지 전송
   *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
   *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
   *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
   *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
   *
   * @param {string} es 서버로 보낼 이벤트 메시지, EventStream 객체의 string 버전
   * @param {string | null} opt 이미지 URL
   * @desc Js to Native
   */
  sendEvent(es: string, opt: string | null): void {
    console.log('sendEvent', es, opt);
  }

  /**
   * 마이크를 열어 달라고 하는 이벤트
   *
   * @param {string} param 개방 옵션, MicrophoneParam 객체를 json 문자열로 변환한 것
   * @desc JS to Native
   */
  openMicrophone(param: string): void {
    console.log('openMicrophone', param);
  }

  /**
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
   *
   * @desc JS To Native
   */
  closeMicrophone(): void {
    console.log('closeMicrophone');
  }

  /**
   * 명시적으로 스피커를 닫는다.
   * @desc JS To Native
   */
  closeSpeaker(): void {
    console.log('closeSpeaker');
  }

  /**
   * 카메라를 엽니다.
   *
   * @param {string} param 문자열로 변환된 CameraParam 객체
   * @desc JS To Native
   */
  openCamera(param: string): void {
    console.log('openCamera', param);
  }

  /**
   * 명시적으로 카메라를 닫는다.
   *
   * @desc JS To Native
   */
  closeCamera(): void {
    console.log('closeCamera');
  }

  openGallery(param: string): void {
    console.log('openGallery', param);
  }

  /**
   * 명시적으로 Gallery를 닫는다.
   */
  closeGallery(): void {
    console.log('closeGallery');
  }

  /**
   *
   * 매번 현재 단말의 위치를 구해와라.
   *
   * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
   * EventStream의 context 정보에 적재하여 전송한다.
   *
   * @desc JS to Native
   */
  getLocation(): void {
    console.log('getLocation');
  }

  /**
   *
   * Phone Number 정보는 JS에서 필요시 호출한다.
   *
   * @desc JS to Native
   */
  getPhoneNumber(): any {
    console.log('getPhoneNumber');
  }

  constructor() {
  }
}