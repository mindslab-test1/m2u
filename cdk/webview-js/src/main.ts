import {
  AsyncInterface,
  AudioEncoding,
  AvatarSetExpressionPayload,
  Card,
  Device,
  DirectiveStream,
  EventContext,
  EventPayload,
  EventStream,
  ExpectSpeechPayload,
  ImageFormat,
  ImageRecognitionParam,
  Lang,
  LauncherLaunchAppPayload,
  LauncherAuthorizePayload,
  LauncherFillSlotsPayload,
  LauncherLaunchPagePayload,
  LauncherViewMapPayload,
  Location,
  MapException,
  MultiFactorVerifyPayload,
  OpenUtter,
  RenderTextPayload,
  SignInPayload,
  SignInResultPayload,
  SignOutPayload,
  SignOutResultPayload,
  SpeechRecognitionParam,
  StreamingRecognizeResponse,
  Struct,
  UserKey,
  UserSettings,
  Utter,
  DialogAgentForwarderParam, EventParam
} from "./proto";
import {
  AppleM2uWebViewClientNative,
  CameraParam,
  CameraStatus,
  GalleryParam,
  GalleryStatus,
  GrpcStatus,
  LocalM2uWebViewClientNative,
  LocationStatus,
  M2uWebViewClientNative,
  M2uWebViewClientNativeEventForward,
  M2uWebViewClientNativeMobile,
  MapSetting,
  MicrophoneParam,
  MicrophoneStatus,
  SpeakerStatus,
  StreamingStatus
} from "./native";

import {UUID} from "./uuid";

/**
 * 윈도우 객체에 추가적인 값을 정의합니다.
 */
declare global {
  /**
   * 표준 윈도우 객체
   */
  interface Window {
    /**
     * Android Native Interface의 이름
     */
    m2uWebViewNative: M2uWebViewClientNativeMobile;
    /**
     * 네이티브에서 JS로 호출할 때 사용하는 객체의 이름을 담는 중간 객체
     */
    m2uNativeForward: any;

    mySecret: any;

    /**
     * webkit을 객체로 정의
     */
    webkit: any;
  }
}


/**
 * 서버에서 내려온 디렉티브 및 네이티브에서 발생한 이벤트를 묶어서 처리하는 구조
 */
export interface MapDirectiveHandler {
  /**
   * 텍스트 출력 콜백
   * @param {RenderTextPayload} text RnderText 호출에 따르는 Payload
   */
  onViewRenderText(text: RenderTextPayload): void;

  /**
   * 카드 출력 콜백
   * @param {Card} card 메시지 카드
   */
  onViewRenderCard(card: Card): void;

  /**
   * 메타 데이터 수신 콜백
   * @param {Struct} hidden
   */
  onViewRenderHidden(hidden: Struct): void;

  /**
   * 음성인식 진행 중일 때 콜백
   * @param {StreamingRecognizeResponse} o 음성인식 응답
   */
  onSpeechRecognizerRecognizing(o: StreamingRecognizeResponse): void;

  /**
   * 음성인식 완료되었을 때 콜백
   * @param {StreamingRecognizeResponse} o 음성인식 응답
   */
  onSpeechRecognizerRecognized(o: StreamingRecognizeResponse): void;

  /**
   * 대화 처리 중일 때 콜백
   */
  onDialogProcessing(): void;

  /**
   * 음성합성 출력할 때 콜백
   */
  onSpeechSynthesizerPlayStarted(): void;

  /**
   * 음성 입력 대기 수신할 때 콜백
   * @param {ExpectSpeechPayload} o 음성입력 대기에 필요한 추가적인 Payload 데이터
   */
  onMicrophoneExpectSpeech(o: ExpectSpeechPayload): void;

  /**
   * 아바타 설정 명령어 수신할 때 콜백
   * @param {AvatarSetExpressionPayload} o 아바타 출력을 위한 페이로드
   */
  onAvatarSetExpression(o: AvatarSetExpressionPayload): void;

  /**
   * 인증을 실행하라는 명렁어 수신할때 콜백
   * @param {LauncherAuthorizePayload} o
   * @param {DialogAgentForwarderParam} p
   */
  onLauncherAuthorize(o: LauncherAuthorizePayload,
                      p: DialogAgentForwarderParam): void;

  /**
   * 단말이 가지고 있는 슬롯을 채워서 보내달라는 콜백
   * @param {LauncherFillSlotsPayload} o
   * @param {DialogAgentForwarderParam} p
   */
  onLauncherFillSlots(o: LauncherFillSlotsPayload,
                      p: DialogAgentForwarderParam): void;

  /**
   * 클라이언트에게 LaunchApp을 실행해달라는 콜백
   * @param {LauncherLaunchAppPayload} o
   */
  onLauncherLaunchApp(o: LauncherLaunchAppPayload): void;

  /**
   * 클라이언트에게 LaunchPage을 실행해달라는 콜백
   * @param {LauncherPagePayload} o
   */
  onLauncherLaunchPage(o: LauncherLaunchPagePayload): void;

  /**
   * 클라이언트에게 LauncherViewMap을 실행해달라는 콜백
   * @param {LauncherViewMapPayload} o
   */
  onLauncherViewMap(o: LauncherViewMapPayload): void;

  /**
   * 이미지 인식이 완료되었을 때
   */
  onImageDocumentRecognized(): void;

  /**
   * 로그인에 대한 응답 콜백
   * @param {SignInResultPayload} o 인증 결과 PAYLOAD
   */
  onAuthenticationSignInResult(o: SignInResultPayload): void;

  /**
   * 로그아웃에 대한 응답 콜백
   * @param {SignOutResultPayload} o 응답 메시지
   */
  onAuthenticationSignOutResult(o: SignOutResultPayload): void;

  /**
   * 다중 인증에 대한 응답이 올 때 발생하는 콜백
   * @param {SignInResultPayload} o 인증 결과 PAYLOAD
   */
  onAuthenticationMultiFactorVerifyResult(o: SignInResultPayload): void;

  /**
   * 사용자 속성 정의에 대한 응답이 올 때 발생하는 콜백
   * @param {UserSettings} o 사용자 속성 정의 결과 PAYLOAD
   */
  onAuthenticationUpdateUserSettingsResult(o: UserSettings): void;

  /**
   * 사용자 속성 조회에 대한 응답이 올 때 발생하는 콜백
   * @param {UserSettings} o 사용자 속성 정의 결과 PAYLOAD
   */
  onAuthenticationGetUserSettingsResult(o: UserSettings): void;

  /**
   * 예외가 발생한 경우에 발생할 콜백
   * @param {MapException} e MapException
   */
  onException(e: MapException): void;

  /**
   * 호출이 완료될 때 나올 콜백
   * @param {string} opid 오퍼레이션 ID
   */
  onOperationCompleted(opid: string): void;

  /**
   * 에러가 발생될 때 처리할 콜밸
   * @param {GrpcStatus} error 에러
   */
  onError(error: GrpcStatus): void;

  /**
   * 음성이나 이미지를 전송 중이거나 음성 출력이 내려오고 있을 때
   * 정보를 알려줍니다.
   *
   * @param {StreamingStatus} o
   */
  onStreamingEvent(o: StreamingStatus): void;

  /**
   * 마이크 생태가 바뀔 때 콜백
   * @param {MicrophoneStatus} st 마이크 상태
   */
  onMicrophoneStatus(st: MicrophoneStatus): void;

  /**
   * 마이크 생태가 바뀔 때 콜백
   *
   * @param {SpeakerStatus} st 스피커 상태
   */
  onSpeakerStatus(st: SpeakerStatus): void;

  /**
   * 카메라 상태 정보
   *
   * @param {CameraStatus} st 카메라 상태
   */
  onCameraStatus(st: CameraStatus): void;

  /**
   * 갤러리 상태 정보
   *
   * @param {GalleryStatus} st 갤러리 상태
   */
  onGalleryStatus(st: GalleryStatus): void;

  /**
   * 위치 상태 정보 변경시 콜백
   * @param {LocationStatus} st 위치정보 변경
   */
  onLocationChanged(st: LocationStatus): void;
}

/**
 * 대화 인터페이스들을 호출 함수 묶음
 */
export interface Dialog {
  /**
   * 새로운 세션을 엽니다.
   *
   * M2U 시스템은 세션이 열리자 마자 새로운 발화를 내보내개 됩니다.
   *
   * @param {OpenUtter} openUtter
   *   `openUtter.chatbot`이 비어있으면 Device.
   *   `openUtter.utter`는 기본값이 비어있습니다.
   *   `openUtter.meta`에는 원하는 추가적인 정보를 넣어줄 수도 있습니다.
   * @returns {string} operation id
   */
  open(openUtter: OpenUtter): string;

  /**
   * 음성으로 보내고 음성으로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * - SpeechRecognizer.Recognizing
   * - SpeechRecognizer.Recognized
   * - Dialog.Processing
   * - SpeechSynthesizer.Play
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   * - Microphone.ExpectSpeech
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
   * @returns {string} operation id
   */
  speechToSpeechTalk(param?: SpeechRecognitionParam): string;

  /**
   * 음성으로 보내고 텍스트로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * - SpeechRecognizer.Recognizing
   * - SpeechRecognizer.Recognized
   * - Dialog.Processing
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
   * @returns {string} operation id
   */
  speechToTextTalk(param?: SpeechRecognitionParam): string;

  /**
   * 텍스트로 보내고 텍스트로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
   * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
   *
   * - Dialog.Processing
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {Utter} utter 이 값은 필수입니다.
   * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
   * @returns {string} operation id
   */
  textToTextTalk(utter: Utter): string;

  /**
   * 텍스트로 보내고 음성으로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
   * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
   *
   * - Dialog.Processing
   * - SpeechSynthesizer.Play
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {Utter} utter 이 값은 필수입니다.
   * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
   */
  textToSpeechTalk(utter: Utter): string;

  /**
   * 이미지를 보내고 텍스트로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
   * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
   *
   * - ImageDocument.Recognized
   * - Dialog.Processing
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
   *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
   * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.

   * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
   */
  imageToTextTalk(imageUrl: string, param?: ImageRecognitionParam): string;


  /**
   * 이미지를 보내고 음성으로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
   * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
   *
   * - ImageDocument.Recognized
   * - Dialog.Processing
   * - SpeechSynthesizer.Play
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   * - Microphone.ExpectSpeech
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
   *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
   * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.

   * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
   */
  imageToSpeechTalk(imageUrl: string, param?: ImageRecognitionParam): string;

  /**
   * 명시적으로 세션을 닫습니다.
   */
  close(): string;
}


/**
 * 인증 처리 인터페이스
 */
export interface Authentication {
  /**
   * 인증을 호출합니다.
   *
   * @param {SignInPayload} payload 인증 처리 요청 데이터
   */
  signIn(payload: SignInPayload): void;

  /**
   * SignOut을 호출합니다.
   * 인증토큰을 클리어합니다.
   */
  signOut(): void;

  /**
   * 다중 팩터 인증을 시작합니다.
   *
   * @param {MultiFactorVerifyPayload} payload 다중 팩터 인증을 위한 처리 데이터
   */
  multiFactorVerify(payload: MultiFactorVerifyPayload): void;

  /**
   * 사용자 속성 정의
   * 인증된 사용자만 이 호출을 진행할 수 있다.
   * 바꾸고 싶은 값만 넣을 수 있다.
   * 응답으로는 모든 설정정보를 다 반환한다.
   *
   * @param {UserSettings} userSettings 사용자 속성 정의 데이터
   * @returns {string}
   * @constructor
   */
  updateUserSettings(payload: UserSettings): string;

  /**
   * 사용자 속성 조회
   * 인증된 사용자만 이 호출을 진행할 수 있다.
   * 설정정보를 조회한다.
   *
   * @param {UserKey} userKey 사용자 속성 조회를 위한 키
   * @returns {string}
   * @constructor
   */
  getUserSettings(payload: UserKey): string;
}


/**
 * Native에서 MAPClient 쪽으로 보내는 메시지를 해석해서 내부적으로 이벤트를 발생시켜주는 클래스
 *
 * 이벤트의 유형에 따라서 다양한 메시지를 전달해준다.
 */
export class NativeEventForward implements M2uWebViewClientNativeEventForward {
  private mapClient: MapClient = undefined;

  /**
   * NativeEventForward 생성자
   * @param {MapClient} mapClient `mapClient`로부터 받은 이벤트를 처리하도록 한다.
   */
  constructor(mapClient: MapClient) {
    this.mapClient = mapClient;
  }

  // ---------------------------------------------------
  // MAP
  // ---------------------------------------------------
  /**
   * MAP 서버로부터 응답을 받은다.
   * - opid에 대한 싱크 관리는 Web에서 처리하도록 한다.
   *
   * @param {string} directive 응답받은 메시지 데이터
   * @desc Native to JS
   */
  receiveDirective(directive: string): void {
    let obj = JSON.parse(directive);
    let dir: DirectiveStream = new DirectiveStream();
    dir = {...dir, ...obj};
    let ifname = dir.interface.interface + '.' + dir.interface.operation;

    // 스트리밍 메시지를 수신한 경우에는 streaming 에 값을 넣어둡니다.
    if (dir.interface.streaming) {
      this.mapClient._streams[dir.streamId] = dir.interface;
    }
    let handler = this.mapClient.handler;

    // 핸들러가 정의되어 있지 않으면 중지 한다.
    if (!handler) {
      console.log('no handler assigned!');
      return;
    }
    // 각 인터페이스별로 분기하여 이를 클라이언트의 콜백으로 호출하도록 한다.
    switch (ifname) {
      case 'View.RenderText': {
        let o1: RenderTextPayload = new RenderTextPayload();
        o1 = {...o1, ...dir.payload};
        if (handler.onViewRenderText)
          handler.onViewRenderText(o1);
        break;
      }
      case 'View.RenderCard': {
        let o2: Card = new Card();
        o2 = {...o2, ...dir.payload};
        if (handler.onViewRenderCard)
          handler.onViewRenderCard(o2);
        break;
      }
      case 'View.RenderHidden': {
        let o3: Struct = new Struct();
        o3 = {...o3, ...dir.payload};
        if (handler.onViewRenderHidden)
          handler.onViewRenderHidden(o3);
        break;
      }
      case 'SpeechRecognizer.Recognizing': {
        let o4: StreamingRecognizeResponse = new StreamingRecognizeResponse();
        o4 = {...o4, ...dir.payload};
        if (handler.onSpeechRecognizerRecognizing)
          handler.onSpeechRecognizerRecognizing(o4);
        break;
      }
      case 'SpeechRecognizer.Recognized': {
        let o5: StreamingRecognizeResponse = new StreamingRecognizeResponse();
        o5 = {...o5, ...dir.payload};
        if (handler.onSpeechRecognizerRecognized)
          handler.onSpeechRecognizerRecognized(o5);
        break;
      }
      case 'Dialog.Processing': {
        if (handler.onDialogProcessing)
          handler.onDialogProcessing();
        break;
      }
      case 'SpeechSynthesizer.Play': {
        if (handler.onSpeechSynthesizerPlayStarted)
          handler.onSpeechSynthesizerPlayStarted();
        break;
      }
      case 'Microphone.ExpectSpeech': {
        let o6: ExpectSpeechPayload = new ExpectSpeechPayload();
        o6 = {...o6, ...dir.payload};
        if (handler.onMicrophoneExpectSpeech)
          handler.onMicrophoneExpectSpeech(o6);
        break;
      }
      case 'Avatar.SetExpression': {
        let o7: AvatarSetExpressionPayload = new AvatarSetExpressionPayload();
        o7 = {...o7, ...dir.payload};
        if (handler.onAvatarSetExpression)
          handler.onAvatarSetExpression(o7);
        break;
      }
      case 'ImageDocument.Recognized': {
        if (handler.onImageDocumentRecognized)
          handler.onImageDocumentRecognized();
        break;
      }
      case 'Authentication.SignInResult': {
        let o8: SignInResultPayload = new SignInResultPayload();
        o8 = {...o8, ...dir.payload};

        // 강제로 `authToken`을 지정합니다.
        if (o8.authSuccess && o8.authSuccess.authToken)
          this.mapClient.authToken = o8.authSuccess.authToken;

        if (handler.onAuthenticationSignInResult)
          handler.onAuthenticationSignInResult(o8);
        break;
      }
      case 'Authentication.SignOutResult': {
        let o9: SignOutResultPayload = new SignOutResultPayload();
        o9 = {...o9, ...dir.payload};
        if (handler.onAuthenticationSignOutResult)
          handler.onAuthenticationSignOutResult(o9);

        this.mapClient.authToken = null;
        break;
      }
      case 'Authentication.MultiFactorVerifyResult': {
        let p0: SignInResultPayload = new SignInResultPayload();
        p0 = {...p0, ...dir.payload};

        // 강제로 `authToken`을 지정합니다.
        if (p0.authSuccess && p0.authSuccess.authToken)
          this.mapClient.authToken = p0.authSuccess.authToken;

        if (handler.onAuthenticationMultiFactorVerifyResult)
          handler.onAuthenticationMultiFactorVerifyResult(p0);
        break;
      }
      case 'Authentication.UpdateUserSettingsResult': {
        let p11: UserSettings = new UserSettings();
        p11 = {...p11, ...dir.payload};
        if (handler.onAuthenticationUpdateUserSettingsResult)
          handler.onAuthenticationUpdateUserSettingsResult(p11);
        break;
      }
      case 'Authentication.GetUserSettingsResult': {
        let p12: UserSettings = new UserSettings();
        p12 = {...p12, ...dir.payload};
        if (handler.onAuthenticationGetUserSettingsResult)
          handler.onAuthenticationGetUserSettingsResult(p12);
        break;
      }
      case 'Launcher.Authorize': {
        let p13: LauncherAuthorizePayload = new LauncherAuthorizePayload();
        let a1: DialogAgentForwarderParam = new DialogAgentForwarderParam();
        a1 = {...a1, ...dir.param.daForwardParam};
        p13 = {...p13, ...dir.payload};
        if (handler.onLauncherAuthorize)
          handler.onLauncherAuthorize(p13, a1);
        break;
      }
      case 'Launcher.FillSlots': {
        let p14: LauncherFillSlotsPayload = new LauncherFillSlotsPayload();
        let a2: DialogAgentForwarderParam = new DialogAgentForwarderParam();
        a2 = {...a2, ...dir.param.daForwardParam};
        p14 = {...p14, ...dir.payload};
        if (handler.onLauncherFillSlots)
          handler.onLauncherFillSlots(p14, a2);
        break;
      }
      case 'Launcher.LaunchApp': {
        let p15: LauncherLaunchAppPayload = new LauncherLaunchAppPayload();
        p15 = {...p15, ...dir.payload};
        if (handler.onLauncherLaunchApp)
          handler.onLauncherLaunchApp(p15);
        break;
      }
      case 'Launcher.LaunchPage': {
        let p16: LauncherLaunchPagePayload = new LauncherLaunchPagePayload();
        p16 = {...p16, ...dir.payload};
        if (handler.onLauncherLaunchPage)
          handler.onLauncherLaunchPage(p16);
        break;
      }
      case 'Launcher.ViewMap': {
        let p17: LauncherViewMapPayload = new LauncherViewMapPayload();
        p17 = {...p17, ...dir.payload};
        if (handler.onLauncherViewMap)
          handler.onLauncherViewMap(p17);
        break;
      }
    }
    if (this.mapClient._debug && this.mapClient._debug.onDirective)
      this.mapClient._debug.onDirective(JSON.stringify(dir, replacer, 2));
  }


  /**
   * map 통신 중에 수신한 exception
   *
   * mapdirective의 내용을 json 형태로 전송한다.
   *
   * @param {string} exception MapException MAP 통신 중에서 Exception이 발생한 경우
   * @desc Native to JS
   */
  receiveException(exception: string): void {
    let obj = JSON.parse(exception);
    let mape: MapException = new MapException();
    mape = {...mape, ...obj};
    let handler = this.mapClient.handler;
    if (handler && handler.onException)
      handler.onException(mape);
  }


  /**
   * 해당 `OPERATION`이 완료되면, 발생시킨다.
   *
   * Java의 경우, `OnComplete()` 호출의 결과 전송하도록 한다.
   *
   * @param {string} opid operation id
   */
  receiveCompleted(opid: string): void {
    this.mapClient.clearOperationId(opid);
    let handler = this.mapClient.handler;
    if (handler && handler.onOperationCompleted)
      handler.onOperationCompleted(opid);
  }

  /**
   * 에러를 수신한다.
   * grpc Error를 수신한 경우에 이를 담아서 보내준다
   *
   * @param {string} status GrpcStatus GRPC 에러 생태 값 및 상세 정보
   * @desc Native to JS
   */
  receiveError(status: string): void {
    let obj = JSON.parse(status);
    let s: GrpcStatus = new GrpcStatus();
    s = {...s, ...obj};
    let handler = this.mapClient.handler;
    if (handler && handler.onError)
      handler.onError(s);
  }


  /**
   * SendEvent에 딸린 스트림을 전송하는 경우에는 Web에 알려준다.
   * 스트림을 전송하고 있는 동안에 화면에 정보를 표시할 수 있도록 처리한다.
   *
   * 스트림의 끝, 스트림 중지의 경우에도 보내준다
   *
   * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
   *
   * @param {string} st StreamEventStatus 타입
   * @desc Native To JS
   */
  sendStreamingEvent(st: string): void {
    let obj = JSON.parse(st);
    let s: StreamingStatus = new StreamingStatus();
    s = {...s, ...obj};
    let aif = this.mapClient.getAsyncInterface(s.streamId);
    if (!aif) {
      console.error('cannot find async interface for streamEvent',
          JSON.stringify(s));
    } else {
      s = {...s, interface: aif};
      let ifname = aif.interface + '.' + aif.operation;
      switch (ifname) {
        case 'Dialog.SpeechToSpeechTalk': {
          console.log(ifname, s);
          break;
        }
        case 'Dialog.SpeechToTextTalk': {
          console.log(ifname, s);
          break;
        }
        case 'Dialog.ImageToSpeechTalk': {
          console.log(ifname, s);
          break;
        }
        case 'Dialog.ImageToTextTalk': {
          console.log(ifname, s);
          break;
        }
      }
      // 스트림이 끝났으면 이를 버립니다.
      if (s.isEnd && s.isBreak) {
        this.mapClient.clearStreamId(s.streamId);
      }
      let handler = this.mapClient.handler;
      if (handler && handler.onStreamingEvent) {
        handler.onStreamingEvent(s);
      }
    }
  }


  /**
   * receiveDirective에 딸린 스트림을 수신하는 경우에는 Web에 알려준다.
   * 이것은 TTS 출력 중인 상태와 같이 스트림을 수신하고 있는 동아네
   * 화면에 정보를 표시할 수 있도록 처리한다.
   *
   * 스트림의 끝, 스트림 중지의 경우에도 보내준다
   *
   * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
   *
   * @param {string} st StreamEventStatus
   * @desc Native To JS
   */
  receiveStreamingDirective(st: string): void {
    let obj = JSON.parse(st);
    let s: StreamingStatus = new StreamingStatus();
    s = {...s, ...obj};
    let aif = this.mapClient.getAsyncInterface(s.streamId);
    if (!aif) {
      console.error('cannot find async interface for streamEvent',
          JSON.stringify(s));
    } else {
      s = {...s, interface: aif};
      let ifname = aif.interface + '.' + aif.operation;
      switch (ifname) {
        case 'SpeechSynthesizer.Play': {
          console.log(ifname, s);
          break;
        }
      }
      // 스트림이 끝났으면 이를 버립니다.
      if (s.isEnd && s.isBreak) {
        this.mapClient.clearStreamId(s.streamId);
      }
      let handler = this.mapClient.handler;
      if (handler && handler.onStreamingEvent) {
        handler.onStreamingEvent(s);
      }
    }
  }

  /**
   * Ping의 결과가 있으면 이를 반환한다.
   *
   * iOS의 경우에 백그라운드 앱은 네트워크를 사용할 수 없으므로 깨어나면 바로 자동으로 시작하도록 한다.
   *
   * setMapSetting() 에 의해서 pingEnabled = false로 주어지면, ping은 내부적으로 중지될 수 있다.
   *
   * @param {string} pong  PongResponse 수신받든 Pong 객체
   * @desc Native To JS
   */
  notifyPong(pong: string): void {
    console.log(pong);
  }

  // ---------------------------------------------------
  // MIC
  // ---------------------------------------------------

  /**
   * 마이크 상태를 반환한다.
   *
   * 1. 마이크 열림 성공, 또는 실패
   * 2. 최초 음성 레코딩이 시작될 때 알림
   * 3. 마이크 닫는 호출에 대한 응답을 반환한다.
   *
   * @param {string } st MicrophoneStatus type
   * @desc Native To JS
   */
  notifyMicrophoneStatus(st: string): void {
    let obj = JSON.parse(st);
    let s: MicrophoneStatus = new MicrophoneStatus();
    s = {...s, ...obj};
    let handler = this.mapClient.handler;
    if (handler && handler.onMicrophoneStatus) {
      handler.onMicrophoneStatus(s);
    }
  }


  // ---------------------------------------------------
  // SPEAKER
  // ---------------------------------------------------
  /**
   * 마이크 개방 후 상태를 알려줍니다.
   *
   * @param {string } ss SpeakerStatus 마이크 개방 상태
   * @desc Native To JS
   */
  notifySpeakerStatus(ss: string): void {
    let obj = JSON.parse(ss);
    let s: SpeakerStatus = new SpeakerStatus();
    s = {...s, ...obj};
    let handler = this.mapClient.handler;
    if (handler && handler.onSpeakerStatus) {
      handler.onSpeakerStatus(s);
    }
  }


  // ---------------------------------------------------
  // CAMERA
  // ---------------------------------------------------

  /**
   * 성공, 실패, 데이터 수집 등 카메라 상태를 반환한다.
   *
   * @param {string} cs CameraStatus type 카메라 상태
   * @desc Native to JS
   */
  notifyCameraStatus(cs: string): void {
    let obj = JSON.parse(cs);
    let s: CameraStatus = new CameraStatus();
    s = {...s, ...obj};
    let handler = this.mapClient.handler;
    if (handler && handler.onCameraStatus) {
      handler.onCameraStatus(s);
    }
  }


  // ---------------------------------------------------
  // GALLERY
  // ---------------------------------------------------
  /**
   * 갤러리 상태를 반환한다.
   *
   * @param {string} st GalleryStatus type
   */
  notifyGalleryStatus(st: string): void {
    let obj = JSON.parse(st);
    let s: GalleryStatus = new GalleryStatus();
    s = {...s, ...obj};
    let handler = this.mapClient.handler;
    if (handler && handler.onGalleryStatus) {
      handler.onGalleryStatus(s);
    }
  }


  // ---------------------------------------------------
  // LOCATION
  // ---------------------------------------------------
  /**
   * 위치정보가 구해지면 이를 저장합니다.
   *
   * @param {LocationStatus} st 구해진 위치 정보를 반환한다.
   */
  notifyLocation(st: string): void {
    let obj = JSON.parse(st);
    let s: LocationStatus = new LocationStatus();
    s = {...s, ...obj};
    let handler = this.mapClient.handler;
    if (!s.hasReason) {
      if (this.mapClient.location == null) {
        // 최초 위치 정보를 얻어온 경우 예외처리
        this.mapClient.location = s.location;
      } else {
        // 이후 위치 정보값이 변경된 경우에만 값을 대입
        if (this.mapClient.location.longitude !== s.location.longitude
            || this.mapClient.location.latitude !== s.location.latitude) {
          this.mapClient.location = s.location;
        }
      }
      // 위치 정보를 onLocationChanged 리스너로 전달
      if (handler && handler.onLocationChanged) {
        handler.onLocationChanged(s);
      }
    } else {
      if (handler && handler.onLocationChanged) {
        handler.onLocationChanged(s);
      }
    }
  }
}

/**
 * 랜던 문자열을 생성한다.
 *
 * @param {number} length 길이
 * @param {string} chars 쓰일 문자열
 * @return {string} 생성된 문자열
 */
function randomString(length: number, chars: string) {
  let result = '';
  for (let i = length; i > 0; --i)
    result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}


/**
 * 디버거를 통해서 MapClient의 동작을 모니터링 할 수 있는 API 구조
 */
export interface MapClientDebugger {
  /**
   * Stream DUMP
   * @param {string} dump
   */
  onEvent(dump: string): void;

  /**
   * Directive DUMP
   * @param {string} dump
   */
  onDirective(dump: string): void;
}


/**
 * CDK main class
 *
 * 이 클래스를 통해서 모든 함수 호출이 처리된다.
 */
export class MapClient implements Dialog, Authentication, M2uWebViewClientNative {
  /**
   * Native로 보낸 EventStream이 streaming 형식일 때 기억하기 위한 변수
   * native로 보내고 난후, `notifyEventStream` 호출을 통해서 정보가 오는데, 이때는 streamId 정보만 온다.
   * 어떤 event에 대한 정보인지를 알수가 없으므로 이를 기억한다.
   *
   * @type {Object}
   * @private
   */
  public _streams: any = {};
  /**
   * Native로 보낸 EventStream에 대해서 기억하는 변수
   * 각각 이벤트에 대한 응답이 오면 지운다.
   * @type {Object}
   * @private
   */
  private _operations: any = undefined;
  /**
   * native에서 JS를 호출할 때 사용하는 변수
   * native에서는 `window.m2uNativeForward.[XXXXX].recieveDirective` 와 같은 형식으로 호출한다.
   * 보안 때문에 내부 함수를 바꿔치기할 수 없도록 이름을 랜덤으로 생성한다.
   * @type {string}
   * @private
   */
  private _forwarderName: string;
  /**
   * Native의 설정값, ip, port 등의 값이 들어있다.
   */
  private _settings: MapSetting;
  /**
   * 실지로 native로 메시지를 전송하는 객체
   * @type {M2uWebViewClientNativeMobile}
   */
  private native: M2uWebViewClientNativeMobile = null;
  /**
   * native의 이벤트를 받아들여서 이를 local WEB javascript callback 함수를 호출해준다.
   * @type {NativeEventForward}
   */
  private forwader: NativeEventForward = null;
  /**
   * 현재 위치를 기억하고 있습니다.
   * @type {Location}
   * @private
   */
  private _location: Location = null;
  /**
   * 등록된 콜백 함수들
   * @type {MapDirectiveHandler}
   * @private
   */
  private _handler: MapDirectiveHandler = undefined;
  /**
   * 등록된 디버깅 함수들
   * @type {MapClientDebugger}
   * @private
   */
  _debug: MapClientDebugger = undefined;

  // 8바이트 추가적인 문자열의 길이
  private static suffixLength = 10;
  deviceSuffix: string = '0000000000';

  /**
   * `MapClient`로 새로 만듭니다.
   *
   * @param {MapSetting} settings Map 서버 접속 주소 등의 정보를 가지고 있습니다.
   * @param {string} deviceSuffix 호출을 구분하는 OPERATION ID를 구분하는 정보
   */
  constructor(settings: MapSetting, deviceSuffix: string) {
    // ANDROID
    if (window.m2uWebViewNative) {
      this.native = window.m2uWebViewNative;
    }
    // IOS
    else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
      this.native = new AppleM2uWebViewClientNative();
    }
    // 아무것도 아니면...
    else {
      this.native = new LocalM2uWebViewClientNative();
    }

    this.deviceSuffix = deviceSuffix.substr(0, MapClient.suffixLength);

    // operations들을 관리하는 map
    this._operations = {};

    //
    // 새로운 Forwader를 설치한다.
    //
    this.forwader = new NativeEventForward(this);
    // 늘 문자로 시작하도록 하고 길이는 20자로 한다.
    this._forwarderName =
        randomString(5,
            'abcdefghijklmnopqrstuvwxyz') +
        randomString(15,
            '1234567890abcdefghijklmnopqrstuvwxyz');
    if (window.m2uNativeForward === undefined)
      window.m2uNativeForward = {};
    window.m2uNativeForward[this._forwarderName] = this.forwader;

    if (window.mySecret === undefined)
      window.mySecret = {};
    window.mySecret = this._forwarderName;

    this._settings = settings;
  }

  /**
   * 설정을 가져온다.
   * @returns {MapSetting}
   */
  get settings(): MapSetting {
    return this._settings;
  }

  /**
   * 설정을 바꾼다.
   * @param {MapSetting} value
   */
  set settings(value: MapSetting) {
    this._settings = value;
    this._settings.notifyObjectPrefix = 'window.m2uNativeForward.' + this._forwarderName;
    this.setMapSetting(this._settings);
  }

  /**
   * 디바디스 정보를 설정한다.
   * @param {Device} value 변경할 디바이스 정보
   */
  set device(value: Device) {
    this._settings.device = value;
    this.setMapSetting(this._settings);
  }

  /**
   * 현재 디바이스 상태를 반환한다.
   *
   * @returns {Device} 현재 디바이스 상태
   */
  get device() {
    return this._settings.device;
  }

  /**
   * 인증 토큰을 반환한다.
   *
   * @returns {string}
   */
  get authToken() {
    return this._settings.authToken;
  }

  /**
   * 인증 토큰을 변경한다.
   * @param {string} value
   */
  set authToken(value: string) {
    this._settings.setAuthToken(value);
    this.setMapSetting(this._settings);
  }

  /**
   * 현재 위치
   * @returns {Location}
   */
  get location(): Location {
    return this._location;
  }

  /**
   * 로케이션 지정
   * @param {Location} value
   */
  set location(value: Location) {
    this._location = value;
  }

  /**
   * 현재 설정된 핸들러 반환
   * @returns {MapDirectiveHandler}
   */
  get handler(): MapDirectiveHandler {
    return this._handler;
  }

  /**
   * 핸들러 지정
   * @param {MapDirectiveHandler} handler
   */
  set handler(handler: MapDirectiveHandler) {
    this._handler = handler;
  }

  //
  // 인증에 대한 호출
  //
  /**
   * 인증을 호출합니다.
   *
   * @param {SignInPayload} payload 인증 처리 요청 데이터
   */
  signIn(payload: SignInPayload): void {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Authentication',
        operation: 'SignIn'
      }
    };

    // 인증 시점에는 반드시 이 토큰으로 사용해야 합니다.
    //this.authToken = '1602a950-e651-11e1-84be-00145e76c700';
    this.authToken = '';

    // 페이로드 지정
    if (payload) {
      // 사용자의 device 정보를 업데이트 합니다.
      payload.device = this.settings.device;
      es.payload = {...es.payload, ...payload};
    }
    console.log(JSON.stringify(es));
    this.sendEvent(es);

  }

  /**
   * SignOut을 호출합니다.
   * 내부에 들고 있는 authToken 정리합니다.
   * @returns {string} operation id
   */
  signOut(): string {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Authentication',
        operation: 'SignOut'
      }
    };

    // 현재 authToken 값을 전달하여 SignOut을 진행합니다
    // TODO CDK에서 사용자 키 (optional)값은 전달해야 할까요?
    let payload = new SignOutPayload(this.authToken);
    es.payload = {...es.payload, ...payload};

    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;
  }

  /**
   * 다중 팩터 인증을 처리합니다.
   *
   * @param {MultiFactorVerifyPayload} payload 다중 팩터 인증을 위한 처리 데이터
   * @returns {string} operation id
   */
  multiFactorVerify(payload: MultiFactorVerifyPayload): string {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Authentication',
        operation: 'MultiFactorVerify'
      }
    };

    // 페이로드 지정
    if (payload)
      es.payload = {...es.payload, ...payload};
    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;
  }

  /**
   * 사용자 속성 정의
   * 인증된 사용자만 이 호출을 진행할 수 있다.
   * 바꾸고 싶은 값만 넣을 수 있다.
   * 응답으로는 모든 설정정보를 다 반환한다.
   *
   * @param {UserSettings} userSettings 사용자 속성 정의 데이터
   * @returns {string}
   * @constructor
   */
  updateUserSettings(payload: UserSettings): string {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Authentication',
        operation: 'UpdateUserSettings'
      }
    };

    // payload 지정
    if (payload)
      es.payload = {...es.payload, ...payload};
    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;
  }

  /**
   * 사용자 속성 조회
   * 인증된 사용자만 이 호출을 진행할 수 있다.
   * 설정정보를 조회한다.
   *
   * @param {UserKey} userKey 사용자 속성 조회를 위한 키
   * @returns {string}
   * @constructor
   */
  getUserSettings(payload: UserKey): string {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Authentication',
        operation: 'GetUserSettings'
      }
    };

    // payload 지정
    if (payload)
      es.payload = {...es.payload, ...payload};
    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;
  }


  //
  // 대화에 대한 호출
  //


  /**
   * 새로운 세션을 엽니다.
   *
   * M2U 시스템은 세션이 열리자 마자 새로운 발화를 내보내개 됩니다.
   *
   * @param {OpenUtter} openUtter
   *   `openUtter.chatbot`이 비어있으면 Device.
   *   `openUtter.utter`는 기본값이 비어있습니다.
   *   `openUtter.meta`에는 원하는 추가적인 정보를 넣어줄 수도 있습니다.
   * @returns {string} operation id
   */
  public open(openUtter: OpenUtter): string {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Dialog',
        operation: 'Open'
      }
    };

    // OpenUtter 값을 지정합니다.
    if (openUtter)
      es.payload = {...es.payload, ...openUtter};
    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;
  }


  /**
   * 음성으로 보내고 음성으로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * - SpeechRecognizer.Recognizing
   * - SpeechRecognizer.Recognized
   * - Dialog.Processing
   * - SpeechSynthesizer.Play
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   * - Microphone.ExpectSpeech
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
   * @returns {string} operation id
   */
  public speechToSpeechTalk(param?: SpeechRecognitionParam): string {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Dialog',
        operation: 'SpeechToSpeechTalk',
        streaming: true
      }
    };

    // param
    if (!param) {
      let srParam = new SpeechRecognitionParam();
      srParam.lang = Lang.ko_KR;
      srParam.model = 'baseline';
      srParam.encoding = AudioEncoding.LINEAR16;
      srParam.sampleRate = 16000;
      srParam.singleUtterance = true;
      es.param._speechRecognitionParam = srParam;
    } else {
      es.param._speechRecognitionParam = param;
    }

    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;
  }

  /**
   * 음성으로 보내고 텍스트로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * - SpeechRecognizer.Recognizing
   * - SpeechRecognizer.Recognized
   * - Dialog.Processing
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
   * @returns {string} operation id
   */
  public speechToTextTalk(param?: SpeechRecognitionParam): string {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Dialog',
        operation: 'SpeechToTextTalk',
        streaming: true
      }
    };

    // param
    if (!param) {
      let srParam = new SpeechRecognitionParam();
      srParam.lang = Lang.ko_KR;
      srParam.model = 'baseline';
      srParam.encoding = AudioEncoding.LINEAR16;
      srParam.sampleRate = 16000;
      srParam.singleUtterance = true;
      es.param._speechRecognitionParam = srParam;
    } else {
      es.param._speechRecognitionParam = param;
    }

    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;

  }

  /**
   * 텍스트로 보내고 텍스트로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
   * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
   *
   * - Dialog.Processing
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {Utter} utter 이 값은 필수입니다.
   * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
   * @returns {string} operation id
   */
  public textToTextTalk(utter: Utter): string {
    if (utter === null)
      throw new Error('invalid utter');

    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Dialog',
        operation: 'TextToTextTalk'
      }
    };
    // OpenUtter 값을 지정합니다.
    if (utter)
      es.payload = {...es.payload, ...utter};
    console.log(JSON.stringify(es));

    this.sendEvent(es);
    return es.operationSyncId;
  }

  /**
   * Client가 명시적으로 interface, operation 명을 지정할 수 있는 디렉티브 생성 함수
   *
   * 예1) MAP으로 부터 Launcher.Authorize 를 수신 받았다면
   * Client는 interface를 Launcher, operation은 Authorized로 설정하여 해당 함수를 호출한다
   * 또는 interface를 Launcher, operation은 AuthorizeFailed로 설정하여 해당 함수를 호출한다
   *
   * 예2) MAP으로 부터 Launcher.FillSlots 를 수신 받았다면
   * Client는 interface를 Launcher, operation은 SlotstFilled로 설정하여 해당 함수를 호출한다
   *
   * @param {string} interfaceName
   * @param {string} operationName
   * @param {EventPayload} eventPayload
   * @param {DialogAgentForwarderParam} param dialog agent forwarder param
   * @returns {string}
   */
  public event(interfaceName: string,
               operationName: string,
               eventPayload: EventPayload,
               param: DialogAgentForwarderParam): string {

    if (eventPayload === null) {
      throw new Error('invalid eventPayload');
    }
    if (param === null) {
      throw new Error('invalid param');
    }

    let evParam = new EventParam();
    evParam._dialogAgentForwarderParam = param;
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: interfaceName,
        operation: operationName,
      }
    };

    if (eventPayload) {
      es.payload = {...es.payload, ...eventPayload};
    }
    es.param = evParam;

    console.log(JSON.stringify(es));

    this.sendEvent(es);
    return es.operationSyncId;
  }

  /**
   * 텍스트로 보내고 음성으로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
   * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
   *
   * - Dialog.Processing
   * - SpeechSynthesizer.Play
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {Utter} utter 이 값은 필수입니다.
   * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
   * @returns {string} operation id
   */
  public textToSpeechTalk(utter: Utter): string {
    if (utter === null)
      throw new Error('invalid utter');

    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Dialog',
        operation: 'TextToSpeechTalk'
      }
    };

    // OpenUtter 값을 지정합니다.
    if (utter)
      es.payload = {...es.payload, ...utter};
    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;
  }

  /**
   * 이미지를 보내고 텍스트로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
   * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
   *
   * - ImageDocument.Recognized
   * - Dialog.Processing
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
   *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
   * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.

   * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
   * @returns {string} operation id
   */
  public imageToTextTalk(imageUrl: string, param?: ImageRecognitionParam): string {
    if (imageUrl === null)
      throw new Error('invalid utter');

    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Dialog',
        operation: 'ImageToTextTalk',
        streaming: true
      }
    };

    // param
    if (!param) {
      es.param._imageRecognitionParam = new ImageRecognitionParam({
        lang: Lang.ko_KR,
        imageFormat: ImageFormat.JPEG,
        width: 1024,
        height: 768,
        refVertexX: 0.104167,
        refVertexY: 0.138889,
        symmCrop: true
      });
    } else {
      es.param._imageRecognitionParam = param;
    }
    console.log(JSON.stringify(es));
    this.sendEvent(es, imageUrl);
    return es.operationSyncId;
  }

  /**
   * 이미지를 보내고 음성으로 받는 대화를 시작합니다.
   *
   * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
   *
   * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
   * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
   *
   * - ImageDocument.Recognized
   * - Dialog.Processing
   * - SpeechSynthesizer.Play
   * - View.RenderText
   * - View.RenderCard
   * - View.RenderHidden
   * - Microphone.ExpectSpeech
   *
   * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
   *
   * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
   *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
   * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.

   * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
   * @returns {string} operation id
   */
  public imageToSpeechTalk(imageUrl: string, param?: ImageRecognitionParam): string {
    if (imageUrl === null)
      throw new Error('invalid utter');

    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Dialog',
        operation: 'ImageToSpeechTalk',
        streaming: true
      }
    };

    // param
    if (!param) {
      es.param._imageRecognitionParam = new ImageRecognitionParam({
        lang: Lang.ko_KR,
        imageFormat: ImageFormat.JPEG,
        width: 1024,
        height: 768,
        refVertexX: 0.104167,
        refVertexY: 0.138889,
        symmCrop: true
      });
    } else {
      es.param._imageRecognitionParam = param;
    }
    console.log(JSON.stringify(es));
    this.sendEvent(es, imageUrl);
    return es.operationSyncId;
  }

  /**
   * 명시적으로 세션을 닫습니다.
   * @returns {string} operation id
   */
  public close(): string {
    let es = new EventStream();
    es.interface = {
      ...es.interface, ...{
        interface: 'Dialog',
        operation: 'Close'
      }
    };
    console.log(JSON.stringify(es));
    this.sendEvent(es);
    return es.operationSyncId;
  }

  //
  // CALL NATIVE DEVICES
  //
  /**
   * map setting 정보를 넣어준다.
   *
   * @param {MapSetting} setting 변경할 map setting 정보
   * @returns {boolean} 새로운 map setting을 넣어준다.
   */
  setMapSetting(setting: MapSetting): boolean {
    // 혹시라도 누락되면 꼬이므로 갱신해준다.
    setting.notifyObjectPrefix = 'window.m2uNativeForward.' + this._forwarderName;
    let temp = JSON.stringify(setting);
    return this.native.setMapSetting(temp);
  }

  /**
   * 스트립 ID를 정리합니다.
   */
  clearStreamId(strId: string): void {
    console.log('clearing opid ', strId);
    delete this._streams[strId];
  }

  /**
   * operation에 대한 정보 삭제
   * @param {string} opid operationId
   */
  clearOperationId(opid: string): void {
    console.log('clearing opid ', opid);
    delete this._operations[opid];
  }

  /**
   * operation에 대한 호출 인터페이스 정보를 가져옵니다.
   * @param {string} streamId 인터페이스 ID를 streamID입니다.
   * @return {AsyncInterface}
   */
  getAsyncInterface(streamId: string): AsyncInterface {
    return <AsyncInterface>this._streams[streamId];
  }

  /**
   * 내부적으로 이벤트를 전송합니다.
   *
   * 이 메소드는 직접 호출할 수도 있지만, 다른 wrapper 함수를 통해서 호출하는 것이
   * 바람직합니다.
   *
   * 매 전송시마다 새로운 streamId, operationSyncId를 생성해서 전달합니다.
   * `operationSyncId`는 UUID + deviceSuffix를 사용하여 전달합니다.
   *
   * @param {EventStream} es 전송할 이벤트
   * @param {string} imageUrl 옵셔널 이미지 URL
   */
  sendEvent(es: EventStream, imageUrl?: string): void {
    // uuid
    es.streamId = UUID.generate();
    es.operationSyncId = UUID.generateAsBase64().substr(0, 22) +
        this.deviceSuffix;

    // 보낸 메시지 ..
    this._operations[es.operationSyncId] = es.interface;
    if (es.interface.streaming) {
      this._streams[es.streamId] = es.interface;
    }

    if (this._location) {
      let ec = new EventContext();
      ec.setLocation(this._location);
      es.contexts.push(ec);
    }

    let temp = JSON.stringify(es);
    this.native.sendEvent(temp, imageUrl);
    if (this._debug && this._debug.onEvent)
      this._debug.onEvent(JSON.stringify(es, replacer, 2));
  }

  /**
   * 마이크를 열어 달라고 하는 이벤트
   *
   * @param {MicrophoneParam} param 개방 옵션
   * @desc JS to Native
   */
  openMicrophone(param: MicrophoneParam): void {
    let temp = JSON.stringify(param);
    this.native.openMicrophone(temp);
  }

  /**
   * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
   *
   * @desc JS To Native
   */
  closeMicrophone(): void {
    this.native.closeMicrophone();
  }

  /**
   * 명시적으로 스피커를 닫는다.
   * @desc JS To Native
   */
  closeSpeaker(): void {
    this.native.closeSpeaker();
  }

  /**
   * 카메라를 엽니다.
   *
   * @param {CameraParam} param
   * @desc JS To Native
   */
  openCamera(param: CameraParam): void {
    let temp = JSON.stringify(param);
    this.native.openCamera(temp);
  }

  /**
   * 명시적으로 카메라를 닫는다.
   *
   * @desc JS To Native
   */
  closeCamera(): void {
    this.native.closeCamera();
  }

  /**
   * 갤러리를 오픈한다.
   *
   * @param {GalleryParam} param
   */
  openGallery(param: GalleryParam): void {
    let temp = JSON.stringify(param);
    this.native.openGallery(temp);
  }

  /**
   * 명시적으로 Gallery를 닫는다.
   */
  closeGallery(): void {
    this.native.closeGallery();
  }

  /**
   *
   * 매번 현재 단말의 위치를 구해와라.
   *
   * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
   * EventStream의 context 정보에 적재하여 전송한다.
   *
   * @desc JS to Native
   */
  getLocation(): void {
    this.native.getLocation();
  }

  /**
   *
   * Phone Number 정보는 JS에서 필요시 호출한다.
   *
   * @desc JS to Native
   */
  getPhoneNumber(): any {
    console.log('call getPhoneNumber');
    var tempPhoneNum = this.native.getPhoneNumber();
    console.log('tempPhoneNum :' + tempPhoneNum);

    return tempPhoneNum;
  }
}

let seen: any = [];

/**
 * JSON.stringify() circular 참조를 호출할 때 오동작을 제거하는 함수
 */
export function clearReplacer() {
  seen = [];
}

/**
 * JSON.stringify()를 호출할 때, circular 참조를 무시하도록 하는 함수
 *
 * @param key 키
 * @param value 값
 * @return {any} 응답 데이터
 */
export function replacer(key: any, value: any) {
  if (value != null && typeof value == "object") {
    if (seen.indexOf(value) >= 0) {
      return;
    }
    seen.push(value);
  }
  return value;
}

