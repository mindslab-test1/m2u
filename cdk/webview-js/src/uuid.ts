/**
 * Fast UUID generator, RFC4122 version 4 compliant.
 * @author Jeff Ward (jcward.com).
 * @license MIT license
 * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
 **/
export class UUID {
  constructor() {
  }


  private static sLut: string[] = [];
  private static lookup: string[] = [];

  /**
   * UUID initialize
   */
  static initialize() {
    // Initialization
    for (let i = 0; i < 256; i++) {
      UUID.sLut[i] = (i < 16 ? '0' : '') + (i).toString(16);
    }
    // BASE64 URL 스펙, RFC7515 를 기준으로 사용한다.
    // 여기서 만들어진 결과를 web url에서 쓰이고 파일이름을 생성할 때에도 쓰이기 때문이다.
    const code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
    for (let i = 0, len = code.length; i < len; ++i) {
      UUID.lookup[i] = code[i];
    }
  }

  /**
   * Base64 encode
   *
   * @param {number} number를 Base64 인코딩
   * @returns {string}
   */
  private static tripletToBase64(num: number): string {
    return UUID.lookup[num >> 18 & 0x3F]
        + UUID.lookup[num >> 12 & 0x3F]
        + UUID.lookup[num >> 6 & 0x3F]
        + UUID.lookup[num & 0x3F];
  }

  /**
   * Combine the three bytes into a single integer
   *
   * @param {Uint8Array} uint8 입력 Byte Array
   * @param {number} start 시작 위치
   * @param {number} end 종료 위치
   * @returns {string}
   */
  private static encodeChunk(uint8: Uint8Array, start: number, end: number): string {
    let tmp;
    let output: string[] = [];
    for (let i = start; i < end; i += 3) {
      tmp = ((uint8[i] << 16) & 0xFF0000) + ((uint8[i + 1] << 8) & 0xFF00) + (uint8[i + 2] & 0xFF);
      output.push(this.tripletToBase64(tmp))
    }
    return output.join('')
  }

  /**
   * Takes a byte array and returns string
   *
   * @param {Uint8Array} uint8 String으로 변환할 Byte Array
   * @returns {string}
   */
  private static fromByteArray(uint8: Uint8Array): string {
    let tmp;
    let len = uint8.length;
    let extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes
    let output = '';
    let parts = [];
    let maxChunkLength = 16383; // must be multiple of 3

    //go through the array every three bytes, we'll deal with trailing stuff later
    for (let i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
      parts.push(this.encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)));
    }

    //pad the end with zeros, but make sure to not forget the extra bytes
    if (extraBytes === 1) {
      tmp = uint8[len - 1];
      output += UUID.lookup[tmp >> 2];
      output += UUID.lookup[(tmp << 4) & 0x3F]
      output += '=='
    } else if (extraBytes === 2) {
      tmp = (uint8[len - 2] << 8) + (uint8[len - 1])
      output += UUID.lookup[tmp >> 10]
      output += UUID.lookup[(tmp >> 4) & 0x3F]
      output += UUID.lookup[(tmp << 2) & 0x3F]
      output += '='
    }
    parts.push(output);
    return parts.join('')
  }

  /**
   * Generate a Base64 to string
   *
   * @returns {string} Base64 인코딩된 값을 String으로 전달
   */
  static generateAsBase64(): string {
    let d0 = Math.random() * 0xffffffff | 0;
    let d1 = Math.random() * 0xffffffff | 0;
    let d2 = Math.random() * 0xffffffff | 0;
    let d3 = Math.random() * 0xffffffff | 0;
    let raw: Uint8Array = new Uint8Array(16);
    raw[0] = d0 & 0xff;
    raw[1] = d0 >> 8 & 0xff;
    raw[2] = d0 >> 16 & 0xff;
    raw[3] = d0 >> 24 & 0xff;
    raw[4] = d1 & 0xff;
    raw[5] = d1 >> 8 & 0xff;
    raw[6] = d1 >> 16 & 0x0f | 0x40;
    raw[7] = d1 >> 24 & 0xff;
    raw[8] = d2 & 0x3f | 0x80;
    raw[9] = d2 >> 8 & 0xff;
    raw[10] = d2 >> 16 & 0xff;
    raw[11] = d2 >> 24 & 0xff;
    raw[12] = d3 & 0xff;
    raw[13] = d3 >> 8 & 0xff;
    raw[14] = d3 >> 16 & 0xff;
    raw[15] = d3 >> 24 & 0xff;
    return UUID.fromByteArray(raw);
  }

  /**
   * Generate a UUID to string
   *
   * @returns {string} UUID 값 생성
   */
  static generate(): string {
    const lut = UUID.sLut;
    let d0 = Math.random() * 0xffffffff | 0;
    let d1 = Math.random() * 0xffffffff | 0;
    let d2 = Math.random() * 0xffffffff | 0;
    let d3 = Math.random() * 0xffffffff | 0;
    return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
        lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
        lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
        lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff];
  }
}

UUID.initialize();
