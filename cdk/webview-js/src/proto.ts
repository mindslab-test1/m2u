// ------------------------------------------------------------------------
// GOOGLE/PROTOBUF
// ------------------------------------------------------------------------

import {UUID} from "./uuid";

declare global {
  function __assign(t:Object, ...objs:Object[]): Object;
}

/**
 * 빈 오브젝트 형태, VOID
 */
export class Empty {
  constructor() {

  }
}

/**
 * 기간
 */
export class Duration {

  /**
   * Signed seconds of the span of time. Must be from -315,576,000,000
   * to +315,576,000,000 inclusive.
   */
  seconds: number;

  /**
   * Signed fractions of a second at nanosecond resolution of the span
   * of time. Durations less than one second are represented with a 0
   * `seconds` field and a positive or negative `nanos` field. For durations
   * of one second or more, a non-zero value for the `nanos` field must be
   * of the same sign as the `seconds` field. Must be from -999,999,999
   * to +999,999,999 inclusive.
   */
  nanos: number;

  /**
   * 생성자
   */
  constructor() {
    this.seconds = 0;
    this.nanos = 0;
  }
}

/**
 * NOT USED, used as Date()
 */
export class Timestamp {
  /**
   * Represents seconds of UTC time since Unix epoch
   * 1970-01-01T00:00:00Z. Must be from from 0001-01-01T00:00:00Z to
   * 9999-12-31T23:59:59Z inclusive.
   * int64 seconds = 1;
   */
  public seconds: number;

  /**
   * Non-negative fractions of a second at nanosecond resolution. Negative
   * second values with fractions must still have non-negative nanos values
   * that count forward in time. Must be from 0 to 999,999,999
   * inclusive.
   */
  public nanos: number;

  /**
   * 생성자,
   * 자동으로 현재의 시간을 구한다.
   */
  constructor() {
    this.getCurrentTimestamp();
  }

  /**
   * 현재의 시간을 구한다.
   */
  public getCurrentTimestamp(): void {
    let ts = Date.now();
    this.seconds = Math.floor(ts / 1000);
    this.nanos = Math.floor(ts % 1000) * 1000000;
  }
}


/**
 * 일반적인 비정형 데이터를 담을 수 있는 그릇입니다.
 */
export class Struct {
  /**
   * 생성자
   */
  constructor() {
  }
}

// ------------------------------------------------------------------------
// COMMON/M2U/DIALOG
// ------------------------------------------------------------------------

// ----------------------------------------------------------------------
// 사용자 발화: Utter
// ----------------------------------------------------------------------

/**
 * 입력 유형
 */
export enum InputType {
  /**
   * STT를 통한 입력
   */
  SPEECH = "SPEECH",
  /**
   * 타이핑에 의한 입력
   */
  KEYBOARD = "KEYBOARD",
  /**
   * CARD 등에 지정된 텍스트를 보내는 경우
   */
  TOUTCH = "TOUTCH",
  /**
   * 이미지 인식의 결과를 보내는 경우, 이미지 애노테이션을 통한 처리
   */
  IMAGE = "IMAGE",
  /**
   *  OCR 이미지 인식의 결과를 보내는 경우
   */
  IMAGE_DOCUMENT = "IMAGE_DOCUMENT",
  /**
   * 이미지 인식의 결과를 보내는 경우
   */
  VIDEO = "VIDEO",
  /**
   * 세션을 생성하는 등의 최초 이벤트, 프로그램 등의 명시적인 이벤트로 처리하는 방식
   */
  OPEN_EVENT = "OPEN_EVENT"
}

/**
 * 사용자의 입력 데이터
 * 입력의 유형은 매우 다양할 수 있습니다.
 */
export class Utter {
  /**
   * 사용자의 입력 메시지
   */
  public utter: string;

  /**
   * 입력 유형
   */
  public inputType: InputType;

  /**
   * 언어
   */
  public lang: Lang;

  /**
   * 현재의 발화에 대한 추가적인 메시지가 존재할 수 있습니다.
   * ETRI STT의 경우에는 이 메시지가 없습니다.
   */
  public altUtters: string[];

  /**
   * 추가적인 입력 데이터
   * IMAGE, IMAGE_DOCUMENT. VIDEO, EVENT의 경우
   * 추가적인 데이터 메시지 데이터를 정의해서 보낼 수 있습니다.
   * 이미지 문서 인식의 경우에는 JSON 형태의 meta 데이터를
   * 여기에 담아서 넣어야 합니다.
   */
  public meta: Struct;

  /**
   * 생성자
   */
  constructor() {
    this.utter = '';
    this.inputType = InputType.KEYBOARD;
    this.lang = Lang.ko_KR;
    this.altUtters = [];
    this.meta = new Struct();
  }
}

/**
 * 명시적으로 새로운 챗봇을 엽니다. 새로운 대화 세션을 생성합니다.
 */
export class OpenUtter {
  /**
   * 사용할 챗봇의 이름
   */
  chatbot: string;
  /**
   * 명시적으로 지정할 SKILL 이름 (이 스펙이 꼭 필요한가?)
   */
  skill: string;

  /**
   * 초기에 보내줄 발화
   * 기본적으로 빈 문자열이 필요하다.
   */
  utter: string;
  /**
   * 추가적인 입력 데이터
   * OPEN 시점에 대화 메시지 외에 추가할 데이터
   */
  meta: Struct;

  /**
   * 생성자
   */
  constructor() {
    this.chatbot = '';
    this.skill = '';
    this.utter = '';
    this.meta = new Struct();
  }
}

export class Event {
  /**
   * Event interface
   */
  interface: string;

  /**
   * Event operation
   */
  operation: string;

  /**
   * DA에서 단말에게 전달되는 param
   */
  daForwardParam: DialogAgentForwarderParam;

  /**
   * Event payload 정의
   */
  eventPayload: EventPayload;

  constructor() {
    this.interface = '';
    this.operation = '';
    this.daForwardParam = new DialogAgentForwarderParam();
    this.eventPayload = new EventPayload();
  }
}

/**
 * Event 관련 payload
 */
export class EventPayload {
  private launcherAuthorized: LauncherAuthorized;

  private launcherAuthorizeFailed: LauncherAuthorizeFailed;

  private launcherSlotsFilled: LauncherSlotsFilled;

  private audioPlayerPlay: AudioPlayerPlay;

  private launcherDocumentRead: LauncherDocumentRead;

  // EventPayload 생성자
  constructor() {
    this.launcherAuthorized = undefined;
    this.launcherAuthorizeFailed = undefined;
    this.launcherSlotsFilled = undefined;
    this.audioPlayerPlay = undefined;
    this.launcherDocumentRead = undefined;
  }

  // EventPayload getter, setter 정의
  get _launcherAuthorized(): LauncherAuthorized {
    return this.launcherAuthorized;
  }

  set _launcherAuthorized(value: LauncherAuthorized) {
    this.launcherAuthorizeFailed = undefined;
    this.launcherSlotsFilled = undefined;
    this.audioPlayerPlay = undefined;
    this.launcherDocumentRead = undefined;
    this.launcherAuthorized = value;
  }

  get _launcherAuthorizeFailed(): LauncherAuthorizeFailed {
    return this.launcherAuthorizeFailed;
  }

  set _launcherAuthorizeFailed(value: LauncherAuthorizeFailed) {
    this.launcherAuthorized = undefined;
    this.launcherSlotsFilled = undefined;
    this.audioPlayerPlay = undefined;
    this.launcherDocumentRead = undefined;
    this.launcherAuthorizeFailed = value;
  }

  get _launcherSlotsFilled(): LauncherSlotsFilled {
    return this.launcherSlotsFilled;
  }

  set _launcherSlotsFilled(value: LauncherSlotsFilled) {
    this.launcherAuthorized = undefined;
    this.launcherAuthorizeFailed = undefined;
    this.audioPlayerPlay = undefined;
    this.launcherDocumentRead = undefined;
    this.launcherSlotsFilled = value;
  }

  get _audioPlayerPlay(): AudioPlayerPlay {
    return this.audioPlayerPlay;
  }

  set _audioPlayerPlay(value: AudioPlayerPlay) {
    this.launcherAuthorized = undefined;
    this.launcherAuthorizeFailed = undefined;
    this.launcherSlotsFilled = undefined;
    this.launcherDocumentRead = undefined;
    this.audioPlayerPlay = value;
  }

  get _launcherDocumentRead(): LauncherDocumentRead {
    return this.launcherDocumentRead;
  }

  set _launcherDocumentRead(value: LauncherDocumentRead) {
    this.launcherAuthorized = undefined;
    this.launcherAuthorizeFailed = undefined;
    this.launcherSlotsFilled = undefined;
    this.audioPlayerPlay = undefined;
    this.launcherDocumentRead = value;
  }
}

// ------------------------------------------------------------------------
// M2U/COMMON/DEVICE
// ------------------------------------------------------------------------

/**
 * 단말의 처리 능력
 */
export class Capability {
  /**
   * OUTPUT: 텍스트를 출력할 수 있다.
   */
  public supportRenderText: boolean = false;
  /**
   * OUTPUT: 카드를 처리할 수 있다.
   */
  public supportRenderCard: boolean;
  /**
   * OUTPUT: 음성합성을 처리할 수 있다.
   */
  public supportSpeechSynthesizer: boolean;
  /**
   * OUTPUT: 오디오를 출력할 수 있다.
   */
  public supportPlayAudio: boolean;
  /**
   * OUTPUT: 비디오를 출력할 수 있다.
   */
  public supportPlayVideo: boolean;
  /**
   * OUTPUT: 행위를 처리할 수 있다.
   */
  public supportAction: boolean;
  /**
   * OUTPUT: 소스를 움직일 수 있다.
   */
  public supportMove: boolean;
  /**
   * INPUT: 음성입력 대기를 처리할 수 있다.
   */
  public supportExpectSpeech: boolean;

  /**
   * 생성자
   */
  constructor() {
    this.supportRenderText = true;
    this.supportRenderCard = true;
    this.supportSpeechSynthesizer = true;
    this.supportPlayAudio = false;
    this.supportAction = true;
    this.supportMove = false;
    this.supportExpectSpeech = true;
  }
}

/**
 * 장치
 */
export class Device {
  /**
   * 장치를 유일하게 식별하는 공유번호,
   * 장치에서 전송할 경우에는 반드시 서로 다른 일련번호를 가져야 합니다.
   * 모든 장치는 하나의 서비스에서 절대적으로 유일한 일련번호를 가지고 있어야 한다
   * 이 정보는 장치별 세션을 생성하는데 사용된다.
   *
   * 웹의 경우, 최초 접근하면 강제로 생성해주는 COOKIE 같은 개념
   * 서로 다른 PC와 브라우저마다 따로 존재해야 햔다.
   */
  public id: string;
  /**
   * 장치의 타입은 문자열로 정의되면, 서비스에 따라서 매우 상이하게 정의할 수 있습니다.
   * 예시) 엡, 안드로이드, IOS, 봇, 스피커, PC. 카카오, 페이스북
   * 접근하는 클라이언트의 API 접속 방법을 분리하는 것이다.
   * 모델 개념을 합친다.
   */
  public type: string;
  /**
   * *optional* 장치의 버전 정보, 장치의 SW나 MODEL과는 다른 개념입니다.
   */
  public version: string;
  /**
   * 서비스 접근 경로
   * *optional* 사이트별로 추가적인 정보가 필요할 수도 있다.
   * 내부적인 업무 구분 용도로 사용될 수 있다.
   */
  public channel: string;
  /**
   * 장치의 처리 능력
   *
   * input 능력
   *   mic, camera_image, camera_movie, keyboard, pointing
   * output 능력
   *   display_text, display_image, speaker, action, move, speaker
   */
  public support: Capability;
  /**
   * *optional* 장치의 현재 시간 정보
   */
  public timestamp: Date;

  /**
   * *optional* 장치의 timezone 정보
   */
  public timezone : string;
  /**
   * *optional* 장치의 메타 데이터 정보
   */
  public meta: Struct;

  /**
   * 생성자
   */
  constructor() {
    this.id = UUID.generate();
    this.type = '';
    this.version = '';
    this.channel = '';
    this.support = new Capability();
    this.timestamp = new Date();
    if ( typeof Intl == "undefined" ) {
        this.timezone = 'KST+9';
    } else {
        this.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    }
    this.meta = new Struct();
  }
}

// ------------------------------------------------------------------------
// M2U/COMMON/DIRECTIVE
// ------------------------------------------------------------------------

// ----------------------------------------------------------------------
// 디렉티브
// ----------------------------------------------------------------------

/**
 * DA에서 단말로 내려보내주는 PAYLOAD
 * 아바타 출력을 위한 페이로드
 */
export class AvatarSetExpressionPayload {
  /**
   * 아바타 출력 장치 정의
   * target에 어느 장치에 출력을 할지 정의한다
   */
  target: string;
  /**
   * 화면에 표현데이터
   */
  expression: string;
  /**
   * 표현 데이터에 대한 설명
   */
  desciption: string;

  /**
   * 생성자
   */
  constructor() {
    this.target = '';
    this.expression = '';
    this.desciption = '';
  }
}

/**
 * 오디오 출력을 위한 페이로드
 * 사용자에게 오디오 스트림또는 오디오 파일을 스피커로 출력하도록 지시한다.
 */
export class AudioPlayPayload {
  //재생할 오디오 스트림의 정보
  stream: Stream;
  // 오디오의 부가 정보 (audio의 title, subtitle, art, background_image)
  metadata : Metadata;
}

/**
 * 오디오 스트림의 정보
 */
export class Stream {
  // 오디오의 id
  id: string;
  // 오디오 위치
  url: string;
  // 오디오의 재생 시간
  playTime: Duration;

  constructor() {
    this.id = '';
    this.url = '';
    this.playTime = new Duration();
  }
}

/**
 * 오디오 스트림의 정보
 */
export class Metadata {
  // 오디오의 title
  title: string;
  // 오디오의 subtitle 정보
  subtitle: string;
  // 오디오의 artist 정보
  art: Sources;
  // 오디오의 backgroundImage 정보
  backgroundImage: Sources;

  constructor() {
    this.title = '';
    this.subtitle = '';
  }
}

/**
 * 부가 정보의 위치
 */
export class Sources {
  // 부가정보 위치
  urls: string[];

  constructor() {
    this.urls = [];
  }
}


/**
 * 오디오 출력의 중지
 */
export class AudioStopPayload {
  id: string;
  reason: string;

  constructor() {
    this.id = '';
    this.reason = '';
  }
}

/**
 * 비디오 출력을 위한 페이로드
 */
export class VideoPlayPayload {
  id: string;
  title: string;
  url: string;
  playTime: Duration;

  constructor() {
    this.id = '';
    this.title = '';
    this.url = '';
    this.playTime = new Duration();
  }
}


/**
 * 지도표시를 위한 페이로드
 */
export class LauncherViewMapPayload {
  /**
   * 검색범위, 반경, 반경 1km 이내 등과 같은 자연어 표현식
   */
  searchRange: string;
  /**
   * 검색 목적물: ATM, 지점
   */
  searchObject: string;
  /**
   * 검색어 : 영등포지점
   */
  searchKeyword: string;

  /**
   * 생성자
   */
  constructor() {
    this.searchRange = '';
    this.searchObject = '';
    this.searchKeyword = '';
  }
}

// ------------------------------------------------------------------------
// M2U/COMMON/EVENT
// ------------------------------------------------------------------------

/**
 * 단말에 대화 처리에 대한 일부 데이터 위임을 하는 경우에 대한 처리
 * 모든 처리를 SKIP하기 위해서 최대한 상세한 정보를 넘겨주도록 한다.
 */

/**
 * 인증 처리를 위한 페이로드
 */
export class LauncherAuthorizePayload {
  /**
   * 사용할 인증 제공자
   */
  provider: string;
  /**
   * 인증 방법
   */
  method: string;

  /**
   * 인증을 위한 추가파라미터
   */
  meta: Struct;

  /**
   * 생성자
   */
  constructor() {
    this.provider = '';
    this.method = '';
    this.meta = new Struct();
  }
}

/**
 * 단말 정보 획득 위한 페이로드
 */
export class LauncherFillSlotsPayload {

  /**
   * 채워진 슬롯 정보들
   */
  filledSlots: Struct;
  /**
   * 요청하는 슬롯 정보들
   */
  requestingSlots: string[];

  constructor() {
    this.filledSlots = new Struct();
    this.requestingSlots = [];
  }
}

/**
 * 문서를 읽은 결과 처리하기 위한 페이로드
 */
export class LauncherReadDocumentPayload {
  /**
   * 문서 id
   */
  id: string;
  /**
   * 문서 url
   */
  documentUrl: string;
  /**
   * 문서 title
   */
  title: string;
  /**
   * 문서 sub_titile
   */
  subTitle: string;

  /**
   * 생성자
   */
  constructor() {
    this.id = '';
    this.documentUrl = '';
    this.title = '';
    this.subTitle = '';
  }
}


/**
 * 권한 부여 성공
 */
export class LauncherAuthorized {
  /**
   * 성공한 accessToken
   * 이 정보는 참고성이다.
   * 헤더에 어떤 값이 들어있는지 정보를 획득하도록 한다.
   */
  accessToken: string;
  /**
   * 획득 시간 정보
   */
  authorizedAt: Date;

  /**
   * 인증을 위한 추가파라미터
   */
  meta: Struct;

  /**
   * 생성자
   */
  constructor() {
    this.accessToken = '';
    this.authorizedAt = new Date();
    this.meta = new Struct();
  }
}

/**
 * 권한 부여 실패
 */
export class LauncherAuthorizeFailed {
  /**
   * 접근 토큰
   */
  accessToken: string;
  /**
   * 인증 시간
   */
  authorizedAt: Date;

  /**
   * 생성자
   */
  constructor() {
    this.accessToken = '';
    this.authorizedAt = new Date();
  }
}

/**
 * 슬롯이 채워진 경우에 그 값을 정해준다.
 */
export class LauncherSlotsFilled {
  /**
   * 채워진 슬롯
   */
  filledSlots: Struct;
  /**
   * 아직도 채워지지 않는 슬롯
   */
  unfilledSlots: string[];

  /**
   * 생성자
   */
  constructor() {
    this.filledSlots = new Struct();
    this.unfilledSlots = [];
  }
}

/**
 * AudioPlayerPlay 관련 payload 정보
 */
export class AudioPlayerPlay {
  /**
   * 오디오 id
   */
  id: string;
  /**
   * 오디오 play 이벤트 (예:finish, stop, pause, resume)
   */
  reason: string;

  /**
   * 생성자
   */
  constructor() {
    this.id = '';
    this.reason = '';
  }
}

/**
 * 문서 읽기 유/무를 처리하기 위한 정보
 */
export class LauncherDocumentRead {
  /**
   * 문서 id
   */
  id: string;
  /**
   * 해당 문서를 읽었으면 true, 읽지 않았으면 false (read는 과거분사형)
   */
  read: boolean;

  /**
   * 생성자
   */
  constructor() {
    this.id = '';
    this.read = false;
  }
}

/**
 * 클라이언트에게 LaunchApp을 실행하는 경우
 * interface: Client
 * operation: LaunchApp
 */
export class LauncherLaunchAppPayload {
  /**
   *
   */
  target: string;
  /**
   * 실행 대상의 내부 실행 정보
   */
  uri: string;
  /**
   * 실행 대상에게 전달되어야할 상세한 파라미터
   */
  meta: Struct;

  /**
   * 실행 환경 정보
   * OS, VERSION, 기타 정보
   */
  runtimeEnv: string;

  /**
   * 이 실행이 가지는 추가적인 설명 정보
   */
  description: string;
  constructor() {
    this.target = '';
    this.uri = '';
    this.meta = {};
    this.runtimeEnv = '';
    this.description = '';
  }
}


/**
 * 클라이언트에게 LaunchPage을 실행하는 경우
 * 주로 웹 브라우저인 M2U 단말이 실행을 가능성이 높습니다.
 *
 * interface: Client
 * operation: LaunchPage
 */
export class LauncherLaunchPagePayload {
  /**
   *실행 대상 정보 : browser target
   */
  target: string;
  /**
   * 실행 대상의 내부 실행 정보
   */
  uri: string;
  /**
   * 실행 대상에게 전달되어야할 상세한 파라미터
   */
  meta: Struct;

  /**
   * 브라우저 환경 정보
   * 브라우저 VERSION, 기타 정보
   */
  browserEnv: string;

  /**
   * 이 페이지 실행이 가지는 추가적인 설명 정보
   */
  description: string;
  constructor() {
    this.target = '';
    this.uri = '';
    this.meta = {};
    this.browserEnv = '';
    this.description = '';
  }
}


/**
 * DA에서 단말로 내려보나는 경우에는 현재의 세션, SKILL, INENT를 모두 담아서 내려보내고
 * 이를 바로 받아볼 수 있도록 처리한다.
 */
export class DialogAgentForwarderParam {
  /**
   *  챗봇 */
  chatbot: string;
  /**
   *  스킬 */
  skill: string;
  /**
   *  의도 */
  intent: string;
  /**
   *  세션 ID */
  sessionId: number;

  /**
   * 생성자
   */
  constructor() {
    this.chatbot = '';
    this.skill = '';
    this.intent = '';
    this.sessionId = 0;
  }

}


// ------------------------------------------------------------------------
// M2U/COMMON/LOCATION
// ------------------------------------------------------------------------

/**
 * 위경도를 포함한 위치 정보
 */
export class Location {
  /** 위도 */
  latitude: number;
  /** 경도 */
  longitude: number;
  /**
   * 기타 문자열로 표현된 장소 정보, "경복궁", "경포대"
   * *optional*
   * 통상적인 경우는 비어있습니다.
   */
  location: string;
  /**
   * 현재 위치가 채집된 시간, 위치는 시간에 따라서 바뀔 수 있다.
   */
  refreshAt: Date;

  /**
   * 생성자
   */
  constructor() {
    this.latitude = 0;
    this.longitude = 0;
    this.location = '';
    this.refreshAt = new Date();
  }
}

// ------------------------------------------------------------------------
// COMMON/LANG
// ------------------------------------------------------------------------


/**
 * 언어와 로캘을 함께 포함한 구조
 */
export enum Lang {
  /**
   * 한국어, 한국
   */
  ko_KR = "ko_KR",
  /**
   * 영어 미국
   */
  en_US = "en_US"
}

// ------------------------------------------------------------------------
// BRAIN/IR
// ------------------------------------------------------------------------

/**
 * 이미지 포맷
 */
export enum ImageFormat {
  /** JPEG */
  JPEG = "JPEG",
  /** PNG */
  PNG = "PNG",
  /** bitmap */
  BMP = "BMP",
  /** TIFF format */
  TIFF = "TIFF",
  /** pdf format */
  PDF = "PDF"
}

/**
 * Image Recognition Param 에 사용될 수 있는 파라미터.
 */
export class ImageRecognitionParam {
  /**
   * 출력 인코딩 포맷
   */
  imageFormat: ImageFormat = ImageFormat.JPEG;

  /**
   * 출력 언어
   */
  lang: Lang = Lang.ko_KR;

  /**
   * guide 비율의 가로 값
   */
  refVertexX: number;

  /**
   * guide 비율의 세로 값
   */
  refVertexY: number;

  /**
   * 대칭형 crop (True), 비대칭형 crop (False)
   */
  symmCrop: boolean;

  /**
   * 너비
   */
  width: number;

  /**
   * 높이
   */
  height: number;

  /** 생성자 */
  constructor(init: Partial<ImageRecognitionParam>) {
    __assign(this, init);
  }
}

/**
 * 주의: Result로 표현되는 경우는 하나의 요청에 대한 응답으로만 동작하는 것이 아니라
 * 다른 곳에 재전송할 수 있는 데이터 유형일 때이다.
 */

/**
 * 이미지 인식 결과물
 *
 */
export class ImageRecognitionResult {
  /** 이미지 분류 표시 */
  imageClass: string;

  /** 인식된 결과물을 표현하는 텍스트 */
  annotatedTexts: string[];

  /** 인식된 이미지로부터 표현가능한 다양한 정보 */
  meta: Struct;

  /**
   * 생성자
   */
  constructor() {
    this.imageClass = '';
    this.annotatedTexts = [];
    this.meta = new Struct();
  }
}


// ------------------------------------------------------------------------
// BRAIN/IR
// ------------------------------------------------------------------------


/**
 * 음성인식을 위한 파라미터
 *
 * AudioEncoding, sample_rate 등의 옵션을 가지고 있다.
 */
export class SpeechRecognitionParam {
  /**
   * STT encoding
   * 현재는 PCM만 지원한다.
   */
  encoding: AudioEncoding;
  /**
   * STT sample rate
   * 8K, 16K를 지원한다. 기본값으로 16K를 사용한다.
   */
  sampleRate: number;
  /**
   * STT Lang
   * 현재 언어는 kor, eng 만 사용가능하다.
   */
  lang: Lang;
  /**
   * STT 모델
   * *Optional* STT 학습 모델 정보는 지정된 내용으로 전송될 수 있다.
   */
  model: string;
  /**
   * if true, detect endpoint
   * *Optional* If `false` or omitted, the recognizer will perform continuous
   * recognition (continuing to wait for and process audio even if the user
   * pauses speaking) until the client closes the input stream (gRPC API) or
   * until the maximum time limit has been reached. May return multiple
   * `StreamingRecognitionResult`s with the `is_final` flag set to `true`.
   *
   * If `true`, the recognizer will detect a single spoken utterance. When it
   * detects that the user has paused or stopped speaking, it will return an
   * `END_OF_SINGLE_UTTERANCE` event and cease recognition. It will return no
   * more than one `StreamingRecognitionResult` with the `is_final` flag set to
   * `true`.
   */
  singleUtterance: boolean;

  /**
   * 생성자
   */
  constructor() {
    this.encoding = AudioEncoding.LINEAR16;
    this.sampleRate = 16000;
    this.lang = Lang.ko_KR;
    this.model = 'baseline';
    this.singleUtterance = true;
  }
}

/**
 * Indicates the type of speech event.
 */
export enum SpeechEventType {
  /** No speech event specified. */
  SPEECH_EVENT_UNSPECIFIED = "SPEECH_EVENT_UNSPECIFIED",

  /**
   * This event indicates that the server has detected the end of the user's
   * speech utterance and expects no additional speech. Therefore, the server
   * will not process additional audio (although it may subsequently return
   * additional results). The client should stop sending additional audio
   * data, half-close the gRPC connection, and wait for any additional results
   * until the server closes the gRPC connection. This event is only sent if
   * `single_utterance` was set to `true`, and is not used otherwise.
   */
  END_OF_SINGLE_UTTERANCE = "END_OF_SINGLE_UTTERANCE"
}

/**
 * 음성인식 응답
 *
 * `StreamingRecognizeResponse` is the only message returned to the client by
 * `StreamingRecognize`. A series of zero or more `StreamingRecognizeResponse`
 * messages are streamed back to the client. If there is no recognizable
 * audio, and `single_utterance` is set to false, then no messages are streamed
 * back to the client.
 * - Only two of the above responses #4 and #7 contain final results; they are
 * indicated by `is_final: true`. Concatenating these together generates the
 * full transcript: "to be or not to be that is the question".
 *
 * - The others contain interim `results`. #3 and #6 contain two interim
 * `results`: the first portion has a high stability and is less likely to
 * change; the second portion has a low stability and is very likely to
 * change. A UI designer might choose to show only high stability `results`.
 *
 * - The specific `stability` and `confidence` values shown above are only for
 * illustrative purposes. Actual values may vary.
 *
 * - In each response, only one of these fields will be set:
 *`error`,
 * `speech_event_type`, or
 * one or more (repeated) `results`.
 */
export class StreamingRecognizeResponse {

  /**
   * *Output-only* If set, returns a [google.rpc.Status][google.rpc.Status] message that
   * specifies the error for the operation.
   * google.rpc.Status error = 1;
   * maum.ai에서는 NOT_OK인 경우에 detail_error_messages에서 해당 내용을 꺼내서 사용할 수 있도록 한다.
   * *Output-only* This repeated list contains zero or more results that
   * correspond to consecutive portions of the audio currently being processed.
   * It contains zero or more `is_final=false` results followed by zero or one
   * `is_final=true` result (the newly settled portion).
   */
  results: SpeechRecognitionResult[];

  /**
   * *Output-only* Indicates the type of speech event.
   */
  speechEventType: SpeechEventType;

  constructor() {
    this.results = undefined;
    this.speechEventType = SpeechEventType.END_OF_SINGLE_UTTERANCE;
  }
}

/**
 * STT Result를 위한 타입
 * 단어 정보
 */
export class WordInfo {
  /**
   * 단어 시작 시간
   */
  startTime: Duration;

  /**
   * 단어 끝나는 시간
   */
  endTime: Duration;

  /**
   * 단어
   */
  word: string;

  constructor() {
    this.startTime = new Duration();
    this.endTime = new Duration();
    this.word = '';
  }
}

/**
 * 음성인식 결과
 */
export class SpeechRecognitionResult {
  /**
   * 인식된 전체
   */
  transcript: string;
  /**
   * 최종 여부
   */
  final: boolean;

  /**
   * 단어 단위로 시간 표시
   */
  words: WordInfo[];

  constructor() {
    this.transcript = '';
    this.final = false;
    this.words = [];
  }
}


// ------------------------------------------------------------------------
// BRAIN/TTS
// ------------------------------------------------------------------------
/**
 * TTS 관련 AsyncInterface를 호출할 때 사용될 수 있는 파라미터.
 */
export class SpeechSynthesizerParam {

  /**
   * 출력 인코딩 포맷
   */
  encoding: AudioEncoding;

  /**
   * 출력 언어
   */
  lang: Lang;

  /**
   * 출력 PCM
   * *Optional*, MP3의 경우에는 꼭 필요하지는 않다.
   * 8K, 16K를 지원한다. 기본값으로 16K를 사용한다.
   */
  sampleRate: number;

  constructor() {
    this.encoding = AudioEncoding.LINEAR16;
    this.lang = Lang.ko_KR;
    this.sampleRate = 16000;
  }
}

// ------------------------------------------------------------------------
// BRAIN/IR
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
// M2U/COMMON/CARD
// ------------------------------------------------------------------------
/**
 *
 * 카드 형태의 데이터
 * 카드 형태의 데이터를 나타나도록 지시한다.
 * CHART
 *    type: 그래프 종류..
 *    options : 출력형태
 *    data: [] , 내부적으로 대시 배열 2차 3차 , object 로 동작한다
 * SELECT CARD
 *  title, 요약 사진 , selected utter,
 * NEWS LIST
 *  title, 요약, 사진 .. 링크 ...
 */

/**
 * 차트 형태의 데이터
 * google charts 형식을 지원할 수 있도록 정의한다.
 */
export class ChartCard {

  /**
   * 차트 타입:
   *  - "PieChart"
   *  - "BarChart"
   *  - "XXXChart"
   */
  type: string;

  /**
   * 각 차트별로 출력 가능한 다양한 옵션을 지정한다.
   */
  options: Struct;

  /**
   * 차트 출력을 위한 실제 대이터는 배렬로 제공된다.
   * 각각의 ListValue는 array로 표현된다.
   * ListValue 내부에는 다시 ListValue를 가질 수 있고 다차원으료 표현이 가능한다.
   */
  data: any[];

  constructor() {
    this.type = '';
    this.data = [];
  }
}

/**
 * Grid 형태의 데이터
 */
export class GridCard {

  /**
   * Grid type에는 데이터 형식에 대한 정보를 제공한다
   * - 시장금리 추이
   * - 기준금리 추이
   * - 대출금리 추이
   * 예를 들어 type이 '시장금리 추이' 라면 해당 표는 금리추세를 표현한 표임을 알 수 있다.
   */
  type: string;

  /**
   * 표 출력을 위한 실제 데이터는 배열로 제공된다.
   * columns는 열이 어떻게 구성되어야 할 지에 대한 구조를 제공한다.
   */
  columns: any;

  /**
   * rows은 행을 의미하며 여러개의 ListValue를 가질수 있다.
   */
  rows: any[];

  constructor() {
    this.type = '';
    this.columns = {};
    this.rows = [];
  }
}

/**
 * 테이블 형태의 데이터
 */
export class TableCard {

  /**
   * 테이블 type에는 데이터 형식에 대한 정보를 제공한다
   */
  type: string;

  /**
   * 테이블 Card의 아이템 목록들
   */
  items: TableItem[];

  constructor() {
    this.type = '';
  }
}

/**
 * 테이블 Card 내 ITEM
 */
export class TableItem {
  /**
   * 키
   */
  key: string;

  /**
   * 값
   */
  value: string;
  /**
   * value에 대해서 적용되는 값 (예 : 정렬, 폰트, 컬러)
   */
  style: string;

  constructor() {
    this.key = '';
    this.value = '';
    this.style = '';
  }
}

/**
 * 리스트 형태의 테이블 형태의 데이터
 */
export class TableListCard {
  /**
   * 테이블 형태의 카드를 배열로 처리한다.
   */
  cards: TableCard[];
  /**
   * 가로로 출력할지 여부..
   */
  horizontal: boolean;

  constructor() {
    this.cards = [];
    this.horizontal = true;
  }
}


/**
 * 선택형 카드
 *
 * 값을 선택할 수 있는 선택형 카드가 필요하다.
 */
export class SelectCard {
  /**
   * 카드 전체의 타이블
   */
  title: string;
  /**
   * 카드의 헤데값
   */
  header: string;
  /**
   * 카드의 아이템 목록들
   */
  items: Item[];
  /**
   * 가로 본능?
   */
  horizontal: boolean;
  /**
   * 다양한 서브 타입을 넣을 수 있도록 한다.
   */
  type: string;

  constructor() {
    this.title = '';
    this.header = '';
    this.type = '';
  }
}

/**
 * 카드내 ITEM
 */
export class Item {
  /**
   * 타이틀
   */
  title: string;
  /**
   * *optional* 요약
   */
  summary: string;
  /**
   * *opt* URL
   */
  imageUrl: string;
  /**
   * 선택될 경우에 전달된 텍스트
   * 이때에는 이벤트 메시지로 UtterType에 "TOUCH"를 전달하도록 한다.
   */
  selectedUtter: string;
  /**
   * Item이 여러개인 경우 대표 Item을 설정한다
   * selected 값이 true이면 해당 Item이 대표 Item 이다
   * Item이 두개이상인 경우 selected = true는 한개여야 한다
   */
  selected: boolean;
  /**
   * 예) "red large" 와 같이 특정 카드의 CSS를 직접 지정할 수 있도록 한다.
   */
  style: string;

  constructor() {
    this.title = '';
    this.summary = '';
    this.imageUrl = '';
    this.selectedUtter = '';
    this.style = '';
  }
}

/**
 * 링크 형태의 카드
 */
export class LinkCard {
  /**
   * 제목
   */
  title: string;

  /**
   * 요약
   */
  summary: string;

  /**
   * 이미지 URL
   */
  imageUrl: string;

  /**
   * 이미지 클릭 후 이동할 URL
   */
  imageHref: string;

  /**
   * 다양한 서브 타입을 넣을 수 있도록 한다.
   */
  type: string;

  constructor() {
    this.title = '';
    this.summary = '';
    this.imageUrl = '';
    this.imageHref = '';
    this.type = '';
  }
}

/**
 * 리스트 형태의 카드들
 */
export class ListCard {
  /**
   * 링크 형태의 카드를 배열로 처리한다.
   */
  cards: LinkCard[];
  /**
   * 가로로 출력할지 여부..
   */
  horizontal: boolean;

  constructor() {
    this.cards = [];
    this.horizontal = true;
  }
}

/**
 * 사용자 지정 카드
 */
export class CustomCard {
  /**
   * 카드 타입
   */
  type: string;

  /**
   * 사용자 지정 카드는 모든 데이터를 다 가지고 있다.
   */
  cardData: Struct;

  constructor() {
    this.type = '';
    this.cardData = new Struct();
  }
}

/**
 * 사용자 지정 카드 목록
 */
export class CustomListCard {
  /**
   * 사용자 지정 카드의 목록
   */
  cards: CustomCard[];
  /**
   * 가로로 출력하도록 한다.
   */
  horizontal: boolean;

  constructor() {
    this.cards = [];
    this.horizontal = true;
  }
}

/**
 * 메시지 카드
 */
export class Card {
  chart: ChartCard = undefined;
  table: TableCard = undefined;
  grid: GridCard = undefined;
  select: SelectCard = undefined;
  link: LinkCard = undefined;
  custom: CustomCard = undefined;
  linkList: ListCard = undefined;
  customList: CustomListCard = undefined;
  tableList: TableListCard = undefined;
  raw: string = undefined;

  constructor(init?: Partial<Card>) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;

    __assign(this, init);
  }

  get _chart() {
    return this.chart;
  }

  set _chart(v: ChartCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this.chart = v;
  }

  get _table() {
    return this.table;
  }

  set _table(v: TableCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this.table = v;
  }

  get _grid() {
    return this.grid;
  }

  set _grid(v: GridCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this.grid = v;
  }

  get _select() {
    return this.select;
  }

  set _select(v: SelectCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this._select = v;
  }

  get _link() {
    return this.link;
  }

  set _link(c: LinkCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this.link = c;
  }

  get _custom() {
    return this.custom;
  }

  set _custom(c: CustomCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this.custom = c;
  }

  get _linkList() {
    return this.linkList;
  }

  set _linkList(c: ListCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this.linkList = c;
  }

  get _customList() {
    return this.customList;
  }

  set _customList(c: CustomListCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this.customList = c;
  }

  get _tableList() {
    return this.tableList;
  }

  set _tableList(c: TableListCard) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = undefined;
    this.tableList = c;
  }

  get _raw() {
    return this.raw;
  }

  set _raw(c: string) {
    this.chart = undefined;
    this.table = undefined;
    this.grid = undefined;
    this.select = undefined;
    this.link = undefined;
    this.custom = undefined;
    this.linkList = undefined;
    this.customList = undefined;
    this.tableList = undefined;
    this.raw = c;
  }
}


// ------------------------------------------------------------------------
// COMMON/AUDIOENCODING
// ------------------------------------------------------------------------
export enum AudioEncoding {
  /**
   * Not specified. Will return result [google.rpc.Code.INVALID_ARGUMENT][google.rpc.Code.INVALID_ARGUMENT].
   */
  ENCODING_UNSPECIFIED = "ENCODING_UNSPECIFIED",
  /**
   * Uncompressed 16-bit signed little-endian samples (Linear PCM).
   */
  LINEAR16 = "LINEAR16",

  /**
   * [`FLAC`](https://xiph.org/flac/documentation.html) (Free Lossless Audio
   * Codec) is the recommended encoding because it is
   * lossless--therefore recognition is not compromised--and
   * requires only about half the bandwidth of `LINEAR16`. `FLAC` stream
   * encoding supports 16-bit and 24-bit samples, however, not all fields in
   * `STREAMINFO` are supported.
   */
  FLAC = "FLAC",

  /**
   * 8-bit samples that compand 14-bit audio samples using G.711 PCMU/mu-law.
   */
  MULAW = "MULAW",

  /**
   * Adaptive Multi-Rate Narrowband codec. `sample_rate_hertz` must be 8000.
   */
  AMR = "AMR",

  /**
   * Adaptive Multi-Rate Wideband codec. `sample_rate_hertz` must be 16000.
   */
  AMR_WB = "AMR_WB",

  /**
   * Opus encoded audio frames in Ogg container
   * ([OggOpus](https://wiki.xiph.org/OggOpus)).
   * `sample_rate_hertz` must be 16000.
   */
  OGG_OPUS = "OGG_OPUS",

  /**
   * Although the use of lossy encodings is not recommended, if a very low
   * bitrate encoding is required, `OGG_OPUS` is highly preferred over
   * Speex encoding. The [Speex](https://speex.org/)  encoding supported by
   * Cloud Speech API has a header byte in each block, as in MIME type
   * `audio/x-speex-with-header-byte`.
   * It is a variant of the RTP Speex encoding defined in
   * [RFC 5574](https://tools.ietf.org/html/rfc5574).
   * The stream is a sequence of blocks, one block per RTP packet. Each block
   * starts with a byte containing the length of the block, in bytes, followed
   * by one or more frames of Speex data, padded to an integral number of
   * bytes (octets) as specified in RFC 5574. In other words, each RTP header
   * is replaced with a single byte containing the block length. Only Speex
   * wideband is supported. `sample_rate_hertz` must be 16000.
   */
  SPEEX_WITH_HEADER_BYTE = "SPEEX_WITH_HEADER_BYTE"
}


// ------------------------------------------------------------------------
// M2U/MAP/AUTHENTICATION
// ------------------------------------------------------------------------


export class AuthenticationParam {
  /**
   * 인증 방법
   */
  method: string;
  /**
   * 값
   */
  value: string;

  constructor(method: string, value: string) {
    this.method = method;
    this.value = value;
  }
}

/**
 * 각 인증 방법에 따른 인증 파라미터들
 * 인증 페이로드
 */
export class SignInPayload {
  /**
   * 사용자 키
   * 사용자를 구분할 수 있는 키
   */
  userkey: string;
  /**
   * 비밀번호
   */
  passphrase: string;

  authParams: AuthenticationParam[];

  device: Device;

  constructor(userkey: string, passphrase: string) {
    this.userkey = userkey;
    this.passphrase = passphrase;
    this.authParams = [];
    this.device = new Device();
  }
}


/**
 * 인증 결과 PAYLOAD
 */
export class SignInResultPayload {
  // ONE OF
  authSuccess: AuthTokenPayload;
  authFailure: AuthFailurePayload;
  multiFactorAuthRequest: MultiFactorAuthRequestPayload;

  constructor() {
    this.authFailure = undefined;
    this.authSuccess = undefined;
    this.multiFactorAuthRequest = undefined;
  }

}


/**
 * 인증 성공 데이터
 *
 * 인증 성공 시점에 단말에 전달되는 데이터
 * 단말은 인증 토콘을 저장해야 한다.
 */
export class AuthTokenPayload {
  /**
   * 인증 요청에 대한 결과
   * 발급된 인증 토큰
   */
  authToken: string;

  /**
   * 이 인증 성공이 multi factor 인증의 결과물인가를 확인해준다.
   */
  multiFactor: boolean;

  /**
   * *optional*
   * 기타 인증 클라이언트에서 필요로 하는 추가적인 데이터
   */
  meta: Struct;

  constructor() {
    this.authToken = '';
    this.multiFactor = false;
    this.meta = new Struct();
  }
}

/**
 * 인증 실패 페이로드
 * 인증 싶패의 이유를 명시적으로 적시해준다.
 */
export class AuthFailurePayload {
  /**
   * 인증 실패 코드
   */
  resCode: string;
  /**
   * 실패 메시지
   */
  message: string;
  /**
   * 인증 실패하는 경우에 대한 상세 메시지
   */
  detailMessage: string;

  constructor() {
    this.resCode = '';
    this.message = '';
    this.detailMessage = '';
  }
}

export class MultiFactorAuthMethod {
  /**
   * 추가적인 인증 요청 방식
   */
  method: string;
  /**
   * 추가적인 인증 요청을 위한 사용자 정의 데이터
   * 어떤 형식의 데이터도 처리할 수 있도록 Struct로 정의
   */
  param: Struct;

  constructor() {
    this.method = '';
    this.param = new Struct();
  }
}

/**
 * MULTI FACTOR 인증을 위한 추가적인 인증 밥업과
 * 인증 방식을 정의합니다.
 */
export class MultiFactorAuthRequestPayload {

  /**
   * 다음 `MultiFactorVerifyPayload` 을 보낼 때,
   * 함께 보내줘야 하는 ID
   */
  tempAuthToken: string;
  /**
   * 다중요소 인증 방법들의 배열
   * 서버에서 필요한 다양한 메시지가 정의될 수 있습니다.
   */
  multiFactorAuthMethods: MultiFactorAuthMethod[];

  constructor() {
    this.tempAuthToken = '';
    this.multiFactorAuthMethods = [];
  }
}


export class MultiFactorAuthResult {
  /**
   * 추가적인 인증 요청 방식
   */
  method: string;
  /**
   * 인증처리된 결과에 대한 값
   */
  value: string;
  /**
   * 해당 값 이외에 추가적으로 정의하려고 하는
   * 데이터 값,
   */
  meta: Struct;

  constructor() {
    this.method = '';
    this.value = '';
    this.meta = new Struct();
  }
}

/**
 * 다중 팩터 요청에 대한 상세 데이터
 */
export class MultiFactorVerifyPayload {
  /**
   * 서버에서 보내온 다음 인증을 위한 키 데이터
   */
  tempAuthToken: string;

  /**
   * 각 인증 방법에 따른 인증 파라미터들
   */
  authParams: AuthenticationParam[];

  /**
   * 다중요소 인증 방법들의 배열
   * 서버에서 필요한 다양한 메시지가 정의될 수 있습니다.
   */
  multiFactorAuthResults: MultiFactorAuthResult[];

  device: Device;

  constructor() {
    this.tempAuthToken = '';
    this.authParams = [];
    this.multiFactorAuthResults = [];
    this.device = new Device();
  }
}

/**
 * SignOut 페이로드
 */
export class SignOutPayload {
  /**
   * 인증 토큰
   */
  authToken: string;
  /**
   * 사용자 키 (optional)
   */
  userKey: string;

  constructor(authtoken: string) {
    this.authToken = authtoken;
    this.userKey = '';
  }
}

/**
 * 응답 메시지
 */
export class SignOutResultPayload {
  /**
   * 로그아웃 후 응답 메시지
   */
  message: string;

  constructor() {
    this.message = '';
  }
}


/**
 * 사용자 속성 정의 데이터
 */
export class UserSettings {
  /**
   * 인증 토큰
   */
  authToken: string;

  /**
   * 사용자 속성
   */
  settings: Struct;

  constructor() {
    this.authToken = '';
    this.settings = new Struct();
  }
}

/**
 * 사용자 속성 조회를 위한 키
 */
export class UserKey {
  /**
   * 인증 토큰
   */
  authToken: string;

  constructor() {
    this.authToken = '';
  }
}

// ------------------------------------------------------------------------
// M2U/MAP/MAP
// ------------------------------------------------------------------------
//
// PING
//

/**
 * 주기적인 Ping 요청
 */
export class PingRequest {
  /**
   * 디바이스 정보
   */
  device: Device;
  /**
   * PING 호출 시간
   */
  pingAt: Date;
}

/**
 * PONG을 통한 클라이언트 상태 표시
 */
export enum PongClientState {
  /**
   * 기존에 이미 존재한 클라이언트 지속
   */
  PONG_CLIENT_CONTINUE = "PONG_CLIENT_CONTINUE",
  /**
   * 새로운 클라이언트가 발견되었고 이를 등록처리
   */
  PONG_NEW_CLIENT_FOUND = "PONG_NEW_CLIENT_FOUND",
  /**
   * 기존의 클라이언트가 재시작 되었음을 인지했음.
   */
  PONG_IDLE_CLIENT_RESTART = "PONG_IDLE_CLIENT_RESTART",
  /**
   * 접근이 거부되었음
   */
  PONG_ACCESS_DENIED = "PONG_ACCESS_DENIED"
}

/**
 * PONG 응답
 */
export class PongResponse {
  /**
   * 클라이언트 상태
   */
  clientState: PongClientState;
  /**
   * 서버 식별 정보
   */
  m2uId: MaumToYouIdentifier;

  /**
   * 새로운 EventStream을 요청할 필요가 있는지 여부
   */
  requireEventStream: boolean;
  dirState: DirectiveState;
  version: string;
  pongAt: Date;


  constructor() {
    this.clientState = PongClientState.PONG_NEW_CLIENT_FOUND;
    this.m2uId = new MaumToYouIdentifier();
    this.requireEventStream = false;
    this.dirState = new DirectiveState();
    this.version = "0.1";
    this.pongAt = new Date();
  }
}

/**
 * M2U 서버에 대한 구분
 */
export class MaumToYouIdentifier {
  uuid: string;
  version: string;
  licensedTo: string;


  constructor() {
    this.uuid = '';
    this.version = '';
    this.licensedTo = '';
  }
}


/**
 * 디렉티브 상태
 */
export class DirectiveState {
  pending: boolean;
  pendingCount: number;
  discarded: boolean;
  discardedCount: number;


  constructor() {
    this.pending = false;
    this.pendingCount = 0;
    this.discarded = false;
    this.discardedCount = 0;
  }
}

//
// EVENTS
//
export class EventContext {
  /**
   * 디바이스 정보
   */
  private device: Device;
  /**
   * 위치 정보
   */
  private location: Location;


  constructor() {
    this.device = null;
    this.location = null;
  }

  setDevice(device: Device): void {
    this.location = undefined;
    this.device = device;
  }

  setLocation(loc: Location): void {
    this.location = loc;
    this.device = undefined;
  }
}

export class EventParam {
  private speechRecognitionParam: SpeechRecognitionParam;
  private imageRecognitionParam: ImageRecognitionParam;
  /**
   * 비디오 파라미터
   */
  private videoParam: VideoParam;
  /**
   * 이미지 파라미터
   */
  private gestureParam: GestureParam;
  /**
   * 키보드 파라미터
   */
  private keyboardParam: KeyboardParam;
  /**
   * 빈 파라미터
   */
  private emptyParam: Empty;

  /**
   * DA에 호출 정보를 넘겨준다.
   */
  private dialogAgentForwarderParam: DialogAgentForwarderParam;


  constructor() {
    this.speechRecognitionParam = undefined;
    this.imageRecognitionParam = undefined;
    this.videoParam = undefined;
    this.gestureParam = undefined;
    this.keyboardParam = undefined;
    this.emptyParam = undefined;
    this.dialogAgentForwarderParam = undefined;
  }


  get _speechRecognitionParam(): SpeechRecognitionParam {
    return this.speechRecognitionParam;
  }

  set _speechRecognitionParam(value: SpeechRecognitionParam) {
    this.imageRecognitionParam = undefined;
    this.videoParam = undefined;
    this.gestureParam = undefined;
    this.keyboardParam = undefined;
    this.emptyParam = undefined;
    this.dialogAgentForwarderParam = undefined;
    this.speechRecognitionParam = value;
  }

  get _imageRecognitionParam(): ImageRecognitionParam {
    return this.imageRecognitionParam;
  }

  set _imageRecognitionParam(value: ImageRecognitionParam) {
    this.speechRecognitionParam = undefined;
    this.videoParam = undefined;
    this.gestureParam = undefined;
    this.keyboardParam = undefined;
    this.emptyParam = undefined;
    this.dialogAgentForwarderParam = undefined;
    this.imageRecognitionParam = value;
  }

  get _videoParam(): VideoParam {
    return this.videoParam;
  }

  set _videoParam(value: VideoParam) {
    this.speechRecognitionParam = undefined;
    this.imageRecognitionParam = undefined;
    this.gestureParam = undefined;
    this.keyboardParam = undefined;
    this.emptyParam = undefined;
    this.dialogAgentForwarderParam = undefined;
    this.videoParam = value;
  }

  get _gestureParam(): GestureParam {
    return this.gestureParam;
  }

  set _gestureParam(value: GestureParam) {
    this.speechRecognitionParam = undefined;
    this.imageRecognitionParam = undefined;
    this.keyboardParam = undefined;
    this.emptyParam = undefined;
    this.videoParam = undefined;
    this.dialogAgentForwarderParam = undefined;
    this.gestureParam = value;
  }

  get _keyboardParam(): KeyboardParam {
    return this.keyboardParam;
  }

  set _keyboardParam(value: KeyboardParam) {
    this.speechRecognitionParam = undefined;
    this.imageRecognitionParam = undefined;
    this.gestureParam = undefined;
    this.emptyParam = undefined;
    this.videoParam = undefined;
    this.dialogAgentForwarderParam = undefined;
    this.keyboardParam = value;
  }

  get _emptyParam(): Empty {
    return this.emptyParam;
  }

  set _emptyParam(value: Empty) {
    this.speechRecognitionParam = undefined;
    this.imageRecognitionParam = undefined;
    this.gestureParam = undefined;
    this.keyboardParam = undefined;
    this.videoParam = undefined;
    this.dialogAgentForwarderParam = undefined;
    this.emptyParam = value;
  }

  get _dialogAgentForwarderParam(): DialogAgentForwarderParam {
    return this.dialogAgentForwarderParam;
  }

  set _dialogAgentForwarderParam(value: DialogAgentForwarderParam) {
    this.speechRecognitionParam = undefined;
    this.imageRecognitionParam = undefined;
    this.gestureParam = undefined;
    this.keyboardParam = undefined;
    this.videoParam = undefined;
    this.dialogAgentForwarderParam = value;
    this.emptyParam = undefined;
  }
}

let kkk = new EventParam();
kkk._speechRecognitionParam = new SpeechRecognitionParam();


let p = AudioEncoding.LINEAR16;
console.log('LEANER16', p.toString(), p.valueOf());


/**
 * 이벤트 형식의 FRAME
 * 단일 이벤트이거나 연속된 스트림 이벤트의 시작일 수 있다.
 */
export class EventStream {
  /**
   * 호출되는 대상에 대한 정의
   * 호출되는 대상은 interface와 operation으로 정의된다.
   * 예를 들어, interface는 VideoPlayer 이고 operation 은 stop 이다.
   */
  interface: AsyncInterface;
  /**
   * 호출 ID
   * AsyncInterface 메시지의 ID에서 유일하게 생성된다.
   */
  streamId: string;
  /**
   * 요청 동기화 ID *Optional*
   * 이벤트가 대응이 되는 응답이 있을 경우에 이를 정의할 수 있다.
   * 예를 들어서 AudioTalk의 이벤트를 클라이언트에서 전송할 경우에는
   * 응답으로 음성인식이 완료됨, 응답 메시지의 전송, 응답 화면 출력 등 디렉티브를
   * 받을 수 있다.
   */
  operationSyncId: string;
  /**
   * 이벤트 컨텍스트
   * 컨텍스트는 한 개 이상 전송될 수 있다.
   */
  contexts: EventContext[];
  /**
   * Event에는 BODY Stream에 대한 파라미터가 존재할 수 있다.
   * 각 파라미터는 스트림 BODY 규격에 따라서 다르다. 자세한 사항은
   * 각 AsyncInterface에 규격표에 정리하여 정의할 수 있다.
   * 이벤트 파라미터는 한개 존재한다.
   */
  param: EventParam;
  /**
   * 이벤트에 대한 메타데이터는 추가적인 메시지로서 각 AsyncInterface 또는 사용자의 정의에
   * 따라서 임의의 메시지가 전송될 수 있다.
   */
  payload: Struct;
  /**
   * 이벤트가 시작된 시간
   */
  beginAt: Date;

  constructor() {
    this.interface = undefined;
    this.streamId = UUID.generate();
    this.operationSyncId = '';
    this.contexts = [];
    this.param = new EventParam();
    this.payload = new Struct();
    this.beginAt = new Date();
  }
}

//
// DIRECTIVES
//
export class DirectiveParam {
  // FIXME
  // oneof 를 어떻게 구현할 것인가?
  /**
   * DA에서 단말에게 전달되는 param
   */
  public daForwardParam: DialogAgentForwarderParam;
  /**
   * TTS 음성인식 파라미터
   */
  private speechSynthesizerParam: SpeechSynthesizerParam;
  /**
   * 비디오 파라미터
   */
  private videoParam: VideoParam;
  /**
   * 빈 파라미터
   */
  private emptyParam: Empty;

  constructor() {
    this.daForwardParam = undefined;
    this.speechSynthesizerParam = undefined;
    this.videoParam = undefined;
    this.emptyParam = undefined;
  }

}

/**
 * 디렉티브 형식의 프레임
 * 단일한 디렉티브 이거나 연속된 스트림 디렉티브의 시작일 수 있다.
 */
export class DirectiveStream {
  /**
   * 호출되는 대상에 대한 정의
   * 호출되는 대상은 interface와 operation으로 정의된다.
   * 예를 들어, interface는 VideoPlayer 이고 operation 은 stop 이다.
   */
  interface: AsyncInterface;
  /**
   * 호출 메시지를 구분하는 ID
   */
  streamId: string;
  /**
   * 대화와 관련된 응답 메시지 규격 *Optional*
   */
  operationSyncId: string;
  param: DirectiveParam;
  /**
   * 디렉티브에 대한 메타데이터는 추가적인 메시지로서 각 AsyncInterface
   * 또는 사용자의 정의에 따라서 임의의 메시지가 전송될 수 있다.
   */
  payload: Struct;
  /**
   * 디렉티브가 시작된 시간
   */
  beginAt: Date;

  constructor() {
    this.interface = new AsyncInterface();
    this.streamId = '';
    this.operationSyncId = '';
    this.param = new DirectiveParam();
    this.payload = '';
    this.beginAt = new Date();
  }
}


//
// EXCEPTION
//
export enum StatusCode {
  STATUS_NOT_SPECIFIED = "STATUS_NOT_SPECIFIED",

  GRPC_STT_ERROR = "GRPC_STT_ERROR",
  GRPC_IDR_ERROR = "GRPC_IDR_ERROR",
  GRPC_TTS_ERROR = "GRPC_TTS_ERROR",
  GRPC_AUTH_SIGN_IN_ERROR = "GRPC_AUTH_SIGN_IN_ERROR",
  GRPC_AUTH_SIGN_OUT_ERROR = "GRPC_AUTH_SIGN_OUT_ERROR",
  GRPC_AUTH_MULTIFACTOR_VERIFY_ERROR = "GRPC_AUTH_MULTIFACTOR_VERIFY_ERROR",
  GRPC_AUTH_IS_VALID_ERROR = "GRPC_AUTH_IS_VALID_ERROR",
  GRPC_AUTH_GET_USER_INFO_ERROR = "GRPC_AUTH_GET_USER_INFO_ERROR",
  GRPC_AUTH_UPDATE_USER_SETTINGS_ERROR = "GRPC_AUTH_UPDATE_USER_SETTINGS_ERROR",
  GRPC_AUTH_GET_USER_SETTINGS_ERROR = "GRPC_AUTH_GET_USER_SETTINGS_ERROR",
  GRPC_ROUTER_OPEN_ERROR = "GRPC_ROUTER_OPEN_ERROR",
  GRPC_ROUTER_TALK_ERROR = "GRPC_ROUTER_TALK_ERROR",
  GRPC_ROUTER_EVENT_ERROR = "GRPC_ROUTER_EVENT_ERROR",
  GRPC_ROUTER_CLOSE_ERROR = "GRPC_ROUTER_CLOSE_ERROR",
  GRPC_ROUTER_FEEDBACK_ERROR = "GRPC_ROUTER_FEEDBACK_ERROR",
  GRPC_STT_TRANSCRIPT_NULL_ERROR = "GRPC_STT_TRANSCRIPT_NULL_ERROR",

  AUTH_IS_VAILD_FAILED = "AUTH_IS_VAILD_FAILED",
  AUTH_INVALID_AUTH_TOKEN = "AUTH_INVALID_AUTH_TOKEN",
  AUTH_FAILED = "AUTH_FAILED",
  AUTH_INVALID_HEADER = "AUTH_INVALID_HEADER",
  AUTH_CHECK_AUTH_FAILED = "AUTH_CHECK_AUTH_FAILED",
  MAP_NO_STREAM_PARAM = "MAP_NO_STREAM_PARAM",
  MAP_IF_NOT_FOUND = "MAP_IF_NOT_FOUND",
  MAP_IF_STREAMING_NOT_MATCH = "MAP_IF_STREAMING_NOT_MATCH",
  MAP_IF_DUPLICATED_STREAMING = "MAP_IF_DUPLICATED_STREAMING",
  MAP_EVENT_CASE_NOT_SET = "MAP_EVENT_CASE_NOT_SET",
  MAP_CURRENTLY_HAS_NO_STREAM = "MAP_CURRENTLY_HAS_NO_STREAM",
  MAP_STREAM_ID_NOT_MATCH = "MAP_STREAM_ID_NOT_MATCH",
  MAP_PAYLOAD_ERROR = "MAP_PAYLOAD_ERROR",
  MAP_CLASS_NOT_FOUND = "MAP_CLASS_NOT_FOUND",

  ROUTER_SESSION_NOT_FOUND = "ROUTER_SESSION_NOT_FOUND",
  ROUTER_SESSION_INVALID = "ROUTER_SESSION_INVALID",
  ROUTER_CHATBOT_NOT_FOUND = "ROUTER_CHATBOT_NOT_FOUND",
  ROUTER_DA_NOT_FOUND = "ROUTER_DA_NOT_FOUND",
  ROUTER_DA_ERROR = "ROUTER_DA_ERROR",
  ROUTER_ITF_NOT_FOUND = "ROUTER_ITF_NOT_FOUND",
  ROUTER_ITF_ERROR = "ROUTER_ITF_ERROR",
  MAP_TOTAL_SESSION_COUNT_EXCEEDED = "MAP_TOTAL_SESSION_COUNT_EXCEEDED",
  MAP_SYSTEM_MAINTENANCE = "MAP_SYSTEM_MAINTENANCE"
}

/**
 * 예외는 Event나 Directive에 대응하여 호출시 문제가 생기면 이를 내보내도록 한다.
 */
export class MapException {
  /**
   * 예외를 위한 ID, stream_id와 동일한 레벨임.
   */
  exceptionId: string;
  /**
   * 상태 정보
   */
  statusCode: StatusCode;
  /**
   * 상세한 서버의 예외 메시지 정보
   */
  exMessage: string;
  /**
   * 상세한 서버의 예외 메시지 정보
   */
  exIndex: number;
  /**
   * 상세한 예외 본문
   */
  payload: Struct;
  /**
   * 예외 전송 시각
   */
  thrownAt: Date;
  /**
   * *Optional* 예외가 발생한 ID
   * 예외가 인증에러와 관련된 것들이 아닌 경우에는
   */
  streamId: string;
  /**
   * *Optional* 예외와 관련된 operation_sync_id
   */
  operationSyncId: string;
  /**
   * *Optional* 예외와 관련된 AsyncInterface
   */
  calledInterface: AsyncInterface;

  constructor() {
    this.exceptionId = '';
    this.statusCode = StatusCode.STATUS_NOT_SPECIFIED;
    this.exIndex = 0;
    this.exMessage = '';
    this.payload = new Struct();
    this.thrownAt = new Date();
    this.streamId = '';
    this.operationSyncId = '';
    this.calledInterface = new AsyncInterface();
  }
}

/**
 * `StreamMeta`을 통해서
 * Event나 Directive에 추가적인 스트림 데이터를 전송할 수 있다.
 * 기본적인 스트림 데이터 형식인 문자열이나 바이트 배열 이외에 다른 메시지를
 * 포함하려고 할 때 사용할 수 있다.
 */
export class StreamMeta {
  /**
   * 객체 타입, 메타 데이터의 객체 유형을 정의 한다.
   */
  objectType: string;
  /**
   * 메타 데이터, 메타 데이터를 정의한다.
   */
  meta: Struct;

  constructor() {
    this.objectType = '';
    this.meta = new Struct();
  }
}

/**
 * 스트림 형식의 Event나 Directive의 끝을 표시해준다.
 */
export class StreamEnd {
  /**
   * 종료시키려는 FRAME을 명시적으로 정의한다.
   */
  streamId: string;
  /**
   * 종료 시간
   */
  endAt: Date;

  constructor() {
    this.streamId = '';
    this.endAt = new Date();
  }
}

/**
 * 중지를 발생시키는 타입
 */
export enum StreamBreaker {
  /**
   * 원인을 알 수 없는 중지
   */
  BREAK_BY_UNSPECIFIED = "BREAK_BY_UNSPECIFIED",
  /**
   * 사용자에 의한 명시적인 중지
   */
  BREAK_BY_USER = "BREAK_BY_USER",
  /**
   * 클라이언트나 서버의 내부 구조에 의한 중지
   */
  BREAK_BY_SYSTEM = "BREAK_BY_SYSTEM",
  /**
   * Stream 전송 규칙 과정에서 발생하는 논리적인 오류
   */
  BREAK_BY_LOGICAL = "BREAK_BY_LOGICAL",
  /**
   * 예외에 의한 중지
   * 예를 들어서 음성 데이터를 받아오는 네트워크의 중지에 의한 예외 등
   */
  BREAK_BY_EXCEPTION = "BREAK_BY_EXCEPTION"
}

export class StreamBreak {
  constructor() {
    this.reason = '';
    this.breaker = StreamBreaker.BREAK_BY_UNSPECIFIED;
    this.streamId = '';
    this.brokenAt = new Date();
  }

  /**
   * 중지 이유 메시지, 빈 문자열일 수 있수 없다. 중지 사유를
   * 로그로 남겨야 하므로 이를 기록해야 한다.
   * 로그를 남길 수 없는 경우에 프로그램은 경고 메시지를 남겨야 한다.
   */
  reason: string;
  /**
   * 멈춤 메시지 구분자
   */
  breaker: StreamBreaker;
  /**
   * *Optional* 중지시키는 메시지 ID
   * 중지시키는 메시지 ID는 참고용으로만 사용되므로 유의미하지는
   * 않다.
   * 메시지 BREAK가 발생한 이후로 들어오는 모든 bytes, text는
   * 무시된다.
   */
  streamId: string;
  /**
   * 중지 시점
   */
  brokenAt: Date;

}

// ###############################################
// ASYNC INTERFACE 들을 위한 공통 규격
/**
 * operation 타입
 */
export enum OperationType {
  /**
   * EVENT 유형, `클라이언트`에서 `MAP`으로 전송된다.
   */
  OP_EVENT = "OP_EVENT",
  /**
   * DIRECTIVE 유형, `MAP`에서 `클라이언트`로 전송된다.
   */
  OP_DIRECTIVE = "OP_DIRECTIVE"
}

/** 호출되는 대상에 대한 정의
 * 호출되는 대상은 interface와 operation으로 정의된다.
 * operation은 단순히 메소드가 아니라 이벤트 메시지이기도 하고
 * 각 상대방이 처리해야할 메시지이기도 하다.
 *
 * 예를 들어, interface는 VideoPlayer 이고 operation 은 stop 이다.
 */
export class AsyncInterface {
  /**
   * 인터페이스 이름
   * 통상적으로 변수 규칙에 따르고 대문자로 시작하고 Camel Case로 작성한다.
   * Map과 통신하는 구조에서는 기본으로 제공하는 인터페이스 외에 추가적인 인터페이스가
   * 존재할 수 있다.
   * 추가적인 인터페이스에 대해서는 플러그인 형태로 개발할 수 있도록 되어 있다.
   */
  interface: string = '';
  /**
   * 오퍼레이션 이름
   * 각 상대방에서 수행해야 할 작업의 이름이고 인터페이스에 종속적이다.
   * 이 이름은 동사이거나 발생하는 이벤트의 이름이어야 한다.
   */
  operation: string = '';
  /**
   * 메시지의 타입은 EVENT 이거나 DIRECTIVE 임.
   */
  type: OperationType = OperationType.OP_EVENT;
  /**
   * 스트리밍 여부
   * operation은 스트리밍 형식이거나 단일 메시지 형식이다.
   * 스트리밍 형식의 operation은 EventStream, DirectiveStream과 StreamEnd 사이에
   * 반복적인 BODY 메시지를 전송할 수 있다.
   * 반복적인 BODY 형식은 string, byte array, meta 형식으로 전송될 수 있다.
   * EventStream, DirectiveStream과 StreamEnd은 같은 stream_id를 공유한다.
   * EventEnd를 만나면 해당 스트림은 종료된다.
   * 스트리밍이 아닌 AsyncInterface 형식을 가진 EventStream이나 DirectiveStream이
   * 중간에 끼여서 전송될 수 있다. 하지만, 스트리밍 이벤트 중간에 다른 스트리밍 이벤트는
   * 전송될 수 없다. 만일, 동시에 스트리밍 이벤트는 여러 개 전송해야 하는 경우에는
   * 새로운 grpc 연결을 사용해서 처리하면 된다.
   * 이 규칙을 어기고 스트림 형식의 새로운 EventStream이나 DirectiveStream이 들어오면
   * 현재의 스트림은 자동으로 종료 처리된다.
   * 스트리밍 프레임들은 StreamBreak 메시지를 통해서 중지될 수 있다.
   * 이 경우, 스트리밍 프레임은 중지되고, stream_id는 더 이상 유효하지 않게 된다.
   * 추가적인 스트리밍 BODY는 모두 버려지게 된다.
   */
  streaming: boolean = false;

  constructor(init?: Partial<AsyncInterface>) {
    __assign(this, init);
  }
}

/**
 * 비동기인터페이스의 목록
 */
export class AsyncInterfaceList {
  interfaces: AsyncInterface[];

  constructor() {
    this.interfaces = [];
  }
}

//
// STREAM PARAMETERS
//


/**
 * Video 관련 AsyncInterface를 호출할 때 사용될 수 있는 파라미터.
 */
export class VideoParam {
  videoFormat: string;
  codec: string;
  todo: string;

  constructor() {
    this.videoFormat = '';
    this.codec = '';
    this.todo = '';
  }
}

export class GestureParam {
  todo: string;

  constructor() {
    this.todo = '';
  }
}

/**
 * 키보드 파라미터
 */
export class KeyboardParam {
  /**
   * 입력 길이
   */
  typedLength: number;
  /**
   * 입력 시간
   */
  duration: Duration;

  constructor() {
    this.typedLength = 0;
    this.duration = new Duration();
  }
}

// ------------------------------------------------------------------------
// M2U/MAP/PAYLOAD
// ------------------------------------------------------------------------


/**
 * RenderText 호출에 따르는 Payload
 */
export class RenderTextPayload {
  text: string;

  constructor() {
    this.text = '';
  }
}


/**
 * 기존 V1, V2 호환을 위해서 또는 디버그 목적으로 DA에서 내려주는 다양한 정보들을
 * 사용하도록 내려준다.
 * 이때 meta가 그대로 내려간다.
 */
export class RenderHiddenPayload {
  /**
   * 기존 V1, V2 호환을 위해서 또는 디버그 목적으로 DA에서 내려주는 다양한 정보들을
   * 사용하도록 내려준다.
   * 이때 meta가 그대로 내려간다.
   */
  meta: Struct;

  constructor() {
    this.meta = new Struct();
  }
}


/**
 * 음성입력 대기에 필요한 추가적인 Payload 데이터
 */
export class ExpectSpeechPayload {
  /**
   * 입력 대기 시간, milli second
   */
  timeoutInMilliseconds: number;

  constructor() {
    this.timeoutInMilliseconds = 0;
  }
}


