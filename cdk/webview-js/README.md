# maum.ai M2U CDK for WebView
- M2U CDK는 Android, iOS WebView 개발을 위한 javascript API 입니다.
- M2U CDK는 M2U Asynchronous Proxy 서버와 통신기능을 제공합니다.  


# requirements
## Running Environment
- M2U 2.0.1 이상
- M2U CDK App for Android
- M2U CDK App for iOS
- WebView ES5 above

## JavaScript requirements
- requirejs (Version 2.3.5)


# 전체 구성도
![overview](../image/overview.png)


# 모듈 구성 요소
## main 
main 모듈은 Map Client 와 응답을 받기 위한 Handler가 정의되어 있다
main 모듈을 통해 아래의 기능을 제공한다
 - 대화 기능 제공
 - 인증 처리 기능 제공
 - MAP 서버로 부터 수신 받은 Directive 및 Native에서 발생된 이벤트를 수신할 수 있는 MapDirectiveHandler 기능 제공 
 - map 통신 중에 발생한 exception 관련 정보 제공
 - Ping에 대한 결과로 Pong 기능 제공
 - 그외 디바이스, 세션 시작/종료, Map Setting, 마이크 On/Off, 스피커 On/Off, 카메라 On/Off, 갤러리 On/Off, 위치 정보 요청

## proto
proto는 main 모듈에서 제공하는 기능들에 사용되는 모든 Proto가 정의되어 있다.
proto 모듈에는 아래의 Proto 정보가 제공된다.
 - [Utter](../doc/classes/_proto_.utter.html) : 사용자 발화 관련 Proto
 - [OpenUtter](../doc/classes/_proto_.utter.html) : 새로운 대화 세션 생성 관련 Proto
 - [Device](../doc/classes/_proto_.device.html) : 단말 관련 Proto
 - [Directive](../doc/classes/_proto_.directivestream.html) : Map에서 전달받은 DIRECTIVE 관련 Proto
 - [Event](../doc/classes/_proto_.eventstream.html) : MAP EVENT 관련 Proto
 - [Location](../doc/classes/_proto_.location.html) : 위경도를 포함한 위치 정보 관련 Proto
 - [Image](../doc/classes/_proto_.imagerecognitionparam.html) : 이미지 관련 Proto
 - [STT](../doc/classes/_proto_.speechrecognitionparam.html) : 음성인식 관련 Proto
 - [TTS](../doc/classes/_proto_.speechsynthesizerparam.html) : 음성출력 관련 Proto
 - [Card](../doc/classes/_proto_.card.html) : 카드 형태 관련 Proto
 - [Authentication](../doc/classes/_proto_.authenticationparam.html) : 인증 관련 Proto
 - [MAP](../doc/classes/_main_.mapclient.html) : MAP 과의 통신 관련 Proto
 - [Exception](../doc/classes/_proto_.mapexception.html) : Exception에 대한 StatusCode Proto
 - [RenderTextPayload](../doc/classes/_proto_.rendertextpayload.html) : RenderText 관련 Proto
 - [RenderHiddenPayload](../doc/classes/_proto_.renderhiddenpayload.html) : RenderHidden 관련 Proto
 - [ExpectSpeechPayload](../doc/classes/_proto_.expectspeechpayload.html) : ExpectSpeech 관련 Proto

## native
native 모듈은 Native와의 Interface 생성 및 Event의 상태값이 정의되어 있다.
native 모듈에는 아래의 정보가 제공된다
 - [MapSetting](../doc/classes/_native_.mapsetting.html)의 정의 및 사용방법
 - [GrpcStatus](../doc/classes/_native_.grpcstatus.html) GRPC 통신에 대한 상태정보
 - [Streaming](../doc/classes/_native_.streamingstatus.html) 관련 정보
 - [MIC](../doc/classes/_native_.microphoneparam.html) 관련 정보
 - [Speaker](../doc/classes/_native_.speakerstatus.html) 관련 정보
 - [Camera](../doc/classes/_native_.cameraparam.html) 관련 정보
 - [Gallery](../doc/classes/_native_.galleryparam.html) 관련 정보
 - [Location](../doc/classes/_native_.locationstatus.html) 관련 정보
 - [WebViewClientNative](../doc/classes/_native_.localm2uwebviewclientnative.html) 웹뷰에서 네이티브로 호출하는 함수들에 대한 규격을 정의
 - [NativeEventForward](../doc/classes/_main_.nativeeventforward.html)네이티브에서 Javascript를 호출할 때 사용하는 정보

## uuid
 - [uuid](../doc/classes/_uuid_.uuid.html) 모듈은 operationSyncId 생성시 필요한 기능을 제공한다

# Dialog Sequnce Flow
## SetMapSetting
 - Webpage 로딩시 서버 IP, Port 등 MAP과 통신을 위한 설정 값, authToken 값, MapEvent 구성에 필요한 device  값 등 전송
![setmapsetting](../image/setmapsetting.png)

## SignIn
 - 인증절차를 시도한다
 - 인증에 필요한 SignInPayload를 전달한다.
![signIn](../image/signin.png)

## Open
 - 최초 Session 생성을 위해 Open을 시도한다.
 - 사용자 발화가 있다면 openUtter를 전달한다.
![open](../image/open.png)

## Speech To Text Talk
 - 사용자의 발화를 MIC를 통하여 전달한다.
 - MIC를 통해 정상적으로 음성 byteStream이 전달되었다면 Recognizing, Recognized를 수신받는다.
![stt](../image/stt.png)

## Text To Speech Talk 
 - Speaker 상태를 통해 Speaker를 Open 하여 수신된 byteStream을 Play 한다
![tts](../image/tts.png)

## Image To Text Talk 
 - openCamera 호출을 통해 Camera 상태를 확인한다.
 - Camera Open에 성공후 Image Captured 상태가 수신되면 ImageUrl을 이용하여 ImageToTextTalk를 호출한다.
![itt](../image/itt.png)

## Getlocation
 - 현재 단말의 위치 정보를 확인한다.
![getlocation](../image/getlocation.png)

# 설정
## setMapSetting
setMapSetting(MapSetting) 함수를 호출하여 아래와 같은 기능을 제공한다 
 - JS는 setMapSetting() 함수를 호출하여 MAP 서버와 통신하기 위해 필요한 정보(TLS 사용여부, 서버의 IP/Port, Ping 주기)를 Native에게 전달한다.
 - 단말 정보 제공 : 단말 정보가 갱신되면 JS에서 setMapSetting() 호출을 하여 Native로 단말 정보를 제공한다.
 - MapSetting 데이터에 pingInterval 값이 있으면 이 주기마다 MAP 에 pingRequest 를 호출하고 MAP 으로부터 응답으로 받은 pongResponse 값을 JS 에게 전달

## dependency 관련 사항 
- require.js는 module 로딩에 사용된다.
- require.js를 설정하는 방법을 설명한다.

```html
<script src="require.js"></script>
<script>
  var m2ucdk = undefined;
  var mapClient = undefined;
  
  requirejs.config({
    baseUrl: '.',
    paths: {
      'main': 'cdk',
      'native': 'cdk',
      'uuid': 'cdk',
      'proto': 'cdk'
    }
  });
  requirejs(['main', 'proto', 'native', 'uuid'], function (main, proto, native, uuid) {
    console.log('starting m2u cdk libraries..');
    m2ucdk = main;
    m2ucdk.proto = proto;
    m2ucdk.native = native;

    var setting = new native.MapSetting("192.168.0.2", 9911, false);
    // assign to global mapClient
    mapClient = new main.MapClient(setting, "DEVUNIQSUFFIX00");
    
    // create device
    var d = new proto.Device();
    d.channel = "sample";
    d.id = uuid.UUID.generate();
    d.type = 'WebViewAndroid';
    d.version = '1.0';
    
    // assign device to mapClient
    mapClient.device = d;
</script>
````

## debug api를 설정하는 방법

```javascript
  var debugCallback = {
    onEvent: function (str) {
      mapEvent.innerText =
          'EventStream: \n' + str;
    },
    onDirective: function (str) {
      mapDirective.innerText = mapDirective.innerText +
      'DirectiveStream: \n' + str;
    }
  };
  mapClient._debug = debugCallback;
```

## callback handler를 설정하는 방법

```javascript
  var directiveCallbackHandler = {
    /**
     * 텍스트 출력 콜백
     * @param {RenderTextPayload} text
     */
    onViewRenderText: function (text) {
      console.log('onViewRenderText', text);
    },
    /**
     * 카드 출력 콜백
     * @param {Card} card
     */
    onViewRenderCard: function (card) {
      console.log('onViewRenderCard', card);
    },
    /**
     * 메타 데이터 수신 콜백
     * @param {Struct} hidden
     */
    onViewRenderHidden: function (hidden) {
      console.log('onViewRenderHidden', hidden);
    },
    /**
     * 음성인식 진행 중일 때 콜백
     * @param {StreamingRecognizeResponse} o
     */
    onSpeechRecognizerRecognizing: function (o) {
      console.log('onSpeechRecognizerRecognizing', o);
    },

    /**
     * 음성인식 완료되었을 때 콜백
     * @param {SpeechRecognitionResult} o
     */
    onSpeechRecognizerRecognized: function (o) {
      console.log('onSpeechRecognizerRecognized', o);
    },
    /**
     * 대화 처리 중일 때 콜백
     */
    onDialogProcessing: function () {
      console.log('onDialogProcessing');
    },
    /**
     * 음성합성 출력할 때 콜백
     */
    onSpeechSynthesizerPlayStarted: function () {
      console.log('onSpeechSynthesizerPlayStarted');
    },
    /**
     * 음성 입력 대기 수신할 때 콜백
     * @param {ExpectSpeechPayload} o
     */
    onMicrophoneExpectSpeech: function (o) {
      console.log('onMicrophoneExpectSpeech', o);
    },
    /**
     * 아바타 설정 명령어 수신할 때 콜백
     * @param {AvatarSetExpressionPayload} o
     */
    onAvatarSetExpression: function (o) {
      console.log('onAvatarSetExpression', o);
    },
    /**
     * 이미지 인식이 완료되었을 때
     */
    onImageDocumentRecognized: function () {
      console.log('onImageDocumentRecognized');
    },
    /**
     * 로그인에 대한 응답 콜백
     * @param {SignInResultPayload} o
     */
    onAuthenticationSignInResult: function (o) {
      console.log('onImageDocumentRecognized', 0);
    },
    /**
     * 로그아웃에 대한 응답 콜백
     * @param {SignOutResultPayload} o
     */
    onAuthenticationSignOutResult: function (o) {
      console.log('onAuthenticationSignOutResult', 0);
    },
    /**
     * 다중 인증에 대한 응답이 올 때 발생하는 콜백
     * @param {SignInResultPayload} o
     */
    onAuthenticationMultiFactorVerifyResult: function (o) {
      console.log('onAuthenticationMultiFactorVerifyResult', 0);
    },
    /**
     * 예외가 발생한 경우에 발생할 콜백
     * @param {MapException} e MapException
     */
    onException: function (e) {
      console.log('onException', 0);
    },
    /**
     * 호출이 완료될 때 나올 콜백
     * @param {string} opid 오퍼레이션 ID
     */
    onOperationCompleted: function (opid) {
      console.log('onOperationCompleted', 0);
    },
    /**
     * 에러가 발생될 때 처리할 콜밸
     * @param {GrpcStatus} error 에러
     */
    onError: function (error) {
      console.log('onError', error);
    },
    /**
     * 음성이나 이미지를 전송 중이거나 음성 출력이 내려오고 있을 때
     * 정보를 알려줍니다.
     *
     * @param {StreamingStatus} o
     */
    onStreamingEvent: function (o) {
      console.log('onStreamingEvent', o);
    },
    /**
     * 마이크 생태가 바뀔 때 콜백
     * @param {MicrophoneStatus} st
     */
    onMicrophoneStatus: function (st) {
      console.log('onMicrophoneStatus', st);
      if (st.eventType == native.MicrophoneEvent.OPEN_SUCCESS) {
        console.log("MICROPHONE OPEN SUCCESS");
        __speechToTextTalk();
      } else {
        console.log("onMicrophoneStatus :" + st.eventType);
      }
    },
    /**
     * 마이크 생태가 바뀔 때 콜백
     * @param {MicrophoneStatus} st
     */
    onSpeakerStatus: function (st) {
      console.log('onSpeakerStatus', st);

    },
    /**
     * 카메라 상태 정보
     *
     * @param {CameraStatus} st
     */
    onCameraStatus: function (st) {
      console.log('onCameraStatus', st);

      // 카메라 열기에 성공한 경우
      if (st.eventType == native.CameraEvent.IMAGE_CAPTURED) {
        console.log("CAMERA IMAGE_CAPTURED");
        __imageToTextTalk(st.imageData.imageUrl);
      } else {
        console.log("onCameraStatus :" + st.eventType);
      }
    },
    /**
     * 갤러시 상태 정보
     * @param {GalleryStatus} st 갤러리 상태
     */
    onGalleryStatus: function (st) {
      console.log('onGalleryStatus', st);
    },
    /**
     * 위치 상태 정보 변경시 콜백
     * @param {LocationStatus} st 위치정보 변경
     */
    onLocationChanged: function (st) {
      console.log('onLocationChanged', st);
    }
  };

  // 처리할 함수를 지정한다.
  mapClient.handler = directiveCallbackHandler;

```
