#include "stt-client.h"
#include <libmaum/common/encoding.h>
#include <memory.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define __LOG 0
#if __LOG
#include <iostream>
using std::cout;
using std::endl;
#endif

const char *kDoneMessage = "<done>";

// ====================================================================
// STT CLIENT
// ====================================================================

SttClient::SttClient(const std::string &ip, int port)
    : ip_(ip),
      port_(port) {
  struct sockaddr_in addr;
  sock_ = socket(PF_INET, SOCK_STREAM, 0);

  addr.sin_family = AF_INET;
  addr.sin_port = htons(uint16_t(port_));
  addr.sin_addr.s_addr = inet_addr(ip_.c_str());
  memset(addr.sin_zero, 0, sizeof addr.sin_zero);

  if (connect(sock_, (sockaddr *) &addr, sizeof addr) < 0) {
    strerror(errno);
    ::close(sock_);
    sock_ = -1;
  }

  SetState(STT_CONNECTED);

  epoll_ = epoll_create(2);

  epoll_event ev;
  ev.events = EPOLLIN;
  ev.data.fd = sock_;

  epoll_ctl(epoll_, EPOLL_CTL_ADD, sock_, &ev);

  std::unique_ptr<std::thread> pt(new std::thread(&SttClient::Run, this));

  runner_ = std::move(pt);
}

SttClient::~SttClient() {
  runner_->join();
  if (sock_ != -1) {
    ::close(sock_);
    sock_ = -1;
  }
  if (epoll_ != -1) {
    ::close(epoll_);
  }
}


bool SttClient::IsReady() {
  // 응답이 다 완료되었을 때까지 대기한다.
  {
#if __LOG
    cout << sock_ << ": Wait READY" << endl;
#endif
    auto ul = std::unique_lock<std::mutex>(rd_mutex_);
    rd_cv_.wait(ul, [&]() { return state_ >= STT_WELCOME; });
#if __LOG
    cout << sock_ << ":wake up READY, state = "  << state_ << endl;
#endif
  }
  return state_ >= STT_WELCOME && state_ <= STT_SENDING;
}

bool SttClient::Send(const std::string &raw) {
  // 준비상태가 되지 않으면 전송할 수 없음.
  {
    auto ul = std::unique_lock<std::mutex>(wr_mutex_);
    if (state_ >= STT_SENT_OVER) {
#if __LOG
      cout << sock_ << ": " << "state is SENT OVER, dropping it. " << raw.size()
          << endl;
#endif
      return false;
    }
    wr_queue_.push(raw);
#if __LOG
    cout << sock_ << ":"  << "state is sending, wr queue size = " << wr_queue_.size() << endl;
#endif
  }
  wr_cv_.notify_one();
  // successfully added.
  return true;
}

bool SttClient::Send(const char *buf, size_t len) {
  return Send(std::string(buf, len));
}

bool SttClient::SendDone() {
  {
    auto ul = std::unique_lock<std::mutex>(wr_mutex_);
    if (state_ >= STT_SENT_OVER) {
#if __LOG
      cout << sock_ << ":"  << "state is SENT OVER, do not enqueue" << endl;
#endif
      return false;
    }

    wr_queue_.push(kDoneMessage);
#if __LOG
    cout << sock_ << ":" 
        << "state will be sent over, wr queue size = "
        << wr_queue_.size() << endl;
#endif
  }
  wr_cv_.notify_one();
  return true;
}

std::string SttClient::GetResult() {
  // 응답이 다 완료되었을 때까지 대기한다.
  {
#if __LOG
    cout << sock_ << ":"  << "GetResult waiting" << endl;
#endif
    auto ul = std::unique_lock<std::mutex>(rd_mutex_);
    rd_cv_.wait(ul, [&]() { return state_ == STT_FINISHED; });
#if __LOG
    cout << sock_ << ":"  << "GetResult wake up" << endl;
#endif
  }
  if (result_ == "ASR_NOTOKEN")
    result_.clear();
  if (result_.empty())
    return result_;
  return EuckrToUtf8(result_);
}


// ====================================================================
// STT CLIENT THREAD
// ====================================================================

void SttClient::SetState(State state) {
  {
    auto ul = std::unique_lock<std::mutex>(rd_mutex_);
#if __LOG
    int prev = state_;
    cout << sock_ << ":"  << "STATE from: " << prev << ", to:" << state << endl;
#endif
    state_ = state;
  }
  rd_cv_.notify_one();
}

void SttClient::Run() {

  while (state_ != STT_FINISHED && state_ != STT_ERROR) {
    switch (state_) {
      case STT_CONNECTED:
        DoSendUser();
        WaitWelcome();
        break;
      case STT_WELCOME:
        DoSendBegin();
        break;
      case STT_STARTED:
        SetState(STT_SENDING);
        break;
      case STT_SENDING:
        FeedVoiceData();
        break;
      case STT_SENT_OVER:
        SetState(STT_RESULT);
        break;
      case STT_RESULT:
        WaitResult();
        break;
      case STT_FINISHED:
      case STT_ERROR:
      default:
        break;
    }
  }
  epoll_event ev;
  ev.data.fd = sock_;
  ev.events = EPOLLIN;
  epoll_ctl(epoll_, EPOLL_CTL_DEL, sock_, &ev);

  close(sock_);
  sock_ = -1;

  close(epoll_);
  epoll_ = -1;
}

std::string SttClient::NextEvent() {
  std::unique_lock<std::mutex> lk(rd_mutex_);
  if (state_ < STT_SENDING) {
    wr_cv_.wait(lk, [&] { return wr_queue_.size() > 0; });
    auto one = wr_queue_.front();
    wr_queue_.pop();
#if __LOG
    cout << runner_->get_id() << "," << sock_ << ":"  << "idle next event = " << one.size() << endl;
#endif
    return one;
  } else {
    using namespace std::chrono;
    steady_clock::time_point tp = steady_clock::now() + microseconds(1000);
    bool ret = wr_cv_.wait_until(lk, tp,
                                 [&] { return wr_queue_.size() > 0; });
    if (!ret) {
#if __LOG
      cout << runner_->get_id() << "," << sock_ << ":"  << "next event NONE " << endl;
#endif
      return ""; /// 빈 문자열을 반환한다. 다음 동작은 그냥 모니터링,
    } else {
      auto one = wr_queue_.front();
      wr_queue_.pop();
#if __LOG
      cout << runner_->get_id() << "," << sock_ << ":"  << "next event = " << one.size() << endl;
#endif
      return one;
    }
  }
}


// 웰컴 메시지가 올때까지 대기한다.
void SttClient::WaitWelcome() {
  char cmdbuf[7];
  char recvbuf[kMaxBufSize];
  epoll_event events[1];

  while (true) {
    int fds = epoll_wait(epoll_, events, 1, 100);
    if (!fds) continue;
    if (events[0].data.fd == sock_) {
      ssize_t n = recv(sock_, cmdbuf, kCmdSize, 0);
      cmdbuf[n] = '\0';
      int len = std::stoi(cmdbuf + 2);
      n = recv(sock_, recvbuf, size_t(len), 0);
      recvbuf[n] = '\0';
#if __LOG
      cout << sock_ << ":"  << "WELCOME: " << recvbuf << endl;
#endif
      SetState(STT_WELCOME);
      return;
    }
  }
}

// 데이터를 지속적으로 전송하고, 중간에 완료되었는지를 점검한다.
// 데이터의 종료를 전송한다.
void SttClient::FeedVoiceData() {
  char cmdbuf[7];
  char recvbuf[kMaxBufSize];
  epoll_event events[2];

  while (true) {
    int fds = epoll_wait(epoll_, events, 2, 10);
    if (fds) {
      ssize_t n = recv(sock_, cmdbuf, kCmdSize, 0);
      cmdbuf[n] = '\0';
      int len = std::stoi(cmdbuf + 2);
      if (cmdbuf[1] != 'E' && len > 0) {
        n = recv(sock_, recvbuf, size_t(len), 0);
      }
      if (len & kSttFrontDetected) {
#if __LOG
        cout << runner_->get_id() << "," << sock_ << ":"  << "IN FEED, endpoint detected" << endl;
#endif
        DoSendFinish();
        // STT_SENT_OVER
        break;
      }
    }

    /// RECV EVENT and SEND IT
    std::string ev = NextEvent();
    if (ev == kDoneMessage) {
      DoSendFinish();
      // STT_SENT_OVER
      break;
    } else if (ev.empty()) {
#if __LOG
      cout << runner_->get_id() << "," << sock_ << ":"  << "event has empty : time out" << endl;
#endif
      continue;
    } else {
      if (DoSendBytes(ev) < 0)
        break;
    }
  }
}

// 데이터를 모두 대기한다.
void SttClient::WaitResult() {
  char cmdbuf[7];
  char recvbuf[kMaxBufSize];
  epoll_event events[2];

  while (true) {
    int fds = epoll_wait(epoll_, events, 2, 100);
    if (fds) {
      ssize_t n = recv(sock_, cmdbuf, kCmdSize, 0);
      cmdbuf[n] = '\0';
      int len = std::stoi(cmdbuf + 2);
      if (cmdbuf[1] != 'E' && len > 0) {
        n = recv(sock_, recvbuf, size_t(len), 0);
        // TODO
        // if n < 0이면 에러..
      }

      switch (cmdbuf[1]) {
        case 'R': {
          AddResult(recvbuf);
          break;
        }
        case 'F': {
          SetState(STT_FINISHED);
          return;
        }
        case 'E':
        default:
          break;
      }
    }
  }
}


int SendAll(int fd, const iovec *vector, int iovec_len) {
  ssize_t n;
  do {
    n = writev(fd, vector, iovec_len);
  } while (n == -1 && errno == EINTR);

  if (n < 0) {
    if (errno != EINTR && errno != EWOULDBLOCK) {
      int eno = errno;
#if __LOG
      char errmsg[1024];
      strerror_r(eno, errmsg, sizeof(errmsg));
      cout <<  fd << ":"  << "errno : " << eno << ", " << errmsg << endl;
#endif
      return -eno;
    }
  }
  return int(n);
}

void SttClient::DoSendUser() {
  static char user_len[] = "%u0008";
  static char user_data[] = "MindsLab";
  iovec buffer[2] = {
      {.iov_base = user_len, .iov_len = 6},
      {.iov_base = user_data, .iov_len = 8}
  };
#if __LOG
  cout << runner_->get_id() << "," << sock_ << ":"  << "--> sending user" << endl;
#endif
  if (SendAll(sock_, buffer, 2) < 0) {
    //
    // TODO
    // set errno
    SetState(STT_ERROR);
  }
}

void SttClient::DoSendBegin() {
  static char begin_buf[] = "%b0000";
  iovec buffer[1] = {
      {.iov_base = begin_buf, .iov_len = 6}
  };
#if __LOG
  cout << runner_->get_id() << "," << sock_ << ":"  << "--> sending begin" << endl;
#endif
  if (SendAll(sock_, buffer, 1) < 0) {
    //
    // TODO
    // set errno
    SetState(STT_ERROR);
  } else
    SetState(STT_STARTED);
}


int SttClient::DoSendBytes(const std::string &raw) {
  char mem[7];
  iovec buffer[2];
  buffer[0].iov_base = mem;
  // assert RAW SIZE should be lesser than 9999
  int sz = snprintf(mem, sizeof(mem), "%%s%04d", int(raw.size()));
  buffer[0].iov_len = size_t(sz);
  buffer[1].iov_base = const_cast<char *>(raw.data());
  buffer[1].iov_len = raw.size();

  if (SendAll(sock_, buffer, 2) < 0) {
    // TODO
    // set error
    SetState(STT_ERROR);
    return -1;
  }
  return 0;
}

void SttClient::DoSendFinish() {
  static char finish_buf[] = "%f0000";
  iovec buffer[1] = {
      {.iov_base = finish_buf, .iov_len = 6}
  };
#if __LOG
  cout << runner_->get_id() << "," << sock_ << ":"  << "--> sending finish" << endl;
#endif
  if (SendAll(sock_, buffer, 1) < 0) {
    // TODO
    // set errno
    SetState(STT_ERROR);
  } else {
    SetState(STT_SENT_OVER);
  }
}

void SttClient::AddResult(const char *recv) {
  const char *p = recv;
  const char *q;
  if (*p == '#') p++; // remove leading '#'
  q = strchr(p, '#');
  if (q) {
    result_ += std::string(p, q);
  } else {
    result_ += std::string(p);
  }

  // #### 와 #### 사이만 기록한다.
}
