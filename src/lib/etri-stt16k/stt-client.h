#ifndef LIB_STT_CLIENT_H
#define LIB_STT_CLIENT_H

#include <string>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <thread>

/**
 * ETRI ASR Server에서 STT로부터 텍스트 데이터를 구하기 위한 클라이언트.
 *
 * 이 클라이언트는 파일에서 데이터를 읽거나 음성 신호 콜백에서 데이터를 읽는 비동기적인
 * 상황에서 원격으로 데이터를 전송하고 그 결과를 받아올 수 있는 구조로 동작한다.
 *
 * 보통 다음과 같은 방식으로 호출이 된다.
 *
 * ```c++
 * void NetworkInput(NetworkStreamReader *reader) {
 *   SttClient client;
 *
 *   string input;
 *   if (client.IsReady()) {
 *     while (reader->GetData(input)) {
 *       client.Send(input);
 *     }
 *     client.SendDone();
 *     string utter = client.GetResult();
 *   } else {
 *     string error = client.GetError();
 *   }
 * }
 *
 * ```
 *
 * STT 통신 프로토콜은 몇가지 상태로 변경 중이지만, 호출하는 클라이언트에서는
 * 단순히 데이터를 전송하고 텍스트 변환 결과를 받는 방식으로 동작하도록 하였다.
 *
 * TODO
 * - to split string buffer when input string is larger than 9999.
 * - More accurate error handling, especially network io.
 * - multiple client and single thread surface, which handles multiple
 *   connections. It requires singleton pattern also.
 */
class SttClient {
 public:
  /**
   * 생성자.
   *
   * 소켓을 초기화하고, 백엔드 쓰레드를 실행한다.
   */
  explicit SttClient(const std::string &ip, int port);
  /**
   * 종료된 쓰레드를 회수하고 리소스를 해제한다.
   */
  virtual ~SttClient();

  /**
   * 음성 데이터를 전송할 준비가 될 때까지 대기하고
   * 성공적으로 준비가 되면 `true`를 리턴한다.
   *
   * @return 음성 데이터를 준비가 되었으면 true를 반환한다. 만일 네트워크 에러가
   * 발생하거나 예외적인 상황이 발생하면 false를 반환한다.
   */
  bool IsReady();

  /**
   * 변환할 음성 데이터를 전송한다.
   *
   * 실지로 전송하지는 않고 이를 큐에 넣어둔다. 큐에 넣어두면 별도의 쓰레드에서 처리한다.
   * 이미 서버에서 음성인식이 완료되어 버린 경우라면 추가적인 데이터는 전송하지 않고
   * 버린다. 이때 false 를 리턴한다.
   *
   * @param raw 음성데이터, 이 음성데이터 길이는 반드시 16bit, 즉 2바이트의 배수
   * 여야 한다. 또한 길이는 9998을 넘어서는 안된다.
   * @return 전송할 큐에 정상적으로 추가하면 true를 반환한다.
   */
  bool Send(const std::string &raw);
  /**
   * `bool Send(const std::string&)`과 동일하다.
   * @param buffer 데이터 버퍼
   * @param len 버퍼의 크기
   * @return see ``bool Send(const std::string&)`
   */
  bool Send(const char *buf, size_t len);

  /**
   * 음성데이터 전송을 완료한다.
   *
   * @return 음성 데이터의 종료를 정상적으로 전송하면 true를 반환한다.
   * 만일 이미 내부적으로 음성 데이터 종료가 전송된 경우에는 false를 반환한다.
   */
  bool SendDone();

  /**
   * 변환된 텍스트를 받을 때까지 대기하고 그 결과를 반환한다.
   * @return 변환된 텍스트 데이터
   */
  std::string GetResult();

  /** 동작 중에 발생한 에러를 반환한다.
   *
   * TODO
   *   서버에서 발생한 에러 메시지, 데이터 오류,네트워크 에러를 정확하게
   *   구분할 필요가 있음.
   * @return 에러
   */
  const std::string &GetError() {
    return error_;
  };

 private:
  // 아래 상태는 모두 특정한 동작이 완료된 상태를 나타낸다.
  // 그래서 Run()에서 동작은 다음 상태로 전이하기 위한
  // 작업을 수행하도록 구성된다.
  /**
   * 프로토콜의 진행 상태를 나타낸다.
   */
  enum State {
    STT_BEGIN,     ///< 처음 상태
    STT_CONNECTED, ///< 연결 상태, 쓰레드 시작하기
    STT_WELCOME,   ///< %u, user 전송, %I, welcome 수신
    STT_STARTED,   ///< %b 전송
    STT_SENDING,   ///< %s 전송, %E 확인
    STT_SENT_OVER, ///< %f 전송
    STT_RESULT,    ///< %R 수신 대기
    STT_FINISHED,  ///< %F 수신 완료
    STT_ERROR      ///< 네트워크 에러가 발생하여 진행 볼가
  };
  int state_ = STT_BEGIN;  ///< 내부적인 상태
  int sock_; ///< TCP 스트림

  /**
   * 상태를 변경한다.
   *전적으로 `Run` 쓰레드 내부에서만 이뤄진다.
   * 모든 변경이 발생하면 이를 대기하고 있는 클라이언트 쓰레드에 알려준다.
   * 이때 `rd_cv_` condition variable 를 활용한다.
   *
   * @param state 새로운 상태값
   */
  void SetState(State state);

  /**
   * 서버에서 변환된 텍스트는 한꺼번에 전송되지 않고 여러번에 나눠서 전송될 수 있다.
   *
   * 받은 데이터를 저장한다.
   * @param 받은 음성 텍스트 데이터
   */
  void AddResult(const char *recv);

  // READ
  std::string result_;            ///< 변환된 텍스트
  std::mutex rd_mutex_;           ///< 상태 변경을 위한 mutex
  std::condition_variable rd_cv_; ///< 상태 변경을 대기하기 위한 이벤트

  // WRITE
  std::mutex wr_mutex_;           ///< 음성 데이터 전송을 위한 lock
  std::condition_variable wr_cv_; ///< 음성 데이터 전송을 위한 이벤트
  std::queue<std::string> wr_queue_; ///< 음성 데이터 전송 큐

  // WORKER THREAD
  std::unique_ptr<std::thread> runner_; ///< 네트워크 처리 쓰레드

  /**
   * `Run()` 쓰레드 내부에서 전송된 음성 데이터를 꺼내온다.
   * @return 음성데이터 블럭
   */
  std::string NextEvent();
  /**
   * 서버와 통신 전체를 처리하고 상태변화를 관장하는 쓰레드 메인 함수.
   */
  void Run();
  /**
   * `%u` 패킷을 전송한다.
   */
  void DoSendUser();
  /*
   * `%b` 패킷은 전송한다.
   */
  void DoSendBegin();
  /*
   * `%f` 패킷을 전송한다.
   */
  void DoSendFinish();
  /**
   * `%s` 패킷을 전송한다.
   * @param raw `NextEvent()`를 통해서 꺼내온 음성 데이터를 보낸다.
   * @return 네트워크 오류가 있으면 -1를 반환한다. 정상이면 0을 반환한다.
   */
  int DoSendBytes(const std::string &raw);
  /**
   * `%I`를 대기하고 데이터가 도착하면 상태를 변환한다.
   */
  void WaitWelcome();
  /**
   * 전송받은 음성 데이터를 전송하고, 서버의 상태를 모니터링한다.
   * 최종적으로 음성 전송 완료를 보내고 중지한다.
   */
  void FeedVoiceData();
  /**
   * 결과가 올때까지 대기하도록 한다.
   */
  void WaitResult();
  std::string ip_;
  int port_;
  int epoll_; ///< 폴링 시스템 핸들
  const int kMaxBufSize = 10000;
  const int kCmdSize = 6;

  const int kSttFrontDetected = 2; // from frontend_api.h

  /// ERROR
  std::string error_; ///< 수집 또는 생성된 에러 메시지
};

#endif //LIB_STT_CLIENT_H
