#ifndef SAMPLE_DIALOG_CLIENT_H
#define SAMPLE_DIALOG_CLIENT_H

#include <grpc++/grpc++.h>
#include <maum/m2u/facade/dialog.grpc.pb.h>

#include <memory>
#include <condition_variable>
#include <vector>
#include <string>
#define __LOG 0
#if __LOG
#include <libmaum/common/config.h>
#endif
#include <queue>
#include <map>

using std::string;
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;
using maum::m2u::facade::DialogService;
using maum::m2u::facade::Session;
using maum::m2u::facade::SessionKey;
using maum::m2u::facade::SessionStat;
using maum::m2u::facade::AudioUtter;
using maum::m2u::facade::Caller;
using grpc::ClientAsyncReaderWriter;
using grpc::CompletionQueue;
using grpc::internal::CompletionQueueTag;

using google::protobuf::int64;

enum OpenMode {
  OPEN_WITHOUT_AUTH,
  OPEN_WITH_AUTH
};

class AsyncDialogClient {
 private:

 public:
  AsyncDialogClient(std::shared_ptr<Channel> channel)
      : stub_(DialogService::NewStub(channel)) {
#if __LOG
    log_ = LOGGER();
    spdlog::set_level(spdlog::level::debug);
    spdlog::set_async_mode((1 << 12),
                         spdlog::async_overflow_policy::discard_log_msg,
                         nullptr,
                         std::chrono::milliseconds(10),
                         nullptr);
#endif
  }

  void AppendOutputBlock(const int16_t *block, size_t length);
  void AppendOutputBlock(const std::string &command);
  std::vector<char> SliceInputBlock(size_t want, int &read);

  int Open();
  void Close();
  int Run();
  void Cancel();

  void SetChatbot(const char *chatbot) {
    chatbot_[0] = chatbot;
    chatbot_[1] = chatbot;
  }

  void SetAuthToken(const char *token) {
    if (token == nullptr)
      auth_token_ = "";
    else
      auth_token_ = token;
    // 기존 세션 ID를 정리한다.
    session_id_ = 0;
  }
  void SetPreferredLanguage(const char *lang) {
    language_ = lang;
  }
  void SetAudioQuality(int quality) {
    audio_quality_ = quality;
  }
  const char *GetMeta(const char *key) {
    auto got = meta_.find(key);
    if (got != meta_.end()) {
      return got->second.c_str();
    }
    return nullptr;
  }
 private:
  struct AsyncCall {
    AsyncCall() = default;
    ClientContext ctx;
    CompletionQueue q;
    Status status;
    AudioUtter utter_recv;
    std::unique_ptr<ClientAsyncReaderWriter<AudioUtter, AudioUtter>> rw_;
    std::queue<AudioUtter> write_queue_;
    bool ready_to_send = false;
    bool write_direct = false;
    int writes_call_count = 0;
    int writes_event_count = 0;
    bool writes_done_signaled = false;
    bool finished = false;
    size_t sent_length = 0;
    size_t recv_length = 0;
  };

  std::unique_ptr<DialogService::Stub> stub_;
  int turn_ = 0;
  string chatbot_[2] = {"meari", "chorong"};
  int64 session_id_ = 0;
  string auth_token_;
  timespec last_recv_;
  bool last_404_ = false;
  AsyncCall *call_ = nullptr;
#if __LOG
  std::shared_ptr<spdlog::logger> log_;
#endif

  enum CompletionTag {
    CT_START = 1000,
    CT_WRITE = 1,
    CT_WRITE_DONE = 2,
    CT_READ = 3,
    CT_FINISH = 4
  };

  std::mutex mutex_;
  std::mutex recv_mutex_;
  std::condition_variable condvar_;

  std::string NextEvent();
  void CheckSession();
  void TalkOpen();
  void TalkSend(const string &next);
  void TalkSendDone();
  int TalkRecv();
  void TalkShutdown();
  std::vector<char> recv_buffer_;
  bool recv_finished_ = false;
  bool send_queue_started_ = false;
  std::queue<string> send_queue_;
  std::string language_ = "kor";
  int audio_quality_ = 16000;
  std::multimap<string, string> meta_;

 public:
  static constexpr const char *kEventStart = "<start>";
  static constexpr const char *kEventDone = "<done>";
  static constexpr const char *kEventExit = "<exit>";

  void Start();

};

#endif
