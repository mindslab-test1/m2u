#ifndef LIB_ADC_H
#define LIB_ADC_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void *adc_t;
#include <stdint.h>

/**
 * 내부적인 환경설정을 초기화한다.
 * 프로그램 초기에 호출해줘야 한다.
 */
extern void AdcConfigInit(int argc, char **argv);

/**
 * 원격의 음성 대화를 위한 클라이언트를 생성한다.
 *
 * @param remote_addr 원격 주소
 * @return Async Dialog Client 인스턴스
 */
extern adc_t AdcInit(const char *remote_addr);

/**
 * 새로운 대화 세션을 생성한다.
 *
 * 대화세션을 생성할 때, meari 또는 chorong과 연결한다.
 * 인증 토큰이 없을 경우에는 meari로 접속하고
 * 인증 토큰이 있는 경우에는 로그인이 성공하면 chorong과 연결한다.
 *
 * 제 3의 서비스 그룹이 지정되면, 인증 모드나 비인증모드나 공히
 * 같은 서비스 그룹으로 접근을 시도한다.
 *
 * @param adc 핸들 포인터
 * @param auth_token 인증 토큰 또는 nullptr
 * @param chatbot 기본적으로 null, 특별한 경우에 지정함.
 * @return  세션을 성공적으로 열면 0을 리턴한다.
 * 제대로 열지 못하면, -1을 리턴한다.
 *
 * @remark 만일 -1을 리턴하는 경우에는 일정한 시간이 지나서 다시 재시도 하거나
 * 다른 이벤트 조건으로 넘어가야 한다.
 */
extern int AdcOpen(adc_t adc, const char *auth_token, const char *chatbot);

/**
 * 새로운 인증 토콘을 지정한다.
 *
 * 새로운 인증 토큰을 지정하면, 기존 세션 정보를 날린다.
 * 새로운 메시지는 새 세션에서 처리된다.
 *
 * 인증토큰이 비정상적이면 영원히 대화를 나눌 수 없다.
 * 인증토큰이 nullptr 이면 `meari`와는 대화를 나눌 수 있다.
 *
 * @param adc 핸들 포인터
 * @param auth_token 새 인증 토큰 또는 nullptr
 */
void AdcSetAuthToken(adc_t adc, const char *auth_token);

/**
 * 선호하는 언어를 지정한다.
 *
 * @param adc 핸들 포인터
 * @param lang 선호하는 언어를 지정한다.
 * 언어 이름은 ISO 639-2에 지정된 언어로 표현하도록 한다.
 * 현재 지원하는 언어는 "kor", "eng"이다.
 * 지정하지 않으면 "Kor"이 기본적으로 동작한다.
 */
extern void AdcSetLanguage(adc_t adc, const char *lang);

/**
 * 음성입력의 품질을 지정한다.
 *
 * @param adc 핸들 포인터
 * @param sampleRate 음성언어의 입력 선호도를 지정한다.
 * 현재는 16000, 8000 두가지 값만을 사용한다.
 * 만일 아무런 값도 지정되지 않으면 8000을 사용한다.
 */
extern void AdcSetAudioQuality(adc_t adc, int sampleRate);

/**
 * 데이터 블럭을 내보낸다.
 *
 * @param adc 핸들 포인터
 * @param outbuf 음성 입력 버퍼
 * @param count 보낼 버퍼의 개수, 길이가 아니므로 버퍼에 있는 int16_t의 개수.
 *
 * @return 의미 없음
 */
extern int AdcSend(adc_t adc, const int16_t *inbuf, unsigned long count);

/**
 * 음성 대화의 입력이 종료된다.
 *
 * @param adc 핸들 포인터
 * @return 의미 없음
 */
extern int AdcSendDone(adc_t);

/**
 * 음성 대화를 임의로 종료한다.
 *
 * @param adc 핸들 포인터
 * @return 의미 없음
 */
extern int AdcCancel(adc_t adc);

/**
 * 데이터를 읽어 들인다.
 *
 * @param adc 핸들력 포인터
 * @param outbuf 음성 출력용 버퍼, 바이트 단위이다.
 * @param want 가져가고 싶은 길이
 *
 * CAUTION: 가능하먼 블럭 단위로 처리하는 것이 속도에 도움이 된다.
 *
 * @return 받은 길이
 * 더 이상 받을 내용이 없을 경우에는 0을 반환한다.
 * 더 받을 내용이 존재하지만 현재는 버퍼가 비어있는 경우에는
 * -1을 반환한다.
 */

extern int AdcRead(adc_t adc, char *outbuf, unsigned long want);

/**
 * 데이터를 읽은 후 서버의 메타 데이터를 조회한다.
 *
 * @param adc 핸들력 포인터
 * @param key 검색할 키
 *
 * 예를 들어서 음악서비스의 경우에는 "music-play.url"을 준다.
 *
 * @return 메타데이터에 대한 키이다. 이 변수는 필요한 만큼 복제하여
 * 사용하고 이 포인터를 다른 용어로 사용해서는 안된다.
 * 해당하는 값이 없을 경우에 NULL을 반환한다.
 */
extern const char *AdcGetMeta(adc_t adc, const char *key);

/**
 * @param adc 무한 실행할 AsyncDialogClient의 인스턴스
 *
 * @return 의미 없음
 */
extern int AdcStart(adc_t adc);

#ifdef __cplusplus
};
#endif

#endif
