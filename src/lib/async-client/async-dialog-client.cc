#include "async-dialog-client.h"
#include <thread>
#include <libmaum/common/base64.h>
//
// GRPC 처리 루틴들
//

using maum::m2u::facade::AccessFrom;

int AsyncDialogClient::Open() {
  Caller caller;
  Session session;
  caller.set_accessfrom(AccessFrom::SPEAKER);
  caller.set_name("ASCAR");
  ClientContext context;
  if (!auth_token_.empty()) {
    context.AddMetadata("x-m2u-authentication-token", auth_token_);
    context.AddMetadata("x-m2u-authentication-provider", "mindslab");
    caller.set_chatbot(chatbot_[OPEN_WITH_AUTH]);
  } else {
    caller.set_chatbot(chatbot_[OPEN_WITHOUT_AUTH]);
  }

  Status status = stub_->Open(&context, caller, &session);
  if (status.ok()) {
    session_id_ = session.id();
    clock_gettime(CLOCK_REALTIME, &last_recv_);
    return 0;
  } else {
#if __LOG
    log_->warn("cannot open session, {} {}",
               status.error_code(), status.error_message());
    return -1;
#endif
    return -1;
  }
}

void AsyncDialogClient::Close() {
  ClientContext ctx;
  SessionKey key;
  SessionStat stat;
  key.set_session_id(session_id_);
  Status ret = stub_->Close(&ctx, key, &stat);
  if (ret.ok()) {
#if __LOG
    log_->debug() << "Session closed!";
#endif
    // TODO
  } else {
#if __LOG
    log_->debug() << "An error when closing!";
#endif
  }
}


//
// THREAD EVENT
//

std::string AsyncDialogClient::NextEvent() {
  std::unique_lock<std::mutex> lk(mutex_);
  if (!call_) {
    condvar_.wait(lk, [&] { return send_queue_.size() > 0; });
    string one = (string &&) send_queue_.front();
#if __LOG
    log_->debug("!call wake up event found len: {}", one.size());
#endif
    send_queue_.pop();
#if __LOG
    log_->debug("!call wake up queue size {}", send_queue_.size());
#endif
    return one;
  } else {
    using namespace std::chrono;
    steady_clock::time_point tp = steady_clock::now() + microseconds(1000);

    bool ret = condvar_.wait_until(
        lk, tp, [&] {
          return (send_queue_.size() > 0) && (call_->ready_to_send);
        });
    if (!ret) {
#if __LOG
      log_->trace("wake up not found, time out");
#endif
      return ""; /// 빈 문자열을 반환한다. 다음 동작은 그냥 모니터링,
    } else {
      auto one = (string &&) send_queue_.front();
#if __LOG
      log_->debug("wake up event until found len:{}", one.size());
#endif
      send_queue_.pop();
#if __LOG
      log_->debug("wake up event until queue size {}", send_queue_.size());
#endif
      return one;
    }
  }
}

/**
 * 쓰레드 메인 함수.
 *
 * 지속적으로 데이터를 읽어들이고, 데이터를 읽고 내보낸다.
 */
int AsyncDialogClient::Run() {
  while (true) {
    ///
    /// 네트워크로 내보내는 출력 명령을 대기 단계
    ///

    // NextEvent에서는
    //   원격에 대한 호출이 있을 때에는 바로 다음 호출이 있는지 대기하고
    //   원격에 대한 호출이 없을 때에는 무한 대기한다.
    string event = NextEvent();
    if (!event.empty()) {
      // 실지로는 호출되지 않는다.
      if (event == kEventExit) {
        return 0;
      }
      if (event == kEventStart) {
        TalkOpen();
      } else if (event == kEventDone) {
        if (call_)
          TalkSendDone();
      } else {
        if (call_) {
          TalkSend(event);
        }
      }
    }
    ///
    /// 네트워크로부터 입력을 기다리는 단계
    ///
    if (call_) {
      TalkRecv();
    }
  }
}

void AsyncDialogClient::Cancel() {
  if (call_) {
    call_->ctx.TryCancel();
    call_->q.Shutdown();
  }
}

const long kOneSedNsec = 1000000000;
const long kTenMinutes = (60 * 10) - 5;

void AsyncDialogClient::CheckSession() {
  timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  // update elapsed time calc
  int64 temp_sec = now.tv_sec - last_recv_.tv_sec;
  int64 temp_nsec = now.tv_nsec - last_recv_.tv_nsec;

  if (temp_nsec < 0) {
    temp_sec--;
    temp_nsec += kOneSedNsec;
    // 0.5 초면 1초로 올려준다.
    if (temp_nsec > (kOneSedNsec / 2))
      temp_sec++;
  }

  // 지난번 대화 이후로 10분에 가까워지면 새로운 세션을 연다.
  if (temp_sec >= kTenMinutes || last_404_ || session_id_ == 0) {
    last_404_ = false;
    Open();
    clock_gettime(CLOCK_REALTIME, &last_recv_);
    turn_ = 1;
  } else
    turn_++;
}

void AsyncDialogClient::TalkOpen() {
  if (call_) {
    std::cout << "OOPS" << std::endl;
    return;
  }

  // 만일 그전에 읽지 않고 남은 기존 데이터가 있다면, 모두 버리도록 한다.
  if (!recv_buffer_.empty()) {
#if __LOG
    log_->info("prev call has remained buffers {}", recv_buffer_.size());
#endif
    recv_buffer_.clear();
  }
  recv_finished_ = false;

#if __LOG
  log_->debug("GetTalkStream()");
#endif
  CheckSession();
  meta_.clear();

  call_ = new AsyncCall;
  std::cout << "NEW CALL : " << turn_ << std::endl;
#if __LOG
  log_->debug() << "new AsyncCall() " << (void *) call_;
#endif
  call_->ctx.AddMetadata("in.sessionid", std::to_string(session_id_));
  if (!language_.empty()) {
    call_->ctx.AddMetadata("in.lang", language_);
  }
  call_->ctx.AddMetadata("in.samplerate", std::to_string(audio_quality_));
  call_->rw_ = stub_->AsyncAudioTalk(&call_->ctx, &call_->q, (void *) 1000);
#if __LOG
  log_->debug("calling finish()");
#endif
  call_->rw_->Finish(&call_->status, (void *) 4);
}

void AsyncDialogClient::TalkSend(const string &buffer) {
  // 맨처음 한번은 WRITE를 호출해줘야 한다.
  AudioUtter utter;
  utter.set_utter(buffer);
  call_->sent_length += buffer.size();
#if __LOG
  log_->trace("trying to send msg {}", buffer.size());
#endif
  if (!call_->ready_to_send || call_->writes_call_count > 0) {
    call_->write_queue_.push(utter);
#if __LOG
    log_->debug("add write queue_ queue:size {}, buffer:size {}",
                call_->write_queue_.size(), buffer.size());
#endif
  } else {
    call_->rw_->Write(utter, (void *) 1); // CT_WRITE
    call_->writes_call_count++;
#if __LOG
    log_->debug("talk sent {} {}", buffer.size(), call_->writes_call_count);
#endif
  }
}

void AsyncDialogClient::TalkSendDone() {
#if __LOG
  log_->debug("talk send done signaled");
#endif
  call_->writes_done_signaled = true;
  // call_->rw_->WritesDone((void *) 2);
}

void AsyncDialogClient::TalkShutdown() {
#if __LOG
  log_->debug("SHUTDOWN returns");
#endif
  std::lock_guard<std::mutex> lock_guard(mutex_);
  delete call_;
  call_ = nullptr;
  std::cout << "STARTED = false" << std::endl;
  send_queue_started_ = false;
}

int AsyncDialogClient::TalkRecv() {
  void *tag;
  bool ok;

  if (call_->write_queue_.size() > 0 && (
      call_->writes_call_count == call_->writes_event_count)) {
    AudioUtter one = (AudioUtter &&) call_->write_queue_.front();
    call_->write_queue_.pop();
    call_->rw_->Write(one, (void *) 1); // CT_WRITE
    call_->writes_call_count++;
#if __LOG
    log_->debug("QUICK WRITE!  {} {} queued:{}, direct {}",
                call_->writes_call_count,
                call_->writes_event_count,
                call_->write_queue_.size(),
                call_->write_direct);
#endif
    return 0;
  }

  if (call_->writes_done_signaled && (
      call_->writes_call_count == call_->writes_event_count)) {
    call_->rw_->WritesDone((void *) 2);
#if __LOG
    log_->debug("QUICK WRITES DONE! {} {}",
                call_->writes_call_count,
                call_->writes_event_count);
#endif
    call_->writes_done_signaled = false;
    // 꼭 여기서 리턴할 필요가 없기는 하지만 큰 문제가 없으므로 반환한다.
    return 0;
  }

  grpc::CompletionQueue::NextStatus
      next = call_->q.AsyncNext(&tag, &ok, gpr_time_0(GPR_CLOCK_REALTIME));
  // log_->debug() << "q.asyncnext returns " << next << ", ok = " << ok;
  switch (next) {
    case grpc::CompletionQueue::SHUTDOWN: {
      std::cout << "  ASYNC event SHUTDOWN" << std::endl;
      TalkShutdown();
      return -1;
    }
    case grpc::CompletionQueue::GOT_EVENT: {
#if INTPTR_MAX == INT32_MAX
      int32_t tag_val = reinterpret_cast<int32_t >(tag);
#else
      int64_t tag_val = reinterpret_cast<int64_t >(tag);
#endif
#if __LOG
      log_->debug() << "tag val ::" << tag_val << ", ok " << ok;
#endif
      if (!ok) {
        return -1;
      }
      switch (tag_val) {
        case CT_START: {
#if __LOG
          log_->debug("STARTED!, set ready to send = true");
#endif
          std::cout << "  ASYNC event STARTED" << std::endl;
          call_->ready_to_send = true;
          break;
        }
        case CT_WRITE: {
          call_->writes_event_count++;
          if (call_->write_queue_.size() > 0) {
            AudioUtter one = (AudioUtter &&) call_->write_queue_.front();
            call_->write_queue_.pop();
            call_->rw_->Write(one, (void *) 1); // CT_WRITE
            call_->writes_call_count++;
#if __LOG
            log_->debug("write event! {} {} queued:{} ",
                        call_->writes_call_count,
                        call_->writes_event_count,
                        call_->write_queue_.size());
#endif
            call_->write_direct = false;
            break;
          } else {
#if __LOG
            log_->debug("write queue empty! {} {} queued:{} ",
                        call_->writes_call_count,
                        call_->writes_event_count,
                        call_->write_queue_.size());
#endif
            call_->write_direct = true;
          }
#if __LOG
          log_->debug("write event! {} {}, signaled {}", call_->writes_call_count,
                      call_->writes_event_count, call_->writes_done_signaled);
#endif
          if (call_->writes_done_signaled &&
              call_->writes_call_count == call_->writes_event_count) {
            call_->rw_->WritesDone((void *) 2);
#if __LOG
            log_->debug("LAZY WRITES DONE! {} {}", call_->writes_call_count,
             call_->writes_event_count);
#endif
            call_->writes_done_signaled = false;
          }
          break;
        }
        case CT_WRITE_DONE: {
          std::cout << "  ASYNC event WRITE DONE, READ FIRST" << std::endl;
          call_->rw_->Read(&call_->utter_recv, (void *) 3);
          break;
        }
        case CT_READ: {
          std::cout << "  ASYNC event READ" << std::endl;
          if (call_->finished) {
#if __LOG
            log_->debug("already finished break : recv size: {}",
                        call_->utter_recv.utter().size());
#endif
            break;
          }
          const auto &utter = call_->utter_recv.utter();
          call_->recv_length += utter.size();
          {
            std::lock_guard<std::mutex> lock_guard(recv_mutex_);
            recv_buffer_.insert(recv_buffer_.end(),
                                utter.begin(), utter.end());
            recv_finished_ = false;
            call_->utter_recv.clear_utter();

          }
#if __LOG
          log_->debug("after recv buffer copied : {}", recv_buffer_.size());
#endif
          call_->utter_recv.clear_utter();
          // 다음 읽기를 대기한다.
          call_->rw_->Read(&call_->utter_recv, (void *) 3);
          break;
        }
        case CT_FINISH: {
          std::cout << "  ASYNC event FINISH" << std::endl;
          call_->finished = true;
          {
            std::lock_guard<std::mutex> lock_guard(recv_mutex_);
            recv_finished_ = true;
          }

          clock_gettime(CLOCK_REALTIME, &last_recv_);

#if __LOG
          log_->debug("sent_length {}, recv_length {}",
                      call_->sent_length, call_->recv_length);
#endif
          // 모든 메타 데이터를 다 복제해서 제공해 준다.
          auto init_meta = call_->ctx.GetServerInitialMetadata();
          for (const auto &kv: init_meta) {
            string k(kv.first.begin(), kv.first.end());
            string v = Base64Decode(string(kv.second.begin(), kv.second.end()));

            meta_.insert(std::make_pair(k, v));
          }

          auto server_map = call_->ctx.GetServerTrailingMetadata();
          auto it = server_map.find("talk.error");
          if (it != server_map.end()) {
#if __LOG
            int err = std::stoi(string(it->second.begin(), it->second.end()));
            // TODO
            // WHAT ???
            log_->debug("server error = {}", err);
#endif
          }

          if (!call_->status.ok()) {
#if __LOG
            log_->debug()
                << "rpc failed "
                << ", Code: " << call_->status.error_code()
                << ", Message: " << call_->status.error_message();
#endif
            // 문제가 발생하면 자동으로 새 세션을 열도록 한다.
            //

            if (call_->status.error_code() == grpc::StatusCode::NOT_FOUND) {
              last_404_ = true;
            } else if (call_->status.error_code()
                == grpc::StatusCode::UNAUTHENTICATED) {
              // 다음 대화에서는 'meari' 와 진행할 수 있도록 한다.
              auth_token_.clear();
              session_id_ = 0;
            }
          }
          call_->q.Shutdown();
          break;
        }
        default: {
          break;
        }
      }
      break;
    }
    case grpc::CompletionQueue::TIMEOUT: {
      // log_->debug("timeout ");
      return -1;
    }
  }
  return (int) recv_buffer_.size();
}

//
// AudioFront CLASS 동기화
//
void AsyncDialogClient::AppendOutputBlock(const int16_t *block,
                                          size_t count) {
  {
    std::lock_guard<std::mutex> lock_guard(mutex_);
    if (!send_queue_started_) {
      std::cout << "ENQEUE START" << std::endl;
      send_queue_.push((string &&) kEventStart);
      send_queue_started_ = true;
    }
    std::string val(reinterpret_cast<const char *>(block),
                    count * sizeof(int16_t));
    send_queue_.push(val);
#if __LOG
    log_->debug("send_queue size = {}", send_queue_.size());
#endif
  }

  condvar_.notify_one();
}

void AsyncDialogClient::AppendOutputBlock(const std::string &command) {
  {
    std::lock_guard<std::mutex> lock_guard(mutex_);
    send_queue_.push(command);
#if __LOG
    log_->debug("send_queue size = {}", send_queue_.size());
#endif
  }
  condvar_.notify_one();
}

std::vector<char> AsyncDialogClient::SliceInputBlock(size_t want, int &read) {
  std::lock_guard<std::mutex> lock_guard(recv_mutex_);
  std::vector<char>::size_type sz = recv_buffer_.size();
#if __LOG
  log_->trace("before recv buffer size = {} {}", sz, recv_buffer_.size());
#endif
  if (sz > want) {
    read = (int) want;
    std::vector<char> ret;
#if __LOG
    log_->trace("before recv buffer size = {}", recv_buffer_.size());
#endif
    auto it = std::next(recv_buffer_.begin(), want);
    std::move(recv_buffer_.begin(), it, std::back_inserter(ret));
    recv_buffer_.erase(recv_buffer_.begin(), it);
#if __LOG
    log_->trace("copied buffer size = {}", ret.size());
    log_->trace("copied after buffer size = {}", recv_buffer_.size());
#endif
    return ret;
  } else if (sz == 0) {
    //  현재의 상태를 살펴봐야 한다.
    if (recv_finished_) {
      read = 0;
    } else {
      read = -1;
    }

    return std::vector<char>();
  } else {
#if __LOG
    log_->trace("before recv buffer size = {}", recv_buffer_.size());
#endif
    std::vector<char> ret;
    std::move(recv_buffer_.begin(),
              recv_buffer_.end(),
              std::back_inserter(ret));
#if __LOG
    log_->trace("moved buffer size = {}, sz = {}", ret.size(), sz);
    log_->trace("moved after buffer size = {}", recv_buffer_.size());
#endif
    recv_buffer_.clear();
    read = (int) ret.size();
    return ret;
  }
}

void AsyncDialogClient::Start() {
  std::thread thread(&AsyncDialogClient::Run, this);
  thread.detach();
}
