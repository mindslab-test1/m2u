#include "adc.h"
#include "async-dialog-client.h"
#include <vector>
#include <libmaum/common/config.h>

extern "C" {

void AdcConfigInit(int argc, char **argv) {
  libmaum::Config::Init(argc, argv, "m2u.conf");
}

adc_t AdcInit(const char *remote_addr) {
  std::shared_ptr<Channel> chan =
      grpc::CreateChannel(remote_addr, grpc::InsecureChannelCredentials());
  AsyncDialogClient *client = new AsyncDialogClient(chan);
  return (adc_t) client;
}

int AdcOpen(adc_t adc, const char *auth_token, const char *chatbot) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);

  if (auth_token != nullptr && *auth_token)
    client->SetAuthToken(auth_token);
  client->SetAuthToken(auth_token);
  if (chatbot != nullptr && *chatbot)
    client->SetChatbot(chatbot);

  return client->Open();
}

void AdcSetAuthToken(adc_t adc, const char *auth_token) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  client->SetAuthToken(auth_token);
}

void AdcSetLanguage(adc_t adc, const char *lang) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  client->SetPreferredLanguage(lang);
}

void AdcSetAudioQuality(adc_t adc, int sampleRate) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  client->SetAudioQuality(sampleRate);
}

int AdcSend(adc_t adc, const int16_t *inbuf, unsigned long count) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  client->AppendOutputBlock(inbuf, count);
  return 0;
}

int AdcSendDone(adc_t adc) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  client->AppendOutputBlock("<done>");
  return 0;
}

int AdcRead(adc_t adc, char *outbuf, unsigned long want) {
  int read;
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  std::vector<char> got = client->SliceInputBlock(want, read);
  if (read > 0) {
    memcpy((void *) outbuf, got.data(), got.size());
  }
  return read;
}

int AdcCancel(adc_t adc) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  client->Cancel();
  return 0;
}

const char *AdcGetMeta(adc_t adc, const char *key) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  return client->GetMeta(key);
}

int AdcStart(adc_t adc) {
  AsyncDialogClient *client = reinterpret_cast<AsyncDialogClient *>(adc);
  client->Start();
  return 0;
}

}
