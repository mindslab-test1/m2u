#ifndef LIB_DA_REG_CLIENT_H
#define LIB_DA_REG_CLIENT_H

#include <grpc++/grpc++.h>
#include <google/protobuf/timestamp.pb.h>

#include <maum/common/lang.pb.h>
#include <maum/m2u/server/pool.grpc.pb.h>

using std::string;
using grpc::Channel;
using google::protobuf::Timestamp;
using maum::m2u::server::DialogAgentInstancePool;
using maum::m2u::server::DialogAgentInstanceResource;

inline maum::common::LangCode ToLangCode(const string& lang) {
  maum::common::LangCode lc;
  if (!maum::common::LangCode_Parse(lang, &lc)) {
    // 예외를 던지지 않고 기본 언어로 사용한다.
    // throw std::invalid_argument(lang + "is not acceptable!");
    lc = maum::common::LangCode::kor;
  }
  return lc;
}

class DialogAgentInstancePoolClient {
 public:
  DialogAgentInstancePoolClient(std::shared_ptr<Channel> channel)
      : stub_(DialogAgentInstancePool::NewStub(channel)) {
  }

  void Register(DialogAgentInstanceResource &res);

  void Unregister();

  const std::vector<string> &keys() {
    return keys_;
  }

 private:
  std::unique_ptr<DialogAgentInstancePool::Stub> stub_;
  std::vector<string> keys_;
  Timestamp timestamp_;
};

#endif
