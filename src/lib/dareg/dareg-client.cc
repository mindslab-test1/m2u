#include <iostream>
#include <grpc++/support/status.h>
#include "dareg-client.h"
#include <libmaum/common/config.h>

using std::cout;
using std::endl;
using grpc::ClientContext;
using grpc::Status;
using maum::m2u::server::DialogAgentInstanceStat;
using maum::m2u::server::RegisterResponse;
using maum::m2u::server::DialogAgentInstanceKey;

void DialogAgentInstancePoolClient::Register(DialogAgentInstanceResource &res) {
  ClientContext ctx;
  RegisterResponse rr;
  Status ret = stub_->Register(&ctx, res, &rr);
  if (ret.ok()) {
    keys_.push_back(rr.key());
    timestamp_ = rr.registered_at();
    LOGGER()->debug("newly registered key [{}] at {}",
                    rr.key(), timestamp_.seconds());
  }
}

void DialogAgentInstancePoolClient::Unregister() {
  if (keys_.empty()) {
    return;
  }
  for (auto k: keys_) {
    ClientContext ctx;
    DialogAgentInstanceKey key;
    DialogAgentInstanceStat stat;

    key.set_key(k);
    Status ret = stub_->Unregister(&ctx, key, &stat);
    if (ret.ok()) {
#if 0
      LOGGER()->debug("unregistered with key {}", key_);
#endif
    }
  }
  keys_.clear();
}
