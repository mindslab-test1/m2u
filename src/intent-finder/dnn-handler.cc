#include <libmaum/common/config.h>
#include "dnn-handler.h"
#include "tls-value.h"
#include "libmaum/brain/error/ta-error.h"

DnnHandler::DnnHandler() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  rsc_path_ = c.Get("brain-ta.resource.2.kor.path");
  rsc_eng_path_ = c.Get("brain-ta.resource.2.eng.path");
}

/**
 * DNN  리소스 로딩 함수
 *
 * @return is_done 로딩 완료 여부
 */
bool DnnHandler::Init() {
  return true;
}

/**
 * DNN 모델 파일 로딩
 *
 * @param model_path DNN 모델 경로
 * @return is_done 모델로드 완료 여부
 */
bool DnnHandler::LoadModel(const string &model,
                           maum::common::Lang lang) {
  const int feature = (1 << NlpFeature::NLPF_MORPHEME);
  auto logger = LOGGER();
  if (lang == maum::common::Lang::ko_KR) { // 한국어
    // 나중에는 map의 처리 대상이 다변화되어야 한다.
    string model_path = GetModelPath(model, lang);
    auto got = kor_dnn_anals_.find(model_path);

    // 존재하고 있지 않은 모델에 대해서만 추가한다.
    if (got == kor_dnn_anals_.end()) {
      auto anal = std::make_shared<Nlp2Kor>(rsc_path_, feature);
      anal->UpdateFeatures();
      try {
        anal->SetClassifierModel(model_path);
      } catch (ta::Exception &te) {
        LOGGER_error("Can't insert dnn model {}, {}:{}",
                     model_path,
                     model,
                     lang);
        throw;
      }
      kor_dnn_anals_.emplace(model_path, anal);
      // kor_dnn_anals_.insert(make_pair(model_path, anal));
      logger_debug("inserting dnn model {}, {}:{}", model_path, model, lang);
    }
  } else if (lang == maum::common::Lang::en_US) { // 영어
    string model_path = GetModelPath(model, lang);
    auto got = eng_dnn_anals_.find(model_path);

    // 존재하고 있지 않은 모델에 대해서만 추가한다.
    if (got == eng_dnn_anals_.end()) {
      auto anal = std::make_shared<Nlp2Eng>(rsc_eng_path_, feature);
      anal->UpdateFeatures();
      try {
        anal->SetClassifierModel(model_path);
      } catch (ta::Exception &te) {
        LOGGER_error("Can't insert dnn model {}, {}:{}",
                     model_path,
                     model,
                     lang);
        throw;
      }
      eng_dnn_anals_.emplace(model_path, anal);
      // eng_dnn_anals_.insert(make_pair(model_path, anal));
      logger_debug("inserting dnn model {}, {}:{}", model_path, model, lang);
    }
  }
  return true;
}

/**
 * DNN 분류  수행 함수
 * @param document 언어분석결과 담긴 Document 정보
 * @param result DNN 분류결과 정보
 * @return is_done DNN 분류 완료 여부
 */
bool DnnHandler::Classify(const string &model,
                          maum::common::Lang lang,
                          const Document &document, ClassifiedSummary *result) {
  string model_path = GetModelPath(model, lang);
  if (lang == maum::common::Lang::ko_KR) { // 한국어
    auto got = kor_dnn_anals_.find(model_path);
    if (got != kor_dnn_anals_.end()) {
      got->second->Classify(document, result);
      return true;
    } else {
      auto logger = LOGGER();
      logger_warn("DNN, cannot find model {} {}", model, lang);
    }
  } else if (lang == maum::common::Lang::en_US) { // 영어
    auto got = eng_dnn_anals_.find(model_path);
    if (got != eng_dnn_anals_.end()) {
      got->second->Classify(document, result);
      return true;
    } else {
      auto logger = LOGGER();
      logger_warn("DNN, cannot find model {} {}", model, lang);
    }
  }
  return false;
}
