#ifndef INTENT_FINDER__SVC__IMPL_H
#define INTENT_FINDER__SVC__IMPL_H

#include <maum/m2u/router/v3/intentfinder.grpc.pb.h>
#include "tls-value.h"
#include "sc-handler.h"
#include "classification-record.h"

using maum::m2u::router::v3::FindIntentRequest;
using maum::m2u::router::v3::FindIntentResponse;
using maum::m2u::router::v3::FoundIntent;

using ::grpc::Status;
using ::grpc::ServerContext;
using ::grpc::ClientContext;

/**
 *
 */
class IntentFinderImpl : public maum::m2u::router::v3::IntentFinder::Service {
 public:
  IntentFinderImpl() = default;
  ~IntentFinderImpl();

  bool Init(const string &name);
  bool Load();

  Status FindIntent(ServerContext *context,
                    const FindIntentRequest *request,
                    FindIntentResponse *response) override;
  Status RetryFindIntent(ServerContext *context,
                         const FindIntentRequest *request,
                         RetryFindIntentResponse *response) override;
  Status Analyze(ServerContext *context,
                 const FindIntentRequest *request,
                 FindIntentResponse *response) override;

  string GetItfIp() {
    return itf_ip_;
  }

 private:
  bool CheckOperationSyncId(const ServerContext *context);
  const Status SyncIdMissing(const ServerContext *context);
  bool CheckStepSkip(const IntentFindingStep &step, const string &skill);
  void FillRecord(ClassificationRecord &record,
                  const FindIntentRequest *request,
                  const Replacement &replace);
  Status SubFindIntent(const FindIntentRequest *org_request,
                       FindIntentResponse *response,
                       RetryFindIntentResponse *retry_response,
                       bool retry_option = false);

  string name_;
  string policy_name_;
  SimpleClassifierHandler sc_handler_;
  // itf instance가 실행되는 서버 주소를 저장할 변수
  string itf_ip_;
  // ITF POLICY는 런타임시에 변경되면 로딩해야할 모델 정보들이 변경되어서 오동작할 위함이
  // 큽니다. 또한, 한번 정해놓은 ITF 정책은 잘 변경하지 않습니다. 거의 프로그램의 배포와
  // 비슷한 수준에서 관리되어야 합니다.
  shared_ptr<LocalIntentFinderPolicy> itf_policy_;
};

#endif //INTENT_FINDER__SVC__IMPL_H
