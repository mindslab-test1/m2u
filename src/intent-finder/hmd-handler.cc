#include <libmaum/common/config.h>
#include "hmd-handler.h"
#include "tls-value.h"

using grpc::ClientContext;
using grpc::Status;
using std::string;
using maum::brain::nlp::InputText;
using maum::brain::nlp::NaturalLanguageProcessingService;
using maum::common::LangCode;
using maum::brain::hmd::HmdInputText;
using maum::brain::hmd::HmdModelList;

/**
 * HMD 분류에 필요한 리소스 로딩 및 초기화하는 함수
 *
 * @return 로딩 완료 여부
 */
bool HmdHandler::Init() {
  bool is_done = false;
  auto logger = LOGGER();
  logger_debug(">>> [Initialize HmdHandler] Start");
  auto &c = libmaum::Config::Instance();

  k_cl_ = KorHmd();
  e_cl_ = EngHmd();

  ml_.SetModelDir(c.Get("brain-ta.hmd.model.dir"));
  vector<HmdModel> models;
  is_started_ = true;
  logger_debug("<<< [Initialize HmdHandler] End");
  is_done = true;
  return is_done;
}

bool HmdHandler::LoadModels(const set<HmdModelInfo> &models) {
  vector<HmdMatrix> matrix_vec;
  auto logger = LOGGER();
  int cnt = 0;

  for (const auto &m: models) {
    HmdMatrix hmd_mat;
    logger_trace("try load hmd matrix [{}] - {}", m.model, m.lang);
    ml_.LoadMatrixModel(m.model,
                        LangCode(m.lang),
                        hmd_mat);
    if (!hmd_mat.model().empty()) {
      if (m.lang == Lang::ko_KR)
        k_cl_.SetMatrix(hmd_mat);
      else if (m.lang == Lang::en_US)
        e_cl_.SetMatrix(hmd_mat);
      ++cnt;
      logger_info("load hmd matrix [{}] - {} done", m.model, m.lang);
    }
  }

  logger_info("Load All Hmd Matrix model completely. (cnt: {})", cnt);
  return true;
}

/**
 * Hmd 분류 수행 함수
 *
 * @param request  언어분석결과가 포함된 HmdInputDocument 데이터
 * @param result Hmd 분류 결과가 담길 변수
 * @return is_done Hmd 분류 완료 여부
 */
bool HmdHandler::Classify(HmdInputDocument *request, HmdResult *result) {
  auto logger = LOGGER();
  bool is_done = false;
  const string &model = request->model();
  logger_debug("<<< [HmdHandler Classify] Start");
  if (!is_started_) {
    logger_debug("<<< [HmdHandler Classify] End, not started", is_started_);
    return false;
  }

  HmdResult res;
  bool has = false;
  if (request->lang() == LangCode::kor) {
    vector<HmdClassified> k_cls_vec;
    has = k_cl_.HasMatrix(model);
    if (!has) {
      logger_debug("<<< [HmdHandler Classify] End, no matrix, {}, {}, {}",
                   has, model, request->lang());
      return false;
    }
    k_cls_vec = k_cl_.ClassifyWithMatrix(model, request->document(), true);
    if (!k_cls_vec.empty()) {
      const auto &k_cls = k_cls_vec.at(0);
      res.add_cls()->CopyFrom(k_cls);
    }
  } else {
    vector<HmdClassified> cls;
    has = e_cl_.HasMatrix(model);
    if (!has) {
      logger_debug("<<< [HmdHandler Classify] End, no matrix, {}, {}, {}",
                   has, model, request->lang());
      return false;
    }
    cls = e_cl_.ClassifyWithMatrix(model, request->document(), true);
    if (!cls.empty()) {
      const auto &e_cls = cls.at(0);
      res.add_cls()->CopyFrom(e_cls);
    }
  }
  if (!res.cls().empty()) {
    result->CopyFrom(res);
    // 가장 첫 번째  결과값 출력.
    logger_debug("<<< [HmdHandler Classify] Find Result:{} in model:{} End.",
                 res.cls(0).category(), model);
    is_done = true;
  } else {
    logger_debug("<<< [HmdHandler Classify] 'Not Found HMD' in model:{} End",
                 model);
  }
  return is_done;
}
