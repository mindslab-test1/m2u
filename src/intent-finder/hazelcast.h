#ifndef INTENT_FINDER_CLASSIFIER_HAZELCAST_H
#define INTENT_FINDER_CLASSIFIER_HAZELCAST_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include <google/protobuf/stubs/port.h>

// Hazelcast namespace
using std::string;
using hazelcast::client::HazelcastClient;
using hazelcast::client::ClientConfig;
using hazelcast::client::exception::IException;
using hazelcast::client::EntryEvent;
using hazelcast::client::EntryListener;
using hazelcast::client::MapEvent;

using google::protobuf::int64;

enum PortableFactoryId {
  PORTABLE_FACTORY_ID = 1
};

// HazelcastTypeId는 중복되지 않도록 enum 으로 선언하여 사용.
enum HazelcastTypeId {
  DIALOG_AGENT_MANAGER = 1,
  DIALOG_AGENT = 2,
  DIALOG_AGENT_ACTIVATION_INFO = 3,
  SIMPLE_CLASSIFIER = 4,
  SKILL = 5,
  CHATBOT = 6,
  CHATBOT_DETAIL = 7,
  AUTHTICATION_POLICY = 8,
  DIALOG_AGENT_INSTANCE = 10,
  DIALOG_AGENT_INSTANCE_EXECUTION_INFO = 11,
  DIALOG_AGENT_INSTANCE_RESOURCE = 12,
  DIALOG_SESSION = 21,
  SESSION_TALK = 22,

  INTENT_FINDER_INSTANCE = 30,
  INTENT_FINDER_POLICY = 31,
  CLASSIFICATION_RECORD = 32
};

#ifndef LOCAL_INTENT_FINDER_H
class LocalIntentFinderInstance;
#endif
#ifndef PROJECT_LOCAL_INTENTFINDER_POLICY_H
class LocalIntentFinderPolicy;
#endif
#ifndef PROJECT_LOCAL_SIMPLECLASSIFIER_H
class LocalSimpleClassifier;
#endif

class ItfHazelcastClient {
  ItfHazelcastClient();
  virtual ~ItfHazelcastClient();

 public:
  static ItfHazelcastClient *GetInstance() {
    static ItfHazelcastClient instance_;
    return &instance_;
  }
  virtual std::shared_ptr<ClientConfig> GetConfig();
  std::shared_ptr<HazelcastClient> GetHazelcastClient();

  std::shared_ptr<LocalIntentFinderInstance> GetIntentFinderInstance(const string &name);
  std::shared_ptr<LocalIntentFinderPolicy> GetIntentFinderPolicy(const string &name);
  std::shared_ptr<LocalSimpleClassifier> GetSimpleClassifier(const string &name);

 public:
  static const char *kNsAuthPolicy;
  static const char *kNsChatbotDetail;
  static const char *kNsDAManager;
  static const char *kNsDA;
  static const char *kNsDAActivation;
  static const char *kNsDAInstance;
  static const char *kNsDAInstExec;
  static const char *kNsIntentFinder;
  static const char *kNsIntentFinderInstExec;

  static const char *kNsSkill;
  static const char *kNsSC;

  static const char *kNsTalk;
  static const char *kNsSession;
  static const char *kNsTalkNext;
  static const char *kNsChatbot;
  static const char *kNsDAIR;
  static const char *kNsIntentFinderInstance;
  static const char *kNsIntentFinderPolicy;
  static const char *kNsClassificationRecord;

  static bool IsReady();
 private:
  int32_t server_cnt_;
  std::list<std::string> server_address_;
  int32_t server_port_;
  std::shared_ptr<HazelcastClient> hazelcast_client_;
  std::string group_name_;
  std::string group_password_;
  long invocation_timeout_;
};

#endif
