#include <libmaum/common/config.h>
#include "custom-script-client.h"
#include "tls-value.h"

/**
 * itf-cs-py-runner 에게 custom-script 파일의 classify 함수를 호출하도록 요청한다.
 *
 * @param request 분류결과 카테고리가 담길 변수
 * @param response 분류결과 카테고리가 담길 변수
 * @return true : 성공, false : 실패
 */
bool CustomScriptClient::Classify(CustomClassifyParam *request,
                                  CustomClassifyResult &response) {
  ClientContext context;
  const string &opsync = GetTlsOpSyncId();
  LOGGER_debug("Add x-operation-sync-id [{}] to meta data.]", opsync);
  context.AddMetadata("x-operation-sync-id", opsync);

  Status status = stub_->Classify(&context, *request, &response);
  if (!status.ok()) {
    LOGGER_debug("client call failed {}, {}",
                 status.error_code(),
                 status.error_message());
    return false;
  }

  return true;
}

