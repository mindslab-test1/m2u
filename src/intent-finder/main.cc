#include <iostream>
#include <libgen.h>
#include <grpc++/grpc++.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <gitversion/version.h>
#include <libmaum/log/call-log.h>
#include <libmaum/rpc/result-status.h>
#include "libmaum/brain/error/ta-error.h"

#include <dlfcn.h>
#include <getopt.h>

#include "hazelcast.h"
#include "google/protobuf/util/json_util.h"
#include "intent-finder-impl.h"
#include "local-simpleclassifier.h"
#include "local-intentfinder.h"
#include "local-intentfinder-policy.h"

using google::protobuf::util::MessageToJsonString;

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using hzc = ItfHazelcastClient;
using hazelcast::client::IMap;
using libmaum::rpc::CommonResultStatus;

using namespace std;

/**
 *
 * @param name
 * @param address
 */
void RunServer(const string &name, const string &address) {
  libmaum::Config &c = libmaum::Config::Instance();

  auto logger = c.GetLogger();
  c.DumpPid();

  //
  // hzc와 연동한다.
  logger->info("[RunServer] intent finder hzc info");
  string policy;

  auto hc = hzc::GetInstance()->GetHazelcastClient();
  {
    auto map = hc->getMap<std::string,
                          LocalIntentFinderInstance>(hzc::kNsIntentFinderInstance);
    boost::shared_ptr<LocalIntentFinderInstance> itf = map.get(name);
    if (itf == nullptr) {
      logger->critical("*****************************************************");
      logger->critical("*  {} Not In kNsIntentFinderInstance Map.  *", name);
      logger->critical("*  Exit IntentFinder.                               *");
      logger->critical("*****************************************************");
      exit(-1);
    }
    string debug;
    MessageToJsonString(*itf, &debug);
    logger->info("itf name={}\n{}", name, debug);
    policy = itf->policy_name();
  }

  {
    auto map = hc->getMap<std::string,
                          LocalIntentFinderPolicy>(hzc::kNsIntentFinderPolicy);
    boost::shared_ptr<LocalIntentFinderPolicy> itf = map.get(policy);
    if (itf == nullptr) {
      logger->critical("***************************************************");
      logger->critical("*  {} Not In kNsIntentFinderPolicy Map.  *", policy);
      logger->critical("*  Exit IntentFinder.                             *");
      logger->critical("***************************************************");
      exit(-1);
    }
    string debug;
    MessageToJsonString(*itf, &debug);
    logger->info("itf policy_name={}\n{}", policy, debug);
  }
  {
    string sc_name = "sc1";
    auto map = hc->getMap<std::string, LocalSimpleClassifier>(hzc::kNsSC);
    boost::shared_ptr<LocalSimpleClassifier> sc = map.get(sc_name);
    if (sc != nullptr) {
      string debug;
      MessageToJsonString(*sc, &debug);
      logger->info("itf SimpleClassifier_name={}\n{}", sc_name, debug);
    }
  }

  //
  // itf 인스턴를 만든 다음 초기화를 한다.
  IntentFinderImpl itf_service;
  itf_service.Init(name);
  {
    try {
      itf_service.Load();
    } catch (ta::Exception &te) {
      logger->error(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      logger->error("          [ItentFinder Stop]");
      logger->error("   <NAME> : {} <PID> : {}", name, getpid());
      logger->error("   Can't run ITF instance");
      logger->error("   Cause : {}", te.ShowMessage());
      logger->error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
      exit(-1);
    }
  }

  long grpc_timeout = 0;
  if (grpc_timeout == 0) {
    logger->info("grpc_timeout is unlimited");
    grpc_timeout = INT_MAX;
    logger->info("grpc_timeout sets to {}", INT_MAX);
  }

  logger->info("[ITF] Starting..{}", address);

  CommonResultStatus::SetModuleAndProcess(maum::rpc::Module::M2U,
                                          maum::rpc::ProcessOfM2u::M2U_ITF);

  ServerBuilder builder;
  builder.AddListeningPort(address, grpc::InsecureServerCredentials());
  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);

  builder.RegisterService(&itf_service);
  unique_ptr<Server> server(builder.BuildAndStart());

  //
  // server를 실행한다.

  if (server) {
    logger->info("[ITF] Started name={}, address={}", name, address);
    cout << "[ITF] Started name=" << name << ", address=" << address << endl;
    server->Wait();
  } else {
    logger->error("[ITF] Cannot Start Server name={}, address={}",
                  name,
                  address);
  }
}

void help(const char *prog) {
  printf("%s --name name --listen ip:port  [--version] [--help]\n",
         prog);
  printf("%s -n name -i ip:port [-v] [-h]\n",
         prog);
}

void process_option(int argc, char *argv[], string &name, string &address) {
  bool do_exit = false;
  int c;

  char *prog = basename(argv[0]);

  while (true) {
    static const struct option long_options[] = {
        {"name", required_argument, nullptr, 'n'},
        {"listen", required_argument, nullptr, 'i'},
        {"version", no_argument, nullptr, 'v'},
        {"test", no_argument, nullptr, 't'},
        {"help", no_argument, nullptr, 'h'},
        {nullptr, no_argument, nullptr, 0}
    };

    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long(argc, argv, "n:i:vht", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
      case 0: {
        break;
      }
      case 'n': {
        name = optarg;
        break;
      }
      case 'i': {
        address = optarg;
        break;
      }
      case 'v': {
        printf("%s version %s\n", basename(argv[0]), version::VERSION_STRING);
        do_exit = true;
        break;
      }
      case 'h': {
        help(prog);
        do_exit = true;
        break;
      }

      default: {
        help(prog);
        do_exit = true;
      }
    }
  }

  if (do_exit)
    exit(EXIT_SUCCESS);
}

void LoadPlugins() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  auto plugin_count = c.GetDefaultAsInt("itfd.plugin.count", "0");
  for (auto i = 0; i < plugin_count; i++) {
    string so_name = c.Get("itfd.plugin." + to_string(i + 1) + ".so");
    string entry = c.Get("itfd.plugin." + to_string(i + 1) + ".entrypoint");

    if (so_name.empty() || entry.empty()) {
      logger->warn("invalid so {} or entry {} ", so_name, entry);
      continue;
    }
    void *ptr = dlopen(so_name.c_str(), RTLD_NOW);
    if (ptr == nullptr) {
      logger->warn("cannot load shared object {}", so_name);
      continue;
    }
    void (*sym)();
    sym = reinterpret_cast<void (*)()>(dlsym(ptr, entry.c_str()));
    if (sym == nullptr) {
      logger->warn("cannot load sym {}", entry);
    }
    logger->info("calling {} {}", so_name, entry);
    sym();
  }
}

#if 0

void testAddHzc(char *name) {
  auto hc = hzc::GetInstance()->GetHazelcastClient();
  {
    string itf_name = name;
    auto map = hc->getMap<std::string,
                          LocalIntentFinderInstance>(hzc::kNsIntentFinderInstance);
    boost::shared_ptr<LocalIntentFinderInstance> itf = map.get(name);

    LocalIntentFinderInstance itf_instance;
    itf_instance.set_name(name);
    itf_instance.set_description("itf1 descrption");
    itf_instance.set_ip("10.120.130.110");
    itf_instance.set_port(50121);
    itf_instance.set_policy_name("policy1");

    if (itf == nullptr)
      map.set(name, itf_instance);
    else
      map.replace(name, itf_instance);

  }
  {
    string policy_name = "policy1";
    auto map = hc->getMap<std::string,
                          LocalIntentFinderPolicy>(hzc::kNsIntentFinderPolicy);
    boost::shared_ptr<LocalIntentFinderPolicy> policy = map.get(policy_name);

    LocalIntentFinderPolicy new_policy;
    new_policy.set_name(policy_name);

    new_policy.set_name(policy_name);
    new_policy.set_description("policy1 descrption");
    new_policy.set_lang(::maum::common::Lang::ko_KR);
    new_policy.set_default_skill("etc");
    {
      IntentFindingStep *step = new_policy.add_steps();
      step->set_type(::maum::m2u::router::v3::ClassifyingMethod::CLM_CLASSIFY_SCRIPT);
      step->set_run_style(::maum::m2u::router::v3::IntentFindingFlowControl::IFC_SKILL_FOUND_COLLECT_HINTS);
      step->set_name("step1");
      step->set_description("step1 description");
      step->set_active(true);
      step->set_is_hint(true);
      {
        step->add_categories()->assign("category_1");
        step->add_categories()->assign("category_2");
        step->add_categories()->assign("category_3");

      }
      ::maum::m2u::router::v3::IntentFindingStep_ClassifyScriptRule
          *rule = step->mutable_custom_script_mod();
      rule->set_filename("echo_custom_script");
      rule->set_classify_func("Classify");
      rule->set_get_categories_func("GetCategories");
    }
    {
      IntentFindingStep *step = new_policy.add_steps();
      step->set_type(::maum::m2u::router::v3::ClassifyingMethod::CLM_HINT_COMPOSER_SCRIPT);
      step->set_run_style(::maum::m2u::router::v3::IntentFindingFlowControl::IFC_SKILL_FOUND_COLLECT_HINTS);
      step->set_name("step1");
      step->set_description("step1 description");
      step->set_active(true);
      step->set_is_hint(true);
      {
        step->add_categories()->assign("category_1");
        step->add_categories()->assign("category_2");
        step->add_categories()->assign("category_3");

      }
      ::maum::m2u::router::v3::IntentFindingStep_HintComposerScriptRule
          *rule = step->mutable_hint_composer_rule();
      {
        rule->add_hint_step_names()->assign("hint1");
        rule->add_hint_step_names()->assign("hint2");
        rule->add_hint_step_names()->assign("hint3");
      }
      rule->set_filename("custom_script_1.py");
      rule->set_compose_hints_func("compose_hints");
      rule->set_get_compose_table_func("get_compose_table");

    }
    {
      IntentFindingStep *step = new_policy.add_steps();
      step->set_type(::maum::m2u::router::v3::ClassifyingMethod::CLM_PCRE);
      step->set_run_style(::maum::m2u::router::v3::IntentFindingFlowControl::IFC_SKILL_FOUND_COLLECT_HINTS);
      step->set_name("pcre");
      step->set_description("pcre description");
      step->set_active(true);
      step->set_is_hint(true);
      {
        step->add_categories()->assign("category_1");
        step->add_categories()->assign("category_2");
        step->add_categories()->assign("category_3");

      }
      ::maum::m2u::router::v3::IntentFindingStep_PcreRule
          *rule = step->mutable_pcre_mod();
      rule->set_model("sc1");
      rule->set_lang(::maum::common::Lang::ko_KR);

    }
    {
      IntentFindingStep *step = new_policy.add_steps();
      step->set_type(::maum::m2u::router::v3::ClassifyingMethod::CLM_HMD);
      step->set_run_style(::maum::m2u::router::v3::IntentFindingFlowControl::IFC_SKILL_FOUND_COLLECT_HINTS);
      step->set_name("hmd");
      step->set_description("hmd description");
      step->set_active(true);
      step->set_is_hint(true);
      {
        step->add_categories()->assign("category_1");
        step->add_categories()->assign("category_2");
        step->add_categories()->assign("category_3");

      }
      ::maum::m2u::router::v3::IntentFindingStep_HmdRule
          *rule = step->mutable_hmd_mod();
      rule->set_model("news");
    }
    {
      IntentFindingStep *step = new_policy.add_steps();
      step->set_type(::maum::m2u::router::v3::ClassifyingMethod::CLM_DNN_CLASSIFIER);
      step->set_run_style(::maum::m2u::router::v3::IntentFindingFlowControl::IFC_SKILL_FOUND_COLLECT_HINTS);
      step->set_name("dnn");
      step->set_description("dnn description");
      step->set_active(true);
      step->set_is_hint(true);
      {
        step->add_categories()->assign("category_1");
        step->add_categories()->assign("category_2");
        step->add_categories()->assign("category_3");

      }
      ::maum::m2u::router::v3::IntentFindingStep_DnnClassifierRule
          *rule = step->mutable_dnn_cl_mod();
      rule->set_model("minds_assistant-kor");
      rule->set_lang(::maum::common::Lang::ko_KR);

    }
    {
      IntentFindingStep *step = new_policy.add_steps();
      step->set_type(::maum::m2u::router::v3::ClassifyingMethod::CLM_FINAL_RULE);
      step->set_run_style(::maum::m2u::router::v3::IntentFindingFlowControl::IFC_SKILL_FOUND_COLLECT_HINTS);
      step->set_name("final");
      step->set_description("final description");
      step->set_active(true);
      step->set_is_hint(true);
      {
        step->add_categories()->assign("category_1");
        step->add_categories()->assign("category_2");
        step->add_categories()->assign("category_3");

      }
      ::maum::m2u::router::v3::IntentFindingStep_FinalRule
          *rule = step->mutable_final_rule();
      rule->set_final_skill("final");
    }

    if (policy == nullptr)
      map.set(policy_name, new_policy);
    else
      map.replace(policy_name, new_policy);
  }
  {
    string sc_name = "sc1";
    auto map = hc->getMap<std::string, LocalSimpleClassifier>(hzc::kNsSC);
    boost::shared_ptr<LocalSimpleClassifier> sc = map.get(sc_name);

    LocalSimpleClassifier local_sc;
    local_sc.set_name(sc_name);
    local_sc.set_description("sc description");
    local_sc.set_pos_tagging(true);
    local_sc.set_named_entity(true);
    local_sc.set_skill_cnt(2);
    {
      auto skill = local_sc.add_skill_list();
      skill->set_name("skill1");
      skill->add_regex("aaa");
      skill->add_regex("bbb");
      skill->add_regex("ccc");
    }
    {
      auto skill = local_sc.add_skill_list();
      skill->set_name("skill2");
      skill->add_regex("111");
      skill->add_regex("222");
      skill->add_regex("333");
    }

    if (sc == nullptr)
      map.set(sc_name, local_sc);
    else
      map.replace(sc_name, local_sc);
  }
}

#endif

#if 0
int test_erase_repeate_elem() {
  FindIntentRequest req;
  req.mutable_exclude_skills()->Add("kkk");
  req.mutable_exclude_skills()->Add("11");
  req.mutable_exclude_skills()->Add("22");
  req.mutable_exclude_skills()->Add("kkk");
  req.mutable_exclude_skills()->Add("33");
  req.mutable_exclude_skills()->Add("44");
  req.mutable_exclude_skills()->Add("55");

  FindIntentRequest copy_req;
  copy_req.CopyFrom(req);

  auto mutable_ex_skills = copy_req.mutable_exclude_skills();
  for (auto it = mutable_ex_skills->begin(); it != mutable_ex_skills->end();) {
    if (*it == "kkk") {
      it = mutable_ex_skills->erase(it);
    } else
      it++;
    cout << "NEXT: --------  " << *it << ", SIZE: " << mutable_ex_skills->size() << endl;
  }

  return 0;
}
#endif

int main(int argc, char *argv[]) {
  string name = "itf1";
  string address = "127.0.0.1:9951";

  process_option(argc, argv, name, address);
  auto &conf = libmaum::Config::Init(argc, argv, "m2u.conf");
  conf.AddConfigFile("brain-ta.conf");
  conf.SetProgramName("m2u-itfd-" + name);
  conf.SetLoggerName("m2u-itfd<" + name + '>');

  {
    auto logger = LOGGER();

    logger->info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    logger->info("          [ItentFinder Starting]");
    logger->info("   <NAME> : {} <PID> : {}", name, getpid());
    logger->info("   <{}>", version::VERSION_STRING);
    logger->info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

    libmaum::log::CallLogger::GetInstance();
    LoadPlugins();

    // Check Hazelcast Server
    if (!ItfHazelcastClient::IsReady()) {
      logger->critical("************************************");
      logger->critical("*  Hazelcast Server is not Ready.  *");
      logger->critical("*  Exit IntentFinder.              *");
      logger->critical("************************************");
      logger->flush();
      return -1;
    }
  }

  RunServer(name, address);

  return 0;
}


