#include <libmaum/common/config.h>
#include "_custom-script.h"
#include "google/protobuf/util/json_util.h"
#include "tls-value.h"

using google::protobuf::util::MessageToJsonString;

static string GetPyErrorString() {
  if (PyErr_Occurred()) {
    PyObject *type_obj;
    PyObject *value_obj;
    PyObject *tb_obj;

    // Fetch error
    PyErr_Fetch(&type_obj, &value_obj, &tb_obj);
    string type = type_obj ? PyString_AsString(PyObject_Str(type_obj)) : "";
    string value = value_obj ? PyString_AsString(PyObject_Str(value_obj)) : "";
    PyObject *module_name, *pyth_module, *pyth_func;
    module_name = PyString_FromString("traceback");
    pyth_module = PyImport_Import(module_name);
    Py_XDECREF(module_name);

    string err = fmt::format("type:{}, value:{}",
                             type, value);
    if (pyth_module == nullptr) {
      return err;
    }

    pyth_func = PyObject_GetAttrString(pyth_module, "format_exception");
    if (pyth_func && PyCallable_Check(pyth_func)) {
      PyObject *pyth_val;
      PyObject *pystr;
      pyth_val = PyObject_CallFunctionObjArgs(pyth_func,
                                              type_obj,
                                              value_obj, tb_obj, NULL);

      pystr = PyObject_Str(pyth_val);
      auto full = PyString_AsString(pystr);
      string err = fmt::format("type:{}, value:{}, trace: {}",
                               type, value, full);
      Py_XDECREF(pyth_val);
      return err;
    } else {
      return err;
    }
  } else {
    return "<no error>";
  }
}

CustomScriptManager::CustomScriptManager() {
  LOGGER_debug(">>> [Initialize CustomScriptManager] Start");
  Py_Initialize();
  PyEval_InitThreads();
  mainThreadState_ = PyEval_SaveThread();
}


CustomScriptManager::~CustomScriptManager() {
  LOGGER_debug(">>> [ CustomScriptManager] END");
  Py_Finalize();
}
/**
 * custom script(python code) 호출하기 위한 초기 작업 수행
 * customscript 경로 syspath 추가, import 작업
 *
 * @return is_done Init 함수 완료여부
 */
bool CustomScriptManager::Init() {
  auto logger = LOGGER();
  bool is_done = false;
  auto &c = libmaum::Config::Instance();
  string custom_script_path = c.Get("itfd.customscript.path");

  string py_cs_path = "sys.path.append(\"";
  py_cs_path += custom_script_path;
  py_cs_path += "\")";

  logger_debug("custom script path={}", py_cs_path);

  PyRun_SimpleString("import sys");
  PyRun_SimpleString(py_cs_path.c_str());

  logger_debug("Py_GetPrefix: {}", Py_GetPrefix());
  logger_debug("Py_GetExecPrefix: {}", Py_GetExecPrefix());
  logger_debug("Py_GetProgramFullPath: {}", Py_GetProgramFullPath());
  logger_debug("Py_GetPath: {}", Py_GetPath());
  logger_debug("Py_GetVersion: {}", Py_GetVersion());
  logger_debug("Py_GetPlatform: {}", Py_GetPlatform());
  logger_debug("Py_GetCopyright: {}", Py_GetCopyright());
  logger_debug("Py_GetCompiler: {}", Py_GetCompiler());
  logger_debug("Py_GetBuildInfo: {}", Py_GetBuildInfo());

  logger_debug("<<< [Initialize CustomScriptManager] End");
  is_done = true;
  return is_done;
}

bool CustomScriptManager::Stop() {
  LOGGER()->debug("[CustomScriptManager Stop]");
  Py_Finalize();
  return true;
}

bool CustomScriptManager::CallPythonFunc(const char *name,
                                         const char *func,
                                         string &param,
                                         string &result) {

  auto logger = LOGGER();
  logger_debug("CallPythonFunc name={}, func={}", name, func);

  Init();
  if (!Py_IsInitialized()) {
    logger_warn("Unable to initialize Python interpreter. py error {}",
                GetPyErrorString());
    return false;
  }

  gilState_= PyGILState_Ensure();

  Init();
  logger_debug("CallPythonFunc filename={}, classify_func={}", name, func);
  PyObject *po_name, *po_module, *py_func;
  PyObject *po_args, *po_value;

  /* Error checking of po_name left out */
  po_name = PyString_FromString(name);
  po_module = PyImport_Import(po_name);

  if (po_module == nullptr) {
    logger_warn("Failed to load {}. py error {}", name, GetPyErrorString());
    PyGILState_Release(gilState_);
    return false;
  }

  py_func = PyObject_GetAttrString(po_module, func);
  /* pFunc is a new reference */
  if ((py_func == nullptr) || (PyCallable_Check(py_func) == 0)) {
    logger_warn("Cannot find function {}. py error {}", func, GetPyErrorString());
    PyGILState_Release(gilState_);
    return false;
  }

  po_args = Py_BuildValue("(s)", param.c_str());
  po_value = PyObject_CallObject(py_func, po_args);

  logger_debug("PyObject_CallObject..ok");

  if (po_value == nullptr) {
    logger_warn("Call failed. py error {}", GetPyErrorString());
    Py_XDECREF(py_func);
    Py_XDECREF(po_module);
    PyGILState_Release(gilState_);
    return false;
  }
  logger_debug("Result of py call: {}\n", PyString_AsString(po_value));

  Py_XDECREF(po_name);
  Py_XDECREF(po_module);
  Py_XDECREF(py_func);
  Py_XDECREF(po_args);
  Py_XDECREF(po_value);
  PyGILState_Release(gilState_);

  return true;
}

/**
 * sys path에 경로추가 함수
 *
 * @param path 추가할 경로
 * @return is_done AppendPath 기능 완료여부
 */
bool CustomScriptManager::AppendPath(string path) {
  bool is_done = false;
  auto logger = LOGGER();
  if (!Py_IsInitialized()) {
    logger_warn("Unable to initialize Python interpreter.");
    logger_error("py error {}", GetPyErrorString());
    return is_done;
  }

  string fpath = "sys.path.append(\"" + path + "\")";

  PyRun_SimpleString("import sys");
  PyRun_SimpleString(fpath.c_str());
  is_done = true;
  return is_done;
}

/**
 * Custom Script 분류 수행 함수
 *
 * @param rule custom script 관련 규칙
 * @param param custom script 에서 수행할 함수명
 * @param result 분류결과 카테고리가 담길 변수
 * @return custom script 분류 완료 여부
 */
bool CustomScriptManager::Classify(const ClassifyScriptRule *rule,
                                   CustomClassifyParam *param,
                                   CustomClassifyResult &result) {
  const char *file_name = rule->model().c_str();
  const char *func_name = rule->classify_func().c_str();
  auto logger = LOGGER();

  logger_debug("<<< ['{}.py' CustomScript '{}'] Start", file_name, func_name);
  string sa_param = param->SerializeAsString();

  bool is_done = false;
  string debug;
  MessageToJsonString(*param, &debug);
  logger_debug("CustomScript param={}", debug);

  if (!Py_IsInitialized()) {
    logger_warn("Unable to initialize Python interpreter. py error {}",
                GetPyErrorString());
    return is_done;
  }

  gilState_= PyGILState_Ensure();

  Init();
  logger_debug("Classify filename={}, classify_func={}",
               file_name, func_name);
  PyObject *po_name, *po_mod, *po_func;
  PyObject *po_args, *po_value;

  po_name = PyString_FromString(file_name);
  po_mod = PyImport_Import(po_name);

  if (po_mod == nullptr) {
    logger_warn("Failed to load {}, py error {}", file_name, GetPyErrorString());
    PyGILState_Release(gilState_);
    return is_done;
  }

  po_func = PyObject_GetAttrString(po_mod, func_name);

  if ((po_func == nullptr) || (PyCallable_Check(po_func) == 0)) {
    logger_warn("Cannot find function {}. py error {}",
                 func_name,
                 GetPyErrorString());
    PyGILState_Release(gilState_);
    return is_done;
  }

  po_args = Py_BuildValue("(s#)", sa_param.data(), (int) sa_param.size());

  po_value = PyObject_CallObject(po_func, po_args);

  if (po_value == nullptr) {
    logger_warn("Call failed. py error {}", GetPyErrorString());
    Py_XDECREF(po_func);
    Py_XDECREF(po_mod);
    PyGILState_Release(gilState_);
    return is_done;
  }
  string values = PyString_AsString(po_value);
  result.ParseFromString(values);
  logger_debug("Result of py call: {}\n", result.Utf8DebugString());

  Py_XDECREF(po_name);
  Py_XDECREF(po_mod);
  Py_XDECREF(po_func);
  Py_XDECREF(po_args);
  Py_XDECREF(po_value);

  PyGILState_Release(gilState_);
  if (values.empty()) {
    logger_debug("<<<Not Found Result.['{}.py' CustomScript '{}'] End",
                 file_name, func_name);
    return is_done;
  }

  is_done = true;
  logger_debug("<<<Find Result: {} ['{}.py' CustomScript '{}'] End",
               values, file_name, func_name);
  return is_done;
}

/**
 * Custom Script 에 존재하는 카테고리 목록 가져오는 함수
 *
 * @param rule custom script 관련 규칙
 * @param result 분류결과 카테고리가 담길 변수
 * @return custom script 분류 완료 여부
 */
bool CustomScriptManager::GetCategories(ClassifyScriptRule *rule,
                                        CustomGetCategoriesResult *result) {
  const char *file_name = rule->model().c_str();
  const char *func_name = rule->classify_func().c_str();

  bool is_done = false;
  auto logger = LOGGER();
  logger_debug("<<< ['{}.py' CustomScript '{}'] Start", file_name, func_name);

  Init();
  logger_debug("CallPythonFunc filename={}, classify_func={}",
                file_name, func_name);
  PyObject *po_name, *po_mod, *po_func, *po_dict;
  PyObject *po_args, *po_value;

  if (!Py_IsInitialized()) {
    logger_warn("Unable to initialize Python interpreter. py error {}",
                GetPyErrorString());
    return is_done;
  }

  po_name = PyString_FromString(file_name);
  /* Error checking of po_name left out */

  po_mod = PyImport_Import(po_name);

  if (po_mod == nullptr) {
    logger_warn("Failed to load {}, py error {}", file_name, GetPyErrorString());
    return is_done;
  }

  po_dict = PyModule_GetDict(po_mod);
  po_func = PyDict_GetItemString(po_dict, func_name);

  if ((po_func == nullptr) || (PyCallable_Check(po_func) == 0)) {
    logger_warn("Cannot find function {}, py error {}", func_name, GetPyErrorString());
    return is_done;
  }

  po_args = Py_BuildValue("O", NULL);
  po_value = PyObject_CallObject(po_func, po_args);

  if (po_value == nullptr) {
    logger_warn("Call failed. py error {}", GetPyErrorString());
    Py_XDECREF(po_func);
    Py_XDECREF(po_mod);
    return is_done;
  }
  google::protobuf::util::JsonParseOptions options2;
  result->ParseFromString(PyString_AsString(po_value));
  string output;
  google::protobuf::util::JsonOptions options;
  options.always_print_primitive_fields = true;
  options.add_whitespace = true;
  MessageToJsonString(*result, &output, options);
  logger_debug("Result of py call: {}\n", output);

  Py_XDECREF(po_name);
  Py_XDECREF(po_mod);
  Py_XDECREF(po_func);
  Py_XDECREF(po_args);
  Py_XDECREF(po_value);

  is_done = true;
  logger_debug("<<<Result: {} ['{}.py' CustomScript '{}'] End",
               output, file_name, func_name);
  return is_done;
}

bool CustomScriptManager::ComposeHints(ClassifyScriptRule *rule,
                                       CustomComposeHintsParam *param,
                                       RunResult *result) {
  string in, out;
  CallPythonFunc(rule->filename().c_str(),
                 rule->classify_func().c_str(),
                 in,
                 out);
  return true;
}

/**
 *
 * @param rule
 * @param result
 * @return
 */
bool CustomScriptManager::GetComposeTable(ClassifyScriptRule *rule,
                                          CustomGetComposeTableResult *result) {
  string in, out;
  CallPythonFunc(rule->filename().c_str(),
                 rule->classify_func().c_str(),
                 in,
                 out);
  return true;
}
