#ifndef PROJECT_SIMPLECLASSIFIERPCRE_H
#define PROJECT_SIMPLECLASSIFIERPCRE_H

#include <pcre.h>
#include <maum/common/lang.pb.h>
#include "hazelcast.h"

using std::string;
using std::vector;

using ::maum::common::Lang;

#define MAX_PCRE_SUB_STR_VEC 100

/**
 *
 */
class SimplePcre {
 public:
  SimplePcre();
  virtual ~SimplePcre();

 public:
  bool Compile(const string &regex);
  bool Exec(const string &context);
  bool Exec(const string &context, vector<string> &match_group);
  static bool PcreExec(const char *regex,
                       const char *context,
                       vector<string> &match_group);
  string GetRegex() {
    return regex_;
  }
 protected:
  string regex_;

  pcre *re_compiled_ = nullptr;
  pcre_extra *pcre_extra_ = nullptr;

};

using SimplePcrePair = std::pair<std::string, std::shared_ptr<SimplePcre>>;

/**
 *
 */
class SimpleClassifierPcre {
 public:
  SimpleClassifierPcre() {
    is_started_ = false;
    is_pos_tagging_ = false;
    is_named_entity_ = false;
  }
  virtual ~SimpleClassifierPcre() = default;

 public:
  inline bool IsStart() { return is_started_; };
  inline void SetPosTagging(bool enabled) { is_pos_tagging_ = enabled; };
  inline void SetNamedEntity(bool enabled) { is_named_entity_ = enabled; };
  inline bool GetPosTagging() { return is_pos_tagging_; };
  inline bool GetNamedEntity() { return is_named_entity_; };

  bool LoadFile(const string &sc_name, const string &path);
  bool LoadHzc(const string &sc_name);
  bool AddPcre(const string &regex);
  bool AddPcre(const string &name, const string &regex);
  inline void SetName(const string &name) { name_ = name; };
  inline void SetLastScSerialize(const char *sc_srz) { last_sc_serialize_ = sc_srz; };

  bool Find(string &skill, const string &input);
  bool Find(const string &input, string &skill, string &rule);
  bool UpdateHzc();
  bool UpdatePcreHzc();

 protected:
  bool ParseJson(const string &input);

 protected:
  bool is_started_;
  bool is_pos_tagging_;
  bool is_named_entity_;
  string name_;
  string last_sc_serialize_;
  std::list<SimplePcrePair> skills_;
};

#endif //PROJECT_SIMPLECLASSIFIERPCRE_H
