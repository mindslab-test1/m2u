#ifndef PROJECT_XDC_HANDLER_H
#define PROJECT_XDC_HANDLER_H

#include <grpc++/grpc++.h>

#include <maum/brain/nlp/nlp.pb.h>
#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <maum/brain/han/run/han_run.pb.h>
#include <maum/brain/han/run/han_run.grpc.pb.h>

#include <string>

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::StatusCode;
using maum::brain::han::run::HanClassifier;
using maum::brain::han::run::HanResult;
using maum::brain::han::run::HanInputDocument;
using maum::brain::nlp::Document;

typedef std::unique_ptr<HanClassifier::Stub> XDCStub;
/**
*
*/
class XdcClient {
 public:
  XdcClient(std::shared_ptr<Channel> channel)
      : xdc_stub_(HanClassifier::NewStub(channel)) {};
  virtual ~XdcClient() {};

  bool Classify(const HanInputDocument &document, HanResult *result);

 private:
  // xdc stub
  XDCStub xdc_stub_;
};

#endif //PROJECT_XDC_HANDLER_H
