
#include "xdc-client.h"
#include "tls-value.h"
#include <thread>
#include <libmaum/common/config.h>
#include <libmaum/brain/error/ta-error.h>

/**
* xdc 분류 함수
*
* @param document 입력 텍스트 언어처리 Document
* @param result xdc 분류 결과 HanResult
* @return is_done 언어분석 성공여부
*/
bool XdcClient::Classify(const HanInputDocument &document,
                          HanResult *result) {
  auto logger = LOGGER();
  ClientContext context;
  logger_debug(">>> [XdcClient::GetClass] Start");
  logger_trace(">>> [XdcClient::GetClass] Request Document=:[{}]",
               document.Utf8DebugString());
  // GetClass 호출
  xdc_stub_->GetClass(&context, document, result);
  logger_trace(">>> [XdcClient::GetClass] Response HanResult=:[{}]",
               result->Utf8DebugString());
  logger_debug(">>> [XdcClient::GetClass] End");
  return true;
}
