#include <iostream>
#include <fstream>
#include <libmaum/common/config.h>
#include "sc-pcre.h"
#include "tls-value.h"
#include "local-simpleclassifier.h"

using namespace std;
using hzc = ItfHazelcastClient;

SimplePcre::SimplePcre() {
}

SimplePcre::~SimplePcre() {

  if (re_compiled_ != nullptr)
    pcre_free(re_compiled_);

  if (pcre_extra_ != nullptr) {
#ifdef PCRE_CONFIG_JIT
    pcre_free_study(pcre_extra_);
#else
    pcre_free(pcre_extra_);
#endif
  }
}

/**
 *
 * @param regex_input 정규표현식
 * @return 성공여부
 */
bool SimplePcre::Compile(const string &regex_input) {
  auto logger = LOGGER();
  const char *err;
  int err_offset;
  logger_debug("compile {}", regex_input);
  regex_ = regex_input;
  re_compiled_ = pcre_compile(regex_.c_str(),
                              0,
                              &err,
                              &err_offset,
                              nullptr);

  // pcre_compile returns NULL on error, and sets pcre_error_offset_ & pcre_error_str_
  if (re_compiled_ == nullptr) {

    logger_debug("ERROR: Could not compile '{}': {}", regex_, err);
    return false;
  } /* end if */

  // Optimize the regex
  pcre_extra_ = pcre_study(re_compiled_, 0, &err);

  /* pcre_study() returns NULL for both errors and when it can not optimize the regex.  The last argument is how one checks for
     errors (it is NULL if everything works, and points to an error string otherwise. */
  if (err != nullptr) {
    logger_debug("ERROR: Could not study '{}': {}", regex_, err);
    return false;
  } /* end if */
  return true;
}

/**
 *
 * @param context
 * @return Exec 수행 성공여부
 */
bool SimplePcre::Exec(const string &context) {
  vector<string> match_group;
  return Exec(context, match_group);
}

/**
 *
 * @param context
 * @param match_group
 * @return
 */
bool SimplePcre::Exec(const string &context, vector<string> &match_group) {
  const char *p_substr_matchstr = nullptr;
  int sub_str_vec[MAX_PCRE_SUB_STR_VEC] = {0,};
  auto logger = LOGGER();
  logger_trace("SimplePcre :Exec {} / {}", context, regex_);
  int pcre_exec_ret = pcre_exec(re_compiled_,
                                pcre_extra_,
                                context.c_str(),
                                int(context.size()),  // length of string
                                0,                      // Start looking at this point
                                0,                      // OPTIONS
                                sub_str_vec,
                                MAX_PCRE_SUB_STR_VEC); // Length of sub_str_vec_

  string str_exec_result;
  if (pcre_exec_ret < 0) { // Something bad happened..
    switch (pcre_exec_ret) {
      case PCRE_ERROR_NOMATCH: {
        str_exec_result = "PCRE_ERROR_NOMATCH";
        break;
      }
      case PCRE_ERROR_NULL: {
        str_exec_result = "PCRE_ERROR_NULL";
        break;
      }
      case PCRE_ERROR_BADOPTION: {
        str_exec_result = "PCRE_ERROR_BADOPTION";
        break;
      }
      case PCRE_ERROR_BADMAGIC: {
        str_exec_result = "PCRE_ERROR_BADMAGIC";
        break;
      }
      case PCRE_ERROR_UNKNOWN_NODE: {
        str_exec_result = "PCRE_ERROR_UNKNOWN_NODE";
        break;
      }
      case PCRE_ERROR_NOMEMORY: {
        str_exec_result = "PCRE_ERROR_NOMEMORY";
        break;
      }
      default: {
        str_exec_result = "PCRE_ERROR_UNKNOWN";
        break;
      }
    } /* end switch */
    //    logger_debug("SimplePcre Result: failed {}", str_exec_result);
    return false;
  }
  // logger_debug("SimplePcre: We have a match!");

  // At this point, rc contains the number of substring matches found...
  if (pcre_exec_ret == 0) {
    // logger_debug("But too many substrings were found to fit in sub_str_vec_!");
    // Set rc to the max number of substring matches possible.
    pcre_exec_ret = MAX_PCRE_SUB_STR_VEC / 3;
  } /* end if */

  // PCRE contains a handy function to do the above for you:
  for (int j = 0; j < pcre_exec_ret; j++) {
    pcre_get_substring(context.c_str(),
                       sub_str_vec,
                       pcre_exec_ret,
                       j,
                       &(p_substr_matchstr));
    /*
    logger_debug("Match({}/{}): ({},{}): '{}'",
                  j,
                  pcre_exec_ret - 1,
                  sub_str_vec_[j * 2],
                  sub_str_vec_[j * 2 + 1],
                  p_substr_matchstr);
    */
    match_group.emplace_back(p_substr_matchstr);
  } /* end for */

  // Free up the substring
  pcre_free_substring(p_substr_matchstr);
  return true;
}

/**
 *
 * @param regex 정규표현식
 * @param subject
 * @param match_group
 * @return 성공여부
 */
bool SimplePcre::PcreExec(const char *regex,
                          const char *subject,
                          vector<string> &match_group) {

  int exec_ret;
  const char *p_substr_matchstr;
  int sub_str_vec[MAX_PCRE_SUB_STR_VEC];
  const char *pcre_error_str;
  int pcre_error_offset;
  auto logger = LOGGER();
  bool is_done = false;
  logger_debug("SimplePcre PcreExec {}/{}", regex, subject);

  pcre *re_compiled =
      pcre_compile(regex, 0, &pcre_error_str, &pcre_error_offset, nullptr);

  if (re_compiled == nullptr) {
    logger_debug("SimplePcre ERROR: Could not compile '{}': {}",
                  regex,
                  pcre_error_str);
    return is_done;
  }

  pcre_extra *pcre_extra = pcre_study(re_compiled, 0, &pcre_error_str);

  if (pcre_error_str != nullptr) {
    logger_debug("SimplePcre ERROR: Could not study '{}': {}",
                  regex,
                  pcre_error_str);
    return is_done;
  }

  exec_ret = pcre_exec(re_compiled,
                       pcre_extra,
                       subject,
                       strlen(subject),  // length of string
                       0,                      // Start looking at this point
                       0,                      // OPTIONS
                       sub_str_vec,
                       MAX_PCRE_SUB_STR_VEC); // Length of sub_str_vec_

  string str_exec_result;
  if (exec_ret < 0) { // Something bad happened..
    switch (exec_ret) {
      case PCRE_ERROR_NOMATCH: {
        str_exec_result = "PCRE_ERROR_NOMATCH";
        break;
      }
      case PCRE_ERROR_NULL: {
        str_exec_result = "PCRE_ERROR_NULL";
        break;
      }
      case PCRE_ERROR_BADOPTION: {
        str_exec_result = "PCRE_ERROR_BADOPTION";
        break;
      }
      case PCRE_ERROR_BADMAGIC: {
        str_exec_result = "PCRE_ERROR_BADMAGIC";
        break;
      }
      case PCRE_ERROR_UNKNOWN_NODE: {
        str_exec_result = "PCRE_ERROR_UNKNOWN_NODE";
        break;
      }
      case PCRE_ERROR_NOMEMORY: {
        str_exec_result = "PCRE_ERROR_NOMEMORY";
        break;
      }
      default: {
        str_exec_result = "PCRE_ERROR_UNKNOWN";
        break;
      }
    } /* end switch */
    // logger_debug("SimplePcre Result: failed {}", str_exec_result);
    return is_done;
  }

  // logger_debug("SimplePcre Result: We have a match!, exec_ret={}", exec_ret);

  if (exec_ret == 0) {
    // logger_debug("But too many substrings were found to fit in sub_str_vec_!");
    // Set rc to the max number of substring matches possible.
    exec_ret = MAX_PCRE_SUB_STR_VEC / 3;
  } /* end if */

  for (int j = 0; j < exec_ret; j++) {
    pcre_get_substring(subject,
                       sub_str_vec,
                       exec_ret,
                       j,
                       &p_substr_matchstr);
/*
    logger_debug(" + [{}] pos={}, {}",  j, sub_str_vec[j], p_substr_matchstr);
    logger_debug("Match({}/{}): ({},{}): '{}'",
                  j,
                  exec_ret - 1,
                  sub_str_vec[j * 2],
                  sub_str_vec[j * 2 + 1],
                  p_substr_matchstr);
*/
    match_group.push_back(p_substr_matchstr);
  } /* end for */

  pcre_free_substring(p_substr_matchstr);
  is_done = true;
  return is_done;
}

/**
 * name_ 값 설정 및 ParseJson(path) 호출하여 skills_에
 * <simple classifier이름, SimplePcre> pair 추가
 *
 * @param sc_name simple classifier 이름
 * @param path 파일경로
 * @return LoadFile 완료여부
 */
bool SimpleClassifierPcre::LoadFile(const string &sc_name,
                                    const string &path) {
  auto logger = LOGGER();
  name_ = sc_name;
  logger_debug("SimplePcre load_file={}, path={}", sc_name, path);
  ParseJson(path);
  return true;
}

/**
 * hzc에서 simpleclassifier 정보 가져와 skills_에
 * <simple classifier이름, SimplePcre> pair 추가
 *
 * @param name simpleclassifier 이름
 * @return hzc 가져온 데이터 유무
 */
bool SimpleClassifierPcre::LoadHzc(const string &name) {
  auto logger = LOGGER();
  logger_debug("SimplePcre load_hzc name={}", name);
  name_ = name;

  try {
    string sc_name = name;
    auto hc = hzc::GetInstance()->GetHazelcastClient();
    auto map = hc->getMap<std::string, LocalSimpleClassifier>(hzc::kNsSC);
    boost::shared_ptr<LocalSimpleClassifier> sc = map.get(name);
    if (sc != nullptr) {
      logger_trace("itf={}\n{}", sc_name, sc->Utf8DebugString());
    }

    if (sc == nullptr) {
      logger_debug("I can't found SimpleClassifier={}", name);
      return false;
    }

    for (const auto &sl : sc->skill_list()) {
      for (int index = 0; index < sl.regex_size(); index++) {
        logger_debug("  +skill=[{}] [{}] [{}]",
                      sl.name(),
                      index,
                      sl.regex(index));

        auto pcre = make_shared<SimplePcre>();
        string pcre_regex = sl.regex(index);
        pcre->Compile(pcre_regex);
        this->skills_.emplace_back(SimplePcrePair(sl.name(), pcre));

      }
    }
    last_sc_serialize_ = sc->SerializeAsString();
  } catch (IException &e) {
    logger_critical("SimpleClassifierPcre::{} Hazelcast: {}",
                     __func__,
                     e.what());
  }
  return true;
}

/**
 * 파일을 읽어들여 Json 형태로 변환하여 skills_에
 * <simple classifier이름, SimplePcre> pair 추가
 *
 * @param input filename
 * @return 함수성공여부
 */
bool SimpleClassifierPcre::ParseJson(const string &input) {
  auto logger = LOGGER();
  if (input.empty()) {
    logger_debug("input file is empty");
    return false;
  }
  is_started_ = true;
  logger_debug("[input Json file] {}", input);

  Json::Value root;

  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;
  std::ifstream ifs(input);
  std::string errs;
  bool status = Json::parseFromStream(builder, ifs, &root, &errs);
  ifs.close();
  if (!status) {
    logger_debug("[Json Err] {}", errs);
    return false;
  }

  //json file parsing
  string reg_result;
  string result_action;
  auto member = root.getMemberNames();
  int index = 0;
  for (auto &iter : member) {
    Json::Value regex = root[iter]["regex"];
    logger_debug(" [{}] {} /size={}", index++, iter, regex.size());
    if (regex.size() <= 1) {
      logger_debug("   [regex] {}", regex[0].asString());
      auto pcre = make_shared<SimplePcre>();
      string pcre_regex = regex[0].asString();
      pcre->Compile(pcre_regex);
      this->skills_.emplace_back(SimplePcrePair(iter, pcre));
    } else {
      for (Json::ArrayIndex i = 0; i < regex.size(); i++) {
        logger_debug("   [{}] {}", i, regex[i].asString());
        auto pcre = make_shared<SimplePcre>();
        string pcre_regex = regex[i].asString();
        pcre->Compile(pcre_regex);
        this->skills_.emplace_back(SimplePcrePair(iter, pcre));
      }
    }
  }

  for (auto &skill : skills_)
    logger_debug(" skill={}", skill.first);

  return true;
}

bool SimpleClassifierPcre::UpdateHzc() {
  auto logger = LOGGER();
  logger_debug("updateHzc name={}", name_);
  if (last_sc_serialize_.empty())
    return true;

  try {
    auto hc = hzc::GetInstance()->GetHazelcastClient();
    auto map = hc->getMap<std::string, LocalSimpleClassifier>(hzc::kNsSC);
    boost::shared_ptr<LocalSimpleClassifier> sc = map.get(name_);

    string cur_sc_serialize = sc->SerializeAsString();

    logger_debug("HZC simple-classifier compare the size cur:{}, old:{}",
                  cur_sc_serialize.size(), last_sc_serialize_.size());

    if (cur_sc_serialize != last_sc_serialize_) {
      logger_debug("HZC simple-classifier is different cur:{}, old:{}",
                    cur_sc_serialize.size(), last_sc_serialize_.size());
      skills_.clear();
      LoadHzc(name_);
    }

  } catch (IException &e) {
    logger_critical("SimpleClassifierPcre::{} Hazelcast: {}",
                     __func__,
                     e.what());
    return false;
  }
  return true;
}

bool SimpleClassifierPcre::UpdatePcreHzc() {
  auto logger = LOGGER();
  logger_debug(">UpdatePcreHzc name={} Start", name_);
  LoadHzc(name_);
  logger_debug(">UpdatePcreHzc name={} End", name_);
  return true;
}

/**
 * input 에 해당하는 분류값이 존재하면 skill에 값 저장
 *
 * @param skill 분류 결과 스킬
 * @param input utter 값
 * @return 분류 결과 유무
 */
bool SimpleClassifierPcre::Find(string &skill, const string &input) {
  auto logger = LOGGER();
  bool found = false;

  for (auto &it : this->skills_) {
    if (it.second->Exec(input)) {
      skill = it.first;
      found = true;
    }
  }
  return found;
}

/**
 * input 에 해당하는 분류값이 존재하면 스킬, 규칙 변수 저장
 *
 * @param input utter 값
 * @param skill 분류 결과 스킬 저장할 변수
 * @param rule 분류 규칙 저장할 변수
 * @return 분류 결과 유무
 */
bool SimpleClassifierPcre::Find(const string &input, string &skill, string &rule) {
  auto logger = LOGGER();
  bool found = false;

  for (auto &it : this->skills_) {
    if (it.second->Exec(input)) {
      skill = it.first;
      rule = it.second->GetRegex();
      found = true;
      logger_debug("'[{}]{}' matches [{}].", skill, rule, input);
      return found;
    } else {
      logger_trace("'[{}]{}' does not match [{}].", it.first, it.second->GetRegex(), input);
    }
  }
  return found;
}

/**
 * SimpleClassfier를 skills_에 <simple classifier 이름, SimplePcre> pair 추가
 *
 * @param regex 정규표현식
 * @return false
 */
bool SimpleClassifierPcre::AddPcre(const string &regex) {
  auto logger = LOGGER();
  logger_debug("SimpleClassifierPcre AddPcre name={}, regex={}", name_, regex);
  auto pcre = make_shared<SimplePcre>();
  pcre->Compile(regex);
  this->skills_.emplace_back(SimplePcrePair(name_, pcre));
  return true;
}

/**
 * SimpleClassfier를 skills_에 <simple classifier이름, SimplePcre> pair 추가
 *
 * @param name simpleclassifier 이름
 * @param regex 정규표현식
 * @return false
 */
bool SimpleClassifierPcre::AddPcre(const string &name, const string &regex) {
  auto logger = LOGGER();
  logger_debug("SimpleClassifierPcre AddPcre name={}, regex={}... start", name, regex);
  auto pcre = make_shared<SimplePcre>();
  string regex_str = regex;
  pcre->Compile(regex_str);
  this->skills_.emplace_back(SimplePcrePair(name, pcre));
  logger_debug("SimpleClassifierPcre AddPcre name={}, regex={}... done", name, regex);
  return true;
}
