#include <libmaum/common/config.h>
#include "classifying-handler.h"
#include "google/protobuf/util/json_util.h"
#include "tls-value.h"
#include "cl-elapsed-timer.h"
#include "libmaum/brain/error/ta-error.h"
#include "local-simpleclassifier.h"
#include "local-intentfinder-policy.h"

using google::protobuf::util::MessageToJsonString;
using google::protobuf::Map;
using std::make_shared;
using ::maum::m2u::router::v3::ClassifyingMethod;
using ::maum::m2u::router::v3::IntentFindingFlowControl;
using ::maum::m2u::router::v3::Replacement;
using ::maum::brain::nlp::KeywordFrequencyLevel;
using hzc = ItfHazelcastClient;

using meta_pair = Map<string, string>::value_type;

bool operator<(const HmdModelInfo &lhs, const HmdModelInfo &rhs) {
  bool same = lhs.model == rhs.model;
  if (same)
    return lhs.lang < rhs.lang;
  else
    return lhs.model < rhs.model;
}


/**
 * Itentfinder 가사용할 nlp, hmd, dnn, custom_script에 필요한 리소스 로딩 및
 * 데이터 초기화.
 *
 * @param policy_name 정책명
 * @return is_done init 함수 완료 여부
 */
bool ClassifyingHandler::Init(const string &policy_name) {
  auto logger = LOGGER();
  bool is_done = true;
  logger_debug("Initialize ClassifyingHandler policy_name={}", policy_name);
  policy_name_ = policy_name;
  //
  // Model들을 초기화
  nlp_kor_ = make_shared<NlpKoreanHandler>();
  nlp_kor_->Init();
  nlp_eng_ = make_shared<NlpEnglishHandler>();
  nlp_eng_->Init();
  hmd_ = make_shared<HmdHandler>();
  hmd_->Init();
  dnn_ = make_shared<DnnHandler>();
  dnn_->Init();


  // m2u-itf-cs-py-runner 와 통신하기 위한 stub 생성
  auto &c = libmaum::Config::Instance();
  string server_info = c.Get("itf.cs.py.ip");
  server_info += ':';
  server_info += c.Get("itf.cs.py.port");
  string xdc_server_info = c.Get("brain-xdc.ip");
  xdc_server_info += ':';
  xdc_server_info += c.Get("brain-xdc.port");
  custom_script_client_ = make_shared<CustomScriptClient>(grpc::CreateChannel(
      server_info, grpc::InsecureChannelCredentials()));
  logger_debug("Custom script py runner address : {}", server_info);

  xdc_client_ = make_shared<XdcClient>(grpc::CreateChannel(
      xdc_server_info, grpc::InsecureChannelCredentials()));
  logger_debug("XDC server address : {}", xdc_server_info);

  set<HmdModelInfo> hmdmodels;
  auto policy = hzc::GetInstance()->GetIntentFinderPolicy(policy_name);
  for (int index = 0; index < policy->steps().size(); index++) {
    const auto &step = policy->steps(index);
    logger_debug(" [{}] Step name={}, type/runstyle={}/{}",
                 index,
                 step.name(),
                 ClassifyingMethod_Name(step.type()),
                 IntentFindingFlowControl_Name(step.run_style())
    );
    switch (step.type()) {
      case ClassifyingMethod::CLM_CLASSIFY_SCRIPT: {
        break;
      }
      case ClassifyingMethod::CLM_HINT_COMPOSER_SCRIPT: {
        break;
      }
      case ClassifyingMethod::CLM_PCRE: {
        auto entry = new atomic<shared_ptr<SimpleClassifierPcre> *>;
        auto pcre = make_shared<SimpleClassifierPcre>();
        string sc_name = step.pcre_mod().model();

        pcre->SetName(sc_name);
        ParsingScNlp(sc_name, pcre);
        entry->store(new shared_ptr<SimpleClassifierPcre>(pcre));

        sc_pcre_.insert(make_pair(sc_name, entry));
        break;
      }
      case ClassifyingMethod::CLM_HMD: {
        HmdModelInfo model_info;
        model_info.lang = step.hmd_mod().lang();
        model_info.model = step.hmd_mod().model();
        hmdmodels.insert(model_info);
        break;
      }
      case ClassifyingMethod::CLM_DNN_CLASSIFIER: {
        try {
          dnn_->LoadModel(step.dnn_cl_mod().model(),
                          step.dnn_cl_mod().lang());
        } catch (ta::Exception &te) {
          throw;
        }
        break;
      }
      case ClassifyingMethod::CLM_FINAL_RULE: {
        break;
      }
      default:
        break;
    }
  }
  if (!hmd_->LoadModels(hmdmodels)) {
    is_done = false;
  }
  logger_debug("ClassifyingHandler Init..end - {}", is_done);
  return is_done;
}

/**
 * utter에 대한 nlp 처리 후 Document 생성 하는 함수
 * (eng에 대해서는 미구현)
 *
 * @param req 의도파악 수행을 위한 기본 정보
 * @param document nlp 처리를 거친 Document 결과
 * @return is_done Prepare 함수 완료 여부
 */
bool ClassifyingHandler::Prepare(const FindIntentRequest *req,
                                 Document *document) {
  auto logger = LOGGER();
  bool is_done = false;
  logger_debug(">>> [Prepare ClassifyingHandler] Start");
  unique_ptr<InputText> input_text(new InputText());
  input_text->set_text(req->utter().utter());
  input_text->set_level(NlpAnalysisLevel::NLP_ANALYSIS_NAMED_ENTITY);
  input_text->set_keyword_frequency_level(KeywordFrequencyLevel::KEYWORD_FREQUENCY_NONE);
  input_text->set_use_tokenizer(false);
  input_text->set_split_sentence(true);
  // 1. nlp 처리
  if (req->utter().lang() == Lang::ko_KR) { // 한국
    logger_debug("Analyze sentences(ko_KR)");
    input_text->set_lang(::maum::common::LangCode::kor);
    logger_trace("InputText={}", input_text->Utf8DebugString());
    // NLP 분석 전에 치환사전 적용 후 언어분석을 수행하는 함수
    nlp_kor_->SpaceAndAnalyze(input_text.get(), document);
    logger_trace("Document={}", document->Utf8DebugString());
    is_done = true;
    logger_debug("  +text={}", document->sentences(0).text());
  } else if (req->utter().lang() == Lang::en_US) { // 영어
    logger_debug("Analyze sentences(en_US)");
    input_text->set_lang(::maum::common::LangCode::eng);
    logger_trace("InputText={}", input_text->Utf8DebugString());
    nlp_eng_->Analyze(input_text.get(), document);
  }
  logger_debug("<<< [Prepare ClassifyingHandler] End");
  return is_done;
}


/**
 * 의도파악 단계의 분류방식에 따라 분류를 수행하고 실행결과값을 저장한다.
 *
 * @param step  의도파악 단계
 * @param request 의도파악 수행을 위한 기본 정보
 * @param response 의도파악 이력의 실행결과 정보
 * @param document Document 정보
 * @return result 분류결과유무
 */
bool ClassifyingHandler::Classify(const IntentFindingStep *step,
                                  const FindIntentRequest *request,
                                  RunResult *response,
                                  Replacement *rep,
                                  const Document &document) {
  auto logger = LOGGER();
  const string &step_style = IntentFindingFlowControl_Name(step->run_style());
  const auto &step_type = step->type();
  const string &cl_type = ClassifyingMethod_Name(step_type);
  const string &step_name = step->name();
  const string &utter = request->utter().utter();
  logger_debug("Classify utter:{} on step:{} type/runstyle:{}/{} Start.",
               utter, step_name, cl_type, step_style);
  bool result = false;
  switch (step_type) {
    case ClassifyingMethod::CLM_CLASSIFY_SCRIPT: {
      result = ClassifyCustomScript(step, request,
                                    response, rep,
                                    document);
      break;
    }
    case ClassifyingMethod::CLM_HINT_COMPOSER_SCRIPT: {
      result = ClassifyComposerScript(step, request,
                                      response,
                                      document);
      break;
    }
    case ClassifyingMethod::CLM_PCRE: {
      result = ClassifyPcre(step,
                            request,
                            response,
                            document);
      break;
    }
    case ClassifyingMethod::CLM_XDC_CLASSIFIER: {
      result = ClassifyXdc(step, request,
                           response,
                           document);
      break;
    }
    case ClassifyingMethod::CLM_HMD: {
      result = ClassifyHmd(step,
                           request,
                           response,
                           document);
      break;
    }
    case ClassifyingMethod::CLM_DNN_CLASSIFIER: {
      result = ClassifyDnn(step, request,
                           response,
                           document);
      break;
    }
    case ClassifyingMethod::CLM_FINAL_RULE: {
      result = ClassifyFinalRule(step, request,
                                 response,
                                 document);
      break;
    }
    default: {
      break;
    }
  }
  // 기본적인 response 값들을 만든다.
  response->set_type(step_type);
  response->set_name(step_name);
  response->set_is_hint(step->is_hint());
  logger_debug("Classify utter:{} on step:{} type/runstyle:{}/{} End - "
               "Find Result:{}", utter, step_name, cl_type, step_style, result);
  return result;
}

/**
 * 커스텀 스크립트 분류실행 결과가 존재하면 의도파악 실행 결과에 값을 채운다.
 *
 * @param step 의도파악 단계
 * @param request 의도파악 수행을 위한 기본 정보
 * @param result 의도파악 이력의 의도파악 실행 결과
 * @param document Document
 * @return found 분류결과유무
 */
bool ClassifyingHandler::ClassifyCustomScript(const IntentFindingStep *step,
                                              const FindIntentRequest *request,
                                              RunResult *result,
                                              Replacement *rep,
                                              const Document &document) {
  auto logger = LOGGER();
  CustomClassifyParam func_param;
  CustomClassifyResult func_result;
  const string &step_name = step->name();
  logger_debug("[ClassifyCustomScript] step={} Start", step_name);

  func_param.mutable_utter()->CopyFrom(request->utter());
  func_param.set_chatbot(request->chatbot());
  func_param.mutable_system_context()->CopyFrom(request->system_context());
  func_param.mutable_session()->CopyFrom(request->session());
  func_param.mutable_exclude_skills()->CopyFrom(request->exclude_skills());
  func_param.mutable_document()->CopyFrom(document);

  bool found;
  {
    ClElapsedTimer timer("Classifying CustomScript");

    // m2u-itf-cs-py-runner 를 사용하도록 함.
    func_param.mutable_rule()->CopyFrom(step->custom_script_mod());
    auto ok = custom_script_client_->Classify(&func_param, func_result);
    // 응답 결과가 비어 있지 않으면 `true`로 처리되어야 한다.
    found = (ok && !func_result.classify_result().empty());
  }
  const string &res_str = func_result.classify_result();

  if (!func_result.replaced_utter().empty()
      && !func_result.replacements().empty()) {
    rep->set_replaced_utter(func_result.replaced_utter());
    auto rep_map = rep->mutable_replacements();
    rep_map->insert(func_result.replacements().begin(),
                    func_result.replacements().end());
    if (logger->level() < spdlog::level::debug) {
      for (const auto &repx : func_result.replacements()) {
        logger_trace("replaced key = {} replaced = {}",
                     repx.first, repx.second);
      }
    }
    logger_info("itf utter changed to replaced_utter: {}",
                rep->replaced_utter());
  }

  std::ostringstream meta_oss;
  if (func_result.custom_meta_size() > 0) {
    meta_oss << "{";
    int i = 0;
    for (auto &meta: func_result.custom_meta()) {
      meta_oss << "\"" << meta.first << "\":\"" << meta.second << "\",";
      logger_debug("custom_meta[{}] - {}: {}", i, meta.first, meta.second);
      ++i;
    }
    meta_oss.seekp(-1, std::ios_base::end);
    meta_oss << "}";
  }

  if (found) {
    logger_debug("Find ITF Result");
    result->set_has_result(true);
    if (step->is_hint()) {
      result->set_hint(res_str);
      logger_debug("Find hint={}", res_str);
    } else {
      result->set_skill(res_str);
      logger_debug("Find skill={}", res_str);
    }
    result->set_probability(1.0);
    result->set_detail(meta_oss.str());
    logger_debug("[ClassifyCustomScript] step:{} Find Result:{} End",
                 step_name,
                 res_str);
  } else {
    logger_debug(
        "[ClassifyCustomScript] step:{} 'Not Found Result' End",
        step_name);
  }
  return found;
}

/**
 *
 * @param step
 * @param request
 * @param result
 * @param document
 * @return
 */
bool ClassifyingHandler::ClassifyComposerScript(const IntentFindingStep *,
                                                const FindIntentRequest *,
                                                RunResult *,
                                                const Document &) {
  return true;
}

/**
 * pcre 규칙들을 차례로 접근하면서 utter에 알맞은 skill 이 존재한다면
 * 의도파악 이력에 결과값을 저장한다.
 *
 * @param step 의도파악 단계
 * @param request 의도파악 수행을 위한 기본 정보
 * @param result 의도파악 이력의 실행결과 정보
 * @param document Document 정보
 * @return found 분류결과유무
 */
bool ClassifyingHandler::ClassifyPcre(const IntentFindingStep *step,
                                      const FindIntentRequest *request,
                                      RunResult *result,
                                      const Document &document) {
  auto logger = LOGGER();
  string utter;
  string skill, rule;
  bool found = false;
  const string &step_name = step->name();
  const string &model = step->pcre_mod().model();

  logger_debug("[ClassifyPCRE] step/model={}/{} Start", step_name, model);

  auto sc_model = sc_pcre_.find(model);
  if (sc_model == sc_pcre_.end()) {
    logger_warn("Cannot access '{}' pcre.", model);
    return found;
  }
  auto pcre = *sc_model->second->load();
  auto lang = (::maum::common::LangCode) step->pcre_mod().lang();
  if (lang == ::maum::common::kor) {
    if (pcre->GetPosTagging() && pcre->GetNamedEntity()) {
      nlp_kor_->GetPosNerTaggedStr(&document, utter);
    } else if (pcre->GetNamedEntity()) {
      nlp_kor_->GetNerTaggedStr(&document, utter);
    } else if (pcre->GetPosTagging()) {
      nlp_kor_->GetPosTaggedStr(&document, utter);
    } else {
      utter = request->utter().utter();
    }
  } else if (lang == ::maum::common::eng) {
    if (pcre->GetPosTagging() && pcre->GetNamedEntity()) {
      // TODO NER Tag 미구현.
      nlp_eng_->GetPosNerTaggedStr(&document, utter);
    } else if (pcre->GetNamedEntity()) {
      // TODO NER Tag 미구현.
      nlp_eng_->GetNerTaggedStr(&document, utter);
    } else if (pcre->GetPosTagging()) {
      nlp_eng_->GetPosTaggedStr(&document, utter);
    } else {
      utter = request->utter().utter();
    }
  }
  logger_debug(" compare sc_name={}, utter={}, pos={}, ner={}",
               model,
               utter,
               pcre->GetPosTagging(),
               pcre->GetNamedEntity());
  {
    ClElapsedTimer timer("Classifying PCRE");
    found = pcre->Find(utter, skill, rule);
  }
  if (found) {
    result->set_has_result(true);
    if (step->is_hint()) {
      logger_debug("Find hint={} from {} on step {}", skill, model, step_name);
      result->set_hint(skill);
    } else {
      result->set_skill(skill);
      logger_debug("Find skill={} from {} on step {}", skill, model, step_name);
    }
    result->set_probability(1.0);
    result->set_detail(rule);
    logger_debug("[ClassifyPCRE] step/model={}/{} Find Result:{} End",
                 step_name, model, skill);
  } else {
    logger_debug("[ClassifyPCRE] step/model={}/{} 'Not Found Result' End",
                 step_name, model);
  }
  return found;
}

/**
 * request 정보의 document에 대하여 의도파악 정보의 모델, 언어를  기준으로
 * HMD 분류를 수행한 후 결과가 존재한다면 의도파악 이력에 결과값을 저장한다.
 *
 * @param step 의도파악 단계
 * @param request 의도파악 수행을 위한 기본 정보
 * @param result 의도파악 이력의 실행결과 정보
 * @param document Document 정보
 * @return found 분류결과유무
 */
bool ClassifyingHandler::ClassifyHmd(const IntentFindingStep *step,
                                     const FindIntentRequest *,
                                     RunResult *result,
                                     const Document &document) {
  auto logger = LOGGER();
  HmdInputDocument hmd_req;
  HmdResult hmd_result;
  const string &model = step->hmd_mod().model();
  const string &step_name = step->name();
  logger_debug(">>> [ClassifyHmd] step/model:{}/{} Start", step_name, model);
  hmd_req.set_model(model);
  hmd_req.set_lang((::maum::common::LangCode) step->hmd_mod().lang());
  hmd_req.mutable_document()->CopyFrom(document);
  bool found;
  {
    ClElapsedTimer timer("Classifying Hmd");
    found = hmd_->Classify(&hmd_req, &hmd_result);
  }
  string res, rule;
  if (found) {
    res = hmd_result.cls(0).category();
    rule = hmd_result.cls(0).pattern();
    result->set_has_result(true);
    if (step->is_hint()) {
      result->set_hint(res);
      logger_debug("Find hint={}/rule={} from {} on step {}",
                   res, rule, model, step_name);
    } else {
      result->set_skill(res);
      logger_debug("Find skill={}/rule={} from {} on step {}",
                   res, rule, model, step_name);
    }
    result->set_detail(rule);
    result->set_probability(1.0);
    logger_debug("[ClassifyHmd] step/model:{}/{} Find Result:{} End",
                 step_name,
                 model,
                 res);
  } else {
    logger_debug("[ClassifyHmd] step/model:{}/{} 'Not Found Result' End",
                 step_name, model);
  }
  return found;
}

/**
 * request 정보의 document에 대하여 의도파악 정보의 모델, 언어를  기준으로
 * Dnn 분류를 수행한 후 결과가 존재한다면 의도파악 이력에 결과값을 저장한다.
 *
 * @param step 의도파악 단계
 * @param request 의도파악 수행을 위한 기본 정보
 * @param result 의도파악 이력의 실행결과 정보
 * @param document Document 정보
 * @return found 분류결과유무
 */
bool ClassifyingHandler::ClassifyDnn(const IntentFindingStep *step,
                                     const FindIntentRequest *,
                                     RunResult *result,
                                     const Document &document) {
  auto logger = LOGGER();
  bool found = false;
  ClassifiedSummary dnn_result;

  const auto &cl_mod = step->dnn_cl_mod();
  const string &step_name = step->name();
  const string &model = cl_mod.model();
  logger_debug(">>> [ClassifyDnn] step/model:{}/{} Start ", step_name, model);
  bool run;
  {
    ClElapsedTimer timer("Classifying DNN");
    run = dnn_->Classify(cl_mod.model(), cl_mod.lang(),
                         document, &dnn_result);
  }
  if (run) {
    float cut = cl_mod.cut_coefficient();
    // calc_cut
    if (dnn_result.probability_top() >= cut) {
      found = true;
      logger_trace("use dnn result top_progb:{} >= cut_coefficient:{}",
                   dnn_result.probability_top(), cut);
      logger_trace("DUMP dnn_result: {}", dnn_result.Utf8DebugString());

    } else {
      logger_trace("CUT result cut_top_progb:{} < cut_coefficient:{}",
                   dnn_result.probability_top(), cut);
      logger_trace("DUMP dnn_result: {}", dnn_result.Utf8DebugString());
    }
  } else {
    logger_error("IN DNN, invalid step value {} {}",
                 step->type(),
                 step->name());
    return false;
  }

  const string &res = dnn_result.c1_top();
  if (found) {
    result->set_has_result(true);
    if (step->is_hint()) {
      result->set_hint(res);
      logger_debug("Find hint={}", res);
    } else {
      result->set_skill(res);
      logger_debug("Find skill={}", res);
    }
    logger_debug("<<< [ClassifyDnn] step/model:{}/{}, Find result:{}, {} End",
                 step_name, model, found, res);
  } else {
    logger_debug("<<< [ClassifyDnn] step/model:{}/{}, 'Not Found Result' End",
                 step_name, model);
  }
  result->set_detail(res);
  result->set_probability(dnn_result.probability_top());

  return found;
}

/**
 * request 정보의 document에 대하여 의도파악 정보의 모델, 언어를  기준으로
 * Xdc 분류를 수행한 후 결과가 존재한다면 의도파악 이력에 결과값을 저장한다.
 *
 * @param step 의도파악 단계
 * @param request 의도파악 수행을 위한 기본 정보
 * @param result 의도파악 이력의 실행결과 정보
 * @param document Document 정보
 * @return found 분류결과유무
 */
bool ClassifyingHandler::ClassifyXdc(const IntentFindingStep *step,
                                     const FindIntentRequest *,
                                     RunResult *result,
                                     const Document &document) {
  auto logger = LOGGER();
  bool found = false;
  HanResult *xdc_result(new HanResult());
  HanInputDocument xdc_document;
  HanInputCommon xdc_common;

  const auto &cl_mod = step->xdc_cl_mod();
  const string &step_name = step->name();
  const string &model = cl_mod.model();
  logger_debug(">>> [ClassifyXdc] step/model:{}/{} Start ", step_name, model);
  xdc_document.mutable_document()->CopyFrom(document);
  xdc_common.set_lang(::maum::common::LangCode::kor);
  xdc_common.set_top_n(1);
  xdc_common.set_use_attn_output(false);
  xdc_document.mutable_common()->CopyFrom(xdc_common);

  bool run;
  {
    ClElapsedTimer timer("Classifying XDC");
    run = xdc_client_->Classify(xdc_document, xdc_result);
    logger_trace("[ClassifyXdc] HanResult: {}", xdc_result->Utf8DebugString());
  }

  if (!run) {
    logger_warn("IN XDC, invalid step value {} {}", step->type(), step->name());
    return found;
  }

  if (xdc_result->cls_size() > 0) {
    const string &res = xdc_result->cls(0).label();
    float cut = cl_mod.cut_coefficient();
    float probabilty = xdc_result->cls(0).probability();
    // calc_cut
    if (probabilty >= cut) {
      found = true;
      logger_trace("use xdc result top_progb:{} >= cut_coefficient:{}",
                   probabilty, cut);
      logger_trace("DUMP xdc_result: {}", xdc_result->Utf8DebugString());
    } else {
      logger_trace("CUT result cut_top_progb:{} < cut_coefficient:{}",
                   probabilty, cut);
      logger_trace("DUMP xdc_result: {}", xdc_result->Utf8DebugString());
    }
    if (found) {
      result->set_has_result(true);
      if (step->is_hint()) {
        result->set_hint(res);
        logger_debug("Find hint={}", res);
      } else {
        result->set_skill(res);
        logger_debug("Find skill={}", res);
      }
      logger_debug("<<< [ClassifyXdc] step/model:{}/{}, Find result:{}, {} End",
                   step_name, model, found, res);
    } else {
      logger_debug("<<< [ClassifyXdc] step/model:{}/{}, 'Not Found Result' End",
                   step_name, model);
    }
    result->set_detail(res);
    result->set_probability(xdc_result->cls(0).probability());
  } else {
    logger_debug("<<< [ClassifyXdc] step/model:{}/{}, 'Not Found Result' End",
                 step_name, model);
  }
  return found;
}

/**
 * request 정보의 document에 대하여 의도파악 정보의 모델, 언어를  기준으로
 * Final 분류를 수행한 후 결과가 존재한다면 의도파악 이력에 결과값을 저장한다.
 *
 * @param step 의도파악 단계
 * @param request 의도파악 수행을 위한 기본 정보
 * @param result 의도파악 이력의 실행결과 정보
 * @param document Document 정보
 * @return found final 분류 결과 유무
 */
bool ClassifyingHandler::ClassifyFinalRule(const IntentFindingStep *step,
                                           const FindIntentRequest *,
                                           RunResult *result,
                                           const Document &) {
  auto logger = LOGGER();
  const string &step_name = step->name();
  logger_debug(">>> [ClassifyFinalRule] step:{} Start ", step_name);
  bool found = true;
  string skill;
  {
    ClElapsedTimer timer("Classifying FinalRule");
    skill = step->final_rule().final_skill();
  }
  // FinalRule은 무조건 true를 리턴 한다.
  result->set_skill(skill);
  result->set_has_result(true);
  result->set_is_hint(step->is_hint());
  result->set_probability(1.0);
  logger_debug("<<<[ClassifyFinalRule] step/skill={}/{}", step_name, skill);
  return found;
}

/**
 * request 정보의 document에 대하여 의도파악 정보의 모델, 언어를  기준으로
 * Final 분류를 수행한 후 결과가 존재한다면 의도파악 이력에 결과값을 저장한다.
 *
 * @param sc_name simpleclassifier 이름
 * @param pcre SimpleClassifierPcre
 * @return pcre 데이터 존재유무
 */
bool ClassifyingHandler::ParsingScNlp(
    const string &sc_name,
    std::shared_ptr<SimpleClassifierPcre> &pcre) {
  auto logger = LOGGER();
  logger_debug("ParsingScNlp name={}", sc_name);
  try {
    pcre->SetName(sc_name);
    std::shared_ptr<LocalSimpleClassifier>
        sc = hzc::GetInstance()->GetSimpleClassifier(sc_name);
    if (sc == nullptr)
      return false;
    logger_debug(" SC name={}, lang={}, pos_tag={}, ner={}",
                 sc->name(),
                 sc->lang(),
                 sc->pos_tagging(),
                 sc->named_entity());
    for (const auto &sl : sc->skill_list()) {
      logger_debug(" skill_name={}", sl.name());
      for (int index = 0; index < sl.regex_size(); index++) {
        logger_debug("  +{}/{}, skill={}",
                     index,
                     sl.regex_size(),
                     sl.regex(index));
        const string &pcre_regex = sl.regex(index);

        if (sc->pos_tagging())
          pcre->SetPosTagging(true);
        if (sc->named_entity())
          pcre->SetNamedEntity(true);

        pcre->AddPcre(sl.name(), pcre_regex);
      }
    }
  } catch (IException &e) {
    logger_critical("SimpleClassifierPcre::{} Hazelcast: {}",
                    __func__,
                    e.what());
  }
  return true;
}
/**
 * Simple Classifier 이름과 일치하는 클래스 멤버변수 sc_pcre_에 존재 할 경우
 * Hzc Sc Map에 등록된 정보로 갱신하는 함수.
 * @param sc_name Simple Classifier Model name
 * @param policy_name ITF Policy name
 * @return true
 */
bool ClassifyingHandler::UpdatePcre(const string &sc_name,
                                    const string &policy_name) {
  auto logger = LOGGER();
  logger_debug("<<[PCRE {} Update Event Start]", sc_name);

  auto it = FindPcre(sc_name, policy_name);
  if (it != sc_pcre_.end()) {
    auto pcre = make_shared<SimpleClassifierPcre>();

    pcre->SetName(sc_name);
    pcre->UpdatePcreHzc();

    auto old = it->second->exchange(new shared_ptr<SimpleClassifierPcre>(pcre));
    delete old;
  } else {
    logger_debug(">>[PCRE {} is not exist.]", sc_name);
  }

  logger_debug(">>[PCRE Update Event End]");
  return true;
}
/**
 * Simple Classifier 이름과 일치하는 클래스 멤버변수 sc_pcre_에 존재 할 경우
 * 해당 정보를 삭제하는 함수.
 * @param sc_name Simple Classifier Model name
 * @param policy_name ITF Policy name
 * @return true
 */
bool ClassifyingHandler::DeletePcre(const string &sc_name,
                                    const string &policy_name) {
  auto logger = LOGGER();
  logger_debug("<<[PCRE {} Delete Event Start]", sc_name);
  auto it = FindPcre(sc_name, policy_name);
  if (it != sc_pcre_.end()) {
    delete it->second->load();
    sc_pcre_.erase(sc_name);
    logger_debug("  [Delete PCRE]");
  } else {
    logger_debug(">>[PCRE {} is not exist.]", sc_name);
  }
  logger_debug("<<[PCRE {} Delete Event End]", sc_name);
  return true;
}

/**
 * Hzc IntentFinderPolicy Map에 Simple Classifier 이름과 일치하는 값이 존재하는지
 * 확인하는 함수
 * @param sc_name Simple Classifier Model name
 * @param policy_name ITF Policy name
 * @return sc_name과 일치하는 sc_pcre_ iterator 반환
 */
ScPcreMap::iterator ClassifyingHandler::FindPcre(const string &sc_name,
                                                 const string &policy_name) {
  auto logger = LOGGER();

  auto policy = hzc::GetInstance()->GetIntentFinderPolicy(policy_name);
  for (const auto &step: policy->steps()) {
    if (step.type() == ClassifyingMethod::CLM_PCRE) {
      auto it = sc_pcre_.find(sc_name);
      if (it != sc_pcre_.end()) {
        logger_debug("  [Find PCRE({}) in {}]", sc_name, policy_name);
        return it;
      }
    }
  }
  logger_debug("  [Not Found PCRE({}) in {}]", sc_name, policy_name);
  return sc_pcre_.end();
}
