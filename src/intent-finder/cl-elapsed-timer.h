#include <libmaum/common/config.h>

using namespace std::chrono;
using namespace spdlog::level;

class ClElapsedTimer {
 public:
  ClElapsedTimer() = delete;
  explicit ClElapsedTimer(const char *msg, level_enum lv = trace)
      : level_(lv), msg_(msg) {
    do_log_ = LOGGER()->level() <= level_;
    if (do_log_) {
      start_ = system_clock::now();
    }
  }
  virtual  ~ClElapsedTimer() {
    if (do_log_) {
      auto took = duration_cast<microseconds>(system_clock::now() - start_);
      LOGGER_log(level_, "[{}] took {} μs.", msg_, took.count());
    }
  }

 private:
  level_enum level_;
  bool do_log_;
  const string msg_;
  system_clock::time_point start_;
};
