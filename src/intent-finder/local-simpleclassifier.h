#ifndef PROJECT_LOCAL_SIMPLECLASSIFIER_H
#define PROJECT_LOCAL_SIMPLECLASSIFIER_H

#include <maum/m2u/console/classifier.pb.h>
#include "hazelcast.h"

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

class LocalSimpleClassifier
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::console::SimpleClassifier {
 public:
  LocalSimpleClassifier() {}
  LocalSimpleClassifier(const LocalSimpleClassifier &rhs) {
    this->CopyFrom(rhs);
  }
  virtual ~LocalSimpleClassifier() {}

  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  }

  int getClassId() const override {
    return HazelcastTypeId::SIMPLE_CLASSIFIER;
  };

  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeUTF("name", &this->name());
    out.writeByteArray("_msg", &data);
    out.writeInt("lang", this->lang());
    //out.writeBoolean("pos_tagging", this->pos_tagging());
    //out.writeBoolean("named_entity", this->named_entity());
  }

  void readPortable(PortableReader &in) override {
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  }
};
#endif //PROJECT_LOCAL_SIMPLECLASSIFIER_H
