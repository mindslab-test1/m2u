#include <sys/resource.h>
#include <grpc++/grpc++.h>
#include <google/protobuf/map.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include "tls-value.h"
#include "sc-handler.h"
#include <google/protobuf/util/json_util.h>
#include "local-simpleclassifier.h"

using hzc = ItfHazelcastClient;
using google::protobuf::util::MessageToJsonString;

SimpleClassifierHandler::~SimpleClassifierHandler() {
  if (!registration_id_.empty()) {
    RemoveListener();
  }
}

/**
 * Hazelcast Map Listener 설정
 * SimpleClassifier 등록, 삭제 시 처리하는 Listner를 설정
 */
void SimpleClassifierHandler::AddListener() {
  if (!registration_id_.empty()) {
    LOGGER()->debug("{} Listener already Registered.", __func__);
    return;
  }

  registration_id_ =
      hzc::GetInstance()->GetHazelcastClient()->getMap<string, ScInfo>(
          getMapName()).addEntryListener(*this, true);

  LOGGER()->debug("Registered Listener for '{}' as [{}]",
                  getMapName(), registration_id_);
}

void SimpleClassifierHandler::RemoveListener() {
  if (hzc::GetInstance()->GetHazelcastClient()->getMap<string, ScInfo>(
          getMapName()).removeEntryListener(registration_id_)) {
    LOGGER()->debug("Unregistered Listener[{}] from '{}'",
                    registration_id_, getMapName());
  } else {
    LOGGER()->debug("Failed to unregister the Listener[{}] for '{}'.",
                    registration_id_, getMapName());
  }
  registration_id_.clear();
}

void SimpleClassifierHandler::SetPolicyName(const string &name) {
  policy_name_ = name;
}

ClassifyingHandler* SimpleClassifierHandler::GetClassifyingHandler() {
  ClassifyingHandler *ptr = &classifying_handler_;
  return ptr;
}

/**
 * SimpleClassifier Map에 Entry가 추가되었을 때, 추가된 SC를 실행한다.
 *
 * @param event entry event
 */
void SimpleClassifierHandler::entryAdded(const ScInfoEvent &event) {
  LOGGER()->info("SimpleClassifierHandler::{} is Unimplemented.", __func__);
}

/**
 * SimpleClassifier Map에 Entry가 삭제되었을 때, itf의 해당 SimpleClassifier 데이터를 삭제한다.
 *
 * @param event entry event
 */
void SimpleClassifierHandler::entryRemoved(const ScInfoEvent &event) {
  auto logger = LOGGER();
  const string sc_name = event.getKey();
  logger_info("Simple Classifier Removed: {}/ {}", sc_name, policy_name_);;
  std::thread([=]() {
    logger_debug("<<[sc_info.mutable print end]");
    classifying_handler_.DeletePcre(sc_name, policy_name_);
    logger_debug("<<[ RemovePcre {} Thread End]", sc_name);
  }).detach();
}

/**
 * DAActivation Map에 Entry가 업데이트되었을 때, 기존 DA를 종료 후 재실행한다.
 *
 * @param event entry event
 */
void SimpleClassifierHandler::entryUpdated(const ScInfoEvent &event) {
  auto logger = LOGGER();
  const string sc_name = event.getKey();
  logger_info("Simple Classifier Updated: {}/ {}", sc_name, policy_name_);;
  std::thread([=]() {
    logger_debug("<<[sc_info.mutable print end]");
    classifying_handler_.UpdatePcre(sc_name, policy_name_);
  }).detach();
}

/**
* Invoked when an entry is evicted.
*
* @param event entry event
*/
void SimpleClassifierHandler::entryEvicted(const ScInfoEvent &event) {
  LOGGER()->info("SimpleClassifierHandler::{} is Unimplemented.", __func__);
}

/**
* Invoked upon expiration of an entry.
*
* @param event the event invoked when an entry is expired.
*/
void SimpleClassifierHandler::entryExpired(const ScInfoEvent &event) {
  LOGGER()->info("SimpleClassifierHandler::{} is Unimplemented.", __func__);
}

/**
*  Invoked after WAN replicated entry is merged.
*
* @param event the event invoked when an entry is expired.
*/
void SimpleClassifierHandler::entryMerged(const ScInfoEvent &event) {
  LOGGER()->info("SimpleClassifierHandler::{} is Unimplemented.", __func__);
}

/**
* Invoked when all entries evicted by {@link IMap#evictAll()}.
*
* @param event map event
*/
void SimpleClassifierHandler::mapEvicted(const MapEvent &event) {
  LOGGER()->info("SimpleClassifierHandler::{} is Unimplemented.", __func__);
}

/**
* Invoked when all entries are removed by {@link IMap#clear()}.}
*/
void SimpleClassifierHandler::mapCleared(const MapEvent &event) {
  LOGGER()->info("SimpleClassifierHandler::{} is Unimplemented.", __func__);
}
