#ifndef PROJECT_LOCAL_INTENTFINDER_POLICY_H
#define PROJECT_LOCAL_INTENTFINDER_POLICY_H

#include <maum/m2u/router/v3/intentfinder.pb.h>

#include "hazelcast.h"

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

/**
 *
 */
class LocalIntentFinderPolicy
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::router::v3::IntentFinderPolicy {
 public:
  LocalIntentFinderPolicy() {}
  LocalIntentFinderPolicy(const LocalIntentFinderPolicy &rhs) {
    this->CopyFrom(rhs);
  }
  virtual ~LocalIntentFinderPolicy() {}

  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  }

  int getClassId() const override {
    return INTENT_FINDER_POLICY;
  }

  /**
   *
   * @param out
   */
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeUTF("name", &this->name());
    out.writeByteArray("_msg", &data);
    out.writeInt("lang", this->lang());
    out.writeUTF("default_skill", &this->default_skill());
  }

  /**
   *
   * @param in
   */
  void readPortable(PortableReader &in) override {
//    in.readUTF("name");
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  }
};

#endif //PROJECT_LOCAL_INTENTFINDER_POLICY_H
