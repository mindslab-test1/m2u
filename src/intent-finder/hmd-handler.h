#ifndef PROJECT_HMD_HANDLER_H
#define PROJECT_HMD_HANDLER_H

#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>
#include <set>

#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <libmaum/brain/nlp/nlp3kor.h>
#include <libmaum/brain/hmd/kor-hmd.h>
#include <libmaum/brain/hmd/eng-hmd.h>
#include <libmaum/brain/nlp/nlp2eng.h>
#include <libmaum/brain/nlp/nlp2kor.h>
#include <libmaum/brain/nlp/nlp3kor.h>

using maum::common::Lang;
using maum::brain::hmd::HmdModel;
using maum::brain::hmd::HmdModelList;
using maum::brain::hmd::HmdResult;
using maum::brain::hmd::HmdResultDocument;
using maum::brain::hmd::ModelKey;
using maum::brain::hmd::HmdInputText;
using maum::brain::hmd::HmdInputDocument;
using maum::brain::hmd::HmdClassifier;


struct HmdModelInfo {
  string model;
  Lang lang;
};


class HmdHandler {
 public:

  HmdHandler() {};
  virtual ~HmdHandler() {};

  /// 라이브러리를 로딩한다.
  bool Init();
  /// 분류기를 실행한다.
  bool Classify(HmdInputDocument *request, HmdResult *result);
  bool LoadModels(const set<HmdModelInfo> &models);

 private:

  bool is_started_ = false;
  ModelLoader ml_;
  KorHmd k_cl_;
  EngHmd e_cl_;
};

#endif //PROJECT_HMD_HANDLER_H
