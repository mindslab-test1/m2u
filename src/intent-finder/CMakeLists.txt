cmake_minimum_required(VERSION 2.8.12)

project(intent-finder CXX)
set(BINOUT m2u-itfd)

include(gitversion/cmake)
FIND_PACKAGE(PythonLibs "2.7" REQUIRED)

add_definitions(-Wall)
add_definitions(-Wno-sign-compare)
add_definitions(-Wno-unused-local-typedefs)

include_directories(../../proto)

include_directories("${CMAKE_INSTALL_PREFIX}/include")
link_directories("${CMAKE_INSTALL_PREFIX}/lib")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unknown-pragmas -Wno-deprecated")
set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif ()

include_directories(${PYTHON_INCLUDE_DIRS})

set(SOURCE_FILES
    main.cc
    hazelcast.cc hazelcast.h
    intent-finder-impl.cc intent-finder-impl.h
    sc-pcre.cc sc-pcre.h
    custom-script-client.cc custom-script-client.h
    classifying-handler.cc classifying-handler.h
    the-portable-factory.h
    local-intentfinder.h
    local-simpleclassifier.h
    nlp-kor-handler.cc nlp-kor-handler.h
    nlp-eng-handler.cc nlp-eng-handler.h
    local-intentfinder-policy.h
    hmd-handler.cc hmd-handler.h
    dnn-handler.cc dnn-handler.h
    xdc-client.cc xdc-client.h
    sc-handler.cc sc-handler.h
    tls-value.cc tls-value.h
    classification-record.h)

# deprecated
#   custom-script.cc custom-script.h

add_executable(${BINOUT} ${SOURCE_FILES})

target_git_version_init(${BINOUT})

ADD_LIBRARY(boost_regex_nlp3 STATIC IMPORTED)
SET_TARGET_PROPERTIES(boost_regex_nlp3
    PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/lib/libboost_regex_nlp3.a)

ADD_LIBRARY(boost_system_nlp3 STATIC IMPORTED)
SET_TARGET_PROPERTIES(boost_system_nlp3
    PROPERTIES IMPORTED_LOCATION ${CMAKE_INSTALL_PREFIX}/lib/libboost_system_nlp3.a)

set(CMAKE_EXE_LINKER_FLAGS "-fopenmp -dl")

target_link_libraries(${BINOUT}
    dl pcre
    brain-hmd
    brain-nlp2eng
    brain-nlp2kor
    brain-nlp3kor
    brain-ta-pb
    m2u-all-pb
    maum-common
    maum-rpc
    maum-json
    maum-pb
    HazelcastClient
    boost_regex_nlp3
    boost_system_nlp3
    curl
#    ${PYTHON_LIBRARIES}
    grpc++ grpc++_reflection
    protobuf
    atomic
    )

install(TARGETS ${BINOUT} RUNTIME DESTINATION bin)
