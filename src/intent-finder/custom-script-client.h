
#ifndef PROJECT_CUSTOM_SCRIPT_CLIENT_H
#define PROJECT_CUSTOM_SCRIPT_CLIENT_H

#include <grpc++/grpc++.h>
#include <maum/m2u/router/v3/intentfinder.grpc.pb.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using maum::m2u::router::v3::CustomScriptRunner;
using maum::m2u::router::v3::CustomClassifyParam;
using maum::m2u::router::v3::CustomClassifyResult;

class CustomScriptClient {
 public:
  CustomScriptClient(std::shared_ptr<Channel> channel)
      : stub_(CustomScriptRunner::NewStub(channel)) {}

 public:
  bool Classify(CustomClassifyParam *request, CustomClassifyResult &response);

 private:
  std::unique_ptr<CustomScriptRunner::Stub> stub_;
};

#endif //PROJECT_CUSTOM_SCRIPT_CLIENT_H
