#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <libmaum/brain/nlp/nlp2eng.h>
#include "nlp-eng-handler.h"
#include "tls-value.h"
#include <thread>
#include <libmaum/common/config.h>
#include <libmaum/brain/error/ta-error.h>

using std::unique_ptr;
using grpc::StatusCode;

const int kIntentFinderFeature = (1 << NlpFeature::NLPF_MORPHEME) |
    (1 << NlpFeature::NLPF_NAMED_ENTITY);

/**
 * nlp 처리를 위한 리소스 로딩
 *
 * @return is_done 리소스 로딩 성공여부
 */
bool NlpEnglishHandler::Init() {
  bool is_done = false;
  auto logger = LOGGER();
  logger_debug(">>> [Initialize NlpEnglishHandler] Start");
  auto &c = libmaum::Config::Instance();
  rsc_path_ = c.Get("brain-ta.resource.2.eng.path");

  nlp2Eng_ = new Nlp2Eng(rsc_path_, kIntentFinderFeature);
  nlp2Eng_->UpdateFeatures();

  is_started = true;
  logger_debug("<<< [Initialize NlpEnglishHandler] End");
  is_done = true;
  return is_done;
}

/**
 * nlp 리소스 내리는 함수
 *
 * @return 리소스 내리기 성공여부
 */
bool NlpEnglishHandler::Stop() {
  bool result = false;
  auto logger = LOGGER();
  logger_debug("NlpEnglishHandler Stop");
  nlp2Eng_->Uninit();
  result = true;
  return result;
}

/**
 * 언어분석함수
 *
 * @param text 입력텍스트
 * @param document 언어분석결과 Document
 * @return is_done 언어분석 성공여부
 */
bool NlpEnglishHandler::Analyze(const InputText *text,
                               Document *document) {
  bool is_done = false;
  auto logger = LOGGER();
  if (is_started == false)
    return is_done;
  nlp2Eng_->AnalyzeOne(text, document);
  is_done = true;
  return is_done;
}

/**
 * 형태소 태그값을 저장하는 함수
 *
 * @param document 언어분석결과 Document
 * @param pos_tag 형태소 태그값
 * @return is_done 태깅 성공여부
 */
bool NlpEnglishHandler::GetPosTaggedStr(const Document *document,
                                       string &pos_tag) {
  bool is_done = false;
  vector<string> pos_tagged_str;
  nlp2Eng_->GetPosTaggedStr(document, &pos_tagged_str);
  pos_tag = "";
  for (int i = 0; i < pos_tagged_str.size(); i++) {
    pos_tag += pos_tagged_str[i];
  }
  is_done = true;
  return is_done;
}

/**
 *  개체명 태그값을 저장하는 함수
 *
 * @param document 언어분석결과 Document
 * @param ner_tag 개체명 태그값
 * @return is_done 태깅 성공여부
 */
bool NlpEnglishHandler::GetNerTaggedStr(const Document *document,
                                       string &ner_tag) {
  bool is_done = false;
  vector<string> ner_tagged_str;
  ///todo nlp에서 구현해줘야함.
  nlp2Eng_->GetNerTaggedStr(document, &ner_tagged_str);
  ner_tag = "";
  for (int i = 0; i < ner_tagged_str.size(); i++) {
    ner_tag += ner_tagged_str[i];
  }
  ner_tag = document->sentences(0).text();
  is_done = true;
  return is_done;
}
/**
 *  개체명 태그값과 Pos 태그값을 Document에서 가져오는 함수
 *
 * @param document 언어분석결과 Document
 * @param pos_ner_tag 결과값
 * @return is_done 태깅 성공여부
 */
bool NlpEnglishHandler::GetPosNerTaggedStr(const Document *document,
                                          string &pos_ner_tag) {
  bool is_done = false;
  vector<string> ner_tagged_str;
  ///todo nlp에서 구현해줘야함.
  nlp2Eng_->GetPosNerTaggedStr(document, &ner_tagged_str);
  pos_ner_tag = "";
  for (int i = 0; i < ner_tagged_str.size(); i++) {
    pos_ner_tag += ner_tagged_str[i];
  }
  pos_ner_tag = document->sentences(0).text();
  is_done = true;
  return is_done;
}
