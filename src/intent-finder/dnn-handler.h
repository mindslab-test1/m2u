#ifndef PROJECT_DNN_HANDLER_H
#define PROJECT_DNN_HANDLER_H

#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>
#include <unordered_map>

#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <libmaum/brain/nlp/nlp2eng.h>
#include <libmaum/brain/nlp/nlp2kor.h>

using maum::common::Lang;
/**
 *
 */
class DnnHandler {
 public:
  DnnHandler();
  virtual ~DnnHandler() {};

  /// 라이브러리를 로딩한다.
  bool Init();

  bool LoadModel(const string &model, maum::common::Lang lang);

  /// 분류기를 실행한다.
  bool Classify(const string &model,
                maum::common::Lang lang,
                const Document &document, ClassifiedSummary *result);

 private:
  inline string GetModelPath(const string &model, maum::common::Lang lang) {
    string model_path(model);
    model_path += '-';
    model_path += lang == Lang::ko_KR ? "kor" : "eng";
    return model_path;
  }
  unordered_map<string, std::shared_ptr<Nlp2Kor>> kor_dnn_anals_;
  unordered_map<string, std::shared_ptr<Nlp2Eng>> eng_dnn_anals_;
  std::string rsc_path_;
  std::string rsc_eng_path_;
};

#endif //PROJECT_DNN_HANDLER_H
