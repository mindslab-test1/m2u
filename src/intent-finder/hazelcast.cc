#include <boost/bind/placeholders.hpp>
#include <boost/bind.hpp>

#include <libmaum/common/config.h>
#include "hazelcast.h"
#include "the-portable-factory.h"

using namespace std;
using hazelcast::client::SerializationConfig;
using hazelcast::client::Address;
using hazelcast::client::serialization::SerializerBase;
using hzc = ItfHazelcastClient;

const char *hzc::kNsAuthPolicy = "auth-policy";
const char *hzc::kNsChatbotDetail = "chatbot-detail";
const char *hzc::kNsDAManager = "dialog-agent-manager";
const char *hzc::kNsDA = "dialog-agent";
const char *hzc::kNsDAActivation = "dialog-agent-activation";
const char *hzc::kNsDAInstance = "dialog-agent-instance";
const char *hzc::kNsDAInstExec = "dialog-agent-instance-exec";
const char *hzc::kNsIntentFinder = "intent-finder";
const char *hzc::kNsIntentFinderInstExec = "intent-finder-instance-exec";

const char *hzc::kNsSkill = "skill";
const char *hzc::kNsSC = "simple-classifier";

const char *hzc::kNsTalk = "talk";
const char *hzc::kNsSession = "session";
const char *hzc::kNsTalkNext = "talk-next";
const char *hzc::kNsChatbot = "chatbot";
const char *hzc::kNsDAIR = "dialog-agent-instance-resource";
const char *hzc::kNsIntentFinderInstance = "intent-finder-instance";
const char *hzc::kNsIntentFinderPolicy = "intent-finder-policy";
const char *hzc::kNsClassificationRecord = "classification-record";

template<typename T>
void do_release(typename boost::shared_ptr<T> const &, T *) {
}

ItfHazelcastClient::ItfHazelcastClient() {
  auto &c = libmaum::Config::Instance();
  hazelcast_client_ = NULL;

  group_name_ = c.Get("hazelcast.group.name");
  group_password_ = c.Get("hazelcast.group.password");
  server_cnt_ = (int32_t) c.GetAsInt("hazelcast.server.count");
  server_port_ = (int32_t) c.GetAsInt("hazelcast.server.port");
  invocation_timeout_ = c.GetDefaultAsInt("hazelcast.invocation.timeout", "0");

  for (auto i = 0; i < server_cnt_; i++) {
    server_address_.push_back(c.Get("hazelcast.server.ip." + to_string(i + 1)));
  }
}

ItfHazelcastClient::~ItfHazelcastClient() {
}

shared_ptr<ClientConfig> ItfHazelcastClient::GetConfig() {
  auto clientConfig = make_shared<ClientConfig>();

  if (!group_name_.empty() && group_name_ != "dev") {
    // dev, dev-pass는 기본으로 라이브러리에 박혀 있습니다.
    // 따로이 지정하지 않더라도 위와 같이 동작합니다.
    clientConfig->getGroupConfig()
        .setName(group_name_).setPassword(group_password_);
  }
  if (invocation_timeout_ > 0) {
    clientConfig->setProperty("hazelcast.client.invocation.timeout.seconds",
                              to_string(invocation_timeout_));
  }
  clientConfig->setLogLevel(hazelcast::client::LogLevel::FINEST);

  // network config
  auto &network_config = clientConfig->getNetworkConfig();
  for (const auto &addr: server_address_) {
    LOGGER()->debug("hazelcast server ip: {}", addr);
    network_config.addAddress(Address(addr, server_port_));
  }
  network_config.setSmartRouting(true);

  // serialization config
  auto &serialization_config = clientConfig->getSerializationConfig();
  serialization_config.addPortableFactory(
      PORTABLE_FACTORY_ID,
      boost::shared_ptr<PortableFactory>(new ThePortableFactory()));

  return clientConfig;
}

shared_ptr<HazelcastClient> ItfHazelcastClient::GetHazelcastClient() {
  if (!hazelcast_client_) {
    try {
      shared_ptr<ClientConfig> config = GetConfig();
      hazelcast_client_ = make_shared<HazelcastClient>(*config);
    } catch (IException &e) {
      auto logger = LOGGER();
      logger->critical("Hazelcast: {}", e.what());
      return shared_ptr<HazelcastClient>();
    }
  }

  return hazelcast_client_;
}

std::shared_ptr<LocalIntentFinderInstance> ItfHazelcastClient::GetIntentFinderInstance(
    const string &name) {
  auto logger = LOGGER();
  try {
    auto hc = GetHazelcastClient();
    auto map = hc->getMap<std::string, LocalIntentFinderInstance>(
        kNsIntentFinderInstance);
    boost::shared_ptr<LocalIntentFinderInstance> itf = map.get(name);

    return
        std::shared_ptr<LocalIntentFinderInstance>(itf.get(),
                                                   boost::bind(&do_release<
                                                                   LocalIntentFinderInstance>,
                                                               itf,
                                                               _1));
  } catch (IException &e) {
    logger->critical("IntentFinder::{} Hazelcast: {}",
                     __func__,
                     e.what());
  }
  return std::shared_ptr<LocalIntentFinderInstance>();
}

shared_ptr<LocalIntentFinderPolicy> ItfHazelcastClient::GetIntentFinderPolicy(
    const string &name) {
  auto logger = LOGGER();
  logger->debug("LocalIntentFinderPolicy {}\n", name);
  try {
    auto hc = GetHazelcastClient();
    auto map =
        hc->getMap<std::string, LocalIntentFinderPolicy>(kNsIntentFinderPolicy);
    boost::shared_ptr<LocalIntentFinderPolicy> policy = map.get(name);

    return std::shared_ptr<LocalIntentFinderPolicy>(
        policy.get(),
        boost::bind(&do_release<LocalIntentFinderPolicy>,
                    policy, _1));
  } catch (IException &e) {
    logger->critical("IntentFinder::{} Hazelcast: {}",
                     __func__,
                     e.what());
  }
  return shared_ptr<LocalIntentFinderPolicy>();
}
shared_ptr<LocalSimpleClassifier> ItfHazelcastClient::GetSimpleClassifier(
    const string &name) {
  auto logger = LOGGER();
  logger->debug("GetSimpleClassifier {}\n", name);
  try {
    auto hc = GetHazelcastClient();
    auto map =
        hc->getMap<std::string, LocalSimpleClassifier>(kNsSC);
    boost::shared_ptr<LocalSimpleClassifier> object = map.get(name);

    return std::shared_ptr<LocalSimpleClassifier>(
        object.get(),
        boost::bind(&do_release<LocalSimpleClassifier>,
                    object, _1));
  } catch (IException &e) {
    logger->critical("IntentFinder::{} Hazelcast: {}",
                     __func__,
                     e.what());
  }
  return shared_ptr<LocalSimpleClassifier>();
}

/**
 * - 일정 시간 이후 접속이 안 되면 서비스를 종료한다.
 */
bool ItfHazelcastClient::IsReady() {
  const int waits[] = {20, 40, 60, 0};
  auto logger = LOGGER();
  int wait_seconds = 0;

  logger->debug("Check Hazelcast Running...");
  for (int i = 0; i < 4; ++i) {
    auto client = GetInstance()->GetHazelcastClient();
    if (client) {
      return true;
    } else {
      wait_seconds += waits[i];
      logger->debug(" Wait more to {}s.", wait_seconds);
    }
    std::this_thread::sleep_for(std::chrono::seconds(waits[i] - 7));
  }

  return false;
}
