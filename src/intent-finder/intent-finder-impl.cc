#include <libmaum/common/config.h>
#include "hazelcast.h"
#include "intent-finder-impl.h"
#include "tls-value.h"
#include <libmaum/log/call-log.h>
#include <libmaum/rpc/result-status.h>
#include <libmaum/brain/error/ta-error.h>
#include "local-intentfinder-policy.h"

using ::grpc::StatusCode;
using libmaum::log::CallLogInvoker;
using libmaum::rpc::CommonResultStatus;

using maum::m2u::router::v3::IntentFindingHistory;
using maum::m2u::router::v3::IntentFindingFlowControl;
using maum::m2u::router::v3::IntentFindingFlowControl_Name;
using RunResult = maum::m2u::router::v3::IntentFindingHistory_RunResult;
using maum::m2u::router::v3::Replacement;
using maum::m2u::router::v3::ClassifyingMethod_Name;

using hzc = ItfHazelcastClient;

// error_index: 00000 ~ 09999
#define ERROR_INDEX_MAJOR 00000

IntentFinderImpl::~IntentFinderImpl() {
  auto logger = LOGGER();
  sc_handler_.RemoveListener();
  logger_info("[SimpleClassifier Listener Remove] ");
}

bool IntentFinderImpl::CheckOperationSyncId(const ServerContext *context) {
  const auto &client_map = context->client_metadata();
  auto item_s = client_map.find("x-operation-sync-id");
  if (item_s != client_map.end()) {
    string xid = string(item_s->second.begin(), item_s->second.end());
    SetTlsOpSyncId(xid);
    LOGGER_info(" operation from {}", context->peer());
    return true;
  } else {
    return false;
  }
}

const Status IntentFinderImpl::SyncIdMissing(const ServerContext *context) {
  Status status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                                   ERROR_INDEX(1100),
                                   "[{}] {}",
                                   context->peer().c_str(),
                                   "x-operation-sync-id is Missing");
  LOGGER()->error(CommonResultStatus::GetErrorString(status));
  return status;
}


/**
 * 의도파악  정책의 단계 중 run_style 규칙에 따라 skip 여부 반환
 *
 * @param step  의도파악 정책의 단계
 * @param skill 분류 skill 결과
 * @return is_next skip 여부
 */
bool IntentFinderImpl::CheckStepSkip(const IntentFindingStep &step,
                                     const string &skill) {
  bool is_next = false;
  auto logger = LOGGER();
  const string &step_name = step.name();
  const auto &run_type = step.run_style();
  const string &run_style = IntentFindingFlowControl_Name(run_type);
  switch (run_type) {
    case IntentFindingFlowControl::IFC_SKILL_FOUND_RETURN:
    case IntentFindingFlowControl::IFC_SKILL_FOUND_COLLECT_HINTS:
      if (!skill.empty()) {
        is_next = true;
        logger_debug("skip a step: {}, runstyle: {}", step_name, run_style);
      }
      break;
    case IntentFindingFlowControl::IFC_UNKOWN: {
      logger_debug(
          "skip a step: {}, runstyle: {}", step_name, run_style);
      is_next = true;
      break;
    }
    case IntentFindingFlowControl::IFC_HINT:
      break;
    default:
      break;
  }
  return is_next;
}

/**
 * intent finder 초기화 함수.
 *
 * @param name IntentFinder 이름
 * @return 초기화 완료여부
 */
bool IntentFinderImpl::Init(const string &name) {
  auto logger = LOGGER();
  logger_debug("<<< [IntentFinderImpl Init] Start");
  auto &c = libmaum::Config::Instance();
  policy_name_ = "";
  name_ = name;
  itf_ip_ = c.Get("itf.ip", false);

  logger_debug("[IntentFinderImpl Init] End");
  return true;
}

/**
 * hzc intent finder 불러와서 정책 정보 로드.
 *
 * @return 로드 완료여부
 */
bool IntentFinderImpl::Load() {
  bool is_done = false;
  auto logger = LOGGER();
  logger_debug(">>> [IntentFinderImpl Load] Start");
  try {
    auto instance = hzc::GetInstance()->GetIntentFinderInstance(name_);
    const string &policy_name = instance->policy_name();
    policy_name_ = policy_name;
    is_done = sc_handler_.GetClassifyingHandler()->Init(policy_name);

    sc_handler_.SetPolicyName(policy_name);
    sc_handler_.AddListener();
    logger_debug("[SimpleClassifier Listener Start] policy name: {}",
                 policy_name);
    logger_debug("<<< [IntentFinderImpl Load] End");

    // 로딩이 완료된 정책은 캐시한다.
    itf_policy_ = hzc::GetInstance()->GetIntentFinderPolicy(policy_name);
  } catch (ta::Exception &te) {
    throw;
  } catch (IException &ie) {
    logger_error("[IntentFinderImpl Load] End Fail: {}", ie.what());
    throw ta::Exception("HZC", ie.what());
  }
  return is_done;
}

/**
 * 의도파악 기능 수행 함수
 * hzc 에서 policy 정보 가져와 분류를 수행한다.
 * 이때 run_style 에 따라 분류를 skip 할지 수행할지 결정한다.
 *
 * @param context
 * @param request  FindIntentRequest  의도파악 수행 위한 기본 정보
 * @param response FindIntentResponse 의도파악 수행 후 응답 정보
 * @return Status
 */
Status IntentFinderImpl::FindIntent(ServerContext *context,
                                    const FindIntentRequest *org_request,
                                    FindIntentResponse *response) {
  if (!CheckOperationSyncId(context)) {
    return SyncIdMissing(context);
  }

  Status status = SubFindIntent(org_request, response, nullptr, false);
  if (status.ok()) {
    CallLogInvoker log(__func__, context, org_request, response, &Status::OK);
    LOGGER_trace(" >>> ITF FindIntent End..\n response={}",
                 response->Utf8DebugString());
  }
  return status;
}

/**
 * 의도파악 기능 다시 수행 함수
 * hzc 에서 policy 정보 가져와 분류를 수행한다.
 * 이때 run_style 에 따라 분류를 skip 할지 수행할지 결정한다.
 *
 * FindInetent와 다른 점은 다음과 같다.
 * - 해당 스텝이 `skip_steps`에 포함되면 넘어간다.
 * - 찾은 skill이 `exclude_skills`에 포함되면, 버린다.
 *   단, default 스킬은 제외 목록에서 제외한다.
 *
 * @param context 서버 컨텍스트
 * @param request  FindIntentRequest  의도파악 수행 위한 기본 정보
 * @param response RetryFindIntentResponse 의도파악 수행 후 응답 정보
 * @return Status
 */
Status IntentFinderImpl::RetryFindIntent(ServerContext *context,
                                         const FindIntentRequest *org_request,
                                         RetryFindIntentResponse *response) {
  if (!CheckOperationSyncId(context)) {
    return SyncIdMissing(context);
  }

  Status status = SubFindIntent(org_request, nullptr, response, true);
  if (status.ok()) {
    CallLogInvoker log(__func__, context, org_request, response, &Status::OK);
    LOGGER_trace(" >>> ITF RetryFindIntent End..\n response={}",
                 response->Utf8DebugString());
  }
  return status;
}

/**
 * utter 에 대해 NLU ANALYZE한 결과를 반환한다.
 * @param context
 * @param org_request utter를 갖는 FindIntentRequest
 * @param response analyze 결과를 nlu_result에 채운 FindtIntentResponse
 * @return Status
 */
Status IntentFinderImpl::Analyze(ServerContext *context,
                                 const FindIntentRequest *request,
                                 FindIntentResponse *response) {
  auto utter = request->utter().utter();
  auto logger = LOGGER();
  Status status;

  try {
    unique_ptr<Document> document(new Document());
    logger_debug(">>> [Analyze] utter : {}", utter);
    sc_handler_.GetClassifyingHandler()->Prepare(request, document.get());
    response->mutable_skill()->mutable_nlu_result()->MergeFrom(*document);
    logger_trace(">>> [Analyze] nlu_result: [{}]",
                 response->skill().nlu_result().Utf8DebugString());
  } catch (IException &e) {
    logger_error("{}:Cannot NLU Analyze, cuase: {}",
                 __func__,
                 e.what());
    Status status = ResultGrpcStatus(ExCode::RUNTIME_ERROR,
                                     ERROR_INDEX(3002),
                                     "Cannot Analyze utter:%s, ITF policy:%s, instance:%s",
                                     utter.c_str(),
                                     name_.c_str(),
                                     policy_name_.c_str());
  }

  return status;
}

void IntentFinderImpl::FillRecord(ClassificationRecord &record,
                                  const FindIntentRequest *request,
                                  const Replacement &replace) {
  timespec now = {0, 0};

  clock_gettime(CLOCK_REALTIME, &now);
  record.mutable_classified_at()->set_seconds(now.tv_sec);
  record.mutable_classified_at()->set_nanos(int32_t(now.tv_nsec));

  record.set_session_id(tls_session_id_i64);
  record.set_operation_sync_id(tls_op_sync_id);
  record.set_policy_name(policy_name_);
  record.set_chatbot(request->chatbot());
  record.set_utter(request->utter().utter());
  record.mutable_replace()->CopyFrom(replace);
}

Status IntentFinderImpl::SubFindIntent(const FindIntentRequest *org_request,
                                       FindIntentResponse *response,
                                       RetryFindIntentResponse *retry_response,
                                       bool retry_option) {
  auto logger = LOGGER();
  auto session_id = org_request->session().id();
  SetTlsSessionId(session_id);

  string func(retry_option ? "RetryFindIntent" : "FindIntent");

  logger_debug(">>> ITF {} Start..", func);
  logger_trace("request = {}", org_request->Utf8DebugString());

  // 1. utter에 대한 nlu 처리한다.
  unique_ptr<Document> document(new Document());
  sc_handler_.GetClassifyingHandler()->Prepare(org_request, document.get());

  // 2. policy 를 기준으로 분류기를 돌린다.
  {
    Replacement *rep;
    FoundIntent *found_intent;
    if (retry_option) {
      rep = retry_response->mutable_replace();
      found_intent = retry_response->mutable_skill();
    } else {
      rep = response->mutable_replace();
      found_intent = response->mutable_skill();
    }
    IntentFindingHistory *history = found_intent->mutable_history();
    auto hints = found_intent->mutable_hints();
    found_intent->mutable_nlu_result()->MergeFrom(*document);

    // 복제본을 유지한다.
    FindIntentRequest copy_req;
    copy_req.CopyFrom(*org_request);

    // RetryFindIntent 경우 EXCLUDE에서 DEFAULT 스킬은 제외한다.
    if (retry_option) {
      auto mutable_ex = copy_req.mutable_exclude_skills();
      for (auto it = mutable_ex->begin(); it != mutable_ex->end();) {
        if (*it == itf_policy_->default_skill()) {
          logger_debug("RETRY: ignore exclude skill {} is default skill {}",
                       *it, itf_policy_->default_skill());
          it = mutable_ex->erase(it);
        } else
          it++;
      }
    }

    const string &usr_utter = copy_req.utter().utter();

    // policy의 step 별로 분류 시작
    for (auto const &step : itf_policy_->steps()) {
      //  해당 step 을 건너뛸지 체크.
      bool is_skip = CheckStepSkip(step, found_intent->skill());
      const string &step_name = step.name();
      const string &step_type = ClassifyingMethod_Name(step.type());
      const auto &run_type = step.run_style();
      const string &run_style = IntentFindingFlowControl_Name(run_type);
      if (is_skip)
        continue;

      // RetryFindIntent의 경우 건너띌 STEPS인지 확인
      if (retry_option) {
        for (auto const &skip_step : copy_req.skip_steps()) {
          if (skip_step == step_name) {
            logger_debug(">RETRY, SKIP skip steps:: this step:{} type:{}",
                         step_name, run_type);
            continue;
          }
        }
      }

      logger_debug(">{}, policy:{}, step:{}, type:{}, runstyle:{}, utter:{} "
                   "Classify",
                   func,
                   policy_name_,
                   step_name,
                   step_type,
                   run_style,
                   usr_utter);

      RunResult *run_result = history->add_run_results();
      Replacement tmp_rep;
      sc_handler_.GetClassifyingHandler()->Classify(&step,
                                                    &copy_req,
                                                    run_result,
                                                    &tmp_rep,
                                                    *document);
      // utter의 변경이 발생하면, 원문을 변경한다.
      if (!tmp_rep.replaced_utter().empty() &&
          !tmp_rep.replacements().empty()) {
        rep->set_replaced_utter(tmp_rep.replaced_utter());
        // 원문을 변경한다.
        copy_req.mutable_utter()->set_utter(rep->replaced_utter());
        logger_debug("{}, req utter is replace for next step = {}",
                     func, rep->replaced_utter());
        rep->mutable_replacements()->insert(tmp_rep.replacements().begin(),
                                            tmp_rep.replacements().end());
        const FindIntentRequest changed_req = copy_req;
        // 변경된 utter 정보로 다시 nlu 처리한다.
        document->clear_sentences();
        sc_handler_.GetClassifyingHandler()->Prepare(&changed_req,
                                                     document.get());
      }

      if (!run_result->has_result())
        continue;
      const string &hint = run_result->hint();
      const string &skill = run_result->skill();
      using kv_pair = google::protobuf::Map<string, string>::value_type;
      switch (run_type) {
        case IntentFindingFlowControl::IFC_SKILL_FOUND_RETURN:
        case IntentFindingFlowControl::IFC_SKILL_FOUND_COLLECT_HINTS: {
          if (retry_option) {
            bool excluded = false;
            for (auto const &exclude_skill : copy_req.exclude_skills()) {
              if (exclude_skill == skill) {
                // 찾은 스킬은 지우고, DETAIL에 excluded 정보를 추가한다.
                run_result->clear_skill();
                run_result->set_detail("skill '" + skill + "' excluded.");
                run_result->set_has_result(false);
                excluded = true;
                break;
              }
            }
            if (excluded) {
              logger_debug("RETRY, found skill {} :: EXCLUDED {}/{}/{}",
                           skill, step_name, step_type, run_style);
              break;
            }
          }
          found_intent->set_skill(skill);
          // 분류가 SKILL 일 경우에 그 확율을 기록해둔다. #4189
          found_intent->set_probability(run_result->probability());
          break;
        }
        case IntentFindingFlowControl::IFC_HINT: {
          logger_debug("{}, insert hints {}, {} {}",
                       func,
                       step.name(),
                       run_result->name(),
                       hint);
          hints->insert(kv_pair(step.name(), hint));
          break;
        }
        default: {
          break;
        }
      }

      // result 가 있고 run_style 이 return 이면 중지한다.
      // 그외 경우는 계속 진행한다.
      if (!found_intent->skill().empty() &&
          (run_type == IntentFindingFlowControl::IFC_SKILL_FOUND_RETURN)) {
        logger_debug("Stop finding intent, policy:{}, step:{}, type:{}, "
                     "runstyle:{}, utter:{} found skill: {}.",
                     policy_name_,
                     step_name,
                     step_type,
                     run_style,
                     usr_utter,
                     skill);
        break;
      }
      logger_debug("Classify Result policy:{}, step:{}, type:{}, runstyle:{},"
                   " utter:{}, skill:{}, hint:{}",
                   policy_name_,
                   step_name,
                   step_type,
                   run_style,
                   usr_utter,
                   skill,
                   hint);
    }

    // skill 값이 비었을 경우 default skill로 설정
    if (found_intent->skill().empty()) {
      found_intent->set_skill(itf_policy_->default_skill());
    }
    found_intent->set_is_default_skill(itf_policy_->default_skill()
                                           == found_intent->skill());
    logger_debug("SubFindIntent success. policy:{}, utter:{}, skill:{}",
                 policy_name_, usr_utter, found_intent->skill());
  }
  // session_id 존재할 경우만 의도파악한 결과를 Map에 저장
  if (session_id != 0) {
    ClassificationRecord record;
    if (retry_option) {
      FillRecord(record, org_request, retry_response->replace());
      record.mutable_skill()->CopyFrom(retry_response->skill());
    } else {
      FillRecord(record, org_request, response->replace());
      if (response->has_skill()) {
        record.mutable_skill()->CopyFrom(response->skill());
      } else {
        record.mutable_transit()->CopyFrom(response->transit());
      }
    }
    try {
      hzc::GetInstance()->GetHazelcastClient()
          ->getMap<string, ClassificationRecord>(hzc::kNsClassificationRecord)
          .set(record.operation_sync_id(), record);
    } catch (IException &e) {
      logger_error("{}: Cannot Store ITF result to Map, cause: {}",
                   func,
                   e.what());
      return Status::OK;
    }
  }
  return Status::OK;
}
