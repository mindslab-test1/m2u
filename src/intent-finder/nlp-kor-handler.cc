#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <libmaum/brain/nlp/nlp3kor.h>
#include "nlp-kor-handler.h"
#include "tls-value.h"
#include <thread>
#include <libmaum/common/config.h>
#include <libmaum/brain/error/ta-error.h>

using std::unique_ptr;
using grpc::StatusCode;

const int kIntentFinderFeature = (1 << NlpFeature::NLPF_SENTENCE_RECOGNITION) |
    (1 << NlpFeature::NLPF_MORPHEME) |
    (1 << NlpFeature::NLPF_NAMED_ENTITY);


/**
 * nlp 처리를 위한 리소스 로딩
 *
 * @return is_done 리소스 로딩 성공여부
 */
bool NlpKoreanHandler::Init() {
  bool is_done = false;
  auto logger = LOGGER();
  logger_debug(">>> [Initialize NlpKoreanHandler] Start");
  auto &c = libmaum::Config::Instance();
  rsc_path_ = c.Get("brain-ta.resource.3.kor.path");
  nlp3Kor_ = new Nlp3Kor(rsc_path_, kIntentFinderFeature);
  nlp3Kor_->UpdateFeatures();
  nlp3Kor_->LoadSpaceDict();

  is_started_ = true;
  logger_debug("<<< [Initialize NlpKoreanHandler] End");
  is_done = true;
  return is_done;
}

/**
 * nlp 리소스 내리는 함수
 *
 * @return 리소스 내리기 성공여부
 */
bool NlpKoreanHandler::Stop() {
  bool result = false;
  auto logger = LOGGER();
  logger_debug("NlpKoreanHandler Stop");
  nlp3Kor_->Uninit();
  result = true;
  return result;
}

/**
 * 언어분석함수
 *
 * @param text 입력텍스트
 * @param document 언어분석결과 Document
 * @return is_done 언어분석 성공여부
 */
bool NlpKoreanHandler::Analyze(const InputText *text,
                               Document *document) {
  if (!is_started_) return false;

  std::lock_guard<std::mutex> guard(lock_);
  nlp3Kor_->AnalyzeOne(text, document);

  return true;
}

/**
*  띄어쓰기에 언어분석 결과가 달라지는 부분을 사전에 치환하여 분석하는 기능을 제공하는 함수
* @return is_done 태깅 성공여부
*/
bool NlpKoreanHandler::SpaceAndAnalyze(const InputText *text,
                                       Document *document) {
  if (!is_started_) return false;

  std::lock_guard<std::mutex> guard(lock_);
  nlp3Kor_->SpaceAndAnalyzeOne(text, document);

  return true;
}

/**
 * 형태소 태그값을 저장하는 함수
 *
 * @param document 언어분석결과 Document
 * @param pos_tag 형태소 태그값
 */
void NlpKoreanHandler::GetPosTaggedStr(const Document *document,
                                       string &pos_tag) {
  vector<string> pos_tagged_str;
  Nlp3Kor::GetPosTaggedString(document, &pos_tagged_str);
  pos_tag = "";
  for (string tagged: pos_tagged_str) pos_tag += tagged;
}

/**
 *  개체명 태그값을 저장하는 함수
 *
 * @param document 언어분석결과 Document
 * @param ner_tag 개체명 태그값
 */
void NlpKoreanHandler::GetNerTaggedStr(const Document *document,
                                       string &ner_tag) {
  vector<string> ner_tagged_str;
  Nlp3Kor::GetNerTaggedString(document, &ner_tagged_str);
  ner_tag = "";
  for (string tagged: ner_tagged_str) ner_tag += tagged;
}

/**
 *  개체명 태그값과 Pos 태그값을 Document에서 가져오는 함수
 *
 * @param document 언어분석결과 Document
 * @param pos_ner_tag 결과값
 */
void NlpKoreanHandler::GetPosNerTaggedStr(const Document *document,
                                          string &pos_ner_tag) {
  vector<string> pos_ner_tagged_str;
  Nlp3Kor::GetPosNerTaggedString(document, &pos_ner_tagged_str);
  pos_ner_tag = "";
  for (string tagged: pos_ner_tagged_str) pos_ner_tag += tagged;
}
