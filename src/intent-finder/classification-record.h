#ifndef CLASSIFICATION_RECORD_H
#define CLASSIFICATION_RECORD_H

#include <maum/m2u/router/v3/intentfinder.pb.h>

#include "hazelcast.h"

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

/**
 *
 */
class ClassificationRecord
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::router::v3::ClassificationRecord {
 public:
  ClassificationRecord() = default;
  ClassificationRecord(const ClassificationRecord &rhs) {
    this->CopyFrom(rhs);
  }
  virtual ~ClassificationRecord() = default;

  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  }

  int getClassId() const override {
    return CLASSIFICATION_RECORD;
  };

  /**
   *
   * @param out
   */
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeByteArray("_msg", &data);
    out.writeLong("session_id", this->session_id());
    out.writeUTF("operation_sync_id", &this->operation_sync_id());
    out.writeUTF("policy_name", &this->policy_name());
    out.writeUTF("chatbot", &this->chatbot());
  }

  /**
   *
   * @param in
   */
  void readPortable(PortableReader &in) override {
//    in.readUTF("name");
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  }
};

#endif // CLASSIFICATION_RECORD_H
