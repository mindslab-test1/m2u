#ifndef ITF_CLASSIFYING_HANDLER_H
#define ITF_CLASSIFYING_HANDLER_H

#include <string>
#include <set>
#include <maum/m2u/router/v3/intentfinder.grpc.pb.h>
#include "sc-pcre.h"
#include "nlp-kor-handler.h"
#include "nlp-eng-handler.h"
#include "custom-script-client.h"
#include "hmd-handler.h"
#include "dnn-handler.h"
#include "xdc-client.h"

using std::string;
using std::shared_ptr;

using maum::brain::nlp::NlpAnalysisLevel;
using maum::brain::nlp::Document;
using maum::brain::nlp::InputText;
using maum::brain::nlp::NlpFeature;
using maum::brain::cl::ClassifiedSummary;
using maum::m2u::router::v3::IntentFinderPolicy;
using maum::m2u::router::v3::IntentFindingStep;
using maum::m2u::router::v3::Replacement;
using maum::m2u::router::v3::FindIntentRequest;
using maum::m2u::router::v3::RetryFindIntentResponse;
using maum::m2u::router::v3::IntentFinderInstance;
using maum::brain::han::run::HanInputDocument;
using maum::brain::han::run::HanInputCommon;
using maum::brain::han::run::HanResult;

using RunResult = maum::m2u::router::v3::IntentFindingHistory_RunResult;
using ScPcreMap = std::map<string, atomic<shared_ptr<SimpleClassifierPcre> *> *>;

class ClassifyingHandler {
 public:
  ClassifyingHandler() = default;
  virtual ~ClassifyingHandler() = default;

  bool Init(const string &policy_name);
  bool Prepare(const FindIntentRequest *req,
               Document *document);
  bool Classify(const IntentFindingStep *step,
                const FindIntentRequest *request,
                RunResult *result,
                Replacement *rep,
                const Document &document);
  bool UpdatePcre(const string &sc_name, const string &policy_name);
  bool DeletePcre(const string &sc_name, const string &policy_name);

 private:
  bool ClassifyCustomScript(const IntentFindingStep *step,
                            const FindIntentRequest *request,
                            RunResult *result,
                            Replacement *rep,
                            const Document &document);
  bool ClassifyComposerScript(const IntentFindingStep *step,
                              const FindIntentRequest *request,
                              RunResult *result,
                              const Document &document);
  bool ClassifyPcre(const IntentFindingStep *step,
                    const FindIntentRequest *request,
                    RunResult *result,
                    const Document &document);
  bool ClassifyHmd(const IntentFindingStep *step,
                   const FindIntentRequest *request,
                   RunResult *result,
                   const Document &document);
  bool ClassifyDnn(const IntentFindingStep *step,
                   const FindIntentRequest *request,
                   RunResult *result,
                   const Document &document);
  bool ClassifyXdc(const IntentFindingStep *step,
                   const FindIntentRequest *request,
                   RunResult *result,
                   const Document &document);
  bool ClassifyFinalRule(const IntentFindingStep *step,
                         const FindIntentRequest *request,
                         RunResult *result,
                         const Document &document);
  bool ParsingScNlp(const string &sc_name,
                    shared_ptr<SimpleClassifierPcre> &pcre);

  ScPcreMap::iterator FindPcre(const string &sc_name,
                               const string &policy_name);

  string policy_name_;

  ScPcreMap sc_pcre_;

  shared_ptr<NlpKoreanHandler> nlp_kor_;
  shared_ptr<NlpEnglishHandler> nlp_eng_;
  shared_ptr<CustomScriptClient> custom_script_client_;
  shared_ptr<XdcClient> xdc_client_;
  shared_ptr<HmdHandler> hmd_;
  shared_ptr<DnnHandler> dnn_;
};

#endif //ITF_CLASSIFYING_HANDLER_H
