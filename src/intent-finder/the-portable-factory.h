#ifndef M2U_THE_PORTABLE_FACTORY_H
#define M2U_THE_PORTABLE_FACTORY_H

#include "tls-value.h"
#include "local-simpleclassifier.h"
#include "local-intentfinder.h"
#include "local-intentfinder-policy.h"
#include "classification-record.h"

using std::auto_ptr;
using std::shared_ptr;
using hazelcast::client::serialization::PortableFactory;
using hazelcast::client::serialization::Portable;

class ThePortableFactory : public PortableFactory {
 public:
  auto_ptr<Portable> create(int32_t classId) const override {
    switch (classId) {
      case SIMPLE_CLASSIFIER: {
        return auto_ptr<Portable>(new LocalSimpleClassifier());
      }
      case INTENT_FINDER_INSTANCE: {
        return auto_ptr<Portable>(new LocalIntentFinderInstance());
      }
      case INTENT_FINDER_POLICY: {
        return auto_ptr<Portable>(new LocalIntentFinderPolicy());
      }
      case CLASSIFICATION_RECORD: {
        return auto_ptr<Portable>(new ClassificationRecord());
      }
      default:return auto_ptr<Portable>();
    }
  }
};

#endif //M2U_THE_PORTABLE_FACTORY_H
