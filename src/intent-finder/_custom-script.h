#ifndef PROJECT_CUSTOM_SCRIPT_H
#define PROJECT_CUSTOM_SCRIPT_H

#include <string>
#include <mutex>
#include <atomic>
#include <maum/m2u/router/v3/intentfinder.grpc.pb.h>
#include <Python.h>

using std::string;
using std::atomic;
using std::shared_ptr;
using ClassifyScriptRule = maum::m2u::router::v3::IntentFindingStep_ClassifyScriptRule;
using maum::m2u::router::v3::CustomClassifyParam;
using maum::m2u::router::v3::CustomClassifyResult;
using maum::m2u::router::v3::CustomGetCategoriesResult;
using maum::m2u::router::v3::CustomGetCategoriesResult;
using maum::m2u::router::v3::CustomComposeHintsParam;
using maum::m2u::router::v3::CustomGetComposeTableResult;
using RunResult = maum::m2u::router::v3::IntentFindingHistory_RunResult;

class CustomScriptManager {
 public:
  CustomScriptManager();
  ~CustomScriptManager();

 public:
  bool Init();
  bool Stop();

  bool AppendPath(string path);

  bool Classify(const ClassifyScriptRule *rule,
                CustomClassifyParam *param,
                CustomClassifyResult &result);
  bool GetCategories(ClassifyScriptRule *rule,
                     CustomGetCategoriesResult *result);
  bool ComposeHints(ClassifyScriptRule *rule,
                    CustomComposeHintsParam *param,
                    RunResult *result);
  bool GetComposeTable(ClassifyScriptRule *rule,
                       CustomGetComposeTableResult *result);

  //protected:
  bool CallPythonFunc(const char *name,
                      const char *func,
                      string &param,
                      string &result);

 private:
  PyThreadState *mainThreadState_;
  PyGILState_STATE gilState_;

};

#endif //PROJECT_CUSTOM_SCRIPT_H
