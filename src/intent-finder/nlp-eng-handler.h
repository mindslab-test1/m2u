#ifndef PROJECT_NLP_ENG_HANDLER_H
#define PROJECT_NLP_ENG_HANDLER_H

#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>

#include <maum/brain/nlp/nlp.grpc.pb.h>
#include <libmaum/brain/nlp/nlp2eng.h>

/**
 *
 */
class NlpEnglishHandler {
 public:
  NlpEnglishHandler() { is_started = false; };
  virtual ~NlpEnglishHandler() {};

  bool Init();
  bool Stop();
  bool Analyze(const InputText *text, Document *document);
  bool GetPosTaggedStr(const Document *document, string &pos_tag);
  bool GetNerTaggedStr(const Document *document, string &ner_tag);
  bool GetPosNerTaggedStr(const Document *document, string &pos_ner_tag);

 private:

  bool is_started;
  //NLP Model
  Nlp2Eng *nlp2Eng_;
  // nlp-eng2 리소스 경로
  std::string rsc_path_;

};

#endif //PROJECT_NLP_ENG_HANDLER_H
