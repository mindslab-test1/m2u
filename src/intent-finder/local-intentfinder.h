#ifndef LOCAL_INTENT_FINDER_H
#define LOCAL_INTENT_FINDER_H

#include <maum/m2u/router/v3/intentfinder.pb.h>

#include "hazelcast.h"

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

/**
 *
 */
class LocalIntentFinderInstance
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::router::v3::IntentFinderInstance {
 public:
  LocalIntentFinderInstance() {}
  LocalIntentFinderInstance(const LocalIntentFinderInstance &rhs) {
    this->CopyFrom(rhs);
  }
  virtual ~LocalIntentFinderInstance() {}

  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  }

  int getClassId() const override {
    return INTENT_FINDER_INSTANCE;
  }

  /**
   *
   * @param out
   */
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    std::string empty;
    out.writeUTF("name", &this->name());
    out.writeByteArray("_msg", &data);
    out.writeUTF("ip", &this->ip());
    out.writeInt("port", this->port());
    out.writeUTF("policy_name", &this->policy_name());

  }

  /**
   *
   * @param in
   */
  void readPortable(PortableReader &in) override {
//    in.readUTF("name");
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  }
};

#endif
