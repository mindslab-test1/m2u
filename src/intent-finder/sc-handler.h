#ifndef DIALOG_AGENT_HANDLER_H
#define DIALOG_AGENT_HANDLER_H

#include <string>
#include <atomic>
#include <unordered_map>
#include <json/json.h>
#include <grpc++/grpc++.h>
#include <maum/m2u/server/dam.grpc.pb.h>
#include <maum/m2u/console/da.grpc.pb.h>

#include "classifying-handler.h"
#include "hazelcast.h"
#include "local-intentfinder.h"

using ScInfo = LocalSimpleClassifier;
using ScInfoEvent = EntryEvent<string, ScInfo>;

/**
 * Dialog Agent 등록/삭제, 실행 정지를 관리한다.
 * Hazelcast kNsDAActivation("dialog-agent-activation") Map을 listene한다.
 */
class SimpleClassifierHandler : protected EntryListener<string, ScInfo> {
 public:
  SimpleClassifierHandler() = default;
  virtual ~SimpleClassifierHandler() override;

  void AddListener();
  void RemoveListener();
  void SetPolicyName(const string &name);
  ClassifyingHandler* GetClassifyingHandler();

 protected:
  /** Invoked when an entry is added. */
  void entryAdded(const ScInfoEvent &event) override;
  /** Invoked when an entry is removed. */
  void entryRemoved(const ScInfoEvent &event) override;
  /** Invoked when an entry is updated. */
  void entryUpdated(const ScInfoEvent &event) override;
  /** Invoked when an entry is evicted. */
  void entryEvicted(const ScInfoEvent &event) override;
  /** Invoked upon expiration of an entry. */
  void entryExpired(const ScInfoEvent &event) override;
  /** Invoked after WAN replicated entry is merged. */
  void entryMerged(const ScInfoEvent &event) override;
  /** Invoked when all entries evicted by {@link IMap#evictAll()}. */
  void mapEvicted(const MapEvent &event) override;
  /** Invoked when all entries are removed by {@link IMap#clear()}.} */
  void mapCleared(const MapEvent &event) override;

  string registration_id_;

  const char *getMapName() { return ItfHazelcastClient::kNsSC; };

 private:
  string policy_name_;
  ClassifyingHandler classifying_handler_;
};

#endif // DIALOG_AGENT_HANDLER_H
