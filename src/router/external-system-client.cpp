#include <ostream>
#include <libmaum/common/util.h>
#include <libmaum/common/config.h>

#include "util.h"
#include "tls-values.h"
#include "external-system-client.h"
#include "talk-impl.h"

using std::ostringstream;
using grpc::StatusCode;
using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using google::protobuf::Empty;
using maum::m2u::router::ext::v1::OpenExternalRequest;
using maum::m2u::router::ext::v1::OpenExternalResponse;
using maum::m2u::router::ext::v1::TalkExternalRequest;
using maum::m2u::router::ext::v1::TalkExternalResponse;
using maum::m2u::router::ext::v1::AnswerExternalRequest;
using maum::m2u::router::ext::v1::AnswerExternalResponse;

/**
 * 외부시스템 Client 생성자
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 */
ExternalSystemClient::ExternalSystemClient(const string &external_system_info) {
  auto pos = external_system_info.find('|');
  endpoint_ = external_system_info.substr(0, pos);
  session_timeout_ = stoi(external_system_info.substr(++pos));
}

/**
 * 외부시스템에 대화 Open을 전달한다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param talk: 대화 세션의 talk 객체
 * @param context: 서버 gRPC 호출의 context
 * @param daRequest: DA에 호출된 원래 request
 * @param daResponse: DA의 원래 response
 */
void ExternalSystemClient::OpenExternal(TalkImpl *talk,
                                        const ServerContext *context,
                                        const OpenSessionRequest &daRequest,
                                        TalkResponse &daResponse) {
  if (IsUnavailable()) {
    LOGGER_trace("OpenExternal External System is Unavailable.");
    return;
  }

  auto stub = ExternalSystemClient::GetStub();

  if (!stub) {
    return;
  }

  OpenExternalRequest request;
  OpenExternalResponse response;

  request.mutable_utter()->MergeFrom(daRequest.utter());
  if (daResponse.has_response() && daResponse.response().has_speech()) {
    request.mutable_answer()->MergeFrom(daResponse.response().speech());
  }
  request.set_chatbot(daRequest.chatbot());
  request.mutable_session()->MergeFrom(daRequest.session());

  auto clientContext = GetClientContext(context, 3000);
  auto reader(stub->OpenExternal(clientContext.get(), request));

  ostringstream utter;
  while (reader->Read(&response)) {
    if (response.has_intervened_answer()) {
      LOGGER_trace("intervened_answer: {}", response
          .intervened_answer()
          .utter());
      utter << response.intervened_answer().utter();
    }
  }

  Status status = reader->Finish();
  // Response 처리
  if (status.ok()) {
    if (response.has_intervened_answer()) {
      response.mutable_intervened_answer()->set_utter(utter.str());
    }
    LOGGER_trace("OpenExternal response:\n{}", response.Utf8DebugString());
    auto alter_response = daResponse.mutable_response();
    if (response.has_intervened_answer()) {
      alter_response->mutable_speech()->MergeFrom(response.intervened_answer());
      SetBoolMeta(alter_response->mutable_meta(), "m2u.intervenedAnswer", true);
    } else {
      SetBoolMeta(alter_response->mutable_meta(),
                  "m2u.intervenedAnswer",
                  false);
    }
    if (!response.directives().empty()) {
      alter_response->mutable_directives()->MergeFrom(response.directives());
    }
    if (!response.session_update().context().fields().empty()) {
      talk->UpdateSessionContext(response.session_update());
    }
    if (!response.meta().fields().empty()) {
      alter_response->mutable_meta()->MergeFrom(response.meta());
    }
    grpc_call_timeout_ = response.requested_timeout();
    LOGGER_debug("OpenExternal Set call-timeout to {}ms.", grpc_call_timeout_);
  } else {
    ProcessError(__func__, status, talk);
  }
}

/**
 * 외부시스템에 대화 발화을 전달한다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param talk: 대화 세션의 talk 객체
 * @param context: 서버 gRPC 호출의 context
 * @param daRequest: DA에 호출된 원래 request
 * @param daResponse: DA의 원래 response
 *
 * @return bool : 대화를 M2U로 대화를 처리할 지 리턴하다.
 *                   true: 처리하지 않고 그대로 답변을 회신한다.
 *                   false: M2U로 대화를 처리한다.
 */
bool ExternalSystemClient::TalkExternal(TalkImpl *talk,
                                        const ServerContext *context,
                                        TalkRequest &daRequest,
                                        TalkResponse &daResponse) {
  if (IsUnavailable()) {
    LOGGER_trace("TalkExternal External System is Unavailable.");
    return false;
  }

  auto stub = ExternalSystemClient::GetStub();

  if (!stub) {
    return false;
  }

  bool result = false;
  TalkExternalRequest request;
  TalkExternalResponse response;

  request.mutable_utter()->MergeFrom(daRequest.utter());
  request.mutable_session()->MergeFrom(daRequest.session());

  auto clientContext = GetClientContext(context, grpc_call_timeout_);
  auto reader(stub->TalkExternal(clientContext.get(), request));

  ostringstream utter;
  while (reader->Read(&response)) {
    if (response.has_altered_utter()) {
      LOGGER_trace("TalkExternal altered_utter: {}",
                   response.altered_utter().utter());
      utter << response.altered_utter().utter();
    } else if (response.has_intervened_answer()) {
      LOGGER_trace("TalkExternal intervened_answer: {}", response
          .intervened_answer()
          .utter());
      utter << response.intervened_answer().utter();
    }
  }

  Status status = reader->Finish();
  // Response 처리
  if (status.ok()) {
    if (response.has_altered_utter()) {
      response.mutable_altered_utter()->set_utter(utter.str());
    } else if (response.has_intervened_answer()) {
      response.mutable_intervened_answer()->set_utter(utter.str());
    }
    LOGGER_trace("TalkExternal response:\n{}", response.Utf8DebugString());
    auto alter_response = daResponse.mutable_response();
    if (response.has_altered_utter()) {
      daRequest.mutable_utter()->MergeFrom(response.altered_utter());
    } else if (response.has_intervened_answer()) {
      alter_response->mutable_speech()->MergeFrom(response.intervened_answer());
      result = true;
    }
    if (!response.directives().empty()) {
      alter_response->mutable_directives()->MergeFrom(response.directives());
    }
    if (!response.session_update().context().fields().empty()) {
      talk->UpdateSessionContext(response.session_update());
    }
    SetBoolMeta(alter_response->mutable_meta(), "m2u.intervenedAnswer", result);
    if (!response.meta().fields().empty()) {
      alter_response->mutable_meta()->MergeFrom(response.meta());
    }
  } else {
    ProcessError(__func__, status, talk);
  }

  LOGGER_trace("TalkExternal result: {}", result);

  return result;
}

/**
 * 외부시스템에 대화 응답을 전달한다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param talk: 대화 세션의 talk 객체
 * @param context: 서버 gRPC 호출의 context
 * @param daRequest: DA에 호출된 원래 request
 * @param daResponse: DA의 원래 response
 */
void ExternalSystemClient::AnswerExternal(TalkImpl *talk,
                                          const ServerContext *context,
                                          const TalkRequest &daRequest,
                                          TalkResponse &daResponse) {
  if (IsUnavailable()) {
    LOGGER_trace("AnswerExternal External System is Unavailable.");
    return;
  }

  if (!daResponse.has_response()) {
    LOGGER_debug("AnswerExternal: response is not exist. skip it.");
    return;
  }

  auto stub = ExternalSystemClient::GetStub();

  if (!stub) {
    return;
  }

  AnswerExternalRequest request;
  AnswerExternalResponse response;

  auto da_response = daResponse.response();
  request.mutable_system_answer()->MergeFrom(da_response.speech());
  request.mutable_directives()->MergeFrom(da_response.directives());
  request.mutable_session()->MergeFrom(daRequest.session());

  auto da_metas = da_response.meta().fields();
  if (da_metas.count("assessment") > 0) {
    request.set_assessment((float) da_metas.at("assessment").number_value());
    LOGGER_trace("AnswerExternal assessment: {}", request.assessment());
  }
  if (da_metas.count("interventionRequested") > 0) {
    request.set_intervention_requested(da_metas.at("interventionRequested").bool_value());
    LOGGER_trace("AnswerExternal interventionRequested: {}",
                 request.intervention_requested());
  }

  auto clientContext = GetClientContext(context, grpc_call_timeout_);
  auto reader(stub->AnswerExternal(clientContext.get(), request));

  ostringstream utter;
  while (reader->Read(&response)) {
    LOGGER_trace("AnswerExternal response:\n{}", response.Utf8DebugString());
    if (response.has_intervened_answer()) {
      LOGGER_trace("intervened_answer: {}", response
          .intervened_answer()
          .utter());
      utter << response.intervened_answer().utter();
    }
  }

  Status status = reader->Finish();
  // Response 처리
  if (status.ok()) {
    if (response.has_intervened_answer()) {
      response.mutable_intervened_answer()->set_utter(utter.str());
    }
    LOGGER_trace("AnswerExternal response:\n{}", response.Utf8DebugString());
    auto alter_response = daResponse.mutable_response();
    if (response.has_intervened_answer()) {
      alter_response->mutable_speech()->MergeFrom(response.intervened_answer());
      SetBoolMeta(alter_response->mutable_meta(), "m2u.intervenedAnswer", true);
    } else {
      SetBoolMeta(alter_response->mutable_meta(),
                  "m2u.intervenedAnswer",
                  false);
    }
    if (!response.directives().empty()) {
      alter_response->mutable_directives()->MergeFrom(response.directives());
    }
    if (!response.session_update().context().fields().empty()) {
      talk->UpdateSessionContext(response.session_update());
    }
    if (!response.meta().fields().empty()) {
      alter_response->mutable_meta()->MergeFrom(response.meta());
    }
  } else {
    ProcessError(__func__, status, talk);
  }
}

/**
 * 외부시스템에 대화 close를 전달한다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param talk: 대화 세션의 talk 객체
 * @param context: 서버 gRPC 호출의 context
 * @param session: 대화의 세션 정보
 */
void ExternalSystemClient::CloseExternal(TalkImpl *talk,
                                         const ServerContext *context,
                                         const Session &session) {
  if (IsUnavailable()) {
    LOGGER_trace("CloseExternal External System is Unavailable.");
    return;
  }

  auto stub = ExternalSystemClient::GetStub();

  if (!stub) {
    return;
  }

  Empty empty;
  Status status = stub->CloseExternal(GetClientContext(context, 3000).get(),
                                      Session(session),
                                      &empty);
  ProcessError(__func__, status, talk);
}

/**
 * 외부시스템을 연동하는 gRPC stub를 생성한다.
 *
 * @return 외부시스템 연동 gRPC stub
 */
unique_ptr<ExternalSystem::Stub> ExternalSystemClient::GetStub() {
  if (channel_ == nullptr) {
    channel_ = grpc::CreateChannel(endpoint_,
                                   grpc::InsecureChannelCredentials());

  }

  auto stub = ExternalSystem::NewStub(channel_);

  if (stub == nullptr) {
    LOGGER_error("Failed to create a external stub.");
    ResetChannel();
  }

  return stub;
}

/**
 * 외부시스템을 접속 불가 시 연동 정보 삭제
 */
void ExternalSystemClient::ProcessError(const string &api,
                                        const Status &status,
                                        TalkImpl *talk) {
  const StatusCode error_code = status.error_code();

  LOGGER_info("{} [{}] {}", api, error_code, status.error_message());

  if (error_code == StatusCode::UNAVAILABLE
      || error_code == StatusCode::DEADLINE_EXCEEDED) {
    LOGGER_error("External System '{}' is UNAVAILABLE.", endpoint_);
    endpoint_.clear();
    talk->GetParent()->mutable_external_system_info()->clear();
  }
}
