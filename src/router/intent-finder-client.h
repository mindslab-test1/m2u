#ifndef M2U_ROUTER_INTENT_FINDER_CLIENT_H
#define M2U_ROUTER_INTENT_FINDER_CLIENT_H

#include <hazelcast/client/query/SqlPredicate.h>
#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>
#include <grpc++/grpc++.h>
#include <maum/m2u/server/simpleclassifier.grpc.pb.h>
#include <maum/m2u/console/classifier.grpc.pb.h>
#include <maum/m2u/da/provider.pb.h>
#include <maum/m2u/router/v3/intentfinder.grpc.pb.h>

#include "hazelcast.h"
#include "local-chatbot.h"
#include "session.h"
#include "talk-impl.h"

using std::string;
using std::shared_ptr;
using std::unique_ptr;

using hzc = GlobalHazelcastClient;

using grpc::Status;
using grpc::StatusCode;

using maum::m2u::router::v3::FindIntentRequest;
using maum::m2u::router::v3::FindIntentResponse;
using maum::m2u::router::v3::FoundIntent;
using maum::m2u::router::v3::IntentFinder;
using maum::m2u::router::v3::RetryFindIntentResponse;

typedef shared_ptr<vector<string>> RemoteList;

enum ItfMethodType {
  NotInvolved = -1,
  ChatbotDefaultSkill = 0,
  FindIntent = 1,
  RetryFindIntent = 2,
  Analyze = 3
};

class IntentFinderClient {
 public:
  IntentFinderClient() = default;
  virtual ~IntentFinderClient() = default;

  static const string& GetMethodTypeString(int type);
  static const string GetDefaultSkill(const string &chatbot);

  Status FindIntent(ServerContext *context,
                    TalkImpl *talk,
                    FindIntentRequest &request,
                    FindIntentResponse &output);
  Status RetryFindIntent(ServerContext *context,
                         const string &policy,
                         FindIntentRequest &request,
                         RetryFindIntentResponse &output);
  Status Analyze(ServerContext *context,
                 TalkImpl *talk,
                 FindIntentRequest &request,
                 FindIntentResponse &output);

 private:
  RemoteList FindIntentFinderRemotes(const string &policy);
};

#endif // M2U_ROUTER_INTENT_FINDER_CLIENT_H
