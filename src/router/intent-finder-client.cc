#include <libmaum/common/util.h>
#include <libmaum/rpc/result-status.h>
#include <maum/m2u/console/da.grpc.pb.h>

#include "util.h"
#include "tls-values.h"
#include "x-error-index.h"
#include "intent-finder-client.h"
#include "local-intentfinder.h"

using std::to_string;
using hazelcast::client::query::SqlPredicate;
using google::protobuf::Value;
using grpc::ClientContext;
using maum::m2u::router::v3::IntentFinderInstance;

using meta_pair = google::protobuf::Map<string, Value>::value_type;
using cl_pair = google::protobuf::Map<int32_t, float>::value_type;

// 파일 단위로 error_index의 상위 두자리를 정의한다.
// error_index: 30000 ~ 39999
#define ERROR_INDEX_MAJOR 30000

/**
 * (static)
 * skill을 찾는 데 사용된 intent finder method의 문자열 값을 반환하는 함수
 *
 * @param type
 * @return type string value
 */
const string &IntentFinderClient::GetMethodTypeString(int type) {
  static const string kUndefined = "undefined";
  static const string kNotInvolved = "not-involved";
  static const string kChatbotDefaultSkill = "chatbot-default-skill";
  static const string kFindIntent = "find-intent";
  static const string kRetryFindIntent = "retry-find-intent";
  static const string kAnalyze = "analyze";

  switch (type) {
    case ItfMethodType::NotInvolved: return kNotInvolved;
    case ItfMethodType::ChatbotDefaultSkill: return kChatbotDefaultSkill;
    case ItfMethodType::FindIntent: return kFindIntent;
    case ItfMethodType::RetryFindIntent: return kRetryFindIntent;
    case ItfMethodType::Analyze: return kAnalyze;
    default: return kUndefined;
  }
}

/**
 * (static)
 * intent finder 가 none 일 때 default skill 을 찾아주는 함수
 *
 * @param chatbot
 * @return default skill
 */
const string IntentFinderClient::GetDefaultSkill(const string &chatbot) {
  LOGGER_debug("IntentFinder::{} chatbot - {}", __func__, chatbot);
  try {
    auto hc = hzc::Instance().GetHazelcastClient();
    auto cb_map = hc->getMap<string, LocalChatbot>(hzc::kNsChatbot);
    auto cb = cb_map.get(chatbot);

    if (cb != nullptr) {
      return cb->default_skill();
    }
  } catch (std::exception &e) {
    LOGGER_critical("IntentFinder::{} Hazelcast: {}", __func__, e.what());
  }

  return string();
}

/**
 * @brief talk-impl 에 저장된 정보로 itf 에서 skill 을 찾음
 * @param talk
 * @param output
 * @return
 */
Status IntentFinderClient::FindIntent(ServerContext *context,
                                      TalkImpl *talk,
                                      FindIntentRequest &request,
                                      FindIntentResponse &output) {
  auto const &policy = talk->GetParent()->intent_finder_policy();

  LOGGER_debug("IntentFinderClient::FindIntent itf_policy: {}", policy);

  RemoteList remotes = FindIntentFinderRemotes(policy);

  if (remotes->empty()) {
    return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                            X_ERROR_INDEX(ExIndex::X_ITF_NOT_FOUND, 10),
                            "Cannot find intent finder instance for [%s]",
                            policy.c_str());
  }

  Status status;
  for (auto &remote: *remotes) {
    LOGGER_trace("IntentFinderClient::{} itf stub: {}", __func__, remote);
    auto chan = grpc::CreateChannel(remote, grpc::InsecureChannelCredentials());
    auto stub = IntentFinder::NewStub(chan);
    LOGGER_trace("request ITF FindIntent: {}", request.Utf8DebugString());
    status = stub->FindIntent(GetClientContext(context).get(),
                              request,
                              &output);
    LOGGER_trace("response ITF FindIntent: {}", output.Utf8DebugString());
    if (status.error_code() != StatusCode::UNAVAILABLE) {
      break;
    }
  }

  if (status.ok() && output.has_skill()) {
    talk->set_skill(output.skill().skill());
  }

  return status;
}

/**
 * @brief talk-impl 에 저장된 정보로 itf 에서 skill을 다시 찾음
 * @param talk
 * @param output
 * @return
 */
Status IntentFinderClient::RetryFindIntent(ServerContext *context,
                                           const string &policy,
                                           FindIntentRequest &request,
                                           RetryFindIntentResponse &output) {
  LOGGER_debug("IntentFinderClient::RetryFindIntent itf_policy: {}", policy);

  // IntentFinder Stub 을 생성
  // ITF정책이름으로 hzc 에서 해당 itf 의 ip:port 을 가져옴
  RemoteList remotes = FindIntentFinderRemotes(policy);

  if (remotes->empty()) {
    return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                            X_ERROR_INDEX(ExIndex::X_ITF_NOT_FOUND, 20),
                            "Cannot find intent finder for [%s]",
                            policy.c_str());
  }

  Status status;
  for (auto &remote: *remotes) {
    LOGGER_trace("IntentFinderClient::{} itf stub: {}", __func__, remote);
    auto chan = grpc::CreateChannel(remote, grpc::InsecureChannelCredentials());
    auto stub = IntentFinder::NewStub(chan);
    LOGGER_trace("request ITF RetryFindIntent: {}", request.Utf8DebugString());
    status = stub->RetryFindIntent(GetClientContext(context).get(),
                                   request,
                                   &output);
    LOGGER_trace("response ITF RetryFindIntent: {}", output.Utf8DebugString());
    if (status.error_code() != StatusCode::UNAVAILABLE) {
      break;
    }
  }

  return status;
}

/**
 * @brief 의도파악 없이 Utter를 NLP처리한 다음 nlu_result를 받는다.
 *
 * @param talk
 * @param request
 * @param output
 * @return
 */
Status IntentFinderClient::Analyze(ServerContext *context,
                                   TalkImpl *talk,
                                   FindIntentRequest &request,
                                   FindIntentResponse &output) {
  // IntentFinder Stub 을 생성
  // ITF이름으로 hzc 에서 해당 itf 의 ip:port 을 가져옴
  auto const &policy = talk->GetParent()->intent_finder_policy();

  LOGGER_debug("IntentFinderClient::Analyze itf_policy: {}", policy);

  RemoteList remotes = FindIntentFinderRemotes(policy);

  if (remotes->empty()) {
    return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                            X_ERROR_INDEX(ExIndex::X_ITF_NOT_FOUND, 30),
                            "Cannot find intent finder for [%s]",
                            policy.c_str());
  }

  Status status;
  for (auto &remote: *remotes) {
    LOGGER_trace("IntentFinderClient::{} itf stub: {}", __func__, remote);
    auto chan = grpc::CreateChannel(remote, grpc::InsecureChannelCredentials());
    auto stub = IntentFinder::NewStub(chan);
    LOGGER_trace("request ITF Analyze: {}", request.Utf8DebugString());
    status = stub->Analyze(GetClientContext(context).get(), request, &output);
    LOGGER_trace("response ITF Analyze: {}", output.Utf8DebugString());
    if (status.error_code() != StatusCode::UNAVAILABLE) {
      break;
    }
  }

  if (status.ok()) {
    output.mutable_skill()->set_skill(talk->skill());
  }

  return status;
}

/**
 * @brief intent finder remote 목록을 찾아 반환
 * @param policy
 * @return RemoteList
 */
RemoteList IntentFinderClient::FindIntentFinderRemotes(const string &policy) {
  LOGGER_trace("IntentFinderClient::{} policy: {}", __func__, policy);

  RemoteList remotes = std::make_shared<vector<string>>();

  try {
    // 정책이름으로 ITF Instance를 검색한다.
    vector<LocalIntentFinderInstance> itfs;
    {
      auto hc = hzc::Instance().GetHazelcastClient();
      auto itf_map = hc->getMap<string, LocalIntentFinderInstance>
          (hzc::kNsIntentFinderInstance);

      ostringstream query;
      query << "policy_name='" << policy.c_str() << "'";
      itfs = itf_map.values(SqlPredicate(query.str()));
    }

    if (itfs.empty()) {
      LOGGER_warn("Can not find ITF Instance with policy {}.", policy);
      return remotes;
    }

    // ITF Instance가 1개 일 때는 0
    // ITF Instance가 1개 이상 있으면 랜덤하게 index 선택
    decltype(itfs.size()) index;
    decltype(itfs.size()) count = itfs.size();
    decltype(itfs.size()) start = count > 1 ? rand() % count : 0;
    for (decltype(itfs.size()) i = 0; i < itfs.size(); ++i) {
      index = (start + i) % count;
      const string &itfi_ip = itfs[index].export_ip().empty() ?
                              itfs[index].ip() :
                              itfs[index].export_ip();
      remotes->push_back(GetRemoteTarget(itfi_ip, itfs[index].port()));
      LOGGER_trace("ITF instance ip : {}", itfi_ip);
    }

    return remotes;
  } catch (std::exception &e) {
    LOGGER_critical("IntentFinder::{} Hazelcast: {}",
                    __func__, e.what());
    return remotes;
  }
}
