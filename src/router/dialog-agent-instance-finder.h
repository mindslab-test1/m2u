#ifndef M2U_ROUTER_DIALOG_AGENT_INSTANCE_POOL_H
#define M2U_ROUTER_DIALOG_AGENT_INSTANCE_POOL_H

#include <string>
#include <unordered_map>
#include <memory>
#include <atomic>
#include <sstream>
#include <mutex>
#include <grpc++/channel.h>
#include <json/json.h>

#include <libmaum/common/config.h>
#include <maum/common/lang.pb.h>
#include <maum/m2u/server/pool.grpc.pb.h>
#include <maum/m2u/da/provider.grpc.pb.h>
#include <hazelcast/client/query/SqlPredicate.h>
#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include "hazelcast.h"
#include "local-chatbot.h"

using std::string;
using google::protobuf::int64;
using std::unordered_map;
using std::unordered_multimap;
using std::shared_ptr;
using std::atomic;
using std::mutex;

using grpc::Status;
using grpc::StatusCode;
using grpc::ServerContext;
using maum::common::LangCode;
using maum::m2u::server::DialogAgentInstanceResource;
using maum::m2u::server::DialogAgentInstanceResourceSkill;
using maum::m2u::server::DialogAgentInstanceKey;
using maum::m2u::server::RegisterResponse;
using maum::m2u::server::DialogAgentInstanceStat;
using maum::m2u::da::AgentKind;
using maum::m2u::server::ChatbotResponse;
using maum::m2u::server::ChatbotRegisterResult;
using maum::m2u::server::ChatbotParam;
using maum::m2u::server::DialogAgentInstancePool;
using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;
using hazelcast::client::serialization::Serializer;
using hazelcast::client::serialization::ObjectDataOutput;
using hazelcast::client::serialization::ObjectDataInput;
using hazelcast::client::IList;
using hzc = GlobalHazelcastClient;

class DialogAgentInstanceResourceSkillPortable :
    public hazelcast::client::serialization::Portable,
    public DialogAgentInstanceResourceSkill {
 public:
  DialogAgentInstanceResourceSkillPortable() = default;
  ~DialogAgentInstanceResourceSkillPortable() override = default;
  DialogAgentInstanceResourceSkillPortable(
      const DialogAgentInstanceResourceSkillPortable &rhs) {
    this->CopyFrom(rhs);
  }

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override;
  int getClassId() const override;
  void writePortable(PortableWriter &out) const override;
  void readPortable(PortableReader &in) override;
};

class DialogAgentInstanceResourcePortable
    : public hazelcast::client::serialization::Portable,
      public DialogAgentInstanceResource {
 public:
  DialogAgentInstanceResourcePortable() = default;
  ~DialogAgentInstanceResourcePortable() override = default;
  DialogAgentInstanceResourcePortable(
      const DialogAgentInstanceResourcePortable &rhs) {
    this->CopyFrom(rhs);
  }

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override;
  int getClassId() const override;
  void writePortable(PortableWriter &out) const override;
  void readPortable(PortableReader &in) override;
};

class TalkImpl;

/**
 * 대화엔진을 등록하고 대화를 추척할 수 있는 기반을 마련한다.
 */
class DialogAgentInstanceFinder {
  DialogAgentInstanceFinder() = default;
 public:
  virtual ~DialogAgentInstanceFinder() = default;

 private:
  void RemoveDialogAgentInstance(const string &key);
  bool IsDialogAgentInstanceRunning(
      shared_ptr<DialogAgentInstanceResource> res);
 public:
  shared_ptr<DialogAgentInstanceResource> FindDialogAgentInstance(
      const TalkImpl &talk);
  shared_ptr<DialogAgentInstanceResource> FindDialogAgentInstance(
      const string &chatbot,
      const string &skill,
      maum::common::Lang lang);
  shared_ptr<DialogAgentInstanceResource> FindDialogAgentInstance(
      const string &chatbot,
      const string &skill,
      const string &key);
  static DialogAgentInstanceFinder *GetInstance();

 private:
  static atomic<DialogAgentInstanceFinder *> instance_;
  static mutex mutex_;
};

#endif // M2U_ROUTER_DIALOG_AGENT_INSTANCE_POOL_H
