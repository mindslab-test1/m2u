#ifndef ROUTER_UTIL_H
#define ROUTER_UTIL_H

#include <grpc++/grpc++.h>
#include <google/protobuf/struct.pb.h>
#include <google/protobuf/util/json_util.h>

using std::unique_ptr;
using std::string;
using std::to_string;
using google::protobuf::Value;
using google::protobuf::Struct;
using grpc::ServerContext;
using grpc::ClientContext;

extern const google::protobuf::util::JsonOptions json_format;
extern void SetGrpcDeadline(long deadline);
inline string GetRemoteTarget(const string &host, int port) {
  return host + ":" + to_string(port);
}
extern void SetStringMeta(Struct *meta, const string &key, const string &value);
extern void SetNumberMeta(Struct *meta, const string &key, const double &value);
extern void SetBoolMeta(Struct *meta, const string &key, const bool &value);
extern unique_ptr<ClientContext> GetClientContext(const ServerContext *context,
                                                  long call_deadline = 0);
extern bool IsPoolReady();

#endif // ROUTER_UTIL_H
