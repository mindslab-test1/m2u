#ifndef M2U_THE_PORTABLE_FACTORY_H
#define M2U_THE_PORTABLE_FACTORY_H

#include "tls-values.h"
#include "local-chatbot.h"
#include "dialog-agent-instance-finder.h"
#include "session.h"
#include "talk-impl.h"
#include "local-intentfinder.h"

using std::auto_ptr;
using std::shared_ptr;
using hazelcast::client::serialization::PortableFactory;
using hazelcast::client::serialization::Portable;

class ThePortableFactory : public PortableFactory {
 public:
  auto_ptr<Portable> create(int32_t classId) const override {
    switch (classId) {
      case CHATBOT: {
        return auto_ptr<Portable>(new LocalChatbot());
      }
      case DIALOG_AGENT_INSTANCE_RESOURCE: {
        return auto_ptr<Portable>(new DialogAgentInstanceResourcePortable());
      }
      case DIALOG_AGENT_INSTANCE_RESOURCE_SKILL: {
        return auto_ptr<Portable>(new DialogAgentInstanceResourceSkillPortable());
      }
      case DIALOG_SESSION_V3: {
        return auto_ptr<Portable>(new DialogSession());
      }
      case SESSION_TALK_V3: {
        return auto_ptr<Portable>(new TalkImpl());
      }
      case INTENT_FINDER_INSTANCE: {
        return auto_ptr<Portable>(new LocalIntentFinderInstance());
      }
      default: return auto_ptr<Portable>();
    }
  }
};

#endif //M2U_THE_PORTABLE_FACTORY_H
