#ifndef SIMPLE_GET_TEXT_H
#define SIMPLE_GET_TEXT_H

#include <mutex>
#include <unordered_map>
#include <grpc++/grpc++.h>

#include <maum/common/lang.pb.h>

using maum::common::Lang;

// FIXME: 관리자가 에러 메시지를 설정하고 기동 시 Map에서 로딩하여 사용하도록 한다.
// FIXME: https://github.com/mindslab-ai/m2u/issues/3811


/**
 * refers proto of maum.m2u.router.v3.DialogResult.
 */
enum TextRes {
  DIALOG_RESPONSE_OK, // 0,
  DIALOG_RESPONSE_USER_INVALIDATED, // 101
  DIALOG_RESPONSE_CHATBOT_NOT_FOUND, // 102
  DIALOG_RESPONSE_SESSION_NOT_FOUND, // 103
  DIALOG_RESPONSE_SESSION_INVALIDATED, // 104

  DIALOG_RESPONSE_EMPTY_IN, // 201

  DIALOG_RESPONSE_INTENT_FINDER_FAILED, // 301
  DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND, // 302
  DIALOG_RESPONSE_DIALOG_AGENT_NOT_READY, // 303
  DIALOG_RESPONSE_LAST_DIALOG_AGENT_LOST, // 304

  DIALOG_RESPONSE_SECONDARY_DIALOG_CONFLICT, // 311
  DIALOG_RESPONSE_LAST_AGENT_NOT_FOUND, // 312
  DIALOG_RESPONSE_DUPLICATED_SKILL_RESOLVED, // 313
  DIALOG_RESPONSE_SECONDARY_DIALOG_AGENT_IS_MULTI_TURN, // 314

  DIALOG_RESPONSE_INTERNAL_ERROR, // 500

  RES_MAX
};

class SimpleGetText {
 public:
  static const char *Get(Lang lang, TextRes res) {
    int32_t code = (lang << 24) | (int(res) & 0xFFFFFF);
    if (!initialized_) {
      Init();
    }
    auto it = resources_.find(code);
    if (it != resources_.end()) {
      return it->second;
    }
    return nullptr;
  }

 private:
  static std::mutex resource_mutex_;
  static bool initialized_;
  static void Init();

  static std::unordered_map<int32_t, const char *> resources_;
};

#define _T(a, b) SimpleGetText::Get(a, b)

#endif // SIMPLE_GET_TEXT_H
