#ifndef M2U_ROUTER_EXTERNAL_SYSTEM_H
#define M2U_ROUTER_EXTERNAL_SYSTEM_H

#include <string>
#include <grpc++/grpc++.h>
#include <maum/m2u/da/v3/talk.grpc.pb.h>
#include <maum/m2u/router/ext/v1/ext.grpc.pb.h>

using std::unique_ptr;
using std::string;
using grpc::Status;
using grpc::ServerContext;
using maum::m2u::common::Session;
using maum::m2u::da::v3::OpenSessionRequest;
using maum::m2u::da::v3::TalkRequest;
using maum::m2u::da::v3::TalkResponse;
using maum::m2u::router::ext::v1::ExternalSystem;

class TalkImpl;

/**
 * 외부시스템 연동 class
 */
class ExternalSystemClient {
 public:
  ExternalSystemClient() = delete;
  explicit ExternalSystemClient(const string &external_system_info);

  void OpenExternal(TalkImpl *talk,
                    const ServerContext *context,
                    const OpenSessionRequest &daRequest,
                    TalkResponse &daResponse);
  bool TalkExternal(TalkImpl *talk,
                    const ServerContext *context,
                    TalkRequest &daRequest,
                    TalkResponse &daResponse);
  void AnswerExternal(TalkImpl *talk,
                      const ServerContext *context,
                      const TalkRequest &daRequest,
                      TalkResponse &daResponse);
  void CloseExternal(TalkImpl *talk,
                     const ServerContext *context,
                     const Session &session);
 private:
  unique_ptr<ExternalSystem::Stub> GetStub();
  inline void ResetChannel() { channel_.reset(); };
  void ProcessError(const string &api, const Status &status, TalkImpl *talk);
  inline bool IsUnavailable() { return endpoint_.empty(); };

  // 외부시스템의 주소 변수
  string endpoint_;
  int session_timeout_;
  // 외부시스템 연동 grpc channel
  std::shared_ptr<grpc::Channel> channel_;
  // 외부시스템 TalkExternal, AnswerExternal 시 응답 대기시간 변수
  long grpc_call_timeout_ = 0;
};

#endif // M2U_ROUTER_EXTERNAL_SYSTEM_H
