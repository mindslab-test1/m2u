#include <libmaum/rpc/result-status.h>
#include <libmaum/log/call-log.h>

#include "tls-values.h"
#include "x-error-index.h"
#include "talk-router-v3.h"

using hazelcast::client::IMap;
using libmaum::rpc::CommonResultStatus;
using libmaum::log::CallLogInvoker;

const char *kMsgNewTalkFailure = "Failed to get a New Talk.";

// 파일 단위로 error_index의 상위 두자리를 정의한다.
// error_index: 10000 ~ 19999
#define ERROR_INDEX_MAJOR 10000

inline void LogStatusNResult(shared_ptr<spdlog::logger> logger,
                             Status &status,
                             DialogResult result) {
  if (status.ok()) {
    logger_debug("result=OK status=OK ");
  } else {
    logger_warn("result={}", DialogResult_Name(result));
    logger_warn("status=NOK {}", CommonResultStatus::GetErrorString(status));
  }
}

atomic<TalkRouterV3 *> TalkRouterV3::instance_;
mutex TalkRouterV3::mutex_;

TalkRouterV3 *TalkRouterV3::GetInstance() {
  TalkRouterV3 *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new TalkRouterV3;
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

TalkRouterV3::TalkRouterV3() {
  container_ = DialogSessionContainer::GetInstance();
}

/**
 * @brief 새로은 세션을 성생하도록 요청합니다.
 * `map`의 `Dialog/Open`에 대응합니다.
 *
 * @param session 세션
 * @param request
 * @param response
 * @return Status
 */
Status TalkRouterV3::OpenRoute(ServerContext *context,
                               shared_ptr<DialogSession> &session,
                               const OpenRouteRequest *req,
                               ServerWriter<TalkRouteResponse> *writer) {
  auto logger = LOGGER();
  logger_info("TalkRouterV3::OpenRoute()");
  logger_trace("request:\n{}", req->Utf8DebugString());


  // 기존 세션이 있으면 정리한다.
  session = container_->FindDialogSession(tls_device_id);
  if (session != nullptr) {
    // 기존 세션이 유효하게 설정되어 있으면 무효화한다.
    if (session->valid()) {
      session->Expire();
      logger_debug("invalidate abnormal session[{}].", session->id());
    }
  }

  // Session을 새로 생성한다.
  logger_debug("Create a new Session.");
  // Chatbot이 등록되어 있는지 확인한다.
  if (!container_->CheckChatbot(req->chatbot())) {
    logger_debug("Unregistered Chatbot[{}].", req->chatbot());
    return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                            X_ERROR_INDEX(ExIndex::X_CHATBOT_NOT_FOUND, 1),
                            "Chatbot is not assigned");
  }
  session = container_->CreateDialogSession(req->system_context().device(),
                                            context->peer(),
                                            tls_device_id,
                                            req->chatbot());
  if (!session) {
    return ResultGrpcStatus(ExCode::RESOURCE_EXHAUSTED,
                            ERROR_INDEX(1000),
                            "Failed to create a New Session");
  }

  SetTlsSessionId(session->id());

  // Message 전사 후 Talk Impl 호출
  OpenSessionRequest request;
  TalkResponse response;

  auto talk = session->NewTalk(session, req->system_context());
  if (talk == nullptr) {
    auto status = ResultGrpcStatus(ExCode::RESOURCE_EXHAUSTED,
                                   ERROR_INDEX(1001),
                                   kMsgNewTalkFailure);
    WriteErrorResponse(writer, response, status);
    return status;
  }

  auto ses = request.mutable_session();
  ses->set_id(session->id());
  ses->mutable_context()->MergeFrom(session->context());
  request.mutable_system_context()->CopyFrom(req->system_context());
  request.mutable_utter()->CopyFrom(req->utter());
  request.set_chatbot(req->chatbot());

  Status status = talk->OpenSession(context, request, response);

  LogStatusNResult(logger, status, talk->dialog_result());

  // 호출 성공이면 Message 전사 후 응답 전송
  if (talk->Ok()) {
    WriteTalkResponse(writer, response, *talk);
  } else {
    WriteErrorResponse(writer, response, status);
  }

  session->Update();

  return status;
}

/**
 * @brief 대화 요청을 보내고, 응답을 받습니다.
 * map의 Dialog Talk에 대응합니다.
 *
 * @param session 세션
 * @param req
 * @param writer
 * @return
 */
Status TalkRouterV3::TalkRoute(ServerContext *context,
                               shared_ptr<DialogSession> &session,
                               const TalkRouteRequest *req,
                               ServerWriter<TalkRouteResponse> *writer) {
  auto logger = LOGGER();
  logger_info("TalkRouterV3::TalkRoute()");
  logger_trace("request:\n{}", req->Utf8DebugString());

  // Message 전사 후 Talk Impl 호출
  TalkRequest request;
  TalkResponse response;

  auto talk = session->NewTalk(session, req->system_context());
  if (talk == nullptr) {
    auto status = ResultGrpcStatus(ExCode::RESOURCE_EXHAUSTED,
                                   ERROR_INDEX(2000),
                                   kMsgNewTalkFailure);
    WriteErrorResponse(writer, response, status);
    return status;
  }

  auto ses = request.mutable_session();
  ses->set_id(session->id());
  ses->mutable_context()->MergeFrom(session->context());
  request.mutable_system_context()->CopyFrom(req->system_context());
  request.mutable_utter()->CopyFrom(req->utter());

  Status status = talk->Talk(context, request, response);

  LogStatusNResult(logger, status, talk->dialog_result());

  // 호출 성공이면 Message 전사 후 응답 전송
  if (talk->Ok()) {
    WriteTalkResponse(writer, response, *talk);
  } else {
    WriteErrorResponse(writer, response, status);
  }

  session->Update();

  return status;
}

/**
 * @brief 이벤트를 보내고, 응답을 받습니다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param session 세션
 * @param request
 * @param response
 * @return
 */
Status TalkRouterV3::EventRoute(ServerContext *context,
                                shared_ptr<DialogSession> &session,
                                const EventRouteRequest *req,
                                ServerWriter<TalkRouteResponse> *writer) {
  auto logger = LOGGER();
  logger_info("TalkRouterV3::EventRoute()");
  logger_trace("request:\n{}", req->Utf8DebugString());

  // Message 전사 후 Talk Impl 호출
  EventRequest request;
  TalkResponse response;

  auto talk = session->NewTalk(session, req->system_context());
  if (talk == nullptr) {
    auto status = ResultGrpcStatus(ExCode::RESOURCE_EXHAUSTED,
                                   ERROR_INDEX(3000),
                                   kMsgNewTalkFailure);
    WriteErrorResponse(writer, response, status);
    return status;
  }

  auto ses = request.mutable_session();
  ses->set_id(session->id());
  ses->mutable_context()->MergeFrom(session->context());
  request.mutable_system_context()->CopyFrom(req->system_context());
  request.mutable_event()->CopyFrom(req->event());

  Status status = talk->Event(context, request, response);

  LogStatusNResult(logger, status, talk->dialog_result());

  // 호출 성공이면 Message 전사 후 응답 전송
  if (talk->Ok()) {
    WriteTalkResponse(writer, response, *talk);
  } else {
    WriteErrorResponse(writer, response, status);
  }

  session->Update();

  return status;
}

/**
 * @brief 명시적으로 대화 종료 요청을 응답을 받습니다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param context
 * @param session 세션
 * @param req
 * @param res
 * @return
 */
Status TalkRouterV3::CloseRoute(ServerContext *context,
                                shared_ptr<DialogSession> &session,
                                const CloseRouteRequest *req,
                                CloseRouteResponse *res) {
  auto logger = LOGGER();
  logger_info("TalkRouterV3::CloseRoute()");
  logger_trace("request:\n{}", req->Utf8DebugString());

  Status status(Status::OK);

  if (!session->last_agent_key().empty()) {
    auto talk = session->NewTalk(session, req->system_context());
    if (talk != nullptr) {
      // Message 전사 후 Talk Impl 호출
      CloseSkillRequest request;
      CloseSkillResponse response;

      auto ses = request.mutable_session();
      ses->set_id(session->id());
      ses->mutable_context()->MergeFrom(session->context());
      request.mutable_system_context()->CopyFrom(req->system_context());
      request.set_chatbot(session->chatbot());
      request.set_skill(session->last_skill());
      request.set_reason(CloseReason_USER_INITIATED);

      status = talk->CloseSkill(context, request, response);

      LogStatusNResult(logger, status, talk->dialog_result());
    } else {
      //CallLogInvoker log("CloseSkill", context, req, res, &status);
      logger_debug("Failed to get a New Talk.");
    }
  }
  // else {
  //  CallLogInvoker log("CloseSkill", context, req, res, &status);
  //}

  // Session을 삭제한다
  int count = container_->DestroyDialogSession(tls_device_id);
  logger_info("Closed skills: {}", count);
  res->set_message("TalkRoute closed. Bye bye, See you again.");

  return status;
}

/**
 * @brief 대화에 대한 피드백을 보냅니다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param session 세션
 * @param request
 * @param empty
 * @return
 */
Status TalkRouterV3::Feedback(ServerContext */*context*/,
                              shared_ptr<DialogSession> &session,
                              const FeedbackRequest *req) {
  auto logger = LOGGER();
  logger_info("TalkRouterV3::Feedback() satisfaction: {}", req->satisfaction());
  logger_trace("request:\n{}", req->Utf8DebugString());

  auto talk = session->GetTalk(session, 0);
  if (talk == nullptr) {
    return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                            ERROR_INDEX(5000),
                            "Failed to find current talk");
  }

  talk->set_feedback(req->satisfaction());
  talk->set_opinion(req->opinion());
  talk->Save();

  return Status::OK;
};

/**
 * @brief Device Id에 맵핑된 세션이 있는지 확인하고 세션을 지정한다.
 *
 * @param context
 * @param session
 * @param status
 * @return
 */
Status TalkRouterV3::IsSessionValid(shared_ptr<DialogSession> &session) {
  session = container_->FindDialogSession(tls_device_id);

  // Session id 를 찾을 수 없을 때
  if (session == nullptr) {
    return ResultGrpcStatus(
        ExCode::MEM_KEY_NOT_FOUND,
        X_ERROR_INDEX(ExIndex::X_SESSION_NOT_FOUND, 1),
        "Session is not found. Device ID: %s", tls_device_id.c_str());
  }

  // Session이 유효하지 않을 때
  if (!session->valid()) {
    return ResultGrpcStatus(
        ExCode::CONDITION_ERROR,
        X_ERROR_INDEX(ExIndex::X_SESSION_INVALIDATED, 2),
        "Session is invalidated. Device ID: %s", tls_device_id.c_str());
  }

  // 세션이 유효할 때, 세션을 지정한다.
  SetTlsSessionId(session->id());
  LOGGER_debug("device_id: {} valid session {}", tls_device_id, session->id());

  return Status::OK;
}

/**
 * @brief TalkImpl에서 받은 TalkResponse를 TalkRouteResponse로 전사한다.
 *
 * @param da_response
 * @param route_response
 */
void TalkRouterV3::WriteTalkResponse(ServerWriter<TalkRouteResponse> *writer,
                                     const TalkResponse &da_response,
                                     const TalkImpl &talk) {
  TalkRouteResponse route_response;
  Struct *meta;

  if (da_response.has_response()) {
    const auto &da_res = da_response.response();
    auto route_res = route_response.mutable_response();
    route_res->mutable_speech()->CopyFrom(da_res.speech());
    route_res->mutable_cards()->CopyFrom(da_res.cards());
    route_res->mutable_directives()->CopyFrom(da_res.directives());
    route_res->mutable_meta()->CopyFrom(da_res.meta());
    meta = route_res->mutable_meta();
  } else if (da_response.has_delegate()) {
    const auto &da_delegate = da_response.delegate();
    auto route_delegate = route_response.mutable_delegate();
    route_delegate->mutable_directive()->CopyFrom(da_delegate.directive());
    route_delegate->mutable_meta()->CopyFrom(da_delegate.meta());
    meta = route_delegate->mutable_meta();
  } else {
    auto response = route_response.mutable_response();
    string message(da_response.has_transit() ?
                   "Invalid TalkResponse from TalkImpl" :
                   "TalkResponse is not set from TalkImpl");
    LOGGER_warn("{}", message);
    TalkImpl::FillSpeech(response->mutable_speech(), message, Lang::en_US);
    SetStringMeta(response->mutable_meta(),
                  "talk.error",
                  da_response.has_transit() ?
                  "Invalid TalkResponse" :
                  "TalkResponse is not set");
    meta = response->mutable_meta();
  }
  // meta name without namespace 'm2u' is DEPRECATED.
  SetStringMeta(meta, "sessionid", tls_session_id);
  SetStringMeta(meta, "m2u.sessionId", tls_session_id);

  if (da_response.has_next_job()) {
    route_response.mutable_next_job()->CopyFrom(da_response.next_job());
  }

  WriteOptions options;
  writer->WriteLast(route_response, options);
}

/**
 * @brief 에러일 경우 TalkImpl에서 받은 TalkResponse 중 speech만 전사한다.
 *
 * @param da_response
 * @param route_response
 */
void TalkRouterV3::WriteErrorResponse(ServerWriter<TalkRouteResponse> *writer,
                                      const TalkResponse &da_response,
                                      const Status &status) {
  TalkRouteResponse route_response;

  auto response = route_response.mutable_response();
  if (da_response.has_response()) {
    const auto da_res = da_response.response();
    response->mutable_speech()->CopyFrom(da_res.speech());
    response->mutable_meta()->CopyFrom(da_res.meta());
  } else {
    LOGGER_warn("{}", status.error_message());
    TalkImpl::FillSpeech(response->mutable_speech(),
                         status.error_message(),
                         Lang::en_US);
    ostringstream error_code;
    error_code << "GRPC(" << status.error_code() << ")";
    SetStringMeta(response->mutable_meta(), "talk.error", error_code.str());
  }
  SetStringMeta(response->mutable_meta(), "sessionid", tls_session_id);

  WriteOptions options;
  writer->WriteLast(route_response, options);
}
