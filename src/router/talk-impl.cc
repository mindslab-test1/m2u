#include <ostream>
#include <maum/m2u/server/imageclass.grpc.pb.h>
#include <maum/m2u/server/session.grpc.pb.h>
#include <libmaum/common/util.h>
#include <libmaum/rpc/result-status.h>
#include <libmaum/log/call-log.h>

#include "util.h"
#include "tls-values.h"
#include "x-error-index.h"
#include "get-text.h"
#include "session.h"
#include "intent-finder-client.h"
#include "dialog-agent-instance-finder.h"

using std::to_string;

using hazelcast::client::serialization::PortableReader;
using hazelcast::client::serialization::PortableWriter;

using grpc::StatusCode;
using grpc::ClientContext;

using namespace google::protobuf;
using google::protobuf::Empty;
using google::protobuf::Map;
using google::protobuf::Duration;
using google::protobuf::Value;

using maum::common::LangCode;
using maum::m2u::common::Speech;
using maum::m2u::da::RuntimeParameterList;
using maum::m2u::da::DialogSessionState;

using libmaum::log::CallLogInvoker;
using libmaum::rpc::CommonResultStatus;

using da_spec = maum::m2u::da::DialogAgentProviderSpec;
using dap_v3 = maum::m2u::da::v3::DialogAgentProvider;
using maum::m2u::da::v3::SkillTransition_TransitionType_Name;
using trans_type = maum::m2u::da::v3::SkillTransition_TransitionType;

using time_pair = Map<string, Duration>::value_type;
using kv_pair = Map<string, string>::value_type;
using meta_pair = Map<string, Value>::value_type;

#define SKILL_TRANS_UNKNOWN trans_type::SkillTransition_TransitionType_SKILL_TRANS_UNKNOWN
#define SKILL_TRANS_GOTO_DEFAULT trans_type::SkillTransition_TransitionType_SKILL_TRANS_GOTO_DEFAULT
#define SKILL_TRANS_GOTO_ONCE trans_type::SkillTransition_TransitionType_SKILL_TRANS_GOTO_ONCE
#define SKILL_TRANS_GOTO trans_type::SkillTransition_TransitionType_SKILL_TRANS_GOTO
#define SKILL_TRANS_PUSH trans_type::SkillTransition_TransitionType_SKILL_TRANS_PUSH
#define SKILL_TRANS_DISCARD trans_type::SkillTransition_TransitionType_SKILL_TRANS_DISCARD
#define SKILL_TRANS_CANNOT_UNDERSTAND trans_type::SkillTransition_TransitionType_SKILL_TRANS_CANNOT_UNDERSTAND
#define SKILL_TRANS_RESET trans_type::SkillTransition_TransitionType_SKILL_TRANS_RESET
#define CHATBOT_TRANS_GOTO trans_type::SkillTransition_TransitionType_CHATBOT_TRANS_GOTO

#define MAX_GOTO_DEPTH  3

// 파일 단위로 error_index의 상위 두자리를 정의한다.
// error_index: 20000 ~ 29999
#define ERROR_INDEX_MAJOR 20000

inline unique_ptr<dap_v3::Stub> get_dap_v3_stub(ProviderStub &stub) {
  auto ptr = static_cast<dap_v3::Stub *>(stub.release());
  return unique_ptr<dap_v3::Stub>(ptr);
}

inline util::Status MessageToJsonString(const Message &message,
                                        string *output) {
  return util::MessageToJsonString(message, output, json_format);
}

inline bool IsInProcessResult(Status &status) {
  return CommonResultStatus::IsInProcessResult(status);
}

TalkImpl::TalkImpl(shared_ptr<DialogSession> &parent)
    : parent_(parent),
      itf_method_type_(ItfMethodType::NotInvolved),
      goto_depth_(0),
      skill_closed_(false) {
  set_session_id(parent_->id());
  set_feedback(-1);
  SetTimeToNow(mutable_start());
  if (!parent_->external_system_info().empty()) {
    auto client = new ExternalSystemClient(parent_->external_system_info());
    external_system_client_.reset(client);
  }
}

// UseQuery start
// virtual method override for public hazelcast::client::serialization::Portable
int TalkImpl::getFactoryId() const {
  return PORTABLE_FACTORY_ID;
}

// virtual method override for public hazelcast::client::serialization::Portable
int TalkImpl::getClassId() const {
  return SESSION_TALK_V3;
}

// virtual method override for public hazelcast::client::serialization::Portable
void TalkImpl::writePortable(PortableWriter &out) const {
  int serializeSize = 0;
  serializeSize = this->ByteSize();
  char bArray[serializeSize];
  this->SerializeToArray(bArray, serializeSize);
  std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

  out.writeInt("seq", this->seq());
  out.writeLong("session_id", this->session_id());
  out.writeByteArray("_msg", &data);

  out.writeInt("lang", this->lang());
  out.writeUTF("_in", &this->in());
  out.writeUTF("out", &this->out());
  out.writeInt("status_code", this->status_code());
  out.writeInt("dialog_result", this->dialog_result());

  out.writeUTF("skill", &this->skill()); //최종 스킬
  out.writeUTF("intent", &this->intent()); //최종 의도
  out.writeLong("start", this->start().seconds()); //시작 시간
  out.writeLong("end", this->end().seconds()); //끝 시간
}

// virtual method override for public hazelcast::client::serialization::Portable
void TalkImpl::readPortable(PortableReader &in) {
  std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
  ParseFromArray(vec.data(), (int) vec.size());
}
// UseQuery end


/**
 *세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다.
 * 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
 * context: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param req
 * @param res
 * @return
 */
Status TalkImpl::OpenSession(ServerContext *context,
                             OpenSessionRequest &req,
                             TalkResponse &res) {
  auto logger = LOGGER();
  const auto &utter = req.utter();
  SetWantDebugMeta(utter.meta());

  logger_debug("TalkImpl::OpenSession chatbot: {} utter: {}",
               req.chatbot(),
               utter.utter());

  SessionAccessor accessor(parent_);

  SetInUtter(utter);
  set_status_code(ExCode::SUCCESS);

  {
    const auto &skill = IntentFinderClient::GetDefaultSkill(req.chatbot());

    if (skill.empty()) {
      SetResults(utter.lang() == Lang::ko_KR ?
                 "반갑습니다. " + req.chatbot() + "입니다." :
                 "Hello. It's " + req.chatbot() + ".",
                 DialogResult::INTENT_FINDER_FAILED,
                 ExCode::MEM_KEY_NOT_FOUND);
      SaveToEnd();
      parent_->Invalidate();
      FillErrorSpeech(res.mutable_response(), out(), lang());
      return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                              ERROR_INDEX(1000),
                              "Chatbot [%s] default-skill not assigned",
                              skill.c_str());
    }

    req.set_skill(skill);
  }

  // DAP 찾기
  Provider provider = GetDialogAgentProvider(req.chatbot(),
                                             req.skill(),
                                             utter.lang());
  // DAP를 찾지 못했을 때
  // TODO: DialogResult는 router proto에 정의한 값으로 바꾼다.
  if (!provider.first) {
    if (status_code() == ExCode::SUCCESS) {
      SetResults(_T(lang(), DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND),
                 DialogResult::DIALOG_AGENT_NOT_FOUND,
                 ExCode::MEM_KEY_NOT_FOUND);
    }
    SaveToEnd();
    parent_->Invalidate();
    FillErrorSpeech(res.mutable_response(), out(), lang());
    return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                            X_ERROR_INDEX(ExIndex::X_DA_NOT_FOUND, 10),
                            "DAI not found for Chatbot[%s], Skill[%s]",
                            req.chatbot().c_str(),
                            req.skill().c_str());
  }

  // skill을 저장한다.
  set_skill(req.skill());

  // 서버로 보낼 메시지를 정리한다.
  Status status;
  auto dap_stub = get_dap_v3_stub(provider.first);
  {
    //from MFRT to DA Call LOGGER call
    //CallLogInvoker log("OpenSession", context, &req, &res, &status);
    logger_trace("DialogAgent OpenSession request\n{}", req.Utf8DebugString());
    TalkElapsedTimer timer("session.open", this);
    status = dap_stub->OpenSession(GetClientContext(context).get(), req, &res);
    logger_trace("DialogAgent OpenSession response\n{}",
                 res.Utf8DebugString());
  }

  if (status.ok()) {
    // session context를 update한다.
    UpdateSessionContext(res);
    // Skill, Chatbot 변경 수행한다.
    if (res.has_transit()) {
      // PerformSkillTransit() 호출 전 request의 utter는 업데이트
      if (!res.transit().replaced_utter().empty()) {
        const auto &req_utter = req.utter().utter();
        const auto &replaced_utter = res.transit().replaced_utter();

        req.mutable_utter()->set_utter(replaced_utter);
        logger_debug("{} req_utter [{}] ==> replaced_utter [{}]", __func__,
                     req_utter,
                     replaced_utter);
      }

      // update된 세션 context
      SetSessionContext(req.mutable_session(), parent_->context());
      // transition의 메타를 merge하여 전달한다.
      if (res.transit().has_meta()) {
        req.mutable_utter()->mutable_meta()->MergeFrom(res.transit().meta());
      }

      status = PerformSkillTransit(context, dap_stub, req, OPEN_SESSION, res);
    }
  }

  if (status.ok()) {
    Struct *meta = nullptr;
    if (res.has_response()) {
      // Speech Response 처리
      const auto &speech = res.response();
      SetOutSpeech(speech.speech());
      MessageToJsonString(speech, mutable_extra_out());
      meta = res.mutable_response()->mutable_meta();
    } else if (res.has_delegate()) {
      MessageToJsonString(res.delegate(), mutable_extra_out());
      meta = res.mutable_delegate()->mutable_meta();
    } else if (res.has_transit()) {
      status = SetTransitionNotAllowed(res,
                                       X_ERROR_INDEX(ExIndex::X_DA_ERROR, 11));
      meta = res.mutable_response()->mutable_meta();
    }

    // META 데이터를 전송에 포함하도록 한다.
    SetMetas(meta);
    if (status.ok()) {
      set_dialog_result(DialogResult::OK);
      set_status_code(ExCode::SUCCESS);
    }
  } else {
    if (status_code() == ExCode::SUCCESS) {
      SetResults(_T(lang(), DIALOG_RESPONSE_INTERNAL_ERROR),
                 DialogResult::INTERNAL_ERROR,
                 ExCode::RUNTIME_ERROR);
    }
    FillErrorSpeech(res.mutable_response(), out(), lang());

    if (!IsInProcessResult(status)) {
      status = GetStackStatus(status,
                              ExCode::RUNTIME_ERROR,
                              X_ERROR_INDEX(ExIndex::X_DA_ERROR, 12),
                              "OpenSession() failed.")
          .AddDebugInfo(GetDetails())
          .GetStatus();
    }
  }

  // 외부시스템 연동
  if (external_system_client_) {
    external_system_client_->OpenExternal(this, context, req, res);
  }

  RecordTalk(req.skill(), req.utter().lang(), res);
  SaveToEnd();
  parent_->Invalidate();

  return status;
}

/**
 * 대화를 주고 응답을 생성하도록 한다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param req
 * @param res
 * @return 대화 status
 */
Status TalkImpl::Talk(ServerContext *context,
                      TalkRequest &req,
                      TalkResponse &res) {
  auto logger = LOGGER();
  const auto &utter = req.utter();
  SetWantDebugMeta(utter.meta());

  logger_debug("TalkImpl::Talk [{}]", utter.utter());

  SessionAccessor accessor(parent_);

  // 외부시스템 연동
  if (external_system_client_) {
    if (external_system_client_->TalkExternal(this, context, req, res)) {
      set_dialog_result(DialogResult::OK);
      set_status_code(ExCode::SUCCESS);
      RecordTalk(req.skill(), req.utter().lang(), res);
      SaveToEnd();
      return Status::OK;
    }
  }

  // 발화 검사
  if (utter.utter().empty()) {
    SetResults(_T(lang(), DIALOG_RESPONSE_EMPTY_IN),
               DialogResult::EMPTY_IN,
               ExCode::INVALID_ARGUMENT);
    SaveToEnd();
    FillErrorSpeech(res.mutable_response(), out(), lang());
    return ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                            ERROR_INDEX(2000),
                            "Request utter is missing");
  }

  SetInUtter(utter);
  set_status_code(ExCode::SUCCESS);

  if (!transit_chatbot().empty()) {
    SetResults(_T(lang(), DIALOG_RESPONSE_INTERNAL_ERROR),
               DialogResult::INTERNAL_ERROR,
               ExCode::RUNTIME_ERROR);
    SaveToEnd();
    FillErrorSpeech(res.mutable_response(), out(), lang());
    return ResultGrpcStatus(ExCode::RUNTIME_ERROR,
                            ERROR_INDEX(2001),
                            "Transition to chatbot[{}] is not supported yet",
                            transit_chatbot().c_str());
  }

  Provider provider;

  // Intent Finder를 통한 DAP 찾기
  Status status;
  FindIntentResponse itf_output;
  bool new_skill;
  provider = GetDialogAgentProvider(context,
                                    req,
                                    status,
                                    itf_output,
                                    new_skill);
  if (!provider.second) {
    if (status_code() == ExCode::SUCCESS) {
      SetResults(_T(lang(), DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND),
                 DialogResult::DIALOG_AGENT_NOT_FOUND,
                 ExCode::MEM_KEY_NOT_FOUND);
    }
    SaveToEnd();
    FillErrorSpeech(res.mutable_response(), out(), lang());

    return !status.ok() ? status :
        ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                         X_ERROR_INDEX(ExIndex::X_DA_NOT_FOUND, 20),
                         "cannot find DAI for [%s]",
                         in().c_str())
  }

  // 서버로 보낼 메시지를 정리한다.
  auto dap_stub = get_dap_v3_stub(provider.first);

  {
    // Skill이 새로 분류되었을 경우 DA OpenSkill 호출
    if (new_skill) {
      OpenSkillResponse response;
      status = OpenSkill(context,
                         dap_stub,
                         req.mutable_session(),
                         req.system_context(),
                         response);
      if (!status.ok()) {
        set_dialog_result(DialogResult::INTERNAL_ERROR);
        set_status_code(ExCode::SUCCESS);
        return GetStackStatus(status,
                              ExCode::RUNTIME_ERROR,
                              ERROR_INDEX(2002),
                              "OpenSkill() failed in %s", __func__)
            .AddDebugInfo(GetDetails())
            .GetStatus();
      }
    }

    // DA Talk 호출
    {
      MakeRequest(req);
      req.mutable_found_intent()->CopyFrom(itf_output.skill());

      auto itf_replace = itf_output.mutable_replace();
      if (!itf_replace->replacements().empty()) {
        SetReplacementMeta(itf_replace, req.mutable_utter());
      }

      //from MFRT to DA Call LOGGER call
      //CallLogInvoker log("Talk", context, &req, &res, &status);
      logger_trace("DialogAgent Talk request\n{}", req.Utf8DebugString());
      TalkElapsedTimer timer("talk.to.first", this);
      status = dap_stub->Talk(GetClientContext(context).get(), req, &res);
      logger_trace("DialogAgent Talk response\n{}", res.Utf8DebugString());
    }
  }

  if (status.ok()) {
    // session context를 update한다.
    UpdateSessionContext(res);

    // Skill, Chatbot 변경 수행한다.
    if (res.has_transit()) {
      // PerformSkillTransit() 호출 전 request의 utter는 업데이트
      if (!res.transit().replaced_utter().empty()) {
        const auto &req_utter = req.utter().utter();
        const auto &replaced_utter = res.transit().replaced_utter();

        req.mutable_utter()->set_utter(replaced_utter);
        logger_debug("{} req_utter [{}] ==> replaced_utter [{}]", __func__,
                     req_utter,
                     replaced_utter);
      }

      // update된 세션 context 적용
      SetSessionContext(req.mutable_session(), parent_->context());
      // transition의 메타를 merge하여 전달한다.
      if (res.transit().has_meta()) {
        req.mutable_utter()->mutable_meta()->MergeFrom(res.transit().meta());
      }

      status = PerformSkillTransit(context, dap_stub, req, TALK, res);
    }
  }

  if (status.ok()) {
    Struct *meta = nullptr;
    if (res.has_response()) {
      // Speech Response 처리
      const auto &speech = res.response();
      SetOutSpeech(speech.speech());
      MessageToJsonString(speech, mutable_extra_out());
      meta = res.mutable_response()->mutable_meta();
      if (speech.close_skill()) {
        // GOTO_ONCE, CANNOT_UNDERSTAND가 아닌 경우(skill과 last_skill이 같지 않음)
        // Invalidate 한다.
        CloseSkillByResponse(context,
                             dap_stub,
                             req.mutable_session(),
                             req.system_context(),
                             skill() == last_skill());
      }
    } else if (res.has_delegate()) {
      MessageToJsonString(res.delegate(), mutable_extra_out());
      meta = res.mutable_delegate()->mutable_meta();
    } else if (res.has_transit()) {
      CloseSkillByResponse(context,
                           dap_stub,
                           req.mutable_session(),
                           req.system_context(),
                           true);
      status = SetTransitionNotAllowed(res,
                                       X_ERROR_INDEX(ExIndex::X_DA_ERROR, 21));
      meta = res.mutable_response()->mutable_meta();
    }

    // META 데이터를 전송에 포함하도록 한다.
    SetMetas(meta);

    if (status.ok()) {
      set_dialog_result(DialogResult::OK);
      set_status_code(ExCode::SUCCESS);
    }
  } else {
    CloseSkillByResponse(context,
                         dap_stub,
                         req.mutable_session(),
                         req.system_context(),
                         true);
    if (status_code() == ExCode::SUCCESS) {
      SetResults(_T(lang(), DIALOG_RESPONSE_INTERNAL_ERROR),
                 DialogResult::INTERNAL_ERROR,
                 ExCode::RUNTIME_ERROR);
    }
    FillErrorSpeech(res.mutable_response(), out(), lang());

    if (!IsInProcessResult(status)) {
      status = GetStackStatus(status,
                              ExCode::RUNTIME_ERROR,
                              X_ERROR_INDEX(ExIndex::X_DA_ERROR, 22),
                              "Talk() failed.")
          .AddDebugInfo(GetDetails())
          .GetStatus();
    }
  }
  // 외부시스템 연동
  if (external_system_client_) {
    external_system_client_->AnswerExternal(this, context, req, res);
  }

  RecordTalk(req.skill(), req.utter().lang(), res);
  SaveToEnd();

  return status;
}

/**
 * DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다.
 * 다음과 같은 동작이 예가 될 수 있다.
 * 로봇이 특정한 위치로 이동하라고 명명하고 수행이 완료되었을 경우에 내보낸다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param req
 * @param res
 * @return
 */
Status TalkImpl::Event(ServerContext *context,
                       EventRequest &req,
                       TalkResponse &res) {
  auto logger = LOGGER();
  const auto &event = req.event();
  const auto &param = event.param();

  logger_debug("TalkImpl::Event [{}:{}]", event.interface(), event.operation());
  logger_debug("Event with chatbot: {} skill: {} lang: {}",
               param.chatbot(),
               param.skill(),
               Lang_Name(param.lang()));

  SessionAccessor accessor(parent_);

  MessageToJsonString(req.event(), mutable_in());
  set_status_code(ExCode::SUCCESS);

  // DAP 찾기
  Provider provider = GetDialogAgentProvider(param.chatbot(),
                                             param.skill(),
                                             param.lang());

  if (!provider.first) {
    if (status_code() == ExCode::SUCCESS) {
      SetResults(_T(lang(), DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND),
                 DialogResult::DIALOG_AGENT_NOT_FOUND,
                 ExCode::MEM_KEY_NOT_FOUND);
    }
    SaveToEnd();
    FillErrorSpeech(res.mutable_response(), out(), lang());
    return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
                            X_ERROR_INDEX(ExIndex::X_DA_NOT_FOUND, 30),
                            "cannot find DAI for [%s, %s, %s]",
                            param.chatbot().c_str(),
                            param.skill().c_str(),
                            Lang_Name(param.lang()).c_str());
  }

  // skill을 저장한다.
  set_skill(param.skill());

  // DAP rpc 호출
  auto dap_stub = get_dap_v3_stub(provider.first);
  Status status;
  {
    //from MFRT to DA Call LOGGER call
    //CallLogInvoker log("Event", context, &req, &res, &status);
    logger_trace("DialogAgent Event request\n{}", req.Utf8DebugString());
    TalkElapsedTimer timer("event.to.first", this);
    status = dap_stub->Event(GetClientContext(context).get(), req, &res);
    logger_trace("DialogAgent Event response\n{}", res.Utf8DebugString());
  };

  if (status.ok()) {
    // session context를 update한다.
    UpdateSessionContext(res);
    // Skill, Chatbot 변경 수행한다.
    if (res.has_transit()) {
      // update된 세션 context
      SetSessionContext(req.mutable_session(), parent_->context());
      status = PerformSkillTransit(context, dap_stub, req, EVENT, res);
    }
  }

  if (status.ok()) {
    Struct *meta = nullptr;
    if (res.has_response()) {
      meta = res.mutable_response()->mutable_meta();
      // Speech Response 처리
      const auto &speech = res.response();
      SetOutSpeech(speech.speech());
      MessageToJsonString(speech, mutable_extra_out());
      if (speech.close_skill()) {
        // FIXME: Event의 CloseSkill은 정책을 재정의하여 최종 적용한다.
        // GOTO_ONCE, CANNOT_UNDERSTAND가 아닌 경우(skill과 last_skill이 같지 않음)
        // Invalidate 한다.
        CloseSkillByResponse(context,
                             dap_stub,
                             req.mutable_session(),
                             req.system_context(),
                             skill() == last_skill());
      }
    } else if (res.has_delegate()) {
      MessageToJsonString(res.delegate(), mutable_extra_out());
      meta = res.mutable_delegate()->mutable_meta();
    } else if (res.has_transit()) {
      CloseSkillByResponse(context,
                           dap_stub,
                           req.mutable_session(),
                           req.system_context(),
                           true);
      status = SetTransitionNotAllowed(res,
                                       X_ERROR_INDEX(ExIndex::X_DA_ERROR, 31));
      meta = res.mutable_response()->mutable_meta();
    }

    // META 데이터를 전송에 포함하도록 한다.
    SetMetas(meta);

    if (status.ok()) {
      set_dialog_result(DialogResult::OK);
      set_status_code(ExCode::SUCCESS);
    }
  } else {
    CloseSkillByResponse(context,
                         dap_stub,
                         req.mutable_session(),
                         req.system_context(),
                         true);
    if (status_code() == ExCode::SUCCESS) {
      SetResults(_T(lang(), DIALOG_RESPONSE_INTERNAL_ERROR),
                 DialogResult::INTERNAL_ERROR,
                 ExCode::RUNTIME_ERROR);
    }
    FillErrorSpeech(res.mutable_response(), out(), lang());

    if (!IsInProcessResult(status)) {
      status = GetStackStatus(status,
                              ExCode::RUNTIME_ERROR,
                              X_ERROR_INDEX(ExIndex::X_DA_ERROR, 32),
                              "Event() failed.")
          .AddDebugInfo(GetDetails())
          .GetStatus();
    }
  }

  RecordTalk(param.skill(), param.lang(), res);
  SaveToEnd();

  return status;
}

/**
 * 현재 SKILL이 종료되었음을 알려준다.
 * SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로 매우 주의해야 한다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *
 * @param req
 * @param res
 * @return
 */
Status TalkImpl::CloseSkill(ServerContext *context,
                            CloseSkillRequest &req,
                            CloseSkillResponse &res) {
  auto logger = LOGGER();
  logger_debug("TalkImpl::CloseSkill [{}]", req.skill());

  SessionAccessor accessor(parent_);

  set_close_skill(true);
  set_status_code(ExCode::SUCCESS);

  const string &last_agent_key = parent_->last_agent_key();

  logger_debug("CloseSkill chatbot: {} skill: {} key: {}",
               req.chatbot(),
               req.skill(),
               last_agent_key);
  Status status;

// DAP 찾기
  Provider provider = GetDialogAgentProvider(req.chatbot(),
                                             req.skill(),
                                             last_agent_key);
  if (provider.first) {
//    if (status_code() == ExCode::SUCCESS) {
//      SetResults(_T(lang(), DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND),
//                 DialogResult::DIALOG_AGENT_NOT_FOUND,
//                 ExCode::MEM_KEY_NOT_FOUND);
//    }
//    SaveToEnd();
//    return ResultGrpcStatus(ExCode::MEM_KEY_NOT_FOUND,
//                            X_ERROR_INDEX(ExIndex::X_DA_NOT_FOUND, 40),
//                            "Cannot find DAI for closing [%s, %s, %s]",
//                            req.chatbot().c_str(),
//                            req.skill().c_str(),
//                            last_agent_key.c_str());
//  }

    auto dap_stub = get_dap_v3_stub(provider.first);
    status = CloseSkill(context, dap_stub, req, res);

    if (status.ok()) {
      UpdateSessionContext(res.session_update());
    } else {
      logger_debug("CloseSkill() failed. {}:{}",
                   status.error_code(),
                   status.error_message());

      if (!IsInProcessResult(status)) {
        status = GetStackStatus(status,
                                ExCode::RUNTIME_ERROR,
                                X_ERROR_INDEX(ExIndex::X_DA_ERROR, 41),
                                "CloseSkill() failed.")
            .AddDebugInfo(GetDetails())
            .GetStatus();
      }
    }
  }

  // 외부시스템 연동
  if (external_system_client_) {
    external_system_client_->CloseExternal(this, context, req.session());
  }

  SaveToEnd();

  return status;
}

/**
 * 이 스킬이 시작되었음을 알려줍니다.
 * 새로운 세션으로 스킬이 시작될 때, 해당 세션에 관련된 초기화 작업을 수행할 수 있습니다.
 * Router는 해당 세션에서 스킬을 새로이 시작할 때 OPEN을 호출해서 정보를 알려줘야 합니다.
 * header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
 *  - 해당하는 스킬을 최초로 호출 할 때 호출된다.
 *  - 싱글턴 일 경우에는 호출되지 않는다.
 *
 * @param stub
 * @param context
 * @param session
 * @param system_context
 * @param response
 * @return Status
 */
Status TalkImpl::OpenSkill(const ServerContext *context,
                           unique_ptr<dap_v3::Stub> &stub,
                           Session *session,
                           const SystemContext &system_context,
                           OpenSkillResponse &res) {
  auto logger = LOGGER();
  logger_debug("TalkImpl::OpenSkill [{}]", skill());

  Status status;
  OpenSkillRequest req;

  req.mutable_session()->CopyFrom(*session);
  req.mutable_system_context()->CopyFrom(system_context);
  req.set_chatbot(chatbot());
  req.set_skill(skill());
  req.set_lang(lang());

  //from MFRT to DA Call LOGGER call
  //CallLogInvoker log("OpenSkill", context, &req, &res, &status);
  logger_trace("DialogAgent OpenSkill request\n{}", req.Utf8DebugString());
  TalkElapsedTimer timer("skill.open", this);
  status = stub->OpenSkill(GetClientContext(context).get(), req, &res);
  logger_trace("DialogAgent OpenSkill response\n{}", res.Utf8DebugString());

  if (status.ok()) {
    UpdateSessionContext(res.session_update());
    SetSessionContext(session, parent_->context());
  }

  return status;
}

/**
 * 주어진 stub, context, session, system_context로 Skill을 Close한다.
 *
 * @param context
 * @param client_context
 * @param stub
 * @param req
 *
 * @return Status
 */
void TalkImpl::CloseSkill(const ServerContext *context,
                          unique_ptr<dap_v3::Stub> &stub,
                          const Session &session,
                          const SystemContext &system_context) {
  auto logger = LOGGER();
  logger_debug("CloseSkill [{}]", skill());

  CloseSkillRequest req;
  CloseSkillResponse res;

  req.mutable_session()->CopyFrom(session);
  req.mutable_system_context()->CopyFrom(system_context);
  req.set_chatbot(chatbot());
  req.set_skill(skill());
  req.set_reason(CloseReason_DIALOG_ENDED);

  Status status = CloseSkill(context, stub, req, res);

  if (status.ok()) {
    // session context를 update한다.
    UpdateSessionContext(res.session_update());
  } else {
    logger_debug("CloseSkill() failed. {}:{}",
                 status.error_code(),
                 status.error_message());
  }
}

/**
 * 주어진 stub, context, request로 Skill을 Close한다.
 *
 * @param context
 * @param client_context
 * @param stub
 * @param req
 *
 * @return Status
 */
Status TalkImpl::CloseSkill(const ServerContext *context,
                            unique_ptr<dap_v3::Stub> &stub,
                            const CloseSkillRequest &req,
                            CloseSkillResponse &res) {
  auto logger = LOGGER();
  Status status;

  //from MFRT to DA Call LOGGER call
  //CallLogInvoker log("CloseSkill", context, &req, &res, &status);
  logger_trace("DialogAgent CloseSkill request\n{}", req.Utf8DebugString());
  TalkElapsedTimer timer("skill.close", this);
  status = stub->CloseSkill(GetClientContext(context).get(), req, &res);
  logger_trace("DialogAgent CloseSkill response\n{}", res.Utf8DebugString());

  return status;
}

/**
 * 전환된 스킬로 Talk을 호출한다.
 *
 * @param context
 * @param session
 * @param dap_stub
 * @param req
 * @param req_type
 * @param res
 * @param transition
 * @param permanent
 *
 * @return Status
 */
Status TalkImpl::CallWithTransitedSkill(ServerContext *context,
                                        const SystemContext &system_context,
                                        Session *session,
                                        unique_ptr<dap_v3::Stub> &dap_stub,
                                        Message &req,
                                        RequestType req_type,
                                        TalkResponse &res,
                                        const SkillTransition &transition,
                                        bool change_skill) {
  auto logger = LOGGER();
  Status status;
  const auto &to_skill = transition.skill();

  // TRANS_GOTO_ONCE, TRANS_CANNOT_UNDERSTAND일 경우는 current agent를 변경하지 않는다.
  Provider provider = GetDialogAgentProvider(chatbot(),
                                             to_skill,
                                             lang(),
                                             change_skill);
  if (!provider.second) {
    return GetResultStatus(ExCode::MEM_KEY_NOT_FOUND,
                           X_ERROR_INDEX(ExIndex::X_DA_NOT_FOUND, 70),
                           "cannot find DAI for [%s, %s, %s]",
                           chatbot().c_str(),
                           to_skill.c_str(),
                           Lang_Name(lang()).c_str())
        .AddDebugInfo(GetDetails())
        .GetStatus();
  }

  // TRANS_GOTO_ONCE, TRANS_CANNOT_UNDERSTAND의 경우 Single-turn DA만 가능하다.
  if (!change_skill) {
    auto &dainst_res =
        static_cast<shared_ptr<DialogAgentInstanceResource> &>(provider.second);
    if (!dainst_res->param().single_turn()) {
      return GetResultStatus(ExCode::CONDITION_ERROR,
                             ERROR_INDEX(7000),
                             "resolved dialog agent is not single turn.")
          .AddDebugInfo(GetDetails())
          .GetStatus();
    }
  }

  // DAP rpc 호출
  set_skill(to_skill);
  dap_stub = get_dap_v3_stub(provider.first);

  // Skill를 영구적으로 변경하는 경우
  if (change_skill && (req_type == TALK || req_type == EVENT)) {
    OpenSkillResponse response;
    status = OpenSkill(context, dap_stub, session, system_context, response);
    if (!status.ok()) {
      return GetStackStatus(status,
                            ExCode::RUNTIME_ERROR,
                            ERROR_INDEX(7001),
                            "OpenSkill() failed in %s", __func__)
          .AddDebugInfo(GetDetails())
          .GetStatus();
    }
  }

  //from MFRT to DA Call LOGGER call
  auto client_context = GetClientContext(context);
  if (req_type == OPEN_SESSION) {
    auto &request = dynamic_cast<OpenSessionRequest &>(req);
    logger_trace("DialogAgent OpenSession request\n{}",
                 req.Utf8DebugString());
    //CallLogInvoker log("OpenSession", context, &request, &res, &status);
    TalkElapsedTimer timer("open.session.after.transition", this);
    status = dap_stub->OpenSession(client_context.get(), request, &res);
    logger_trace("DialogAgent OpenSession response\n{}",
                 res.Utf8DebugString());
  } else if (req_type == TALK) {
    auto &request = dynamic_cast<TalkRequest &>(req);
    request.set_skill(to_skill);
    request.set_intent(transition.intent());
    if (!transition.replaced_utter().empty()) {
      FindIntentResponse itf_output;
      Analyze(context, request.utter(), itf_output);
      request.mutable_found_intent()->CopyFrom(itf_output.skill());
    }
    logger_trace("DialogAgent Talk request\n{}", req.Utf8DebugString());
    //CallLogInvoker log("Talk", context, &request, &res, &status);
    TalkElapsedTimer timer("talk.after.transition", this);
    status = dap_stub->Talk(client_context.get(), request, &res);
    logger_trace("DialogAgent Talk response\n{}",
                 res.Utf8DebugString());
  } else if (req_type == EVENT) {
    auto &request = dynamic_cast<EventRequest &>(req);
    logger_trace("DialogAgent Event request\n{}",
                 req.Utf8DebugString());
    //CallLogInvoker log("Event", context, &request, &res, &status);
    TalkElapsedTimer timer("event.after.transition", this);
    status = dap_stub->Event(client_context.get(), request, &res);
    logger_trace("DialogAgent Event response\n{}",
                 res.Utf8DebugString());
  }

  if (status.ok()) {
    // session context를 update한다.
    UpdateSessionContext(res);
  }

  return status;
}

/**
 * 스킬 전환을 수행한다.
 *
 * @param context
 * @param dap_stub
 * @param req
 * @param req_type
 * @param res
 *
 * @return Status
 */
Status TalkImpl::PerformSkillTransit(ServerContext *context,
                                     unique_ptr<dap_v3::Stub> &dap_stub,
                                     Message &req,
                                     RequestType req_type,
                                     TalkResponse &res) {
  auto logger = LOGGER();

  SkillTransition transition = res.transit();
  const trans_type transition_type = transition.transition_type();

  logger_debug("PerformSkillTransit type: {} to_skill: {}",
               SkillTransition_TransitionType_Name(transition_type),
               transition.skill());

  res.clear_transit();

  Session *session =
      req_type == TALK ?
      dynamic_cast<TalkRequest &>(req).mutable_session() :
      req_type == EVENT ?
      dynamic_cast<EventRequest &>(req).mutable_session() :
      dynamic_cast<OpenSessionRequest &>(req).mutable_session();
  const SystemContext &system_context =
      req_type == TALK ?
      dynamic_cast<TalkRequest &>(req).system_context() :
      req_type == EVENT ?
      dynamic_cast<EventRequest &>(req).system_context() :
      dynamic_cast<OpenSessionRequest &>(req).system_context();

  switch (transition_type) {
    // 현재 skill을 변경 후 Talk하여
    case SKILL_TRANS_GOTO_DEFAULT:
    case SKILL_TRANS_GOTO_ONCE:
    case SKILL_TRANS_GOTO: {
      ++goto_depth_;
      logger_debug("Transition GOTO Depth: {}", goto_depth_);
      if (transition_type == SKILL_TRANS_GOTO_DEFAULT) {
        auto default_skill = IntentFinderClient::GetDefaultSkill(chatbot());
        transition.set_skill(default_skill);
      }
      if (HasStackedSkill() &&
          (transition.skill() == parent_->stacked_skill())) {
        return GetResultStatus(ExCode::CONDITION_ERROR,
                               ERROR_INDEX(6000),
                               "Not allowed going to stacked-skill[%s]",
                               transition.skill().c_str())
            .AddDebugInfo(GetDetails())
            .GetStatus();
      }
      bool change_skill = transition_type != SKILL_TRANS_GOTO_ONCE;
      if (change_skill) {
        CloseSkill(context, dap_stub, *session, system_context);
        parent_->Invalidate();
      }

      Status status = CallWithTransitedSkill(context,
                                             system_context,
                                             session,
                                             dap_stub,
                                             req,
                                             req_type,
                                             res,
                                             transition,
                                             change_skill);

      // GOTO의 결과가 GOTO 일 경우에 Skill 전환을 MAX_GOTO_DEPTH회 추가로 진행한다.
      if (goto_depth_ < MAX_GOTO_DEPTH
          && transition_type == SKILL_TRANS_GOTO
          && status.ok()
          && res.transit().transition_type() == SKILL_TRANS_GOTO) {
        if (req_type == OPEN_SESSION || req_type == TALK) {
          // PerformSkillTransit() 호출 전 request의 utter는 업데이트
          if (!res.transit().replaced_utter().empty()) {
            auto mutable_utter =
                req_type == OPEN_SESSION ?
                dynamic_cast<OpenSessionRequest &>(req).mutable_utter() :
                dynamic_cast<TalkRequest &>(req).mutable_utter();

            const auto &req_utter = mutable_utter->utter();
            const auto &replaced_utter = res.transit().replaced_utter();

            logger_debug("{} req_utter [{}] ==> replaced_utter [{}]",
                         __func__, req_utter, replaced_utter);
            mutable_utter->set_utter(replaced_utter);
          }

          // transition의 메타를 merge하여 전달한다.
          if (res.transit().has_meta()) {
            auto mutable_utter =
                req_type == OPEN_SESSION ?
                dynamic_cast<OpenSessionRequest &>(req).mutable_utter() :
                dynamic_cast<TalkRequest &>(req).mutable_utter();
            mutable_utter->mutable_meta()->MergeFrom(res.transit().meta());
          }
        }

        // update된 세션 context 적용
        SetSessionContext(session, parent_->context());
        status = PerformSkillTransit(context, dap_stub, req, req_type, res);
      }

      return status;
    }
    case SKILL_TRANS_PUSH: {
      // 이미 stacked skill이 있으면 에러 로그 출력 후 진행
      if (HasStackedSkill()) {
        logger_warn("Failed to push the skill. A skill has stacked!!");
        return Status::OK;
      }

      PushSkill();

      return CallWithTransitedSkill(context,
                                    system_context,
                                    session,
                                    dap_stub,
                                    req,
                                    req_type,
                                    res,
                                    transition,
                                    true);
    }
      /**
       * - SKILL_TRANS_DISCARD 동작
       *   현재 skill을 제외하고 ITF 분류 후 skill 이동
       *   SKILL_TRANS_GOTO와 유사함.
       *
       * - SKILL_TRANS_CANNOT_UNDERSTAND 동작
       *   현재 skill을 제외하고 ITF 분류한 skill로 현재 턴만 Talk함.
       *   싱글턴 DA만 가능함.
       *   SKILL_TRANS_GOTO_ONCE와 유사함.
       */
    case SKILL_TRANS_DISCARD:
    case SKILL_TRANS_CANNOT_UNDERSTAND: {
      if (req_type != TALK) {
        return GetResultStatus(ExCode::INDEX_OUT_OF_RANGE,
                               ERROR_INDEX(6001),
                               "Invalid Request Type[%d]", req_type)
            .AddDebugInfo(GetDetails())
            .GetStatus();
      }

      vector<string> exclusions;
      exclusions.emplace_back(skill());
      exclusions.emplace_back(parent_->stacked_skill());

      // 현재 skill을 close
      CloseSkill(context, dap_stub, *session, system_context);
      parent_->Invalidate();

      auto &request = dynamic_cast<TalkRequest &>(req);
      RetryFindIntentResponse itf_res;
      Status status = ResolveSecondarySkill(context,
                                            request,
                                            exclusions,
                                            itf_res);
      if (!status.ok()) {
        return status;
      }

      if (!itf_res.skill().is_default_skill()
          && itf_res.skill().skill() == skill()) {
        logger_warn("resolved skill is duplicated.");
        set_dialog_result(DialogResult::DUPLICATED_SKILL_RESOLVED);
        SetResults(_T(lang(), DIALOG_RESPONSE_DUPLICATED_SKILL_RESOLVED),
                   DialogResult::DUPLICATED_SKILL_RESOLVED,
                   ExCode::FAILED_PRECONDITION);
        return GetStackStatus(status,
                              ExCode::FAILED_PRECONDITION,
                              ERROR_INDEX(6003),
                              "resolved skill is duplicated.")
            .AddDebugInfo(GetDetails())
            .GetStatus();
      }

      request.mutable_found_intent()->CopyFrom(itf_res.skill());
      transition.set_skill(itf_res.skill().skill());
      transition.clear_replaced_utter();

      auto itf_replace = itf_res.mutable_replace();
      auto req_utter = request.mutable_utter();
      if (!itf_replace->replacements().empty()) {
        SetReplacementMeta(itf_replace, req_utter);
      }

      status = CallWithTransitedSkill(context,
                                      system_context,
                                      session,
                                      dap_stub,
                                      req,
                                      req_type,
                                      res,
                                      transition,
                                      transition_type == SKILL_TRANS_DISCARD);

      // DISCARD의 결과가 GOTO 일 경우에 Skill 전환을 1회만 추가로 진행한다.
      if (transition_type == SKILL_TRANS_DISCARD
          && status.ok()
          && res.transit().transition_type() == SKILL_TRANS_GOTO) {
        // PerformSkillTransit() 호출 전 request의 utter는 업데이트
        if (!res.transit().replaced_utter().empty()) {
          const auto &req_utter = request.utter().utter();
          const auto &replaced_utter = res.transit().replaced_utter();

          logger_debug("{} req_utter [{}] ==> replaced_utter [{}]", __func__,
                       req_utter,
                       replaced_utter);
          request.mutable_utter()->set_utter(replaced_utter);
        }

        // transition의 메타를 merge하여 전달한다.
        if (res.transit().has_meta()) {
          request.mutable_utter()
              ->mutable_meta()->MergeFrom(res.transit().meta());
        }

        // update된 세션 context 적용
        SetSessionContext(request.mutable_session(), parent_->context());
        status = PerformSkillTransit(context, dap_stub, req, TALK, res);
      }

      return status;
    }
    case SKILL_TRANS_RESET: {
      if (HasStackedSkill()) {
        CloseSkill(context, dap_stub, *session, system_context);
        PopSkill();
        set_skill(parent_->last_skill());
      }
      CloseSkill(context, dap_stub, *session, system_context);
      parent_->Invalidate();

      return CallWithTransitedSkill(context,
                                    system_context,
                                    session,
                                    dap_stub,
                                    req,
                                    req_type,
                                    res,
                                    transition,
                                    true);
    }
    case CHATBOT_TRANS_GOTO: {
      return GetResultStatus(ExCode::RUNTIME_ERROR,
                             ERROR_INDEX(6900),
                             "CHATBOT_TRANS_GOTO is not implemented yet")
          .AddDebugInfo(GetDetails())
          .GetStatus();
    }

    default:
    case SKILL_TRANS_UNKNOWN:
      return GetResultStatus(ExCode::INDEX_OUT_OF_RANGE,
                             ERROR_INDEX(6999),
                             "Invalid Transition Type(%d)",
                             transition_type)
          .AddDebugInfo(GetDetails())
          .GetStatus();
  }
}

void TalkImpl::MakeRequest(TalkRequest &req) {
  auto utter = req.mutable_utter();
  utter->set_lang(lang());
  utter->set_utter(in());
  req.set_chatbot(chatbot());
  req.set_skill(skill());
}

void TalkImpl::UpdateSessionContext(const TalkResponse &res) {
  if (res.has_response()) {
    UpdateSessionContext(res.response().session_update());
  } else if (res.has_transit()) {
    UpdateSessionContext(res.transit().session_update());
  } else if (res.has_delegate()) {
    UpdateSessionContext(res.delegate().session_update());
  }
}

void TalkImpl::SetMetas(Struct *meta) {
  if (meta != nullptr) {
    // meta name without namespace 'm2u' is DEPRECATED.
    SetStringMeta(meta, "skill", skill());
    SetStringMeta(meta, "intent", intent());
    SetStringMeta(meta, "m2u.skill", skill());
    SetStringMeta(meta, "m2u.intent", intent());
    SetStringMeta(meta, "m2u.itfMethod",
                  IntentFinderClient::GetMethodTypeString(itf_method_type_));
    if (skill_closed_)
      SetBoolMeta(meta, "m2u.skillClosed", skill_closed_);
    if (want_debug_meta_) {
      for (auto &dair : dair_flow_) {
        SetStringMeta(meta, "m2u.debug." + dair.first, dair.second);
      }
    }
  } else {
    LOGGER_debug("Struct Meta in null.");
  }
}

/**
 * 의도파악 분류 후 대체 된 값이 존재하면 사용자 발화 정보의 meta 영역에
 * "replacement", replace message 정보를 저장합니다.
 *
 * @param replacement  의도파악분류결과 중 대체된 문자열 정보
 * @param utter 사용자 발화 정보
 */
void TalkImpl::SetReplacementMeta(const Replacement *replacement,
                                  Utter *utter) {
  auto utter_meta = utter->mutable_meta()->mutable_fields();
  Value v;
  auto v_fields = v.mutable_struct_value()->mutable_fields();
  auto rep = replacement->replacements();
  for (auto &r: rep) {
    Value v2;
    v2.set_string_value(r.second);
    v_fields->insert(meta_pair(r.first, v2));
  }
  utter_meta->insert(meta_pair("replacements", v));
  utter->set_utter(replacement->replaced_utter());
}

/**
 * OpenSession, Talk, Event의 TalkResponse에 따른 skill close 처리
 *
 * @param context
 * @param stub
 * @param session
 * @param system_context
 * @param invalidate
 */
void TalkImpl::CloseSkillByResponse(const ServerContext *context,
                                    unique_ptr<dap_v3::Stub> &stub,
                                    Session *session,
                                    const SystemContext &system_context,
                                    bool invalidate) {
  // update된 세션 context 적용
  SetSessionContext(session, parent_->context());
  CloseSkill(context, stub, *session, system_context);
  if (HasStackedSkill()) {
    PopSkill();
  } else if (invalidate) {
    set_close_skill(true);
    parent_->Invalidate();
    skill_closed_ = true;
  }
}

/**
 * OpenSession, Talk, Event의 TalkResponse가 SkillTransition일 때, 에러 처리
 *
 * @param res  TalkResponse
 * @param error_index 상세 에러 코드
 *
 * @return status GRPC Status
 */
Status TalkImpl::SetTransitionNotAllowed(TalkResponse &res, int error_index) {
  ostringstream oss;
  string message;
  Struct *meta = new Struct();
  auto transition = res.transit();

  oss << SkillTransition_TransitionType_Name(transition.transition_type())
      << " to " << transition.skill() << " is not allowed as Response.";
  message = oss.str();
  meta->CopyFrom(transition.meta());
  res.clear_transit();

  if (status_code() == ExCode::SUCCESS) {
    SetResults(_T(lang(), DIALOG_RESPONSE_INTERNAL_ERROR),
               DialogResult::INTERNAL_ERROR,
               ExCode::RUNTIME_ERROR);
  }
  res.mutable_response()->set_allocated_meta(meta);
  FillErrorSpeech(res.mutable_response(), message, lang());

  return GetResultStatus(ExCode::RUNTIME_ERROR,
                         error_index,
                         message.c_str())
      .AddDebugInfo(GetDetails())
      .GetStatus();
}

void TalkImpl::RecordTalk(const string skill,
                          Lang lang,
                          const TalkResponse &rsp) {
  auto agent = add_agents();

  agent->set_skill(skill);
  agent->set_lang(lang);

  mutable_statistics()->CopyFrom(rsp.statistics());

  // 메타 및 컨텍스트 저장
  auto stalk_meta = mutable_output_meta();
  Map<string, Value> fields;
  if (rsp.has_response()) {
    fields = rsp.response().meta().fields();
  } else if (rsp.has_transit()) {
    fields = rsp.transit().meta().fields();
  } else if (rsp.has_delegate()) {
    fields = rsp.delegate().meta().fields();
  } else {
    LOGGER_error("Invalid Type of TalkResponse.");
  }

  for (auto &kv : fields) {
    if (!kv.second.string_value().empty()) {
      stalk_meta->insert(kv_pair(kv.first, kv.second.string_value()));
    }
  }
}

Provider TalkImpl::GetDialogAgentProvider(const string &chatbot,
                                          const string &skill,
                                          const string &key) {

  // 대화 에이전트 풀에서 적절한 에이젠트를 찾는다.
  auto da_finder = DialogAgentInstanceFinder::GetInstance();

  // 원격 서버의 정보를 구해온다.
  auto agent_res = da_finder->FindDialogAgentInstance(chatbot, skill, key);
  if (!agent_res) {
    LOGGER_trace("Cannot find DialogAgentInstance with {}, {}",
                 chatbot,
                 skill);
    set_dialog_result(DialogResult::DIALOG_AGENT_NOT_FOUND);
    return std::make_pair(ProviderStub(), ProviderRes());
  }

  // 채널을 생성한다.
  return CreateDAProvider(agent_res, skill, false);
}

Provider TalkImpl::GetDialogAgentProvider(const string &chatbot,
                                          const string &skill,
                                          const Lang lang,
                                          bool set_current) {
  shared_ptr<DialogAgentInstanceResource> agent_res(nullptr);
  auto da_finder = DialogAgentInstanceFinder::GetInstance();

  // skill이 last skill과 같으면 이전에 사용하던 DAI를 이용한다.
  if (skill == last_skill()) {
    LOGGER_debug("GetDialogAgentProvider with last_skill: {}", last_skill());
    const string &last_agent_key = parent_->last_agent_key();

    if (last_agent_key.empty()) {
      LOGGER_warn("last_agent_key is empty.");
    } else {
      agent_res = da_finder->FindDialogAgentInstance(chatbot,
                                                     last_skill(),
                                                     last_agent_key);
      if (!agent_res) {
        LOGGER_trace("Cannot find agent with {} {}",
                     last_skill(),
                     last_agent_key);
      }
    }
  }

  // 이전에 사용하던 DAI를 사용할 수 없으면 새로 DAI의 정보를 구해온다.
  if (!agent_res) {
    agent_res = da_finder->FindDialogAgentInstance(chatbot, skill, lang);
  }

  if (!agent_res) {
    LOGGER_trace("Cannot find DialogAgentInstance with {}, {}, {}",
                 chatbot,
                 skill,
                 Lang_Name(lang));
    SetResults(_T(this->lang(), DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND),
               DialogResult::DIALOG_AGENT_NOT_FOUND,
               ExCode::MEM_KEY_NOT_FOUND);
    return std::make_pair(ProviderStub(), ProviderRes());
  }

  // 채널을 생성한다.
  return CreateDAProvider(agent_res, skill, set_current);
}

/**
 * 발화문의 의도에 맞는 DAI를 정보를 확득한다.
 *
 * @param context
 * @param req
 * @param status
 * @param itf_output
 * @param new_skill 새로운 skill을 시작할 경우 OpenSkill을 하도록 flag를 넘겨준다.
 *
 * @return DAI 정보
 */
Provider TalkImpl::GetDialogAgentProvider(ServerContext *context,
                                          TalkRequest &req,
                                          Status &status,
                                          FindIntentResponse &itf_output,
                                          bool &new_skill) {
  // Client에서 요청한 Talk-Skill을 확인
  string talk_skill;
  bool use_talk_skill;
  {
    auto &metas = req.utter().meta().fields();
    auto it = metas.find("m2u.talkSkill");
    if (it != metas.cend() && !it->second.string_value().empty()) {
      talk_skill = it->second.string_value();
      // 멀티턴으로 대화를 진행 중인 skill이 있을 경우, Talk-Skill를 무시한다.
      if (parent_->last_agent_key().empty()) {
        use_talk_skill = true;
        LOGGER_debug("talkSkill [{}] is accepted.", talk_skill);
      } else {
        LOGGER_debug("talkSkill [{}] is rejected. "
                     "There is an ongoing skill[{}].",
                     talk_skill, last_skill());
      }
    }
  }

  Provider res;

  if (parent_->last_agent_key().empty() && !use_talk_skill) {
    res = ResolveNewSkill(context, req, status, itf_output);
    new_skill = true;
  } else {
    res = ResolveSpecifiedSkill(context,
                                req,
                                use_talk_skill ? talk_skill : last_skill(),
                                !use_talk_skill,
                                itf_output);
    new_skill = use_talk_skill;
    status = Status::OK;
  }

  if (res.first) {
    LOGGER_debug("Classified - skill: {} intent: {} new: {}",
                 skill(),
                 intent(),
                 new_skill);
  }

  return res;
}

/**
 * 대화에이전트를 선택하기 위해서 다른 `SkillResolver`들을 호출한다.
 * 모든 대화 에이젠트에 대해서 수용 여부를 점검해야 한다.
 *
 * @return 대화 연결을 위한 원격 서버 주소를 가지고 있는 Stub 정보를
 * 반환한다. unique_ptr<dap_v3::Stub>
 */
Provider TalkImpl::ResolveNewSkill(ServerContext *context,
                                   TalkRequest &req,
                                   Status &status,
                                   FindIntentResponse &itf_output) {
  LOGGER_debug("ResolveNewSkill utter: {}", req.utter().utter());

// TODO: trigger chatbot 기능 추가 시 참고할 것.
//  // 다른 챗봇에서 온 transit인지를 체크한다.
//  if (transit_chatbot().empty() && itf_output.has_transit()) {
//    set_transit_chatbot(itf_output.transit().chatbot());
//  }

  auto const &policy = parent_->intent_finder_policy();

  // itf 가 없을 때 default skill 세팅
  if (policy == "none" || policy.empty()) {
    const string skill = IntentFinderClient::GetDefaultSkill(chatbot());
    LOGGER_debug("Use Chatbot default Skill: {}", skill);
    if (skill.empty()) {
      LOGGER_debug("Default Skill for {} is empty.", chatbot());
      SetResults(_T(lang(), DIALOG_RESPONSE_INTENT_FINDER_FAILED),
                 DialogResult::INTENT_FINDER_FAILED,
                 ExCode::MEM_KEY_NOT_FOUND);
      return std::make_pair(ProviderStub(), ProviderRes());
    }
    set_skill(skill);
    itf_method_type_ = ItfMethodType::ChatbotDefaultSkill;
  } else {
    // itf variables
    IntentFinderClient itf_client;
    FindIntentRequest itf_req;

    itf_req.mutable_utter()->CopyFrom(req.utter());
    itf_req.mutable_system_context()->CopyFrom(req.system_context());
    itf_req.mutable_session()->CopyFrom(req.session());
    itf_req.set_chatbot(chatbot());

    status = itf_client.FindIntent(context, this, itf_req, itf_output);

    if (!status.ok()) {
      // TODO: 상위로 status를 전달하도록 flow를 수정한다.
      LOGGER_debug("ITF Error: {}({})",
                   status.error_message(),
                   status.error_code());
      SetResults(_T(lang(), DIALOG_RESPONSE_INTENT_FINDER_FAILED),
                 DialogResult::INTENT_FINDER_FAILED,
                 ExCode::RUNTIME_ERROR);
      status = GetStackStatus(status,
                              ExCode::RUNTIME_ERROR,
                              X_ERROR_INDEX(ExIndex::X_ITF_ERROR, 10),
                              "Failed to Resolve a skill.")
          .AddDebugInfo(GetDetails())
          .GetStatus();

      return std::make_pair(ProviderStub(), ProviderRes());
    }
    itf_method_type_ = ItfMethodType::FindIntent;
  }

  // 대화 에이전트 풀에서 적절한 에이젠트를 찾는다.
  auto da_finder = DialogAgentInstanceFinder::GetInstance();
  // 원격 서버의 정보를 구해온다.
  auto agent_res = da_finder->FindDialogAgentInstance(*this);
  if (!agent_res) {
    LOGGER_trace("Cannot find DAI for '{}'", skill());
    SetResults(_T(lang(), DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND),
               DialogResult::DIALOG_AGENT_NOT_FOUND,
               ExCode::MEM_KEY_NOT_FOUND);
    return std::make_pair(ProviderStub(), ProviderRes());
  }

  return CreateDAProvider(agent_res, skill());
}

Provider TalkImpl::ResolveSpecifiedSkill(ServerContext *context,
                                         TalkRequest &req,
                                         const string &skill,
                                         bool is_last,
                                         FindIntentResponse &itf_output) {
  LOGGER_debug("ResolveSpecifiedSkill with [{}] is_last: {}", skill, is_last);
  // 대화 에이전트 풀에서 적절한 에이젠트를 찾는다.
  auto da_finder = DialogAgentInstanceFinder::GetInstance();
  shared_ptr<DialogAgentInstanceResource> agent_res;

  if (is_last) {
    const string &last_agent_key = parent_->last_agent_key();

    if (last_agent_key.empty()) {
      SetResults(_T(lang(), DIALOG_RESPONSE_LAST_DIALOG_AGENT_LOST),
                 DialogResult::LAST_DIALOG_AGENT_LOST,
                 ExCode::MEM_KEY_NOT_FOUND);
      return std::make_pair(ProviderStub(), ProviderRes());
    }
    // 원격 서버의 정보를 구해온다.
    agent_res = da_finder->FindDialogAgentInstance(chatbot(),
                                                   skill,
                                                   last_agent_key);
    if (!agent_res) {
      LOGGER_trace("Cannot find agent with {} {}", skill, last_agent_key);
      agent_res = da_finder->FindDialogAgentInstance(chatbot(),
                                                     skill,
                                                     lang());
    }
  } else {
    // 원격 서버의 정보를 구해온다.
    agent_res = da_finder->FindDialogAgentInstance(chatbot(),
                                                   skill,
                                                   lang());
  }

  if (!agent_res) {
    LOGGER_trace("Cannot find agent with {} {}", skill, Lang_Name(lang()));
    SetResults(_T(lang(), DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND),
               DialogResult::DIALOG_AGENT_NOT_FOUND,
               ExCode::MEM_KEY_NOT_FOUND);
    return std::make_pair(ProviderStub(), ProviderRes());
  }

  // 강제적으로 intention classifier와 named entity를 돌린다.
  // 호출하기 전에 skill을 지정한다.
  set_skill(skill);

  // 채널을 생성한다.
  Provider provider = CreateDAProvider(agent_res, skill, !is_last);

  if (provider.second) {
    Analyze(context, req.utter(), itf_output);
  }

  return provider;
}

/**
 * 1차 skill을 제외하고 skill을 분류한다.
 *
 * @param context
 * @param req
 * @param itf_res
 *
 * @return ITF 호출 결과 Status
 */
Status TalkImpl::ResolveSecondarySkill(ServerContext *context,
                                       TalkRequest &req,
                                       vector<string> &exclusions,
                                       RetryFindIntentResponse &itf_res) {
  auto logger = LOGGER();

  // itf variables
  FindIntentRequest itf_req;
  IntentFinderClient itf_client;

  itf_req.mutable_utter()->CopyFrom(req.utter());
  itf_req.mutable_system_context()->CopyFrom(req.system_context());
  itf_req.mutable_session()->CopyFrom(req.session());
  itf_req.set_chatbot(chatbot());

  for (auto &skill : exclusions) {
    itf_req.mutable_exclude_skills()->Add(std::move(skill));
  }

  Status itf_status = itf_client.RetryFindIntent(context,
                                                 parent_->intent_finder_policy(),
                                                 itf_req,
                                                 itf_res);
  if (itf_status.ok()) {
    itf_method_type_ = ItfMethodType::RetryFindIntent;
  } else {
    itf_status = GetStackStatus(itf_status,
                                ExCode::RUNTIME_ERROR,
                                X_ERROR_INDEX(ExIndex::X_ITF_ERROR, 20),
                                "Failed to Resolve a secondary skill.")
        .AddDebugInfo(GetDetails())
        .GetStatus();
  }

  return itf_status;
}

void TalkImpl::Analyze(ServerContext *context,
                       const Utter &utter,
                       FindIntentResponse &itf_output) {
  LOGGER_trace("TalkImpl::Analyze {}", utter.Utf8DebugString());

  auto const &policy = parent_->intent_finder_policy();

  // itf 가 없을 때 현재 skill 세팅
  if (policy == "none" || policy.empty()) {
    itf_output.mutable_skill()->set_skill(skill());
    itf_method_type_ = ItfMethodType::ChatbotDefaultSkill;
    return;
  }

  IntentFinderClient itf_client;
  FindIntentRequest itf_req;

  itf_req.mutable_utter()->CopyFrom(utter);

  Status status = itf_client.Analyze(context, this, itf_req, itf_output);
  if (status.ok()) {
    itf_method_type_ = ItfMethodType::Analyze;
  } else {
    LOGGER_warn("ITF Error: {}", CommonResultStatus::GetErrorString(status));
  }
}

// ELAPSED TIME
void TalkImpl::StartElapsed(timespec &temp) {
  clock_gettime(CLOCK_MONOTONIC, &temp);
}

const long ONE_SEC_NSEC = 1000000000;

void TalkImpl::AddElapsedTime(const string &key, timespec &started) {
  timespec now = {0, 0};
  clock_gettime(CLOCK_MONOTONIC, &now);

  // update elapsed time calc
  long temp_sec = now.tv_sec - started.tv_sec;
  auto temp_nsec = int32_t(now.tv_nsec - started.tv_nsec);
  if (temp_nsec < 0) {
    temp_sec--;
    temp_nsec += ONE_SEC_NSEC;
  }

  google::protobuf::Duration dur;
  dur.set_seconds(temp_sec);
  dur.set_nanos(temp_nsec);
  auto elapsed = mutable_elapsed_time();

  elapsed->insert(time_pair(key, dur));
}

void TalkImpl::Save() {
  try {
    auto hc = hzc::Instance().GetHazelcastClient();
    auto map = hc->getMap<string, TalkImpl>(hzc::kNsTalkV3);

    map.set(parent_->GetTalkKey(seq()), *this);
  } catch (IException &e) {
    LOGGER_critical("TalkImpl::{} Hazelcast: {}", __func__, e.what());
  }
}

void TalkImpl::SaveToEnd() {
  SetTimeToNow(mutable_end());
  Save();
}

void TalkImpl::FillSpeech(Speech *speech,
                          const string &utter,
                          const Lang lang) {
  if (speech == nullptr) {
    LOGGER_warn("FillSpeech: speech is null");
    return;
  }
  if (!Lang_IsValid(lang)) {
    LOGGER_warn("FillSpeech: lang value({}) is invalid .", lang);
    return;
  }
  speech->set_utter(utter);
  speech->set_lang(lang);
}

void TalkImpl::FillErrorSpeech(SpeechResponse *speech,
                               const string &utter,
                               const Lang lang) {
  if (speech == nullptr) {
    LOGGER_warn("FillErrorSpeech: speech is null");
    return;
  }
  FillSpeech(speech->mutable_speech(), utter, lang);
  SetStringMeta(speech->mutable_meta(),
                "talk.error",
                DialogResult_Name(dialog_result()));
}

// UseQuery start 20170907
const shared_ptr<DialogSession> &TalkImpl::GetParent() const {
  return parent_;
}
// UseQuery end

Provider TalkImpl::CreateDAProvider(
    shared_ptr<DialogAgentInstanceResource> &agent_res,
    const string &skill,
    bool set_current) {

  // da_spec v3이 아니면 null stub를 반환한다
  if (agent_res->da_spec() != da_spec::DAP_SPEC_V_3) {
    LOGGER_error("Invalid Dialog Agent Version({}).", agent_res->da_spec());
    return std::make_pair(ProviderStub(), ProviderRes());
  }

  // 원격에 접속할 주소를 만든다.
  const string remote = agent_res->server_ip() + ":"
      + to_string(agent_res->server_port());
  LOGGER_debug("Create Talk Stub [{}/{}]", agent_res->name(), remote);
  // 채널을 생성한다.
  auto channel = grpc::CreateChannel(remote,
                                     grpc::InsecureChannelCredentials());
  if (set_current) {
    parent_->RegisterAgent(skill, agent_res->key());
  }

  const string order("dai" + to_string(dair_flow_.size() + 1));
  ostringstream oss;
  oss << agent_res->chatbot() << "/"
      << skill << "/"
      << remote << "/"
      << Lang_Name((Lang) agent_res->lang()) << "/"
      << agent_res->name() << "/"
      << agent_res->dam_name() << "/"
      << agent_res->launcher() << "/"
      << agent_res->pid();

  dair_flow_.push_back(move(std::make_pair(order, oss.str())));

  return std::make_pair(dap_v3::NewStub(channel), agent_res);
}


