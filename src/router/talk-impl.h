#ifndef M2U_ROUTER_TALK_IMPL_H
#define M2U_ROUTER_TALK_IMPL_H

#include <memory>

#include <google/protobuf/stubs/port.h>
#include <grpc++/grpc++.h>
#include <hazelcast/client/query/SqlPredicate.h>
#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include <libmaum/common/config.h>
#include <maum/m2u/server/pool.pb.h>
#include <maum/m2u/server/simpleclassifier.pb.h>
#include <maum/m2u/server/session.pb.h>
#include <maum/m2u/router/v3/router.pb.h>
#include <maum/m2u/router/v3/session.pb.h>

#include "hazelcast.h"
#include "session-container.h"
#include "external-system-client.h"

using std::string;
using std::shared_ptr;
using std::unique_ptr;

using hazelcast::client::serialization::Serializer;
using hazelcast::client::serialization::ObjectDataOutput;
using hazelcast::client::serialization::ObjectDataInput;
using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

using google::protobuf::int64;
using google::protobuf::Struct;
using grpc::Status;
using grpc::StatusCode;
using grpc::ServerContext;
using grpc::ClientContext;
using grpc::ServerReaderWriter;
using grpc::ServerReader;
using grpc::internal::ReaderInterface;
using grpc::internal::WriterInterface;

using maum::common::Lang;
using maum::m2u::common::Utter;
using maum::m2u::common::Speech;
using maum::m2u::server::DialogAgentInstanceResource;
using maum::m2u::router::v3::SessionTalk;
using maum::m2u::router::v3::OpenRouteRequest;
using maum::m2u::router::v3::TalkRouteRequest;
using maum::m2u::router::v3::TalkRouteResponse;
using maum::m2u::router::v3::TalkRouteSpeechResponse;
using maum::m2u::router::v3::TalkRouteDialogDelegate;
using maum::m2u::router::v3::EventRouteRequest;
using maum::m2u::router::v3::CloseRouteRequest;
using maum::m2u::router::v3::CloseRouteResponse;
using maum::m2u::router::v3::FeedbackRequest;
using maum::m2u::router::v3::FindIntentRequest;
using maum::m2u::router::v3::FindIntentResponse;
using maum::m2u::router::v3::RetryFindIntentResponse;
using maum::m2u::router::v3::FoundIntent;
using maum::m2u::router::v3::DialogResult;
using maum::m2u::router::v3::Replacement;

using maum::m2u::da::v3::OpenSessionRequest;
using maum::m2u::da::v3::OpenSkillRequest;
using maum::m2u::da::v3::TalkRequest;
using maum::m2u::da::v3::EventRequest;
using maum::m2u::da::v3::CloseSkillRequest;
using maum::m2u::da::v3::TalkResponse;
using maum::m2u::da::v3::SkillTransition;
using maum::m2u::da::v3::OpenSkillResponse;
using maum::m2u::da::v3::CloseSkillResponse;
using maum::m2u::da::v3::SpeechResponse;

using ProviderRes = shared_ptr<DialogAgentInstanceResource>;
using ProviderStub = unique_ptr<void>;
using Provider = std::pair<ProviderStub, ProviderRes>;

#define CloseReason_USER_INITIATED CloseSkillRequest::CloseReason::CloseSkillRequest_CloseReason_USER_INITIATED
#define CloseReason_DIALOG_ENDED CloseSkillRequest::CloseReason::CloseSkillRequest_CloseReason_DIALOG_ENDED

class TalkImpl : public hazelcast::client::serialization::Portable,
                 public maum::m2u::router::v3::SessionTalk {
 public:
  TalkImpl() = default;
  TalkImpl(shared_ptr<DialogSession> &parent);
  virtual ~TalkImpl() = default;

  // UseQuery start
  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override;
  int getClassId() const override;
  void writePortable(PortableWriter &out) const override;
  void readPortable(PortableReader &in) override;
  // UseQuery end

  Status OpenSession(ServerContext *context,
                     OpenSessionRequest &req,
                     TalkResponse &res);
  Status Talk(ServerContext *context,
              TalkRequest &req,
              TalkResponse &res);
  Status Event(ServerContext *context,
               EventRequest &req,
               TalkResponse &res);
  Status CloseSkill(ServerContext *context,
                    CloseSkillRequest &req,
                    CloseSkillResponse &res);

  inline const string &chatbot() const {
    return parent_->chatbot();
  };
  inline const string &last_skill() const {
    return parent_->last_skill();
  };
  void AddElapsedTime(const string &key, timespec &started);
  void StartElapsed(timespec &temp);

  void Save();
  void SaveToEnd();
  inline bool Ok() {
    return dialog_result() == DialogResult::OK;
  }

  inline void SetParent(shared_ptr<DialogSession> &parent) {
    parent_ = parent;
  }

 protected:
  enum RequestType {
    OPEN_SESSION = 1,
    TALK = 2,
    EVENT = 3
  };
  Provider GetDialogAgentProvider(const string &chatbot,
                                  const string &skill,
                                  Lang lang,
                                  bool set_current = false);

  Provider GetDialogAgentProvider(const string &chatbot,
                                  const string &skill,
                                  const string &key);

  Provider GetDialogAgentProvider(ServerContext *context,
                                  TalkRequest &req,
                                  Status &status,
                                  FindIntentResponse &itf_output,
                                  bool &new_skill);

  Provider ResolveNewSkill(ServerContext *context,
                           TalkRequest &req,
                           Status &status,
                           FindIntentResponse &itf_output);

  Provider ResolveSpecifiedSkill(ServerContext *context,
                                 TalkRequest &req,
                                 const string &skill,
                                 bool is_last,
                                 FindIntentResponse &itf_output);

  Status ResolveSecondarySkill(ServerContext *context,
                               TalkRequest &req,
                               vector<string> &exclusions,
                               RetryFindIntentResponse &itf_res);

  void Analyze(ServerContext *context,
               const Utter &utter,
               FindIntentResponse &itf_output);

  void MakeRequest(TalkRequest &req);
  inline void UpdateSessionContext(const Session &session_update) {
    parent_->UpdateSessionContext(session_update);
  }
  void UpdateSessionContext(const TalkResponse &res);
  inline void SetSessionContext(Session *session, const Struct &context) {
    session->mutable_context()->CopyFrom(parent_->context());
  }
  inline void SetWantDebugMeta(const Struct &in_meta) {
    auto it = in_meta.fields().find("debug");
    want_debug_meta_ = it != in_meta.fields().cend() ?
                       it->second.bool_value() :
                       false;
  }
  void SetMetas(Struct *meta);
  inline vector<string> GetDetails() {
    vector<string> details;
    for (auto &dair : dair_flow_) {
      details.push_back(move(string(dair.first + ": " + dair.second)));
    }
    return details;
  }
  inline void SetTimeToNow(google::protobuf::Timestamp *time) {
    timespec now = {0, 0};
    clock_gettime(CLOCK_REALTIME, &now);
    time->set_seconds(now.tv_sec);
    time->set_nanos(int32_t(now.tv_nsec));
  }
  inline void SetInUtter(const Utter &utter) {
    set_in(utter.utter());
    set_input_type(utter.input_type());
    set_lang(utter.lang());
    mutable_input_meta()->CopyFrom(utter.meta());
  }
  inline void SetOutSpeech(const Speech &speech) {
    set_out(speech.utter());
    set_speech_out(speech.speech_utter());
    set_reprompt(speech.reprompt());
  }
  inline void SetResults(const string &text, DialogResult result, int status) {
    set_out(text);
    set_dialog_result(result);
    set_status_code(status);
  }

  void CloseSkillByResponse(const ServerContext *context,
                            unique_ptr<dap_v3::Stub> &stub,
                            Session *session,
                            const SystemContext &system_context,
                            bool invalidate);

  Status SetTransitionNotAllowed(TalkResponse &res, int error_index);

  void SetReplacementMeta(const Replacement *replacement, Utter *utter);

  void RecordTalk(const string skill, Lang lang, const TalkResponse &rsp);

  Status OpenSkill(const ServerContext *context,
                   unique_ptr<dap_v3::Stub> &stub,
                   Session *session,
                   const SystemContext &system_context,
                   OpenSkillResponse &res);
  void CloseSkill(const ServerContext *context,
                  unique_ptr<dap_v3::Stub> &stub,
                  const Session &session,
                  const SystemContext &system_context);
  Status CloseSkill(const ServerContext *context,
                    unique_ptr<dap_v3::Stub> &stub,
                    const CloseSkillRequest &req,
                    CloseSkillResponse &res);
  inline void PushSkill() {
    parent_->set_stacked_skill(skill());
    parent_->set_stacked_agent_key(parent_->last_agent_key());
    LOGGER_debug(">> Push: skill [{}]", skill());
  }
  inline void PopSkill() {
    parent_->set_last_skill(parent_->stacked_skill());
    parent_->set_last_agent_key(parent_->stacked_agent_key());
    parent_->clear_stacked_skill();
    parent_->clear_stacked_agent_key();
    LOGGER_debug("<< Pop: skill [{}]", parent_->last_skill());
  }
  inline void ReleaseStack() {
    parent_->clear_stacked_skill();
    parent_->clear_stacked_agent_key();
    LOGGER_debug("-- Release stack: skill [{}]", skill());
  }
  inline bool HasStackedSkill() {
    return !parent_->stacked_skill().empty();
  }
  Status CallWithTransitedSkill(ServerContext *context,
                                const SystemContext &system_context,
                                Session *session,
                                unique_ptr<dap_v3::Stub> &dap_stub,
                                Message &req,
                                RequestType req_type,
                                TalkResponse &res,
                                const SkillTransition &transition,
                                bool change_skill);
  Status PerformSkillTransit(ServerContext *context,
                             unique_ptr<dap_v3::Stub> &dap_stub,
                             Message &req,
                             RequestType req_type,
                             TalkResponse &res);
  Provider CreateDAProvider(shared_ptr<DialogAgentInstanceResource> &agent_res,
                            const string &skill,
                            bool set_current = true);
  shared_ptr<DialogSession> parent_;
  shared_ptr<DialogSessionContainer> container_;
  shared_ptr<ExternalSystemClient> external_system_client_;
  int itf_method_type_;
  int goto_depth_;
  bool skill_closed_;
  bool want_debug_meta_;
  vector<std::pair<string, string>> dair_flow_;

 public:
  static void FillSpeech(Speech *speech,
                         const string &utter,
                         const Lang lang);

  void FillErrorSpeech(SpeechResponse *speech,
                       const string &utter,
                       const Lang lang);
  const shared_ptr<DialogSession> &GetParent() const;

  friend class ExternalSystemClient;
};

class TalkElapsedTimer {
 public:
  TalkElapsedTimer(const string &key, TalkImpl *talk)
      : key_(key), talk_(talk), timer_({0, 0}) {
    talk_->StartElapsed(timer_);
  }
  virtual ~TalkElapsedTimer() {
    talk_->AddElapsedTime(key_, timer_);
  }
 private:
  string key_;
  TalkImpl *talk_;
  timespec timer_;
};

#endif // M2U_ROUTER_TALK_IMPL_H
