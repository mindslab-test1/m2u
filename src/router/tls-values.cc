#include "tls-values.h"

thread_local string tls_op_sync_id;
thread_local string tls_device_id;
thread_local string tls_session_id("unassigned");
