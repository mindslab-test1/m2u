#include "get-text.h"

using namespace std;

inline int32_t K(TextRes tr) {
  return (Lang::ko_KR << 24) | (int(tr) & 0xFFFFFF);
}

inline int32_t E(TextRes tr) {
  return (Lang::en_US << 24) | (int(tr) & 0xFFFFFF);
}

struct {
  TextRes code;
  const char *k_msg;
  const char *e_msg;
} g_res[RES_MAX] =
    {
        // 0
        {
            DIALOG_RESPONSE_OK,
            "",
            ""
        },

        // 101
        {
            DIALOG_RESPONSE_USER_INVALIDATED,
            "사용자가 적절하지 않습니다.",
            "The user is not valid."
        },
        // 102
        {
            DIALOG_RESPONSE_CHATBOT_NOT_FOUND,
            "지정한 챗봇이 없습니다. 챗봇을 등록하거나 챗봇이름을 확인해 주세요.",
            "The chatbot is not exist. Please register it or check out the name of chatbot."
        },
        // 103
        {
            DIALOG_RESPONSE_SESSION_NOT_FOUND,
            "이전 대화가 종료되었습니다. 새로운 대화를 시작해주세요.",
            "The last conversation has been closed! Please start a new conversation."
        },
        // 104
        {
            DIALOG_RESPONSE_SESSION_INVALIDATED,
            "이전 대화가 종료되었습니다. 새로운 대화를 시작해주세요.",
            "The last conversation has been closed! Please start a new conversation."
        },

        // 201
        {
            DIALOG_RESPONSE_EMPTY_IN,
            "안녕하세요. 대화를 입력해주세요.",
            "Hello! Please send me a message"
        },


        // 301
        {
            DIALOG_RESPONSE_INTENT_FINDER_FAILED,
            "현재 지원되지 않는 기능입니다. 앞으로 더 많은 기능이 추가될 예정이니 기대해주세요.",
            "The service is currently not available. We will be adding more services. Please visit us again."
        },
        // 302
        {
            DIALOG_RESPONSE_DIALOG_AGENT_NOT_FOUND,
            "현재 지원되지 않는 기능입니다. 앞으로 더 많은 기능이 추가될 예정이니 기대해주세요.",
            "The service is currently not available. We will be adding more services. Please visit us again."
        },
        // 303
        {
            DIALOG_RESPONSE_DIALOG_AGENT_NOT_READY,
            "현재 지원되지 않는 기능입니다. 앞으로 더 많은 기능이 추가될 예정이니 기대해주세요.",
            "The service is currently not available. We will be adding more services. Please visit us again."
        },
        // 304
        {
            DIALOG_RESPONSE_LAST_DIALOG_AGENT_LOST,
            "",
            ""
        },

        // 311
        {
            DIALOG_RESPONSE_SECONDARY_DIALOG_CONFLICT,
            "",
            ""
        },
        // 312
        {
            DIALOG_RESPONSE_LAST_AGENT_NOT_FOUND,
            "기존 대화를 발견할 수 없습니다. 관리자에게 문의하세요.",
            "Oops, I can't track the last conversation. Please report this to the administrator."
        },
        // 313
        {
            DIALOG_RESPONSE_DUPLICATED_SKILL_RESOLVED,
            "죄송합니다. 이전 대화를 불러올 수 없습니다. 관리자에게 문의해주세요",
            "Sorry, i can't retrieve the last conversation. Please report this matter to the administrator."
        },
        // 314
        {
            DIALOG_RESPONSE_SECONDARY_DIALOG_AGENT_IS_MULTI_TURN,
            "이전 대화가 아직 진행 중입니다. 이전 대화를 먼저 마쳐주세요.",
            "The last conversation has not finished yet. Please finish the current conversation first."
        },

        // 500
        {
            DIALOG_RESPONSE_INTERNAL_ERROR,
            "원하시는 답변을 찾지 못했어요. 다시 말씀해주시겠어요?",
            "Sorry, I couldn't find the answer to your question. Could you ask me again?"
        }
    };

std::mutex SimpleGetText::resource_mutex_;
bool SimpleGetText::initialized_ = false;
unordered_map<int32_t, const char *> SimpleGetText::resources_;

void SimpleGetText::Init() {
  lock_guard<mutex> lock(resource_mutex_);
  if (initialized_) {
    return;
  }
  for (auto &r : g_res) {
    resources_.insert(make_pair(K(r.code), r.k_msg));
    resources_.insert(make_pair(E(r.code), r.e_msg));
  }
  initialized_ = true;
}
