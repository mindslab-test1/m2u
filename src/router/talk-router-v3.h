#ifndef M2U_ROUTER_TALK_ROUTER_H
#define M2U_ROUTER_TALK_ROUTER_H

#include <atomic>
#include <memory>
#include <string>
#include <mutex>
#include <sys/time.h>
#include <grpc++/grpc++.h>
#include <google/protobuf/util/json_util.h>
#include <libmaum/common/config.h>

#include "util.h"
#include "intent-finder-client.h"
#include "session.h"
#include "session-container.h"

using std::string;
using std::atomic;
using std::shared_ptr;
using std::unique_ptr;
using std::make_shared;
using std::mutex;

using namespace google::protobuf;
using google::protobuf::Map;
using google::protobuf::MapPair;
using google::protobuf::Value;
using google::protobuf::Empty;

using grpc::Status;
using grpc::StatusCode;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::WriteOptions;

using maum::common::Lang;
using maum::m2u::common::DelegateDirective;
using maum::m2u::common::Speech;
using maum::m2u::common::SystemContext;

using string_pair = Map<string, string>::value_type;
using meta_pair = Map<string, Value>::value_type;
using struct_pair = Map<string, Struct>::value_type;

class TalkRouterV3 {
  TalkRouterV3();
 public:
  static TalkRouterV3 *GetInstance();
  virtual ~TalkRouterV3() = default;

  Status OpenRoute(ServerContext *context,
                   shared_ptr<DialogSession> &session,
                   const OpenRouteRequest *req,
                   ServerWriter<TalkRouteResponse> *writer);

  Status TalkRoute(ServerContext *context,
                   shared_ptr<DialogSession> &session,
                   const TalkRouteRequest *req,
                   ServerWriter<TalkRouteResponse> *writer);

  Status EventRoute(ServerContext *context,
                    shared_ptr<DialogSession> &session,
                    const EventRouteRequest *req,
                    ServerWriter<TalkRouteResponse> *writer);

  Status CloseRoute(ServerContext *context,
                    shared_ptr<DialogSession> &session,
                    const CloseRouteRequest *req,
                    CloseRouteResponse *res);
  // 대화에 대한 피드백을 보냅니다.
  Status Feedback(ServerContext *context,
                  shared_ptr<DialogSession> &session,
                  const FeedbackRequest *req);

  Status IsSessionValid(shared_ptr<DialogSession> &session);

 private:
  // singleton
  static mutex mutex_;
  static atomic<TalkRouterV3 *> instance_;
  DialogSessionContainer *container_;

  void WriteTalkResponse(ServerWriter<TalkRouteResponse> *writer,
                         const TalkResponse &da_response,
                         const TalkImpl &talk);
  void WriteErrorResponse(ServerWriter<TalkRouteResponse> *writer,
                          const TalkResponse &da_response,
                          const Status &status);
};

#endif // M2U_ROUTER_TALK_ROUTER_H
