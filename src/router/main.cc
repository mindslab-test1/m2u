#include <iostream>
#include <csignal>
#include <syslog.h>
#include <dlfcn.h>
#include <getopt.h>
#include <gitversion/version.h>
#include <grpc++/grpc++.h>

#include <libmaum/common/util.h>
#include <libmaum/common/config.h>
#include <libmaum/rpc/result-status.h>
#include <libmaum/log/call-log.h>

#include "hazelcast.h"
#include "dialog-agent-instance-finder.h"
#include "talk-router-v3-impl.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using libmaum::rpc::CommonResultStatus;

std::mutex g_sigterm_mutex;

void ExitSignalHandler(int signo) {
  static int sig_count = 0;
  sigset_t full;
  char hostname[HOST_NAME_MAX];

  sigfillset(&full);
  sigprocmask(SIG_BLOCK, &full, nullptr);

  gethostname(hostname, sizeof(hostname));
  auto program = libmaum::Config::Instance().GetProgramPath();

  {
    std::unique_lock<std::mutex> lock(g_sigterm_mutex);
    if (sig_count) {
      std::this_thread::sleep_for(std::chrono::microseconds(1));
      exit(0);
    }
    auto logger = LOGGER();
    logger->info("signal {} {} hostname: {}", signo, time(nullptr), hostname);
    syslog(LOG_INFO, "%s received SIGNAL %d", program.c_str(), signo);
    sig_count = 1;
  }

  exit(0);
}

void RunServer() {
  auto logger = LOGGER();
  libmaum::Config &c = libmaum::Config::Instance();
  string server_address(c.Get("routed.listen")); // default port =s 9300
  long grpc_timeout = c.GetAsInt("routed.grpc.timeout");

  SetGrpcDeadline(c.GetDefaultAsInt("routed.grpc.deadline", "0"));

  srand((unsigned int) time(nullptr));

  TalkRouterV3Impl talk_router;

#if 1
  if (grpc_timeout == 0)
    grpc_timeout = INT_MAX;
#endif
  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
  builder.RegisterService(&talk_router);

  unique_ptr<Server> server(builder.BuildAndStart());

  if (server) {
    logger->info("Server listening on {}", server_address);
    c.DumpPid();
    server->Wait();
  }
}

void help(char *argv) {
  printf("%s [--version] [--help]\n", basename(argv));
}

void process_option(int argc, char *argv[]) {
  bool do_exit = false;
  int c;
  while (true) {
    static const struct option long_options[] = {
        {"version", no_argument, nullptr, 'v'},
        {"help", no_argument, nullptr, 'h'},
        {nullptr, no_argument, nullptr, 0}
    };

    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long(argc, argv, "vh?", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
      case 0:break;
      case 'v':
        printf("%s version %s\n",
               basename(argv[0]),
               version::VERSION_STRING);
        do_exit = true;
        break;
      case 'h':
      case '?':
      default:help(argv[0]);
        do_exit = true;
        break;
    }
  }
  if (do_exit)
    exit(EXIT_SUCCESS);
}

void LoadPlugins() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  auto plugin_count = c.GetDefaultAsInt("routed.plugin.count", "0");
  for (auto i = 0; i < plugin_count; i++) {
    string so_name = c.Get("routed.plugin." + to_string(i + 1) + ".so");
    string entry = c.Get("routed.plugin." + to_string(i + 1) + ".entrypoint");

    if (so_name.empty() || entry.empty()) {
      logger->warn("invalid so {} or entry {} ", so_name, entry);
      continue;
    }

    void *ptr = dlopen(so_name.c_str(), RTLD_NOW);
    if (ptr == nullptr) {
      logger->warn("cannot load shared object {}", so_name);
      continue;
    }
    void (*sym)();
    sym = reinterpret_cast<void (*)()>(dlsym(ptr, entry.c_str()));
    if (sym == nullptr) {
      logger->warn("cannot load sym {}", entry);
    }
    logger->info("calling {} {}", so_name, entry);
    sym();
  }
}

int main(int argc, char *argv[]) {
  process_option(argc, argv);
  libmaum::Config::Init(argc, argv, "m2u.conf");

  // 공통 에러코드 모듈, 프로세스 설정
  CommonResultStatus::SetModuleAndProcess(maum::rpc::Module::M2U,
                                          maum::rpc::ProcessOfM2u::M2U_ROUTER);

  auto logger = LOGGER();

  signal(SIGTERM, ExitSignalHandler);
  //signal(SIGSEGV, ExitSignalHandler);
  //signal(SIGABRT, ExitSignalHandler);
  signal(SIGINT, ExitSignalHandler);

  logger->info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
  logger->info("    STARTING M2U Router [PID:{}]", getpid());
  logger->info("   {}", version::VERSION_STRING);
  logger->info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

  libmaum::log::CallLogger::GetInstance();

  LoadPlugins();

  // Check Hazelcast Server
  if (!GlobalHazelcastClient::IsReady()) {
    logger->critical("************************************");
    logger->critical("*  Hazelcast Server is not Ready.  *");
    logger->critical("*  Exit Router.                    *");
    logger->critical("************************************");
    logger->flush();
    return -1;
  }

  if (!IsPoolReady()) {
    logger->critical("********************************");
    logger->critical("*  Pool Service is not Ready.  *");
    logger->critical("*  Exit Router.                *");
    logger->critical("********************************");
    logger->flush();
    return -1;
  }

  EnableCoreDump();
  RunServer();
  return 0;
}
