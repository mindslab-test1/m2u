#include <fstream>
#include <grpc++/grpc++.h>
#include <maum/brain/cl/classifier.grpc.pb.h>

#include "util.h"
#include "tls-values.h"
#include "dialog-agent-instance-finder.h"
#include "talk-impl.h"

using namespace std;
using std::map;

using hazelcast::client::IMap;
using hazelcast::client::MultiMap;
using hazelcast::client::query::SqlPredicate;

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::StatusCode;
using google::protobuf::Empty;

using maum::brain::cl::ClassifierResolver;
using maum::brain::cl::Model;
using maum::brain::cl::ServerStatus;
using maum::common::Lang_Name;
using maum::m2u::server::ChatbotClassifier;
using maum::m2u::server::DialogAgentInstancePool;

using maum::m2u::da::DialogAgentState;
using maum::m2u::da::DialogAgentStatus;
using maum::m2u::da::DialogAgentProviderSpec;

using dap_v3 = maum::m2u::da::v3::DialogAgentProvider;
using DAIRPortable = DialogAgentInstanceResourcePortable;
using DAIRSkillPortable = DialogAgentInstanceResourceSkillPortable;

static inline string MergeSkills(const DialogAgentInstanceResource &dai) {
  string skills;
  for (const auto &sk : dai.skills()) {
    skills += sk;
    skills += ' ';
  }
  return skills;
}

atomic<DialogAgentInstanceFinder *> DialogAgentInstanceFinder::instance_;
mutex DialogAgentInstanceFinder::mutex_;


/**
 * Dialog Agent Instance Resource Skill Portable class Implementations
 *
 */
// virtual method override for public hazelcast::client::serialization::Portable
int DialogAgentInstanceResourceSkillPortable::getFactoryId() const {
  return PORTABLE_FACTORY_ID;
}

// virtual method override for public hazelcast::client::serialization::Portable
int DialogAgentInstanceResourceSkillPortable::getClassId() const {
  return DIALOG_AGENT_INSTANCE_RESOURCE_SKILL;
}

// virtual method override for public hazelcast::client::serialization::Portable
void DialogAgentInstanceResourceSkillPortable::writePortable(
    hazelcast::client::serialization::PortableWriter &out) const {
  int serializeSize = 0;
  serializeSize = this->ByteSize();
  char bArray[serializeSize];
  SerializeToArray(bArray, serializeSize);
  std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

  out.writeUTF("dair_key", &this->dair_key());
  out.writeUTF("chatbot", &this->chatbot());
  out.writeUTF("skill", &this->skill());
  out.writeInt("lang", this->lang());
  out.writeByteArray("_msg", &data);
}

// virtual method override for public hazelcast::client::serialization::Portable
void DialogAgentInstanceResourceSkillPortable::readPortable(
    hazelcast::client::serialization::PortableReader &in) {
  std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
  ParseFromArray(vec.data(), (int) vec.size());
}


/**
 * Dialog Agent Instance Resource Portable class Implementations
 *
 */
// virtual method override for public hazelcast::client::serialization::Portable
int DialogAgentInstanceResourcePortable::getFactoryId() const {
  return PORTABLE_FACTORY_ID;
}

// virtual method override for public hazelcast::client::serialization::Portable
int DialogAgentInstanceResourcePortable::getClassId() const {
  return DIALOG_AGENT_INSTANCE_RESOURCE;
}

// virtual method override for public hazelcast::client::serialization::Portable
void DialogAgentInstanceResourcePortable::writePortable(
    hazelcast::client::serialization::PortableWriter &out) const {
  int serializeSize = 0;
  serializeSize = this->ByteSize();
  char bArray[serializeSize];
  this->SerializeToArray(bArray, serializeSize);
  std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

  out.writeUTF("key", &this->key());
  out.writeUTF("name", &this->name());
  out.writeUTF("chatbot", &this->chatbot());
  out.writeInt("lang", this->lang());
  out.writeUTF("server_ip", &this->server_ip());
  out.writeInt("server_port", this->server_port());
  out.writeInt("launch_type", this->launch_type());
  out.writeUTF("launcher", &this->launcher());
  out.writeInt("pid", this->pid());
  out.writeLong("started_at", this->started_at().seconds());
  out.writeByteArray("_msg", &data);
}

// virtual method override for public hazelcast::client::serialization::Portable
void DialogAgentInstanceResourcePortable::readPortable(
    hazelcast::client::serialization::PortableReader &in) {
  std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
  ParseFromArray(vec.data(), (int) vec.size());
}

DialogAgentInstanceFinder *DialogAgentInstanceFinder::GetInstance() {
  DialogAgentInstanceFinder *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new DialogAgentInstanceFinder();
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

void DialogAgentInstanceFinder::RemoveDialogAgentInstance(const string &key) {
  libmaum::Config &c = libmaum::Config::Instance();
  string admin_remote = c.Get("pool.export");
  auto channel = grpc::CreateChannel(admin_remote,
                                     grpc::InsecureChannelCredentials());
  auto stub = DialogAgentInstancePool::NewStub(channel);
  DialogAgentInstanceKey da_key;
  da_key.set_key(key);
  DialogAgentInstanceStat stat;
  Status st = stub->Unregister(GetClientContext(nullptr).get(), da_key, &stat);

  if (st.ok()) {
    LOGGER_info("Removed DA Instance {}", key);
  } else {
    LOGGER_info("failed to remove DA Instance {}, error: {}, {} {}",
                key, st.error_code(),
                st.error_message(), st.error_details());
  }
}

bool DialogAgentInstanceFinder::IsDialogAgentInstanceRunning(
    shared_ptr<DialogAgentInstanceResource> res) {
  auto logger = LOGGER();
  // 원격에 접속할 메시지를 만든다.
  const string remote = GetRemoteTarget(res->server_ip(), res->server_port());

  logger_debug("Create talk stub [{}/{}] for IsReady", res->name(), remote);

  // 채널을 생성한다.
  google::protobuf::Empty empty;
  DialogAgentStatus agentStatus;
  Status status;

  auto channel = grpc::CreateChannel(remote,
                                     grpc::InsecureChannelCredentials());

  if (res->da_spec() == DialogAgentProviderSpec::DAP_SPEC_V_3) {
    auto stub = dap_v3::NewStub(channel);
    status = stub->IsReady(GetClientContext(nullptr).get(),
                           empty,
                           &agentStatus);
    if (status.error_code() == grpc::UNAVAILABLE) {
      logger_info("NOT RUNNING {}:{}/{}/{},{},{}/{},{}",
                  res->server_ip(), res->server_port(),
                  res->key(), res->chatbot(), MergeSkills(*res),
                  res->lang(), res->name(), res->version());
      RemoveDialogAgentInstance(res->key());
      return false;
    }
  } else {
    logger_info("Unsupported DA version({}).", res->da_spec());
    RemoveDialogAgentInstance(res->key());
    return false;
  }

  DialogAgentState state = agentStatus.state();

  if (state != DialogAgentState::DIAG_STATE_RUNNING) {
    logger_debug("DAI State: {}({})", DialogAgentState_Name(state), state);
    if (state == DialogAgentState::DIAG_STATE_UNUSED) {
      logger_warn("DAI is in Abnormal state.");
      return false;
    }
    if (state == DialogAgentState::DIAG_STATE_TERMINATED) {
      logger_warn("DAI is Terminated.");
      RemoveDialogAgentInstance(res->key());
      return false;
    }
  }

  return true;
}

/**
 * 먼저 스킬이 일치하면 일치된 것으로 반환한다.
 *
 * 하나의 스킬에는 여러 개의 DA가 존재할 수 있다. 하나 이상의 DA가 존재하게 되면
 * 그중에서 같은 언어로 동작하는 DA들을 골라낸 다음에 개수만큼 나눠서 Round
 * Robin 방식으로 DA를 선택한다.
 *
 * 선택된 결과에 대한 대화 에이전트 리소스를 반환한다.
 *
 * @param talk 현재 대화 객체 여기에서 식별된 챗봇, 언어, 스킬을 이용해서
 * 적절한 Dialog Agent를 구해온다.
 * @return 구해진 DialogAgentResouce에 대한 shared pointer를 반환한다.
 */
shared_ptr<DialogAgentInstanceResource>
DialogAgentInstanceFinder::FindDialogAgentInstance(const TalkImpl &talk) {

  LOGGER_debug("finding DA for chatbot: {}, skill: {}, lang: {}",
               talk.chatbot(), talk.skill(), talk.lang());

  return FindDialogAgentInstance(talk.chatbot(), talk.skill(), talk.lang());
}

/**
 * 대화 에이전트를 구한다.
 *
 * TODO, 위 함수와 동일한다. 하나는 인라인으로 만들어도 됩니다. TODO
 *
 * @param chatbot 챗봇
 * @param skill 스킬
 * @param langCode 언어
 * @return 대화 에이전트 리소스에 대한 공유 포인터를 반환한다.
 */
shared_ptr<DialogAgentInstanceResource>
DialogAgentInstanceFinder::FindDialogAgentInstance(
    const string &chatbot,
    const string &skill,
    Lang lang) {
  auto logger = LOGGER();

  try {
    auto hc = hzc::Instance().GetHazelcastClient();
    vector<DAIRSkillPortable> rs;

    {
      auto map = hc->getMap<string, DAIRSkillPortable>(hzc::kNsDAIRSkill);

      // composit query
      ostringstream query;
      query << "chatbot='" << chatbot << "' and skill='" << skill
            << "' and lang=" << lang;

      // run query
      rs = map.values(SqlPredicate(query.str()));
      logger_trace("at chatbot {}, skill {}, found DA resources {}",
                   chatbot, skill, rs.size());

      if (rs.empty()) {
        logger_debug("DA Instance not found {} {} {}",
                     chatbot,
                     skill,
                     Lang_Name(lang));
        return shared_ptr<DialogAgentInstanceResource>(nullptr);
      }
    }

    // DA가 여러개일 경우 Load Balancing Logic 구현. 현재는 Random.
    // 나중에는 여러가지 LB 옵션이 추가되어야 할 것으로 보임.
    shared_ptr<DialogAgentInstanceResource> selected = nullptr;
    decltype(rs.size()) max = rs.size();
    decltype(rs.size()) index = -1;
    if (rs.size() == 1) {
      // 선택된 DA
      auto dair_map = hc->getMap<string, DAIRPortable>(hzc::kNsDAIR);
      auto dai = dair_map.get(rs[0].dair_key());
      selected = dai == nullptr ?
                 nullptr :
                 std::make_shared<DialogAgentInstanceResource>(*dai);

      if (selected != nullptr) {
        // 살아있으면 index 설정, 죽어 있으면 없다고 null 설정
        if (IsDialogAgentInstanceRunning(selected)) {
          index = 0;
        } else {
          selected = nullptr;
        }
      }
    } else if (rs.size() > 1) {
      // 랜덤하게 index 선택
      unsigned idx = (unsigned) (rand() % rs.size());

      // 선택한 index 이후로는 순차적으로 access
      // 예를 들어, DA가 5개인 상황에서 index 3이 랜덤하게 선택될 경우,
      // tempList는 {3,4,0,1,2}로 만들어둔다.
      // 이 tempList는 선택된 3번 DA가 죽었을 경우 선택될 후보 DA list 이다.
      unsigned tempList[rs.size()];
      for (decltype(max) i = 0; i < max; ++i, ++idx) {
        tempList[i] = unsigned((idx + rs.size()) % rs.size());
      }

      auto dair_map = hc->getMap<string, DAIRPortable>(hzc::kNsDAIR);
      for (auto &x : tempList) {
        // 선택된 DA
        auto dai = dair_map.get(rs[x].dair_key());
        selected = dai == nullptr ?
                   nullptr :
                   std::make_shared<DialogAgentInstanceResource>(*dai);

        if (selected == nullptr) {
          continue;
        }

        // 여기 IsDialogAgentRunning서 GRPC 호출해서 살아있는지 확인한다.
        if (IsDialogAgentInstanceRunning(selected)) {
          index = x;
          break;
        } else {
          selected = nullptr;
        }
      }
    }

    // RETURN SELECTED DA
    if (selected) {
      logger_debug("found DAI stub = {}:{}[{}]/{}/[{}]/{},{},{} {}:{},{}",
                   selected->server_ip(), selected->server_port(),
                   selected->pid(),
                   selected->chatbot(),
                   MergeSkills(*selected),
                   Lang_Name((Lang) selected->lang()),
                   selected->name(),
                   selected->version(),
                   index, max,
                   selected->key());
    } else {
      // (다 죽어 있어서) 살아있는 DA가 없는 경우
      logger_warn("DAI stub not found {} {} {}",
                  chatbot,
                  skill,
                  Lang_Name(lang));
    }

    return selected;
  } catch (IException &e) {
    logger_critical("DialogAgentInstanceFinder::{} Hazelcast: {}",
                    __func__, e.what());
    return shared_ptr<DialogAgentInstanceResource>(nullptr);
  }
}

/**
 * 멀티턴 환경에서 대화가 계속 이어지는 경우에 기존의 DiaogAgentResource 를
 * 구해온다.
 *
 * 기존 대화 에이전트는 KEY 형태로 메모리에 유지된다.
 * 메모리에 유지되는 정보는 서로 다른 프론트 서버를 통해서도
 * 처리가 가능합니다.
 *
 * @param chatbot DA 리소스 검색을 위한 챗봇, 검토 목적, 로그 목적
 * @param skill DA 리소스 검색을 위한 스킬, 검토 목적, 로그 목적
 * @param key 찾으려는 DA 리소스
 * @return 검색된 DA 리소스를 반환한다.
 */
shared_ptr<DialogAgentInstanceResource>
DialogAgentInstanceFinder::FindDialogAgentInstance(
    const string &chatbot, const string &skill, const string &key) {
  auto logger = LOGGER();
  try {
    auto hc = hzc::Instance().GetHazelcastClient();
    logger_debug("DA Pool: FindDialogAgentInstance with key [{}]", key);
    auto agent_map = hc->getMap<string, DAIRPortable>(hzc::kNsDAIR);
    auto res = agent_map.get(key);
    if (res) {
      auto found = std::make_shared<DialogAgentInstanceResource>(*res);
      if (IsDialogAgentInstanceRunning(found)) {
        return found;
      } else {
        logger_warn("oops, last DA not running {} {} {}", chatbot, skill, key);
        return shared_ptr<DialogAgentInstanceResource>(nullptr);
      }
    } else {
      logger_warn("oops, last DA not found {} {} {}", chatbot, skill, key);
      return shared_ptr<DialogAgentInstanceResource>(nullptr);
    }
  } catch (IException &e) {
    logger_critical("DialogAgentInstanceFinder::{} Hazelcast: {}",
                    __func__,
                    e.what());
    return shared_ptr<DialogAgentInstanceResource>(nullptr);
  }
}
