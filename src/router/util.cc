#include <maum/m2u/server/pool.grpc.pb.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

#include "util.h"

using google::protobuf::Map;
using google::protobuf::Message;
using google::protobuf::Empty;
using grpc::Status;
using grpc::CreateChannel;
using grpc::InsecureChannelCredentials;
using maum::m2u::server::PoolState;
using maum::m2u::server::DialogAgentInstancePool;

using meta_pair = Map<std::string, Value>::value_type;

// GRPC client call deadline.
long grpc_call_deadline = 0;
const google::protobuf::util::JsonOptions json_format;

void SetGrpcDeadline(long deadline) {
  grpc_call_deadline = deadline * 1000;
}

void SetStringMeta(Struct *meta, const string &key, const string &value) {
  if (meta == nullptr)
    return;

  auto fields = meta->mutable_fields();

  Value v;
  v.set_string_value(value);
  fields->insert(meta_pair(key, v));
}

void SetNumberMeta(Struct *meta, const string &key, const double &number) {
  if (meta == nullptr)
    return;

  auto fields = meta->mutable_fields();

  Value v;
  v.set_number_value(number);

  fields->insert(meta_pair(key, v));
}

void SetBoolMeta(Struct *meta, const string &key, const bool &value) {
  if (meta == nullptr)
    return;

  auto fields = meta->mutable_fields();

  Value v;
  v.set_bool_value(value);

  fields->insert(meta_pair(key, v));
}

unique_ptr<ClientContext> GetClientContext(const ServerContext *context,
                                           long call_deadline) {
  using namespace std::chrono;

  unique_ptr<ClientContext> client_context(new ClientContext());

  if (call_deadline > 0) {
    auto deadline = system_clock::now() + milliseconds(call_deadline);
    client_context->set_deadline(deadline);
  } else if (grpc_call_deadline > 0) {
    auto deadline = system_clock::now() + milliseconds(grpc_call_deadline);
    client_context->set_deadline(deadline);
  }

  if (context != nullptr) {
    auto const &client_map = context->client_metadata();
    auto item_s = client_map.find("x-operation-sync-id");

    if (item_s != client_map.end()) {
      client_context->AddMetadata(
          "x-operation-sync-id",
          string(item_s->second.begin(), item_s->second.end()));
    }
  }

  return client_context;
}

/**
 * Router 기동 전에 Pool Service가 준비되었는지 확인한다.
 *
 * @return
 */
bool IsPoolReady() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  const int waits[] = {5, 5, 20, 0};
  int wait_seconds = 0;

  logger->debug("Check Pool is Ready...");

  const string pool_remote = c.Get("pool.export");
  if (pool_remote.empty()) {
    logger->warn("Pool remote is not set.");
    return false;
  }

  for (int i = 0; i < 4; ++i) {
    auto channel = CreateChannel(pool_remote, InsecureChannelCredentials());
    auto pool = DialogAgentInstancePool::NewStub(channel);

    ClientContext ctx;
    Empty empty;
    PoolState state;

    Status status = pool->IsReady(&ctx, empty, &state);

    if (status.ok() && state.is_ready()) {
      return true;
    } else {
      logger->debug("Pool Error: {}, {}:{}",
                    status.error_code(),
                    status.error_message(),
                    status.error_details());
      wait_seconds += waits[i];
      logger->debug(" Wait more to {}s.", wait_seconds);
    }
    std::this_thread::sleep_for(std::chrono::seconds(waits[i]));
  }

  return false;
}
