#include <ctime>
#include <libmaum/common/base64.h>

#include "util.h"
#include "tls-values.h"
#include "session.h"
#include "intent-finder-client.h"
#include "dialog-agent-instance-finder.h"

using hzc = GlobalHazelcastClient;

using google::protobuf::Map;
using str_pair = std::map<string, string>::value_type;

using maum::m2u::da::TalkKey;
using maum::m2u::da::TalkStat;
using maum::m2u::da::DialogSessionState;

#define TransionReason_USER_UTTER TalkAgent::TransionReason::TalkAgent_TransionReason_USER_UTTER
#define TransionReason_SKILL_TRANS_DISCARD TalkAgent::TransionReason::TalkAgent_TransionReason_SKILL_TRANS_DISCARD

DialogSession::DialogSession(int64 id) {
  set_id(id);
  set_valid(true);

  auto started_at = mutable_started_at();
  timespec now = {0, 0};
  clock_gettime(CLOCK_REALTIME, &now);
  started_at->set_seconds(now.tv_sec);
  started_at->set_nanos((int32_t) now.tv_nsec);

  auto accumulated_time = mutable_accumulated_time();
  accumulated_time->set_seconds(0);
  accumulated_time->set_nanos(0);
  auto last_talked_at = mutable_last_talked_at();
  last_talked_at->set_seconds(now.tv_sec);
  last_talked_at->set_nanos((int32_t) now.tv_nsec);
}

// UseQuery start
// virtual method override for public hazelcast::client::serialization::Portable
int DialogSession::getFactoryId() const {
  return PORTABLE_FACTORY_ID;
}

// virtual method override for public hazelcast::client::serialization::Portable
int DialogSession::getClassId() const {
  return DIALOG_SESSION_V3;
}

// virtual method override for public hazelcast::client::serialization::Portable
void DialogSession::writePortable(hazelcast::client::serialization::PortableWriter &out) const {
  int serializeSize = 0;
  serializeSize = this->ByteSize();
  char bArray[serializeSize];
  this->SerializeToArray(bArray, serializeSize);
  std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

  out.writeLong("id", this->id());
  out.writeByteArray("_msg", &data);
  out.writeUTF("user_id", &this->user_id());
  out.writeUTF("device_id", &this->device_id());
  out.writeUTF("device_type", &this->device_type());
  out.writeUTF("device_version", &this->device_version());
  out.writeUTF("channel", &this->channel());
  out.writeUTF("chatbot", &this->chatbot());
  out.writeBoolean("valid", this->valid());
  out.writeBoolean("talking", this->talking());
  out.writeLong("started_at", this->started_at().seconds());
  out.writeLong("last_talked_at", this->last_talked_at().seconds());
}

// virtual method override for public hazelcast::client::serialization::Portable
void DialogSession::readPortable(hazelcast::client::serialization::PortableReader &in) {
//  in.readLong("id"); //just read. not set. set은 아래 ParseFromArray에서 수행됨.
  std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
  ParseFromArray(vec.data(), (int) vec.size());
}
// UseQuery end

void DialogSession::GetTalkStartTime(timespec &temp) {
  clock_gettime(CLOCK_MONOTONIC, &temp);
  set_talking(true);
}

const long ONE_SEC_NSEC = 1000000000L;

void DialogSession::UpdateTalkTime(timespec &started) {

  timespec now = {0, 0};
  timespec tmp = {0, 0};
  clock_gettime(CLOCK_MONOTONIC, &now);
  clock_gettime(CLOCK_REALTIME, &tmp);
  auto last_talked_at = mutable_last_talked_at();
  last_talked_at->set_seconds(tmp.tv_sec);
  last_talked_at->set_nanos(int32_t(tmp.tv_nsec));
  LOGGER_debug("update talk time: {}.{}",
               last_talked_at->seconds(),
               last_talked_at->nanos());

  // update elapsed time calc
  auto temp_sec = now.tv_sec - started.tv_sec;
  auto temp_nsec = now.tv_nsec - started.tv_nsec;
  if (temp_nsec < 0) {
    temp_sec--;
    temp_nsec += ONE_SEC_NSEC;
  }

  auto accumulated_time = mutable_accumulated_time();
  accumulated_time->set_seconds(temp_sec);
  accumulated_time->set_nanos((int32_t) temp_nsec);

  set_talking(false);
}

void DialogSession::Expire() {
  timespec tmp = {0, 0};
  clock_gettime(CLOCK_REALTIME, &tmp);
  LOGGER_info("EXPIRED {} {} {}", id(), tmp.tv_sec, tmp.tv_nsec);
  set_valid(false);
}

void DialogSession::UpdateSessionContext(const Session &update_session) {
  mutable_context()->MergeFrom(update_session.context());

  // Value가 없는 field는 삭제한다.
  auto fields = update_session.context().fields();
  for (auto iterator = fields.begin(); iterator != fields.end(); ++iterator) {
    if (iterator->second.kind_case() == Value::KindCase::KIND_NOT_SET) {
      mutable_context()->mutable_fields()->erase(iterator->first);
    }
  }
}

void DialogSession::RegisterAgent(const string &skill, const string &key) {
  LOGGER_debug("DialogSession::RegisterAgent last_skill: {}", skill);
  // Session에 설정 후 Hazelcast 업데이드하는 항목
  set_last_agent_key(key);
  set_last_skill(skill);
}

void DialogSession::Invalidate() {
  if (!last_agent_key().empty()) {
    LOGGER_debug("DialogSession::Invalidate last_skill: {}", last_skill());
    clear_last_agent_key();
    clear_last_skill();
  }
}

int DialogSession::CloseAll() {
  auto logger = LOGGER();

  int closed_count = 0;

  // 대화 에이전트 풀에서 적절한 에이젠트를 찾는다.
  // 서버가 죽었을 수도 있으므로 매번 `DialogAgentInstancePool`에서 찾아야 한다.
  auto da_finder = DialogAgentInstanceFinder::GetInstance();
  try {
    // session의 talk를 구한다.
    auto max = GetTalkCount();
    auto hc = hzc::Instance().GetHazelcastClient();
    auto map = hc->getMap<string, TalkImpl>(hzc::kNsTalkV3);
    std::map<string, string> skill_agents;
    std::map<string, string> agents;

    for (decltype(max) idx = 1; idx < (max + 1); idx++) {
      auto talk = map.get(GetTalkKey(idx));
      if (!talk)
        continue;
      for (const auto &ag: talk->agents()) {
        if (ag.reason() == TransionReason_USER_UTTER ||
            ag.reason() == TransionReason_SKILL_TRANS_DISCARD) {
          // FIXME
          // (gih2yun) 여기의 remote 주소는 기존에 이미 연결했던 주소여도 충분할 듯
          // 여기서 느려지고 있으면 안됩니다.
          auto agent_res = da_finder->FindDialogAgentInstance(chatbot(),
                                                              ag.skill(),
                                                              ag.lang());
          if (!agent_res) {
            logger_trace("Cannot find agent with {} {} {}",
                         chatbot(),
                         ag.skill(),
                         ag.lang());
            continue;
          }
          // 원격에 접속할 메시지를 만든다.
          string remote = GetRemoteTarget(agent_res->server_ip(),
                                          agent_res->server_port());
          skill_agents.insert(str_pair(ag.skill(), remote));
          agents.insert(str_pair(remote, agent_res->key()));
        } else {
          // 기존에 PROGRESS인 경우에 있다면 이를 삭제한다.
          auto got = skill_agents.find(ag.skill());
          if (got != skill_agents.end()) {
            logger_debug("STATE: {} remove an agent to call close [{}, {}]",
                         ag.reason(),
                         got->first, got->second);
            agents.erase(got->second);
            skill_agents.erase(ag.skill());
          }
        }
      }
    }
  } catch (IException &e) {
    logger_critical("DialogSession::{} Hazelcast: {}", __func__, e.what());
  }

  return closed_count;
}

// --------------------------------------------------------------------
// TALK TRACE and PERSISTENCE
// --------------------------------------------------------------------
shared_ptr<TalkImpl> DialogSession::NewTalk(shared_ptr<DialogSession> &s,
                                            const SystemContext &context) {
  try {
    auto hc = hzc::Instance().GetHazelcastClient();
    auto talks = hc->getMap<string, TalkImpl>(hzc::kNsTalkV3);

    auto new_talk = std::make_shared<TalkImpl>(s);
    auto seq = SetNextTalkCount();

    new_talk->set_seq(seq);
    new_talk->set_operation_sync_id(tls_op_sync_id);
    new_talk->mutable_system_context()->CopyFrom(context);
    talks.set(GetTalkKey(seq), *new_talk);
    return new_talk;
  } catch (IException &e) {
    LOGGER_critical("DialogSession::{} Hazelcast: {}", __func__, e.what());
    return shared_ptr<TalkImpl>();
  }
}

unsigned DialogSession::GetTalkCount() {
  try {
    auto sid = id();
    auto hc = hzc::Instance().GetHazelcastClient();
    auto map = hc->getMap<int64_t, int>(hzc::kNsTalkNext);
    map.lock(sid);
    auto val = map.get(sid);
    if (!val) {
      map.unlock(sid);
      return 0;
    }
    map.unlock(sid);
    return (unsigned) *val;
  } catch (IException &e) {
    LOGGER_critical("DialogSession::{} Hazelcast: {}", __func__, e.what());
    return 0;
  }
}

unsigned DialogSession::SetNextTalkCount() {
  try {
    auto hc = hzc::Instance().GetHazelcastClient();
    auto map = hc->getMap<int64_t, int>(hzc::kNsTalkNext);
    auto sid = id();
    map.lock(sid);
    auto val = map.get(sid);
    if (!val) {
      map.set(sid, 1);
      map.unlock(sid);
      return 1;
    } else {
      (*val)++;
      map.set(sid, *val);
    }
    map.unlock(sid);
    return (unsigned) *val;
  } catch (IException &e) {
    LOGGER_critical("DialogSession::{} Hazelcast: {}", __func__, e.what());
    return 0;
  }
}

void DialogSession::Update() {
  try {
    auto hc = hzc::Instance().GetHazelcastClient();
    auto map = hc->getMap<int64, DialogSession>(hzc::kNsSessionV3);

    map.set(id(), *this);
  } catch (IException &e) {
    LOGGER_critical("DialogSession::{} Hazelcast: {}", __func__, e.what());
  }
}

shared_ptr<TalkImpl> DialogSession::GetTalk(shared_ptr<DialogSession> &s,
                                            uint32_t talk_seq) {
  try {
    if (talk_seq == 0)
      talk_seq = GetTalkCount();

    auto hc = GlobalHazelcastClient::Instance().GetHazelcastClient();
    auto map = hc->getMap<string, TalkImpl>(hzc::kNsTalkV3);

    auto found_talk = map.get(GetTalkKey(talk_seq));
    auto talk = std::make_shared<TalkImpl>(*found_talk.get());
    talk->SetParent(s);
    return talk;
  } catch (IException &e) {
    return shared_ptr<TalkImpl>();
  }
}
