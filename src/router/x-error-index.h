#ifndef ROUTER_X_ERROR_INDEX_H
#define ROUTER_X_ERROR_INDEX_H

#include <maum/m2u/router/v3/router.pb.h>

using maum::m2u::router::v3::ExIndex;

#define X_ERROR_INDEX(xx, pp) (xx + pp)

#endif // ROUTER_X_ERROR_INDEX_H
