#ifndef M2U_ROUTE_SESSION_CONTAINER_H
#define M2U_ROUTE_SESSION_CONTAINER_H

#include <memory>
#include <unordered_map>
#include <atomic>
#include <google/protobuf/stubs/port.h>
#include <thread>
#include <mutex>
#include <json/json.h>

#include "session.h"

using std::atomic;
using std::shared_ptr;
using google::protobuf::int64;
using maum::m2u::common::Device;

class DialogSessionContainer final {
 private:
  /**
 * ::DialogSessionContainer 생성자
 */
  DialogSessionContainer() = default;
 public:
  static DialogSessionContainer *GetInstance();
  virtual ~DialogSessionContainer() = default;
  bool CheckChatbot(const string &chatbot);
  shared_ptr<DialogSession> FindDialogSession(const string &device_id);
  shared_ptr<DialogSession> CreateDialogSession(const Device &device,
                                                const string &peer,
                                                const string &device_id,
                                                const string &chatbot);
  int DestroyDialogSession(int64 id);
  int DestroyDialogSession(const string &device_token);

 private:
  shared_ptr<DialogSession> FindDialogSession(int64 id);
  // instance
  static atomic<DialogSessionContainer *> instance_;
  static std::mutex instance_mutex_;
};

#endif // M2U_ROUTE_SESSION_CONTAINER_H
