#ifndef _LOCAL_CHATBOT_H__
#define _LOCAL_CHATBOT_H__

#include <maum/m2u/console/chatbot.pb.h>
#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>

#include "hazelcast.h"

using std::string;
using std::unordered_map;
using std::atomic;
using std::shared_ptr;
using std::list;

using google::protobuf::int64;

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

class LocalChatbot : public hazelcast::client::serialization::Portable,
                     public maum::m2u::console::Chatbot {
 public:
  LocalChatbot() = default;
  ~LocalChatbot() override = default;
  LocalChatbot(const LocalChatbot &rhs) {
    this->CopyFrom(rhs);
  }

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  };
  int getClassId() const override {
    return CHATBOT;
  };
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeUTF("name", &this->name());
    out.writeByteArray("_msg", &data);
    out.writeUTF("title", &this->title());
    out.writeUTF("description", &this->description());
    out.writeInt("input", this->input());
    out.writeInt("output", this->output());
    out.writeUTF("external_system_info", &this->external_system_info());
    out.writeBoolean("active", this->active());
    out.writeUTF("auth_provider", &this->auth_provider());
    out.writeUTF("intent_finder_policy", &this->intent_finder_policy());
  };
  void readPortable(PortableReader &in) override {
//    in.readUTF("name");
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  };
};

#endif // _MAUM_CHATBOT_H
