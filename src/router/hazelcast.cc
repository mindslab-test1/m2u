#include <libmaum/common/config.h>

#include "hazelcast.h"
#include "the-portable-factory.h"

using namespace std;
using hazelcast::client::SerializationConfig;
using hazelcast::client::Address;
using hazelcast::client::serialization::SerializerBase;
using hzc = GlobalHazelcastClient;

const char *hzc::kNsAuthPolicy = "auth-policy";
const char *hzc::kNsChatbotDetail = "chatbot-detail";
const char *hzc::kNsDAManager = "dialog-agent-manager";
const char *hzc::kNsDA = "dialog-agent";
const char *hzc::kNsDAActivation = "dialog-agent-activation";
const char *hzc::kNsDAInstance = "dialog-agent-instance";
const char *hzc::kNsDAInstExec = "dialog-agent-instance-exec";
const char *hzc::kNsDAIR = "dialog-agent-instance-resource";
const char *hzc::kNsDAIRSkill = "dialog-agent-instance-resource-skill";
const char *hzc::kNsIntentFinder = "intent-finder";
const char *hzc::kNsITFInstExec = "intent-finder-instance-exec";
const char *hzc::kNsSkill = "skill";
const char *hzc::kNsSC = "simple-classifier";
const char *hzc::kNsQA = "dialog-qa"; // Dialog Trigger Q/A set

/** Session ID AtomicLong */
const char *hzc::kNsSessionId = "session";

/** 대화 for v1,v2 [ Deprecated ] */
const char *hzc::kNsTalk = "talk";
/** 세션 for v1,v2 [ Deprecated ] */
const char *hzc::kNsSession = "session";
/** 대화 for v3 */
const char *hzc::kNsTalkV3 = "talk-v3";
/** 세션 for v3 */
const char *hzc::kNsSessionV3 = "session-v3";
/** 다음 대화 */
const char *hzc::kNsTalkNext = "talk-next";
/** 챗봇 */
const char *hzc::kNsChatbot = "chatbot";
/** Device 정보 */
const char *hzc::kNsDevices = "devices";

const char *hzc::kNsIntentFinderInstance = "intent-finder-instance";

GlobalHazelcastClient::GlobalHazelcastClient() {
  auto &c = libmaum::Config::Instance();
  hazelcast_client_ = nullptr;

  group_name_ = c.Get("hazelcast.group.name");
  group_password_ = c.Get("hazelcast.group.password");
  server_cnt_ = (int32_t) c.GetAsInt("hazelcast.server.count");
  server_port_ = (int32_t) c.GetAsInt("hazelcast.server.port");
  invocation_timeout_ = c.GetDefaultAsInt("hazelcast.invocation.timeout", "0");

  for (auto i = 0; i < server_cnt_; i++) {
    server_address_.push_back(c.Get("hazelcast.server.ip." + to_string(i + 1)));
  }
}

shared_ptr<ClientConfig> GlobalHazelcastClient::GetConfig() {
  auto clientConfig = make_shared<ClientConfig>();

  if (!group_name_.empty() && group_name_ != "dev") {
    // dev, dev-pass는 기본으로 라이브러리에 박혀 있습니다.
    // 따로이 지정하지 않더라도 위와 같이 동작합니다.
    clientConfig->getGroupConfig()
        .setName(group_name_).setPassword(group_password_);
  }
  if (invocation_timeout_ > 0) {
    clientConfig->setProperty("hazelcast.client.invocation.timeout.seconds",
                              to_string(invocation_timeout_));
  }
  clientConfig->setLogLevel(hazelcast::client::LogLevel::FINEST);

  // network config
  auto &network_config = clientConfig->getNetworkConfig();
  for (const auto &addr: server_address_) {
    LOGGER()->debug("hazelcast server ip: {}", addr);
    network_config.addAddress(Address(addr, server_port_));
  }
  network_config.setSmartRouting(true);

  // serialization config
  auto &serialization_config = clientConfig->getSerializationConfig();
  serialization_config.addPortableFactory(
      PORTABLE_FACTORY_ID,
      boost::shared_ptr<PortableFactory>(new ThePortableFactory()));

  return clientConfig;
}

shared_ptr<HazelcastClient> GlobalHazelcastClient::GetHazelcastClient() {
  if (!hazelcast_client_) {
    try {
      hazelcast_client_ = make_shared<HazelcastClient>(*GetConfig());
    } catch (IException &e) {
      LOGGER()->critical("Hazelcast: {}", e.what());
      return shared_ptr<HazelcastClient>();
    }
  }

  return hazelcast_client_;
}


/**
 * - 일정 시간 이후 접속이 안 되면 서비스를 종료한다.
 */
bool GlobalHazelcastClient::IsReady() {
  const int waits[] = {20, 40, 60, 0};
  auto logger = LOGGER();
  int wait_seconds = 0;

  logger->debug("Check Hazelcast Running...");
  for (auto &wait : waits) {
    auto client = Instance().GetHazelcastClient();
    if (client) {
      return true;
    } else {
      wait_seconds += wait;
      logger->debug(" Wait more to {}s.", wait_seconds);
    }
    std::this_thread::sleep_for(std::chrono::seconds(wait - 7));
  }

  return false;
}
