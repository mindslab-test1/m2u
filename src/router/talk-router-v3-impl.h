#ifndef M2U_ROUTER_TALK_ROUTER_V3_IMPL_H
#define M2U_ROUTER_TALK_ROUTER_V3_IMPL_H

#include <libmaum/rpc/result-status.h>
#include <libmaum/log/call-log.h>
#include <maum/m2u/router/v3/router.grpc.pb.h>
#include "tls-values.h"
#include "talk-router-v3.h"

using maum::m2u::router::v3::TalkRouter;
using libmaum::log::CallLogInvoker;


// 파일 단위로 error_index의 상위 두자리를 정의한다.
// error_index: 00000 ~ 09999
#define ERROR_INDEX_MAJOR 00000

class TalkRouterV3Impl final : public TalkRouter::Service {
 public:
  TalkRouterV3Impl() {
    service_ = ::TalkRouterV3::GetInstance();
  }

  virtual ~TalkRouterV3Impl() = default;

  Status OpenRoute(ServerContext *context,
                   const OpenRouteRequest *request,
                   ServerWriter<TalkRouteResponse> *writer) override {
    ResetTlsValues();
    Status status = CheckOperationSyncId(context);

    CallLogInvoker log("Open", context, request, EmptyMessage(), &status);

    if (!status.ok()) return status;

    status = CheckDeviceId(request->system_context());
    if (!status.ok()) return status;

    shared_ptr<DialogSession> session;
    service_->IsSessionValid(session);

    status = service_->OpenRoute(context, session, request, writer);

    return status;
  }

  Status TalkRoute(ServerContext *context,
                   const TalkRouteRequest *request,
                   ServerWriter<TalkRouteResponse> *writer) override {
    ResetTlsValues();
    Status status = CheckOperationSyncId(context);

    CallLogInvoker log("Talk", context, request, EmptyMessage(), &status);

    if (!status.ok()) return status;

    status = CheckDeviceId(request->system_context());
    if (!status.ok()) return status;

    shared_ptr<DialogSession> session;
    status = service_->IsSessionValid(session);
    if (!status.ok()) {
      LOGGER_warn("TalkRouterV3Impl::{} failed. [{}]",
                  __func__,
                  status.error_message());
      return status;
    }

    status = service_->TalkRoute(context, session, request, writer);

    return status;
  }

  Status EventRoute(ServerContext *context,
                    const EventRouteRequest *request,
                    ServerWriter<TalkRouteResponse> *writer) override {
    ResetTlsValues();
    Status status = CheckOperationSyncId(context);

    CallLogInvoker log("Event", context, request, EmptyMessage(), &status);

    if (!status.ok()) return status;

    status = CheckDeviceId(request->system_context());
    if (!status.ok()) return status;

    shared_ptr<DialogSession> session;
    status = service_->IsSessionValid(session);
    if (!status.ok()) {
      LOGGER_warn("TalkRouterV3Impl::{} failed. [{}]",
                  __func__,
                  status.error_message());
      return status;
    }

    status = service_->EventRoute(context, session, request, writer);

    return status;
  }

  Status CloseRoute(ServerContext *context,
                    const CloseRouteRequest *request,
                    CloseRouteResponse *response) override {
    ResetTlsValues();
    Status status = CheckOperationSyncId(context);

    CallLogInvoker log("Close", context, request, EmptyMessage(), &status);

    if (!status.ok()) return status;

    status = CheckDeviceId(request->system_context());
    if (!status.ok()) return status;

    shared_ptr<DialogSession> session;
    status = service_->IsSessionValid(session);
    if (!status.ok()) {
      LOGGER_warn("TalkRouterV3Impl::{} failed. [{}]",
                  __func__,
                  status.error_message());
      return status;
    }

    status = service_->CloseRoute(context, session, request, response);

    return status;
  }

  Status Feedback(ServerContext *context,
                  const FeedbackRequest *request,
                  Empty *empty) override {
    ResetTlsValues();
    Status status = CheckOperationSyncId(context);

    if (!status.ok()) return status;

    status = CheckDeviceId(request->system_context());
    if (!status.ok()) return status;

    shared_ptr<DialogSession> session;
    status = service_->IsSessionValid(session);
    if (!status.ok()) {
      LOGGER_warn("TalkRouterV3Impl::{} failed. [{}]",
                  __func__,
                  status.error_message());
      return status;
    }

    status = service_->Feedback(context, session, request);

    return status;
  }

 private:
  static const Empty *EmptyMessage() {
    static const Empty empty = Empty();
    return &empty;
  }

  void ResetTlsValues() {
    tls_op_sync_id.clear();
    tls_device_id.clear();
    tls_session_id = "unassigned";
  }

  /**
  * @brief Server Context에서 x-operation-sync-id를 가져온다.
  *
  * @param context
  * @param status
  * @return context에 'x-operation-sync-id'가 있으면 OK, 아니면 error Status;
  */
  Status CheckOperationSyncId(const ServerContext *context) {
    const auto &client_map = context->client_metadata();
    auto item_s = client_map.find("x-operation-sync-id");

    if (item_s == client_map.end()) {
      Status status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                                       ERROR_INDEX(0),
                                       "x-operation-sync-id is Missing");
      LOGGER()->error("[{}] {}",
                      context->peer(),
                      status.error_message());
      return status;
    }

    SetTlsOpSyncId(string(item_s->second.begin(), item_s->second.end()));
    LOGGER_info("operation from [{}]", context->peer());

    return Status::OK;
  }

/**
* @brief Request의 System Context에서 Device ID를 가져온다.
*
* @param context
* @return status
*/
  Status CheckDeviceId(const SystemContext &context) {
    auto logger = LOGGER();
    // Device 정보가 확인
    if (context.has_device()) {
      const auto &device = context.device();
      string token = device.type() + '_' + device.id();
      // Device ID가 없을 때
      if (token.length() > 2) {
        logger_debug("Device ID: {}", token);
        SetTlsDeviceId(token);
        return Status::OK;
      } else {
        Status status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                                         ERROR_INDEX(2),
                                         "Device ID is Missing");
        logger_debug("{}", status.error_message());
        return status;
      }
    } else {  // Device 정보가 없을 때
      Status status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                                       ERROR_INDEX(1),
                                       "Device Information is Missing");
      logger_debug("{}", status.error_message());
      return status;
    }
  }

  TalkRouterV3 *service_;
};

#endif // M2U_ROUTER_TALK_ROUTER_V3_IMPL_H
