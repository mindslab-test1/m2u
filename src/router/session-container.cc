#include <chrono>
#include <hazelcast/client/IAtomicLong.h>

#include "tls-values.h"
#include "local-chatbot.h"
#include "local-intentfinder.h"
#include "session-container.h"

using namespace std::chrono;
using hazelcast::client::IAtomicLong;
using hazelcast::client::query::SqlPredicate;
using hzc = GlobalHazelcastClient;
using maum::m2u::common::SystemContext;
using maum::m2u::server::AgentUserAttribute;
using maum::m2u::server::SessionDetail;

atomic<DialogSessionContainer *> DialogSessionContainer::instance_;
std::mutex DialogSessionContainer::instance_mutex_;

DialogSessionContainer *DialogSessionContainer::GetInstance() {
  DialogSessionContainer *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(instance_mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new DialogSessionContainer;
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

/**
 * @brief Chatbot이 등록되어 있는지 확인한다.
 *
 * @param chatbot Chatbot이름
 * @return 있으면 true, 없으면 false
 */
bool DialogSessionContainer::CheckChatbot(const string &chatbot) {
  try {
    auto hzc_client = hzc::Instance().GetHazelcastClient();
    auto cb_map = hzc_client->getMap<string, LocalChatbot>(hzc::kNsChatbot);

    return cb_map.containsKey(chatbot);
  } catch (IException &e) {
    LOGGER_critical("DialogSessionContainer::{} Hazelcast: {}",
                    __func__,
                    e.what());
  }

  return false;
}

/**
 * @brief DialogueSession 을 찾아서 반환한다.
 *
 * @param sessionId 세션의 키
 * @return 기존의 세션을 반환한다.
 */
shared_ptr<DialogSession> DialogSessionContainer::FindDialogSession(int64 id) {
  auto logger = LOGGER();

  try {
    auto hzc_client = hzc::Instance().GetHazelcastClient();
    auto map = hzc_client->getMap<int64, DialogSession>(hzc::kNsSessionV3);
    auto session = map.get(id);
    if (session) {
      return std::make_shared<DialogSession>(*session);
    }
  } catch (IException &e) {
    logger_critical("DialogSessionContainer::{} Hazelcast: {}",
                    __func__,
                    e.what());
  }

  logger_debug("FindDialogSession failed for id:{}", id);

  return shared_ptr<DialogSession>();
}

/**
 * @brief DialogueSession 을 찾아서 반환한다.
 *
 * @param 세션의 키에 맵핑된 Device ID
 * @return 기존의 세션을 반환한다.
 */
shared_ptr<DialogSession> DialogSessionContainer::FindDialogSession(
    const string &device_id) {
  auto logger = LOGGER();

  try {
    auto hzc_client = hzc::Instance().GetHazelcastClient();
    auto device_map = hzc_client->getMap<string, int64>(hzc::kNsDevices);
    auto session_id = device_map.get(device_id);

    if (session_id) {
      auto map = hzc_client->getMap<int64, DialogSession>(hzc::kNsSessionV3);
      if (map.size() > 0) {
        auto session = map.get(*session_id);
        if (session) {
          return std::make_shared<DialogSession>(*session);
        } else {
          logger_warn("session id {} not exist in map for device {}",
                      *session_id,
                      device_id);
        }
      } else {
        logger_warn("session id {} not exist in map for device {}, size = 0",
                    *session_id,
                    device_id);
      }
    } else {
      logger_debug("Oops! device not found in map {}",
                   device_id);
    }

  } catch (IException &e) {
    logger_critical("DialogSessionContainer::{} Hazelcast: {}",
                    __func__,
                    e.what());
  }

  logger_debug("FindDialogSession failed for Device ID: {}", device_id);

  return shared_ptr<DialogSession>();
}

shared_ptr<DialogSession> DialogSessionContainer::CreateDialogSession(
    const Device &device,
    const string &peer,
    const string &device_id,
    const string &chatbot) {
  auto logger = LOGGER();
  try {
    // Session ID를 hazelcast IAtomicLong으로 가져온다.
    auto hzc_client = hzc::Instance().GetHazelcastClient();
    IAtomicLong atomicNumber = hzc_client->getIAtomicLong(hzc::kNsSessionId);
    int64 new_key = atomicNumber.incrementAndGet();
    auto psession = std::make_shared<DialogSession>(new_key);

    // 만들어진 session id 를 x-device-id과 맵핑 시킨다.
    auto device_map = hzc_client->getMap<string, int64>(hzc::kNsDevices);
    device_map.set(device_id, new_key);

    // session 정보를 저장한다.
    psession->set_peer(peer);
    psession->set_device_id(device_id);
    psession->set_device_type(device.type());
    psession->set_device_version(device.version());
    psession->set_channel(device.channel());

    psession->set_chatbot(chatbot);
    auto cb_map = hzc_client->getMap<string, LocalChatbot>(hzc::kNsChatbot);
    auto cb = cb_map.get(chatbot);
    if (cb != nullptr) {
      psession->set_intent_finder_policy(cb->intent_finder_policy());
      if (!cb->external_system_info().empty()) {
        string es(cb->external_system_info() + "|"
                      + to_string(cb->session_timeout()));
        psession->set_external_system_info(move(es));;
      }
    }

    // 만들어진 데이터를 KEY로 저장하도록 한다.
    logger_debug("New Session ID: {} Device ID: {} chatbot: {} itf: {}"
                 " channel: {} peer: {}",
                 psession->id(),
                 psession->device_id(),
                 psession->chatbot(),
                 psession->intent_finder_policy(),
                 psession->channel(),
                 psession->peer());
    auto map = hzc_client->getMap<int64, DialogSession>(hzc::kNsSessionV3);
    map.set(new_key, *psession);
    return psession;
  } catch (IException &e) {
    logger_critical("DialogSessionContainer::{} Hazelcast: {}",
                    __func__,
                    e.what());
    return shared_ptr<DialogSession>();
  }
}

/**
 * 기존의 세션을 삭제한다.
 *
 * @param id 세션 ID
 * @return skill에 명시적인 close를 호출한 개수를 반환
 */
int DialogSessionContainer::DestroyDialogSession(int64 id) {
  auto logger = LOGGER();
  int count = 0;
  try {
    auto hzc_client = hzc::Instance().GetHazelcastClient();
    auto map = hzc_client->getMap<int64, DialogSession>(hzc::kNsSessionV3);
    auto session = map.get(id);
    if (session) {
      count = session->CloseAll();
      logger_info("Close session all {}, closed: {}", session->id(), count);
      session->Expire();
      map.set(session->id(), *session);
      logger_debug("Invalidated session {}", session->id());
    } else {
      logger_warn("Oops! cannot find the session {}", id);
    }
  } catch (IException &e) {
    logger_critical("DialogSessionContainer::{} Hazelcast: {}",
                    __func__,
                    e.what());
  }
  return count;
}

/**
 * @brief 기존의 세션을 device id 값으로 삭제한다.
 * @param device_id
 * @return
 */
int DialogSessionContainer::DestroyDialogSession(const string &device_token) {
  auto logger = LOGGER();
  int count = 0;
  try {
    auto hzc_client = hzc::Instance().GetHazelcastClient();
    auto device_map = hzc_client->getMap<string, int64>(hzc::kNsDevices);
    auto session_id = device_map.get(device_token);
    if (session_id) {
      count = DestroyDialogSession(*session_id);
      device_map.deleteEntry(device_token);
    }
  } catch (IException &e) {
    logger_critical("DialogSessionContainer::{} Hazelcast: {}",
                    __func__,
                    e.what());
  }
  return count;
}

