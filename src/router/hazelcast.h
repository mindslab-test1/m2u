#ifndef M2U_ROUTER_HAZELCAST_H
#define M2U_ROUTER_HAZELCAST_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/ObjectDataOutput.h>
#include <hazelcast/client/serialization/ObjectDataInput.h>

#include <google/protobuf/stubs/port.h>

using std::string;

// Hazelcast namespace
using hazelcast::client::HazelcastClient;
using hazelcast::client::ClientConfig;
using hazelcast::client::exception::IException;

using google::protobuf::int64;


enum PortableFactoryId {
  PORTABLE_FACTORY_ID = 1
};

/*
 * 참고 : `MapStore` Java 소스에서 정의한 class id와 같아야 한다.
 *
  public static final int MAUMD_FACTORY_ID = 1;

  public static final class CLASS_ID {

    public static final int DIALOG_AGENT_INSTANCE_RESOURCE = 12;
    public static final int DIALOG_SESSION = 21;
    public static final int SESSION_TALK = 22;
  }

 */

// HazelcastTypeId는 중복되지 않도록 enum 으로 선언하여 사용.
enum HazelcastTypeId {
  CHATBOT = 6,
  DIALOG_SESSION = 21, // Deprecated
  SESSION_TALK = 22, // Deprecated
  DIALOG_AGENT_INSTANCE_RESOURCE = 12,
  DIALOG_AGENT_INSTANCE_RESOURCE_SKILL = 13,
  DIALOG_SESSION_V3 = 24,
  SESSION_TALK_V3 = 25,

  INTENT_FINDER_INSTANCE = 30,
  INTENT_FINDER_POLICY = 31,
  INTENT_FINDING_STEP = 32
};

class GlobalHazelcastClient {
  GlobalHazelcastClient();
  virtual ~GlobalHazelcastClient() = default;

 public:
  static GlobalHazelcastClient &Instance() {
    static GlobalHazelcastClient instance_;
    return instance_;
  }

  virtual std::shared_ptr<ClientConfig> GetConfig();
  std::shared_ptr<HazelcastClient> GetHazelcastClient();

  static const char *kNsAuthPolicy;
  static const char *kNsChatbotDetail;
  static const char *kNsDAManager;
  static const char *kNsDA;
  static const char *kNsDAActivation;
  static const char *kNsDAInstance;
  static const char *kNsDAInstExec;
  static const char *kNsIntentFinder;
  static const char *kNsITFInstExec;
  static const char *kNsSkill;
  static const char *kNsSC;
  static const char *kNsQA;

  /** Session ID AtomicLong */
  static const char *kNsSessionId;

  /** 대화 for v1,v2 [ Deprecated ] */
  static const char *kNsTalk;
  /** 세션 for v1,v2 [ Deprecated ] */
  static const char *kNsSession;
  /** 대화 for v3 */
  static const char *kNsTalkV3;
  /** 세션 for v3 */
  static const char *kNsSessionV3;
  /** 다음 대화 */
  static const char *kNsTalkNext;
  /** 챗봇 */
  static const char *kNsChatbot;
  /** 모든 에이전트 */
  static const char *kNsDAIR;
  /** 모든 에이전트 Skill */
  static const char *kNsDAIRSkill;
  /** Device 정보 */
  static const char *kNsDevices;

  static const char *kNsIntentFinderInstance;

  static bool IsReady();

 private:
  int32_t server_cnt_;
  std::list<string> server_address_;
  int32_t server_port_;
  std::shared_ptr<HazelcastClient> hazelcast_client_;
  string group_name_;
  string group_password_;
  long invocation_timeout_;
};

#endif // M2U_ROUTER_HAZELCAST_H
