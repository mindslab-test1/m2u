#ifndef M2U_ROUTER_DIALOGSESSION_H
#define M2U_ROUTER_DIALOGSESSION_H

#include <unordered_map>
#include <atomic>
#include <mutex>
#include <list>
#include <string>

// UseQuery start
#include <hazelcast/client/query/SqlPredicate.h>
#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>
// UseQuery end

#include <libmaum/common/config.h>
#include <maum/m2u/server/session.grpc.pb.h>
#include <maum/m2u/da/provider.grpc.pb.h>
#include <maum/m2u/da/provider.grpc.pb.h>
#include <maum/m2u/da/v3/talk.grpc.pb.h>
#include <maum/m2u/router/v3/session.pb.h>

#include "hazelcast.h"

using std::string;
using std::to_string;
using std::unordered_map;
using std::atomic;
using std::shared_ptr;
using std::list;

using hazelcast::client::serialization::Serializer;
using hazelcast::client::serialization::ObjectDataOutput;
using hazelcast::client::serialization::ObjectDataInput;
using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

using google::protobuf::int64;
using google::protobuf::Struct;

using maum::common::Lang;
using maum::m2u::common::Session;
using maum::m2u::common::SystemContext;
using maum::m2u::router::v3::SessionTalk;
using maum::m2u::router::v3::TalkAgent;
using maum::m2u::router::v3::FoundIntent;
using dap_v3 = maum::m2u::da::v3::DialogAgentProvider;

class TalkImpl;

// UseQuery start
// query 기능 사용하기 위하여 Portable 상속받아야 한다.
class DialogSession : public hazelcast::client::serialization::Portable,
                      public maum::m2u::router::v3::DialogSession {

// UseQuery end
 public:
  DialogSession() = default;
  explicit DialogSession(int64 id);
  virtual ~DialogSession() = default;
  DialogSession(const DialogSession &rhs) {
    this->CopyFrom(rhs);
  }

  // UseQuery start
  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override;
  int getClassId() const override;
  void writePortable(PortableWriter &out) const override;
  void readPortable(PortableReader &in) override;
  // UseQuery end

  void GetTalkStartTime(timespec &temp);
  void UpdateTalkTime(timespec &started);
  void Expire();
  void UpdateSessionContext(const Session &update_session);

  void Invalidate();
  int CloseAll();

  shared_ptr<TalkImpl> NewTalk(shared_ptr<DialogSession> &s,
                               const SystemContext &context);
  shared_ptr<TalkImpl> GetTalk(shared_ptr<DialogSession> &s, uint32_t talk_seq);
  unsigned GetTalkCount();
  unsigned SetNextTalkCount();
  inline string GetTalkKey(uint32_t seq) {
    return string("#" + to_string(id()) + "-#" + to_string(seq));
  }
  void RegisterAgent(const string &skill, const string &key);
  void Update();
};

class SessionAccessor {
 public:
  explicit SessionAccessor(shared_ptr<DialogSession> &session)
      : temp_({0, 0}),
        session_(session) {
    session_->GetTalkStartTime(temp_);
  }
  ~SessionAccessor() {
    session_->UpdateTalkTime(temp_);
  }
 private:
  timespec temp_;
  shared_ptr<DialogSession> session_;
};

#endif // M2U_ROUTER_DIALOGSESSION_H
