cmake_minimum_required(VERSION 2.8.12)
project(m2ud CXX)
set(OUTBIN m2u-ttsd)

include(import)

find_package(Protobuf REQUIRED)

if (${CMAKE_SYSTEM_NAME} STREQUAL "Linux" AND ${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")
  find_package(OpenMP)
  if (OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
  endif ()

  msl_import_binary(CV_TTS_API_DIR CV_TTS_API_MADE "CV_TTS"
      corevoice-core-tts-api.tar.gz)
endif ()

include(gitversion/cmake)
include_directories(${PROTOBUF_INCLUDE_DIRS})
include_directories(../../proto)

if (${CMAKE_SYSTEM_NAME} STREQUAL "Linux" AND ${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")
  include_directories(${CV_TTS_API_DIR})
  link_directories(${CV_TTS_API_DIR})
endif ()

include_directories("${CMAKE_INSTALL_PREFIX}/include")
link_directories("${CMAKE_INSTALL_PREFIX}/lib")

set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif ()

set(SOURCE_FILES
    main.cc
    tts-client.cc tts-client.h
    speech-synthesizer-impl.cc speech-synthesizer-impl.h
    )

if (${CMAKE_SYSTEM_NAME} STREQUAL "Linux" AND ${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")
  set(LIB_TTS TtsClientApi)
endif ()

add_executable(${OUTBIN}
    ${SOURCE_FILES}
    ${CV_TTS_API_MADE})

target_git_version_init(${OUTBIN})

target_link_libraries(${OUTBIN}
    TtsClientApi
    maum-common
    m2u-all-pb
    grpc++ grpc++_reflection
    grpc protobuf
    )

#install(TARGETS ${OUTBIN} RUNTIME DESTINATION bin)

if (${CMAKE_SYSTEM_NAME} STREQUAL "Linux" AND ${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")
  install(PROGRAMS ${CV_TTS_API_DIR}/libTtsClientApi.so
      DESTINATION lib
      PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
      )
endif ()
