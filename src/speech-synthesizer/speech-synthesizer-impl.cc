
#include "speech-synthesizer-impl.h"

#include "tts-client.h"

using grpc::StatusCode;
using std::string;

SpeechSynthesizerImpl::SpeechSynthesizerImpl() {
}

SpeechSynthesizerImpl::~SpeechSynthesizerImpl() {
}

Status SpeechSynthesizerImpl::Speak(ServerContext *context,
                                    const SpeakRequest *req,
                                    ServerWriter<SpeakResponse> *stream) {
  auto logger = LOGGER();
  unique_ptr<SimpleTtsClient>
      tts(TtsClientFactory::GetInstance()
              ->Create(
                  req->lang(),
                  req->encoding(),
                  req->sample_rate()));

  if (!tts->Open(stream)) {
    logger->warn<const char *>("tts client open failed");
    return Status(StatusCode::INTERNAL, "tts client: open failed");
  }
  if (!tts->GetSpeech(req->text())) {
    logger->warn<const char *>("tts client: get speech failed");
    return Status(StatusCode::INTERNAL, "tts client: get speech failed");
  }
  return Status::OK;
}
