#ifdef __linux__

#include <string>
#include "tts-client.h"
#include <libmaum/common/encoding.h>

using std::string;

/**
 * 새로운 TTS 클라이언트를 생성한다.
 *
 * @param ip 원격 주소
 * @param port 포트
 * @param format 전송 포맷
 */
SimpleTtsClient::SimpleTtsClient(char *ip, int port,
                                 Lang lang,
                                 AudioEncoding audio_encoding,
                                 int sample_rate)
    : lang_(lang),
      encoding_(audio_encoding),
      sample_rate_(sample_rate) {
  int format = GetFormat();
  // 음성합성엔진과 접속하기 위한 클라이언트를 생성한다
  client_ = TTSCreate(ip, port, format);
  int rc = TTSSetCallback(client_, TtsCallback);
  if (rc != TTS_OK) {
    client_ = NULL;
    status_ = None;
  } else
    status_ = Created;
}

/**
 * TTS 클라이언트를 삭제한다.
 *
 * 내부적인 상태에 따라서 소멸을 달리하게 된다.
 * 특히, 소멸자에서는 기존의 TTS 클라이언트를 API 맵에서 삭제한다.
 */
SimpleTtsClient::~SimpleTtsClient() {
  switch (status_) {
    case TtsApiStatus::GotChannel:
      if (client_) {
        //
        // ********** 매우 중요한 코드 임 ********
        TtsClientFactory *api = TtsClientFactory::GetInstance();
        api->Unregister(channel_);
        // TODO
        // logging
        channel_ = 0;
      }
    case TtsApiStatus::Opened:
      if (client_) {
        TTSClose(client_);
      }
    case TtsApiStatus::Created:
      if (client_) {
        TTSDelete(client_);
        client_ = NULL;
      }
    default:break;
  }
}

/**
 * 음성 변환용 채널을 확보한다.
 *
 * 채널이 확보되면 이를 API의 맵에 등록한다.
 *
 * @param stream 음성 출력용 스트림을 전달한다.
 * @return 성공하면 참을 반환.
 */
bool SimpleTtsClient::Open(WriterInterface<SpeakResponse> *stream) {
  if (!client_) {
    return false;
  }
  int rc = TTSOpen(client_);
  if (rc != TTS_OK) {
    return false;
  }
  status_ = TtsApiStatus::Opened;
  // 이미 정상적으로 열린 경우에 채널에서 데이터를 가져오는 경우는 없다고 본다.
  int channel = TTSGetChannel(client_);
  LOGGER()->debug("TTS: new channel {} for {}", channel, (void *) this);
  if (channel == TTSERR_CHANNEL_ASSIGN) {
    // TODO
    // log 처리,
    // printf("TTSGetChannel Error[%X]: can't get channel number..\n",
    // nChannel);
    return false;
  }
  channel_ = channel;
  stream_ = stream;
  status_ = TtsApiStatus::GotChannel;

  // ********* 매우 중요한 코드 ******
  TtsClientFactory *api = TtsClientFactory::GetInstance();
  api->Register(channel_, this);

  return true;
}

/**
 * 음성으로 변환한다.
 *
 * 결과는 지정된 콜백에서 처리된다.
 *
 * @param utter 전송할 텍스트
 */
bool SimpleTtsClient::GetSpeech(const string &utter) {
  string str = Utf8ToEuckr(utter);
  int rc = TTSGetSpeechStream(client_, str.c_str());
  if (rc != TTS_OK) {
    // TODO
    // LOGGING:
    // 예상 가능한 예외 조건
    // 네트워크 에러가 발생하여 처리 중에 에러가 나온다거나... 등등..
    // printf("TTSGetSpeechStream Error[%X]: can't get speech..\n", nRet);
    return false;
  }
  return true;
}

/**
 * 전송받은 음성 데이터를 클라이언트에게 전송해주는 콜백.
 *
 * @param chan 이 채널은 클라이언트에서 ::TtsOpen(), ::TtsGetChannel() 후에
 * 얻어진 채널 번호이다. 이 채널번호는 얻어지지마자 ::SimpleTtsClient::Open()에서 자동으로
 * 등록된 것이다. 여기서 등록된 채널번호가 모든 분기점의 시작이다.
 * @param voice 데이터
 * @param voiceLen 데이터의 길이
 */
unsigned int TtsCallback(int chan, unsigned char *voice, int voiceLen) {
  auto logger = LOGGER();
  auto api = TtsClientFactory::GetInstance();
  auto client = api->GetClient(chan);

  logger->trace("client : {}", (void *) client);
  logger->trace("chan : {}, voice len: {}", chan, voiceLen);
  if (client) {
    auto stream = client->GetStream();
    if (client->GetVoiceLength() == 0) {
      SpeakResponse param_response;
      auto param = param_response.mutable_param();
      param->set_lang(client->lang());
      param->set_encoding(client->encoding());
      param->set_sample_rate(client->sample_rate());
      stream->Write(param_response);
    }
    SpeakResponse response;
    response.mutable_audio_content()->assign(
        (const char *)voice, (size_t)voiceLen);
    stream->Write(response);
    client->AddVoiceLength(voiceLen);
    return (unsigned int) voiceLen;
  }
  return 0;
}

// ====================================================================
// TTS CLIENT API
// ====================================================================
atomic<TtsClientFactory *> TtsClientFactory::instance_;
mutex TtsClientFactory::mutex_;

/**
 * 싱글톤 인스턴스를 구해온다.
 *
 * 이를 싱글톤으로 처리하는 이유는 API 초기화를 처리하기 위해서이다.
 *
 * @return 싱글톤 TtsClientFactory 객체
 */
TtsClientFactory *TtsClientFactory::GetInstance() {
  TtsClientFactory *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new TtsClientFactory();
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

TtsClientFactory::TtsClientFactory() {
  StartWSA();
  auto &c = libmaum::Config::Instance();
  server_kr_ip_ = c.Get("corevoice.tts.server.kr.ip");
  server_kr_port_ = (int) c.GetAsInt("corevoice.tts.server.kr.port");
  server_en_ip_ = c.Get("corevoice.tts.server.en.ip");
  server_en_port_ = (int) c.GetAsInt("corevoice.tts.server.en.port");
}

// TODO
// 나중에는 클라이언트 풀을 이용해서 접속을 처리해야 한다.
/**
 * 새로운 SimpleTtsClient 객체를 생성한다.
 *
 * 나중에는 이미 등록된 클라이언트 풀에서 사용하도록 한다.
 *
 * @return 새로운 SimpleTtsClient 객체
 */
SimpleTtsClient *TtsClientFactory::Create(Lang lang,
                                          AudioEncoding encoding,
                                          int sample_rate) {
  char buf[50];
  int port;
  if (lang == maum::common::Lang::en_US) {
    strncpy(buf, server_en_ip_.c_str(), sizeof(buf));
    port = server_en_port_;
  } else {
    strncpy(buf, server_kr_ip_.c_str(), sizeof(buf));
    port = server_kr_port_;
  }

  SimpleTtsClient *client = new SimpleTtsClient(buf,
                                                port,
                                                lang,
                                                encoding,
                                                sample_rate);
  LOGGER()->debug("TTS: newly created client : {}", (void *) client);
  return client;
}

#endif