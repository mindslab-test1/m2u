#include <iostream>

#include <grpc++/grpc++.h>

#include <libmaum/common/util.h>
#include <libmaum/common/config.h>
#include <libmaum/log/call-log.h>
#include <limits.h>

#include <csignal>
#include <syslog.h>

#include <getopt.h>
#include <gitversion/version.h>
#include <limits.h>
#include "speech-synthesizer-impl.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using namespace std;

std::mutex g_sigterm_mutex;


void ExitSignalHandler(int signo) {
  static int sig_count = 0;
  sigset_t full;
  char hostname[HOST_NAME_MAX];

  sigfillset(&full);
  sigprocmask(SIG_BLOCK, &full, NULL);

  gethostname(hostname, sizeof(hostname));
  auto program = libmaum::Config::Instance().GetProgramPath();

  {
    std::unique_lock<std::mutex> lock(g_sigterm_mutex);
    if (sig_count) {
      std::this_thread::sleep_for(std::chrono::microseconds(1));
      exit(0);
    }
    auto logger = LOGGER();
    logger->info("signal {} {} hostname: {}",
                 signo, time(0), hostname);
    syslog(LOG_INFO, "%s received SIGNAL %d", program.c_str(), signo);
    sig_count = 1;
  }

  exit(0);
}


void RunServer() {
  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = c.GetLogger();
  std::string server_address(c.Get("speech-synthesizer.listen"));
  long grpc_timeout = c.GetAsInt("speech-synthesizer.grpc.timeout");

#if 1
  if (grpc_timeout == 0) {
    logger->info("grpc_timeout is unlimited");
    grpc_timeout = INT_MAX;
  }
#endif

  SpeechSynthesizerImpl impl;

  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
  builder.RegisterService(&impl);

  unique_ptr<Server> server(builder.BuildAndStart());

  if (server) {
    logger->info("Server listening on {}", server_address);
    c.DumpPid();
    server->Wait();
  }
}

void help(char *argv) {
  printf("%s [--version] [--help]\n", basename(argv));
}

void process_option(int argc, char *argv[]) {
  bool do_exit = false;
  int c;
  while (1) {
    static const struct option long_options[] = {
        {"version", no_argument, 0, 'v'},
        {"help", no_argument, 0, 'h'},
        {nullptr, no_argument, nullptr, 0 }
    };

    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long(argc, argv, "vh?", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
      case 0:
        break;
      case 'v':
        printf("%s version %s\n", basename(argv[0]), version::VERSION_STRING);
        do_exit = true;
        break;
      case 'h':
        help(argv[0]);
        do_exit = true;
        break;
      case '?':
        help(argv[0]);
        do_exit = true;
        break;
      default:
        help(argv[0]);
        do_exit = true;
    }
  }
  if (do_exit)
    exit(EXIT_SUCCESS);
}


int main(int argc, char *argv[]) {
  process_option(argc, argv);
  libmaum::Config::Init(argc, argv, "m2u.conf");

  auto logger = LOGGER();

  signal(SIGTERM, ExitSignalHandler);
  //signal(SIGSEGV, ExitSignalHandler);
  //signal(SIGABRT, ExitSignalHandler);
  signal(SIGINT, ExitSignalHandler);

  logger->info("STARTING M2U Speech Synthesizer Server version: {},  {}",
               version::VERSION_STRING,
               getpid());

  EnableCoreDump();
  RunServer();
  return 0;
}
