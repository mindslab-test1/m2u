#ifndef M2U_TTS_CLIENT_H
#define M2U_TTS_CLIENT_H

#ifdef __linux__

#include "CoreTtsApi.h"    // 음성합성엔진 클라이언트 API
#include <string>
#include <unordered_map>
#include <memory>
#include <atomic>
#include <mutex>

#include <libmaum/common/config.h>

#include <grpc++/impl/codegen/sync_stream.h>
#include <maum/brain/tts/speech.pb.h>

using grpc::ServerReaderWriter;
using grpc::internal::WriterInterface;
using maum::brain::tts::SpeakResponse;
using maum::brain::tts::SpeakRequest;
using maum::common::AudioEncoding;
using maum::common::Lang;
using std::string;
using std::unordered_map;
using std::shared_ptr;
using std::atomic;
using std::mutex;

unsigned int TtsCallback(int chan, unsigned char *voice, int voiceLen);

class TtsClientFactory;

/**
 * CoreVoice SimpleTtsClient 로서 음성으로 변환을 처리한다.
 */
class SimpleTtsClient {
 public:
  explicit SimpleTtsClient(char *ip, int port,
                           Lang lang,
                           AudioEncoding encoding,
                           int sample_rate);
 public:
  virtual ~SimpleTtsClient();
  /**
   * 생성된 클라이언트가 정상적인지 확인한다.
   * 생성된 클라이언트가 내부적으로 비정상적인 수 있다.
   */
  bool IsValid() {
    return client_ != NULL;
  }

  bool Open(WriterInterface<SpeakResponse> *stream);

  bool GetSpeech(const string &utter);
  void AddVoiceLength(int len) {
    length_ += len;
  }

  int32_t GetVoiceLength() {
    return length_;
  }

  /**
   * 등록된 grpc 스트림 전송 객체를 반환한다.
   */
  WriterInterface<SpeakResponse> *GetStream() {
    return stream_;
  }

  Lang lang() {
    return lang_;
  }
  AudioEncoding encoding() {
    return encoding_;
  }
  int sample_rate() {
    return sample_rate_;
  }

 private:
  int GetFormat() {
    int speech_format = WF_M16;
    if (encoding_ == AudioEncoding::LINEAR16 && sample_rate_ == 8000) {
      speech_format = WF_L8;
    } else if (encoding_ == AudioEncoding::LINEAR16 && sample_rate_ == 16000) {
      speech_format = WF_L16;
    } else if (encoding_ == AudioEncoding::MULAW && sample_rate_ == 16000) {
      speech_format = WF_M16;
    } else if (encoding_ == AudioEncoding::MULAW && sample_rate_ == 8000) {
      speech_format = WF_M8;
    } else {
      speech_format = WF_M16;
    }
    return speech_format;
  }
  /**
   * SimpleTtsClient API 상태
   */
  enum TtsApiStatus {
    None = 0, // 비어있는 상태
    Created = 1,
    Opened = 2,
    GotChannel = 3
  };
  Lang lang_;
  AudioEncoding  encoding_;
  int sample_rate_;
  CoreTTSClient *client_ = nullptr;
  int32_t length_ = 0;
  TtsApiStatus status_ = TtsApiStatus::None;
  int channel_ = 0;
  WriterInterface<SpeakResponse> *stream_;
  friend class TtsClientFactory;
};

/**
 * SimpleTtsClient 를 위한 API 초기화 및 콜백 관리.
 */
class TtsClientFactory {
  /**
   * 생성자
   */
  TtsClientFactory();
 public:
  SimpleTtsClient *Create(Lang lang,
                          AudioEncoding encoding,
                          int sample_rate);
  /**
   * 소멸자
   */
  virtual ~TtsClientFactory() {
    CleanWSA();
    channels_.clear();
  }

  /**
   * 채널과 클라이언트를 등록한다.
   *
   * @param channel 채널번호
   * @param client 채널을 가지고 있는 클라이언트 객체
   */
  void Register(int channel, SimpleTtsClient *client) {
    LOGGER()->debug("registering chan: {}, client:{}",
                    channel,
                    (void *) client);
    channels_.insert(std::make_pair(channel, client));
  }

  /**
   * 채널에 연결된 클라이언트에 대한 등록 해제.
   *
   * 등록 해제 후 클라이언트를 삭제하지 않는다. 클라이언트는 자체적으로 소멸된다.
   * 실지로는 소멸자의 호출 과정에서 처리된다.
   *
   * FIXME
   * 채널번호가 클라이언트에 의해서 발생되는 경우라면
   * 모든 채널이 항상 1이 아닐지 의심이 된다.
   *
   * 만일 이 경우라면, 채널은 지정해서 사용하면 된다.
   *
   * @param channel 지울 채널 번호
   */
  void Unregister(int channel) {
    auto got = channels_.find(channel);
    if (got != channels_.end()) {
      LOGGER()->debug("TTS: unregister chan: {}", channel);
      SimpleTtsClient *tts = got->second;
      LOGGER()->debug("TTS: Written Length: {}", tts->GetVoiceLength());
      channels_.erase(got);
      // LOGGER()->debug() << "unregister channels count: " << channels_.size();
    }
  }

  /**
   * 채널에 연결된 클라이언트를 반환한다.
   *
   * 클라이언트를 가져오는 이유는 현재 클라이언트에 grpc 스트림 전송 객체가 연결되어
   * 있기 때문이다.
   * @param channel 채널 번호
   * @return SimpleTtsClient 객체
   */
  SimpleTtsClient *GetClient(int channel) {
    auto got = channels_.find(channel);
    return got->second;
  }
  static TtsClientFactory *GetInstance();
 private:
  unordered_map<int, SimpleTtsClient *> channels_;
  /*!< 채널 맵*/
  string server_kr_ip_ = "10.122.64.121";
  /*!< 서버 주소 */
  string server_en_ip_ = "10.122.64.121"; /*!< 서버 주소 */
  // max ipv6 address 45
  int server_kr_port_ = 40050;
  int server_en_port_ = 40040;
  /*!< 포트 */
  static atomic<TtsClientFactory *> instance_;
  /*!< 싱글톤 */
  static mutex mutex_; /*!< 싱글톤 처리 방법 */
};

#endif

#endif //  M2U_TTS_CLIENT_H
