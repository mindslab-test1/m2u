#ifndef SPEECH_SYNTHESIZER_IMPL_H
#define SPEECH_SYNTHESIZER_IMPL_H

#include <memory>
#include <grpc++/grpc++.h>

#include <maum/brain/tts/speech.grpc.pb.h>

using std::unique_ptr;
using grpc::Status;
using grpc::ServerContext;
using grpc::ServerWriter;
using maum::brain::tts::SpeechSynthesizerParam;
using maum::brain::tts::SpeechSynthesizerService;
using maum::brain::tts::SpeakRequest;
using maum::brain::tts::SpeakResponse;


class SpeechSynthesizerImpl final : public SpeechSynthesizerService::Service {
 public:
  SpeechSynthesizerImpl();
  virtual ~SpeechSynthesizerImpl();

  Status Speak(ServerContext *context,
               const SpeakRequest *req,
               ServerWriter<SpeakResponse> *stream) override;
 private:
};


#endif //SPEECH_SYNTHESIZER_IMPL_H
