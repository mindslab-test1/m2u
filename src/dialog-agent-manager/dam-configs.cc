#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

#include "dam-configs.h"

int DamConfigs::last_port_;

/**
 * 설정 값을 읽는다.
 */
DamConfigs::DamConfigs(const string &dam_name) : dam_name_(dam_name) {
  char export_ip[128];
  GetPrimaryIp(export_ip, sizeof export_ip);
  export_ip_ = export_ip;

  libmaum::Config &c = libmaum::Config::Instance();
  export_port_ = (int) c.GetAsInt("dam-svcd.export.port");
  admin_remote_ = c.Get("admin.export");
  pool_remote_ = c.Get("pool.export");
  child_wait_ms_ = (int) c.GetDefaultAsInt("dam-svcd.child.wait", "2000");
  bin_path_ = c.Get("da.dir") + "/";
}
