#include <magic.h>
#include <boost/algorithm/string.hpp>
#include <libmaum/common/config.h>

#include "file-magic.h"


/**
 * libmagic 라이브러리를 초기화하고 기본 database를 로드한다.
 *
 * @return
 */
bool FileMagic::Open() {
  magic_t handle = magic_open(MAGIC_NONE);

  if (handle == NULL) {
    LOGGER()->debug("Failed to initialize magic library");
    return false;
  }

  if (magic_load(handle, NULL) != 0) {
    LOGGER()->debug("faile to load magic database - {}", magic_error(handle));
    magic_close(handle);
    return false;
  }

  handle_ = handle;
  return true;
}

/**
 * 사용이 끝난 라이브러리를 닫는다.
 *
 */
void FileMagic::Close() {
  if (handle_ != nullptr) {
    magic_close(static_cast<magic_t >(handle_));
    handle_ = nullptr;
  }
}

/**
 * File의 형태을 분석한 후 Runtime Environment로 변환하여 리턴한다
 * @param file_path
 * @return
 */
RuntimeEnvironment FileMagic::Inspect(const string &file_path) {
  RuntimeEnvironment runtime_env = RuntimeEnvironment(-1);
  // libmagic 분석 결과 얻기
  const string magic_full = magic_file((magic_t) handle_, file_path.c_str());
  // 확장자 추출
  size_t ext_pos = file_path.find_last_of('.');
  const string &extension = ext_pos < file_path.length() ?
                            file_path.substr(ext_pos) :
                            string();

  // Java: 확장자가 jar이고 'Zip archive data'가 포함된 경우
  if (boost::iequals(extension, ".jar")) {
    if (!boost::ifind_first(magic_full, "Zip archive data").empty()) {
      runtime_env = RuntimeEnvironment::RE_JAVA;
    }
  }

  // Python: 'python script'가 포함된 경우
  if (!RuntimeEnvironment_IsValid(runtime_env)) {
    if (!boost::ifind_first(magic_full, "python script").empty()) {
      runtime_env = RuntimeEnvironment::RE_PYTHON;
    }
  }

  // Native: 'ELF', 'executable', '(GNU/LINUX)'가 모두 포함되고 실행 속성이 있는 경우
  if (!RuntimeEnvironment_IsValid(runtime_env)) {
    if (!boost::ifind_first(magic_full, "ELF").empty()
        && !boost::ifind_first(magic_full, "executable").empty()
        && !boost::ifind_first(magic_full, "(GNU/LINUX)").empty()) {
      struct stat stbuf = {};
      stat(file_path.c_str(), &stbuf);
      if (stbuf.st_mode & S_IXUSR) {
        runtime_env = RuntimeEnvironment::RE_NATIVE;
      } else {
        LOGGER()->debug(">> Execution attribute is missing.");
      }
    }
  }

  // 그 외는 아직 지원하지 않는다.
  if (!RuntimeEnvironment_IsValid(runtime_env)) {
    size_t comma = magic_full.find_first_of(',');
    LOGGER()->debug("** Not Supported - {}, {}",
                    file_path,
                    magic_full.substr(0, comma));
  }

  return runtime_env;
}


