#include <chrono>

using namespace std::chrono;

class ElapsedTimer {
 public:
  ElapsedTimer() {
    start_ = system_clock::now();
  }
  virtual ~ElapsedTimer() = default;

  long GetTookMs() {
    auto took = duration_cast<milliseconds>(system_clock::now() - start_);
    return took.count();
  }

 private:
  system_clock::time_point start_;
};
