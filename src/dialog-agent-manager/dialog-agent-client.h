#ifndef DIALOG_AGENT_CLIENT_H
#define DIALOG_AGENT_CLIENT_H

#include "dialog-agent-manager-types.h"

/**
 * Dialog Agent Server 연결 gPRC client
 */
class DialogAgentClient {
 public:
  DialogAgentClient() = delete;
  DialogAgentClient(const string &endpoint,
                    const DialogAgentProviderSpec da_prod_spec);
  virtual ~DialogAgentClient() = default;

  bool CheckStubReady(int max_wait);
  Status IsReady(DialogAgentStatus &das);
  Status Init(maum::m2u::da::InitParameter &init_param,
              DialogAgentProviderParam &provider_param);
  Status GetRuntimeParameters(RuntimeParameterList &params);
  Status GetProviderParameter(DialogAgentProviderParam &prod_param);
  Status GetUserAttributes(UserAttributeList &user_attr_list);
  Status Terminate();

 private:
  string endpoint_;
  DialogAgentProviderSpec prod_spec_;
  std::shared_ptr<grpc::Channel> channel_;
  ProviderStub stub_;
};

#endif //DIALOG_AGENT_CLIENT_H
