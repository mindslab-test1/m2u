#include <fstream>

#include <dirent.h>
#include <dareg-client.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <maum/m2u/da/provider.grpc.pb.h>
#include <maum/m2u/da/v1/talk.grpc.pb.h>
#include <maum/m2u/da/v2/talk.grpc.pb.h>
#include <maum/m2u/da/v2p1/talk.grpc.pb.h>
#include <maum/m2u/da/v3/talk.grpc.pb.h>

#include "file-magic.h"
#include "dam-configs.h"
#include "da-manager.h"
#include "elapsed-timer.h"

using std::atomic;
using std::unique_ptr;
using std::to_string;
using std::ifstream;
using std::ofstream;
using std::ios_base;
using google::protobuf::Empty;
using grpc::ClientContext;
using namespace maum::m2u;
using maum::m2u::da::DialogAgentState;
using maum::m2u::server::RegisterResponse;
using maum::m2u::console::DialogAgentList;
using maum::m2u::console::DialogAgentAdmin;
using maum::m2u::console::DialogAgentInstance;
using maum::m2u::console::DialogAgentInstanceList;
using maum::m2u::console::DialogAgentWithDialogAgentInstance;
using maum::m2u::console::Key;

using hzc = GlobalHazelcastClient;
using LaunchResult = maum::m2u::server::DialogAgentManagerRegisterResult;
using DaExecSpec = maum::m2u::server::DialogAgentExecutables_DialogAgentSpec;
using ProviderV3Stub = da::v3::DialogAgentProvider::Stub;
using ProviderV2p1Stub = da::v2p1::DialogAgentProvider::Stub;
using ProviderV2Stub = da::v2::DialogAgentProvider::Stub;
using ProviderV1Stub = da::v1::DialogAgentProvider::Stub;

atomic<::DialogAgentManager *> DialogAgentManager::instance_;
mutex DialogAgentManager::mutex_;

DialogAgentManager *DialogAgentManager::GetInstance() {
  DialogAgentManager *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new DialogAgentManager;
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

/**
 * 생성자
 *
 * Map Listener를 설정하고 등록되어 있는 모든 DA, DAI를 재기동한다.
 */
DialogAgentManager::DialogAgentManager() {
  // DAM 이름 확인
  string dam_name;
  if (ReadDamStateFile(dam_name)) {
    auto logger = LOGGER();
    if (dam_name.empty()) {
      logger->warn("********* WARNING *************");
      logger->warn("* DAM Name is not registered. *");
      logger->warn("*******************************");
    } else {
      logger->info("*******************************");
      logger->info("        DAM Name: {}", dam_name);
      logger->info("*******************************");
    }
  } else {
    ResetDamStateFile();
    LOGGER()->debug("dam.state.json file is created");
  }

  configs_ = new DamConfigs(dam_name);
}

/**
 * 소멸자
 *
 * Map Listener를 제거한다.
 */
DialogAgentManager::~DialogAgentManager() {
  RemoveListener();

  if (daai_handler_ != nullptr) {
    daai_handler_->RemoveListener();
    delete daai_handler_;
  }
#if 0
  if (daiex_handler != nullptr) {
    daiex_handler->RemoveListener();
    delete daiex_handler;
  }
#endif
  if (configs_ != nullptr) {
    delete configs_;
  }
}

/**
 * '$MAUM_ROOT/run/dam.state.json'파일에서 등록된 DAM이름을 읽어 온다.
 *
 * @param dam_name
 * @return
 */
bool DialogAgentManager::ReadDamStateFile(string &dam_name) {
  if (dam_state_file_.empty()) {
    libmaum::Config &c = libmaum::Config::Instance();
    dam_state_file_ = c.Get("run.dir") + '/' + c.Get("dam-svcd.state.file");
  }
  ifstream ifs(dam_state_file_);
  Json::Value root;
  Json::Reader reader;
  std::string errs;
  bool ok = reader.parse(ifs, root);
  ifs.close();

  if (ok) {
    dam_name = root["dam_name"].asString();
  } else {
    LOGGER()->info("cannot parsing dam state json {} {}",
                   dam_state_file_,
                   reader.getFormattedErrorMessages());
    dam_name.clear();
  }
  return ok;
}

/**
 * '$MAUM_ROOT/run/dam.state.json'파일에 등록된 DAM이름을 쓴다.
 *
 * @param dam_name
 * @return
 */
void DialogAgentManager::WriteDamStateFile(const string &dam_name) {
  if (dam_state_file_.empty()) {
    libmaum::Config &c = libmaum::Config::Instance();
    dam_state_file_ = c.Get("run.dir") + '/' + c.Get("dam-svcd.state.file");
  }

  Json::Value root;
  ofstream ofs(dam_state_file_, ios_base::trunc | ios_base::out);
  root["dam_name"] = dam_name;
  Json::StyledStreamWriter styledStreamWriter;
  styledStreamWriter.write(ofs, root);
  ofs << std::endl;
  ofs.close();
}

void DialogAgentManager::Initialize() {
  // DA, DAI Handler 생성
  daai_handler_ = new DialogAgentAIHandler(*configs_);
#if 0
  daiex_handler = new DialogAgentIExHandler(*configs_);
  if (daai_handler == nullptr || daiex_handler == nullptr) {
#endif
  if (daai_handler_ == nullptr /*|| daiex_handler == nullptr*/) {
    auto logger = LOGGER();
    logger->critical("************ ERROR *************");
    logger->critical("    DA Handler({})", (void *) daai_handler_);
#if 0
    logger->critical("    DAI Handler({})", (void *) daiex_handler);
#endif
    logger->critical("   Exit Dialog Agent Manager.");
    logger->critical("********************************");
    logger->flush();
    exit(-1);
  }

  if (!GetDamName().empty()) {
    vector<DialogAgentManagerPortable> dams;
    {
      auto dam_map = hzc::Instance().GetHazelcastClient()
          ->getMap<string, DialogAgentManagerPortable>(hzc::kNsDAManager);
      dams = dam_map.values(SqlPredicate(" name='" + GetDamName() + "'"));
    }
    if (dams.empty()) {
      auto logger = LOGGER();
      logger->warn("*************** WARNING ***************");
      logger->warn("*  Registered DAM Name is not valid.  *");
      logger->warn("*  Reset it.                          *");
      logger->warn("***************************************");
      logger->flush();
      configs_->SetDamName("");
      ResetDamStateFile();
      return;
    }

#if 0
    // 종료되지 않은 DAI 정리
    auto zombies = daiex_handler->ListUpZombies();
#endif

    // DAM이 Active일 경에만 수행
    if (dams[0].active()) {
      AddListener();
      Activate();
    } else {
      LOGGER()->info(">>> DAM {} is not Active.");
    }
#if 0
    daiex_handler->CleanUpZombies(zombies);
#endif
  }
}

/**
 * Dialog Agent Manager name을 json 파일에 등록한다.
 *
 */
DamResult DialogAgentManager::SetDamName(const DamKey *dam_key) {
  const string &dam_name = dam_key->name();
  DamResult dam_result;

  if (!GetDamName().empty()) {
    LOGGER()->debug("DAM Name[{}] is already registered.", GetDamName());
    dam_result.set_result_code(LaunchResult::DIALOG_AGENT_MANAGER_ADD_FAILED);
    dam_result.set_detail_message("DAM Name is already registered."
                                  " Delete it first.");
    return dam_result;
  }

  vector<DialogAgentManagerPortable> dams;
  {
    auto dam_map = hzc::Instance().GetHazelcastClient()
        ->getMap<string, DialogAgentManagerPortable>(hzc::kNsDAManager);
    dams = dam_map.values(SqlPredicate("name='" + dam_name + "'"));
  }

  if (!dams.empty()) {
    LOGGER()->debug("DAM Name[{}] is already registered.", dam_name);
    DamResult dam_result;
    dam_result.set_result_code(LaunchResult::DIALOG_AGENT_MANAGER_ADD_FAILED);
    dam_result.set_detail_message("DAM Name is already registered in Map."
                                  " Delete it first.");
    return dam_result;
  }

  WriteDamStateFile(dam_name);
  LOGGER()->debug("DAM Name[{}] has added to dam.state.json file", dam_name);
  configs_->SetDamName(dam_name);
  this->AddListener();

  dam_result.set_result_code(LaunchResult::DIALOG_AGENT_MANAGER_ADD_SUCCESS);
  dam_result.set_detail_message("DAM Name has added to dam.state.json file");

  return dam_result;
}

/**
 * Launch DA, DAI
 * Add DAAI, DAIEX Listener
 */
void DialogAgentManager::Activate() {
  auto logger = LOGGER();

  // Map에 등록된 DA, DAI 기동
  ElapsedTimer timer;
  logger->info(">>> Launch Dialog Agents.");
  daai_handler_->LaunchDialogAgents();
#if 0
  logger->info(">>> Launch Dialog Agent Intances.");
  daiex_handler->LaunchDialogAgentInstances();
#endif
  logger->info("--------------------------------------");
  logger->debug("### Launching DAs/DAIs took {} ms.", timer.GetTookMs());
  logger->info("--------------------------------------");
  DumpRunningStatus();
  logger->info("--------------------------------------");
  // DA, DAI Map listener 설정
  daai_handler_->AddListener();
#if 0
  daiex_handler->AddListener();
#endif
  is_active_ = true;
}

/**
 * Remove DAAI, DAIEX Listener
 * Unregister DAIR
 */
void DialogAgentManager::Deactivate() {
  is_active_ = false;

#if 0
  daiex_handler->RemoveListener();
#endif
  daai_handler_->RemoveListener();
#if 0
  daiex_handler->DeactivateDialogAgentInstances();
#endif
  daai_handler_->DeactivateDialogAgents();
}

void DialogAgentManager::AddListener() {
  if (!registration_id_.empty()) {
    LOGGER()->debug("DialogAgentManager::AddListener "
                    "Listener already Registered.");
    return;
  }
  registration_id_ = hzc::Instance().GetHazelcastClient()
      ->getMap<string, DialogAgentManagerPortable>(hzc::kNsDAManager)
      .addEntryListener(*this, true);
}

void DialogAgentManager::RemoveListener() {
  if (!registration_id_.empty()) {
    hzc::Instance().GetHazelcastClient()
        ->getMap<string, DialogAgentManagerPortable>(hzc::kNsDAManager)
        .removeEntryListener(registration_id_);
    registration_id_.clear();
  }
}

/**
 * DAManager Map에 Entry가 추가되었을 때
 * DAM 추가는 명시적인 SetDamName()을 call하므로 사용하지 않는다.
 *
 * @param event entry event
 */
void DialogAgentManager::entryAdded(const DAMInfoEvent &event) {
  auto logger = LOGGER();
  const DialogAgentManagerPortable added_dam = event.getValue();
  logger->info("Dialog Agent Manager Added: {} / {}:{} / {}",
               added_dam.name(),
               added_dam.ip(),
               to_string(added_dam.port()),
               added_dam.active());

  if (added_dam.name() != GetDamName()) {
    logger->info("DAM Name differs: {}/{}", added_dam.name(), GetDamName());
    return;
  }

  if (added_dam.active() == is_active_) {
    logger->info("DAM Active not changed: {}/{}",
                 added_dam.active(),
                 is_active_);
    return;
  }

  std::thread([=]() {
    if (added_dam.active()) {
      LOGGER()->info(">>> Activate DAM{[]}.", GetDamName());
      Activate();
    } else {
      LOGGER()->info(">>> Deactivate DAM{[]}.", GetDamName());
      Deactivate();
    }
  }).detach();
}

/**
 * DAManager Map에 Entry가 삭제되었을 때, 삭제된 DAM를 중지한다.
 *
 * @param event entry event
 */
void DialogAgentManager::entryRemoved(const DAMInfoEvent &event) {
  const string &removed_key = event.getKey();

  if (removed_key != GetDamName()) {
//    LOGGER()->info("Invalid DAM Name: {}", removed_key);
    return;
  }

  LOGGER()->info("Dialog Agent Manager Removed: {}", removed_key);

  std::thread([=]() {
    RemoveListener();
    Deactivate();
#if 0
    daiex_handler->ClearDialogAgentInstances();
    daai_handler->ClearDialogAgents();
#endif
    daai_handler_->ClearDialogAgents();
    configs_->SetDamName("");
    ResetDamStateFile();
  }).detach();
}

/**
 * DAActivation Map에 Entry가 업데이트되었을 때, 기존 DA를 종료 후 재실행한다.
 *
 * @param event entry event
 */
void DialogAgentManager::entryUpdated(const DAMInfoEvent &event) {
  const DialogAgentManagerPortable cur_dam = event.getValue();

  if (cur_dam.name() != GetDamName()) {
//    LOGGER()->info("Invalid DAM Name: {}", cur_dam.name());
    return;
  }

  LOGGER()->info("Dialog Agent Manager updated: ==> {} / {}:{} / {}",
                 cur_dam.name(),
                 cur_dam.ip(),
                 cur_dam.port(),
                 cur_dam.active());

  if (cur_dam.active() == is_active_) {
    LOGGER()->info("Active is not changed. Do nothing.");
    return;
  }

  std::thread([=]() {
    if (cur_dam.active()) {
      LOGGER()->info(">>> Activate DAM[{}].", GetDamName());
      Activate();
    } else {
      LOGGER()->info(">>> Deactivate DAM[{}].", GetDamName());
      Deactivate();
    }
  }).detach();
}

/**
* Invoked when an entry is evicted.
*
* @param event entry event
*/
void DialogAgentManager::entryEvicted(const DAMInfoEvent &event) {
  LOGGER()->debug("DialogAgentManager::{} is Unimplemented.", __func__);
}

/**
* Invoked upon expiration of an entry.
*
* @param event the event invoked when an entry is expired.
*/
void DialogAgentManager::entryExpired(const DAMInfoEvent &event) {
  LOGGER()->debug("DialogAgentManager::{} is Unimplemented.", __func__);
}

/**
*  Invoked after WAN replicated entry is merged.
*
* @param event the event invoked when an entry is expired.
*/
void DialogAgentManager::entryMerged(const DAMInfoEvent &event) {
  LOGGER()->debug("DialogAgentManager::{} is Unimplemented.", __func__);
}

/**
* Invoked when all entries evicted by {@link IMap#evictAll()}.
*
* @param event map event
*/
void DialogAgentManager::mapEvicted(const MapEvent &event) {
  LOGGER()->debug("DialogAgentManager::{} is Unimplemented.", __func__);
}

/**
* Invoked when all entries are removed by {@link IMap#clear()}.}
*/
void DialogAgentManager::mapCleared(const MapEvent &event) {
  LOGGER()->debug("DialogAgentManager::{} is Unimplemented.", __func__);
}

Status DialogAgentManager::GetRuntimeParameters(
    const DialogAgentName *da_name,
    RuntimeParameterList *rpl) {
  const ActiveDialogAgent *da = GetActiveDialogAgent(da_name->da_name());

  if (da) {
    *rpl = da->rt_param_list;
    return Status::OK;
  } else {
    LOGGER()->info("{}: DA is not active.", __func__);
    return Status(StatusCode::UNAVAILABLE, "DA is not active.");
  }
}

Status DialogAgentManager::GetProviderParameter(
    const DialogAgentName *da_name,
    DialogAgentProviderParam *dapp) {
  const ActiveDialogAgent *da = GetActiveDialogAgent(da_name->da_name());

  if (da) {
    *dapp = da->da_prod_param;
    return Status::OK;
  } else {
    LOGGER()->info("{}: DA is not active.", __func__);
    return Status(StatusCode::UNAVAILABLE, "DA is not active.");
  }
}

Status DialogAgentManager::GetUserAttributes(
    const DialogAgentName *da_name,
    UserAttributeList *ual) {
  const ActiveDialogAgent *da = GetActiveDialogAgent(da_name->da_name());

  if (da) {
    *ual = da->user_attr_list;
    return Status::OK;
  } else {
    LOGGER()->info("{}: DA is not active.", __func__);
    return Status(StatusCode::UNAVAILABLE, "DA is not active.");
  }
}

void DialogAgentManager::UnregisterChild(pid_t pid, DamResult &stat) {
  string key;

  {
    std::lock_guard<std::mutex> lock(GetDaiMapMutex());
    DamResult da_status;
    auto run_dainst =
        find_if(GetRunningDaiMap().begin(), GetRunningDaiMap().end(),
                [pid](const RunInstPair &t) -> bool {
                  return t.second.pid == pid;
                });
    if (run_dainst == GetRunningDaiMap().end()) {
      stat.set_result_code(LaunchResult::CHILD_RESTART_FAILED);
      stat.set_detail_message("cannot find process");
      return;
    }
    key = run_dainst->first;
    GetRunningDaiMap().erase(key);
  }
#if 0
  if (daiex_handler->UnregisterDialogAgentInstance(key)) {
    stat.set_result_code(LaunchResult::SUCCESS);
  } else {
    stat.set_result_code(LaunchResult::DIALOG_AGENT_REMOVE_FAILED);
    stat.set_detail_message("Domain Unregister is failed");
  }
#endif
  stat.set_result_code(LaunchResult::DIALOG_AGENT_REMOVE_FAILED);
  stat.set_detail_message("Domain Unregister is failed");
}

/**
 * 실행 가능한 DA 목록을 반환한다.
 *
 * @param da_names DA 목록
 * @return 실행 성공 여부
 */
Status DialogAgentManager::GetExecutableDialogAgents(
    DialogAgentExecutables *da_execs) {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();

  // DA directory를 가져온다.
  string da_path = c.Get("da.dir");
  DIR *dir;
  logger->debug("DA Directory: {}", da_path);
  dir = opendir(da_path.c_str());
  if (dir == nullptr) {
    logger->error("Cannot open directory : {}", da_path);
    return Status::OK;
  }

  // DA directory의 파일들을 검사하여 지원하는 파일들을 추가한다.
  FileMagic magic;
  if (!magic.Open()) {
    logger->error("Cannot look into a file.");
    return Status::OK;
  }

  struct dirent *ent;
  while ((ent = readdir(dir)) != NULL) {
    if ((strncmp(ent->d_name, "..", ent->d_reclen) == 0)
        || (strncmp(ent->d_name, ".", ent->d_reclen) == 0)
        || (strncmp(ent->d_name, ".ctx", ent->d_reclen) == 0)
        || (strncmp(ent->d_name, ".cdt", ent->d_reclen) == 0)) {
      continue;
    }

    // 유효하면서 실행할 수 있는 파일을 결과에 추가한다.
    string fullpath = da_path + "/" + ent->d_name;
    RuntimeEnvironment runtime_env = magic.Inspect(fullpath);
    if (RuntimeEnvironment_IsValid(runtime_env)) {
      auto da_spec = da_execs->add_executable_das();
      da_spec->set_da_name(ent->d_name);
      da_spec->set_runtime_env(runtime_env);
    }
  }
  magic.Close();
  closedir(dir);

  std::sort(da_execs->mutable_executable_das()->begin(),
            da_execs->mutable_executable_das()->end(),
            [](DaExecSpec a, DaExecSpec b) {
              return a.da_name() < b.da_name();
            });

  for (auto &exda : da_execs->executable_das()) {
    logger->debug("DA - {} [{}]",
                  exda.da_name(),
                  RuntimeEnvironment_Name(exda.runtime_env()));
  }

  return Status::OK;
}

/**
 * 실행 중인 DA의 정보를 가져온다.
 *
 * @param da_name
 * @return
 */
const ActiveDialogAgent *DialogAgentManager::GetActiveDialogAgent(
    const string &da_name) {
  std::lock_guard<std::mutex> lock(GetDaMapMutex());
  auto target = GetActiveDaMap().find(da_name);
  return target != GetActiveDaMap().end() ? &target->second : nullptr;
}

void DialogAgentManager::DumpRunningStatus() {
  if (daai_handler_) {
    daai_handler_->DumpDialogAgentStatus();
  }
#if 0
  if (daiex_handler) {
    LOGGER()->info("--------------------------------------");
    daiex_handler->DumpDialogAgentInstanceStatus();
  }
#endif
}

/**
 * 요청한 `pid`에 일치하는 자식 프로세스를 찾아서 반환한다.
 * @param pid 찾으려는 자식 프로세스의 pid
 * @param child 찾은 자식 프로세스 상세 적보
 * @return 찾으면 true를 반환합니다.
 */
bool DialogAgentManager::FindChild(pid_t pid, RunningChild &child) {
  {
    std::lock_guard<std::mutex> lock(GetDaiMapMutex());
    auto run_it = find_if(GetRunningDaiMap().begin(), GetRunningDaiMap().end(),
                          [pid](const RunInstPair &t) -> bool {
                            return t.second.pid == pid;
                          });
    if (run_it == GetRunningDaiMap().end()) {
      LOGGER()->debug(" $$ the child is not in DAI map.");
      return false;
    }
    child.pid = pid;
    child.inst = run_it->second;
  }

  try {
    std::lock_guard<std::mutex> lock(GetDaMapMutex());
    child.exec_da = GetActiveDaMap().at(child.inst.agent);
    return true;
  } catch (const std::out_of_range &oor) {
    LOGGER()->debug(" $$ the child's agent is not in DA map.");
    return false;
  }
}

DamResult DialogAgentManager::HandleChildExit(pid_t pid) {
  RunningChild child;
  if (!FindChild(pid, child)) {
    DamResult res;
    res.set_result_code(LaunchResult::DAINSTANCE_IS_NOT_FOUND_IN_MAP);
    res.set_detail_message("the child is not found.");
    return res;
  }

  // 아래 로직이 잘 이해가 안됩니다. ㅠㅠㅠ..
  // FIXME ..
  // Child Process is domain
  DamResult res;

#if 0
  if (daiex_handler->IsDialogAgentInstanceRunning(child.inst)) {
    res = daiex_handler->RestartDialogAgentInstance(child.inst.param,
                                                    child.exec_da);
  } else {
    UnregisterChild(pid, res);
    if (res.result_code() == LaunchResult::SUCCESS) {
      LOGGER()->debug(" $$ Unregister success {}", res.result_code());
    } else {
      LOGGER()->debug(" $$ Unregister failed {} {}",
                      res.result_code(),
                      res.detail_message());
    }
  }
#endif
  UnregisterChild(pid, res);
  if (res.result_code() == LaunchResult::SUCCESS) {
    LOGGER()->debug(" $$ Unregister success {}", res.result_code());
  } else {
    LOGGER()->debug(" $$ Unregister failed {} {}",
                    res.result_code(),
                    res.detail_message());
  }
  return res;
}
