#ifndef M2U_DAM_HAZELCAST_H
#define M2U_DAM_HAZELCAST_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/ObjectDataOutput.h>
#include <hazelcast/client/serialization/ObjectDataInput.h>
#include <hazelcast/client/query/SqlPredicate.h>

#include <google/protobuf/stubs/port.h>

using std::string;

// Hazelcast namespace
using hazelcast::client::HazelcastClient;
using hazelcast::client::ClientConfig;
using hazelcast::client::exception::IException;
using hazelcast::client::EntryListener;
using hazelcast::client::EntryEvent;
using hazelcast::client::MapEvent;
using hazelcast::client::query::SqlPredicate;

using google::protobuf::int64;

enum PortableFactoryId {
  PORTABLE_FACTORY_ID = 1
};

/**
 * HazelcastTypeId는 중복되지 않도록 enum 으로 선언하여 사용.
 *
 * 참고 : Mapstore Java 소스에서 정의한 class id와 같아야 한다.
 */
enum HazelcastTypeId {
  DIALOG_AGENT_MANAGER = 1,
  DIALOG_AGENT = 2,
  DIALOG_AGENT_ACTIVATION_INFO = 3,
  DIALOG_AGENT_INSTANCE = 10,
  DIALOG_AGENT_INSTANCE_EXECUTION_INFO = 11,
  DIALOG_AGENT_INSTANCE_RESOURCE = 12,
};

class GlobalHazelcastClient {
  GlobalHazelcastClient();
  virtual ~GlobalHazelcastClient();

 public:
  static GlobalHazelcastClient &Instance() {
    static GlobalHazelcastClient instance_;
    return instance_;
  }

  virtual std::shared_ptr<ClientConfig> GetConfig();
  std::shared_ptr<HazelcastClient> GetHazelcastClient();

  static const string kNsAuthPolicy;
  static const string kNsChatbotDetail;
  static const string kNsDAManager;
  static const string kNsDA;
  static const string kNsDAActivation;
  static const string kNsDAInstance;
  static const string kNsDAInstExec;
  static const string kNsDAIR;
  static const string kNsIntentFinder;
  static const string kNsITFInstExec;
  static const string kNsSkill;
  static const string kNsSC;
  static const string kNsQA;

  /** Session ID AtomicLong */
  static const string kNsSessionId;

  /** 대화 for v1,v2 [ Deprecated ] */
  static const string kNsTalk;
  /** 세션 for v1,v2 [ Deprecated ] */
  static const string kNsSession;
  /** 대화 for v3 */
  static const string kNsTalkV3;
  /** 세션 for v3 */
  static const string kNsSessionV3;
  /** 다음 대화 */
  static const string kNsTalkNext;
  /** 챗봇 */
  static const string kNsChatbot;
  /** Device 정보 */
  static const string kNsDevices;

  static bool IsReady();

 private:
  int32_t server_cnt_;
  std::list<string> server_address_;
  int32_t server_port_;
  std::shared_ptr<HazelcastClient> hazelcast_client_;
  string group_name_;
  string group_password_;
  long invocation_timeout_;
};

#endif // M2U_ROUTER_HAZELCAST_H
