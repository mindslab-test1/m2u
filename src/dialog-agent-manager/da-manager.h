#ifndef DIALOG_AGENT_MANAGER_H
#define DIALOG_AGENT_MANAGER_H

#include <string>
#include <atomic>

#include "hazelcast.h"
#include "dialog-agent-manager-types.h"
#include "dialog-agent-manager-portable.h"
#include "dialog-agent-ai-handler.h"
#include "dialog-agent-iex-handler.h"

using DAMInfoEvent = EntryEvent<string, DialogAgentManagerPortable>;

class DialogAgentManager
    : protected EntryListener<string, DialogAgentManagerPortable> {
  DialogAgentManager();
 public:
  static DialogAgentManager *GetInstance();
  virtual ~DialogAgentManager();
  void Initialize();

  Status GetRuntimeParameters(const DialogAgentName *da_name,
                              RuntimeParameterList *r);
  Status GetProviderParameter(const DialogAgentName *da_name,
                              DialogAgentProviderParam *r);
  Status GetUserAttributes(const DialogAgentName *da_name,
                           UserAttributeList *r);
  Status GetExecutableDialogAgents(DialogAgentExecutables *da_execs);
  DamResult HandleChildExit(pid_t q);
  DamResult SetDamName(const DamKey *dam_key);

 protected:
  void Activate();
  void Deactivate();
  void AddListener();
  void RemoveListener();

  /** Invoked when an entry is added. */
  void entryAdded(const DAMInfoEvent &event) override;
  /** Invoked when an entry is removed. */
  void entryRemoved(const DAMInfoEvent &event) override;
  /** Invoked when an entry is updated. */
  void entryUpdated(const DAMInfoEvent &event) override;
  /** Invoked when an entry is evicted. */
  void entryEvicted(const DAMInfoEvent &event) override;
  /** Invoked upon expiration of an entry. */
  void entryExpired(const DAMInfoEvent &event) override;
  /** Invoked after WAN replicated entry is merged. */
  void entryMerged(const DAMInfoEvent &event) override;
  /** Invoked when all entries evicted by {@link IMap#evictAll()}. */
  void mapEvicted(const MapEvent &event) override;
  /** Invoked when all entries are removed by {@link IMap#clear()}.} */
  void mapCleared(const MapEvent &event) override;

 private:
  bool ReadDamStateFile(string &dam_name);
  void WriteDamStateFile(const string &dam_name);
  inline void ResetDamStateFile() { WriteDamStateFile(""); }

  inline const string &GetDamName() { return configs_->GetDamName(); }
  inline const string &GetBinPath() { return configs_->GetBinPath(); }
  inline const string &GetExportIp() { return configs_->GetExportIp(); }
  inline int GetExportPort() { return configs_->GetExportPort(); }
  inline const string &GetAdminRemote() { return configs_->GetAdminRemote(); }
  inline const string &GetPoolRemote() { return configs_->GetPoolRemote(); }
  inline int &GetLastPort() { return configs_->GetLastPort(); }
  inline int GetChildWaitMs() { return configs_->GetChildWaitMs(); }
  inline mutex &GetDaMapMutex() { return configs_->GetDaMapMutex(); }
  inline mutex &GetDaiMapMutex() { return configs_->GetDaiMapMutex(); }
  inline ActiveDA &GetActiveDaMap() { return configs_->GetActiveDaMap(); }
  inline RunInst &GetRunningDaiMap() { return configs_->GetRunningDaiMap(); }

  const ActiveDialogAgent *GetActiveDialogAgent(const string &da_name);

  // DUMP
  void DumpRunningStatus();

  // PROCESS CHECK
  bool FindChild(pid_t pid, RunningChild &child);
  void UnregisterChild(pid_t pid, DamResult &);

  bool is_active_;
  string dam_state_file_;
  string registration_id_;
  DamConfigs *configs_;
  DialogAgentAIHandler *daai_handler_;
#if 0
  DialogAgentIExHandler *daiex_handler;
#endif

  // singleton
  static std::mutex mutex_;
  static std::atomic<DialogAgentManager *> instance_;
};

#endif // DIALOG_AGENT_MANAGER_H