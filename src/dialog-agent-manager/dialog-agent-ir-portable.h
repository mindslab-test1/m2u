#ifndef DIALOG_AGENT_INSTANCE_RESOURCE_PORTABLE_H
#define DIALOG_AGENT_INSTANCE_RESOURCE_PORTABLE_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include <maum/m2u/server/pool.pb.h>

#include "hazelcast.h"

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

class DialogAgentIRPortable
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::server::DialogAgentInstanceResource {
 public:
  DialogAgentIRPortable() = default;
  DialogAgentIRPortable(const DialogAgentIRPortable &rhs) {
    this->CopyFrom(rhs);
  }
  ~DialogAgentIRPortable() override = default;

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  };
  int getClassId() const override {
    return HazelcastTypeId::DIALOG_AGENT_INSTANCE_RESOURCE;
  };
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeByteArray("_msg", &data);
    out.writeUTF("key", &this->key());
    out.writeUTF("dai_id", &this->dai_id());
    out.writeUTF("name", &this->name());
    out.writeUTF("chatbot", &this->chatbot());
    out.writeInt("lang", this->lang());
    out.writeUTF("dam_name", &this->dam_name());
    out.writeUTF("server_ip", &this->server_ip());
    out.writeInt("server_port", this->server_port());
    out.writeInt("launch_type", this->launch_type());
    out.writeUTF("launcher", &this->launcher());
    out.writeInt("pid", this->pid());
    out.writeLong("started_at", this->started_at().seconds());
  };
  void readPortable(PortableReader &in) override {
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  };
};

#endif
