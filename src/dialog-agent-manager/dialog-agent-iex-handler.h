#ifndef DIALOG_AGENT_INSTANCE_HANDLER_H
#define DIALOG_AGENT_INSTANCE_HANDLER_H

#include <string>
#include <atomic>
#include <unordered_map>
#include <json/json.h>
#include <grpc++/grpc++.h>
#include <maum/m2u/server/dam.grpc.pb.h>
#include <maum/m2u/console/da.grpc.pb.h>

#include "hazelcast.h"
#include "dialog-agent-client.h"
#include "dialog-agent-iex-portable.h"
#include "dialog-agent-ir-portable.h"
#include "dialog-agent-manager-types.h"
#include "dam-configs.h"

using DaiExInfo = DialogAgentIExInfoPortable;
using DaiExInfoEvent = EntryEvent<string, DaiExInfo>;

/**
 * Dialog Agent Instance 등록/삭제, 실행 정지를 관리한다.
 * Hazelcast kNsDAActivation("dialog-agent-activation") Map을 listene한다.
 */
class DialogAgentIExHandler : protected EntryListener<string, DaiExInfo> {
 public:
  DialogAgentIExHandler() = delete;
  DialogAgentIExHandler(DamConfigs &configs) : configs_(configs) {};
  ~DialogAgentIExHandler() override;

  void AddListener();
  void RemoveListener();
  vector<DialogAgentIRPortable> ListUpZombies();
  void CleanUpZombies(vector<DialogAgentIRPortable> &zombies);
  void LaunchDialogAgentInstances();
  void DeactivateDialogAgentInstances();
  void ClearDialogAgentInstances();
  void DumpDialogAgentInstanceStatus();
  bool IsDialogAgentInstanceRunning(const RunningDialogAgentInstance &run_inst);
  bool RegisterDialogAgentInstance(DialogAgentIRPortable &dair);
  bool UnregisterDialogAgentInstance(const string &key);
  DamResult RunDialogAgentInstance(const RunDialogAgentInstanceParam &run_param,
                                   const ActiveDialogAgent &exec_da,
                                   int pre_port);
  DamResult StopDialogAgentInstance(const string &key);
  DamResult RestartDialogAgentInstance(
      const RunDialogAgentInstanceParam &prev_param,
      const ActiveDialogAgent &exec_da);

 protected:
  inline const string &GetDamName() { return configs_.GetDamName(); }
  inline const string &GetBinPath() { return configs_.GetBinPath(); }
  inline const string &GetExportIp() { return configs_.GetExportIp(); }
  inline int GetExportPort() { return configs_.GetExportPort(); }
  inline const string &GetAdminRemote() { return configs_.GetAdminRemote(); }
  inline const string &GetPoolRemote() { return configs_.GetPoolRemote(); }
  inline int &GetLastPort() { return configs_.GetLastPort(); }
  inline int GetChildWaitMs() { return configs_.GetChildWaitMs(); }
  inline mutex &GetDaMapMutex() { return configs_.GetDaMapMutex(); }
  inline mutex &GetDaiMapMutex() { return configs_.GetDaiMapMutex(); }
  inline ActiveDA &GetActiveDaMap() { return configs_.GetActiveDaMap(); }
  inline RunInst &GetRunningDaiMap() { return configs_.GetRunningDaiMap(); }

  void StartDialogAgentInstances(const string &dai_id, int cnt);
  void StopDialogAgentInstances(const string &dam_name, const string &dai_id);

  /** Invoked when an entry is added. */
  void entryAdded(const DaiExInfoEvent &event) override;
  /** Invoked when an entry is removed. */
  void entryRemoved(const DaiExInfoEvent &event) override;
  /** Invoked when an entry is updated. */
  void entryUpdated(const DaiExInfoEvent &event) override;
  /** Invoked when an entry is evicted. */
  void entryEvicted(const DaiExInfoEvent &event) override;
  /** Invoked upon expiration of an entry. */
  void entryExpired(const DaiExInfoEvent &event) override;
  /** Invoked after WAN replicated entry is merged. */
  void entryMerged(const DaiExInfoEvent &event) override;
  /** Invoked when all entries evicted by {@link IMap#evictAll()}. */
  void mapEvicted(const MapEvent &event) override;
  /** Invoked when all entries are removed by {@link IMap#clear()}.} */
  void mapCleared(const MapEvent &event) override;

  DamConfigs &configs_;
  string registration_id_;

  const string &getMapName() { return GlobalHazelcastClient::kNsDAInstExec; };
};

#endif //DIALOG_AGENT_INSTANCE_HANDLER_H
