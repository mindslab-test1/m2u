#ifndef DIALOG_AGENT_HANDLER_H
#define DIALOG_AGENT_HANDLER_H

#include <string>
#include <atomic>
#include <unordered_map>
#include <json/json.h>
#include <grpc++/grpc++.h>
#include <maum/m2u/server/dam.grpc.pb.h>
#include <maum/m2u/console/da.grpc.pb.h>

#include "hazelcast.h"
#include "dialog-agent-ai-portable.h"
#include "dialog-agent-manager-types.h"
#include "dam-configs.h"

using DaActInfo = DialogAgentAIPortable;
using DaActInfoEvent = EntryEvent<string, DaActInfo>;

/**
 * Dialog Agent 등록/삭제, 실행 정지를 관리한다.
 * Hazelcast kNsDAActivation("dialog-agent-activation") Map을 listene한다.
 */
class DialogAgentAIHandler : protected EntryListener<string, DaActInfo> {
 public:
  DialogAgentAIHandler() = delete;
  DialogAgentAIHandler(DamConfigs &config) : configs_(config) {};
  ~DialogAgentAIHandler() override;

  void AddListener();
  void RemoveListener();
  void LaunchDialogAgents();
  void DeactivateDialogAgents();
  void ClearDialogAgents();
  void DumpDialogAgentStatus();
  DamResult ActivateDA(const DialogAgent &da);
  DamResult DeactivateDA(const DaActInfo &da);

 protected:
  inline const string &GetDamName() { return configs_.GetDamName(); }
  inline const string &GetBinPath() { return configs_.GetBinPath(); }
  inline const string &GetExportIp() { return configs_.GetExportIp(); }
  inline int GetExportPort() { return configs_.GetExportPort(); }
  inline const string &GetAdminRemote() { return configs_.GetAdminRemote(); }
  inline const string &GetPoolRemote() { return configs_.GetPoolRemote(); }
  inline int &GetLastPort() { return configs_.GetLastPort(); }
  inline int GetChildWaitMs() { return configs_.GetChildWaitMs(); }
  inline mutex &GetDaMapMutex() { return configs_.GetDaMapMutex(); }
  inline mutex &GetDaiMapMutex() { return configs_.GetDaiMapMutex(); }
  inline ActiveDA &GetActiveDaMap() { return configs_.GetActiveDaMap(); }
  inline RunInst &GetRunningDaiMap() { return configs_.GetRunningDaiMap(); }

  /** Invoked when an entry is added. */
  void entryAdded(const DaActInfoEvent &event) override;
  /** Invoked when an entry is removed. */
  void entryRemoved(const DaActInfoEvent &event) override;
  /** Invoked when an entry is updated. */
  void entryUpdated(const DaActInfoEvent &event) override;
  /** Invoked when an entry is evicted. */
  void entryEvicted(const DaActInfoEvent &event) override;
  /** Invoked upon expiration of an entry. */
  void entryExpired(const DaActInfoEvent &event) override;
  /** Invoked after WAN replicated entry is merged. */
  void entryMerged(const DaActInfoEvent &event) override;
  /** Invoked when all entries evicted by {@link IMap#evictAll()}. */
  void mapEvicted(const MapEvent &event) override;
  /** Invoked when all entries are removed by {@link IMap#clear()}.} */
  void mapCleared(const MapEvent &event) override;

  DamConfigs &configs_;
  string registration_id_;

  const string &getMapName() { return GlobalHazelcastClient::kNsDAActivation; };
};

#endif // DIALOG_AGENT_HANDLER_H
