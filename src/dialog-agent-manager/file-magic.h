#ifndef DIALOG_AGENT_MANAGER_MAGIC_H
#define DIALOG_AGENT_MANAGER_MAGIC_H

#include <string>
#include <maum/m2u/server/dam.grpc.pb.h>

using std::string;
using maum::m2u::server::RuntimeEnvironment;

/**
 * libmagic wrapper class
 */
class FileMagic {
 public:
  FileMagic() = default;
  virtual ~FileMagic() { Close(); }

  // libmagic 라이브러리를 초기화하고 기본 database를 로드한다.
  bool Open();
  // 사용이 끝난 라이브러리를 닫는다.
  void Close();
  // File의 형태을 분석한 후 Runtime Environment로 변환하여 리턴한다
  RuntimeEnvironment Inspect(const string &file_path);

 private:
  void *handle_;
};

#endif // DIALOG_AGENT_MANAGER_MAGIC_H
