#include <sys/resource.h>
#include <iostream>
#include <sstream>
#include <iterator>
#include <grpc++/grpc++.h>
#include <google/protobuf/map.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

#include "util.h"
#include "dialog-agent-client.h"
#include "dialog-agent-portable.h"
#include "dialog-agent-instance-portable.h"
#include "dialog-agent-ai-handler.h"
#include "dialog-agent-iex-handler.h"

using hzc = GlobalHazelcastClient;
using DaiPortable = DialogAgentInstancePortable;

DialogAgentAIHandler::~DialogAgentAIHandler() {
  if (!registration_id_.empty()) {
    RemoveListener();
  }
}

/**
 * Hazelcast Map Listener 설정
 * DA 등록, 삭제 시 처리하는 Listner를 설정
 */
void DialogAgentAIHandler::AddListener() {
  if (!registration_id_.empty()) {
    LOGGER()->debug("DialogAgentAIHandler::AddListener "
                    "Listener already Registered.");
    return;
  }
  registration_id_ = hzc::Instance().GetHazelcastClient()
      ->getMap<string, DaActInfo>(getMapName())
      .addEntryListener(*this, true);

  LOGGER()->debug("Registered Listener for '{}' as [{}]",
                  getMapName(),
                  registration_id_);
}

void DialogAgentAIHandler::RemoveListener() {
  if (hzc::Instance().GetHazelcastClient()
      ->getMap<string, DaActInfo>(getMapName())
      .removeEntryListener(registration_id_)) {
    LOGGER()->debug("Unregistered Listener[{}] from '{}'",
                    registration_id_,
                    getMapName());
  } else {
    LOGGER()->debug("Failed to unregister the Listener[{}] for '{}'.",
                    registration_id_,
                    getMapName());
  }
  registration_id_.clear();
}

/**
 * 등록된 Dialog Agent를 시작한다.
 */
void DialogAgentAIHandler::LaunchDialogAgents() {
  // 현재 active한 DAM에서 실행되도록 등록되어 있는 DA목록을 가져온다.
  auto client = hzc::Instance().GetHazelcastClient();
  auto daai_map = client->getMap<string, DaActInfo>(hzc::kNsDAActivation);
  const string sql = " dam_name='" + GetDamName() + "'";
  vector<DaActInfo> daais = daai_map.values(SqlPredicate(sql));

  auto logger = LOGGER();
  logger->debug("Dialog Agent Registered: {}", daais.size());

  auto da_map = client->getMap<string, DialogAgentPortable>(hzc::kNsDA);
  for (auto &daai : daais) {
    auto da = da_map.get(daai.da_name());
    if (da == nullptr) {
      logger->info("DA {} is not found. Delete it.", daai.da_name());
      daai_map.deleteEntry(daai.da_name());
      continue;
    }

    DamResult da_status = this->ActivateDA(*da);
    if (da_status.result_code() == LaunchResult::SUCCESS) {
      logger->info("{} ExecutableDa is Activated.", da->name());
    } else {
      logger->info("Failed to activate [{}]. detail: {}",
                   da->name(),
                   da_status.detail_message());
    }
  }
}

/**
 * 활성화된 Dialog Agent를 Map에서 모두 삭제한다.
 */
void DialogAgentAIHandler::DeactivateDialogAgents() {
  std::lock_guard<std::mutex> lock(GetDaMapMutex());
  int count = (int) GetActiveDaMap().size();
  GetActiveDaMap().clear();
  LOGGER()->info("DAs Deactivated: {}", count);
}

void DialogAgentAIHandler::ClearDialogAgents() {
  auto client = hzc::Instance().GetHazelcastClient();
  auto daai_map = client->getMap<string, DaActInfo>(hzc::kNsDAActivation);
  const string sql = " dam_name='" + GetDamName() + "'";
  SqlPredicate sql_predicate(sql);

  int count = (int) daai_map.values(sql_predicate).size();
  daai_map.removeAll(sql_predicate);

  LOGGER()->info("Dialog Agent Removed: {}", count);
}

/**
 * Dialog Agent 실행 상태를 반환한다.
 */
void DialogAgentAIHandler::DumpDialogAgentStatus() {
  auto logger = LOGGER();

  std::lock_guard<std::mutex> lock(GetDaMapMutex());
  logger->info("Active DAs: {}", GetActiveDaMap().size());

  vector<string> list;
  std::ostringstream oss;
  for (auto &da : GetActiveDaMap()) {
    oss << "da :[name:" << da.first
        << "][full_exec_cmd:" << da.second.full_exec_cmd << "]";
    list.push_back(std::move(oss.str()));
    oss.str(string());
  }

  sort(list.begin(), list.end());
  for (auto &dai : list) {
    logger->info(dai);
  }
}

/**
 * 실행 가능한 DA를 실행하고 내부에 등록한다.
 *
 * @param da_rsc DA 리소스 정보
 * @return DAM Status를 반환한다.
 */
DamResult DialogAgentAIHandler::ActivateDA(const DialogAgent &da) {
  auto logger = LOGGER();

  logger->debug("Add da function call");

  DamResult dam_result;

  const RuntimeEnvironment rt_env = da.runtime_env();
  switch (rt_env) {
    case RuntimeEnvironment::RE_NATIVE:break;
    case RuntimeEnvironment::RE_JAVA:break;
    case RuntimeEnvironment::RE_PYTHON:break;
      // Not Supported yet.
    case RuntimeEnvironment::RE_NODE:
    case RuntimeEnvironment::RE_RUBY:
    case RuntimeEnvironment::RE_PHP:
    default: {
      dam_result.set_result_code(LaunchResult::DIALOG_AGENT_ADD_FAILED);
      dam_result.set_detail_message("Runtime Environment ["
                                        + RuntimeEnvironment_Name(rt_env)
                                        + "] is not Supported yet.");
      return dam_result;
    }
  }

  // DA를 이미 실행하고 있는지 확인한다.
  {
    std::lock_guard<std::mutex> lock(GetDaMapMutex());
    if (GetActiveDaMap().find(da.name()) != GetActiveDaMap().end()) {
      dam_result.set_result_code(LaunchResult::DIALOG_AGENT_IS_RUNNING);
      dam_result.set_detail_message("Executable DA is already Active");
      return dam_result;
    }
  }

  // DA가 실행 경로명에 있고 실행 가능한 파일인지 확인한다.
  string full_path = GetBinPath() + da.da_executable();
  logger->debug("Path : {}", full_path);
  int path_found = access(full_path.c_str(), R_OK);
  if (path_found != 0) {
    dam_result.set_detail_message(full_path + " not found");
    dam_result.set_result_code(LaunchResult::DIALOG_AGENT_EXE_FILE_NOT_FOUND);
    return dam_result;
  }

  // DA에 할당할 Port를 가져 온다.
  int port = GetPort(GetExportIp().c_str(), GetLastPort());
  if (port == -1) {
    dam_result.set_result_code(LaunchResult::DIALOG_AGENT_ADD_FAILED);
    dam_result.set_detail_message("DA Port assign failed");
    return dam_result;
  }

  libmaum::Config &c = libmaum::Config::Instance();
  vector<string> arg_vec;
  switch (rt_env) {
    case RuntimeEnvironment::RE_NATIVE: {
      arg_vec.push_back(full_path);
      break;
    }
    case RuntimeEnvironment::RE_JAVA: {
      const string java_path = c.GetDefault("dam-svcd.java.path",
                                            "/usr/bin/java");
      arg_vec.emplace_back(java_path);
      arg_vec.emplace_back(da.extra_command().empty() ? "-jar" : "-cp");
      arg_vec.push_back(full_path);
      if (!da.extra_command().empty()) {
        std::istringstream iss(da.extra_command());
        copy(std::istream_iterator<string>(iss),
             std::istream_iterator<string>(),
             back_inserter(arg_vec));
      }
      break;
    }
    case RuntimeEnvironment::RE_PYTHON: {
      const string python_path = c.GetDefault("dam-svcd.python.path",
                                              "/usr/bin/python2.7");
      arg_vec.emplace_back(python_path);
      arg_vec.push_back(full_path);
      break;
    }
    case RuntimeEnvironment::RE_NODE:
    case RuntimeEnvironment::RE_RUBY:
    case RuntimeEnvironment::RE_PHP:
    default: {
      break;
    }
  }

  // child process에서 DA를 실행한다.
  int pid = fork();
  if (pid == -1) {
    dam_result.set_result_code(LaunchResult::DIALOG_AGENT_ADD_FAILED);
    dam_result.set_detail_message("Dialog Agent Add Fork failed");
    return dam_result;
  }

  if (pid == 0) {
    struct rlimit rl;
    getrlimit(RLIMIT_NOFILE, &rl);
    for (unsigned int fd = 3; fd < rl.rlim_cur; fd++) {
      close(fd);
    }

    arg_vec.emplace_back("-p");
    arg_vec.push_back(to_string(port));
    if (rt_env != RuntimeEnvironment::RE_PYTHON) {
      arg_vec.emplace_back(da.name());
    }

    char **argv = new char *[arg_vec.size() + 1];
    for (size_t i = 0; i < arg_vec.size(); i++) {
      argv[i] = (char *) arg_vec[i].c_str();
    }
    argv[arg_vec.size()] = nullptr;

    execv(argv[0], argv);
    exit(0);
  } else {
    // DA가 완전히 기동할 때 까지 기다린다.
    logger->debug("Wait DA[{}] running for {}ms", pid, GetChildWaitMs());
    std::this_thread::sleep_for(std::chrono::milliseconds(GetChildWaitMs()));
    string endpoint = GetExportIp() + ':' + to_string(port);

    // DA가 정상적으로 실행되었는지 확인한다.
    // Dialog Agent Client 생성 후 IsReady 호출
    Status status;
    DialogAgentStatus da_status;
    logger->debug("Check DA[{}] IsReady.", endpoint);
    DialogAgentClient da_client(GetExportIp() + ':' + to_string(port),
                                da.da_prod_spec());
    bool retry = false;
    // IsReady 호출
    status = da_client.IsReady(da_status);
    if (!status.ok()) {
      retry = true;
      if (da_client.CheckStubReady(30)) {
        da_client.IsReady(da_status);
      }
    }

    if (da_status.state() == DialogAgentState::DIAG_STATE_IDLE) {
      logger->debug("new DA [PID] {} on port {}. Retry: {}", pid, port, retry);

      RuntimeParameterList rt_param_list;
      da_client.GetRuntimeParameters(rt_param_list);
      DialogAgentProviderParam da_prod_param;
      da_client.GetProviderParameter(da_prod_param);
      UserAttributeList user_attr_list;
      da_client.GetUserAttributes(user_attr_list);

      ActiveDialogAgent exec_da;
      exec_da.agent = da.name();
      exec_da.full_exec_cmd = full_path;
      exec_da.exec_cmd = da.da_executable();
      exec_da.da_prod_spec = da.da_prod_spec();
      exec_da.runtime_env = da.runtime_env();
      exec_da.arg_vec = arg_vec;
      exec_da.rt_param_list = rt_param_list;
      exec_da.da_prod_param = da_prod_param;
      exec_da.user_attr_list = user_attr_list;

      logger->info("Insert into DA map: {}, {}",
                   exec_da.agent,
                   DialogAgentProviderSpec_Name(exec_da.da_prod_spec));
      logger->debug(" - File : {}", exec_da.exec_cmd);

      std::lock_guard<std::mutex> lock(GetDaMapMutex());
      GetActiveDaMap().insert(std::make_pair(da.name(), exec_da));
      dam_result.set_result_code(LaunchResult::SUCCESS);
    } else {
      const char *msg = "DA is not in Idle state. Failed to execute a DA.";
      dam_result.set_result_code(LaunchResult::DIALOG_AGENT_IS_NOT_IDLE_STATE);
      dam_result.set_detail_message(msg);
      logger->warn("{}, now kill it", msg);
    }
    kill(pid, SIGTERM);
  }

  return dam_result;
}

/**
 * 실행 가능한 DA 에서 해당 목록을 삭제한다.
 * @param daai 삭제할 DA
 * @return 성공 여부를 반환한다.
 */
DamResult DialogAgentAIHandler::DeactivateDA(const DaActInfo &daai) {
  auto logger = LOGGER();

  logger->debug("Remove da function call");

  DamResult dam_result;
  const string &dam_name = daai.dam_name();
  const string &da_name = daai.da_name();
  auto hzc_client = hzc::Instance().GetHazelcastClient();
  string sql;

  // Dialog Agent Instance 제거
  {
    // DAI에서 dai_id 획득
    auto dai_map = hzc_client->getMap<string, DaiPortable>(hzc::kNsDAInstance);
    sql = " da_name='" + daai.da_name() + "'";
    vector<DaiPortable> dais = dai_map.values(SqlPredicate(sql));

    // 관련된 DAI Execution Info에서 dai_id와 dam_nam으로 검색 후 삭제
    auto daiex_map = hzc_client->getMap<string, DaiExInfo>(hzc::kNsDAInstExec);
    for (auto &dai : dais) {
      const string dai_id = dai.dai_id();
      sql = " dai_id='" + dai_id + "' and dam_name='" + dam_name + "'";
      daiex_map.removeAll(SqlPredicate(sql));

      // 관련된 DAI Execution Info에서 dai_id로 된 것이 없으면 관련 DAI삭제
      // FIXME: 실제 운영 시 삭제 하지 않아야 할 수도 있음.
      sql = " dai_id='" + dai_id + "'";
      if (daiex_map.values(SqlPredicate(sql)).empty()) {
        dai_map.deleteEntry(dai.dai_id());
      }
    }
  }

  // Dialog Agent Activation Info에서 da_name과 dam_nam 조건으로 삭제
  {
    auto daai_map = hzc_client->getMap<string, DaActInfo>(hzc::kNsDAActivation);
    sql = " da_name='" + da_name + "' and dam_name='" + dam_name + "'";
    daai_map.removeAll(SqlPredicate(sql));

    sql = " da_name='" + da_name + "'";
    if (daai_map.values(SqlPredicate(sql)).empty()) {
      // 해당 이름의 da를 실행 map에서 삭제 및 종료
      std::lock_guard<std::mutex> lock(GetDaMapMutex());
      auto e_da = GetActiveDaMap().find(da_name);
      if (e_da != GetActiveDaMap().end()) {
        GetActiveDaMap().erase(da_name);
        dam_result.set_result_code(LaunchResult::SUCCESS);
      } else {
        dam_result.set_result_code(LaunchResult::DIALOG_AGENT_IS_NOT_FOUND_IN_MAP);
        dam_result.set_detail_message(da_name + " is not found in map");
      }
    }
  }

  return dam_result;
}

/**
 * DAActivation Map에 Entry가 추가되었을 때, 추가된 DA를 실행한다.
 *
 * @param event entry event
 */
void DialogAgentAIHandler::entryAdded(const DaActInfoEvent &event) {
  if (event.getValue().dam_name() != GetDamName()) {
    return;
  }
  auto logger = LOGGER();
  const DaActInfo added_da = event.getValue();
  LOGGER()->info("Dialog Agent Added: {} / {} / {}",
                 added_da.da_name(),
                 added_da.active(),
                 added_da.dam_name());
  if (!added_da.active()) {
    logger->info("Not Active. Do nothing.");
    return;
  }
  std::thread([=]() {
    auto da_map = hzc::Instance().GetHazelcastClient()->
        getMap<string, DialogAgentPortable>(hzc::kNsDA);
    auto da = da_map.get(added_da.da_name());

    if (da) {
      DamResult da_status = ActivateDA(*da);
      if (da_status.result_code() == LaunchResult::SUCCESS) {
        logger->debug("{} ExecutableDa is Activated.", da->name());
      } else {
        logger->info("{} failed to Activate. [{}]",
                     da->name(),
                     da_status.detail_message());
      }
    } else {
      LOGGER()->info("DA {} is not Found.", added_da.da_name());
    }
  }).detach();
}

/**
 * DAActivation Map에 Entry가 삭제되었을 때, 삭제된 DA를 종료한다.
 *
 * @param event entry event
 */
void DialogAgentAIHandler::entryRemoved(const DaActInfoEvent &event) {
  const string &removed_key = event.getKey();
  LOGGER()->info("Dialog Agent Removed: {}", removed_key);
  std::thread([=]() {
    DaActInfo daai;
    // DAM 내부 DA Map과 DAAI Map을 비교하여 제거된 DA를 찾는다.
    {
      auto daai_map = hzc::Instance().GetHazelcastClient()
          ->getMap<string, DaActInfo>(hzc::kNsDAActivation);
      std::lock_guard<std::mutex> lock(GetDaMapMutex());
      for (auto &da : GetActiveDaMap()) {
        const string sql = " da_name='" + da.second.agent
            + "' and dam_name='" + GetDamName() + "'";
        if (daai_map.values(SqlPredicate(sql)).empty()) {
          daai.set_dam_name(GetDamName());
          daai.set_da_name(da.second.agent);
          break;
        }
      }
    }
    if (daai.da_name().empty()) {
      LOGGER()->info("Removed Dialog Agent[{}] not found.", removed_key);
    } else {
      DamResult da_status = DeactivateDA(daai);
      if (da_status.result_code() == LaunchResult::SUCCESS) {
        LOGGER()->debug("{} ExecutableDa is Deactivated.", daai.da_name());
      } else {
        LOGGER()->info("{} failed to Deactivate. [{}]",
                       daai.da_name(),
                       da_status.detail_message());
      }
    }
  }).detach();
}

/**
 * DAActivation Map에 Entry가 업데이트되었을 때, 기존 DA를 종료 후 재실행한다.
 *
 * @param event entry event
 */
void DialogAgentAIHandler::entryUpdated(const DaActInfoEvent &event) {
  if (event.getValue().dam_name() != GetDamName()) {
    return;
  }
  auto logger = LOGGER();
  const DaActInfo cur_da = event.getValue();

  logger->info("Dialog Agent updated: ==> {} / {} / {}",
               cur_da.da_name(), cur_da.active(), cur_da.dam_name());

  std::thread([=]() {
    bool old_active;
    {
      std::lock_guard<std::mutex> lock(GetDaMapMutex());
      auto got = GetActiveDaMap().find(cur_da.da_name());

      old_active = got != GetActiveDaMap().end();
    }

    if (cur_da.active() == old_active) {
      logger->info("Active is not changed. Do nothing.");
      return;
    }

    if (cur_da.active()) {
      auto da_map = hzc::Instance().GetHazelcastClient()->
          getMap<string, DialogAgentPortable>(hzc::kNsDA);
      auto da = da_map.get(cur_da.da_name());

      if (da) {
        logger->info("Activate the DA {}.", cur_da.da_name());
        DamResult da_status = ActivateDA(*da);
        if (da_status.result_code() == LaunchResult::SUCCESS) {
          logger->debug("{} ExecutableDa is Activated.", da->name());
        } else {
          logger->info("{} failed to Activate. [{}]",
                       da->name(),
                       da_status.detail_message());
        }
      } else {
        logger->info("DA {} is not Found.", cur_da.da_name());
      }
    } else {
      logger->info("Deactivate the DA {}.", cur_da.da_name());
      DamResult da_status = DeactivateDA(cur_da);
      if (da_status.result_code() == LaunchResult::SUCCESS) {
        logger->debug("{} ExecutableDa is Deactivated.", cur_da.da_name());
      } else {
        logger->info("{} failed to Deactivate. [{}]",
                     cur_da.da_name(),
                     da_status.detail_message());
      }
    }
  }).detach();
}

/**
* Invoked when an entry is evicted.
*
* @param event entry event
*/
void DialogAgentAIHandler::entryEvicted(const DaActInfoEvent &event) {
  LOGGER()->debug("DialogAgentAIHandler::{} is Unimplemented.", __func__);
}

/**
* Invoked upon expiration of an entry.
*
* @param event the event invoked when an entry is expired.
*/
void DialogAgentAIHandler::entryExpired(const DaActInfoEvent &event) {
  LOGGER()->debug("DialogAgentAIHandler::{} is Unimplemented.", __func__);
}

/**
*  Invoked after WAN replicated entry is merged.
*
* @param event the event invoked when an entry is expired.
*/
void DialogAgentAIHandler::entryMerged(const DaActInfoEvent &event) {
  LOGGER()->debug("DialogAgentAIHandler::{} is Unimplemented.", __func__);
}

/**
* Invoked when all entries evicted by {@link IMap#evictAll()}.
*
* @param event map event
*/
void DialogAgentAIHandler::mapEvicted(const MapEvent &event) {
  LOGGER()->debug("DialogAgentAIHandler::{} is Unimplemented.", __func__);
}

/**
* Invoked when all entries are removed by {@link IMap#clear()}.}
*/
void DialogAgentAIHandler::mapCleared(const MapEvent &event) {
  LOGGER()->debug("DialogAgentAIHandler::{} is Unimplemented.", __func__);
}
