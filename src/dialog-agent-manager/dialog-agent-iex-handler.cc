
#include <sys/resource.h>
#include <grpc++/grpc++.h>
#include <google/protobuf/map.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

#include "util.h"
#include "elapsed-timer.h"
#include "dialog-agent-instance-portable.h"
#include "dialog-agent-iex-handler.h"

#define START_NEW_DA_INSTANCE -1

using std::unique_ptr;
using hzc = GlobalHazelcastClient;
using maum::common::Lang;
using maum::m2u::server::DAL_DIALOG_AGENT_MANAGER;
using maum::m2u::server::RegisterResponse;
using maum::m2u::server::DialogAgentInstanceStat;
using maum::m2u::server::DialogAgentInstanceKey;
using DaiPortable = DialogAgentInstancePortable;

DialogAgentIExHandler::~DialogAgentIExHandler() {
  if (!registration_id_.empty()) {
    RemoveListener();
  }
}

/**
 * Hazelcast Map Listener 설정
 * DAI 등록, 삭제 시 처리하는 Listner를 설정
 */
void DialogAgentIExHandler::AddListener() {
  if (!registration_id_.empty()) {
    LOGGER()->debug("DialogAgentIExHandler::AddListener "
                    "Listener already Registered.");
    return;
  }
  registration_id_ = hzc::Instance().GetHazelcastClient()
      ->getMap<string, DaiExInfo>(getMapName())
      .addEntryListener(*this, true);

  LOGGER()->debug("Registered Listener for '{}' as [{}]",
                  getMapName(),
                  registration_id_);
}

void DialogAgentIExHandler::RemoveListener() {
  if (hzc::Instance().GetHazelcastClient()
      ->getMap<string, DaiExInfo>(getMapName())
      .removeEntryListener(registration_id_)) {
    LOGGER()->debug("Unregistered Listener[{}] from '{}'",
                    registration_id_,
                    getMapName());
  } else {
    LOGGER()->debug("Failed to unregister the Listener[{}] for '{}'.",
                    registration_id_,
                    getMapName());
  }
  registration_id_.clear();
}

/**
 * Zombie 목록를 만든다.
 *
 * @return zombie list
 */
vector<DialogAgentIRPortable> DialogAgentIExHandler::ListUpZombies() {
  auto logger = LOGGER();

  logger->debug("List up zombies of '{}'.", GetDamName());

  vector<DialogAgentIRPortable> zombies;
  auto client = hzc::Instance().GetHazelcastClient();
  auto dair_map = client->getMap<string, DialogAgentIRPortable>(hzc::kNsDAIR);
  string sql = " dam_name='" + GetDamName() + "'";

  vector<DialogAgentIRPortable> candidates = dair_map.values(SqlPredicate(sql));

  logger->debug("Zombie candidate(s): {}", candidates.size());

  auto dai_map = client->getMap<string, DaiPortable>(hzc::kNsDAInstance);

  for (auto &candidate : candidates) {
    auto dai = dai_map.get(candidate.dai_id());

    if (!dai) {
      logger->debug("DAI [{}] is not exist.", candidate.key());
      candidate.set_pid(-1);
      zombies.push_back(std::move(candidate));
      continue;
    }

    if (candidate.launch_type() == DAL_DIALOG_AGENT_MANAGER) {
      zombies.push_back(std::move(candidate));
    } else {
      logger->info("Keep DAI by {} {}[{}] - [{}].",
                   DialogAgentLaunchType_Name(candidate.launch_type()),
                   candidate.name(),
                   candidate.pid(),
                   candidate.key());
    }
  }

  logger->debug("{} Zombie(s) are listed up.", zombies.size());

  return zombies;
}

/**
 * Zombie 목록를 등록 해제한다.
 *
 * @param zombies 좀비 목록
 */
void DialogAgentIExHandler::CleanUpZombies(vector<DialogAgentIRPortable> &zombies) {
  auto logger = LOGGER();

  logger->debug("Clean up zombies.");

  for (auto &zombie : zombies) {
    std::thread([=]() {
      // Pool에서 DAI 등록 취소
      UnregisterDialogAgentInstance(zombie.key());

      if (zombie.pid() < 0) {
        return;
      }

      if (kill(zombie.pid(), 0) < 0) {
        logger->debug("DAI [{}] is a phantom. errno: {}", zombie.key(), errno);
        return;
      }

      string endpoint = zombie.server_ip()
          + ':' + to_string(zombie.server_port());
      {
        std::lock_guard<std::mutex> lock(GetDaiMapMutex());
        for (auto &dainst : GetRunningDaiMap()) {
          if (endpoint == dainst.second.endpoint) {
            logger->debug("endpoint[{}] is used by active DAI[{}]",
                          endpoint,
                          dainst.second.agent);
            return;
          }
        }
      }

      logger->trace("checking DAI {}/{}/{}",
                    zombie.name(),
                    DialogAgentProviderSpec_Name(zombie.da_spec()),
                    endpoint);
      DialogAgentClient dai_client(endpoint, zombie.da_spec());
      DialogAgentStatus da_status;
      Status status = dai_client.IsReady(da_status);

      if (status.ok()) {
        // DAI 종료
        dai_client.Terminate();
        kill(zombie.pid(), SIGTERM);
        logger->debug("DAI [{}] - [{}] has cleaned.",
                      zombie.key(),
                      zombie.pid());
      } else {
        logger->debug("[{}] may not a DAI.",
                      zombie.pid());
      }
    }).detach();
  }
}

/**
 * 등록된 Dialog Agent Instance를 재시작한다.
 */
void DialogAgentIExHandler::LaunchDialogAgentInstances() {

  // 현재 active한 DAM에서 실행되도록 등록되어 있는 DAI목록을 가져온다.
  auto client = hzc::Instance().GetHazelcastClient();
  auto dai_map = client->getMap<string, DaiPortable>(hzc::kNsDAInstance);
  auto daiex_map = client->getMap<string, DaiExInfo>(hzc::kNsDAInstExec);
  const string sql = " dam_name='" + GetDamName() + "'";

  vector<DaiExInfo> daiexs = daiex_map.values(SqlPredicate(sql));

  LOGGER()->debug("Dialog Agent Instance Registered: {}", daiexs.size());

  for (auto &daiex : daiexs) {
    auto dai_id = daiex.dai_id();
    // DAI Map에서 dai_id로 DAI 정보 가져오기
    if (!dai_map.get(dai_id)) {
      // DAI가 없으면 실행 정보 삭제
      daiex_map.deleteEntry(dai_id + "_" + GetDamName());
      LOGGER()->info("DAI [{}] does not exist. Delete Execution Info.",
                     dai_id);
      continue;
    }
    StartDialogAgentInstances(dai_id, daiex.cnt());
  }
}

/**
 * 실행 중인 DAIR을 모두 중지한다.
 */
void DialogAgentIExHandler::DeactivateDialogAgentInstances() {
  vector<string> dair_keys;

  // 실행 중인 DAIR의 key를 모두 가져온다.
  {
    std::lock_guard<std::mutex> lock(GetDaiMapMutex());
    for (auto &dainst : GetRunningDaiMap()) {
      dair_keys.push_back(dainst.first);
    }
  }

  // key로 실행 중인 DAIR를 모두 중지한다.
  for (string key : dair_keys) {
    StopDialogAgentInstance(key);
  }

  LOGGER()->debug("DAIs Deactivated: {}", dair_keys.size());
}

void DialogAgentIExHandler::ClearDialogAgentInstances() {
  auto client = hzc::Instance().GetHazelcastClient();
  auto daiex_map = client->getMap<string, DaiExInfo>(hzc::kNsDAInstExec);
  const string sql = " dam_name='" + GetDamName() + "'";
  SqlPredicate sql_predicate(sql);

  int count = (int) daiex_map.values(sql_predicate).size();
  daiex_map.removeAll(sql_predicate);

  LOGGER()->debug("Dialog Agent Instance Removed: {}", count);
}

/**
 * DialogAgentInstance 실행 상태를 반환한다.
 */
void DialogAgentIExHandler::DumpDialogAgentInstanceStatus() {
  auto logger = LOGGER();
  std::lock_guard<std::mutex> lock(GetDaiMapMutex());

  logger->info("Running DAIs: {}", GetRunningDaiMap().size());

  vector<string> list;
  std::ostringstream oss;
  for (auto &dainst : GetRunningDaiMap()) {
    oss << "dainst: [" << dainst.second.agent
        << "][endpoint:" << dainst.second.endpoint
        << "][pid:" << dainst.second.pid
        << "][" << dainst.first << "]";
    list.push_back(std::move(oss.str()));
    oss.str(string());
  }

  sort(list.begin(), list.end());
  for (auto &dai : list) {
    logger->info(dai);
  }
}

bool DialogAgentIExHandler::IsDialogAgentInstanceRunning(
    const RunningDialogAgentInstance &run_inst) {

  DialogAgentStatus das;
  DialogAgentClient
      da_client(run_inst.endpoint, run_inst.param.da_prod_spec());
  Status status = da_client.IsReady(das);

  if (status.ok()) {
    auto logger = LOGGER();
    if (das.state() == DialogAgentState::DIAG_STATE_TERMINATED) {
      logger->debug("This da instance has been shut down normally");
      kill(run_inst.pid, SIGTERM);
    } else {
      logger->warn("This da instance was terminated abnormally.");
      return true;
    }
  } else if (status.error_code() == StatusCode::UNAVAILABLE) {
    // 이미 죽어 있는 상태입니다.
    return true;
  }

  return false;
}


/**
 * Pool Service에서 DAI 등록
 *
 * @return
 */
bool DialogAgentIExHandler::RegisterDialogAgentInstance(DialogAgentIRPortable &dair) {
  auto channel = CreateChannel(GetPoolRemote(), InsecureChannelCredentials());
  auto pool = DialogAgentInstancePool::NewStub(channel);

  ClientContext ctx;
  RegisterResponse res;

  grpc::Status st = pool->Register(&ctx, dair, &res);
  if (st.ok()) {
    return true;
  } else {
    LOGGER()->info("reg res:{} {} {}, key:{}",
                   st.error_code(),
                   st.error_message(),
                   st.error_details());
  }

  return false;
}


/**
 * Pool Service에서 DAI 등록 취소
 *
 * @param key
 * @return
 */
bool DialogAgentIExHandler::UnregisterDialogAgentInstance(
    const string &key) {
  auto channel = CreateChannel(GetPoolRemote(), InsecureChannelCredentials());
  auto pool = DialogAgentInstancePool::NewStub(channel);

  ClientContext ctx;
  DialogAgentInstanceStat res;
  DialogAgentInstanceKey inst_key;
  inst_key.set_key(key);
  grpc::Status st = pool->Unregister(&ctx, inst_key, &res);
  if (st.ok()) {
    return true;
  } else {
    LOGGER()->info("unreg res:{} {} {}, key:{}",
                   st.error_code(),
                   st.error_message(),
                   st.error_details(),
                   key);
    return false;
  }
}

DamResult DialogAgentIExHandler::RunDialogAgentInstance(
    const RunDialogAgentInstanceParam &run_param,
    const ActiveDialogAgent &exec_da,
    int pre_port) {
  auto logger = LOGGER();
  logger->debug("Run DA Instance function call");

  DamResult dam_result;
  dam_result.set_result_code(LaunchResult::DAINSTANCE_RUN_FAILED);

  // DAI listen Port 가져오기
  int port;
  if (pre_port < 1) {
    port = GetPort(GetExportIp().c_str(), GetLastPort());
  } else {
    if (CheckPort(GetExportIp().c_str(), pre_port)) {
      // 포트를 이미 사용 중이면 Hazelcast DIALOG_AGENT_INSTANCE_RESOURCE Map에서 제거
      logger->debug("pre_dainst_port {} is already used by another process."
                    " Cancel to existing, and re-registreing", pre_port);
      UnregisterDialogAgentInstance(run_param.key());
      auto client = hzc::Instance().GetHazelcastClient();
      auto dair_map = client
          ->getMap<string, DialogAgentIRPortable>(hzc::kNsDAIR);
      dair_map.deleteEntry(run_param.key());
      pre_port = GetLastPort();
    }
    port = GetPort(GetExportIp().c_str(), pre_port);
  }

  if (port == -1) {
    dam_result.set_result_code(LaunchResult::DAINSTANCE_RUN_FAILED);
    dam_result.set_detail_message("DAI Port assign failed");
    return dam_result;
  }

  // child process에서 DAI 실행
  int pid = fork();
  if (pid == -1) {
    dam_result.set_detail_message("Dialog Agent Instance Add Fork failed");
    return dam_result;
  }

  if (pid == 0) {
    struct rlimit rl;
    getrlimit(RLIMIT_NOFILE, &rl);
    for (unsigned int fd = 3; fd < rl.rlim_cur; fd++) {
      close(fd);
    }

    vector<string> arg_vec = exec_da.arg_vec;
    auto &param = run_param.param();
    arg_vec.emplace_back("-p");
    arg_vec.push_back(to_string(port));
    if (exec_da.runtime_env != RuntimeEnvironment::RE_PYTHON) {
      arg_vec.emplace_back(run_param.agent()
                               + "-" + param.chatbot()
                               + "-" + MergeStrings(param.skills(), '.')
                               + "-" + Lang_Name((Lang) param.lang()));
    }

    char **argv = new char *[arg_vec.size() + 1];
    for (size_t i = 0; i < arg_vec.size(); i++) {
      argv[i] = (char *) arg_vec[i].c_str();
    }
    argv[arg_vec.size()] = nullptr;

    execv(argv[0], argv);
    exit(0);
  } else {
    logger->debug("Wait new DAI[{}] running for {}ms", pid, GetChildWaitMs());
    std::this_thread::sleep_for(std::chrono::milliseconds(GetChildWaitMs()));
    string endpoint = GetExportIp() + ':' + to_string(port);

    // DAI가 정상적으로 실행되었는지 확인한다.
    // Dialog Agent Client 생성 후 IsReady 호출
    Status status;
    DialogAgentStatus dai_status;
    logger->debug("Check DAI[{}] IsReady.", endpoint);
    DialogAgentClient dai_client(endpoint, exec_da.da_prod_spec);

    bool retry = false;
    // IsReady 호출
    status = dai_client.IsReady(dai_status);
    if (!status.ok()) {
      retry = true;
      if (dai_client.CheckStubReady(30)) {
        status = dai_client.IsReady(dai_status);
      }
    }

    if (!status.ok()) {
      dam_result.set_detail_message("IsReady() call failed!");
      dam_result.set_result_code(LaunchResult::DAINSTANCE_SERVER_IS_NOT_READY);
      kill(pid, SIGTERM);
      return dam_result;
    }

    logger->debug("Child DAI[{},{}] is in {} state. Retry: {}",
                  endpoint, pid,
                  DialogAgentState_Name(dai_status.state()),
                  retry);
    if (dai_status.state() != DialogAgentState::DIAG_STATE_IDLE) {
      // 비정상적인 상태를 반환하는 경우, 프로그램을 잘못 짠 것으로 봐야 함.
      logger->warn("now kill invalid state child {} {}",
                   DialogAgentState_Name(dai_status.state()), pid);
      dam_result.set_detail_message("invalid state");
      dam_result.set_result_code(
          LaunchResult::DAINSTANCE_SERVER_IS_INVALID);
      kill(pid, SIGTERM);
      return dam_result;
    }

    DialogAgentProviderParam providerParam;
    maum::m2u::da::InitParameter init_param;
    init_param.CopyFrom(run_param.param());

    libmaum::Config &c = libmaum::Config::Instance();

    string remote(c.GetDefault("maum.sds.listen", ""));
    if (remote.empty()) {
      logger->info("SDS Remote is not assigned.");
    } else {
      logger->debug("sds_remote_ : {}", remote);
    }
    init_param.set_sds_remote_addr(remote);

    status = dai_client.Init(init_param, providerParam);

    if (!status.ok()) {
      logger->warn("Init call failed! - msg: {} / code: {}",
                   status.error_message(),
                   status.error_code());
      dam_result.set_detail_message("init call failed!");
      dam_result.set_result_code(LaunchResult::DAINSTANCE_SERVER_INIT_FAILED);
      kill(pid, SIGTERM);
      return dam_result;
    }

    // `POOL`에 Register function call
    if (providerParam.name().empty()) {
      dam_result.set_detail_message("da internal failed!");
      dam_result.set_result_code(LaunchResult::DAINSTANCE_SERVER_INTERNAL_FAILED);
      return dam_result;
    }

    // Hazelcast DIALOG_AGENT_INSTANCE_RESOURCE Map에 저장
    DialogAgentIRPortable dair;
    dair.set_key(run_param.key());
    dair.set_dai_id(run_param.dai_id());
    dair.set_name(run_param.agent());
    dair.set_description(run_param.description());
    dair.set_version(run_param.version());
    dair.set_chatbot(run_param.param().chatbot());
    dair.mutable_skills()->CopyFrom(run_param.param().skills());
    dair.set_lang(run_param.param().lang());
    dair.set_dam_name(GetDamName());
    dair.set_server_ip(GetExportIp());
    dair.set_server_port(port);
    dair.set_da_spec(exec_da.da_prod_spec);
    dair.mutable_param()->CopyFrom(providerParam);
    dair.set_launch_type(maum::m2u::server::DAL_DIALOG_AGENT_MANAGER);
    string
        launcher = "DAM " + GetExportIp() + ":" + to_string(GetExportPort());
    dair.set_launcher(launcher);
    dair.set_pid(pid);
    dair.mutable_started_at()->set_seconds(time(nullptr));

    RegisterDialogAgentInstance(dair);

    // 실행 중인 DA Instance map에 실행한 DA Instance의 key, Info 추가
    RunningDialogAgentInstance run_inst;
    run_inst.pid = dair.pid();
    run_inst.endpoint =
        dair.server_ip() + ':' + to_string(dair.server_port());
    run_inst.port = dair.server_port();
    run_inst.agent = dair.name();
    run_inst.dai_id = dair.dai_id();
    run_inst.key = dair.key();
    run_inst.param.CopyFrom(run_param);
    run_inst.param.set_da_prod_spec(dair.da_spec());

    logger->info("Insert Running DAI: PID: {} agent: {} endpoint: {} {}",
                 run_inst.pid,
                 run_inst.agent,
                 run_inst.endpoint,
                 DialogAgentProviderSpec_Name(run_inst.param.da_prod_spec()));
    {
      std::lock_guard<std::mutex> lock(GetDaiMapMutex());
      GetRunningDaiMap().insert(std::make_pair(run_inst.key, run_inst));
    }

    dam_result.set_result_code(LaunchResult::SUCCESS);
    dam_result.set_detail_message("success");
    return dam_result;
  }
}

DamResult DialogAgentIExHandler::StopDialogAgentInstance(const string &key) {
  auto logger = LOGGER();
  logger->debug("{} START", __func__);

  DamResult dam_result;
  LaunchResult rs = LaunchResult::DAINSTANCE_STOP_FAILED;
  dam_result.set_result_code(rs);

  RunningDialogAgentInstance stop_dainst;
  {
    std::lock_guard<std::mutex> lock(GetDaiMapMutex());
    // web에서 전달 받은 uid들을 기반으로 실행중인 da instance map에서 해당
    // da instance을 찾아 정지시킨다.
    auto got = GetRunningDaiMap().find(key);
    if (got != GetRunningDaiMap().end()) {
      stop_dainst = got->second;
    }
  }

  if (!stop_dainst.key.empty()) {
    logger->debug("DAI Terminate remote addr: {}", stop_dainst.endpoint);

    DialogAgentClient dai_client(stop_dainst.endpoint,
                                 stop_dainst.param.da_prod_spec());
    Status status = dai_client.Terminate();

    if (!status.ok()) {
      logger->warn("Terminate call failed: {}", status.error_message());
      dam_result.set_detail_message("Terminate call failed");
      return dam_result;
    }

    if (UnregisterDialogAgentInstance(key)) {
      //running da instance의 process를 중지한다.
      logger->info("Stop Running DAI: [PID : {}] [dai: {}] [endpoint: {}]",
                   stop_dainst.pid,
                   stop_dainst.agent,
                   stop_dainst.endpoint);
      kill(stop_dainst.pid, SIGTERM);
      logger->debug("DAI [{}] Stopped.", stop_dainst.pid);
      rs = LaunchResult::SUCCESS;
      //running da instance map 에서 해당 da instance 의 key 삭제
      {
        std::lock_guard<std::mutex> lock(GetDaiMapMutex());
        GetRunningDaiMap().erase(key);
      }
      dam_result.set_result_code(rs);
      return dam_result;
    } else {
      rs = LaunchResult::DAINSTANCE_UNREGISTER_FAILED;
      dam_result.set_result_code(rs);
      dam_result.set_detail_message("Unregister Failed " + key);
      logger->info("Unregister Failed {}" + key);
      return dam_result;
    }
  } else {
    rs = LaunchResult::DAINSTANCE_IS_NOT_FOUND_IN_MAP;
    dam_result.set_result_code(rs);
    dam_result.set_detail_message(key + " is not found");
    logger->info("{} is not found", key);
    return dam_result;
  }
}

DamResult DialogAgentIExHandler::RestartDialogAgentInstance(
    const RunDialogAgentInstanceParam &prev_param,
    const ActiveDialogAgent &exec_da) {
  DamResult ls;

  ls = RunDialogAgentInstance(prev_param, exec_da, START_NEW_DA_INSTANCE);
  auto logger = LOGGER();
  if (ls.result_code() == LaunchResult::SUCCESS) {
    logger->debug("{} dainst is restarting", prev_param.key());
  } else {
    logger->debug("{} dainst restarting failed", prev_param.key());
    logger->debug("dainst restart failed msg : {} ", ls.detail_message());
  }

  return ls;
}

void DialogAgentIExHandler::StartDialogAgentInstances(const string &dai_id,
                                                      int cnt) {
  DaiPortable dai;

  {
    // DAI Map에서 dai_id로 DAI 정보 가져오기
    auto dai_ = hzc::Instance().GetHazelcastClient()
        ->getMap<string, DaiPortable>(hzc::kNsDAInstance)
        .get(dai_id);

    if (!dai_) {
      // daiex map에서 삭제?
      LOGGER()->info("DAI [{}] does not exist.", dai_id);
      return;
    }
    dai = *dai_;
  }

  // DA가 실행 중인지 확인하고 실행 정보를 가져온다.
  ActiveDialogAgent exec_da;
  {
    std::lock_guard<std::mutex> lock(GetDaMapMutex());
    auto got = GetActiveDaMap().find(dai.da_name());
    if (got == GetActiveDaMap().end()) {
      LOGGER()->info("DA {} is not active!", dai.da_name());
      return;
    }
    exec_da = got->second;
  }

  // DA Map에서 Runtime Environment 가저온 후 지원하는 것인지 확인한다.
  RuntimeEnvironment rt_env = exec_da.runtime_env;
  switch (rt_env) {
    case RuntimeEnvironment::RE_NATIVE:break;
    case RuntimeEnvironment::RE_JAVA:break;
    case RuntimeEnvironment::RE_PYTHON:break;
      // Not Supported yet.
    case RuntimeEnvironment::RE_NODE:
    case RuntimeEnvironment::RE_RUBY:
    case RuntimeEnvironment::RE_PHP:
    default:
      LOGGER()->debug("Runtime Environment [{}] is not Supported yet.",
                      RuntimeEnvironment_Name(rt_env));
      return;
  }

  RunDialogAgentInstanceParam rsc;
  rsc.set_dai_id(dai_id);
  rsc.set_agent(dai.name());
  rsc.set_version(dai.version());
  rsc.set_description(dai.description());
  auto param = rsc.mutable_param();
  param->mutable_params()->insert(dai.params().begin(), dai.params().end());
  param->set_chatbot(dai.chatbot_name());
  param->mutable_skills()->CopyFrom(dai.skill_names());
  param->set_lang(dai.lang());

  // Instance 갯수 만큼 DAI 실행
  auto logger = LOGGER();
  for (int i = 0; i < cnt; i++) {
    rsc.set_key(GetUUID());
    DamResult dai_status = RunDialogAgentInstance(rsc,
                                                  exec_da,
                                                  START_NEW_DA_INSTANCE);
    if (dai_status.result_code() == LaunchResult::SUCCESS) {
      logger->debug("{} {}/{}/{} DAI has launched.",
                    dai.chatbot_name(),
                    MergeStrings(dai.skill_names()),
                    dai.lang(),
                    dai.dai_id());
    } else {
      logger->debug("{} {}/{}/{} DAI failed to launch.",
                    dai.chatbot_name(),
                    MergeStrings(dai.skill_names()),
                    dai.lang(),
                    dai.dai_id());
      logger->info("DAI Launch failed. msg: {}", dai_status.detail_message());
    }
  }
}

/**
 * DAM에서 실행 중인 DAI를 중지한다.
 * @param dam_name
 */
void DialogAgentIExHandler::StopDialogAgentInstances(const string &dam_name,
                                                     const string &dai_id) {
  vector<DialogAgentIRPortable> dairs;
  {
    auto dair_map = hzc::Instance().GetHazelcastClient()
        ->getMap<string, DialogAgentIRPortable>(hzc::kNsDAIR);
    const string sql = " dam_name='" + dam_name
        + "' and dai_id='" + dai_id
        + "' and launch_type=" + to_string(DAL_DIALOG_AGENT_MANAGER);
    dairs = dair_map.values(SqlPredicate(sql));
  }

  LOGGER()->debug("Stopping {} DAI.", dairs.size());

  for (auto const &dair : dairs) {
    StopDialogAgentInstance(dair.key());
  }
}

/**
* Invoked when an entry is added.
*
* @param event entry event
*/
void DialogAgentIExHandler::entryAdded(const DaiExInfoEvent &event) {
  if (event.getValue().dam_name() != GetDamName()) {
    return;
  }
  const DaiExInfo added_dai = event.getValue();
  LOGGER()->info("DAI Added: {} / {} / {} / {}",
                 added_dai.dai_id(),
                 added_dai.dam_name(),
                 added_dai.active(),
                 added_dai.cnt());
  if (!added_dai.active()) {
    LOGGER()->info("Not Active. Do nothing.");
    return;
  }

  const string added_key = event.getKey();
  std::thread([=]() {
    // Map의 데이타 정합성 확인
    // dai_id, dam_name이 같은 entry가 있으면 검색
    {
      auto daiex_map = hzc::Instance().GetHazelcastClient()
          ->getMap<string, DaiExInfo>(hzc::kNsDAInstExec);
      const string sql = " dai_id='" + added_dai.dai_id()
          + "' and dam_name='" + added_dai.dam_name() + "'";
      auto conflicts = daiex_map.keySet(SqlPredicate(sql));
      // 충돌이 나는 DAI를 중단한다.
      if (conflicts.size() > 0) {
        LOGGER()->info("Dialog Agent Instance {} Conflicts found. Fix it.",
                       conflicts.size());
        StopDialogAgentInstances(added_dai.dam_name(), added_dai.dai_id());
        // 충돌이 나는 DAI를 Map에서 삭제한다.
        for (auto &key : conflicts) {
          if (key != added_key) {
            // entryRemoved()가 호출 되는 것을 막기 위해 잠시 Listener를 없앰.
            RemoveListener();
            daiex_map.deleteEntry(key);
            AddListener();
          }
        }
      }
    }

    StartDialogAgentInstances(added_dai.dai_id(), added_dai.cnt());
  }).detach();
}

/**
* Invoked when an entry is removed.
*
* @param event entry event
*/
void DialogAgentIExHandler::entryRemoved(const DaiExInfoEvent &event) {
  const string &removed_key = event.getKey();
  LOGGER()->info("DAI Removed: {}", removed_key);
  std::thread([=]() {
    string dai_id;
    // DAM 내부 DA Map과 DAAI Map을 비교하여 제거된 DA를 찾는다.
    {
      auto daiex_map = hzc::Instance().GetHazelcastClient()
          ->getMap<string, DaiExInfo>(hzc::kNsDAInstExec);
      std::lock_guard<std::mutex> lock(GetDaiMapMutex());
      for (auto &dai : GetRunningDaiMap()) {
        const string sql = " dai_id='" + dai.second.dai_id
            + "' and dam_name='" + GetDamName() + "'";
        if (daiex_map.values(SqlPredicate(sql)).empty()) {
          dai_id = dai.second.dai_id;
          break;
        }
      }
    }

    if (dai_id.empty()) {
      LOGGER()->info("Removed Dialog Agent Instance[{}] not found.",
                     removed_key);
    } else {
      StopDialogAgentInstances(GetDamName(), dai_id);
    }
  }).detach();
};

/**
* Invoked when an entry is updated.
*
* @param event entry event
*/
void DialogAgentIExHandler::entryUpdated(const DaiExInfoEvent &event) {
  if (event.getValue().dam_name() != GetDamName()) {
    return;
  }
  const DaiExInfo cur_dai = event.getValue();
  LOGGER()->info("DAI Updated: ==> {} / {} / {} / {}",
                 cur_dai.dai_id(),
                 cur_dai.dam_name(),
                 cur_dai.active(),
                 cur_dai.cnt());

  string updated_key = event.getKey();
  std::thread([=]() {
    // DAI를 모두 중지한다.
    StopDialogAgentInstances(cur_dai.dam_name(), cur_dai.dai_id());

    // Map의 데이타 정합성 확인
    // dai_id, dam_name이 같은 entry가 있으면 검색
    {
      auto daiex_map = hzc::Instance().GetHazelcastClient()
          ->getMap<string, DaiExInfo>(hzc::kNsDAInstExec);
      const string sql = " dai_id='" + cur_dai.dai_id()
          + "' and dam_name='" + cur_dai.dam_name() + "'";
      auto conflicts = daiex_map.keySet(SqlPredicate(sql));
      if (conflicts.size() > 1) {
        // 충돌이 나는 DAI를 Map에서 삭제한다.
        for (auto &key : conflicts) {
          LOGGER()->info("Dialog Agent Instance {} Conflicts found. Fix it.",
                         conflicts.size() - 1);
          if (key != updated_key) {
            // entryRemoved()가 호출 되는 것을 막기 위해 잠시 Listener를 없앰.
            RemoveListener();
            daiex_map.deleteEntry(key);
            AddListener();
          }
        }
      }
    }

    // 활성화 되어 있으면 DAI를 다시 시작한다.
    if (cur_dai.active()) {
      StartDialogAgentInstances(cur_dai.dai_id(), cur_dai.cnt());
    }
  }).detach();
}

/**
* Invoked when an entry is evicted.
*
* @param event entry event
*/
void DialogAgentIExHandler::entryEvicted(const DaiExInfoEvent &event) {
  LOGGER()->debug("DialogAgentIExHandler::{} is Unimplemented.", __func__);
};

/**
* Invoked upon expiration of an entry.
*
* @param event the event invoked when an entry is expired.
*/
void DialogAgentIExHandler::entryExpired(const DaiExInfoEvent &event) {
  LOGGER()->debug("DialogAgentIExHandler::{} is Unimplemented.", __func__);
};

/**
*  Invoked after WAN replicated entry is merged.
*
* @param event the event invoked when an entry is expired.
*/
void DialogAgentIExHandler::entryMerged(const DaiExInfoEvent &event) {
  LOGGER()->debug("DialogAgentIExHandler::{} is Unimplemented.", __func__);
};

/**
* Invoked when all entries evicted by {@link IMap#evictAll()}.
*
* @param event map event
*/
void DialogAgentIExHandler::mapEvicted(const MapEvent &event) {
  LOGGER()->debug("DialogAgentIExHandler::{} is Unimplemented.", __func__);
};

/**
* Invoked when all entries are removed by {@link IMap#clear()}.}
*/
void DialogAgentIExHandler::mapCleared(const MapEvent &event) {
  LOGGER()->debug("DialogAgentIExHandler::{} is Unimplemented.", __func__);
};
