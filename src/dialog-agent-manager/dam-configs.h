#ifndef DAM_CONFIGS_H
#define DAM_CONFIGS_H

#include "dialog-agent-manager-types.h"

class DamConfigs {
 public:
  DamConfigs() = delete;
  DamConfigs(const string &dam_name);
  virtual ~DamConfigs() = default;

  inline void SetDamName(const string &dam_name) { dam_name_ = dam_name; }
  inline const string &GetDamName() { return dam_name_; }
  inline const string &GetBinPath() { return bin_path_; }
  inline const string &GetExportIp() { return export_ip_; }
  inline int GetExportPort() { return export_port_; }
  inline const string &GetAdminRemote() { return admin_remote_; }
  inline const string &GetPoolRemote() { return pool_remote_; }
  inline int &GetLastPort() { return last_port_; }
  inline int GetChildWaitMs() { return child_wait_ms_; }
  inline mutex &GetDaMapMutex() { return da_map_mutex_; }
  inline mutex &GetDaiMapMutex() { return dai_map_mutex_; }
  inline ActiveDA &GetActiveDaMap() { return active_da_map_; }
  inline RunInst &GetRunningDaiMap() { return running_dai_map_; }

 private:
  string dam_name_;
  string export_ip_;
  int export_port_;
  string bin_path_;
  string admin_remote_;
  string pool_remote_;
  int child_wait_ms_;
  std::mutex da_map_mutex_;
  std::mutex dai_map_mutex_;
  ActiveDA active_da_map_;
  RunInst running_dai_map_;

  static int last_port_;
};

#endif //DAM_CONFIGS_H
