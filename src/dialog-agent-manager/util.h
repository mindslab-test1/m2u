#ifndef DAM_UTIL_H
#define DAM_UTIL_H

#include <google/protobuf/repeated_field.h>

using google::protobuf::RepeatedPtrField;

int GetPort(const char *ip, int &next_port);
const string GetUUID();
bool IsPoolReady();
string MergeStrings(const RepeatedPtrField<string> &strings,
                    const char merger = ',');

#endif // DAM_UTIL_H
