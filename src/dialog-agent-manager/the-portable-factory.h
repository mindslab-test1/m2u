#ifndef M2U_THE_PORTABLE_FACTORY_H
#define M2U_THE_PORTABLE_FACTORY_H

#include "dialog-agent-manager-portable.h"
#include "dialog-agent-portable.h"
#include "dialog-agent-ai-portable.h"
#include "dialog-agent-instance-portable.h"
#include "dialog-agent-iex-portable.h"
#include "dialog-agent-ir-portable.h"

using std::auto_ptr;
using std::shared_ptr;
using hazelcast::client::serialization::PortableFactory;
using hazelcast::client::serialization::Portable;

class ThePortableFactory : public PortableFactory {
 public:
  auto_ptr<Portable> create(int32_t classId) const override {
    switch (classId) {
      case DIALOG_AGENT_MANAGER: {
        return auto_ptr<Portable>(new DialogAgentManagerPortable());
      }
      case DIALOG_AGENT: {
        return auto_ptr<Portable>(new DialogAgentPortable());
      }
      case DIALOG_AGENT_ACTIVATION_INFO: {
        return auto_ptr<Portable>(new DialogAgentAIPortable());
      }
      case DIALOG_AGENT_INSTANCE: {
        return auto_ptr<Portable>(new DialogAgentInstancePortable());
      }
      case DIALOG_AGENT_INSTANCE_EXECUTION_INFO: {
        return auto_ptr<Portable>(new DialogAgentIExInfoPortable());
      }
      case DIALOG_AGENT_INSTANCE_RESOURCE: {
        return auto_ptr<Portable>(new DialogAgentIRPortable());
      }
      default:return auto_ptr<Portable>();
    }
  }
};

#endif //M2U_THE_PORTABLE_FACTORY_H
