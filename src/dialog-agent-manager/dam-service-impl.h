#include <grpc++/grpc++.h>
#include <json/json.h>
#include <atomic>
#include <mutex>
#include <maum/m2u/server/dam.grpc.pb.h>
#include "da-manager.h"

using grpc::ServerContext;
using grpc::Status;
using google::protobuf::Empty;
using maum::m2u::server::DialogAgentManagerRegisterResult;
using maum::m2u::server::DialogAgentMangerRegInfo;

class DialogAgentManagerImpl final :
    public maum::m2u::server::DialogAgentManager::Service {
 public:
  DialogAgentManagerImpl() {
    dam_ = DialogAgentManager::GetInstance();
  }

  ~DialogAgentManagerImpl() override {
    dam_ = nullptr;
  }

  Status GetRuntimeParameters(ServerContext *context,
                              const DialogAgentName *q,
                              RuntimeParameterList *r) override {
    return dam_->GetRuntimeParameters(q, r);
  }

  Status GetProviderParameter(ServerContext *context,
                              const DialogAgentName *q,
                              DialogAgentProviderParam *r) override {
    return dam_->GetProviderParameter(q, r);
  }

  Status GetUserAttributes(ServerContext *context,
                           const DialogAgentName *q,
                           UserAttributeList *r) override {
    return dam_->GetUserAttributes(q, r);
  }

  Status GetExecutableDA(ServerContext *context,
                         const Empty *,
                         DialogAgentExecutables *r) override {
    return dam_->GetExecutableDialogAgents(r);
  }

  Status SetDamName(ServerContext *context,
                    const DamKey *key,
                    DamResult *r) override {
    DamResult dam_status = dam_->SetDamName(key);
    r->CopyFrom(dam_status);
    return Status::OK;
  }

 private:
  DialogAgentManager *dam_;
};
