#include <uuid/uuid.h>
#include <grpc++/grpc++.h>
#include <maum/m2u/server/pool.grpc.pb.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

#include "util.h"

using std::to_string;
using google::protobuf::Map;
using google::protobuf::Empty;
using grpc::Status;
using grpc::ClientContext;
using grpc::CreateChannel;
using grpc::InsecureChannelCredentials;
using maum::m2u::server::PoolState;
using maum::m2u::server::DialogAgentInstancePool;

int GetPort(const char *ip, int &last_port) {
  auto logger = LOGGER();

  int port;
  int counter = 5;

  libmaum::Config &c = libmaum::Config::Instance();

  std::int32_t min_port;
  std::int32_t max_port;

  try {
    min_port = c.GetAsInt("dam-svcd.random.min.port");
    max_port = c.GetAsInt("dam-svcd.random.max.port");
    port = last_port < min_port ? min_port : last_port + 1;
  } catch (...) {
    min_port = 0;
    max_port = 0;
  }

  if (min_port != 0 && max_port != 0) {
    int increment = -1;
    int candidates = max_port - min_port + 1;
    do {
      if (port > max_port)
        port = min_port + port - max_port;
      logger->info("Gen port : {} min port : {} max port {}",
                   port,
                   min_port,
                   max_port);
      if (CheckPort(ip, port)) {
        --counter;
        logger->debug("[{}] : [port:{} is already used]", counter, port);
        if (increment < 0) {
          if (candidates > counter)
            increment = candidates / counter;
          while (candidates % increment == 0) {
            ++increment;
          }
        } else {
          increment = 1;
        }
        port += increment;
      } else {
        break;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    } while (counter);
    last_port = port;
  } else {
    do {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      port = GenerateRandomPort();
      logger->debug("Gen port : {}", port);
      if (CheckPort(ip, port)) {
        counter--;
        logger->debug("[{}] : [port:{} is already used]", counter, port);
        port = -1;
      } else {
        break;
      }
    } while (counter);
  }

  return port;
}

const string GetUUID() {
  char out[40];
  uuid_t uuid = {0};
  uuid_generate_time_safe(uuid);
  uuid_unparse(uuid, out);
  return string(out);
}

/**
 * DAM 기동 전에 Pool Service가 준비되었는지 확인한다.
 *
 * @return
 */
bool IsPoolReady() {
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  const int waits[] = {5, 5, 20, 0};
  int wait_seconds = 0;

  logger->debug("Check Pool is Ready...");

  const string pool_remote = c.Get("pool.export");
  if (pool_remote.empty()) {
    logger->warn("Pool remote is not set.");
    return false;
  }

  for (int i = 0; i < 4; ++i) {
    auto channel = CreateChannel(pool_remote, InsecureChannelCredentials());
    auto pool = DialogAgentInstancePool::NewStub(channel);

    ClientContext ctx;
    Empty empty;
    PoolState state;

    Status status = pool->IsReady(&ctx, empty, &state);

    if (status.ok() && state.is_ready()) {
      return true;
    } else {
      logger->debug("Pool Error:{}, {}:{}",
                    status.error_code(),
                    status.error_message(),
                    status.error_details());
      wait_seconds += waits[i];
      logger->debug(" Wait more to {}s.", wait_seconds);
    }
    std::this_thread::sleep_for(std::chrono::seconds(waits[i]));
  }

  return false;
}

string MergeStrings(const RepeatedPtrField<string> &strings,
                    const char merger) {
  int count = strings.size() - 1;

  if (count < 0)
    return string();

  string merged;
  for (int i = 0; i < count; ++i) {
    merged += strings[i];
    merged += merger;
  }
  merged += strings[count];

  return merged;
}
