#include <libmaum/common/config.h>
#include <maum/m2u/da/provider.grpc.pb.h>
#include <maum/m2u/da/v1/talk.grpc.pb.h>
#include <maum/m2u/da/v2/talk.grpc.pb.h>
#include <maum/m2u/da/v2p1/talk.grpc.pb.h>
#include <maum/m2u/da/v3/talk.grpc.pb.h>

#include "dialog-agent-client.h"

using google::protobuf::Empty;
using namespace maum::m2u;
using ProviderV3Stub = da::v3::DialogAgentProvider::Stub;
using ProviderV2p1Stub = da::v2p1::DialogAgentProvider::Stub;
using ProviderV2Stub = da::v2::DialogAgentProvider::Stub;
using ProviderV1Stub = da::v1::DialogAgentProvider::Stub;

/**
 * Remote endpoint와 DA Provider Spec를 받아서 DA Stub를 생성한다.
 *
 * @param endpoint
 * @param da_prod_spec
 */
DialogAgentClient::DialogAgentClient(const string &endpoint,
                                     const DialogAgentProviderSpec da_prod_spec)
    : endpoint_(endpoint), prod_spec_(da_prod_spec) {

  if (endpoint_.empty()
      || prod_spec_ == DialogAgentProviderSpec::DAP_SPEC_UNSPECIFIED
      || !DialogAgentProviderSpec_IsValid(prod_spec_)) {
    LOGGER()->debug("Invalid Parameters!! endpoint: [{}] prod_spec: [{}]",
                    endpoint_, prod_spec_);
    return;
  }

  channel_ = CreateChannel(endpoint_, InsecureChannelCredentials());

  switch (prod_spec_) {
    case DialogAgentProviderSpec::DAP_SPEC_V_3: {
      stub_ = da::v3::DialogAgentProvider::NewStub(channel_);
      break;
    }
    case DialogAgentProviderSpec::DAP_SPEC_V_2_P1: {
      stub_ = da::v2p1::DialogAgentProvider::NewStub(channel_);
      break;
    }
    case DialogAgentProviderSpec::DAP_SPEC_V_2: {
      stub_ = da::v2::DialogAgentProvider::NewStub(channel_);
      break;
    }
    case DialogAgentProviderSpec::DAP_SPEC_V_1: {
      stub_ = da::v1::DialogAgentProvider::NewStub(channel_);
      break;
    }
    default:
    case DialogAgentProviderSpec::DAP_SPEC_UNSPECIFIED: {
      LOGGER()->debug("No stub_. Invalid Dialog Provider Spec {}.", prod_spec_);
      break;
    }
  }

}


/**
 * 채널이 Ready인지 확인하고 아니면 max_wait 까지 Ready가 되기를 기다린다.
 *
 * @param max_wait 최대 대기 시간, 단위: 초
 * @return Ready 상태이면 true, 아니면 false
 */
bool DialogAgentClient::CheckStubReady(int max_wait) {
  auto state = channel_->GetState(true);

  if (state != GRPC_CHANNEL_READY) {
    using namespace std::chrono;

    auto logger = LOGGER();
    const system_clock::time_point start = system_clock::now();
    const seconds wait_seconds = seconds(max_wait);
    seconds waiting = seconds(0);
    bool changed;

    logger->flush();
    do {
      if (state == GRPC_CHANNEL_TRANSIENT_FAILURE) {
        logger->debug("Channel in TRANSIENT_FAILURE.");
      } else {
        logger->debug("Channel in state({}).", state);
      }
      // max_wait가 넘었으면 false를 반환한다.
      waiting = duration_cast<seconds>(system_clock::now() - start);
      if (waiting > wait_seconds) {
        logger->debug("Channel Recovery failed.");
        return false;
      }
      // 상태 변화를 최대 max_wait 까지 기다린다.
      changed = channel_
          ->WaitForStateChange(state,
                               system_clock::now() + wait_seconds);
      if (changed) {
        state = channel_->GetState(false);
        // Channel이 ready 상태인지 확인
        if (state == GRPC_CHANNEL_READY) {
          auto took = duration_cast<milliseconds>(system_clock::now() - start);
          logger->debug("Channel Recovered. took {} ms.", took.count());
        }
      }
    } while (state != GRPC_CHANNEL_READY);
  }

  return true;
}

Status DialogAgentClient::IsReady(DialogAgentStatus &das) {
  if (stub_ == nullptr) {
    LOGGER()->debug("Stub is not established.");
    return Status::CANCELLED;
  }

  ClientContext context;
  Empty empty;
  switch (prod_spec_) {
    case DialogAgentProviderSpec::DAP_SPEC_V_3:
      return reinterpret_cast<ProviderV3Stub *>(stub_.get())
          ->IsReady(&context, empty, &das);
    case DialogAgentProviderSpec::DAP_SPEC_V_2_P1:
      return reinterpret_cast<ProviderV2p1Stub *>(stub_.get())
          ->IsReady(&context, empty, &das);
    case DialogAgentProviderSpec::DAP_SPEC_V_2:
      return reinterpret_cast<ProviderV2Stub *>(stub_.get())
          ->IsReady(&context, empty, &das);
    case DialogAgentProviderSpec::DAP_SPEC_V_1:
      return reinterpret_cast<ProviderV1Stub *>(stub_.get())
          ->IsReady(&context, empty, &das);
    default: {
      return Status::CANCELLED;
    }
  }
}

Status DialogAgentClient::Init(maum::m2u::da::InitParameter &init_param,
                               DialogAgentProviderParam &provider_param) {
  if (stub_ == nullptr) {
    LOGGER()->debug("Stub is not established.");
    return Status::CANCELLED;
  }

  ClientContext context;
  switch (prod_spec_) {
    case DialogAgentProviderSpec::DAP_SPEC_V_3:
      return reinterpret_cast<ProviderV3Stub *>(stub_.get())
          ->Init(&context, init_param, &provider_param);
    case DialogAgentProviderSpec::DAP_SPEC_V_2_P1:
      return reinterpret_cast<ProviderV2p1Stub *>(stub_.get())
          ->Init(&context, init_param, &provider_param);
    case DialogAgentProviderSpec::DAP_SPEC_V_2:
      return reinterpret_cast<ProviderV2Stub *>(stub_.get())
          ->Init(&context, init_param, &provider_param);
    case DialogAgentProviderSpec::DAP_SPEC_V_1:
      return reinterpret_cast<ProviderV1Stub *>(stub_.get())
          ->Init(&context, init_param, &provider_param);
    default: {
      return Status::CANCELLED;
    }
  }
}

Status DialogAgentClient::GetRuntimeParameters(RuntimeParameterList &params) {
  if (stub_ == nullptr) {
    LOGGER()->debug("Stub is not established.");
    return Status::CANCELLED;
  }

  ClientContext context;
  Empty empty;
  switch (prod_spec_) {
    case DialogAgentProviderSpec::DAP_SPEC_V_3:
      return reinterpret_cast<ProviderV3Stub *>(stub_.get())
          ->GetRuntimeParameters(&context, empty, &params);
    case DialogAgentProviderSpec::DAP_SPEC_V_2_P1:
      return reinterpret_cast<ProviderV2p1Stub *>(stub_.get())
          ->GetRuntimeParameters(&context, empty, &params);
    case DialogAgentProviderSpec::DAP_SPEC_V_2:
      return reinterpret_cast<ProviderV2Stub *>(stub_.get())
          ->GetRuntimeParameters(&context, empty, &params);
    case DialogAgentProviderSpec::DAP_SPEC_V_1:
      return reinterpret_cast<ProviderV1Stub *>(stub_.get())
          ->GetRuntimeParameters(&context, empty, &params);
    default: {
      return Status::CANCELLED;
    }
  }
}

Status DialogAgentClient::GetProviderParameter(DialogAgentProviderParam &param) {
  if (stub_ == nullptr) {
    LOGGER()->debug("Stub is not established.");
    return Status::CANCELLED;
  }

  ClientContext context;
  Empty empty;
  switch (prod_spec_) {
    case DialogAgentProviderSpec::DAP_SPEC_V_3:
      return reinterpret_cast<ProviderV3Stub *>(stub_.get())
          ->GetProviderParameter(&context, empty, &param);
    case DialogAgentProviderSpec::DAP_SPEC_V_2_P1:
      return reinterpret_cast<ProviderV2p1Stub *>(stub_.get())
          ->GetProviderParameter(&context, empty, &param);
    case DialogAgentProviderSpec::DAP_SPEC_V_2:
      return reinterpret_cast<ProviderV2Stub *>(stub_.get())
          ->GetProviderParameter(&context, empty, &param);
    case DialogAgentProviderSpec::DAP_SPEC_V_1:
      return reinterpret_cast<ProviderV1Stub *>(stub_.get())
          ->GetProviderParameter(&context, empty, &param);
    default: {
      return Status::CANCELLED;
    }
  }

}

Status DialogAgentClient::GetUserAttributes(UserAttributeList &user_attrs) {
  if (stub_ == nullptr) {
    LOGGER()->debug("Stub is not established.");
    return Status::CANCELLED;
  }

  ClientContext context;
  Empty empty;
  switch (prod_spec_) {
    case DialogAgentProviderSpec::DAP_SPEC_V_3:
      return reinterpret_cast<ProviderV3Stub *>(stub_.get())
          ->GetUserAttributes(&context, empty, &empty);
    case DialogAgentProviderSpec::DAP_SPEC_V_2_P1:
      return reinterpret_cast<ProviderV2p1Stub *>(stub_.get())
          ->GetUserAttributes(&context, empty, &user_attrs);
    case DialogAgentProviderSpec::DAP_SPEC_V_2:
      return reinterpret_cast<ProviderV2Stub *>(stub_.get())
          ->GetUserAttributes(&context, empty, &user_attrs);
    case DialogAgentProviderSpec::DAP_SPEC_V_1:
      return reinterpret_cast<ProviderV1Stub *>(stub_.get())
          ->GetUserAttributes(&context, empty, &user_attrs);
    default: {
      return Status::CANCELLED;
    }
  }

}

Status DialogAgentClient::Terminate() {
  if (stub_ == nullptr) {
    LOGGER()->debug("Stub is not established.");
    return Status::CANCELLED;
  }

  ClientContext context;
  Empty empty, empty2;

  switch (prod_spec_) {
    case DialogAgentProviderSpec::DAP_SPEC_V_3:
      return reinterpret_cast<ProviderV3Stub *>(stub_.get())
          ->Terminate(&context, empty, &empty2);
    case DialogAgentProviderSpec::DAP_SPEC_V_2_P1:
      return reinterpret_cast<ProviderV2p1Stub *>(stub_.get())
          ->Terminate(&context, empty, &empty2);
    case DialogAgentProviderSpec::DAP_SPEC_V_2:
      return reinterpret_cast<ProviderV2Stub *>(stub_.get())
          ->Terminate(&context, empty, &empty2);
    case DialogAgentProviderSpec::DAP_SPEC_V_1:
      return reinterpret_cast<ProviderV1Stub *>(stub_.get())
          ->Terminate(&context, empty, &empty2);
    default: {
      return Status::CANCELLED;
    }
  }
}
