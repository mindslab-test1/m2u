#ifndef DIALOG_AGENT_INSTANCE_EXECUTION_INFO_PORTABLE_H
#define DIALOG_AGENT_INSTANCE_EXECUTION_INFO_PORTABLE_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include <maum/m2u/console/da_instance.pb.h>

#include "hazelcast.h"

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

class DialogAgentIExInfoPortable
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::console::DialogAgentInstanceExecutionInfo {
 public:
  DialogAgentIExInfoPortable() = default;
  DialogAgentIExInfoPortable(const DialogAgentIExInfoPortable &rhs) {
    this->CopyFrom(rhs);
  }
  ~DialogAgentIExInfoPortable() override = default;

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  };
  int getClassId() const override {
    return HazelcastTypeId::DIALOG_AGENT_INSTANCE_EXECUTION_INFO;
  };
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeUTF("dai_id", &this->dai_id());
    out.writeUTF("dam_name", &this->dam_name());
    out.writeBoolean("active", this->active());
    out.writeInt("cnt", this->cnt());
    out.writeByteArray("_msg", &data);
  };
  void readPortable(PortableReader &in) override {
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  };
};

#endif
