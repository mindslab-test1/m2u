#ifndef DIALOG_AGENT_PORTABLE_H
#define DIALOG_AGENT_PORTABLE_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include <maum/m2u/console/da.pb.h>

#include "hazelcast.h"

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

class DialogAgentPortable
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::console::DialogAgent {
 public:
  DialogAgentPortable() = default;
  DialogAgentPortable(const DialogAgentPortable &rhs) {
    this->CopyFrom(rhs);
  }
  ~DialogAgentPortable() override = default;

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  };
  int getClassId() const override {
    return HazelcastTypeId::DIALOG_AGENT;
  };
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeUTF("name", &this->name());
    out.writeUTF("description", &this->description());
    out.writeUTF("da_executable", &this->da_executable());
    out.writeUTF("type", &this->type());
    out.writeInt("da_prod_spec", this->da_prod_spec());
    out.writeByteArray("_msg", &data);
  };
  void readPortable(PortableReader &in) override {
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  };
};

#endif
