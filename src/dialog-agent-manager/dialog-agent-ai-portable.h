#ifndef DIALOG_AGENT_AI_PORTABLE_H
#define DIALOG_AGENT_AI_PORTABLE_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include <maum/m2u/console/da.pb.h>

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

class DialogAgentAIPortable
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::console::DialogAgentActivationInfo {
 public:
  DialogAgentAIPortable() = default;
  DialogAgentAIPortable(const DialogAgentAIPortable &rhs) {
    this->CopyFrom(rhs);
  }
  ~DialogAgentAIPortable() override = default;

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  };
  int getClassId() const override {
    return HazelcastTypeId::DIALOG_AGENT_ACTIVATION_INFO;
  };
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeUTF("da_name", &this->da_name());
    out.writeUTF("dam_name", &this->dam_name());
    out.writeBoolean("active", this->active());
    out.writeByteArray("_msg", &data);
  };
  void readPortable(PortableReader &in) override {
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  };
};

#endif
