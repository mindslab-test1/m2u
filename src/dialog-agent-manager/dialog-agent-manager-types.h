#ifndef DIALOG_AGENT_MANAGER_TYPES_H
#define DIALOG_AGENT_MANAGER_TYPES_H

#include <unordered_map>
#include <json/json.h>
#include <grpc++/grpc++.h>
#include <maum/common/lang.pb.h>
#include <maum/m2u/server/dam.grpc.pb.h>
#include <maum/m2u/server/pool.grpc.pb.h>
#include <maum/m2u/console/da.grpc.pb.h>
#include <maum/m2u/console/da_instance.pb.h>
#include <maum/m2u/console/da_instance.grpc.pb.h>

using std::string;
using std::to_string;
using std::vector;
using std::mutex;
using std::lock_guard;

using grpc::Status;
using grpc::StatusCode;
using grpc::ClientContext;
using grpc::CreateChannel;
using grpc::InsecureChannelCredentials;

using maum::m2u::server::RuntimeEnvironment;
using maum::m2u::server::DamDialogAgentResource;
using maum::m2u::server::DamKey;
using maum::m2u::server::DialogAgentExecutables;
using maum::m2u::server::DialogAgentInstances;
using maum::m2u::server::RunDialogAgentInstanceParam;
using maum::m2u::server::DialogAgentName;
using maum::m2u::server::DialogAgentInstancePool;
using maum::m2u::server::RuntimeEnvironment;

using maum::m2u::console::Key;
using maum::m2u::console::DialogAgentAdmin;
using maum::m2u::console::DialogAgent;
using maum::m2u::console::DialogAgentInstance;
using maum::m2u::console::DialogAgentInstanceAdmin;
using maum::m2u::console::DialogAgentInstanceList;
using maum::m2u::console::DialogAgentWithDialogAgentInstance;
using maum::m2u::console::RunDialogAgentInstanceParamList;
using maum::m2u::da::DialogAgentProviderSpec;
using maum::m2u::da::DialogAgentState;
using maum::m2u::da::DialogAgentStatus;
using maum::m2u::da::RuntimeParameterList;
using maum::m2u::da::DialogAgentProviderParam;
using maum::m2u::common::UserAttributeList;

using ProviderStub = std::unique_ptr<void>;
using DamResult = maum::m2u::server::DialogAgentManagerStat;
using LaunchResult = maum::m2u::server::DialogAgentManagerRegisterResult;

struct ActiveDialogAgent {
  string agent;
  string full_exec_cmd;
  string exec_cmd;
  DialogAgentProviderSpec da_prod_spec;
  RuntimeEnvironment runtime_env;
  vector<string> arg_vec;
  RuntimeParameterList rt_param_list;
  DialogAgentProviderParam da_prod_param;
  UserAttributeList user_attr_list;
};

struct RunningDialogAgentInstance {
  pid_t pid;
  string key;
  string endpoint;
  int port;
  string agent;
  string dai_id;
  RunDialogAgentInstanceParam param;
};

struct RunningChild {
  int pid;
  ActiveDialogAgent exec_da;
  RunningDialogAgentInstance inst;
};

using ActiveDA = std::unordered_map<string, const ActiveDialogAgent>;
using ActiveDAPair = std::pair<string, const ActiveDialogAgent>;
using RunInst = std::unordered_map<string, const RunningDialogAgentInstance>;
using RunInstPair = std::pair<string, const RunningDialogAgentInstance>;

#endif //DIALOG_AGENT_MANAGER_TYPES_H
