#ifndef DIALOG_AGENT_MANAGER_PORTABLE_H
#define DIALOG_AGENT_MANAGER_PORTABLE_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include <maum/m2u/console/da.pb.h>

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

class DialogAgentManagerPortable
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::console::DialogAgentManager {
 public:
  DialogAgentManagerPortable() = default;
  DialogAgentManagerPortable(const DialogAgentManagerPortable &rhs) {
    this->CopyFrom(rhs);
  }
  ~DialogAgentManagerPortable() override = default;

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  };
  int getClassId() const override {
    return HazelcastTypeId::DIALOG_AGENT_MANAGER;
  };
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeUTF("name", &this->name());
    out.writeUTF("description", &this->description());
    out.writeUTF("ip", &this->ip());
    out.writeInt("port", this->port());
    out.writeBoolean("active", this->active());
    out.writeBoolean("default", this->default_());
    out.writeLong("create_at", this->create_at().seconds());
    out.writeLong("modify_at", this->modify_at().seconds());
    out.writeByteArray("_msg", &data);
  };
  void readPortable(PortableReader &in) override {
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  };
};

#endif
