#ifndef DIALOG_AGENT_INSTANCE_PORTABLE_H
#define DIALOG_AGENT_INSTANCE_PORTABLE_H

#include <hazelcast/client/HazelcastClient.h>
#include <hazelcast/client/serialization/PortableWriter.h>
#include <hazelcast/client/serialization/PortableReader.h>
#include <hazelcast/util/Util.h>

#include <maum/m2u/console/da_instance.pb.h>

#include "hazelcast.h"

using hazelcast::client::serialization::PortableWriter;
using hazelcast::client::serialization::PortableReader;

class DialogAgentInstancePortable
    : public hazelcast::client::serialization::Portable,
      public maum::m2u::console::DialogAgentInstance {
 public:
  DialogAgentInstancePortable() = default;
  DialogAgentInstancePortable(const DialogAgentInstancePortable &rhs) {
    this->CopyFrom(rhs);
  }
  ~DialogAgentInstancePortable() override = default;

  // virtual method override for hazelcast::client::serialization::Portable
  int getFactoryId() const override {
    return PORTABLE_FACTORY_ID;
  };
  int getClassId() const override {
    return HazelcastTypeId::DIALOG_AGENT_INSTANCE;
  };
  void writePortable(PortableWriter &out) const override {
    int serializeSize = 0;
    serializeSize = this->ByteSize();
    char bArray[serializeSize];
    this->SerializeToArray(bArray, serializeSize);
    std::vector<hazelcast::byte> data(bArray, bArray + serializeSize);

    out.writeByteArray("_msg", &data);
    out.writeUTF("dai_id", &this->dai_id());
    out.writeUTF("chatbot_name", &this->chatbot_name());
    out.writeUTF("da_name", &this->da_name());
    out.writeUTF("name", &this->name());
    out.writeInt("lang", this->lang());
    out.writeUTF("description", &this->description());
    out.writeInt("da_prod_spec", this->da_prod_spec());
  };
  void readPortable(PortableReader &in) override {
    std::vector<hazelcast::byte> vec = *in.readByteArray("_msg");
    ParseFromArray(vec.data(), (int) vec.size());
  };
};

#endif
