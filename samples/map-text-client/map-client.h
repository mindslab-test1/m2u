#ifndef MAP_CLIENT_H
#define MAP_CLIENT_H

#include <grpc++/channel.h>
#include <maum/m2u/map/map.grpc.pb.h>
#include <libmaum/common/config.h>
using grpc::Channel;
using maum::m2u::map::MaumToYouProxyService;
using maum::m2u::map::MapEvent;
using maum::m2u::map::MapDirective;
using maum::m2u::map::EventStream;
using maum::m2u::map::AsyncInterface;
using maum::m2u::common::Device;
using maum::m2u::common::Location;
using google::protobuf::Struct;
using std::string;

class MapClient {
 public:
  MapClient(std::shared_ptr<Channel> channel, string chatbot);
  virtual ~MapClient() {}
  void Echo();
  void EchoStream();
  void Open();
  void SpeechToSpeechText(const string &filename);
  void TextToTextTalk(const string &text, const string &outfile = "");
  void TextToSpeechTalk(const string &text);
  void AudioPlayer(const string &operation, const string &reason);
  void SpeechToText(const string &filename);
  void DialogOpen();
  void DialogClose();
  void SignIn(const string &text);
  void SignOut();
  void IsValid();
  void GetUserInfo();
  void ImageToTextTalk(string& file_name);
  void ImageToSpeechTalk(string& file_name);

 private:
  EventStream *MakeEventStream(MapEvent *mevent,
                               const string &itf,
                               const string &op,
                               bool stream);
  void AddContextDevice(EventStream *estr, const Device &device);
  void AddContextLocation(EventStream *estr, const Location &location);
  void AddParam(EventStream *estr);
  void AddParamSpeechRecognizer(EventStream *estr);
  void AddParamDialogAgentForwarder(EventStream *estr);
  void AddEventPayloadPlay(EventStream *estr, const string &reason);
  void AddPayload(EventStream *estr, const Struct &payload);
  void AddParamImageRecognizer(EventStream *estr);
  //----------
  void MakeEventBytes(MapEvent *mev, const string& bytes);
  void MakeEventText(MapEvent *mev, const string& text);
  void MakeEventMeta(MapEvent *mev, const Struct &str);
  void MakeEventEnd(MapEvent *mev, const EventStream& start);

  void PrintEvent(const MapEvent &msg);
  void PrintDirective(const MapDirective &msg);
  void WriteEvent(const MapEvent &msg, const string &outfile);
  void WriteDirective(const MapDirective &dir, const string &outfile);

  std::unique_ptr<maum::m2u::map::MaumToYouProxyService::Stub> stub_;
  Device device_;
  Location location_;
  Struct payload_;
  Struct sign_in_payload_;
  Struct sign_out_payload_;
  Struct sign_on_payload_;
  Struct is_valid_payload_;
  Struct get_user_info_payload_;
  Struct open_payload_;
  Struct talk_payload_;
  Struct speech_to_text_payload_;
  Struct audio_payload_;

 public:
  string sign_in_;
  string token_;
  string chatbot_;
};

#endif //MAP_CLIENT_H
