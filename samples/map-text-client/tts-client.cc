#ifdef __linux__
#include <string>
#include "tts-client.h"
#include <libmaum/common/encoding.h>
#include <fstream>
#include <sstream>
#include <unistd.h> // get_current_dir_name()
#include <vector>

using std::string;

unsigned int TtsCallback(int chan, unsigned char *voice, int voiceLen);
/**
 * 새로운 TTS 클라이언트를 생성한다.
 *
 * @param ip 원격 주소
 * @param port 포트
 * @param format 전송 포맷
 */
SimpleTtsClient::SimpleTtsClient(char *ip, int port, int format) {
  // 음성합성엔진과 접속하기 위한 클라이언트를 생성한다
  core_tts_ = TTSCreate(ip, port, format);
  if (core_tts_ == NULL) {
    status_ = None;
    return;
  }
  int rc = TTSSetCallback(core_tts_, TtsCallback);
  if (rc != TTS_OK) {
    core_tts_ = nullptr;
    status_ = None;
  } else
    status_ = Created;
}

/**
 * TTS 클라이언트를 삭제한다.
 *
 * 내부적인 상태에 따라서 소멸을 달리하게 된다.
 * 특히, 소멸자에서는 기존의 TTS 클라이언트를 API 맵에서 삭제한다.
 */
SimpleTtsClient::~SimpleTtsClient() {

  switch (status_) {
    case TtsApiStatus::GotChannel:
      if (core_tts_) {
        //
        // ********** 매우 중요한 코드 임 ********
        TtsClientFactory *api = TtsClientFactory::GetInstance();
        api->Unregister(channel_);
        // TODO
        // logging
        channel_ = 0;
      }
    case TtsApiStatus::Opened:
      if (core_tts_) {
        TTSClose(core_tts_);
      }
    case TtsApiStatus::Created:
      if (core_tts_) {
        TTSDelete(core_tts_);
        core_tts_ = nullptr;
      }
      status_ = TtsApiStatus::None;
    default:break;
  }
}

/**
 * 음성 변환용 채널을 확보한다.
 *
 * 채널이 확보되면 이를 API의 맵에 등록한다.
 *
 * @param stream 음성 출력용 스트림을 전달한다.
 * @return 성공하면 참을 반환.
 */
bool SimpleTtsClient::Open() {
  return innerOpen();
}
bool SimpleTtsClient::Open(ClientReaderWriter<AudioUtter, AudioUtter> *stream) {
  stream_ = stream;
  return innerOpen();
}

bool SimpleTtsClient::Open(ClientWriter<AudioUtter> *stream) {
  wstream_ = stream;
  return innerOpen();
}

bool SimpleTtsClient::innerOpen() {
  int rc = TTSOpen(core_tts_);
  if (rc != TTS_OK) {
    return false;
  }
  status_ = TtsApiStatus::Opened;
  // 이미 정상적으로 열린 경우에 채널에서 데이터를 가져오는 경우는 없다고 본다.
  int channel = TTSGetChannel(core_tts_);
  std::cout << "new channel " << channel << " for " << core_tts_ << std::endl;
  if (channel == TTSERR_CHANNEL_ASSIGN) {
    // TODO
    // log 처리,
    // printf("TTSGetChannel Error[%X]: can't get channel number..\n",
    // nChannel);
    return false;
  }
  channel_ = channel;

  status_ = TtsApiStatus::Opened;

  filename_ = "";
  // path가 있는 경우 추가
  if (path_.size())
    filename_ += path_;

  filename_ += "test-tts-out-";
  filename_ += std::to_string(channel_);
  filename_ += '-';
  filename_ += std::to_string(time(0));
  filename_ += ".mulaw";

  // ********* 매우 중요한 코드 ******
  TtsClientFactory *api = TtsClientFactory::GetInstance();
  api->Register(channel_, this);

  status_ = TtsApiStatus::GotChannel;

  return true;
}

string SimpleTtsClient::ConvertForSTT() {
  std::ostringstream cmd;

  string outfile;
  if (path_.size())
    outfile += path_;
  outfile += "test-tts-out-";
  outfile += std::to_string(channel_);
  outfile += '-';
  outfile += std::to_string(time(0));
  outfile += ".pcm";

  cmd
      << "ffmpeg -y "
      << " -f mulaw -ar 16000 -ac 1 -i " << filename_
      << " -ar 16000 -f s16le " << outfile;

  std::cout << "cmd: " << cmd.str()
            << ", cwd " << get_current_dir_name() << std::endl;

  std::system(cmd.str().c_str());

  return outfile;
}

void SimpleTtsClient::SendTtsAsStream(int mode) {
  string outfile = ConvertForSTT();
  std::ifstream ifs(outfile, std::ifstream::binary);

  std::vector<char> buffer((
                               std::istreambuf_iterator<char>(ifs)),
                           (std::istreambuf_iterator<char>()));
  std::cout << "buffer size " << buffer.size() << std::endl;
  // 파일을 쪼개서 조금씩 보낸다.
  AudioUtter utter;
  std::vector<char>::size_type sent = 0;
  while (sent < buffer.size()) {
    size_t remain = buffer.size() - sent;
    if (remain > 2048)
      remain = 2048;
    utter.set_utter(buffer.data() + sent, remain);
    if (mode == AUDIO) {
      stream_->Write(utter);
    } else if (mode == AUDIOTOTEXT) {
      wstream_->Write(utter);
    }

    sent += remain;
    std::cout << "sent: " << sent << ", last sent: " << remain << std::endl;
  }

  ifs.close();
}

/**
 * 음성으로 변환한다.
 *
 * 결과는 지정된 콜백에서 처리된다.
 *
 * @param utter 전송할 텍스트
 */
bool SimpleTtsClient::GetSpeech(const string &utter) {
  string str = Utf8ToEuckr(utter);
  int rc = TTSGetSpeechStream(core_tts_, str.c_str());
  if (rc != TTS_OK) {
    // TODO
    // LOGGING:
    // 예상 가능한 예외 조건
    // 네트워크 에러가 발생하여 처리 중에 에러가 나온다거나... 등등..
    // printf("TTSGetSpeechStream Error[%X]: can't get speech..\n", nRet);
    return false;
  }
  std::cout << "speech  returning " << std::endl;
  return true;
}

/**
 * 전송받은 음성 데이터를 클라이언트에게 전송해주는 콜백.
 *
 * @param chan 이 채널은 클라이언트에서 ::TtsOpen(), ::TtsGetChannel() 후에
 * 얻어진 채널 번호이다. 이 채널번호는 얻어지지마자 ::SimpleTtsClient::Open()에서 자동으로
 * 등록된 것이다. 여기서 등록된 채널번호가 모든 분기점의 시작이다.
 * @param voice 데이터
 * @param voiceLen 데이터의 길이
 */
unsigned int TtsCallback(int chan, unsigned char *voice, int voiceLen) {
  auto api = TtsClientFactory::GetInstance();
  auto client = api->GetClient(chan);
  if (client) {

    std::ofstream ofs(client->TempFile(),
                      std::ofstream::binary | std::ofstream::app
                          | std::ofstream::out);
    ofs.write((const char *) voice, voiceLen);
    ofs.close();

    std::cout << "client: " << client->channel() << ", len :" << voiceLen
              << std::endl;
/*
    auto stream = client->GetStream();
    AudioUtter utter;
    utter.set_utter(voice, voiceLen);
    stream->Write(utter);
*/
    return (unsigned int) voiceLen;
  }
  return 0;
}

// ====================================================================
// TTS CLIENT API
// ====================================================================
atomic<TtsClientFactory *> TtsClientFactory::instance_;
mutex TtsClientFactory::mutex_;

/**
 * 싱글톤 인스턴스를 구해온다.
 *
 * 이를 싱글톤으로 처리하는 이유는 API 초기화를 처리하기 위해서이다.
 *
 * @return 싱글톤 TtsClientFactory 객체
 */
TtsClientFactory *TtsClientFactory::GetInstance() {
  TtsClientFactory *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new TtsClientFactory();
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

TtsClientFactory::TtsClientFactory() {
  StartWSA();
}

TtsClientFactory::~TtsClientFactory() {
  CleanWSA();
  channels_.clear();
}

// TODO
// 나중에는 클라이언트 풀을 이용해서 접속을 처리해야 한다.
/**
 * 새로운 SimpleTtsClient 객체를 생성한다.
 *
 * 나중에는 이미 등록된 클라이언트 풀에서 사용하도록 한다.
 *
 * @return 새로운 SimpleTtsClient 객체
 */
SimpleTtsClient *TtsClientFactory::Create() {
  char buf[50];
  strncpy(buf, server_ip_.c_str(), sizeof(buf));

  SimpleTtsClient *client = new SimpleTtsClient(buf,
                                                server_port_,
                                                speech_format_);
  return client;
}

string sing_tts_file_name = "sing_tts_file.pcm";
unsigned int SingleTtsCallback(int chan, unsigned char *voice, int voiceLen) {
  std::cout << "SingleTtsCallback: " << chan << ", len :" << voiceLen
            << std::endl;

  std::ofstream ofs(sing_tts_file_name,
                    std::ofstream::binary | std::ofstream::app
                        | std::ofstream::out);
  ofs.write((const char *) voice, voiceLen);
  ofs.close();



  return (unsigned int) voiceLen;
}

string SingleTtsAudioFile(char *ip, int port, int format, const char* text)
{
  CoreTTSClient *core_tts = NULL;
  int rc;
  std::cout << " SingleTtsAudioFile ip=" <<ip <<", port=" << port <<", format=" << format <<",text=" <<text << std::endl;
  core_tts = TTSCreate(ip, port, format);
  if (core_tts == NULL) {
    std::cout << " error TTSCreate failed" << std::endl;
    return "";
  }
  rc = TTSSetCallback(core_tts, SingleTtsCallback);
  if (rc != TTS_OK) {
    std::cout << " error TTSSetCallback failed" << std::endl;
    return "";
  }
  rc = TTSOpen(core_tts);
  if (rc != TTS_OK) {
    std::cout << " error TTSOpen failed" << std::endl;
    return "";
  }

  int channel = TTSGetChannel(core_tts);
  if (channel == TTSERR_CHANNEL_ASSIGN) {
    std::cout << " error TTSGetChannel failed" << std::endl;
    return "";
  }

  string utter;
  string str = Utf8ToEuckr(text);
  rc = TTSGetSpeechStream(core_tts, str.c_str());
  if (rc != TTS_OK) {
    std::cout << " error TTSGetSpeechStream failed" << std::endl;
    return "";
  }

  TTSClose(core_tts);
  TTSDelete(core_tts);
  std::cout << " SingleTtsAudioFile ...end" << std::endl;
  return sing_tts_file_name;
}
#endif