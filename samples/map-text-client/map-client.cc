#include <sys/time.h>
#include "map-client.h"

#include <uuid/uuid.h>
#include <google/protobuf/util/json_util.h>
#include <fstream>

using grpc::ClientContext;
using grpc::ClientReaderWriter;
using grpc::ClientAsyncReaderWriter;
using grpc::CompletionQueue;
using grpc::internal::CompletionQueueTag;
using google::protobuf::util::MessageToJsonString;
using google::protobuf::util::JsonOptions;
using google::protobuf::Value;
using google::protobuf::Map;
using std::ifstream;
using maum::common::AudioEncoding;
using maum::m2u::common::Device_Capability;
using maum::common::ko_KR;
using maum::brain::idr::ImageFormat;

using meta_pair = Map<std::string, Value>::value_type;

MapClient::MapClient(std::shared_ptr<Channel> channel, string chatbot)
    : stub_(MaumToYouProxyService::NewStub(channel)) {
  // LOCATION
  location_.set_latitude(10.0);
  location_.set_longitude(120.0);

  string random_id = "CLI_";
  std::string rand_str("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");

  std::random_device rd;
  std::mt19937 generator(rd());

  std::shuffle(rand_str.begin(), rand_str.end(), generator);
  random_id += rand_str.substr(0, 15);

  // DEVICE
  device_.set_id(random_id);
  device_.set_type("Text Client");
  device_.set_version("0.1");
  device_.set_channel("MAPCLI");
  device_.set_timezone("KST+9");
  {
    Device_Capability *support = new Device_Capability();
    support->set_support_action(true);
    support->set_support_expect_speech(true);
    support->set_support_move(true);
    support->set_support_play_audio(true);
    support->set_support_play_video(true);
    support->set_support_render_card(true);
    support->set_support_render_text(true);
    support->set_support_speech_synthesizer(true);
    device_.set_allocated_support(support);

    auto st = device_.mutable_meta();
    auto kv = st->mutable_fields();
    Value value;
    value.set_string_value("마인즈랩");
    kv->insert(meta_pair("manufacture", value));
  }

  chatbot_ = chatbot;

//  user_.set_name("user1");
//  user_.set_user_id("USER0001");
//  {
//    auto st = user_.mutable_meta();
//    auto kv = st->mutable_fields();
//    Value value;
//    value.set_string_value("경기도 성남시 분당구 상평동");
//    kv->insert(meta_pair("address", value));
//  }

//  auto kv = payload_.mutable_fields();
//  {
//    Value v1;
//    v1.set_bool_value(true);
//    kv->insert(meta_pair("booleanValue", v1));
//
//    Value v2;
//    v2.set_number_value(3.1456);
//    kv->insert(meta_pair("number", v2));
//
//    Value v3;
//    v2.set_string_value("value value");
//    kv->insert(meta_pair("key", v3));
//  }

  {
    auto kv = payload_.mutable_fields();
    Value v1;
    v1.set_string_value("admin");
    kv->insert(meta_pair("userkey", v1));

    Value v2;
    v2.set_string_value("1234");
    kv->insert(meta_pair("passphrase", v2));
  }

  {
    auto kv2 = sign_out_payload_.mutable_fields();
    Value v1;
    v1.set_string_value("1602a950-e651-11e1-84be-00145e76c700");
    kv2->insert(meta_pair("authToken", v1));

    Value v2;
    v2.set_string_value("admin");
    kv2->insert(meta_pair("userKey", v2));
  }

  auto kv3 = is_valid_payload_.mutable_fields();
  {
    Value v1;
    v1.set_string_value("1602a950-e651-11e1-84be-00145e76c700");
    kv3->insert(meta_pair("authToken", v1));
  }

  auto kv4 = get_user_info_payload_.mutable_fields();
  {
    Value v1;
    v1.set_string_value("eac2a31c-f5e3-50c2-bc1a-ac10be7916c5");
    kv4->insert(meta_pair("accessToken", v1));
  }

  auto kv5 = sign_on_payload_.mutable_fields();
  {
    Value v1;
    v1.set_string_value("1602a950-e651-11e1-84be-00145e76c700");
    kv5->insert(meta_pair("authToken", v1));

    Value v2;
    v2.set_string_value("admin");
    kv5->insert(meta_pair("userKey", v2));
  }

  auto kv6 = open_payload_.mutable_fields();
  {
    Value v1;
    v1.set_string_value(chatbot_);
    kv6->insert(meta_pair("chatbot", v1));

    Value v2;
    v2.set_string_value("ko_KR");
    kv6->insert(meta_pair("lang", v2));

    Value v3;
    v3.set_string_value("cs 열어줘");
    kv6->insert(meta_pair("utter", v3));

    Struct meta;
    auto meta_fields = meta.mutable_fields();
    {
      Value meta_v1;
      meta_v1.set_string_value("CrbuCX40TCe33yEYNz49PQ");
      meta_fields->insert(meta_pair("userId", meta_v1));

      Value meta_v2;
      meta_v2.set_string_value("3b97dba7-baf5-4f7f-9ab7-f4abb71aace0");
      meta_fields->insert(meta_pair("accessToken", meta_v2));

      Value meta_v3;
      meta_v3.set_string_value("시작해줘");
      meta_fields->insert(meta_pair("orderType", meta_v3));

      Value meta_v4;
      meta_v4.set_string_value("영어말하기");
      meta_fields->insert(meta_pair("lectureType", meta_v4));
    }

    // payload에 meta 정보 CopyFrom
    Value v4;
    auto tmp3 = v4.mutable_struct_value();
    tmp3->CopyFrom(meta);
    kv6->insert(meta_pair("meta", v4));
  }

  auto kv7 = talk_payload_.mutable_fields();
  {
    Value v1;
    v1.set_string_value(chatbot_);
    kv7->insert(meta_pair("chatbot", v1));

    Value v2;
    v2.set_string_value("ko_KR");
    kv7->insert(meta_pair("lang", v2));

    Value v3;
    v3.set_string_value("cs 열어줘");
    kv7->insert(meta_pair("utter", v3));

    Value v4;
    v4.set_string_value("KEYBOARD");
    kv7->insert(meta_pair("inputType", v4));

    Struct meta;
    auto meta_fields = meta.mutable_fields();
    {
      Value meta_v1;
      meta_v1.set_string_value("CrbuCX40TCe33yEYNz49PQ");
      meta_fields->insert(meta_pair("userId", meta_v1));

      Value meta_v2;
      meta_v2.set_string_value("3b97dba7-baf5-4f7f-9ab7-f4abb71aace0");
      meta_fields->insert(meta_pair("accessToken", meta_v2));

      Value meta_v3;
      meta_v3.set_string_value("시작해줘");
      meta_fields->insert(meta_pair("orderType", meta_v3));

      Value meta_v4;
      meta_v4.set_string_value("영어말하기");
      meta_fields->insert(meta_pair("lectureType", meta_v4));
    }

    // payload에 meta 정보 CopyFrom
    Value v5;
    auto tmp3 = v5.mutable_struct_value();
    tmp3->CopyFrom(meta);
    kv7->insert(meta_pair("meta", v5));
  }

  auto kv8 = speech_to_text_payload_.mutable_fields();
  {
    Value v1;
    v1.set_string_value(chatbot_);
    kv8->insert(meta_pair("chatbot", v1));

    Value v2;
    v2.set_string_value("SKILL1");
    kv8->insert(meta_pair("skill", v2));

    Value v3;
    v3.set_string_value("UTTER1");
    kv8->insert(meta_pair("utter", v3));
  }

  sign_in_ = "asdfasdfasfd";
  token_ = "FIX_AUTH_TOKEN";
}

void MapClient::PrintEvent(const MapEvent &echo) {
  LOGGER()->debug(
      ">> EVENT {} {}", std::to_string((int) echo.test_event_case()), "\n");
  auto jopt = JsonOptions();
  jopt.add_whitespace = true;
  std::string json;
  MessageToJsonString(echo.event().payload(), &json, jopt);
  LOGGER()->debug(json);
}

void MapClient::WriteEvent(const MapEvent &msg, const string &outfile) {
  auto log = LOGGER();
  log->debug(">> Write Event {} ", std::to_string((int)msg.test_event_case()));
  auto jopt = JsonOptions();
  jopt.add_whitespace = true;
  std::string json;
  MessageToJsonString(msg.event().payload(), &json, jopt);
  std::ofstream ofs;
  try {
    ofs.open(outfile, std::ofstream::out | std::ofstream::app);
    ofs << ">> Event:\n" << json << std::endl;
    usleep(50 * 1000);
    ofs.close();
  } catch (std::exception &e) {
    log->error("Cannot write result on {}, cause:{}", outfile, e.what());
  }
  log->debug(">> Write {} complete!", outfile);
}

void MapClient::PrintDirective(const MapDirective &dir) {
  auto logger = LOGGER();

  logger->debug(
      ">> DIRECTIVE {} {}",
      std::to_string((int) dir.test_directive_case()),
      "\n");
  logger->debug("EventStream response: {}", dir.DebugString());

  auto payload = dir.directive().payload().fields();

  if (!payload.empty()) { // payload가 비어있는지 체크
    if (payload.find("intent") != payload.end()) {
      // intent와 skill은 map_cli 실행창에서 출력하지 않는다.
      logger->debug("intent, skill is not print");
    } else if (payload.find("text") != payload.end()) {
      // SpeechResponse인 경우, 응답 발화만 출력한다.
      auto utter = payload.find("text")->second;
      std::cout << "[response text] " << utter.string_value() << std::endl;
    } else {
      // card, directive는 json 형식으로 출력한다.
      auto jopt = JsonOptions();
      jopt.add_whitespace = true;
      std::string json;
      MessageToJsonString(dir.directive().payload(), &json, jopt);
      std::cout << "[response " << payload.begin()->first << "]\n" << json
                << std::endl;
    }
  }
}

void MapClient::WriteDirective(const MapDirective &dir, const string &outfile) {
  auto log = LOGGER();
  log->debug(">> Write DIRECTIVE {}", std::to_string((int)dir.test_directive_case()));
  auto payload = dir.directive().payload().fields();
  auto jopt = JsonOptions();
  std::ofstream ofs;
  jopt.add_whitespace = true;
  std::string json;
  try {
    ofs.open(outfile, std::ofstream::out | std::ofstream::app);
    if (!payload.empty()) {
      auto itf = payload.find("intent");
      auto text = payload.find("text");
      if (itf != payload.end()) {
        MessageToJsonString(itf->second, &json, jopt);
        ofs << "<< Directive:\n" << json << std::endl;
      } else if (text != payload.end()) {
        MessageToJsonString(text->second, &json, jopt);
        ofs << "<< Directive:\n" << json << std::endl;
      } else {
        auto jopt = JsonOptions();
        jopt.add_whitespace = true;
        MessageToJsonString(dir.directive().payload(), &json, jopt);
        ofs << "<< Directive:\n" << json << std::endl;
      }
    }
    usleep(50 * 1000);
    ofs.close();
  } catch (std::exception &e) {
    log->error("Cannot write result on {}, cause:{}", outfile, e.what());
  }
  log->debug(">> Write {} complete!", outfile);
}

EventStream *MapClient::MakeEventStream(MapEvent *mevent,
                                        const string &itf,
                                        const string &op,
                                        bool stream) {
  EventStream *event_stream = mevent->mutable_event();

  auto ai = event_stream->mutable_interface();
  ai->set_interface(itf);
  ai->set_operation(op);
  ai->set_streaming(stream);
  ai->set_type(AsyncInterface::OP_EVENT);

  char out[40];
  uuid_t uuid = {0};
  uuid_generate_time_safe(uuid);
  uuid_unparse(uuid, out);
  event_stream->set_stream_id(out);

  uuid_generate_time_safe(uuid);
  uuid_unparse(uuid, out);
  event_stream->set_operation_sync_id(out);

  timeval tv;
  gettimeofday(&tv, NULL);
  auto ts = event_stream->mutable_begin_at();
  ts->set_seconds(tv.tv_sec);
  ts->set_nanos(tv.tv_usec * 1000);

  return event_stream;
}

void MapClient::AddContextDevice(EventStream *estr, const Device &device) {
  auto c = estr->mutable_contexts();
  auto ec1 = c->Add();
  Device *ctx_dev = ec1->mutable_device();
  ctx_dev->CopyFrom(device);
}

void MapClient::AddContextLocation(EventStream *estr,
                                   const Location &location) {
  auto c = estr->mutable_contexts();
  auto ec1 = c->Add();
  Location *ctx_dev = ec1->mutable_location();
  ctx_dev->CopyFrom(location);
}

void MapClient::AddParam(EventStream *estr) {
  auto param = estr->mutable_param();
  param->mutable_empty_param();
}

void MapClient::AddParamSpeechRecognizer(EventStream *estr) {
  auto param = estr->mutable_param();
  auto sr = param->mutable_speech_recognition_param();
  sr->set_sample_rate(16000);
  sr->set_encoding(AudioEncoding::LINEAR16);
  sr->set_lang(ko_KR);
  sr->set_model("baseline");
}

void MapClient::AddParamImageRecognizer(EventStream *estr) {
  auto param = estr->mutable_param();
  //auto sr = param->mutable_speech_recognition_param();
  auto ir = param->mutable_image_recognition_param();

  ir->set_width(1280);
  ir->set_height(720);
  ir->set_image_format(ImageFormat::BMP);
  ir->set_ref_vertex_x(0.104167);
  ir->set_ref_vertex_y(0.138889);
  ir->set_symm_crop(true);
  ir->set_lang(ko_KR);
}

void MapClient::AddParamDialogAgentForwarder(EventStream *estr) {
  auto param = estr->mutable_param();

  auto da = param->mutable_dialog_agent_forwarder_param();
  da->set_chatbot(chatbot_);
  da->set_skill("deposit");
  da->set_intent("INTENT1");
  da->set_session_id(1234567);
  da->set_lang(ko_KR);
}

void MapClient::AddEventPayloadPlay(EventStream *estr, const string &reason) {
  auto eventPayload = estr->mutable_payload();
  auto ep = eventPayload->mutable_fields();
  {
    google::protobuf::Value v1;
    v1.set_string_value("123812301");
    ep->insert(meta_pair("id", v1));
    google::protobuf::Value v2;
    v2.set_string_value("stop");
    ep->insert(meta_pair("reason", v2));
  }
}

void MapClient::AddPayload(EventStream *estr, const Struct &payload) {
  auto s = estr->mutable_payload();
  s->CopyFrom(payload);
}

void MapClient::MakeEventBytes(MapEvent *mev, const string &bytes) {
  mev->mutable_bytes()->assign(bytes);
}

void MapClient::MakeEventText(MapEvent *mev, const string &text) {
  mev->mutable_text()->assign(text);
}

void MapClient::MakeEventMeta(MapEvent *mev, const Struct &str) {
  auto meta = mev->mutable_meta();
  meta->set_object_type("echo data type");
  meta->mutable_meta()->CopyFrom(str);
}

void MapClient::MakeEventEnd(MapEvent *mev, const EventStream &start) {
  auto str_end = mev->mutable_stream_end();
  str_end->set_stream_id(start.stream_id());
  timeval tv;
  gettimeofday(&tv, NULL);
  auto ts = str_end->mutable_end_at();
  ts->set_seconds(tv.tv_sec);
  ts->set_nanos(tv.tv_usec * 1000);
}

void MapClient::Echo() {
  ClientContext ctx;

  std::unique_ptr<ClientReaderWriter<MapEvent, MapDirective>> stream =
      stub_->EventStream(&ctx);

  MapEvent echo;
  EventStream *event_stream = MakeEventStream(&echo, "Echo", "Single", false);
  AddContextDevice(event_stream, device_);
  AddParam(event_stream);
  AddPayload(event_stream, payload_);

  stream->Write(echo);
  PrintEvent(echo);

  stream->WritesDone();
  MapDirective dir;
  while (stream->Read(&dir)) {
    PrintDirective(dir);
    if (dir.has_directive()) {
      if (dir.directive().operation_sync_id() ==
          echo.event().operation_sync_id()) {
        LOGGER()->debug("operation sync id is SAME: {}",
                        echo.event().operation_sync_id());
      } else {
        std::cout << "event operation sync id is different from directive"
                  << std::endl;
        LOGGER()->debug("operation sync id is not SAME (e): {}, (d): {}",
                        echo.event().operation_sync_id(),
                        dir.directive().operation_sync_id());
      }
    }
  }

  grpc::Status status = stream->Finish();
  if (status.ok()) {
    std::cout << "status: OK!" << status.error_message() << std::endl;
  } else {
    std::cout << "status: " << status.error_message() << std::endl;
  }
  std::cout << std::endl << std::endl << std::endl << std::endl;
}

void MapClient::EchoStream() {
  ClientContext ctx;

  std::unique_ptr<ClientReaderWriter<MapEvent, MapDirective>> stream =
      stub_->EventStream(&ctx);

  MapEvent echo_start;
  EventStream *event_stream = MakeEventStream(&echo_start,
                                              "Echo", "Stream", true);
  AddContextDevice(event_stream, device_);
  AddParam(event_stream);
  AddPayload(event_stream, payload_);

  // WRITE STREAM EVENT
  stream->Write(echo_start);
  PrintEvent(echo_start);

  for (int i = 0; i < 10; i++) {
    MapEvent echo_bytes;
    MakeEventBytes(&echo_bytes, "echo binary");
    stream->Write(echo_bytes);
    PrintEvent(echo_bytes);
  }

  for (int i = 0; i < 10; i++) {
    MapEvent echo_text;
    MakeEventText(&echo_text, "echo text");
    stream->Write(echo_text);
    PrintEvent(echo_text);
  }

  for (int i = 0; i < 10; i++) {
    MapEvent echo_meta;
    Struct str;
    auto kv = str.mutable_fields();
    Value v1;
    v1.set_bool_value((i % 2) == 1);
    kv->insert(meta_pair("booleanValue", v1));

    Value v2;
    v2.set_number_value((double) (i * 100));
    kv->insert(meta_pair("number", v2));

    MakeEventMeta(&echo_meta, str);
    stream->Write(echo_meta);
    PrintEvent(echo_meta);
  }

  MapEvent echo_end;
  MakeEventEnd(&echo_end, echo_start.event());
  stream->Write(echo_end);
  PrintEvent(echo_end);
  stream->WritesDone();

  MapDirective dir;
  while (stream->Read(&dir)) {
    PrintDirective(dir);
    if (dir.has_directive()) {
      if (dir.directive().operation_sync_id() ==
          echo_start.event().operation_sync_id()) {
        LOGGER()->debug("operation sync id is SAME: {}",
                        echo_start.event().operation_sync_id());
      } else {
        std::cout << "event operation sync id is different from directive"
                  << std::endl;
        LOGGER()->debug("operation sync id is not SAME (e): {}, (d): {}",
                        echo_start.event().operation_sync_id(),
                        dir.directive().operation_sync_id());
      }
    }
  }

  grpc::Status status = stream->Finish();
  if (status.ok()) {
    std::cout << "status: OK!" << status.error_message() << std::endl;
  } else {
    std::cout << "status: " << status.error_message() << std::endl;
  }

}

enum CqTag {
  CQ_INIT = 1,
  CQ_OPEN = 2,
  CQ_STREAM = 3,
  CQ_BODY = 4,
  CQ_END = 5,
  CQ_DONE = 6,
  CQ_READ = 7,
  CQ_FINISH = 8,
  CQ_AUDIOPLAY_PLAY = 9,
  CQ_AUDIOPLAY_STOP = 10
};

void *tag(CqTag i) { return (void *) (intptr_t) (int) i; }
CqTag detag(void *p) { return static_cast<CqTag>(reinterpret_cast<intptr_t>(p)); }

void MapClient::Open() {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  string auth_token = "1602a950-e651-11e1-84be-00145e76c700";

  ctx.AddMetadata("m2u-auth-token", auth_token);
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  logger->debug("add metaData to ClientContext key: m2u-auth-token, value: {}",
                auth_token);
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));
  std::cout << device_.ShortDebugString() << std::endl;
  MapEvent s2s_open;
  {
    EventStream *event_stream = MakeEventStream(&s2s_open,
                                                "Dialog", "Open", false);
    AddContextDevice(event_stream, device_);
    AddContextLocation(event_stream, location_);
    //AddContextUser(event_stream, user_);
    AddPayload(event_stream, open_payload_);
  }

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(5, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("open timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      logger->debug("open got_event, stream finished is {}", finished);
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          // WRITE STREAM EVENT
          PrintEvent(s2s_open);
          stream->Write(s2s_open, tag(CQ_OPEN));
          break;
        }
        case CQ_OPEN: {
          // WRITE STREAM EVENT
          stream->Read(&dir, tag(CQ_READ));
        }
        case CQ_STREAM:

        case CQ_END: {

          break;
        }
        case CQ_DONE: {

          break;
        }
        case CQ_READ: {
          PrintDirective(dir);
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("------------------- shutdown ----");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    logger->debug("status: OK!" + st.error_message());
    std::cout << "status: OK!" << st.error_message() << std::endl;
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
  }
}

void MapClient::SpeechToSpeechText(const string &filename) {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  string auth_token = "1602a950-e651-11e1-84be-00145e76c700";

  ctx.AddMetadata("m2u-auth-token", auth_token);
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  logger->debug("add metaData to ClientContext key: m2u-auth-token, value: {}",
                auth_token);
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Dialog",
                                              "SpeechToSpeechTalk",
                                              true);
  AddContextDevice(event_stream, device_);
  AddContextLocation(event_stream, location_);
  AddParamSpeechRecognizer(event_stream);
  bool finished = false;

  ifstream ifs(filename);
  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("SpeechToSpeechText timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      logger->debug("SpeechToSpeechText got_event, stream finished is {}",
                    finished);
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_STREAM));
          break;
        }
        case CQ_STREAM:
        case CQ_BODY: {
          char buf[3200];
          std::streamsize sz = ifs.readsome(buf, sizeof buf);
          if (sz > 0) {
            MapEvent bytes;
            bytes.mutable_bytes()->assign(buf, sz);
            stream->Write(bytes, tag(CQ_BODY));
            PrintEvent(bytes);
          }
          if (sz <= 0 && ifs.rdbuf()->in_avail() <= 0) {
            MapEvent end;
            MakeEventEnd(&end, s2s_talk.event());
            stream->Write(end, tag(CQ_END));
            PrintEvent(end);
          }
          break;
        }
        case CQ_END: {
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          // stream->Read(&dir, tag(CQ_READ));
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("------------------- shutdown ----");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    logger->debug("status: OK!" + st.error_message());
  } else {
    logger->error("status: OK!" + st.error_message());
  }
}

void MapClient::DialogOpen() {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Dialog", "Open", false);
  AddContextDevice(event_stream, device_);
  AddContextLocation(event_stream, location_);
  //AddContextUser(event_stream, user_);
  AddPayload(event_stream, open_payload_);

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("DialogOpen timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("DialogOpen CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("DialogOpen CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("DialogOpen CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("DialogOpen CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("DialogOpen CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("DialogOpen CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("DialogOpen CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {

      logger->debug("DialogOpen shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    std::cout << "status: OK!" << st.error_message() << std::endl;
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::DialogClose() {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Dialog", "Close", false);
  AddContextDevice(event_stream, device_);
  AddContextLocation(event_stream, location_);
  //AddContextUser(event_stream, user_);
  AddPayload(event_stream, open_payload_);

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("DialogClose timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {

      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("DialogClose CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("DialogClose CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("DialogClose CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("DialogClose CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("DialogClose CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("DialogClose CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("DialogClose CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {

      logger->debug("DialogClose shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    std::cout << "status: OK!" << st.error_message() << std::endl;
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::AudioPlayer(const string &operation, const string &reason) {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");

  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "AudioPlayer", operation, false);
  AddContextDevice(event_stream, device_);
  AddContextLocation(event_stream, location_);
  AddParamDialogAgentForwarder(event_stream);
  AddEventPayloadPlay(event_stream, reason);

  auto param = event_stream->mutable_payload();
  auto kv = param->mutable_fields();
  {
    google::protobuf::Value v1;
    v1.set_string_value("123812301");
    kv->insert(meta_pair("id", v1));
    google::protobuf::Value v2;
    v2.set_string_value("stop");
    kv->insert(meta_pair("reason", v2));
  }

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("AudioPlayer timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("AudioPlayer CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("AudioPlayer CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("AudioPlayer CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("AudioPlayer CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("AudioPlayer CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("AudioPlayer CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());

            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("AudioPlayer CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("AudioPlayer shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::SpeechToText(const string &filename) {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Dialog",
                                              "SpeechToTextTalk",
                                              true);
  AddContextDevice(event_stream, device_);
  //AddContextUser(event_stream, user_);
  AddParamSpeechRecognizer(event_stream);
  bool finished = false;

  ifstream ifs(filename);
  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("SpeechToText timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("SpeechToText CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_STREAM));
          break;
        }
        case CQ_STREAM: {
          logger->debug("SpeechToText CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("SpeechToText CQ_BODY");
          char buf[3200];
          std::streamsize sz = ifs.readsome(buf, sizeof buf);
          if (sz > 0) {
            MapEvent bytes;
            bytes.mutable_bytes()->assign(buf, sz);
            stream->Write(bytes, tag(CQ_BODY));
            PrintEvent(bytes);
          }
          if (sz <= 0 && ifs.rdbuf()->in_avail() <= 0) {
            MapEvent end;
            MakeEventEnd(&end, s2s_talk.event());
            stream->Write(end, tag(CQ_END));
            PrintEvent(end);
          }
          break;
        }
        case CQ_END: {
          logger->debug("SpeechToText CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("SpeechToText CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("SpeechToText CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:" +
                  s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("SpeechToText CQ_FINISH, stream is finished");
          // stream->Read(&dir, tag(CQ_READ));
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      std::cout << "SpeechToText shutdown" << std::endl;
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::TextToTextTalk(const string &text, const string &outfile) {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Dialog",
                                              "TextToTextTalk",
                                              false);
  AddContextDevice(event_stream, device_);
  AddPayload(event_stream, talk_payload_);

  bool finished = false;

  MapDirective dir;
  auto utter_map = talk_payload_.fields().find("utter");
  if (utter_map != talk_payload_.fields().end()) {
    auto utter = talk_payload_.mutable_fields()->find("utter");
    Value v;
    v.set_string_value(text);
    utter->second = v;
    logger->debug("change utter : {} ", text);
  }

  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("TextToTextTalk timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      logger->debug("TextToTextTalk got_event, stream finished is {}",
                    finished);
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("TextToTextTalk CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          // -f  옵션 실행 시 이벤트 파일 저장
          if (!outfile.empty()) {
            WriteEvent(s2s_talk, outfile);
          }
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("TextToTextTalk CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("TextToTextTalk CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("TextToTextTalk CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("TextToTextTalk CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("TextToTextTalk CQ_READ");
          PrintDirective(dir);
          // -f  옵션 실행 시 결과 파일 저장
          if (!outfile.empty()) {
            WriteDirective(dir, outfile);
          }
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("TextToTextTalk CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("TextToTextTalk shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::TextToSpeechTalk(const string &text) {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Dialog",
                                              "TextToSpeechTalk",
                                              false);
  AddContextDevice(event_stream, device_);
  AddPayload(event_stream, talk_payload_);

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("TextToSpeechTalk timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("TextToSpeechTalk CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("TextToSpeechTalk CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("TextToSpeechTalk CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("TextToSpeechTalk CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("TextToSpeechTalk CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("TextToSpeechTalk CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }

            /*if (!dir.directive().payload().fields().empty()) {
              auto m = dir.directive().payload().fields();
              for (auto i = m.begin(); i != m.end(); i++) {
                std::cout << "key: " << i->first << std::endl;
                //std::cout << "value: " << i->second <<std::endl;
              }
            }*/
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("TextToSpeechTalk CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("tts shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::SignIn(const string &text) {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-sign-in", "eac2a31c-f5e3-50c2-bc1a-ac10be7916c5");
  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Authentication",
                                              "SignIn",
                                              false);
  AddContextDevice(event_stream, device_);
  //AddContextUser(event_stream, user_);
  AddPayload(event_stream, payload_);

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("SignIn timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      logger->debug("SignIn got_event, stream finished is {}", finished);
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("SignIn CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("SignIn CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("SignIn CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("SignIn CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("SignIn CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("SignIn CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("SignIn CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("SignIn shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    std::cout << "status: OK!" << st.error_message() << std::endl;
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::SignOut() {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-sign-in", "eac2a31c-f5e3-50c2-bc1a-ac10be7916c5");
  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Authentication",
                                              "SignOut",
                                              false);
  AddContextDevice(event_stream, device_);
  //AddContextUser(event_stream, user_);
  AddPayload(event_stream, sign_out_payload_);

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("SignOut timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("SignOut CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("SignIn CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("SignIn CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("SignIn CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("SignIn CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("SignIn CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("SignIn CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("SignOut shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    std::cout << "status: OK!" << st.error_message() << std::endl;
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status: " + st.error_message());
  }
}

void MapClient::IsValid() {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Authentication",
                                              "IsValid",
                                              false);
  AddContextDevice(event_stream, device_);
  //AddContextUser(event_stream, user_);
  AddPayload(event_stream, is_valid_payload_);

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("IsValid timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("IsValid CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("IsValid CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("IsValid CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("IsValid CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("IsValid CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("IsValid CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("IsValid CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("IsValid shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    std::cout << "status: OK!" << st.error_message() << std::endl;
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::GetUserInfo() {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");
  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Authentication",
                                              "GetUserInfo",
                                              false);
  AddContextDevice(event_stream, device_);
  //AddContextUser(event_stream, user_);
  AddPayload(event_stream, get_user_info_payload_);

  bool finished = false;

  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("GetUserInfo timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("GetUserInfo CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_END));
          break;
        }
        case CQ_STREAM: {
          logger->debug("GetUserInfo CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("GetUserInfo CQ_BODY");
          break;
        }
        case CQ_END: {
          logger->debug("GetUserInfo CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("GetUserInfo CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("GetUserInfo CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("GetUserInfo CQ_FINISH, stream is finished");
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("GetUserInfo shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    std::cout << "status: OK!" << st.error_message() << std::endl;
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::ImageToTextTalk(string &file_name) {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  std::cout << "ImageToTextTalk file_name=" << file_name.c_str() << std::endl;
  ctx.AddMetadata("m2u-auth-sign-in", "asdfasdfasfd");
  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");

  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Dialog",
                                              "ImageToTextTalk",
                                              true);
  AddContextDevice(event_stream, device_);
  AddContextLocation(event_stream, location_);
  AddParamImageRecognizer(event_stream);
  bool finished = false;

  ifstream ifs(file_name);
  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("ImageToTextTalk timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("ImageToTextTalk CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_STREAM));
          break;
        }
        case CQ_STREAM: {
          logger->debug("ImageToTextTalk CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("ImageToTextTalk CQ_BODY");
          char buf[3200];
          std::streamsize sz = ifs.readsome(buf, sizeof buf);
          if (sz > 0) {
            MapEvent bytes;
            bytes.mutable_bytes()->assign(buf, sz);
            stream->Write(bytes, tag(CQ_BODY));
            PrintEvent(bytes);
          }
          if (sz <= 0 && ifs.rdbuf()->in_avail() <= 0) {
            MapEvent end;
            MakeEventEnd(&end, s2s_talk.event());
            stream->Write(end, tag(CQ_END));
            PrintEvent(end);
          }
          break;
        }
        case CQ_END: {
          logger->debug("ImageToTextTalk CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("ImageToTextTalk CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("ImageToTextTalk CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("ImageToTextTalk CQ_FINISH, stream is finished");
          // stream->Read(&dir, tag(CQ_READ));
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("ImageToTextTalk shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

void MapClient::ImageToSpeechTalk(string &file_name) {
  ClientContext ctx;
  CompletionQueue cq;
  auto logger = LOGGER();

  std::cout << "ImageToTextTalk file_name=" << file_name.c_str() << std::endl;
  ctx.AddMetadata("m2u-auth-sign-in", "asdfasdfasfd");
  ctx.AddMetadata("m2u-auth-token", "1602a950-e651-11e1-84be-00145e76c700");

  std::unique_ptr<ClientAsyncReaderWriter<MapEvent, MapDirective>> stream(
      stub_->AsyncEventStream(&ctx, &cq, tag(CQ_INIT)));
  grpc::Status st;
  stream->Finish(&st, tag(CQ_FINISH));

  MapEvent s2s_talk;
  EventStream *event_stream = MakeEventStream(&s2s_talk,
                                              "Dialog",
                                              "ImageToSpeechTalk",
                                              true);
  AddContextDevice(event_stream, device_);
  AddContextLocation(event_stream, location_);
  AddParamImageRecognizer(event_stream);
  bool finished = false;

  ifstream ifs(file_name);
  MapDirective dir;
  while (!finished) {
    void *got_tag;
    bool ok;
    auto r = cq.AsyncNext(&got_tag,
                          &ok,
                          gpr_time_from_millis(10, GPR_CLOCK_REALTIME));
    if (r == CompletionQueue::TIMEOUT) {
      //logger->debug("ImageToSpeechTalk timeout");
    } else if (r == CompletionQueue::GOT_EVENT) {
      CqTag cq_tag = detag(got_tag);
      switch (cq_tag) {
        case CQ_INIT: {
          logger->debug("ImageToSpeechTalk CQ_INIT");
          // WRITE STREAM EVENT
          PrintEvent(s2s_talk);
          stream->Write(s2s_talk, tag(CQ_STREAM));
          break;
        }
        case CQ_STREAM: {
          logger->debug("ImageToSpeechTalk CQ_STREAM");
          break;
        }
        case CQ_BODY: {
          logger->debug("ImageToSpeechTalk CQ_BODY");
          char buf[3200];
          std::streamsize sz = ifs.readsome(buf, sizeof buf);
          if (sz > 0) {
            MapEvent bytes;
            bytes.mutable_bytes()->assign(buf, sz);
            stream->Write(bytes, tag(CQ_BODY));
            PrintEvent(bytes);
          }
          if (sz <= 0 && ifs.rdbuf()->in_avail() <= 0) {
            MapEvent end;
            MakeEventEnd(&end, s2s_talk.event());
            stream->Write(end, tag(CQ_END));
            PrintEvent(end);
          }
          break;
        }
        case CQ_END: {
          logger->debug("ImageToSpeechTalk CQ_END");
          stream->WritesDone(tag(CQ_DONE));
          break;
        }
        case CQ_DONE: {
          logger->debug("ImageToSpeechTalk CQ_DONE");
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_READ: {
          logger->debug("ImageToSpeechTalk CQ_READ");
          PrintDirective(dir);
          if (dir.has_directive()) {
            if (dir.directive().operation_sync_id() ==
                s2s_talk.event().operation_sync_id()) {
              logger->debug("operation sync id is SAME:"
                                + s2s_talk.event().operation_sync_id());
            } else {
              logger->debug("operation sync id is not SAME (e)" +
                  s2s_talk.event().operation_sync_id() + "\n" +
                  "(d)" + dir.directive().operation_sync_id());
            }
          }
          dir.Clear();
          stream->Read(&dir, tag(CQ_READ));
          break;
        }
        case CQ_FINISH: {
          logger->debug("ImageToSpeechTalk CQ_FINISH, stream is finished");
          // stream->Read(&dir, tag(CQ_READ));
          cq.Shutdown();
          finished = true;
          break;
        }
        default: {
          break;
        }
      }
    } else if (r == CompletionQueue::SHUTDOWN) {
      logger->debug("ImageToSpeechTalk shutdown");
      finished = true;
    }
  } // while loop

  if (st.ok()) {
    logger->debug("status: OK!" + st.error_message());
  } else {
    std::cout << "status: " << st.error_message() << std::endl;
    logger->error("status:" + st.error_message());
  }
}

