#include <memory>
#include <vector>
#include <iostream>
#include <fstream>

#include <grpc++/grpc++.h>
#include <getopt.h>
#include <maum/m2u/facade/dialog.grpc.pb.h>
#include <libmaum/common/config.h>
#include <libgen.h>
#include <libmaum/common/util.h>
#include <gitversion/version.h>
#include <libgen.h>
#include <CoreTtsApi.h>
#include <boost/algorithm/string.hpp>

#include "map-client.h"
#include "tts-client.h"

void Usage(const char *prname) {
  std::cout << "Usage: ./m2u-map-cli [OPTION]" << std::endl
            << "<required>" << std::endl
            << "    --chatbot [chatbot-name] : must select a chatbot." << std::endl
            << "<optional>" << std::endl
            << " -f --file [file-name] : read message from text file."
            << std::endl
            << " -h --help  : show this message." << std::endl
            << std::endl;
  exit(0);
}
void help_display() {
  std::cout << "Help:  " << std::endl
            << "q: Stop Chatting" << std::endl
            << "d: Dialog" << std::endl
            << "a: Authentication" << std::endl
            << "s: Set Config" << std::endl
            << "p: Audio Play" << std::endl;

  std::cout << "{map command} [qdasp | user]: ";
}
bool makeTTTS(string &in_utter, string &out_tts_file) {
  string msg = in_utter;
  auto &c = libmaum::Config::Instance();
  string path = c.GetRuntimeHome();
  SimpleTtsClient *tts = TtsClientFactory::GetInstance()->Create();
  if (!tts->IsValid()) {
    delete tts;
    std::cout << "Invalid TTS Client status: cannot create tts client."
              << std::endl;
  }
  path += "/samples/";
  tts->SetPath(path);
  if (!tts->Open()) {
    delete tts;
    std::cout << "Invalid TTS Client status: cannot generate audio message."
              << std::endl;
  }
  if (!tts->GetSpeech(msg)) {
    delete tts;
    std::cout << "Invalid TTS Conversion status: invalid retun"
              << std::endl;
  }
  out_tts_file = tts->ConvertForSTT();
  return true;
}
int ProcessMap(MapClient &client) {

  std::string line;
  // bool is_kor_lang = true;
  std::string text = "hello world";
  std::string image_name = "test.jpg";
  help_display();
  std::string category;
  int number;
  std::string type;
  std::string select_text;
  std::string in_text;

  auto logger = LOGGER();

  while (getline(std::cin, line)) {
    trim(line);
    in_text = "";
    if ((!category.empty()) && type.empty()) {
      line = category;
    } else if ((!category.empty()) && (!type.empty())) {
      in_text = line;
    }

    if (line == "q" || line == "Q") {
      std::cout << "process quit" << std::endl;
      logger->debug("input q, process is quit");
      exit(0);
    } else if (line == "d" || line == "D") {
      logger->debug("category changed Dialog");
      category = "d";
      type = "";
      in_text = "";
      logger->debug("call open");
      client.DialogOpen();
      std::cout << std::endl << std::endl
                << "[select number]" << std::endl
                << ">>>>> 1. SpeechToSpeechTalk (sts)" << std::endl << std::endl
                << ">>>>> 2. SpeechToTextTalk (stt)" << std::endl << std::endl
                << ">>>>> 3. TextToTextTalk (ttt)" << std::endl << std::endl
                << ">>>>> 4. TextToSpeechTalk (tts)" << std::endl << std::endl
                << ">>>>> 5. ImageToTextTalk (itt)" << std::endl << std::endl
                << ">>>>> 6. ImageToSpeechTalk (its)" << std::endl << std::endl
                << ">>>>> 7. Close" << std::endl << std::endl;
      std::cout << "{map command} [qdasp | user]: ";

      getline(std::cin, select_text);
      number = atoi(select_text.c_str());
      if (number == 1) {
        type = "sts";
      } else if (number == 2) {
        type = "stt";
      } else if (number == 3) {
        type = "ttt";
      } else if (number == 4) {
        type = "tts";
      } else if (number == 5) {
        type = "itt";
      } else if (number == 6) {
        type = "its";
      } else if (number == 7) {
        type = "close";
      }
    } else if (line == "a" || line == "A") {
      logger->debug("category changed Authentication");
      category = "a";
      type = "";
      in_text = "";
      std::cout << std::endl << std::endl
                << "[select number]" << std::endl
                << ">>>>> 1. SignIn" << std::endl << std::endl
                << ">>>>> 2. SignOut" << std::endl << std::endl
                << ">>>>> 3. IsValid" << std::endl << std::endl
                << ">>>>> 4. GetUserInfo" << std::endl << std::endl;
      std::cout << "{map command} [qdasp | user]: ";

      getline(std::cin, select_text);
      number = atoi(select_text.c_str());
      if (number == 1) {
        type = "si";
      } else if (number == 2) {
        type = "so";
      } else if (number == 3) {
        type = "iv";
      } else if (number == 4) {
        type = "gui";
      }
    } else if (line == "s" || line == "S") {
      logger->debug("category changed Set Config");
      category = "s";
      type == "";
      in_text = "";
      std::cout << std::endl << std::endl
                << "[select number]" << std::endl
                << ">>>>> 1. set image file name" << std::endl << std::endl
                << ">>>>> 2. set sign" << std::endl << std::endl
                << ">>>>> 3. set token" << std::endl << std::endl;
      std::cout << "{map command} [qdasp | user]: ";

      getline(std::cin, select_text);
      number = atoi(select_text.c_str());
      if (number == 1) {
        type = "image";
      } else if (number == 2) {
        type = "sign";
      } else if (number == 3) {
        type = "token";
      }
    } else if (line == "p" || line == "P") {
      logger->debug("category changed Audio Play");
      category = "p";
      type = "";
      in_text = "";
      std::cout << std::endl << std::endl
                << "[select number]" << std::endl
                << ">>>>> 1. Play Finished" << std::endl << std::endl
                << ">>>>> 2. Play Paused" << std::endl << std::endl
                << ">>>>> 3. Play Resumed" << std::endl << std::endl
                << ">>>>> 4. Play Started" << std::endl << std::endl
                << ">>>>> 5. Play Stopped" << std::endl << std::endl
                << ">>>>> 6. Progress Report Delay Passed" << std::endl
                << std::endl
                << ">>>>> 7. Progress Report Interval Passed" << std::endl
                << std::endl
                << ">>>>> 8. Progress Report Position Passed" << std::endl
                << std::endl
                << ">>>>> 9. Stream Requested" << std::endl << std::endl;
      std::cout << "{map command} [qdasp | user]: ";

      getline(std::cin, select_text);
      number = atoi(select_text.c_str());
      if (number == 1) {
        type = "pf";
      } else if (number == 2) {
        type = "pp";
      } else if (number == 3) {
        type = "pr";
      } else if (number == 4) {
        type = "pstart";
      } else if (number == 5) {
        type = "pstop";
      } else if (number == 6) {
        type = "prdp";
      } else if (number == 7) {
        type = "prip";
      } else if (number == 8) {
        type = "prpp";
      } else if (number == 9) {
        type = "sr";
      }
    } else if (category.empty()) {
      help_display();
    }
    //if ()

    if (type.compare("sts") == 0) {
      string outfile;
      logger->debug("select sts, outfile: {}", outfile);
      if (makeTTTS(text, outfile) == true) {
        client.SpeechToSpeechText(outfile);
      }
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("stt") == 0) {
      string outfile;
      logger->debug("select stt, outfile: {}", outfile);
      if (makeTTTS(text, outfile) == true) {
        client.SpeechToText(outfile);
      }
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("ttt") == 0) {
      if (in_text.empty()) {
        std::cout << "[qdasp | input text] ";
        getline(std::cin, in_text);
      }
      logger->debug("select ttt, input text: {}", in_text);
      client.TextToTextTalk(in_text);
      std::cout << "[qdasp | input text] ";
    } else if (type.compare("tts") == 0) {
      if (in_text.empty()) {
        std::cout << "[qdasp | input text] ";
        getline(std::cin, in_text);
      }
      logger->debug("select tts, input text: {}", in_text);
      client.TextToSpeechTalk(in_text);
      std::cout << "[qdasp | input text] ";
    } else if (type.compare("itt") == 0) {
      logger->debug("select itt");
      client.ImageToTextTalk(image_name);
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("its") == 0) {
      logger->debug("select its");
      client.ImageToSpeechTalk(image_name);
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("close") == 0) {
      logger->debug("call close");
      client.DialogClose();
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("si") == 0) {
      logger->debug("select si");
      client.SignIn("123123");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("so") == 0) {
      logger->debug("select so");
      client.SignOut();
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("iv") == 0) {
      logger->debug("select iv");
      client.IsValid();
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("gui") == 0) {
      logger->debug("select gui");
      client.GetUserInfo();
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("image") == 0) {
      logger->debug("select set image");
      string in_text;
      std::cout << "[qdasp | input image file name]: ";
      getline(std::cin, in_text);
      image_name = in_text;
      category = "";
      type = "";
      std::cout << "saved image file name" << std::endl;
      help_display();
    } else if (type.compare("sign") == 0) {
      logger->debug("select set sign");
      string in_text;
      std::cout << "[qdasp | input sign]: ";
      getline(std::cin, in_text);
      client.sign_in_ = in_text;
      category = "";
      type = "";
      std::cout << "saved sign" << std::endl;
      help_display();
    } else if (type.compare("token") == 0) {
      logger->debug("select set token");
      string in_text;
      std::cout << "[qdasp | input token]: ";
      getline(std::cin, in_text);
      client.token_ = in_text;
      category = "";
      type = "";
      std::cout << "saved token" << std::endl;
      help_display();
    } else if (type.compare("pf") == 0) {
      logger->debug("select PlayFinished");
      client.AudioPlayer("PlayFinished", "finish");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("pp") == 0) {
      logger->debug("select PlayPaused");
      client.AudioPlayer("PlayPaused", "paused");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("pr") == 0) {
      logger->debug("select PlayResumed");
      client.AudioPlayer("PlayResumed", "resumed");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("pstart") == 0) {
      logger->debug("select PlayStarted");
      client.AudioPlayer("PlayStarted", "started");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("pstop") == 0) {
      logger->debug("select PlayStopped");
      client.AudioPlayer("PlayStopped", "stopped");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("prdp") == 0) {
      logger->debug("select ProgressReportDelayPassed");
      client.AudioPlayer("ProgressReportDelayPassed", "delayPassed");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("prip") == 0) {
      logger->debug("select ProgressReportIntervalPassed");
      client.AudioPlayer("ProgressReportIntervalPassed", "intervalPassed");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("prpp") == 0) {
      logger->debug("select ProgressReportPositionPassed");
      client.AudioPlayer("ProgressReportPositionPassed", "positionPassed");
      std::cout << "[qdasp | input token]: ";
    } else if (type.compare("sr") == 0) {
      logger->debug("select StreamRequested");
      client.AudioPlayer("StreamRequested", "streamRequested");
      std::cout << "[qdasp | input token]: ";
    }
  }

  std::cout <<
            std::endl;
  return 0;
}

int Dofile(MapClient &client, const string &file) {
  string outfile, in_text, lines;
  auto logger = LOGGER();
  outfile = file + ".result";
  std::ofstream ofs;
  client.DialogOpen();
  try {
    std::ifstream ifs(file);
    if (!ifs.is_open()) {
      logger->error("Cannot read [{}].", file);
    }
    ofs.open(outfile, std::ofstream::out | std::ofstream::trunc);
    ofs << "";
    ofs.close();

    while (getline(ifs, in_text)) {
      client.TextToTextTalk(in_text, outfile);
      lines += (in_text + "\n");
    }
    logger->debug("contents of '{}' : {} ", file, lines);
  } catch (std::exception &e) {
    logger->error("{} err, cause: {}", __func__, e.what());
  }
  client.DialogClose();
  logger->debug("TextToTextTalk complete!");
  return 0;
}

int DoEchoTest(MapClient &client) {
  std::cout << "Echo test start..." << std::endl;
  client.Echo();
  client.EchoStream();
  std::cout << "Echo test end..." << std::endl;
  return 0;
}

int process_option(int argc, char *argv[], string &server, string &chatbot, string &file) {
  char *base = basename(argv[0]);
  bool ischat = false;
  while (true) {
    int optindex = 0;
    static option l_options[] = {
        {"version", 0, 0, 'v'},
        {"help", 0, 0, 'h'},
        {"chatbot", 1, 0, 1},
        {"file", 1, 0, 'f'},
        {"server", 1, 0, 's'},
        {0, 0, 0, 0}
    };

    int ch = getopt_long(argc, argv, "vhs:f:", l_options, &optindex);

    if (ch == -1) {
      if (optindex >= argc) {
        Usage(base);
      } else if (!ischat) {
        printf("you must select chatbot!");
        Usage(base);
      } else {
        break;
      }
    }
    switch (ch) {
      case 'v': {
        printf("%s version %s\n", basename(argv[0]), version::VERSION_STRING);
        break;
      }
      case 1: {
        chatbot = optarg;
        ischat = true;
        break;
      }
      case 'f': {
        file = optarg;
        break;
      }
      case 's': {
        server = optarg;
        break;
      }
      case 'h':
      default: {
        return -1;
      }
    }
  }
  return 0;
}

int main(int argc, char **argv) {
  string filename;
  string chatbot;
  string server;
  string file;
  char *dir = dirname(argv[0]);
  chdir(dir);

  auto &c = libmaum::Config::Init(argc, argv, "m2u.conf");

  int res = 0;
  res = process_option(argc, argv, server, chatbot, file);
  if (res < 0) {
    Usage(basename(argv[0]));
    return -1;
  }
  auto logger = LOGGER();

  logger->debug(">>>> start m2u-map-cli >>>>");
  string server_info = c.Get("map.export");
  if (server.length()) {
    server_info = server;
  }

  // #@ For localTest
  // server_info = "0.0.0.0:9911";

  logger->debug("connect server = " + server_info);
  MapClient mapc(grpc::CreateChannel(
      server_info, grpc::InsecureChannelCredentials()), chatbot);

  std::cout << "Select Chatbot [" << chatbot << "]" << std::endl;

  if (!file.empty()) {
    Dofile(mapc, file);
    exit(0);
  }
  return ProcessMap(mapc);
}
