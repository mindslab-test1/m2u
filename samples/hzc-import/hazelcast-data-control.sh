#!/bin/bash

function usage() {
  echo "*prerequisite : 'm2u:m2u-hzc' must be in active."
  echo "hazelcast-data-control help : this messages"
  echo "hazelcast-data-control import FILE : import M2U setting from FILE."
  echo "hazelcast-data-control export [FILE] : export M2U setting to FILE or exported-setting.json"
  echo "hazelcast-data-control view : view current M2U setting"
  echo
}

conf_dir=${MAUM_ROOT}/conf
jar_dir=${MAUM_ROOT}/lib/m2u-service-admin-2.1.0-all.jar
method=ai.maum.m2u.admin.hzc.HazelcastSettingsCarrier

function import_data() {
  if [ -z "$1" ]; then
    echo "Please assign a setting file to import."
    echo
    usage
    exit 0
  elif [ ! -f "$1" ]; then
    echo "File [$1] is not exist."
    exit 0
  fi

  java -cp "${conf_dir}":"${jar_dir}" ${method} import "$1"
}

function export_data() {
  local target

  if [ -z "$1" ]; then
    target="exported-setting.json"
  else
    target="$1"
  fi

  java -cp "${conf_dir}":"${jar_dir}" ${method} export "${target}"
}

function view_data() {
  java -cp "${conf_dir}":"${jar_dir}" ${method} view
}

case "$1" in
import) import_data "$2" ;;
export) export_data "$2" ;;
view) view_data ;;
*) usage ;;
esac
