#include "audio-front.h"
#include <libmaum/common/config.h>

int AudioFrontCallback(const void *inputBuffer,
                       void *outputBuffer,
                       unsigned long frames,
                       const PaStreamCallbackTimeInfo *cbTimeInfo,
                       PaStreamCallbackFlags cbFlags,
                       void *ctx) {
  AudioFront *front = reinterpret_cast<AudioFront *>(ctx);
  // auto logger = LOGGER();

  // 분기를 시작하기 전에 먼저 네트워크로부터 읽어들인다.
  int read = 0;
  std::vector<char> buffer = front->ReadFromNetwork(read, frames);

  // 분기를 시작한다.
  switch (front->State()) {
    // 입력을 대기하는 상태, WAKEUP 시그널이 일정정도 되면, 깨어난다.
    case AudioFront::AudioFrontState::AFS_IDLE: {
      if (read < 0) {
        front->ResampleInput(reinterpret_cast<const int16_t *>(inputBuffer));
        // 입력이 있으면 볼륨을 계산한다.
        front->CalcVolume();
        front->PrintInput(frames);
        // 여기서 음성인식된 것이 "얘 초롱아"인가 확인하는 모드입니다.
        // volume detection

        // 소리가 나면
        if (front->IsUtteranceVolume()) {
          // 깨우는 신호를 지속하도록 한다.
          front->WakeupCountUp();
          // 다 깨워졌으면, (1초 미만 정도 지속하면)
          if (front->IsWakedUp())
            // 시그널을 내보내도록 상태를 변경한다.
            front->StateSignal();
        } else {
          // 다시 초기 상태로 돌아간다.
          // 이 입력 음성은 그냥 버려진다.
          front->ClearWakeup();
        }
        front->SetZeroToOutput(outputBuffer, frames);
      } else if (read > 0) {
        // 출력할 데이터가 있으면 계속 IDLE 상태이다.
        // 입력을 출력할 곳으로 보내준다.
        front->CopyBufferToOutput(buffer);
        // 입력 데이터를 RESAMPLE 해서 내보낸다.
        front->ResampleOutput(reinterpret_cast<int16_t *>(outputBuffer),
                              frames);
      } else {
        front->SetZeroToOutput(outputBuffer, frames);
      }
      break;
    }
      // 입력이 지속되는 상태. 입력이 종료되면, 입력 완료 처리.
    case AudioFront::AudioFrontState::AFS_INPUT_START: {
      // 입력 대기 모드는
      // 깨우는 신호로 깨어난 다음에 BEEP를 내보내고 바로 전환된다..
      front->ResampleInput(reinterpret_cast<const int16_t *>(inputBuffer));
      // 입력이 있으면 볼륨을 계산한다.
      front->CalcVolume();
      front->PrintInput(frames);

      // 입력을 처리하는 단계
      if (front->IsUtteranceVolume()) {
        // 입력이 계속 되면 묵음 시작 표시를 초기화한다.
        front->ClearSilenceCountDown();
      } else {
        // 묵음이면, 인식중단을 준비한다.
        front->SilenceCountDown();
      }
      // 묵음구간이 완료되었으면..
      if (front->IsSilenceCountFinished()) {
        // 입력이 완료된다.
        front->WritesDone();
        // 대기 상태로 바뀐다.
        front->StateIdle();
      } else {
        // 대화 메시지를 보낸다.
        front->WriteToNetwork();
      }
      break;
    }

      // 입력 신호를 내보낸다.
    case AudioFront::AudioFrontState::AFS_INPUT_SIGNAL: {
      // 바로 출력 버퍼에 기록한다.
      int16_t *out = reinterpret_cast<int16_t *>(outputBuffer);
      float sin = 0;
      for (size_t i = 0; i < frames; i++) {
        *out++ = (int16_t) (sinf(sin) * 10000);
        if (sin < 200)
          sin += 0.075; // 0.15;
        else
          sin += 0.15;  // 0.3;
      }
      std::cout << "TALK SIGNALED" << std::endl;
      front->StateBeep();
      front->ClearSilenceCountDown();
      break;
    }
      // 입력 신호 이후로 입력은 모두 다 버려진다, 출력도 마찬가지.
    case AudioFront::AudioFrontState::AFS_BEEP:
    case AudioFront::AudioFrontState::AFS_BEEP_3:
    case AudioFront::AudioFrontState::AFS_BEEP_4:
    case AudioFront::AudioFrontState::AFS_BEEP_5:
    case AudioFront::AudioFrontState::AFS_BEEP_6: {
      front->StateBeepNext();
      front->SetZeroToOutput(outputBuffer, frames);
      break;
    }
  }

  return paContinue;
}
