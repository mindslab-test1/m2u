#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "portaudio.h"
#include <grpc++/grpc++.h>
#include <libmaum/common/config.h>
#include "adc.h"
#include "audio-front.h"

int pa_init(int argc, char **argv) {
  auto logger = LOGGER();
  int inputDeviceNum;
  int outputDeviceNum;
  unsigned long framesPerBuffer = 8192;
  double srate = 44100;
  // unsigned long framesPerBuffer = 4096;
  // double srate = 16000;
  adc_t adc;
  AudioFront  * audioFront;
  string remote = argv[1];

  PaError err;

  // init
  err = Pa_Initialize();
  if (err != paNoError) goto error;

  // enum devices
  int numDevices;
  numDevices = Pa_GetDeviceCount();
  if (numDevices < 0) {
    printf("ERROR: Pa_CountDevices returned 0x%x\n", numDevices);
    logger->error("Pa_CountDevices returned {}", numDevices);
    err = numDevices;
    goto error;
  }
  const PaDeviceInfo *deviceInfo;
  for (int i = 0; i < numDevices; i++) {
    deviceInfo = Pa_GetDeviceInfo(i);
    logger->info("[DEVICE INFO]{} {}", i, deviceInfo->name);
    logger->info("  maxInputChannels : {}", deviceInfo->maxInputChannels);
    logger->info("  maxOutputChannels : {}", deviceInfo->maxOutputChannels);
    logger->info("  defaultSampleRate : {}\n", deviceInfo->defaultSampleRate);
  }
  logger->info("[SELECTED INPUT] {} [OUTPUT] {}", argv[2], argv[3]);

  inputDeviceNum = atoi(argv[2]);
  outputDeviceNum = atoi(argv[3]);

  if (inputDeviceNum >= numDevices || outputDeviceNum >= numDevices) {
    printf("ERROR: invalid device number, it should less than %d\n",
           numDevices);
    goto error;
  }

  // open stream
  PaStream *stream;
  // could be paFramesPerBufferUnspecified, in which case PortAudio will do its
  // best to manage it for you, but, on some platforms, the framesPerBuffer will
  // change in each call to the callback
  PaStreamParameters outputParameters;
  PaStreamParameters inputParameters;


  inputParameters.channelCount = 1;
  inputParameters.device = inputDeviceNum;
  inputParameters.hostApiSpecificStreamInfo = NULL;
  inputParameters.sampleFormat = paInt16;
  inputParameters.suggestedLatency =
      Pa_GetDeviceInfo(inputDeviceNum)->defaultLowInputLatency;
  inputParameters.hostApiSpecificStreamInfo = NULL;
  //See you specific host's API docs for info on using this field
#ifdef WIN32
  memset(&outputParameters, 0, sizeof(outputParameters));
#else
  bzero(&outputParameters, sizeof(outputParameters));
  //not necessary if you are filling in all the fields
#endif
  outputDeviceNum = atoi(argv[3]);
  outputParameters.channelCount = 1;
  outputParameters.device = outputDeviceNum;
  outputParameters.hostApiSpecificStreamInfo = NULL;
  outputParameters.sampleFormat = paInt16;
  outputParameters.suggestedLatency =
      Pa_GetDeviceInfo(outputDeviceNum)->defaultLowOutputLatency;
  outputParameters.hostApiSpecificStreamInfo = NULL;
  //See you specific host's API docs for info on using this field

  adc = AdcOpen(remote.c_str());
  audioFront = new AudioFront(adc);

  AdcStart(adc);

  AdcSetAudioQuality(adc, 16000);

  err = Pa_OpenStream(
      &stream,
      &inputParameters,
      //NULL,
      &outputParameters,
      //NULL,
      srate,
      framesPerBuffer,
      paNoFlag, //flags that can be used to define dither, clip settings and more
      AudioFrontCallback, //your callback function
      audioFront
  ); //data to be passed to callback. In C++, it is frequently (void *)this
  //don't forget to check errors!
  logger->info("input PortAudio error: {}", Pa_GetErrorText(err));
  if (err != paNoError) {
    goto error;
  }

  err = Pa_StartStream(stream);
  logger->info("output PortAudio error: {}", Pa_GetErrorText(err));
  if (err != paNoError) {
    goto error;
  }

  for (int i = 0; i < 10000; i++) {
    Pa_Sleep(1000);
    //printf("%d\n", i);
  }
  Pa_CloseStream(stream);
  return 0;

  error:
  err = Pa_Terminate();
  if (err != paNoError) {
    logger->error("PortAudio error: {}", Pa_GetErrorText(err));
    printf("PortAudio error: %s\n", Pa_GetErrorText(err));
  }

  return -1;
}
