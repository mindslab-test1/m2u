#include <stdio.h>

#ifdef WIN32
#include <windows.h>
#endif
#include <fcntl.h>

#ifdef PSPX
#include "pocketsphinx.h"
#include "sphinxbase/cmd_ln.h"
#endif

#include <libmaum/common/config.h>
#include "util.h"

short getMax(short *buf, int size) {
  short max = 0;
  for (int i = 0; i < size; i++) {
    if (max < *(buf + i)) max = *(buf + i);
  }
  return max;
}

extern int sockout;
extern int sockin;

int main(int argc, char *argv[]) {
  libmaum::Config::Init(argc, argv, "m2u.conf");


#ifdef PSPX
  ps_decoder_t *ps;
  cmd_ln_t *config;
  FILE **fh = NULL;
  char const *hyp, *uttid;
  int16_t buf[1024];
  int rv;
  int32_t score;
#endif

  if (argc < 4) {
    fprintf(stderr,
            "%s remote [input_dev_num output_dev_num]\n",
            argv[0]);
    return -1;
  }

#ifdef SOCKET
  //sockout=init_socket("125.132.250.204", 1998);
  sockout = init_socket(argv[1], atoi(argv[2]));
  //init_socket("google.com", 80);
  if (sockout == -1) {
    fprintf(stderr, "connection error\n");
    clean_socket();
    return -1;
  }
  //sockin = init_socket("125.132.250.204", 1999);
  sockin = init_socket(argv[3], atoi(argv[4]));
  //init_socket("google.com", 80);
  if (sockin == -1) {
    fprintf(stderr, "connection error\n");
    clean_socket();
    return -1;
  }
#ifdef WIN32
  u_long flags = 1;
  ioctlsocket(sockin, FIONBIO, &flags);
#else
  fcntl(sockin, O_NONBLOCK);
#endif
  // MAUM_ROOT 이라는 환경변수에 의해서 동작한다.
  // 이 부분은 설치되어야 하는 부분으로서 현재는 run 디렉토리에 임시로 배치하였다.

  const char * lsa_home = getenv("LSA_RUNTIME");
  char hmm_directory[BUFSIZ];
  char lm_file[BUFSIZ];
  char dic_file[BUFSIZ];

  snprintf(hmm_directory, sizeof(hmm_directory), "%s/%s",
           lsa_home,  "cmusphinx-en-us-5.2");
  snprintf(lm_file, sizeof(lm_file), "%s/%s",
           lsa_home,  "jack.lm");
  snprintf(dic_file, sizeof(dic_file), "%s/%s",
           lsa_home,  "jack.dic");

  config = cmd_ln_init(NULL, ps_args(), TRUE,
                       "-hmm", hmm_directory,
                       "-lm", lm_file,
                       "-dict", dic_file,
                       "-logfn", "nul",
                       //"-agc", "max",
                       //"-samprate", "8000",
                       NULL);
  if (config == NULL) {
    fprintf(stderr, "Failed to create config object, see log for details\n");
    return -1;
  }
#endif
#ifdef PSPX
  ps = ps_init(config);
  if (ps == NULL) {
    fprintf(stderr, "Failed to create recognizer, see log for details\n");
    return -1;
  }
  if (pa_init(ps, argc, argv) == -1){
    fprintf(stderr, "Failed to init portaudio\n");
    return -1;

  }
  printf("paInit completed\n");

  ps_free(ps);
  cmd_ln_free_r(config);
#else

  if (pa_init(argc, argv) == -1) {
    fprintf(stderr, "Failed to init portaudio\n");
    return -1;
  }
  printf("paInit completed\n");

#endif

  return 0;
}
