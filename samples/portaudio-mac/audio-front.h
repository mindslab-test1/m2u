#ifndef AUDIO__FRONT_H
#define AUDIO__FRONT_H

#include "portaudio.h"
#include "samplerate.h"
#include <iostream>
#include <vector>
#include "util.h"

#include "adc.h"

constexpr float kDefaultSampleRate = 44100.0;
//constexpr float kInputSampleRate = 8000.0;
constexpr float kInputSampleRate = 16000.0;
constexpr float kOutputSampleRate = 16000.0;
constexpr size_t kDefaultFrameLength = 8192;

class AudioFront {
 public:
  explicit AudioFront(adc_t adc)
      :client_(adc) {
  }
  // STATE
  enum AudioFrontState {
    AFS_IDLE = 0,
    AFS_INPUT_SIGNAL,
    AFS_BEEP,
    AFS_BEEP_3,
    AFS_BEEP_4,
    AFS_BEEP_5,
    AFS_BEEP_6,
    AFS_INPUT_START
  };
  // STATE
  inline int State() {
    return state_;
  }
  inline void StateSignal() {
    state_ = AFS_INPUT_SIGNAL;
  }
  inline void StateBeep() {
    state_ = AFS_BEEP;
  }
  inline void StateBeepNext() {
    state_++;
  }
  inline void StateInput() {
    state_ = AFS_INPUT_START;
  }
  inline void StateIdle() {
    state_ = AFS_IDLE;
  }

  // SILENCE
  /// 음성입력이 없는 상태가 지속되는 것을 모니터링
  inline void SilenceCountDown() {
    act_--;
  }
  inline void WakeupCountUp() {
    act_++;
  }
  inline void ClearWakeup() {
    act_ = 0;
  }

  inline bool IsWakedUp() {
    return act_ > 2;
  }
  inline bool IsSilenceCountFinished() {
    return act_ == 0;
  }
  inline void ClearSilenceCountDown() {
    act_ = kSilenceCountStart;
  }

  // RESAMPL
  int ResampleInput(const int16_t *input) {
    // preprocess buffers
    src_short_to_float_array(input, convInBufferFloat_, 8192);
    contextIn_.data_in = convInBufferFloat_;
    contextIn_.input_frames = 8192;
    contextIn_.data_out = convInBufferFloatDownsampled_;
    //contextIn_.output_frames = 2048;
    contextIn_.output_frames = 4096;
    //contextIn_.src_ratio = 8000.0 / 44100.0;
    contextIn_.src_ratio = 16000.0 / 44100.0;
    int ret = src_simple(&contextIn_, SRC_SINC_FASTEST, 1);
    if (ret != 0) {
      std::cout << "[ERROR]" << src_strerror(ret) << std::endl;
    }
    int newSampleLength = (int) contextIn_.output_frames_gen;
    src_float_to_short_array(contextIn_.data_out,
                             convInBufferShort_,
                             newSampleLength);
    inputSampleLength_ = newSampleLength;
    return newSampleLength;
  }

  void ResampleOutput(int16_t *out, size_t frames) {
    int sampleLength = GetOututSampleLength(frames);
    src_short_to_float_array(convOutBufferShort_,
                             convOutBufferFloatDownsampled_,
                             sampleLength);
    contextOut_.data_in = convOutBufferFloatDownsampled_;
    contextOut_.input_frames = sampleLength;
    contextOut_.data_out = convOutBufferFloat_;
    contextOut_.output_frames = 8192;
    contextOut_.src_ratio = 44100.0 / 16000.0;
    int ret = src_simple(&contextOut_, SRC_SINC_FASTEST, 1);
    if (ret != 0) {
      std::cout << "[ERROR]" << src_strerror(ret) << std::endl;
    }
    src_float_to_short_array(convOutBufferFloat_,
                             out,
                             (int) contextOut_.output_frames_gen);
  }


  size_t GetInputSampleLength(size_t frames) {
    // 1486
    return size_t (frames * (kInputSampleRate / kDefaultSampleRate));
  }

  size_t GetOututSampleLength(size_t frames) {
    return size_t (frames * (kOutputSampleRate / kDefaultSampleRate));
    // 2972
  }

  void CopyBufferToOutput(const std::vector<char>& buf) {
    int16_t * out = convOutBufferShort_;
    for ( const auto& ch : buf) {
      *out++ = Snack_Mulaw2Lin(ch);
    }
  }

  inline void SetZeroToOutput(void *buffer, size_t frames) {
    memset(buffer, 0, frames * sizeof(int16_t));
  }
  //// READ , WRITE TO NETWORK..
  std::vector<char> ReadFromNetwork(int& read,
                                    size_t frames = kDefaultFrameLength) {
    size_t  want = GetOututSampleLength(frames);
    std::vector<char> buffer(want, 0);
    read = AdcRead(client_, buffer.data(), want);
    if (read > 0) {
      buffer.resize(read);
    }
    return buffer;
  }
  void WriteToNetwork() {
    AdcSend(client_, convInBufferShort_, inputSampleLength_);
  }
  void WritesDone() {
    AdcSendDone(client_);
  };

  // VOLUME CALC
  inline void PrintInput(unsigned long frames) {
    std::cout
        << "#frame: " << frames
        << ", average: " << average_
        << ", sample length: " << inputSampleLength_
        << std::endl;
  }

  int CalcVolume() {
    int16_t  * conv_input = convInBufferShort_;
    size_t count = inputSampleLength_;
    int16_t max = 0;
    int32_t sum = 0;
    int16_t avg = 0;
    for (int i = 0; i < count; i++) {
      if (max < *conv_input) max = *conv_input;
      if (*conv_input > 0) sum += *conv_input;
      conv_input++;
    }
    avg = int16_t(sum / count);
    average_ = avg;
    return avg;
  }

  inline bool IsUtteranceVolume() {
    return average_ > kUtterenceAverage;
  }
  inline bool IsSilenceVolume() {
    return average_ <= kUtterenceAverage;
  }
 private:
  //
  // GLOBAL STATE
  //  It is shared over each callback
  //
  int state_ = AFS_IDLE;
  const int kSilenceCountStart = 5;
  int act_;


  // CALLBCACK EACH STATE
  // it is used in callback.
  // All this variable is used internally.
  // That is static.
  const int kUtterenceAverage = 2000; //< if lower than this, it maybe silence.
  int average_;

  // RESAMPLE
  size_t inputSampleLength_;
 public:
  short convInBufferShort_[4096];
  short convOutBufferShort_[4096];
 private:
  float convInBufferFloat_[8192];
  float convInBufferFloatDownsampled_[4096];
  float convOutBufferFloat_[8192];
  float convOutBufferFloatDownsampled_[4096];
  SRC_DATA contextIn_;
  SRC_DATA contextOut_;

  /// DATA RACING WITH AsyncDialogClient
  adc_t client_;
};


int AudioFrontCallback(const void *inputBuffer,
                       void *outputBuffer,
                       unsigned long frames,
                       const PaStreamCallbackTimeInfo *cbTimeInfo,
                       PaStreamCallbackFlags cbFlags,
                       void *ctx);

#endif
