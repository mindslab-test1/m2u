#ifdef _WIN32
#include <windows.h>
#endif
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "frontend_api.h"
#include "laser.h"

#define N_BUF_SIZE 160
#define READSIZE                        160
#define MFCCSIZE                        53
#define MAXFEATLEN  MFCCSIZE*5000 //1000frames in 10sec

#define FLAG_FEATINPUT             0x0001
#define FLAG_ENABLEUV              0x0002
#define FLAG_ENDPOINTDETECTION     0x0004
#define FLAG_KWSMODE               0x0008
#define FLAG_ENABLE_MULTIDETECTION 0x0010

typedef void ARecUV;

double g_time_elapsed = 0;
double g_time_input = 0;

extern "C" void setErrorHandleProc(void (*a_proc)(int code, const char *));

static void errorHandler(int code, const char *str) {
  fprintf(stderr, "%d %s\n", code, str);
}

LASER *kws_init() {
  LASER *pLASER = NULL;
  LFrontEnd *frontP;

  char *logdir = NULL;
  char *logfname = NULL;
  char *cfg_frontend = NULL;
  char *pTrace = NULL;

  setErrorHandleProc(&errorHandler);

  pLASER = CreateLASERExt("./LASERDB",
                          LASER_OPTION_CREATEUV | LASER_OPTION_CREATEKWS);
  frontP = GetFrontEnd(pLASER);

  SetOptionLASER(pLASER, "LASER_MAXLEN_BUFFER", "2000");
  //SetOptionLASER(pLASER, "LASER_OPTION_UTFRESULT", "0");
  SetOptionLASER(pLASER, "LASER_OPTION_KWSMODE", "1");
  SetOptionLASER(pLASER, "LASER_OPTION_DOPOSTUV", "1");
  setOptionLFrontEnd(frontP, "FRONTEND_OPTION_DOEPD", "0");
  SetOptionLASER(pLASER, "LASER_KWSNREC_LMWEIGHT", "1.0");
  SetOptionLASER(pLASER, "LASER_NBEST", "5");
  if (pTrace)
    SetOptionLASER(pLASER, "LASER_TRACE", pTrace);
  if (logfname && SetOptionLASER(pLASER, "LASER_LOGFILENAME", logfname)) {
    fprintf(stderr, "ERROR: fail to SetOptionLASER %s\n", logfname);
    return NULL;
  }
  if (logdir && SetOptionLASER(pLASER, "LASER_PCMLOGDIR", logdir)) {
    fprintf(stderr, "ERROR: fail to SetOptionLASER %s\n", logdir);
    return NULL;
  }
  if (pLASER == NULL) {
    fprintf(stderr, "ERROR: fail to CreateLASER\n");
    return NULL;
  }

  if (readOptionLFrontEnd(frontP, cfg_frontend) == 0) {
    fprintf(stderr, "Read config %s successfully.\n", cfg_frontend);
  }

  if (PrepareLASER(pLASER, "kws", 5)) {
    fprintf(stderr, "ERROR: while PrepareLASER: 'kws'\n");
    exit(EXIT_FAILURE);
  }

  SetRecModeLASER(pLASER, 5);

  return pLASER;
}

void kws_cleanup(LASER *pLASER) {
  ResetLASER(pLASER);
  UnprepareLASER(pLASER);
  FreeLASER(pLASER);
}
