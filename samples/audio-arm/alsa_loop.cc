/* 
   A Minimal Capture Program
   This program opens an audio interface for capture, configures it for
   stereo, 16 bit, 44.1kHz, interleaved conventional read/write
   access. Then its reads a chunk of random data from it, and exits. It
   isn't meant to be a real program.
   From on Paul David's tutorial : http://equalarea.com/paul/alsa-audio.html
   Fixes rate and buffer problems
   sudo apt-get install libasound2-dev
   gcc -o alsa-record-example -lasound alsa-record-example.c && ./alsa-record-example hw:0
*/

#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <pthread.h>
#include "alsa_loop.h"

//#define PCM_DEVICE "hw:1,0"
int16_t *buffer_in, *buffer_out;
snd_pcm_t *capture_handle, *play_handle;
void *output(void *frames);

int alsa_init(char *PCM_DEVICE,
              unsigned int rate,
              int frames,
              int ch_in,
              int ch_out,
              AlsaCallback callback) {
  /*
    unsigned int rate=44100;
    int frames=2048;
    int ch_in=1;
    int ch_out=2;
  */
  int err;
  //  int16_t *buffer_in, *buffer_out;
  //  snd_pcm_t *capture_handle, *play_handle;

  snd_pcm_hw_params_t *hw_params;
  snd_pcm_format_t format = SND_PCM_FORMAT_S16_LE;
  snd_pcm_uframes_t bfsize = 16000;

  // OPEN AND CONFIG CAPTURE DEVICE
  if ((err =
           snd_pcm_open(&capture_handle, PCM_DEVICE, SND_PCM_STREAM_CAPTURE, 0))
      < 0) {
    fprintf(stderr, "cannot open audio device %s (%s)\n",
            PCM_DEVICE,
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "capture device opened\n");

  if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
    fprintf(stderr, "cannot allocate hardware parameter structure (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params allocated\n");

  if ((err = snd_pcm_hw_params_any(capture_handle, hw_params)) < 0) {
    fprintf(stderr, "cannot initialize hardware parameter structure (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "capture hw_params initialized\n");

  // INTERLEAVE
  if ((err = snd_pcm_hw_params_set_access(capture_handle,
                                          hw_params,
                                          SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    fprintf(stderr, "cannot set access type (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params access setted\n");

  // FORMAT
  if ((err = snd_pcm_hw_params_set_format(capture_handle,
                                          hw_params,
                                          SND_PCM_FORMAT_S16_LE)) < 0) {
    fprintf(stderr, "cannot set sample format (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params format setted\n");

  // RATE
  if ((err =
           snd_pcm_hw_params_set_rate(capture_handle, hw_params, rate, 0))
      < 0) {
    fprintf(stderr, "cannot set sample rate (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params rate setted : %d\n", rate);

  // CHANNEL
  if ((err = snd_pcm_hw_params_set_channels(capture_handle, hw_params, ch_in))
      < 0) {
    fprintf(stderr, "cannot set channel count (%s)\n",
            snd_strerror(err));
    exit(1);
  }

  // BUFFER SIZE
  if ((err =
           snd_pcm_hw_params_set_buffer_size(capture_handle, hw_params, bfsize))
      < 0) {
    fprintf(stderr, "cannot set parameters (%s)\n",
            snd_strerror(err));
    exit(1);
  }

  fprintf(stdout, "hw_params channels setted\n");

  if ((err = snd_pcm_hw_params(capture_handle, hw_params)) < 0) {
    fprintf(stderr, "cannot set parameters (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params set\n");

  snd_pcm_hw_params_get_buffer_size(hw_params, &bfsize);
  printf("<<in buffer size : %ld\n", bfsize);





  // OPEN AND CONFIG PLAYBACK DEVICE
  if ((err = snd_pcm_open(&play_handle, PCM_DEVICE, SND_PCM_STREAM_PLAYBACK, 0)
      < 0)) {
    fprintf(stderr, "cannot open audio device %s (%s)\n",
            PCM_DEVICE,
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "playback device opened\n");

  if ((err = snd_pcm_hw_params_any(play_handle, hw_params)) < 0) {
    fprintf(stderr, "cannot initialize hardware parameter structure (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "capture hw_params initialized\n");

  // SET ACCESS
  if ((err = snd_pcm_hw_params_set_access(play_handle,
                                          hw_params,
                                          SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    fprintf(stderr, "cannot set access type (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params access setted\n");

  // SOUND FORMAT
  if ((err = snd_pcm_hw_params_set_format(play_handle,
                                          hw_params,
                                          SND_PCM_FORMAT_S16_LE)) < 0) {
    fprintf(stderr, "cannot set sample format (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params format setted\n");

  // RATE
  if ((err = snd_pcm_hw_params_set_rate_near(play_handle, hw_params, &rate, 0))
      < 0) {
    fprintf(stderr, "cannot set sample rate (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params rate setted to %d\n", rate);

  // CHANNELS
  if ((err = snd_pcm_hw_params_set_channels(play_handle, hw_params, ch_out))
      < 0) {
    fprintf(stderr, "cannot set channel count (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params channels setted\n");

  // BUFFER SIZE
  if ((err = snd_pcm_hw_params_set_buffer_size(play_handle, hw_params, bfsize))
      < 0) {
    fprintf(stderr, "cannot set parameters (%s)\n",
            snd_strerror(err));
    exit(1);
  }

  if ((err = snd_pcm_hw_params(play_handle, hw_params)) < 0) {
    fprintf(stderr, "cannot set parameters (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "hw_params set\n");

  // TEST
  snd_pcm_hw_params_get_buffer_size(hw_params, &bfsize);
  printf(">>out buffer size : %ld\n", bfsize);

  // FREE HW PARAMS
  snd_pcm_hw_params_free(hw_params);
  fprintf(stdout, "hw_params freed\n");


  // READY buffer_in
  buffer_in =
      (short *) malloc(ch_in * frames * snd_pcm_format_width(format) / 8);
  buffer_out =
      (short *) malloc(ch_out * frames * snd_pcm_format_width(format) / 8);
  printf("in buffer size:%d, out buffer size:%d\n",
         ch_in * frames * snd_pcm_format_width(format) / 8,
         ch_out * frames * snd_pcm_format_width(format) / 8
  );

  fprintf(stdout, "buffer_in allocated\n");

  // pre-fill-up
  //for (int i = 0; i < 1; i++) snd_pcm_writei(play_handle, buffer_out, frames);

  // PREPARE
  if ((err = snd_pcm_prepare(capture_handle)) < 0) {
    fprintf(stderr, "cannot prepare audio interface for use (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "audio interface prepared\n");
  if ((err = snd_pcm_prepare(play_handle)) < 0) {
    fprintf(stderr, "cannot prepare audio interface for use (%s)\n",
            snd_strerror(err));
    exit(1);
  }
  fprintf(stdout, "audio interface prepared\n");

  unsigned long frames_ = 0;
  pthread_t output_thread;
  int *p = &frames;

  /*	
	if(pthread_create(&output_thread, NULL, output, (void*)p)) {
	fprintf(stderr, "Error creating thread\n");
	return 1;
	}
  */
  while (1) {
    frames_++;
    if (frames_ % 100 == 0) printf("frames : %lu\n", frames_);

    if ((err = snd_pcm_readi(capture_handle, buffer_in, ch_in * frames))
        != frames) {
      if (err == -EPIPE) {
        fprintf(stderr, "read from audio interface failed %d (%s)\n",
                err, snd_strerror(err));
        break;
      } else if (err == -EBADFD) {
        fprintf(stderr, "read from audio interface failed %d (%s)\n",
                err, snd_strerror(err));
        break;
      } else if (err == -ESTRPIPE) {
        fprintf(stderr, "read from audio interface failed %d (%s)\n",
                err, snd_strerror(err));
        break;
      } else {
        printf("ibu.");
        fflush(stdout);
        snd_pcm_prepare(capture_handle);

      }
    }

    if (callback != NULL) callback(buffer_in, buffer_out, ch_in * frames);

    if ((err = snd_pcm_writei(play_handle, buffer_out, frames) == -EPIPE)) {
      printf("obu.");
      fflush(stdout);
      snd_pcm_prepare(play_handle);
    } else if (err < 0) {
      printf("ERROR. Can't write to PCM device. %s\n", snd_strerror(err));
    }

  }

  printf("Freeing buffer_in ... ");
  fflush(stdout);
  //free((void *)buffer_in);
  printf(" Done\n");
  fflush(stdout);

  fprintf(stdout, "buffer_in freed\n");

  snd_pcm_close(capture_handle);
  fprintf(stdout, "audio interface closed\n");

  snd_pcm_close(play_handle);
  fprintf(stdout, "audio interface closed\n");

  exit(0);
}

void *output(void *frames) {
  int err;
  while (1) {
    if ((err = snd_pcm_writei(play_handle, buffer_out, *((int *) frames))
        == -EPIPE)) {
      printf("obu.");
      fflush(stdout);
      snd_pcm_prepare(play_handle);
    } else if (err < 0) {
      printf("ERROR. Can't write to PCM device. %s\n", snd_strerror(err));
    }
  }
}

