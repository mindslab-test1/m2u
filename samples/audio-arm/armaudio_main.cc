#include <stdio.h>
#include <memory.h>
#include <math.h>
#include "alsa_loop.h"
#include "adc.h"
#include "laser.h"
#include "encoding.h"
// KWS
#include "frontend_api.h"
#include "laser.h"
//#include "config.h"

char *param_peer;
int param_stt_rate;
int param_avg_cutoff;
int param_timeout;

// LASER POINTER :)
LASER *pLASER;
LASERRESULT results[10];
unsigned int nEvent;
unsigned int NBest;
static void errorHandler(int code, const char *str) {
  fprintf(stderr, "%d %s\n", code, str);

}
extern LASER *kws_init();

// mulaw converter
extern "C" short Snack_Mulaw2Lin(unsigned char);

// function in module a
FILE *fh = NULL;

// sin wave
float PI = 3.14159265;
short sine_wave[2000];
void makeSineWave() {
  for (int i = 0; i < 2000; i++) {
    sine_wave[i] = (short) (sinf(2 * PI / 100 * i) * 5000);
  }
}

// util
void toStereo(short *in, short *out, int len) {
  for (int i = 0; i < len; i++) {
    *(out + i * 2) = *(out + i * 2 + 1) = *(in + i);
  }
}

// load q sound
short *ef_q[2];
int ef_q_size[2] = {0, 0};
char *ef_files[2] = {"q.pcm", "eng.pcm"};
void loadEffect(int i) {

  FILE *f = fopen(ef_files[i], "rb");

  fseek(f, 0L, SEEK_END);
  ef_q_size[i] = ftell(f) / 2;
  rewind(f);

  ef_q[i] = (short *) malloc(ef_q_size[i] * 2);
  fread(ef_q[i], 2, ef_q_size[i], f);
  fclose(f);
}

// audio file pointer
char af_name[256];
FILE *afh = NULL;
long af_size;
long af_index = 0;

// state
typedef enum {
  SLEEPING,
  AWAKEN,
  LISTENING_QUESTION,
  WAITING_ANSWER,
  SPEAKING,
  WARNING,
  SINGING
} State;
char *StateMsg[] = {
    "SLEEPING",
    "AWAKEN",
    "LISTENING_QUESTION",
    "WAITING_ANSWER",
    "SPEAKING",
    "WARNING",
    "SINGING"
};
State state_ = SLEEPING;

// state meta
State meta_ = state_;
// general frame counter for time delaying, checking time out etc.
int generalFrameCounter = 0;
// adc object
adc_t adc_;
// internal buffer;
char buf[160];

// callback
int callback(short *in, short *out, int len_in) {

  // effect q position
  static int q_pos = ef_q_size[0];
  // effect number
  static int effect_no = 0;


  // state monitor
  //printf("%d %d\n", state_,meta_);
  if (meta_ != state_) {
    printf(">>> State changed from %s to %s\n",
           StateMsg[meta_],
           StateMsg[state_]);
    fflush(stdout);
    meta_ = state_;
    if (state_ == LISTENING_QUESTION || state_ == WAITING_ANSWER
        || state_ == SPEAKING)
      system(
          "sudo bash -c 'echo out > /sys/class/gpio/gpio30/direction; echo 1 > /sys/class/gpio/gpio30/value'");
    else
      system(
          "sudo bash -c 'echo out > /sys/class/gpio/gpio30/direction; echo 0 > /sys/class/gpio/gpio30/value'");
    if (state_ == SLEEPING) ResetLASER(pLASER);

  }

  // recognize
  if (state_ == SLEEPING) {
    int ret = Recognize(pLASER,
                        in,
                        len_in,
                        &nEvent,
                        &NBest,
                        results);
    if (ret == LASER_OK) {
      if (nEvent & LASER_EVENT_RESULT) {
        std::string s = results[0].szWord;
        const char *keyword = EuckrToUtf8(s).c_str();
        printf("OK. got a result. %s (%f)\n",
               keyword,
               results[0].fScore);

        q_pos = 0;

        if (strcmp(keyword, "얘초롱아 ") == 0) {
          state_ = AWAKEN;
          effect_no = 0;
        } else {
          state_ = WARNING;
          effect_no = 1;
        }
        ResetLASER(pLASER);
      } else if (nEvent != 0) {
        printf("nEvent - %d\n", nEvent);
        ResetLASER(pLASER);
      } else {
      }
    } else {
      printf("LASER not ok - %d\n", ret);
      ResetLASER(pLASER);
    }
  } else if (state_ == AWAKEN) {
    generalFrameCounter++;
    //printf("%d "+generalFrameCounter); fflush(stdout);
    if (generalFrameCounter == 100) {
      generalFrameCounter = 0;
      state_ = LISTENING_QUESTION;
    }
  } else if (state_ == LISTENING_QUESTION) {
    if (param_stt_rate == 8000) {
      short buf[len_in / 2];
      for (int i = 0; i < len_in; i++) {
        buf[i / 2] = in[i];
      }
      AdcSend(adc_, buf, len_in / 2);

    } else AdcSend(adc_, in, len_in);

    unsigned long avg = 0;
    for (int i = 0; i < len_in; i++) {
      if (in[i] > 0)
        avg += in[i];
    }
    avg /= len_in;

    generalFrameCounter++;
    if (avg > (unsigned long) param_avg_cutoff) generalFrameCounter = 0;
    //printf("%ld %d\n", avg, generalFrameCounter);
    if (generalFrameCounter == 200) {
      printf(">>> SENDING EOM ...");
      fflush(stdout);
      AdcSendDone(adc_);
      printf(" DONE\n");
      fflush(stdout);

      generalFrameCounter = 0;
      state_ = WAITING_ANSWER;
    }
  } else if (state_ == WAITING_ANSWER) {
    generalFrameCounter++;
    //printf(">>> WAITING_ANSWER ...");fflush(stdout);
    int ret = AdcRead(adc_, buf, 0);
    if (ret == 0) {
      generalFrameCounter = 0;
      state_ = SPEAKING;
    }
    // check timeout
    if (generalFrameCounter > param_timeout) {
      generalFrameCounter = 0;
      state_ = SLEEPING;
    }
  } else if (state_ == SPEAKING) {
    //printf(">>> WAITING_ANSWER ...");fflush(stdout);
    int ret = AdcRead(adc_, buf, 160);
    //printf(" RECEIVED %d\n",ret);fflush(stdout);

    if (ret == -1) {
      const char *filename = AdcGetMeta(adc_, "music-play.url");
      printf(">>> MUSIC URL : %s\n", filename);
      if (filename != NULL) {
        printf("Let's play!!\n");
        state_ = SINGING;
        strcpy(af_name, filename);
      } else state_ = SLEEPING;
    } else if (ret > 160) {
      printf("ERROR receive count : %d\n", ret);
      state_ = SLEEPING;
    } else {
      for (int i = 0; i < ret; i++) {
        out[i] = Snack_Mulaw2Lin(buf[i]);
      }
    }
    return ret;
  } else if (state_ == WARNING) {
    generalFrameCounter++;
    //printf("%d "+generalFrameCounter); fflush(stdout);
    if (generalFrameCounter == 500) {
      generalFrameCounter = 0;
      state_ = SLEEPING;
    }
  } else if (state_ == SINGING) {
    if (afh == NULL) {
      afh = fopen(af_name, "rb");
      fseek(afh, 0L, SEEK_END);
      af_size = ftell(afh) / 2;
      fseek(afh, 0L, SEEK_SET);
      af_index = 0;
    }
    if (af_index < af_size) {
      int n = af_size - af_index;
      if (n > len_in) n = len_in;
      int r = fread(out, 2, n, afh);
      af_index += r;
    } else {
      fclose(afh);
      afh = NULL;
      state_ = SLEEPING;
    }
  }

  // clear output
  if (state_ != SPEAKING && state_ != SINGING) bzero(out, len_in * 2);

  // write awakening sound
  if (q_pos < ef_q_size[effect_no] - 1) {
    int len = ef_q_size[effect_no] - 1 - q_pos;
    if (len > len_in) len = len_in;
    memcpy(out, ef_q[effect_no] + q_pos, len * 2);
    q_pos += len_in;
  }

  return 0;
}

int main(int argc, char *argv[]) {

  if (argc < 5) {
    printf("%s <peer-address> <sample-rate> <avg-cutoff> <timeout>\n", argv[0]);
    return 1;
  }
  param_peer = argv[1];
  param_stt_rate = atoi(argv[2]);
  param_avg_cutoff = atoi(argv[3]);
  param_timeout = atoi(argv[4]);

  // init adc
  adc_ = AdcOpen(param_peer);
  // set parameters
  AdcSetAudioQuality(adc_, param_stt_rate);
  // AdcSetTrain(adc_, 1);
  AdcStart(adc_);
  // init kws
  pLASER = kws_init();
  ResetLASER(pLASER);
  // make sine wave
  // makeSineWave();
  // make q sound
  loadEffect(0);
  loadEffect(1);
  // make output file
  // fh = fopen("buffer.pcm","wb");
  // intro
  //strcpy(af_name,"q.pcm");
  //state_ = SINGING;

  if (alsa_init("plug:default", 16000, 160, 1, 1, callback) == -1) {
    fprintf(stderr, "Failed to init portaudio\n");
    return -1;
  }
  printf("paInit completed\n");
  printf("laser init completed\n");
  return 0;
}

