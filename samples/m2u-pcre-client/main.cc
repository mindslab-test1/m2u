#include <memory>
#include <vector>
#include <iostream>
#include <fstream>

#include <grpc++/grpc++.h>
#include <getopt.h>
#include <maum/m2u/facade/dialog.grpc.pb.h>
#include <libmaum/common/config.h>
#include <libgen.h>
#include <libmaum/common/util.h>
#include <gitversion/version.h>
#include <libgen.h>
#include <sc-pcre.h>

using namespace std;

void Usage(const char *prname) {
  std::cout
      << prname << " -f xxx.json -t \"context\"" << std::endl
      << prname << " -r \"regex string\" -t \"context\"" << std::endl
      << " -h --help  : show this message." << std::endl
      << " -v : version " << std::endl
      << " -f : regex file path" << std::endl
      << " -t : input string" << std::endl
      << " -r : regex string" << std::endl
      << std::endl;

  exit(0);
}

int main(int argc, char **argv) {
  char *base = basename(argv[0]);
  char *dir = dirname(argv[0]);

  chdir(dir);

  string filename;
  string input;
  string pcre;

  while (true) {
    int optindex = 0;
    static option l_options[] = {
        {"version", no_argument, nullptr, 'v'},
        {"help", no_argument, nullptr, 'h'},
        {"regex file path", required_argument, nullptr, 'f'},
        {"regex string", required_argument, nullptr, 'r'},
        {"input string", required_argument, nullptr, 't'},
        {nullptr, no_argument, nullptr, 0}
    };

    int ch = getopt_long(argc, argv, "vhf:r:t:", l_options, &optindex);
    if (ch == -1) {
        break;
    }
    switch (ch) {
      case 'v': {
        printf("%s version %s\n", base, version::VERSION_STRING);
        break;
      }
      case 'f': {
        filename = optarg;
        break;
      }
      case 'r': {
        pcre = optarg;
        break;
      }
      case 't': {
        filename = optarg;
        break;
      }
      case 'h': {
        Usage(base);
        break;
      }
      default: {
        break;
      }
    }
  }
  if (input.empty()){
    Usage(base);
    return 0;
  }

  if (filename.empty() && pcre.empty()){
    Usage(base);
    return 0;
  }

  libmaum::Config::Init(argc, argv, "m2u.conf");


  auto logger = LOGGER();

  if (!pcre.empty()){

    vector<string> match_group;
    if (SimplePcre::PcreExec(pcre.c_str(), input.c_str(), match_group)){
      std::cout << "[result] found" << std::endl;
    }else{
      std::cout << "[result] not found" << std::endl;
    }
    return 0;
  }else{
    auto ppcre = make_shared<SimpleClassifierPcre>();
    ppcre->LoadFile("test", filename);
    string skill;
    std::cout << "input = " << input << std::endl;

    if (ppcre->Find(skill, input)) {
      std::cout << "[result] found:" << skill << std::endl;
    }else{
      std::cout << "[result] not found" << std::endl;
    }
  }



  return 0;
}
