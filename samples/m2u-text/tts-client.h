#ifndef SAMPLE_TTS_CLIENT_H
#define SAMPLE_TTS_CLIENT_H

#include "CoreTtsApi.h"	// 음성합성엔진 클라이언트 API
#include <string>
#include <unordered_map>
#include <memory>
#include <atomic>
#include <mutex>

#include <maum/m2u/facade/dialog.grpc.pb.h>
#include <maum/m2u/facade/dialog.pb.h>

using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using std::string;
using maum::m2u::facade::AudioUtter;
using std::unordered_map;
using std::shared_ptr;
using std::atomic;
using std::mutex;

// unsigned int TtsCallback(int chan, unsigned char *voice, int voiceLen);

class TtsClientFactory;

/**
 * CoreVoice SimpleTtsClient 로서 음성으로 변환을 처리한다.
 */
class SimpleTtsClient {
 public:
  explicit SimpleTtsClient(char *ip, int port, int format);
 public:
  virtual ~SimpleTtsClient();
  /**
   * 생성된 클라이언트가 정상적인지 확인한다.
   * 생성된 클라이언트가 내부적으로 비정상적인 수 있다.
   */
  bool IsValid() {
    return core_tts_ != nullptr;
  }

  bool Open(ClientReaderWriter<AudioUtter, AudioUtter> *pstream);
  bool Open(ClientWriter<AudioUtter> *pstream);

  bool GetSpeech(const string &utter);
  int channel() {
    return channel_;
  }

  string TempFile() {
    return filename_;
  }

  string ConvertForSTT();

  void SendTtsAsStream(int mode);

  /**
   * 등록된 grpc 스트림 전송 객체를 반환한다.
   */
  ClientReaderWriter<AudioUtter, AudioUtter> *GetStream() {
    return stream_;
  }

 private:
  /**
   * SimpleTtsClient API 상태
   */
  enum TtsApiStatus {
    None = 0, // 비어있는 상태
    Created = 1,
    Opened = 2,
    GotChannel = 3
  };
  enum RunMode {
    NONE = 0,
    AUDIO,
    TEXT,
    AUDIOTOTEXT
  };
  CoreTTSClient *core_tts_ = nullptr;
  TtsApiStatus status_ = TtsApiStatus::None;
  int channel_ = 0;
  string filename_;
  ClientReaderWriter<AudioUtter, AudioUtter> *stream_;
  ClientWriter<AudioUtter> *wstream_;
  friend class TtsClientFactory;
  friend unsigned int TtsCallback(int chan, unsigned char *voice, int voiceLen);
  bool innerOpen();
};

/**
 * SimpleTtsClient 를 위한 API 초기화 및 콜백 관리.
 */
class TtsClientFactory {
  /**
   * 생성자
   */
  TtsClientFactory();
 public:
  SimpleTtsClient *Create();
  virtual ~TtsClientFactory();

  /**
   * 채널과 클라이언트를 등록한다.
   *
   * @param channel 채널번호
   * @param client 채널을 가지고 있는 클라이언트 객체
   */
  void Register(int channel, SimpleTtsClient *client) {
    channels_.insert(std::make_pair(channel, client));
  }

  /**
   * 채널에 연결된 클라이언트에 대한 등록 해제.
   *
   * 등록 해제 후 클라이언트를 삭제하지 않는다. 클라이언트는 자체적으로 소멸된다.
   * 실지로는 소멸자의 호출 과정에서 처리된다.
   *
   * FIXME
   * 채널번호가 클라이언트에 의해서 발생되는 경우라면
   * 모든 채널이 항상 1이 아닐지 의심이 된다.
   *
   * 만일 이 경우라면, 채널은 지정해서 사용하면 된다.
   *
   * @param channel 지울 채널 번호
   */
  void Unregister(int channel) {
    auto got = channels_.find(channel);
    if (got != channels_.end()) {
      channels_.erase(got);
    }
  }

  /**
   * 채널에 연결된 클라이언트를 반환한다.
   *
   * 클라이언트를 가져오는 이유는 현재 클라이언트에 grpc 스트림 전송 객체가 연결되어
   * 있기 때문이다.
   * @param channel 채널 번호
   * @return SimpleTtsClient 객체
   */
  SimpleTtsClient *GetClient(int channel) {
    auto got = channels_.find(channel);
    return got->second;
  }
  static TtsClientFactory *GetInstance();
 private:
  unordered_map<int, SimpleTtsClient *> channels_;
  /*!< 채널 맵*/
  string server_ip_ = "218.235.67.30"; /*!< 서버 주소 */
  // max ipv6 address 45
  int server_port_ = 40050;
  /*!< 포트 */
  int speech_format_ = WF_M16;
  /*!< 포맷 */
  static atomic<TtsClientFactory *> instance_;
  /*!< 싱글톤 */
  static mutex mutex_; /*!< 싱글톤 처리 방법 */
};

#endif // MAUM_TTS_CLIENT_H
