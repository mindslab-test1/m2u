#include "dialog-client.h"
#include "tts-client.h"
#include <fstream>
#include <libmaum/common/base64.h>

using std::unique_ptr;
using std::ofstream;

using maum::m2u::facade::TalkQuery;
using maum::m2u::facade::TalkAnalysis;

static inline string __str(const grpc::string_ref &ref) {
  return string(ref.begin(), ref.end());
}

std::string DialogClient::Talk(const std::vector<std::string> &messages,
                               std::string &domain, std::string &intention,
                               Meta &meta) {
  switch (mode_) {
    case AUDIO: {
      string merged;
      for (const auto &str : messages) {
        merged += str;
      }
      return AudioTalk(merged, domain, intention, meta);
    }
    case TEXT: {
      if (spec == DialogAgentProviderSpec::DAP_SPEC_V_2) {
        string merged;
        for (const auto &str : messages) {
          merged += str;
        }
        return TextTalkV2(merged, domain, intention, meta);
      } else {
        return TextTalk(messages, domain, intention, meta);
      }
    }
    case AUDIOTOTEXT: {
      string merged;
      for (const auto &str : messages) {
        merged += str;
      }
      return AudioToTextTalk(merged, domain, intention, meta);
    }
    case IMAGE: {
      string merged;
      for (const auto &str : messages) {
        merged += str;
      }
      return ImageToTextTalk(merged, domain, intention, meta);
    }
    default:return "";
  }
}

std::string DialogClient::AudioTalk(const string &msg,
                                    std::string &domain,
                                    std::string &intention,
                                    Meta &meta) {
  ClientContext ctx;
  ctx.AddMetadata("in.sessionid", std::to_string(session_id_));
  ctx.AddMetadata("in.lang", language_);
  ctx.AddMetadata("in.samplerate", "16000");

  std::shared_ptr<ClientReaderWriter<AudioUtter, AudioUtter>> stream(
      dialog_service_stub_->AudioTalk(&ctx));

  SimpleTtsClient *tts = TtsClientFactory::GetInstance()->Create();
  if (!tts->IsValid()) {
    delete tts;
    return "Invalid TTS Client status: cannot create tts client.";
  }
  if (!tts->Open(stream.get())) {
    delete tts;
    return "Invalid TTS Client status: cannot generate audio message.";
  }
  if (!tts->GetSpeech(msg)) {
    delete tts;
    return "Invalid TTS Conversion status: invalid retun";
  }

  auto logger = LOGGER();
  logger->debug<const char *>("calling finish ");

  //
  // TTS TO AUDIO FOR STT
  // SAVE FOR TTS, AND SEND AS STREAM
  tts->SendTtsAsStream(AUDIO);
  stream->WritesDone();

  turn_++;
  string file("audio-out");
  file += '-';
  file += std::to_string(session_id_);
  file += '-';
  file += std::to_string(turn_);
  file += ".mulaw";

  ofstream ofs(file, std::ofstream::binary);

  AudioUtter result;
  while (stream->Read(&result)) {
    ofs.write(result.utter().c_str(), result.utter().size());
  }
  Status status = stream->Finish();
  ofs.flush();
  ofs.close();

  if (status.ok()) {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second))));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    auto init_map = ctx.GetServerInitialMetadata();
    auto it_d = init_map.find("talk.domain");
    if (it_d != init_map.end()) {
      domain = Base64Decode(__str(it_d->second));
    }
    auto it_i = init_map.find("talk.intention");
    if (it_i != init_map.end()) {
      intention = Base64Decode(__str(it_i->second));
    }

    auto server_map = ctx.GetServerTrailingMetadata();
    auto it = server_map.find("talk.error");
    if (it != server_map.end()) {
      logger->info("talk.error {}", __str(it->second));
    }

    logger->debug("rpc failed. Code: {}, , Message: {}",
                  status.error_code(),
                  status.error_message());
    logger->debug("result file : {}", file);
    return "Result audio saved at " + file;
  } else if (status.error_code() == grpc::StatusCode::NOT_FOUND) {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(kv.first.data(),
                                 Base64Decode(kv.second.data())));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(kv.first.data(),
                                 kv.second.data()));
    }
    this->Open();
    return "RESTARTED!";
  } else {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(kv.first.data(),
                                 Base64Decode(kv.second.data())));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(string(kv.first.begin(), kv.first.end()),
                                 kv.second.data()));
    }

    return status.error_message() + std::to_string(status.error_code());
  }
  // delete tts;
}

std::string DialogClient::TextTalk(const std::vector<std::string> &messages,
                                   std::string &domain,
                                   std::string &intention,
                                   Meta &meta) {
  ClientContext ctx;
  ctx.AddMetadata("in.sessionid", std::to_string(session_id_));
  ctx.AddMetadata("in.lang", language_);

  std::shared_ptr<ClientReaderWriter<TextUtter, TextUtter>> stream(
      dialog_service_stub_->TextTalk(&ctx));

  for (const std::string &msg : messages) {
    TextUtter real;
    real.set_utter(msg);
    stream->Write(real);
  }
  stream->WritesDone();

  string full;
  TextUtter result;
  while (stream->Read(&result)) {
    full += result.utter();
  }
  Status status = stream->Finish();

  auto logger = LOGGER();
  if (!status.ok()) {
    logger->debug("rpc failed. Code: {}, Message: {}",
                  status.error_code(),
                  status.error_message());
  }
  if (status.ok()) {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second))));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    auto init_map = ctx.GetServerInitialMetadata();
    auto it_d = init_map.find("talk.domain");
    if (it_d != init_map.end()) {
      domain = Base64Decode(__str(it_d->second));
    }
    auto it_i = init_map.find("talk.intention");
    if (it_i != init_map.end()) {
      intention = Base64Decode(__str(it_i->second));
    }

    auto server_map = ctx.GetServerTrailingMetadata();
    auto it = server_map.find("talk.error");
    if (it != server_map.end()) {
      logger->info("talk.error {}", __str(it->second));
    }

    return full;
  } else if (status.error_code() == grpc::StatusCode::NOT_FOUND) {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second))));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    this->Open();
    return "RESTARTED!";
  } else {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second))));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    return status.error_message() + std::to_string(status.error_code());
  }
}

std::string DialogClient::TextTalkV2(const std::string &msg,
                                     std::string &domain,
                                     std::string &intention,
                                     Meta &meta) {
  ClientContext ctx;
  ctx.AddMetadata("in.sessionid", std::to_string(session_id_));
  ctx.AddMetadata("in.devicetoken", std::to_string(session_id_));

  TalkQuery query;
  TalkAnalysis analysis;

  query.set_chatbot(chatbot_);
  query.set_context_reset("false");
  query.mutable_query()->set_utter(msg);

  auto meta_fields = query.mutable_meta()->mutable_fields();
  Value v;
  v.set_string_value(std::to_string(session_id_));
  meta_fields->insert(meta_pair("in.sessionid", v));
  v.Clear();
  v.set_string_value("123123");
  meta_fields->insert(meta_pair("in.devicetoken", v));

  auto status = talk_service_stub_->Analyze(&ctx, query, &analysis);
  if (status.ok()) {
    domain = analysis.skill();
    intention = analysis.intent();
    return analysis.answer().utter();
  }
  return status.error_message();
}

std::string DialogClient::AudioToTextTalk(const string &msg,
                                          std::string &domain,
                                          std::string &intention,
                                          Meta &meta) {
#if 0
  ClientContext ctx;
  ctx.AddMetadata("in.sessionid", std::to_string(session_id_));
  ctx.AddMetadata("in.lang", language_);
  ctx.AddMetadata("in.samplerate", "8000");
  TextUtter results;
  std::shared_ptr<ClientReaderWriter<TextUtter, AudioUtter>> stream(
      stub_->AudioToTextTalk(&ctx));


  SimpleTtsClient *tts = TtsClientFactory::GetInstance()->Create();
  if (!tts->IsValid()) {
    delete tts;
    return "Invalid TTS Client status: cannot create tts client.";
  }
  if (!tts->Open(stream.get())) {
    delete tts;
    return "Invalid TTS Client status: cannot generate audio message.";
  }
  if (!tts->GetSpeech(msg)) {
    delete tts;
    return "Invalid TTS Conversion status: invalid retun";
  }

  auto logger = LOGGER();
  logger->debug<const char *>("calling finish ");

  //
  // TTS TO AUDIO FOR STT
  // SAVE FOR TTS, AND SEND AS STREAM
  tts->SendTtsAsStream(AUDIOTOTEXT);
  stream->WritesDone();

  Status status = stream->Finish();
  string full;
  if (!status.ok()) {
    logger->debug("rpc failed. Code: {}, Message: {}",
                  status.error_code(),
                  status.error_message());
  }
  if (status.ok()) {
    full = results.utter();

    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second)));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    auto init_map = ctx.GetServerInitialMetadata();
    auto it_d = init_map.find("talk.domain");
    if (it_d != init_map.end()) {
      domain = Base64Decode(__str(it_d->second));
    }
    auto it_i = init_map.find("talk.intention");
    if (it_i != init_map.end()) {
      intention = Base64Decode(__str(it_i->second));
    }

    auto server_map = ctx.GetServerTrailingMetadata();
    auto it = server_map.find("talk.error");
    if (it != server_map.end()) {
      logger->info("talk.error {}", __str(it->second));
    }

    return full;
  } else if (status.error_code() == grpc::StatusCode::NOT_FOUND) {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second))));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    this->Open();
    return "RESTARTED!";
  } else {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second))));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    return status.error_message() + std::to_string(status.error_code());
  }
#else
  return "";
#endif
}

std::string DialogClient::ImageToTextTalk(const string &filename,
                                          std::string &domain,
                                          std::string &intention,
                                          Meta &meta) {
  ClientContext ctx;
  auto logger = LOGGER();
  ctx.AddMetadata("in.sessionid", std::to_string(session_id_));
  ctx.AddMetadata("in.lang", language_);

  std::shared_ptr<ClientReaderWriter<Image, TextUtter>> stream(
      dialog_service_stub_->ImageToTextTalk(&ctx));

  // send image
  std::ifstream ifs(filename, std::ifstream::binary);

  std::vector<char> buffer((
                               std::istreambuf_iterator<char>(ifs)),
                           (std::istreambuf_iterator<char>()));
  std::cout << "buffer size " << buffer.size() << std::endl;
  // 파일을 쪼개서 조금씩 보낸다.

  Image utter;

  std::vector<char>::size_type sent = 0;
  while (sent < buffer.size()) {
    size_t remain = buffer.size() - sent;
    if (remain > 2048)
      remain = 2048;
    utter.set_body(buffer.data() + sent, remain);

    stream->Write(utter);

    sent += remain;
    std::cout << "sent: " << sent << ", last sent: " << remain << std::endl;
  }

  ifs.close();
  // end send image

  stream->WritesDone();

  TextUtter results;
  while (stream->Read(&results)) {

  }

  Status status = stream->Finish();
  string full;
  if (!status.ok()) {
    logger->debug("rpc failed. Code: {}, Message: {}",
                  status.error_code(),
                  status.error_message());
  }

  if (status.ok()) {  // 성공
    full = results.utter();
    return full;
  } else if (status.error_code() == grpc::StatusCode::NOT_FOUND) {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second))));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    this->Open();
    return "RESTARTED!";
  } else {
    for (auto kv: ctx.GetServerInitialMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 Base64Decode(__str(kv.second))));
    }
    for (auto kv: ctx.GetServerTrailingMetadata()) {
      meta.insert(std::make_pair(__str(kv.first),
                                 __str(kv.second)));
    }

    return status.error_message() + std::to_string(status.error_code());
  }
}
