#ifndef SAMPLE_DIALOG_CLIENT_H
#define SAMPLE_DIALOG_CLIENT_H

#include <memory>
#include <vector>
#include <map>

#include <grpc++/grpc++.h>
#include <maum/m2u/facade/dialog.grpc.pb.h>
#include <maum/m2u/facade/front.grpc.pb.h>
#include <maum/m2u/da/provider.grpc.pb.h>
#include "iconv.h"
#include <iostream>
#include <libmaum/common/config.h>

using std::string;
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using maum::m2u::facade::DialogService;
using maum::m2u::facade::Session;
using maum::m2u::facade::SessionKey;
using maum::m2u::facade::SessionStat;
using maum::m2u::facade::TextUtter;
using maum::m2u::facade::AudioUtter;
using maum::m2u::facade::Image;
using maum::m2u::facade::Caller;
using maum::m2u::facade::ChatbotList;
using maum::m2u::facade::ChatbotFinder;
using maum::m2u::facade::TalkService;
using maum::m2u::da::DialogAgentProviderSpec;

using google::protobuf::int64;
using google::protobuf::Empty;
using google::protobuf::Value;
using google::protobuf::Map;
using meta_pair = google::protobuf::Map<std::string, Value>::value_type;

class DialogClient {
 private:
  int64 session_id_ = 0;
  string language_ = "kor";
  int mode_ = NONE;
  ChatbotList chatbots_;
  string chatbot_;
  DialogAgentProviderSpec spec = DialogAgentProviderSpec::DAP_SPEC_V_2;
 public:
  typedef std::multimap<string, string> Meta;

 public:
  DialogClient(std::shared_ptr<Channel> channel)
      : dialog_service_stub_(DialogService::NewStub(channel)),
        talk_service_stub_(TalkService::NewStub(channel)),
        finder_stub_(ChatbotFinder::NewStub(channel)) {
  }

  int ShowChatbots() {
    Caller caller;

    Empty empty;
    ClientContext context;

    Status
        status = finder_stub_->GetChatbots(&context, empty, &chatbots_);
    if (chatbots_.chatbots_size() == 0) {
      std::cout << "No Chatbot!" << std::endl;
      return -1;
    } else if (status.ok()) {
      int index = 0;
      for (auto sg: chatbots_.chatbots()) {
        std::cout << ">>> " << (index + 1) << ": "
                  << sg.name() << ", " << sg.description() << std::endl;
        for (auto &skill: sg.skills()) {
          std::cout << "\t" << skill.skill() << " {" << skill.name()
                    << ", " << skill.lang()
                    << ", " << skill.version()
                    << ", " << skill.description()
                    << "} " << std::endl;
        }
        std::cout << std::endl;
        index++;
      }
      return index;
    } else {
      return -1;
    }
  }

  void SetChatbot(int index) {
    chatbot_ = chatbots_.chatbots(index).name();
  }
  void SetChatbot(const string &chatbot) {
    chatbot_ = chatbot;
  }

  bool Open() {
    Caller caller;
    Session session;
    caller.set_accessfrom(maum::m2u::facade::AccessFrom::TEXT_MESSENGER);
    caller.set_name("m2u text");
    caller.set_chatbot(chatbot_);

    ClientContext context;

    Status status = dialog_service_stub_->Open(&context, caller, &session);
    if (status.ok()) {
      session_id_ = session.id();
    } else {

    }
    return true;
  }

  // Assambles the client's payload, sends it and presents the response back
  // from the server.
  std::string Talk(const std::vector<std::string> &messages,
                   std::string &domain, std::string &intention, Meta &meta);

  std::string Talk(const std::string &message,
                   std::string &domain, std::string &intention, Meta &meta) {
    std::vector<std::string> tmp = {message};
    return Talk(tmp, domain, intention, meta);
  }

  void Close() {
    ClientContext ctx;
    SessionKey key;
    SessionStat stat;
    key.set_session_id(session_id_);
    Status ret = dialog_service_stub_->Close(&ctx, key, &stat);
    if (ret.ok()) {
      LOGGER()->debug("Session closed!");
    } else {
      LOGGER()->debug("An error when closing!");
    }
  }

  void SetMode(int mode) {
    mode_ = mode;
  }
  int64 GetSessionId() {
    return session_id_;
  }
  void SetLanguage(const string &lang) {
    language_ = lang;
  }
  void SwitchSpec() {
    // TODO
    // 지금은 V1 <-> V2 간 switching 만 존재
    if (spec == DialogAgentProviderSpec::DAP_SPEC_V_1)
      spec = DialogAgentProviderSpec::DAP_SPEC_V_2;
    else
      spec = DialogAgentProviderSpec::DAP_SPEC_V_1;
  }

 private:
  std::string AudioTalk(const string &msg,
                        std::string &domain, std::string &intention,
                        Meta &meta);
  std::string TextTalk(const std::vector<string> &msgs,
                       std::string &domain, std::string &intention,
                       Meta &meta);
  std::string TextTalkV2(const std::string &msg,
                         std::string &domain, std::string &intention,
                         Meta &meta);
  std::string AudioToTextTalk(const string &msg,
                              std::string &domain, std::string &intention,
                              Meta &meta);
  std::string ImageToTextTalk(const string &msg,
                              std::string &domain, std::string &intention,
                              Meta &meta);
  std::unique_ptr<DialogService::Stub> dialog_service_stub_;
  std::unique_ptr<TalkService::Stub> talk_service_stub_;
  std::unique_ptr<ChatbotFinder::Stub> finder_stub_;
  int turn_;
  enum RunMode {
    NONE = 0,
    AUDIO,
    TEXT,
    AUDIOTOTEXT,
    IMAGE
  };
};

#endif
