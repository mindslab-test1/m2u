#include <memory>
#include <vector>
#include <iostream>
#include <fstream>

#include <grpc++/grpc++.h>
#include <getopt.h>
#include <maum/m2u/facade/dialog.grpc.pb.h>
#include <libmaum/common/config.h>
#include <libgen.h>
#include <libmaum/common/util.h>
#include <gitversion/version.h>
#include <libgen.h>

#include "dialog-client.h"

enum FileType {
  OUT = 0,
  CSV
};

void Usage(const char *prname) {
  std::cout
      << prname << " [-a|-t]  [-r|-f file]" << std::endl
      << " -h --help  : show this message." << std::endl
      << "SERVER ---" << std::endl
      << " -s --server host:port : remote addr (eg:127.0.0.1:9901)" << std::endl
      << "CHATBOT ---" << std::endl
      << " -c --chatbot chatbot-name : chatbot name " << std::endl
      << "MODE ---" << std::endl
      << " -a --audio : audio mode " << std::endl
      << " -u --audiototext : check server response to text from audio file" << std::endl
      << " -i --image : send image file and receive to text"
      << std::endl
      << " -t --text  : text mode(default) " << std::endl
      << "INPUT ---" << std::endl
      << " -r --run   : run specified text only." << std::endl
      << " -f --file filename : read text messages from file." << std::endl
      << " -x --filetocsv filename : read message line by line to .csv file"
      << std::endl
      << "If no another argument, it runs text mode, user input!" << std::endl;

  exit(0);
}

int SelectChatbot(DialogClient &dialog) {
  int max = dialog.ShowChatbots();
  if (max < 0)
    return -1;

  std::string line;
  std::cout << "Select Chatbot [1-" << max << "|ql]: ";
  while (getline(std::cin, line)) {
    trim(line);
    if (line == "q" || line == "Q")
      return -1;
    if (line == "l" || line == "L") {
      std::cout << std::endl;
      max = dialog.ShowChatbots();
      if (max < 0)
        return -1;
    } else {
      try {
        int input = std::stoi(line);
        if (input >= 1 && input <= max) {
          dialog.SetChatbot(input - 1);
          std::cout << std::endl;
          return 0;
        }
      } catch (...) {
      }

    }

    std::cout << "Select Chatbot [1-" << max << "|ql]: ";
  }
  return 0;
}

int DoInput(DialogClient &dialog) {
  std::string line;
  std::string domain;
  std::string intention;
  DialogClient::Meta meta;
  std::cout << "Help:" << std::endl
            << "q: Stop chatting " << std::endl
            << "n: Restart session " << std::endl
            << "e: Set language as English " << std::endl
            << "k: Set language as Korean " << std::endl
            << "v: Switch Version (default v2) " << std::endl
            << "any text: talk talk talk" << std::endl << std::endl;

  std::cout << "{" << dialog.GetSessionId() << "} [qnke|User]: ";

  while (getline(std::cin, line)) {
    trim(line);
    if (line == "q" || line == "Q")
      break;
    if (line == "n" || line == "N") {
      dialog.Close();
      dialog.Open();
    } else if (line == "C" || line == "c") {
      dialog.Close();
    } else if (line == "k" || line == "K") {
      dialog.SetLanguage("kor");
    } else if (line == "e" || line == "E") {
      dialog.SetLanguage("eng");
    } else if (line == "v" || line == "V"){
      dialog.SwitchSpec();
    } else {
      domain.clear();
      intention.clear();
      meta.clear();
      std::string reply = dialog.Talk(line, domain, intention, meta);
      std::cout << "AI <<< : ["
                << domain << "/" << intention
                << "] : " << reply << std::endl;
      for (auto &kv : meta) {
        std::cout << "\tmeta[" << kv.first << "] = {" << kv.second << "}"
                  << std::endl;
      }
      std::cout << std::endl;
    }
    std::cout << "{" << dialog.GetSessionId() << "} [qnke|User]: ";
  }

  std::cout << std::endl;
  return 0;
}

int DoRun(DialogClient &dialog) {
  string reply;
  std::string domain;
  std::string intention;
  DialogClient::Meta meta;

  std::vector<std::string> messages = {
      "내일 ",
      "서울시 ",
      " 날씨 ",
      " 어때?",
  };

  reply = dialog.Talk(messages, domain, intention, meta);
  std::cout << "AI <<<: ["
            << domain << "/" << intention
            << "] : " << reply << std::endl;

  reply = dialog.Talk("내일은?", domain, intention, meta);
  std::cout << "AI <<<: ["
            << domain << "/" << intention
            << "] : " << reply << std::endl;

  reply = dialog.Talk("잘가!", domain, intention, meta);
  std::cout << "AI <<<: ["
            << domain << "/" << intention
            << "] : " << reply << std::endl;

  return 0;
}

int DoFile(DialogClient &dialog, const string &filename, const FileType &type) {
  std::string line;
  std::string domain;
  std::string intention;
  DialogClient::Meta meta;

  std::ifstream ifs(filename);
  std::ofstream ofs;
  if (type == OUT) {
    ofs.open(filename + ".out", std::ofstream::trunc);
  } else if (type == CSV) {
    ofs.open(filename + ".csv", std::ofstream::trunc);
  }

  while (getline(ifs, line)) {
    std::string reply = dialog.Talk(line, domain, intention, meta);
    if (type == OUT) {
      std::cout << ">>>>>>: " << line << std::endl;
      std::cout << "<<<<<<: " << domain << '/' << intention << ":: " << reply
                << std::endl;
      for (auto &kv : meta) {
        std::cout << "\tmeta[" << kv.first << "] = {" << kv.second << "}"
                  << std::endl;
      }
      std::cout << std::endl;
      ofs << "<<<<<<: " << domain << '/' << intention << ":: " << reply
          << std::endl;
    } else if (type == CSV) {
      std::cout << line << " , " << domain << " , " << reply << std::endl;
      ofs << line << " , " << domain << " , " << reply << std::endl;
    }
    usleep(50 * 1000);
  }

  std::cout << std::endl;
  return 0;
}

int main(int argc, char **argv) {

  enum RunMode {
    NONE = 0,
    AUDIO,
    TEXT,
    AUDIOTOTEXT,
    IMAGE
  };

  enum RunCmd {
    RUN_INPUT = 1,
    RUN_FILE,
    RUN_INTERNAL
  };

  char *base = basename(argv[0]);
  char *dir = dirname(argv[0]);
  chdir(dir);

  RunMode mode = TEXT;
  RunCmd cmd = RUN_INPUT;
  FileType type;

  string filename;
  string chatbot;
  string server;

  while (true) {
    int optindex = 0;
    static option l_options[] = {
        {"version", 0, 0, 'v'},
        {"chatbot", 1, 0, 'c'},
        {"server", 1, 0, 's'},
        {"audio", 0, 0, 'a'},
        {"text", 0, 0, 't'},
        {"audiototext", 0, 0, 'u'},
        {"file", 1, 0, 'f'},
        {"filetocsv", 1, 0, 'x'},
        {"run", 0, 0, 'r'},
        {"image", 0, 0, 'i'},
        {"help", 0, 0, 'h'}
    };

    int ch = getopt_long(argc, argv, "vc:atuf:x:rhs:i", l_options, &optindex);
    if (ch == -1) {
      if (optindex >= argc)
        Usage(base);
      else
        break;
    }
    switch (ch) {
      case 'v':
        printf("%s version %s\n", base, version::VERSION_STRING);
        break;
      case 'a':
        mode = AUDIO;
        break;
      case 't':
        mode = TEXT;
        break;
      case 'u':
        mode = AUDIOTOTEXT;
        break;
      case 'i':
        mode = IMAGE;
        break;
      case 'c':
        chatbot = optarg;
        break;
      case 'f':
        type = OUT;
        filename = optarg;
        cmd = RUN_FILE;
        break;
      case 'x':
        type = CSV;
        filename = optarg;
        cmd = RUN_FILE;
        break;
      case 'r':
        cmd = RUN_INTERNAL;
        break;
      case 's':
        server = optarg;
        break;
      case 'h':
        Usage(base);
        break;
      default:
        Usage(base);
        break;
    }
  }

  auto &c = libmaum::Config::Init(argc, argv, "m2u.conf");

  if (server.empty()) {

    string ip = c.Get("frontd.export.ip");
    string port = c.Get("frontd.export.port");
    server = ip;
    server += ':';
    server += port;
  }

  DialogClient dialog(grpc::CreateChannel(
      server, grpc::InsecureChannelCredentials()));

  dialog.SetMode(mode);

  if (!chatbot.empty()) {
    dialog.SetChatbot(chatbot);
  } else {
    if (SelectChatbot(dialog) < 0) {
      return -1;
    }
  }

  dialog.Open();
  switch (cmd) {
    case RUN_INTERNAL:
      DoRun(dialog);
      break;
    case RUN_FILE:
      DoFile(dialog, filename, type);
      break;
    case RUN_INPUT:
    default:
      DoInput(dialog);
      break;
  }

  dialog.Close();

  return 0;
}

