#!/bin/bash

java -Dhazelcast.client.config=${MAUM_ROOT}/etc/hazelcast-client.xml \
    -cp ${MAUM_ROOT}/lib/m2u-hzc-repack-2.1.0-all.jar \
    com.hazelcast.client.console.ClientConsoleApp
