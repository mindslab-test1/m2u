#include "musicplayer-agent.h"
#include <sstream>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

using namespace std;
using grpc::Channel;
using maum::m2u::da::DialogSessionState;

bool RegexCompiling(const string &regexp, vector<regex_t *> &regVec) {
  regex_t *re = new regex_t();

  int status = regcomp(re, regexp.c_str(), REG_EXTENDED | REG_ICASE);
  if (status != 0) {
    return false;
  }

  regVec.push_back(re);
  return true;
}

MusicPlayerAgent::MusicPlayerAgent(const string &ip, const string &port)
    : ip_(ip),
      port_(port) {
  auto logger = LOGGER();
  regexlist_ = vector<regex_t *>();

  // Regex prepearation

  std::string regexp =
      ".*(listen to|play|please|place|played|playing)[[:space:]]+[a |the |an ]*(y |any |some )*(.+)(music|song|songs)+( by (.+))*$";
  if (!RegexCompiling(regexp, regexlist_)) {
    //Error code - unable to compile regex
    logger->debug<const char *>("something went wrong to compile");
  }

  regexp =
      ".*(listen to|play|please|place|played|playing)[[:space:]]+(a |the |an )*(.+)([:space:]|(played )|sung )+(by)[[:space:]]+(.+)";
  if (!RegexCompiling(regexp, regexlist_)) {
    logger->debug<const char *>("something went wrong to compile");
    //Error code - unable to compile regex
  }

  regexp =
      ".*(listen to|play|please|place|played|playing)[[:space:]]+[a |the |an ]*(.+)by[[:space:]]+(.+)";
  if (!RegexCompiling(regexp, regexlist_)) {
    logger->debug<const char *>("something went wrong to compile");
    //Error code - unable to compile regex
  }

  // 확장된 Provider
  // DA 정보
  provider_ = make_shared<DialogAgentProviderParam>();

  provider_->set_name("music");
  provider_->set_description("MindsLab Music Agent");
  provider_->set_version("0.2");
  provider_->set_single_turn(true);
  provider_->set_agent_kind(AgentKind::AGENT_SDS);
  provider_->set_require_user_privacy(false);

  // Runtime 정보
  runtime_ = make_shared<RuntimeParameterList>();

  auto db_host = runtime_->mutable_params()->Add();
  db_host->set_name("db_host");
  db_host->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_host->set_desc("Database IP");
  db_host->set_default_value("10.122.64.66");
  db_host->set_required(true);

  auto db_port = runtime_->mutable_params()->Add();
  db_port->set_name("db_port");
  db_port->set_type(maum::m2u::common::DataType::DATA_TYPE_INT);
  db_port->set_desc("Database Port");
  db_port->set_default_value("3306");
  db_port->set_required(true);

  auto db_user = runtime_->mutable_params()->Add();
  db_user->set_name("db_user");
  db_user->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_user->set_desc("Database Username");
  db_user->set_default_value("root");
  db_user->set_required(true);

  auto db_pwd = runtime_->mutable_params()->Add();
  db_pwd->set_name("db_pwd");
  db_pwd->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_pwd->set_desc("Database Password");
  db_pwd->set_default_value("root");
  db_pwd->set_required(true);

  auto db_database = runtime_->mutable_params()->Add();
  db_database->set_name("db_database");
  db_database->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_database->set_desc("Database DB");
  db_database->set_default_value("Music");
  db_database->set_required(true);

  // 사용자 속성 정보
  attrs_ = make_shared<UserAttributeList>();

  auto genre = attrs_->mutable_attrs()->Add();
  genre->set_name("genre");
  genre->set_title("장르");
  genre->set_desc("좋아하는 장르");
  genre->set_type(maum::m2u::facade::DATA_TYPE_STRING);

  status_ = DialogAgentState::DIAG_STATE_IDLE;
}

MusicPlayerAgent::~MusicPlayerAgent() {
  mysql_close(connection_);
  for (regex_t *regexp : regexlist_) {
    regfree(regexp);
    free(regexp);
  }
}

int MatchAnalysis(regmatch_t *pieces, int maxMatch) {
  int matchedLen = 0;
  for (int index = 0; index < maxMatch; index++) {
    if (pieces[index].rm_so != -1) {
      matchedLen = index + 1;
    }
  }

  return matchedLen;
}

static int g_count = 0;
Status MusicPlayerAgent::Talk(::grpc::ServerContext *context,
                              const TalkRequest *request,
                              TalkResponse *response) {
  // 이 대화 에이전트는 현재 상태를 무조건 종료한다.
  response->set_state(DialogSessionState::DIAG_CLOSED);

  auto logger = LOGGER();

  logger->debug("domain : {}", request->skill());
  logger->debug("intention : {}", request->intent());
  logger->debug("lang : ", request->lang());

  connection_ = mysql_init(NULL);

  if (mysql_real_connect(connection_,
                         db_host_.c_str(),
                         db_user_.c_str(),
                         db_pwd_.c_str(),
                         db_database_.c_str(),
                         db_port_,
                         NULL,
                         0) == NULL) {
    logger->error("에러 : {} => {}",
                  mysql_errno(connection_),
                  mysql_error(connection_));
  } else {
    logger->debug<const char *>("연결 성공");
  }

  if (!mysql_set_character_set(connection_, "utf8")) {
    logger->debug("New client character set: {}",
                  mysql_character_set_name(connection_));
  }

  string url, inputSent;

  inputSent = request->text();
  logger->debug("inputSent : {}", inputSent);

  if (request->lang() == maum::common::LangCode::eng) {
    for (regex_t *regexp : regexlist_) {
      const int maxMatch = 10;
      regmatch_t pieces[maxMatch];
      int matched = regexec(regexp, inputSent.c_str(), maxMatch, pieces, 0);
      int matchingLen = MatchAnalysis(pieces, maxMatch);

      if (!matched && matchingLen == 4) {
        logger->debug<const char *>("pieceMatch size 4");
        std::string title, artist;

        try {
          title = inputSent.substr(pieces[2].rm_so,
                                   pieces[2].rm_eo - pieces[2].rm_so - 1);
          artist = inputSent.substr(pieces[3].rm_so,
                                    pieces[3].rm_eo - pieces[3].rm_so);
        } catch (std::exception &ex) {
          logger->debug("Exception Error : {}", ex.what());
          break;
        }

        struct SongInfo
            targetsong = {title, artist, std::string(), std::string(), "EN"};
        url = GetKnowledge(targetsong);
      } else if (!matched && matchingLen == 5) {
        logger->debug<const char *>("pieceMatch size 5");
        std::string target;

        try {
          target = inputSent.substr(pieces[3].rm_so,
                                    pieces[3].rm_eo - pieces[3].rm_so - 1);
        } catch (std::exception &ex) {
          logger->debug("Exception Error : {}", ex.what());
          break;
        }

        struct SongInfo
            targetsong = {std::string(), std::string(), target, target, "EN"};
        url = GetKnowledge(targetsong);
      } else if (!matched && matchingLen == 7) {
        logger->debug<const char *>("pieceMatch size 7");
        std::string target, artist;

        try {
          target = inputSent.substr(pieces[3].rm_so,
                                    pieces[3].rm_eo - pieces[3].rm_so - 1);
          artist = inputSent.substr(pieces[6].rm_so,
                                    pieces[6].rm_eo - pieces[6].rm_so);
        } catch (std::exception &ex) {
          logger->debug("Exception Error : {}", ex.what());
          break;
        }

        struct SongInfo
            targetsong = {std::string(), artist, target, target, "EN"};
        url = GetKnowledge(targetsong);
      } else if (!matched && matchingLen == 8) {
        logger->debug<const char *>("pieceMatch size 6");
        std::string title, artist;

        try {
          title = inputSent.substr(pieces[3].rm_so,
                                   pieces[3].rm_eo - pieces[3].rm_so - 1);
          artist = inputSent.substr(pieces[7].rm_so,
                                    pieces[7].rm_eo - pieces[7].rm_so - 1);
        } catch (std::exception &ex) {
          logger->debug("Exception Error : {}", ex.what());
          break;
        }

        struct SongInfo
            targetsong = {title, artist, std::string(), std::string(), "EN"};
        url = GetKnowledge(targetsong);
      }

      if (!url.empty() && url != "None" && url != "Fail") {
        break;
      }
    }
  } else {
    struct SongInfo targetsong =
        {std::string(), std::string(), std::string(), std::string(), "KR"};
    for (const auto &sent: request->named_entity_analysis().sentences()) {
      for (const auto &ne : sent.nes()) {
        logger->debug("  named entity : type={}, text={}",
                      ne.type(),
                      ne.text());
        if (ne.type() == "AF_WORKS") {
          string title = ne.text();
          EraseSpaces(title);
          logger->debug("  AF_WORKS: trimmed text={}", title);
          targetsong.title = title;
        } else if (ne.type() == "PS_SINGER") {
          string singer = ne.text();
          EraseSpaces(singer);
          logger->debug("  PS_SINGER: trimmed text={}", singer);
          targetsong.artist = singer;
        }
      }
    }

    url = GetKnowledge(targetsong);
  }

  if (url.empty() || url == "None" || url == "Fail") {
    if (request->lang() == maum::common::LangCode::kor) {
      response->set_text("요청하신 노래가 준비되있지 않습니다. 죄송합니다.");
    } else {
      response->set_text("I'm sorry, the song is not available yet");
    }
    response->set_state(DialogSessionState::DIAG_CLOSED);
    return Status::OK;
  }

  g_count++;
  switch (g_count % 2) {
    case 0:
      if (request->lang() == maum::common::LangCode::kor) {
        response->set_text("이제 음악은 초롱이와 함께 즐기세요.");
      } else {
        response->set_text(
            "This song is provided by Minds Lab, enjoy the music.");
      }
      break;
    case 1:
      if (request->lang() == maum::common::LangCode::kor) {
        response->set_text("요청하신 노래 시작합니다.");
      } else {
        response->set_text(
            "We wish you have a wonderful day with this music, enjoy it");
      }
      break;
  }

  using namespace google::protobuf;
  using mp = google::protobuf::Map<string, string>::value_type;
  auto meta_map = response->mutable_meta();
  meta_map->insert(mp("out.embed.type", "audio"));
  if (request->access_from() == maum::m2u::facade::AccessFrom::SPEAKER) {
    string url2 = "http://cdn.mindslab.ai/musics/" + url + ".g711";
    meta_map->insert(mp("out.embed.data.url", url2));
    meta_map->insert(mp("out.embed.data.mime", "audio/x-raw"));
    meta_map->insert(mp("out.embed.data.samplerate", "44100"));
  } else {
    string url2 = "http://cdn.mindslab.ai/musics/" + url + ".mp3";
    meta_map->insert(mp("out.embed.data.url", url2));
    meta_map->insert(mp("out.embed.data.mime", "audio/mpeg3"));
  }

  return Status::OK;
}

Status MusicPlayerAgent::Close(ServerContext *context,
                               const ::maum::m2u::da::TalkKey *key,
                               ::maum::m2u::da::TalkStat *stat) {
  auto logger = LOGGER();
  logger->debug("Closing for {} {}",
                key->session_id(), key->agent_key());
  stat->set_session_key(key->session_id());
  stat->set_agent_key(key->agent_key());

  // 해당 세션 및 대화에 대한 처리 결과 데이터를 상세히 정의할 수 없어서
  // 아래와 같이 처리한다.
  using pair = google::protobuf::Map<string, string>::value_type;
  auto meta = stat->mutable_meta();
  meta->insert(pair("name, value"));

  return Status::OK;
}

Status MusicPlayerAgent::Init(::grpc::ServerContext *context,
                              const ::maum::m2u::da::InitParameter *request,
                              ::maum::m2u::da::DialogAgentProviderParam *response) {
  LOGGER()->debug<const char *>("Music Init Function");
  status_ = DialogAgentState::DIAG_STATE_INITIALIZING;
  db_host_ = request->params().at("db_host");
  db_port_ = std::stoi(request->params().at("db_port"));
  db_user_ = request->params().at("db_user");
  db_pwd_ = request->params().at("db_pwd");
  db_database_ = request->params().at("db_database");

  response->CopyFrom(*provider_);
  status_ = DialogAgentState::DIAG_STATE_RUNNING;
  return Status::OK;
}

Status MusicPlayerAgent::GetProviderParameter(::grpc::ServerContext *context,
                                              const ::google::protobuf::Empty *request,
                                              ::maum::m2u::da::DialogAgentProviderParam *response) {
  response->CopyFrom(*provider_);

  return Status::OK;
}

Status MusicPlayerAgent::IsReady(::grpc::ServerContext *context,
                                 const ::google::protobuf::Empty *request,
                                 ::maum::m2u::da::DialogAgentStatus *response) {
  response->set_state(status_); // 현재 DA status를 반환
  return Status::OK;
}

Status MusicPlayerAgent::GetRuntimeParameters(::grpc::ServerContext *context,
                                              const ::google::protobuf::Empty *request,
                                              ::maum::m2u::da::RuntimeParameterList *response) {
  response->CopyFrom(*runtime_);
  return Status::OK;
}

Status MusicPlayerAgent::GetUserAttributes(::grpc::ServerContext *context,
                                           const ::google::protobuf::Empty *request,
                                           ::maum::m2u::common::UserAttributeList *response) {
  shared_ptr<UserAttributeList> attr = make_shared<UserAttributeList>();
  auto location = attr->mutable_attrs()->Add();
  location->set_name("favorite");
  location->set_type(DataType::DATA_TYPE_STRING);
  location->set_desc("즐겨찾기");

  response->CopyFrom(*attr);
  return Status::OK;
}

Status MusicPlayerAgent::Terminate(::grpc::ServerContext *context,
                                   const ::google::protobuf::Empty *request,
                                   ::google::protobuf::Empty *response) {
  status_ = DialogAgentState::DIAG_STATE_TERMINATED;
  return Status::OK;
}

