#include "musicplayer-agent.h"
#include <iostream>
#include <vector>
#include <libmaum/common/config.h>
using namespace std;

string MusicPlayerAgent::GetKnowledge(const struct SongInfo &targetsong) {
  MYSQL_RES *sql_result;
  MYSQL_ROW sql_row;

  std::string query, db_result;
  auto logger = LOGGER();

  // 데이터베이스 선택
  string dbname = db_database_;
  if (mysql_select_db(connection_, dbname.c_str())) {
    logger->error("에러 : {} => {}",
                  mysql_errno(connection_),
                  mysql_error(connection_));
    return NULL;
  }

  // Query Initialization
  query = "SELECT URL FROM Music.Songs WHERE ";

  // Language
  if (targetsong.lang == "EN") {
    query += "Language = 'EN'";
  } else {
    query += "Language = 'KR'";
  }

  if (!targetsong.title.empty() && !targetsong.artist.empty()) {
    query += " AND (Title = \"" + targetsong.title + "\" AND Artist LIKE \"%" + targetsong.artist + "%\")";
  } else if (!targetsong.title.empty()) {
    query += " AND Title = \"" + targetsong.title + "\"";
  } else if (!targetsong.artist.empty()) {
    query += " AND Artist LIKE \"%" + targetsong.artist + "%\"";
  }

  if (!targetsong.genre.empty() && !targetsong.mood.empty()) {
    query += " AND (Genre = \"" + targetsong.genre + "\" OR Mood = \"" + targetsong.mood + "\")";
  } else if (!targetsong.genre.empty()) {
    query += " AND Genre = \"" + targetsong.genre + "\"";
  } else if (!targetsong.mood.empty()) {
    query += " AND Mood = \"" + targetsong.mood + "\"";
  }

  logger->debug("Query : {}", query);

  int state = 0;
  state = mysql_query(connection_, query.c_str());
  int row_num = 0, field = 0;
  vector<string> songList;

  if (state == 0) {
    sql_result = mysql_store_result(connection_);
    row_num = mysql_num_rows(sql_result);
    field = mysql_num_fields(sql_result);

    if (row_num == 0) {
      logger->debug<const char *>("sql query result : None");

      return "None";
    }

    while ((sql_row = mysql_fetch_row(sql_result)) != NULL) {
      // Result Set 에서 1개씩 배열을 가져옴.
      for (int cnt = 0; cnt < field; cnt++) {
        // printf("%s\t", sql_row[cnt]);
        songList.emplace_back(sql_row[cnt]);
      }
    }

    mysql_free_result(sql_result);
  } else {
    logger->debug("sql query state fail : {}", state);

    return "Fail";
  }

  db_result = songList.front();
  mysql_close(connection_);

  return db_result;
}
