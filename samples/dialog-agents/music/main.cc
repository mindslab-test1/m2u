#include <iostream>

#include <grpc++/grpc++.h>

#include "musicplayer-agent.h"
#include <csignal>

#include <libmaum/common/config.h>

using grpc::Server;
using grpc::ServerBuilder;

using namespace std;
using std::shared_ptr;

shared_ptr<MusicPlayerAgent> globAgent;

volatile std::sig_atomic_t g_signal_status;

void HandleSignal(int signal) {
  // CALL DESTRUCTOR
  LOGGER()->info("handling {}", signal);
  g_signal_status = signal;
  globAgent.reset();
  exit(0);
}

void RunServer(const string &port) {
  auto logger = LOGGER();
  std::string server_address("[::]:");
  server_address += port;
  logger->debug("Weather server address : {}", server_address);



  auto agent = std::make_shared<MusicPlayerAgent>("[::]",
                                                  port);
  globAgent = agent;

  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(agent.get());

  unique_ptr<Server> server(builder.BuildAndStart());

  if (server) {
    logger->info("Server listening on {} with {}",
                 server_address,
                 agent->Name());
    agent.reset();
    server->Wait();
  }
}

int main(int argc, char *argv[]) {
  std::signal(SIGINT, HandleSignal);
  //std::signal(SIGSEGV, HandleSignal);
  //std::signal(SIGBUS, HandleSignal);
  std::signal(SIGTERM, HandleSignal);

  libmaum::Config::Init(argc, argv, "m2u.conf");

  RunServer(argv[2]);
}
