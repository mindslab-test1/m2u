#include <iostream>
#include <grpc++/grpc++.h>
#include <csignal>
#include <libmaum/common/config.h>
#include "app-weather-agent.h"

using grpc::Server;
using grpc::ServerBuilder;

using namespace std;
using std::shared_ptr;

shared_ptr<AppWeatherAgent> globAgent;

volatile std::sig_atomic_t g_signal_status;

void HandleSignal(int signal) {
  // CALL DESTRUCTOR
  LOGGER()->info("handling {}", signal);
  g_signal_status = signal;
  globAgent.reset();
  exit(0);
}

void RunServer(libmaum::Config sds_conf) {
  libmaum::Config &c = libmaum::Config::Instance();
  c.DumpPid();
  auto logger = LOGGER();

  std::string server_address(c.Get("listen"));

  string sds_remote = sds_conf.Get("maum.sds.listen");
  LOGGER()->debug("sds_remote: {}", sds_remote);

  string maum_remote = c.Get("frontd.export.ip");
  maum_remote += ':';
  maum_remote += c.Get("frontd.export.port");

  auto agent = std::make_shared<AppWeatherAgent>(c.Get("host"),
                                                 c.Get("port"),
                                                 maum_remote, sds_remote);

  if (agent->IsReady()) {
    globAgent = agent;

    ServerBuilder builder;
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(agent.get());

    unique_ptr<Server> server(builder.BuildAndStart());
    if (server) {
      logger->info("Server listening on {} with {}",
                   server_address,
                   agent->Name());
      agent.reset();
      server->Wait();
    }
  } else {
    logger->warn<const char *>("Cannot register service!");
  }
}

int main(int argc, char *argv[]) {
  std::signal(SIGINT, HandleSignal);
  //std::signal(SIGSEGV, HandleSignal);
  //std::signal(SIGBUS, HandleSignal);
  std::signal(SIGTERM, HandleSignal);

  libmaum::Config::Init(argc, argv, "m2u.conf");
  libmaum::Config &sds_conf = libmaum::Config::Init(argc, argv, "brain-sds.conf");

  RunServer(sds_conf);
}
