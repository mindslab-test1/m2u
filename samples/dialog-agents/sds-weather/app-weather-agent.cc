#include <maum/brain/sds/resolver.grpc.pb.h>
#include <maum/brain/sds/sds.grpc.pb.h>
#include "app-weather-agent.h"
#include <libmaum/common/config.h>
#include <grpc++/create_channel.h>

using namespace std;
using grpc::Channel;
using google::protobuf::Map;
using maum::m2u::da::DialogSessionState;
using grpc::ClientContext;
using namespace google::protobuf;
using namespace maum::brain::sds;

AppWeatherAgent::AppWeatherAgent(const string &ip, const string &port,
                                 const string &maumd, const string &resolver)
    : ip_(ip),
      port_(port),
      client_(grpc::CreateChannel(maumd, grpc::InsecureChannelCredentials())),
      sds_remote_(resolver) {
  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  DialogAgentInstanceResource res;
  res.set_name("weather");
  res.set_description("MindsLab AppWeather Agent test Version 0.1");
  res.set_version("0.1");
  res.set_server_ip(ip_);
  res.set_server_port(std::stoi(port_));
  res.mutable_param()->set_single_turn(false);

  Json::Value &root = c.GetJson();
  Json::Value &regs = root["register"];
  for (auto &reg: regs) {
    res.set_chatbot(reg["svc-group"].asString());
    res.set_skill(reg["domain"].asString());
    res.set_lang(ToLangCode(reg["lang"].asString()));
    client_.Register(res);
  }

  loc_timediff_ = c.Get("timediff");
}

AppWeatherAgent::~AppWeatherAgent() {
  client_.Unregister();

  mysql_close(connection_);
}

void TimeConverter(string &input) {
  if (stoi(input) > 12) {
    input =
        "오후 " + to_string(stoi(input) - 12);
  } else if (stoi(input) < 12) {
    input =
        "오전 " + to_string(stoi(input));
  } else if (input == "00") {
    input = "오전 0";
  } else {
    input =
        "낮 " + to_string(stoi(input));
  }
}

Status AppWeatherAgent::Talk(::grpc::ServerContext *context,
                             const TalkRequest *request,
                             TalkResponse *response) {
  auto logger = LOGGER();
  libmaum::Config &c = libmaum::Config::Instance();
  logger->debug("Sesstion ID : {}", request->session_id());

  // session 기반 동작을 위해 추가.

  ModelGroup mg;
  mg.set_name("Weather");
  mg.set_lang(maum::common::LangCode::kor);
  mg.set_is_external(true);
  mg.set_is_education(false);

  stub_sds_resolver_ = SdsServiceResolver::NewStub(
      grpc::CreateChannel(sds_remote_, grpc::InsecureChannelCredentials()));

  ClientContext ctx;
  ServerStatus ss;
  Status find_status = stub_sds_resolver_->Find(&ctx, mg, &ss);

  if (find_status.ok()) {
    logger->debug<const char *>("Sds Impl Open Success");
  } else {
    logger->debug<const char *>("Sds Impl Open Fail");
  }

  auto sdssImpl =
      SpokenDialogService::NewStub(
          grpc::CreateChannel(ss.server_address(),
                              grpc::InsecureChannelCredentials()));

  DialogueParam dp;
  dp.set_model("Weather_v0.2");
  dp.set_session_key(request->session_id());
  OpenResult openResult;
  Status open_status = sdssImpl->Open(&ctx, dp, &openResult);

  if (open_status.ok()) {
    logger->debug<const char *>("Sds Impl Open Success");
  } else {
    logger->debug<const char *>("Sds Impl Open Fail");
  }

  // DB Connection 설정

  connection_ = mysql_init(NULL);
  if (mysql_real_connect(connection_,
                         c.Get("db-host").c_str(),
                         c.Get("db-user").c_str(),
                         c.Get("db-pass").c_str(),
                         c.Get("db-database").c_str(),
                         (unsigned int) c.GetAsInt("db-port"),
                         NULL,
                         0) == NULL) {
    logger->error("에러 : {} => {}",
                  mysql_errno(connection_),
                  mysql_error(connection_));
  } else {
    logger->debug<const char *>("연결 성공");
  }
  // DB character_set
  if (!mysql_set_character_set(connection_, "utf8")) {
    logger->debug("New client character set: {}",
                  mysql_character_set_name(connection_));
  }

  // 대화 입력
  SdsQuery sq;
  sq.set_model(dp.model());
  sq.set_session_key(dp.session_key());
  sq.set_utter(request->text().c_str());
  Intent intent;
  ClientContext understand_ctx;
  Status understand_status = sdssImpl->Understand(&understand_ctx, sq, &intent);

  if (understand_status.ok()) {
    logger->debug<const char *>("Sds Impl Understand Success");
  } else {
    logger->debug<const char *>("Sds Impl Understand Fail");
  }

  // condition 에 따른 처리
  Entities entities;
  entities.set_model(dp.model());
  // session key 설정
  entities.set_session_key(dp.session_key());
  // copy filled slot map to result slot map
  EntityData entityData;
  entities.mutable_entity_data()->Add()
      ->mutable_entities()->insert(intent.filled_entities().begin(),
                                   intent.filled_entities().end());

  bool _province = false, _location = false, _date = false;
  string value[4];

  // check filled slot
  auto filled_slot = intent.filled_entities();
  logger->debug("Size : {}", filled_slot.size());
  for (auto &it : filled_slot) {
    if (it.first == "Weather.province") {
      value[0] = it.second;
      _province = true;
    } else if (it.first == "Weather.location") {
      value[1] = it.second;
      _location = true;
    } else if (it.first == "Weather.date") {
      value[2] = it.second;
      _date = true;
    }
    logger->debug("first : {}/second : {}", it.first, it.second);
  }

  string location_token[4];
  unsigned int location_token_size = 0;
  string::size_type s_pos, e_pos;
  if (_location) {
    string location_temp;
    auto got = filled_slot.find("Weather.location");
    if (got != filled_slot.end()) {
      logger->debug("[Location] {}", got->second);
      location_temp = got->second;
    }
    s_pos = 0;
    e_pos = location_temp.find_first_of(" ");
    if (e_pos == string::npos) {
      location_token[location_token_size] = location_temp;
    }
    while (e_pos != string::npos) {
      location_token[location_token_size] =
          location_temp.substr(s_pos, e_pos - s_pos);
      logger->debug("[Location Token {}] {}",
                    location_token_size,
                    location_token[location_token_size]);
      location_temp = location_temp.substr(e_pos - s_pos + 1);
      location_token_size++;
      e_pos = location_temp.find_first_of(" ");
      if (e_pos == string::npos) {
        location_token[location_token_size] = location_temp;
        logger->debug("[Location Token {}] {}",
                      location_token_size,
                      location_token[location_token_size]);
      } else {
        logger->debug<const char *>("npos 아님");
      }
    }
  }

  // get sub command
  string slu1_request;
  if (!intent.origin_best_slu().empty()) {
    s_pos = intent.origin_best_slu().find_first_of("(");
    e_pos = intent.origin_best_slu().find_first_of(",");
    if (e_pos == string::npos) {
      e_pos = intent.origin_best_slu().find_first_of(")");
    } else {
      logger->debug("[SLU1_FIND] {}",
                    intent.origin_best_slu().find_first_of(")"));
    }
    slu1_request =
        intent.origin_best_slu().substr(s_pos + 1, e_pos - s_pos - 1);
    logger->debug("[SLU1 Request] {}", slu1_request);
  }

  // 데이터베이스 선택
  MYSQL_RES *sql_result;
  MYSQL_ROW sql_row;
  string dbname = c.Get("db-database");

  if (mysql_select_db(connection_, dbname.c_str())) {
    logger->debug("에러 : {}, {}",
                  mysql_errno(connection_),
                  mysql_error(connection_));
  }

  string query;

  // Get zone code
  query = "select code from zone where zone like '";
  if (_province && _location) {
    for (unsigned index = 0; index <= location_token_size; index++) {
      query += location_token[index];
      query += "%";
    }
    query +=
        "' and substr(code, 1, 2) in(select substr(code, 1, 2) gun from zone where zone like '";
    query += value[0];
    query += "%')";
  } else if (_province || _location) {
    if (_province) {
      query += value[0];
      query += "%' and substr(code,1,2)!='00' and substr(code,3,2)='00'";
    } else if (_location) {
      for (unsigned index = 0; index <= location_token_size; index++) {
        query += location_token[index];
        query += "%";
      }
      query += "'";
    }
  } else {
    query +=
        "강남구%' and substr(code, 1, 2) in(select substr(code, 1, 2) gun from zone where zone like '서울특별시%')";
  }

  logger->debug("[Get ZoneCode QUERY] {}", query);

  int state = 0;
  char node_data[12][40];
  string s_node_data[100][12];
  string column_name[12] = {
      "id",
      "Weather.province",
      "Weather.city",
      "Weather.Gu",
      "Weather.dong",
      "Weather.date",
      "Weather.state",
      "Weather.temp",
      "Weather.humi",
      "Weather.rain_prob",
      "Weather.Dust",
      "zone_code"};

  state = mysql_query(connection_, query.c_str());
  int row_num = 0, field = 0;
  string s_zone_code[281];
  if (state == 0) {
    char zone_code[281][40];
    sql_result = mysql_store_result(connection_);
    row_num = mysql_num_rows(sql_result);
    field = mysql_num_fields(sql_result);
    logger->debug("row_num : {}", row_num);
    if (row_num == 0) {
      query = "select code from zone where zone like '";
      for (unsigned index = 0; index <= location_token_size; index++) {
        query += "%";
        query += location_token[index];
        query += "%";
      }
      query += "'";
      logger->debug("[re Get Zonecode Query] {}", query);
      int re_query_state = 0;
      re_query_state = mysql_query(connection_, query.c_str());
      if (re_query_state == 0) {
        sql_result = mysql_store_result(connection_);
        row_num = mysql_num_rows(sql_result);
        field = mysql_num_fields(sql_result);
        logger->debug("row_num : {}", row_num);
      } else logger->debug("sql re-query state fail : {}", state);
    }

    if (row_num == 0) {
      logger->debug<const char *>("No ZoneCode!!!");
    } else {
      logger->debug<const char *>("[Get ZoneCode] : ");
      while ((sql_row = mysql_fetch_row(sql_result)) != NULL) {
        for (int cnt = 0; cnt < field; cnt++) {
          snprintf(zone_code[cnt], sizeof(zone_code[cnt]), "%s", sql_row[cnt]);
          s_zone_code[cnt] = zone_code[cnt];
          logger->debug("{}", s_zone_code[cnt]);
        }
      }
    }
    mysql_free_result(sql_result);
  } else {
    logger->debug("sql query state fail : {}", state);
  }
  query = "select * from ";
  query += c.Get("db-database").c_str();
  query += ".weather where zone_code='";
  query += s_zone_code[0];
  query += "'";

  //Get Weather data
  if (row_num != 0) {
    if (slu1_request == "Weather.Info") {
      if (_date) {
        if ((value[2] == "오늘")
            || (value[2] == "지금")
            || (value[2] == "현재")
            || (value[2] == "현제")) {
          query +=
              " and date between now() + interval ";
          query +=
              loc_timediff_;
          query +=
              " hour and now()+interval 3 + ";
          query +=
              loc_timediff_;
          query +=
              " hour order by date desc limit 1";
        } else if ((value[2] == "내일")
            || (value[2] == "네일")) {
          query +=
              " and date between now()+interval 1 day + interval ";
          query +=
              loc_timediff_;
          query +=
              " hour and now()+interval 1 day + interval 3 + ";
          query +=
              loc_timediff_;
          query +=
              " hour order by date desc limit 1";
        } else if ((value[2] == "모레")
            || (value[2] == "모래")) {
          query +=
              " and date between now()+interval 2 day + interval ";
          query +=
              loc_timediff_;
          query +=
              " hour and now() + interval 2 day + interval 3 + ";
          query +=
              loc_timediff_;
          query +=
              " hour order by date desc limit 1";
        } else {
          query +=
              " and date between now() + interval ";
          query +=
              loc_timediff_;
          query +=
              " hour and now()+interval 3 + ";
          query +=
              loc_timediff_;
          query +=
              " hour order by date desc limit 1";
        }
      } else {
        query +=
            " and date between now() + interval ";
        query +=
            loc_timediff_;
        query +=
            " hour and now()+interval 3 + ";
        query +=
            loc_timediff_;
        query +=
            " hour order by date desc limit 1";
      }
    } else if ((slu1_request == "Weather.Rain")
        || (slu1_request == "Weather.Snow")
        || (slu1_request == "Weather.Humidity")
        || (slu1_request == "Weather.Temperature")) {
      if (_date) {
        if ((value[2] == "오늘")
            || (value[2] == "지금")
            || (value[2] == "현재")
            || (value[2] == "현제")) {
          query += "and date(date) = date(now()+ interval ";
          query +=
              loc_timediff_;
          query += " hour) order by date asc";
        } else if ((value[2] == "내일")
            || (value[2] == "네일")) {
          query += " and date(date) = date(now()+ interval 1 day + interval ";
          query +=
              loc_timediff_;
          query += " hour) order by date asc";
        } else if ((value[2] == "모레")
            || (value[2] == "모래")) {
          query += "and date(date) = date(now()+ interval 2 day + interval ";
          query +=
              loc_timediff_;
          query += " hour) order by date asc";
        } else {
          query += "and date(date) = date(now()+ interval ";
          query +=
              loc_timediff_;
          query += " hour) order by date asc";
        }
      } else {
        query +=
            " and date between now() + interval ";
        query +=
            loc_timediff_;
        query +=
            " hour and now()+interval 3 + ";
        query +=
            loc_timediff_;
        query +=
            " hour order by date desc limit 1";
      }
    }

    logger->debug("query state : {}", query.c_str());
    state = mysql_query(connection_, query.c_str());
    if (state == 0) {
      sql_result = mysql_store_result(connection_);
      row_num = mysql_num_rows(sql_result);
      field = mysql_num_fields(sql_result);
      logger->debug("row_num : {}", row_num);
      if (row_num != 0) {
        logger->debug<const char *>("original db result : ");
        int row_index = 0;
        while ((sql_row = mysql_fetch_row(sql_result)) != NULL) {
          for (int cnt = 0; cnt < field; cnt++) {
            snprintf(node_data[cnt],
                     sizeof(node_data[cnt]),
                     "%s",
                     sql_row[cnt]);
            s_node_data[row_index][cnt] = node_data[cnt];
          }
          row_index++;
        }
      }
      mysql_free_result(sql_result);
    } else {
      logger->debug("sql query state fail : {}", state);
      row_num = 0;
    }

    for (int i = 0; i < row_num; i++) {
      string row_data;
      for (int j = 0; j < 12; j++) {
        row_data += s_node_data[i][j];
        row_data += "  ";
      }
      logger->debug("[ROW {}] {}", i, row_data);
    }
  }

  map<string, string> result_slot;
  // insert result information
  if (row_num != 0) {
    if (slu1_request == "Weather.Info") {
      for (int cnt = 1; cnt < field - 1; cnt++) {
        if (s_node_data[0][cnt] != "(null)") {
          if (cnt != 5)
            entities.mutable_entity_data()->Add()
                ->mutable_entities()->insert(MapPair<string,
                                                     string>(column_name[cnt],
                                                             node_data[cnt]));
          else
            entities.mutable_entity_data()->Add()
                ->mutable_entities()->insert(MapPair<string,
                                                     string>("Weather.time",
                                                             node_data[cnt]));
        }
      }
      entities.mutable_entity_data()->Add()
          ->mutable_entities()->insert(MapPair<string,
                                               string>("Weather.Info",
                                                       "<Count>1</Count>"));
    } else if (slu1_request == "Weather.Rain") {
      unsigned int state_counter = 0;
      string state_time[8];
      for (int i = 0; i < row_num; i++) {
        if (s_node_data[i][6].find("비") != string::npos) {
          if (s_node_data[i][5].find(":") != string::npos) {
            state_time[state_counter] =
                s_node_data[i][5].substr(s_node_data[i][5].find(":") - 2, 2);
            if (stoi(state_time[state_counter]) > 12) {
              state_time[state_counter] =
                  "오후 " + to_string(stoi(state_time[state_counter]) - 12);
            } else if (stoi(state_time[state_counter]) < 12) {
              state_time[state_counter] =
                  "오전 " + to_string(stoi(state_time[state_counter]));
            } else if (state_time[state_counter] == "00") {
              state_time[state_counter] = "오전 0";
            } else {
              state_time[state_counter] =
                  "낮 " + to_string(stoi(state_time[state_counter]));
            }
          }
          state_counter++;
        }
      }
      logger->debug("[state_counter] {}", state_counter);
      string infotext;
      if (state_counter != 0) {
        logger->debug("[s_time] {}", state_time[0]);
        logger->debug("[e_time] {}", state_time[state_counter - 1]);
        infotext += state_time[0];
        if (state_counter == 1) {
          infotext += "시에 비 예보가 있습니다.";
        } else {
          infotext += "시부터 ";
          infotext += state_time[state_counter - 1];
          infotext += "시까지 비 예보가 있습니다.";
        }
      } else {
        logger->debug<const char *>("비 데이터 없음.");
        infotext += "비 예보가 없습니다";
      }
      entities.mutable_entity_data()->Add()
          ->mutable_entities()->insert(MapPair<string,
                                               string>("Weather.infotext",
                                                       infotext));
    } else if (slu1_request == "Weather.Snow") {
      unsigned int state_counter = 0;
      string state_time[8];
      for (int i = 0; i < row_num; i++) {
        if (s_node_data[i][6].find("눈") != string::npos) {
          if (s_node_data[i][5].find(":") != string::npos) {
            state_time[state_counter] =
                s_node_data[i][5].substr(s_node_data[i][5].find(":") - 2, 2);
            if (stoi(state_time[state_counter]) > 12) {
              state_time[state_counter] =
                  "오후 " + to_string(stoi(state_time[state_counter]) - 12);
            } else if (stoi(state_time[state_counter]) < 12) {
              state_time[state_counter] =
                  "오전 " + to_string(stoi(state_time[state_counter]));
            } else if (state_time[state_counter] == "00") {
              state_time[state_counter] = "오전 0";
            } else {
              state_time[state_counter] =
                  "낮 " + to_string(stoi(state_time[state_counter]));
            }
          }
          state_counter++;
        }
      }
      logger->debug("[state_counter] {}", state_counter);
      string infotext;
      if (state_counter != 0) {
        logger->debug("[s_time] {}", state_time[0]);
        logger->debug("[e_time] {}", state_time[state_counter - 1]);
        infotext += state_time[0];
        if (state_counter == 1) {
          infotext += "시에 눈 예보가 있습니다.";
        } else {
          infotext += "시부터 ";
          infotext += state_time[state_counter - 1];
          infotext += "시까지 눈 예보가 있습니다.";
        }
      } else {
        logger->debug<const char *>("눈 데이터 없음.");
        infotext += "눈 예보가 없습니다";
      }
      entities.mutable_entity_data()->Add()
          ->mutable_entities()->insert(MapPair<string,
                                               string>("Weather.infotext",
                                                       infotext));
    }
    entities.mutable_entity_data()->Add()
        ->mutable_entities()->insert(MapPair<string,
                                             string>(slu1_request,
                                                     "<Count>1</Count>"));
  } else {
    entities.mutable_entity_data()->Add()
        ->mutable_entities()->clear();
    entities.mutable_entity_data()->Add()
        ->mutable_entities()->insert(MapPair<string,
                                             string>("Weather.Info",
                                                     "<Count>0</Count>"));
  }

  // 처리된 결과 입력
  SdsUtter sdsUtter;
  ClientContext generate_ctx;
  Status
      generate_status = sdssImpl->Generate(&generate_ctx, entities, &sdsUtter);

  logger->debug("[System output] {}", sdsUtter.response());
  if (generate_status.ok()) {
    logger->debug("[System output] {}", sdsUtter.response());
    response->set_text(sdsUtter.response());
    return Status::OK;
  } else {
    logger->debug<const char *>("DM response Fail.");
    return Status::CANCELLED;
  }
}

Status AppWeatherAgent::Close(ServerContext *context,
                              const ::maum::m2u::da::TalkKey *key,
                              ::maum::m2u::da::TalkStat *stat) {
  auto logger = LOGGER();
  logger->debug("Closing for {} {}",
                key->session_id(), key->agent_key());
  stat->set_session_key(key->session_id());
  stat->set_agent_key(key->agent_key());

  // 해당 세션 및 대화에 대한 처리 결과 데이터를 상세히 정의할 수 없어서
  // 아래와 같이 처리한다.
  using pair = google::protobuf::Map<string, string>::value_type;
  auto meta = stat->mutable_meta();
  meta->insert(pair("name, value"));

  return Status::OK;
}
