#ifndef APP_WEATHER_AGENT_H
#define APP_WEATHER_AGENT_H

#include <maum/m2u/da/provider.grpc.pb.h>
#include <unordered_map>
#include <memory>
#include <maum/m2u/da/provider.pb.h>
#include <maum/brain/sds/resolver.grpc.pb.h>
#include <maum/brain/sds/sds.grpc.pb.h>
#include <maum/m2u/da/v1/talk.pb.h>
#include <maum/m2u/da/v1/talk.grpc.pb.h>
#include "mysql.h"
#include "dareg-client.h"

using grpc::ServerContext;
using google::protobuf::int64;
using grpc::Status;
using maum::m2u::da::v1::TalkRequest;
using maum::m2u::da::v1::TalkResponse;
using maum::brain::sds::SdsQuery;
using maum::brain::sds::SdsServiceResolver;


using std::unordered_map;
using std::shared_ptr;

class AppWeatherAgent : public maum::m2u::da::v1::DialogAgentProvider::Service {
 public:
  explicit AppWeatherAgent(const std::string &ip,
                           const std::string &port,
                           const std::string &maumd,
                           const std::string &resolver);
  virtual ~AppWeatherAgent();
  const char *Name() {
    return "MindsLab Weather Agent";
  }

  Status Talk(::grpc::ServerContext *context,
              const TalkRequest *request,
              TalkResponse *response) override;
  Status Close(ServerContext *context,
               const ::maum::m2u::da::TalkKey *request,
               ::maum::m2u::da::TalkStat *response) override;

  bool IsReady() {
    return client_.keys().size() > 0;
  }
 private:
  string ip_;
  string port_;
  DialogAgentInstancePoolClient client_;
  string sds_remote_;

  //ServiceLocation sl_;
  string loc_timediff_;

  MYSQL *connection_;

  std::unique_ptr<SdsServiceResolver::Stub> stub_sds_resolver_;
};

#endif
