cmake_minimum_required(VERSION 2.8.12)

project(da-internal-assistd CXX)
set(BINOUT da-internal-assistd)

msl_import_binary(kSDSAPI_DIR kSDSAPI_MADE "kSDSAPI"
    etri_dialog_api_20170627.tar.gz)

include_directories(../../../proto)
include_directories(../../../src/lib/dareg)
include_directories(${CMAKE_INSTALL_PREFIX}/include)

include_directories(/usr/include/mysql)
include_directories(${kSDSAPI_DIR})
link_directories(${kSDSAPI_DIR})
link_directories(/usr/lib64/mysql)
link_directories(${CMAKE_INSTALL_PREFIX}/lib)

add_definitions(
    -D_OPEN_API
    -ffloat-store
    -fpermissive
)

set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif ()

set(SOURCE_FILES
    main.cc
    finance-assistant.cc finance-assistant.h ${kSDSAPI_MADE}
    )

add_executable(${BINOUT} ${SOURCE_FILES})

#CFLAGS   += -Wno-write-strings
#CFLAGS   += -ffloat-store
# -fpermissive

target_link_libraries(${BINOUT}
    -l:dial.externalDB.korean.a
    EverIsam
    sqlite3
    curl
    pthread
    maum-common
    maum-json
    m2u-dareg
    m2u-all-pb grpc++ protobuf
    )

# install(TARGETS ${BINOUT} RUNTIME DESTINATION da)
