#ifndef FINANCE_ASSISTANT_H
#define FINANCE_ASSISTANT_H

#include <maum/m2u/da/provider.grpc.pb.h>

#include <unordered_map>
#include <memory>
#include <string>
#include <maum/m2u/da/v1/talk.pb.h>
#include <maum/m2u/da/v1/talk.grpc.pb.h>
using std::string;
#include "kSDSAPI.h"
#include "dareg-client.h"

using grpc::ServerContext;
using google::protobuf::int64;
using grpc::Status;
using maum::m2u::da::v1::TalkRequest;
using maum::m2u::da::v1::TalkResponse;

using std::unordered_map;
using std::shared_ptr;

struct DialogInfo {
  std::string user_info_confirm_state;
  std::string ex_dialog_state;
};

class FinanceAssistant : public maum::m2u::da::v1::DialogAgentProvider::Service {
 public:
  explicit FinanceAssistant(const std::string &ip,
                            const std::string &port,
                            const std::string &maumd);
  virtual ~FinanceAssistant();
  const char *Name() {
    return "MindsLab Finance Assistant";
  }

  Status Talk(::grpc::ServerContext *context,
              const TalkRequest *request,
              TalkResponse *response) override;
  Status Close(ServerContext *context,
               const ::maum::m2u::da::TalkKey *request,
               ::maum::m2u::da::TalkStat *response) override;

  bool IsReady() {
    return client_.keys().size() > 0;
  }
 private:
  string ip_;
  string port_;
  unsigned int user_;
  DialogAgentInstancePoolClient client_;

  char dic_path_[1024 * 2] = {0};
  char domain_[1024 * 2] = {0};

  unordered_map<int64, DialogSession *> session_dialog_map_;
  unordered_map<int64, DialogInfo *> dialog_info_map_;
};

#endif
