#include <iostream>

#include <grpc++/grpc++.h>

#include "finance-assistant.h"
#include <memory>
#include <csignal>

#include <libmaum/common/config.h>

using grpc::Server;
using grpc::ServerBuilder;

using namespace std;
using std::shared_ptr;

shared_ptr<FinanceAssistant> globAgent;

volatile std::sig_atomic_t g_signal_status;

void HandleSignal(int signal) {
  // CALL DESTRUCTOR
  LOGGER()->info("handling {}", signal);
  g_signal_status = signal;
  globAgent.reset();
  exit(0);
}

void RunServer() {
  libmaum::Config &c = libmaum::Config::Instance();
  c.DumpPid();
  auto logger = LOGGER();

  std::string server_address(c.Get("listen"));

  string remote = c.Get("frontd.export.ip");
  remote += ':';
  remote += c.Get("frontd.export.port");

  auto agent = std::make_shared<FinanceAssistant>(c.Get("host"),
                                                  c.Get("port"),
                                                  remote);

  if (agent->IsReady()) {
    globAgent = agent;

    ServerBuilder builder;
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(agent.get());

    unique_ptr<Server> server(builder.BuildAndStart());
    if (server) {
      logger->info("Server listening on {} with {}",
                   server_address,
                   agent->Name());
      agent.reset();
      server->Wait();
      agent.reset();
      server->Wait();
    }
  } else {
    logger->warn<const char *>("Cannot register service!");
  }
}

int main(int argc, char *argv[]) {
  std::signal(SIGINT, HandleSignal);
  //std::signal(SIGSEGV, HandleSignal);
  //std::signal(SIGBUS, HandleSignal);
  std::signal(SIGTERM, HandleSignal);

  libmaum::Config::Init(argc, argv, "maum.conf");

  RunServer();
}

