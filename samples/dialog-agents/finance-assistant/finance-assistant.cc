#include "finance-assistant.h"

#include <iostream>
#include <sstream>

#include <grpc++/grpc++.h>
#include <maum/m2u/server/pool.grpc.pb.h>

#include <libmaum/common/encoding.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

using namespace std;
using grpc::Channel;
using google::protobuf::Map;
using maum::m2u::da::DialogSessionState;

FinanceAssistant::FinanceAssistant(const string &ip, const string &port,
                                   const string &maumd)
    : ip_(ip),
      port_(port),
      client_(grpc::CreateChannel(maumd, grpc::InsecureChannelCredentials())) {
  auto &c = libmaum::Config::Instance();

  DialogAgentInstanceResource res;
  res.set_name("financeAssistant");
  res.set_description("MindsLab Finance Assistant Version 0.1");
  res.set_version("0.1");
  res.set_server_ip(ip_);
  res.set_server_port(std::stoi(port_));

  Json::Value &root = c.GetJson();
  Json::Value &regs = root["register"];
  for (auto &reg: regs) {
    res.set_chatbot(reg["svc-group"].asString());
    res.set_skill(reg["domain"].asString());
    res.set_lang(ToLangCode(reg["lang"].asString()));
    client_.Register(res);
  }

  //time zone set
  setenv("TZ", "KST+9", 1);
  tzset();

  //
  // CONSTRUCTOR CODE for Stock
  //

  auto logger = LOGGER();
  string dm_path = c.Get("dm-path");
  string dm_domain = c.Get("dm-domain");
  strncpy(dic_path_, dm_path.c_str(), dm_path.length());
  strncpy(domain_, dm_domain.c_str(), dm_domain.length());

  // init system : 대화시스템 지식 로딩 및 초기화 : 사전 path와 구동하는 응용 도메인 이름 부여

  if (!open_DialogSystemKB(dic_path_, domain_)) {
    logger->error<const char *>("Error : open KB");
  }
}

FinanceAssistant::~FinanceAssistant() {
  client_.Unregister();
  //
  // DESTRUCTOR CODE for Stock
  //

  close_DialogSystemKB(); // 대화시스템 전체 사전 release
}

Status FinanceAssistant::Talk(::grpc::ServerContext *context,
                              const TalkRequest *request,
                              TalkResponse *response) {
  auto logger = LOGGER();

  //loan_volumne, contract_date, contract_volume
  string user_info[8][3] = {
      {"1,200만", "1,500만", "3.3%"},
      {"500만", "800만", "3.6%"},
      {"1,000만", "1,250만", "3.5%"},
      {"1,500만", "1,875만", "3.1%"},
      {"1,700만", "2,250만", "2.9%"},
      {"1,750만", "2,300만", "3.1%"},
      {"1,234만", "1,650만", "3.7%"},
      {"2,500만", "4,000만", "2.5%"}
  };

  logger->debug("Sesstion ID : {}", request->session_id());

  auto got = session_dialog_map_.find(request->session_id());

  DialogSession *dialog = nullptr;
  DialogInfo *dialogInfo;
  if (got != session_dialog_map_.end()) {
    dialog = got->second;
    auto info_got = dialog_info_map_.find(request->session_id());
    if (info_got != dialog_info_map_.end()) {
      dialogInfo = info_got->second;
    }
  } else {
    // 새로운 DialogSession 생성
    if (request->turn() == 0 && request->text() == "네") {
      response->set_state(DialogSessionState::DIAG_DISCARD);
      return Status::OK;
    }
    dialog = new DialogSession;
    char set_information[1] = {0};
    int rc = dialog->open(domain_, set_information);
    if (!rc) {
      logger->error("Cannot open dialog session {}",
                    dialog->get_error_message());
      delete dialog;
      return Status(grpc::StatusCode::INTERNAL, "Cannot open dialog session");
    }
    user_ = request->session_id() % 8;
    dialogInfo = new DialogInfo;
    dialogInfo->user_info_confirm_state = "false";
    dialogInfo->ex_dialog_state = "";

    dialog_info_map_.insert(std::make_pair(request->session_id(), dialogInfo));
    // 생성된 DialogSession 을 session_id와 매핑하여 등록
    session_dialog_map_.insert(std::make_pair(request->session_id(), dialog));
  }
  logger->debug("[user_info_confirm_state_] {}",
                dialogInfo->user_info_confirm_state);
  logger->debug("[ex_dialog_state_] {}", dialogInfo->ex_dialog_state);
  logger->debug("[User Num] {}", user_);

  char slu_result[1024 * 4] = {0};

  string sel_text = request->text();

  logger->debug("[QUESTION] {}", sel_text);
  if (dialogInfo->ex_dialog_state == "request_insurance_loan()"
      || dialogInfo->ex_dialog_state == "request_revive()"
      || dialogInfo->ex_dialog_state == "request_cancel()") {
    if (dialogInfo->user_info_confirm_state == "false") {
      sel_text += "이요";
      logger->debug("[Change name pattern] {}", sel_text);
    }
  }

  if (dialogInfo->ex_dialog_state == "replace_info") {
    logger->debug("[sel_text_length] {}", sel_text.length());
    if (sel_text.length() == 6
        || sel_text.length() == 9
        || sel_text.length() == 12) {
      sel_text += "이요";
    }
  }

  if (!dialog->dialog_SLU(Utf8ToEuckr(sel_text).c_str())) {
    logger->error("Dialog SLU Error: {}",
                  EuckrToUtf8(dialog->get_error_message()));
    return Status::CANCELLED;
  }

  // dialog에서 처리한 slu 결과를 가져오기
  strcpy(slu_result, EuckrToUtf8(dialog->get_user_dialog_act()).c_str());
  logger->debug("[SLU] {}", slu_result);

  string::size_type s_pos, e_pos, m_pos;

  string slu = EuckrToUtf8(dialog->get_user_dialog_act());
  string slu1, slu1_request, slu1_condition;

  logger->debug("[original SLU Set]\n{}", slu);

  s_pos = slu.find("unknown");
  if (s_pos != string::npos) {
    logger->debug<const char *>("SDS output SLU = Unknown");
    response->set_text("제가 잘 모르는 표현입니다. 다시 말씀해주세요.");
    return Status::OK;
  }
  else {
    s_pos = slu.find("SLU1");
    slu1 = slu.substr(s_pos);
    e_pos = slu1.find("\n");
    slu1 = slu1.substr(0, e_pos);
    logger->debug("[Best SLU by SDS] : {}", slu1);

    s_pos = slu1.find_first_of("#");
    e_pos = slu1.find_first_of(")");

    slu1_request = slu1.substr(s_pos + 1, e_pos - s_pos);

    s_pos = slu1.find_first_of("(");
    e_pos = slu1.find_first_of(")", s_pos);
    dialogInfo->ex_dialog_state = slu1_request;
  }
  logger->debug("[SLU REQUEST] {}", slu1_request);

  if (dialogInfo->ex_dialog_state == "confirm_user_info"
      || slu1_request == "affirm()") {
    dialogInfo->user_info_confirm_state = "true";
    dialogInfo->ex_dialog_state = "progress";
  }
  else if (dialogInfo->ex_dialog_state == "confirm_user_info"
      || slu1_request == "negate()") {
    dialogInfo->user_info_confirm_state = "false";
    dialogInfo->ex_dialog_state = "replace_info";
  }

  if (slu1_request == "faq()") {
    logger->debug<const char *>("FAQ processing");
    response->set_state(DialogSessionState::DIAG_CANNOT_UNDERSTAND);
    return Status::OK;
  }

  string c_text, slu_query;
  // DB request string
  if (!dialog->get_db_request().empty()) {
    logger->debug("[Query Condition] {}", dialog->get_db_request());
    slu_query = dialog->m_db_request;
  }
  // tokenize Condition
  string slot[10], value[10];
  unsigned int slot_counter = 0;
  if (!dialog->get_db_request().empty()) {
    s_pos = slu_query.find("condition");
    e_pos = slu_query.rfind("condition");

    c_text = slu_query.substr((s_pos + 10), (e_pos - s_pos - 12));
    logger->debug("tokenized condition text : {}", c_text);
    if (c_text.empty()) {
      logger->debug<const char *>(
          "No condition slot, Do not processing tokenizer");
    } else {
      s_pos = 0;
      e_pos = c_text.find_first_of(",");

      if (e_pos == string::npos) {
        e_pos = c_text.rfind("\"") + 1;
        slot[slot_counter] = c_text.substr(s_pos, (e_pos - s_pos));
        m_pos = slot[slot_counter].find_first_of("=");
        value[slot_counter] = slot[slot_counter].substr(m_pos + 2,
                                                        (slot[slot_counter].length()
                                                            - m_pos - 3));
        slot[slot_counter] = slot[slot_counter].substr(0, m_pos);
        logger->debug("slot {} : {} / value {} : {}",
                      slot_counter,
                      slot[slot_counter],
                      slot_counter,
                      value[slot_counter]);
        slot_counter++;
        e_pos = string::npos;
      }

      while (e_pos != string::npos) {
        slot[slot_counter] = c_text.substr(s_pos, (e_pos - s_pos));
        m_pos = slot[slot_counter].find_first_of("=");
        value[slot_counter] = slot[slot_counter].substr(m_pos + 2,
                                                        (slot[slot_counter].length()
                                                            - m_pos - 3));
        slot[slot_counter] = slot[slot_counter].substr(0, m_pos);
        logger->debug("slot {} : {} / value {} : {}",
                      slot_counter,
                      slot[slot_counter],
                      slot_counter,
                      value[slot_counter]);
        slot_counter++;
        s_pos = e_pos + 1;
        e_pos = c_text.find_first_of(",", s_pos + 1);

        if (e_pos == string::npos) {
          slot[slot_counter] = c_text.substr(s_pos, (c_text.length() - s_pos));
          m_pos = slot[slot_counter].find_first_of("=");
          value[slot_counter] = slot[slot_counter].substr(m_pos + 2,
                                                          (slot[slot_counter].length()
                                                              - m_pos - 3));
          slot[slot_counter] = slot[slot_counter].substr(0, m_pos);
          logger->debug("slot {} : {} / value {} : {}",
                        slot_counter,
                        slot[slot_counter],
                        slot_counter,
                        value[slot_counter]);
          slot_counter++;
        }
      }
    }
  }

  string db_result;
  if (!dialog->get_db_request().empty()) {
    db_result += "<node>";
    for (unsigned index = 0; index < slot_counter; index++) {
      db_result += "<";
      db_result += slot[index];
      db_result += ">";

      db_result += value[index];

      db_result += "</";
      db_result += slot[index];
      db_result += ">";
    }
    if (slu1_request == "request(loan_volume)") {
      db_result += "<loan_volume>";
      db_result += user_info[user_][0];
      db_result += "</loan_volume>";
    }
    else if (slu1_request == "request(contract_volume)") {
      struct tm newtime;
      time_t ltime;
      char buf[50];

      ltime = time(&ltime);

      localtime_r(&ltime, &newtime);

      snprintf(buf, sizeof buf, "%d년 %d월 %d일", newtime.tm_year + 1900,
               newtime.tm_mon + 1, newtime.tm_mday);
      logger->debug("[Local Time] {}", buf);

      db_result += "<contract_date>";
      db_result += buf;
      db_result += "</contract_date>";
      db_result += "<contract_volume>";
      db_result += user_info[user_][1];
      db_result += "</contract_volume>";
    }
    else if (slu1_request == "request(loan_interest)") {
      db_result += "<loan_interest>";
      db_result += user_info[user_][2];
      db_result += "</loan_interest>";
    }
    db_result += "</node>";
  }
  else {
    db_result = "";
  }

  logger->debug("[DB_RESULT] {}", db_result);

  if (!dialog->m_db_request.empty()) {
    logger->debug("db result [external DB searching] {}", db_result);
  }

  // set dialog dm
  if (!dialog->dialog_DM(db_result.c_str())) {
    logger->error("Dialog DM Error: {}",
                  EuckrToUtf8(dialog->get_error_message()));
    return Status::CANCELLED;
  }

  string output = EuckrToUtf8(dialog->get_system_response());

  if (output.find("맞으십니까?") != string::npos) {
    dialogInfo->ex_dialog_state = "confirm_user_info";
  }

  s_pos = output.find("[END]");
  if (s_pos == string::npos) {
    logger->debug<const char *>(" END 아님 ");
  } else {
    logger->debug<const char *>("END!!");
    output = output.substr(0, s_pos);
  }
  // dialog에서 처리한 시스템 발화를 가져오기
  logger->debug("[System output] {}", output);
  response->set_text(output);

  if (s_pos != string::npos) {
    dialog->close();
    response->set_state(DialogSessionState::DIAG_CLOSED);
    auto temp = session_dialog_map_.find(request->session_id());
    if (temp != session_dialog_map_.end()) {
      session_dialog_map_.erase(temp);
      delete dialog;
    }

    auto tempInfo = dialog_info_map_.find(request->session_id());
    if (tempInfo != dialog_info_map_.end()) {
      dialog_info_map_.erase(tempInfo);
      delete dialogInfo;
    }
  }
  return Status::OK;
}

Status FinanceAssistant::Close(ServerContext *context,
                               const ::maum::m2u::da::TalkKey *key,
                               ::maum::m2u::da::TalkStat *stat) {
  auto logger = LOGGER();
  logger->debug("Closing for {} {}",
                key->session_id(), key->agent_key());
  stat->set_session_key(key->session_id());
  stat->set_agent_key(key->agent_key());

  // 해당 세션 및 대화에 대한 처리 결과 데이터를 상세히 정의할 수 없어서
  // 아래와 같이 처리한다.
  using pair = google::protobuf::Map<string, string>::value_type;
  auto meta = stat->mutable_meta();
  meta->insert(pair("name, value"));

  return Status::OK;
}
