cmake_minimum_required(VERSION 2.8.12)

project(da-faqd CXX)
set(BINOUT da-faqd)

include_directories(../../../proto)
include_directories(../../../src/lib/dareg)
include_directories(${CMAKE_INSTALL_PREFIX}/include)

include_directories(/usr/include/mysql)
link_directories(/usr/lib64/mysql)
link_directories(${CMAKE_INSTALL_PREFIX}/lib)

add_definitions(
    -D_OPEN_API
    -ffloat-store
    -fpermissive
    )

set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif ()

set(SOURCE_FILES
    main.cc
    faq-agent.cc faq-agent.h
    )

add_executable(${BINOUT} ${SOURCE_FILES})

#CFLAGS   += -Wno-write-strings
#CFLAGS   += -ffloat-store
# -fpermissive

target_link_libraries(${BINOUT}
    curl
    mysqlclient
    pthread
    maum-common
    m2u-dareg
    m2u-all-pb grpc++ protobuf
    )

install(TARGETS ${BINOUT} RUNTIME DESTINATION da)
