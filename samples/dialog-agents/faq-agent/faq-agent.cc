#include "faq-agent.h"
#include <libmaum/common/util.h>
#include <libmaum/common/config.h>

using namespace std;
using grpc::Channel;
using maum::m2u::da::DialogSessionState;

FaqAgent::FaqAgent(const string &ip, const string &port)
    : ip_(ip),
      port_(port) {
  auto logger = LOGGER();

  // DA 정보
  provider_ = make_shared<DialogAgentProviderParam>();

  provider_->set_name("faq");
  provider_->set_description("MindsLab FAQ Agent");
  provider_->set_version("0.2");
  provider_->set_single_turn(true);
  provider_->set_agent_kind(AgentKind::AGENT_SDS);
  provider_->set_require_user_privacy(false);

  // Runtime 정보
  runtime_ = make_shared<RuntimeParameterList>();

  auto db_host = runtime_->mutable_params()->Add();
  db_host->set_name("db_host");
  db_host->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_host->set_desc("Database IP");
  db_host->set_default_value("10.122.64.66");
  db_host->set_required(true);

  auto db_port = runtime_->mutable_params()->Add();
  db_port->set_name("db_port");
  db_port->set_type(maum::m2u::common::DataType::DATA_TYPE_INT);
  db_port->set_desc("Database Port");
  db_port->set_default_value("3306");
  db_port->set_required(true);

  auto db_user = runtime_->mutable_params()->Add();
  db_user->set_name("db_user");
  db_user->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_user->set_desc("Database Username");
  db_user->set_default_value("root");
  db_user->set_required(true);

  auto db_pwd = runtime_->mutable_params()->Add();
  db_pwd->set_name("db_pwd");
  db_pwd->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_pwd->set_desc("Database Password");
  db_pwd->set_default_value("root");
  db_pwd->set_required(true);

  auto db_database = runtime_->mutable_params()->Add();
  db_database->set_name("db_database");
  db_database->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_database->set_desc("Database DB");
  db_database->set_default_value("lg");
  db_database->set_required(true);

  auto db_table = runtime_->mutable_params()->Add();
  db_table->set_name("db_table");
  db_table->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  db_table->set_desc("Database Table");
  db_table->set_default_value("aircon");
  db_table->set_required(true);

  status_ = DialogAgentState::DIAG_STATE_IDLE;
}

FaqAgent::~FaqAgent() {
  mysql_close(connection_);

  //
  // DESTRUCTOR CODE for WEATHER
  //

  //dialog_.close();
}

int isnewline(int c) {
  return c == '\n' ? 1 : 0;
}
int isequal(int c) {
  if (isspace(c)) {
    return 1;
  }
  return c == '=' ? 1 : 0;
}

Status FaqAgent::Talk(::grpc::ServerContext *context,
                      const TalkRequest *request,
                      TalkResponse *response) {
  // 이 대화 에이전트는 현재 상태를 무조건 종료한다.
  response->set_state(DialogSessionState::DIAG_CLOSED);

  auto logger = LOGGER();
  string db_name = db_database_;
  string db_table_name = db_table_;

  MYSQL_RES *sql_result;
  MYSQL_ROW sql_row;

  string intention_text;
  string sel_text = request->text();
  if (!request->intent().empty())
    intention_text = "'" + request->intent() + "'";
  else
    intention_text = "구체적으로 다시 한번 질문해 주세요";
  logger->debug("intention text = {}", intention_text);

  logger->debug("[QUESTION] {}", sel_text);

  connection_ = mysql_init(NULL);
  if (mysql_real_connect(connection_,
                         db_host_.c_str(),
                         db_user_.c_str(),
                         db_pwd_.c_str(),
                         db_database_.c_str(),
                         db_port_,
                         NULL,
                         0) == NULL) {
    logger->debug("에러 : {} => {}",
                  mysql_errno(connection_),
                  mysql_error(connection_));
  } else {
    logger->debug<const char *>("연결 성공");
  }
  // DB character_set
  if (!mysql_set_character_set(connection_, "utf8")) {
    logger->debug("New client character set: {}",
                  mysql_character_set_name(connection_));

  }
  // 데이터베이스 선택
  if (mysql_select_db(connection_, db_name.c_str())) {
    logger->debug("에러 : {}, {}",
                  mysql_errno(connection_),
                  mysql_error(connection_));
    //return NULL;
  }
  string query =
      "select * from " + db_name + "." + db_table_name + " where category = ";
  query += intention_text;

  int state = 0;

  logger->debug("query : {}", query);

  state = mysql_query(connection_, query.c_str());

  int row_num = 0, cnt = 0, field = 0;

  char node_data[12][4000];
  string s_node_data[3];
  if (state == 0) {
    sql_result = mysql_store_result(connection_);
    row_num = mysql_num_rows(sql_result);
    field = mysql_num_fields(sql_result);
    logger->debug("row_num : {}", row_num);
    logger->debug("field : {}", field);

    if (row_num != 0) {
      logger->debug<const char *>("\noriginal db result : ");
      while ((sql_row = mysql_fetch_row(sql_result))
          != NULL)     // Result Set 에서 1개씩 배열을 가져옴.
      {
        for (cnt = 0; cnt < field; cnt++) {
          logger->debug("{}]\t", sql_row[cnt]);
          snprintf(node_data[cnt], sizeof(node_data[cnt]), "%s", sql_row[cnt]);
          s_node_data[cnt] = node_data[cnt];
        }
        printf("\n\n");
      }
      mysql_free_result(sql_result);
    }
  } else {
    logger->debug("sql query state fail : {}", state);
  }

  if (!request->intent().empty()) {
    logger->debug("[System output] {}", node_data[1]);
    response->set_text(node_data[1]);
    if (request->access_from() != maum::m2u::facade::AccessFrom::SPEAKER) {
      using namespace google::protobuf;
      using mp = google::protobuf::Map<string, string>::value_type;
      auto meta_map = response->mutable_meta();
      auto lines = split(s_node_data[2], isnewline);
      for (auto s: lines) {
        auto kv = split(s, isequal);
        meta_map->insert(mp(kv[0], kv[1]));
      }
    }
  } else {
    logger->debug("[System output] {}", intention_text);
    response->set_text(intention_text);
  }
  mysql_close(connection_);

  return Status::OK;
}

::grpc::Status FaqAgent::Close(::grpc::ServerContext *context,
                               const ::maum::m2u::da::TalkKey *key,
                               ::maum::m2u::da::TalkStat *stat) {
  auto logger = LOGGER();
  logger->debug("Closing for {} {}",
                key->session_id(), key->agent_key());
  stat->set_session_key(key->session_id());
  stat->set_agent_key(key->agent_key());

  // 해당 세션 및 대화에 대한 처리 결과 데이터를 상세히 정의할 수 없어서
  // 아래와 같이 처리한다.
  using pair = google::protobuf::Map<string, string>::value_type;
  auto meta = stat->mutable_meta();
  meta->insert(pair("name, value"));

  return Status::OK;
}

Status FaqAgent::Init(::grpc::ServerContext *context,
                      const ::maum::m2u::da::InitParameter *request,
                      ::maum::m2u::da::DialogAgentProviderParam *response) {
  auto logger = LOGGER();
  logger->debug<const char *>("Faq Init Function");
  status_ = DialogAgentState::DIAG_STATE_INITIALIZING;
  db_host_ = request->params().at("db_host");
  db_port_ = std::stoi(request->params().at("db_port"));
  db_user_ = request->params().at("db_user");
  db_pwd_ = request->params().at("db_pwd");
  db_database_ = request->params().at("db_database");
  db_table_ = request->params().at("db_table");

  response->CopyFrom(*provider_);
  status_ = DialogAgentState::DIAG_STATE_RUNNING;
  return Status::OK;
}

Status FaqAgent::GetProviderParameter(::grpc::ServerContext *context,
                                      const ::google::protobuf::Empty *request,
                                      ::maum::m2u::da::DialogAgentProviderParam *response) {

  response->CopyFrom(*provider_);
  return Status::OK;
}

Status FaqAgent::IsReady(::grpc::ServerContext *context,
                         const ::google::protobuf::Empty *request,
                         ::maum::m2u::da::DialogAgentStatus *response) {
  response->set_state(status_); // 현재 DA status를 반환
  return Status::OK;
}

Status FaqAgent::GetRuntimeParameters(::grpc::ServerContext *context,
                                      const ::google::protobuf::Empty *request,
                                      ::maum::m2u::da::RuntimeParameterList *response) {
  response->CopyFrom(*runtime_);
  return Status::OK;
}

Status FaqAgent::GetUserAttributes(::grpc::ServerContext *context,
                                   const ::google::protobuf::Empty *request,
                                   ::maum::m2u::common::UserAttributeList *response) {
  shared_ptr<UserAttributeList> attr = make_shared<UserAttributeList>();
  response->CopyFrom(*attr);
  return Status::OK;
}

Status FaqAgent::Terminate(::grpc::ServerContext *context,
                           const ::google::protobuf::Empty *request,
                           ::google::protobuf::Empty *response) {
  status_ = DialogAgentState::DIAG_STATE_TERMINATED;
  return Status::OK;
}
