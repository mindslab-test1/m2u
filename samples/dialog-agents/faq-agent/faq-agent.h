#ifndef FAQ_AGENT_H
#define FAQ_AGENT_H

#include <maum/m2u/da/provider.grpc.pb.h>
#include <maum/m2u/da/v1/talk.grpc.pb.h>

#include <unordered_map>
#include <memory>
#include <string>
#include "mysql.h"
#include "dareg-client.h"

using std::string;
using grpc::ServerContext;
using google::protobuf::int64;
using grpc::Status;
using maum::m2u::da::v1::TalkRequest;
using maum::m2u::da::v1::TalkResponse;

using maum::m2u::da::DialogAgentState;
using maum::m2u::da::DialogAgentProviderParam;
using maum::m2u::da::RuntimeParameterList;
using maum::m2u::da::RuntimeParameter;
using maum::m2u::da::AgentKind;
using maum::m2u::facade::UserAttributeList;
using maum::m2u::facade::UserAttribute;
using maum::m2u::facade::DataType;

using std::unordered_map;
using std::shared_ptr;

class FaqAgent : public maum::m2u::da::v1::DialogAgentProvider::Service {
 public:
  explicit FaqAgent(const std::string &ip,
                    const std::string &port);
  virtual ~FaqAgent();
  const char *Name() {
    return "MindsLab FAQ Agent";
  }

  Status Talk(::grpc::ServerContext *context,
              const TalkRequest *request,
              TalkResponse *response) override;
  Status Close(ServerContext *context,
               const ::maum::m2u::da::TalkKey *request,
               ::maum::m2u::da::TalkStat *response) override;
  Status Init(::grpc::ServerContext *context,
              const ::maum::m2u::da::InitParameter *request,
              ::maum::m2u::da::DialogAgentProviderParam *response) override;
  Status GetProviderParameter(::grpc::ServerContext *context,
                              const ::google::protobuf::Empty *request,
                              ::maum::m2u::da::DialogAgentProviderParam *response) override;
  Status IsReady(::grpc::ServerContext *context,
                 const ::google::protobuf::Empty *request,
                 ::maum::m2u::da::DialogAgentStatus *response) override;
  Status GetRuntimeParameters(::grpc::ServerContext *context,
                              const ::google::protobuf::Empty *request,
                              ::maum::m2u::da::RuntimeParameterList *response) override;
  Status GetUserAttributes(::grpc::ServerContext *context,
                           const ::google::protobuf::Empty *request,
                           ::maum::m2u::common::UserAttributeList *response) override;
  Status Terminate(::grpc::ServerContext *context,
                   const ::google::protobuf::Empty *request,
                   ::google::protobuf::Empty *response) override;
 private:
  string ip_;
  string port_;
  MYSQL *connection_;

  DialogAgentState status_;
  shared_ptr<DialogAgentProviderParam> provider_;
  shared_ptr<RuntimeParameterList> runtime_;

  string db_host_;
  int db_port_;
  string db_user_;
  string db_pwd_;
  string db_database_;
  string db_table_;
};

#endif
