#include "chatbot-agent.h"

#include <sstream>
#include <iostream>
#include <libmaum/common/config.h>

#include <curl/curl.h>

using namespace std;
using grpc::Channel;
using maum::m2u::da::DialogSessionState;
using ::maum::m2u::common::UserAttributeList;

ChatbotAgent::ChatbotAgent(const string &ip, const string &port)
    : ip_(ip),
      port_(port) {
  // DA 정보
  provider_ = make_shared<DialogAgentProviderParam>();

  provider_->set_name("chatbot");
  provider_->set_description("Chatbot Agent");
  provider_->set_version("0.2");
  provider_->set_single_turn(false);
  provider_->set_agent_kind(AgentKind::AGENT_SDS);
  provider_->set_require_user_privacy(false);

  // Runtime 정보
  runtime_ = make_shared<RuntimeParameterList>();

  auto url = runtime_->mutable_params()->Add();
  url->set_name("url");
  url->set_type(maum::m2u::common::DataType::DATA_TYPE_STRING);
  url->set_desc("URL");
  url->set_default_value("http://ivoc.mindslab.ai");
  url->set_required(true);

  auto url_port = runtime_->mutable_params()->Add();
  url_port->set_name("url_port");
  url_port->set_type(maum::m2u::common::DataType::DATA_TYPE_INT);
  url_port->set_desc("URL PORT");
  url_port->set_default_value("9000");
  url_port->set_required(true);


  status_ = DialogAgentState::DIAG_STATE_IDLE;

  // CURL 초기화
  curl_global_init(CURL_GLOBAL_DEFAULT);
}

ChatbotAgent::~ChatbotAgent() {

  curl_global_cleanup();
}

uint Chatbot_WriteCb(char *in, uint size, uint nmemb, void *out) {
  LOGGER()->debug("in = [{}], size {}, nmemb {}", in, size, nmemb);
  std::string *body = reinterpret_cast<std::string *>(out);
  body->append(string(in, size * nmemb));
  return size * nmemb;
}

std::vector<std::string> SplitString(const std::string &str,
                                     const std::string &delimiter) {
  std::vector<std::string> strings;

  std::string::size_type pos = 0;
  std::string::size_type prev = 0;
  while ((pos = str.find(delimiter, prev)) != std::string::npos) {
    strings.push_back(str.substr(prev, pos - prev));
    prev = pos + 1;
  }

  // To get the last substring (or only, if delimiter is not found)
  strings.push_back(str.substr(prev));
  return strings;
}

Status ChatbotAgent::Talk(::grpc::ServerContext *context,
                          const TalkRequest *request,
                          TalkResponse *response) {
  auto logger = LOGGER();
  logger->debug("domain : {}", request->skill());
  logger->debug("intention : {}", request->intent());

  // 1회성 대화 이므로 대화 이후 바로 종료하고 상태를 유지하지 않는다.
  response->set_state(DialogSessionState::DIAG_CLOSED);

  CURL *curl;
  char err_buf[CURL_ERROR_SIZE];

  std::string body;

  curl = curl_easy_init();
  if (curl) {
    char *temp = curl_easy_escape(curl,
                                  request->text().c_str(),
                                  (int) request->text().size());
    string url(url_);
    url += ":";
    url += url_port_;
    url += "/?query=";
    url += temp;
    url += "&beam=5";
    curl_free(temp);

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buf);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, Chatbot_WriteCb);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &body);

    int err = curl_easy_perform(curl);
    if (!err) {
      std::vector<std::string> best_replies = SplitString(body, "\n");
      for (const auto &s : best_replies) {
        logger->debug("replies: {}", s);
      }

      response->set_text(best_replies[0]);
      return Status::OK;
    } else {
      return Status(grpc::StatusCode::INTERNAL, "curl perform failed");
    }
  } else {
    return Status(grpc::StatusCode::INTERNAL, "curl init failed");
  }
}

Status ChatbotAgent::Close(ServerContext *context,
                           const ::maum::m2u::da::TalkKey *key,
                           ::maum::m2u::da::TalkStat *stat) {
  auto logger = LOGGER();
  logger->debug("Closing for {} {}",
                key->session_id(), key->agent_key());
  stat->set_session_key(key->session_id());
  stat->set_agent_key(key->agent_key());

  // 해당 세션 및 대화에 대한 처리 결과 데이터를 상세히 정의할 수 없어서
  // 아래와 같이 처리한다.
  using pair = google::protobuf::Map<string, string>::value_type;
  auto meta = stat->mutable_meta();
  meta->insert(pair("name, value"));

  return Status::OK;
}


Status ChatbotAgent::Init(::grpc::ServerContext *context,
                          const ::maum::m2u::da::InitParameter *init_param,
                          ::maum::m2u::da::DialogAgentProviderParam *response) {
  LOGGER()->debug<const char *>("Chatbot Init Function");
  status_ = DialogAgentState::DIAG_STATE_INITIALIZING;

  auto logger = LOGGER();
  url_ = init_param->params().at("url");
  url_port_ = init_param->params().at("url_port");

  response->CopyFrom(*provider_);
  status_ = DialogAgentState::DIAG_STATE_RUNNING;
  return Status::OK;
}

Status ChatbotAgent::GetProviderParameter(::grpc::ServerContext *context,
                                          const ::google::protobuf::Empty *request,
                                          ::maum::m2u::da::DialogAgentProviderParam *response) {
  response->CopyFrom(*provider_);
  return Status::OK;
}

Status ChatbotAgent::IsReady(::grpc::ServerContext *context,
                             const ::google::protobuf::Empty *request,
                             ::maum::m2u::da::DialogAgentStatus *response) {
  response->set_state(status_); // 현재 DA status를 반환
  return Status::OK;
}

Status ChatbotAgent::GetRuntimeParameters(::grpc::ServerContext *context,
                                          const ::google::protobuf::Empty *request,
                                          ::maum::m2u::da::RuntimeParameterList *response) {
  response->CopyFrom(*runtime_);
  return Status::OK;
}

Status ChatbotAgent::GetUserAttributes(::grpc::ServerContext *context,
                                       const ::google::protobuf::Empty *request,
                                       UserAttributeList *response) {
  shared_ptr<UserAttributeList> attr = make_shared<UserAttributeList>();
  response->CopyFrom(*attr);
  return Status::OK;
}

Status ChatbotAgent::Terminate(::grpc::ServerContext *context,
                               const ::google::protobuf::Empty *request,
                               ::google::protobuf::Empty *response) {
  status_ = DialogAgentState::DIAG_STATE_TERMINATED;
  return Status::OK;
}
