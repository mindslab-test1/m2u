cmake_minimum_required(VERSION 2.8.12)

project(da-chatbotd CXX)
set(BINOUT da-chatbotd)

include_directories(../../../proto)
include_directories(../../../src/lib/dareg)
include_directories(${CMAKE_INSTALL_PREFIX}/include)
link_directories(${CMAKE_INSTALL_PREFIX}/lib)

set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif ()

set(SOURCE_FILES
    main.cc
    chatbot-agent.cc chatbot-agent.h
    )

add_executable(${BINOUT} ${SOURCE_FILES})

target_link_libraries(${BINOUT}
    maum-common
    m2u-dareg
    m2u-all-pb grpc++ protobuf
    curl
    )

# install(TARGETS ${BINOUT} RUNTIME DESTINATION da)
