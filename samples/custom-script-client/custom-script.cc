#include <Python.h>
#include <libmaum/common/config.h>
#include "custom-script.h"
#include "google/protobuf/util/json_util.h"

#include <spdlog/fmt/fmt.h>
#include <object.h>

using google::protobuf::util::MessageToJsonString;
using maum::m2u::router::v3::CustomClassifyResult;

static string GetPyErrorString() {
  if (PyErr_Occurred()) {
    PyObject* type_obj;
    PyObject* value_obj;
    PyObject* tb_obj;

    // Fetch error
    PyErr_Fetch(&type_obj, &value_obj, &tb_obj);
    string type = type_obj ? PyString_AsString(PyObject_Str(type_obj)): "";
    string value = value_obj ? PyString_AsString(PyObject_Str(value_obj)): "";
    PyObject *module_name, *pyth_module, *pyth_func;
    module_name = PyString_FromString("traceback");
    pyth_module = PyImport_Import(module_name);
    Py_XDECREF(module_name);

    string err = fmt::format("type:{}, value:{}",
                             type, value);
    if (pyth_module == nullptr) {
      return err;
    }

    pyth_func = PyObject_GetAttrString(pyth_module, "format_exception");
    if (pyth_func && PyCallable_Check(pyth_func)) {
      PyObject *pyth_val;
      PyObject *pystr;
      pyth_val = PyObject_CallFunctionObjArgs(pyth_func,
                                              type_obj,
                                              value_obj, tb_obj, NULL);

      pystr = PyObject_Str(pyth_val);
      auto full = PyString_AsString(pystr);
      string err = fmt::format("type:{}, value:{}, trace: {}",
                               type, value, full);
      Py_XDECREF(pyth_val);
      return err;
    } else {
      return err;
    }
  } else {
    return "<no error>";
  }
}

bool CustomScriptManager::Init() {
  auto logger = LOGGER();
  logger->debug("Initialize CustomScriptManager");
  Py_Initialize();
  auto &c = libmaum::Config::Instance();
  string custom_script_path = c.Get("itfd.customscript.path");

  string py_cs_path = "sys.path.append(\"";
  py_cs_path += custom_script_path;
  py_cs_path += "\")";

  logger->debug("custom script path={}", py_cs_path);

  PyRun_SimpleString("import sys");
  PyRun_SimpleString(py_cs_path.c_str());

  logger->debug("Py_GetPrefix: {}", Py_GetPrefix());
  logger->debug("Py_GetExecPrefix: {}", Py_GetExecPrefix());
  logger->debug("Py_GetProgramFullPath: {}", Py_GetProgramFullPath());
  logger->debug("Py_GetPath: {}", Py_GetPath());
  logger->debug("Py_GetVersion: {}", Py_GetVersion());
  logger->debug("Py_GetPlatform: {}", Py_GetPlatform());
  logger->debug("Py_GetCopyright: {}", Py_GetCopyright());
  logger->debug("Py_GetCompiler: {}", Py_GetCompiler());
  logger->debug("Py_GetBuildInfo: {}", Py_GetBuildInfo());

  logger->debug("Initialize CustomScriptManager...end");

  return true;
}

bool CustomScriptManager::Stop() {
  LOGGER()->debug("CustomScriptManager Stop");
  Py_Finalize();
  return true;
}

bool CustomScriptManager::CallPythonFunc(const char *name,
                                         const char *func,
                                         string &param,
                                         string &result) {

  auto logger = LOGGER();
  logger->debug("CallPythonFunc name={}, func={}", name, func);

  PyObject *po_name, *po_module, *py_func;
  PyObject *po_args, *po_value;

  Init();
  if (!Py_IsInitialized()) {
    logger->warn("Unable to initialize Python interpreter.");
    logger->error("py error {}", GetPyErrorString());
    return false;
  }

//  PyRun_SimpleString("sys.path.append(\"/maum/libexec/custom-script/\")");

  po_name = PyString_FromString(name);
  /* Error checking of po_name left out */

  po_module = PyImport_Import(po_name);

  if (po_module == nullptr) {
    logger->warn("Failed to load {}", name);
    logger->error("py error {}", GetPyErrorString());
    return false;
  }

  py_func = PyObject_GetAttrString(po_module, func);
  /* pFunc is a new reference */
  if ((py_func == nullptr) || (PyCallable_Check(py_func) == 0)) {
    logger->warn("Cannot find function {}", func);
    logger->error("py error {}", GetPyErrorString());
    return false;
  }

  po_args = Py_BuildValue("(s)", param.c_str());
  po_value = PyObject_CallObject(py_func, po_args);

  std::cout << "PyObject_CallObject..ok" << std::endl;

  if (po_value == nullptr) {
    logger->warn("Call failed\n");
    logger->debug("py error {}", GetPyErrorString());

    Py_XDECREF(py_func);
    Py_XDECREF(po_module);
    return false;
  }
  logger->warn("Result of call: {}\n", PyString_AsString(po_value));

  Py_XDECREF(po_name);
  Py_XDECREF(po_module);
  Py_XDECREF(py_func);
  Py_XDECREF(po_args);
  Py_XDECREF(po_value);

  Py_Finalize();
  return true;
}

bool CustomScriptManager::AppendPath(string path) {
  auto logger = LOGGER();
  if (!Py_IsInitialized()) {
    logger->warn("Unable to initialize Python interpreter.");
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    return false;
  }

  string fpath = "sys.path.append(\"" + path + "\")";

  PyRun_SimpleString("import sys");
  PyRun_SimpleString(fpath.c_str());
  return true;
}

bool CustomScriptManager::Classify(ClassifyScriptRule *rule,
                                   CustomClassifyParam *param,
                                   RunResult *result) {
  const char* file_name = rule->filename().c_str();
  const char* func_name = rule->classify_func().c_str();
  string sa_param = param->SerializeAsString();

  CustomClassifyResult *csres = nullptr;

  auto logger = LOGGER();
  string debug;
  MessageToJsonString(*param, &debug);
  logger->debug("CustomScript param={}", debug);

  Init();
  logger->debug("CallPythonFunc filename={}, classify_func={}", file_name, func_name);
  PyObject *po_name, *po_mod, *po_func;
  PyObject *po_args, *po_value;

  if (!Py_IsInitialized()) {
    logger->warn("Unable to initialize Python interpreter.");
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    return false;
  }

  po_name = PyString_FromString(file_name);
  po_mod = PyImport_Import(po_name);

  if (po_mod == nullptr) {
    logger->warn("Failed to load {}", file_name);
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    return false;
  }

  po_func = PyObject_GetAttrString(po_mod, func_name);

  if ((po_func == nullptr) || (PyCallable_Check(po_func) == 0)) {
    logger->warn("Cannot find function {}", func_name);
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    return false;
  }

  po_args = Py_BuildValue("(s#)", sa_param.data(), (int)sa_param.size());

  po_value = PyObject_CallObject(po_func, po_args);

  if (po_value == nullptr) {
    logger->warn("Call failed\n");
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    Py_XDECREF(po_func);
    Py_XDECREF(po_mod);
    return false;
  }
  string values = PyString_AsString(po_value);
  csres->ParseFromString(values);
  result->ParsePartialFromString(values);
  logger->warn("Result of call: {}\n", csres->Utf8DebugString());
  logger->warn("Result of call: {}\n", result->Utf8DebugString());

  Py_XDECREF(po_name);
  Py_XDECREF(po_mod);
  Py_XDECREF(po_func);
  Py_XDECREF(po_args);
  Py_XDECREF(po_value);

  return true;
}

bool CustomScriptManager::GetCategories(ClassifyScriptRule *rule,
                                        CustomGetCategoriesResult *result) {
  const char *file_name = rule->filename().c_str();
  const char *func_name = rule->classify_func().c_str();

  auto logger = LOGGER();

  Init();
  logger->debug("CallPythonFunc filename={}, classify_func={}",
                file_name,
                func_name);
  PyObject *po_name, *po_mod, *po_func, *po_dict;
  PyObject *po_args, *po_value;

  if (!Py_IsInitialized()) {
    logger->warn("Unable to initialize Python interpreter.");
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    return false;
  }

  po_name = PyString_FromString(file_name);
  /* Error checking of po_name left out */

  po_mod = PyImport_Import(po_name);

  if (po_mod == nullptr) {
    logger->warn("Failed to load {}", file_name);
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    return false;
  }

  po_dict = PyModule_GetDict(po_mod);
  po_func = PyDict_GetItemString(po_dict, func_name);

  if ((po_func == nullptr) || (PyCallable_Check(po_func) == 0)) {
    logger->warn("Cannot find function {}", func_name);
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    return false;
  }

  po_args = Py_BuildValue("O", NULL);
  po_value = PyObject_CallObject(po_func, po_args);

  if (po_value == nullptr) {
    logger->warn("Call failed\n");
    logger->error("py error {}", GetPyErrorString());
    logger->flush();
    Py_XDECREF(po_func);
    Py_XDECREF(po_mod);
    return false;
  }
  google::protobuf::util::JsonParseOptions options2;
  result->ParseFromString(PyString_AsString(po_value));
  string output;
  google::protobuf::util::JsonOptions options;
  options.always_print_primitive_fields = true;
  options.add_whitespace = true;
  MessageToJsonString(*result, &output, options);
  logger->debug("Result of call: {}\n", output);

  Py_XDECREF(po_name);
  Py_XDECREF(po_mod);
  Py_XDECREF(po_func);
  Py_XDECREF(po_args);
  Py_XDECREF(po_value);

  return true;
}

bool CustomScriptManager::ComposeHints(ClassifyScriptRule *rule,
                                       CustomComposeHintsParam *param,
                                       RunResult *result) {
  string in, out;
  CallPythonFunc(rule->filename().c_str(),
                 rule->classify_func().c_str(),
                 in,
                 out);
  return true;
}
bool CustomScriptManager::GetComposeTable(ClassifyScriptRule *rule,
                                          CustomGetComposeTableResult *result) {
  string in, out;
  CallPythonFunc(rule->filename().c_str(),
                 rule->classify_func().c_str(),
                 in,
                 out);
  return true;
}
