#include <iostream>
#include <libgen.h>
#include <google/protobuf/util/json_util.h>
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <gitversion/version.h>
#include <getopt.h>

#include "custom-script.h"

using google::protobuf::util::MessageToJsonString;

using namespace std;

void help(const char *prog) {
  printf("%s [--version] [--help]\n",
         prog);
}

void process_option(int argc, char *argv[]) {
  // bool do_exit = false;

  char *prog = basename(argv[0]);

  while (true) {
    static const struct option long_options[] = {
        {"version", no_argument, nullptr, 'v'},
        {"help", no_argument, nullptr, 'h'},
        {nullptr, no_argument, nullptr, 0}
    };

    /* getopt_long stores the option index here. */
    int option_index = 0;
    int c;
    c = getopt_long(argc, argv, "n:i:vht", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
      case 0: {
        break;
      }
      case 'v': {
        printf("%s version %s\n", basename(argv[0]), version::VERSION_STRING);
        // do_exit = true;
        break;
      }
      case 'h': {
        help(prog);
        // do_exit = true;
        break;
      }

      default: {
        help(prog);
        // do_exit = true;
      }
    }
  }

  // if (do_exit)
  //   exit(EXIT_SUCCESS);
}

void custom_script(){
  auto logger = LOGGER();
  string param;

  CustomScriptManager custom;
  ClassifyScriptRule rule;
  RunResult run_result;
  rule.set_filename("echo_custom_script");
  rule.set_classify_func("Classify");
  CustomClassifyParam func_param;
  func_param.set_chatbot("mychatbot");

  custom.Init();
  custom.Classify(&rule, &func_param, &run_result);
  string debug;
  MessageToJsonString(run_result, &debug);
  logger->info("RunResult = {}", debug);
  custom.Init();
  CustomGetCategoriesResult result2;
  rule.set_classify_func("GetCategories");
  custom.GetCategories(&rule, &result2);
  debug = "";
  MessageToJsonString(result2, &debug);
  logger->info("RunReulst2 = {}", debug);
}


int main(int argc, char *argv[]) {
  process_option(argc, argv);

  libmaum::Config::Init(argc, argv, "m2u.conf");


  custom_script();

  return 0;
}
