#include <sstream>

#include <libmaum/log/call-log.h>
#include <libmaum/rpc/result-status.h>

using libmaum::rpc::CommonResultStatus;
using libmaum::log::CallLogRecord;

string MyCallLogHandler(void *param) {
  CallLogRecord *rec = reinterpret_cast<CallLogRecord *>(param);

  std::ostringstream oss;
  oss << "|func=" << rec->func()
      << "|context=" << (rec->context() ? "exist" : "null")
      << "|request=" << rec->request()->SerializeAsString()
      << "|response=" << rec->response()->SerializeAsString()
      << "|status=" << CommonResultStatus::GetResultCode(*rec->status())
      << "|";
  return oss.str();
}

extern "C" void init(void *arg) {
  libmaum::log::CallLogger::GetInstance()->InitLogger("sample",
                                                      nullptr,
                                                      "xxxxx");
  libmaum::log::SetCallLogHandler(MyCallLogHandler);
}