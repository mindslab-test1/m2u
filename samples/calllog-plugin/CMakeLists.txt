cmake_minimum_required(VERSION 2.8.12)
project(calllog-plugin CXX)
set(OUTLIB calllog-plugin-sample)

include_directories(.)

set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  #  add_definitions(-std=c++11)
endif ()

include_directories("${CMAKE_INSTALL_PREFIX}/include")
link_directories("${CMAKE_INSTALL_PREFIX}/lib")

set(SOURCE_FILES
    calllog-plugin-sample.cc
    )
add_library(${OUTLIB} SHARED ${SOURCE_FILES})
if (APPLE)
  set_target_properties(${OUTLIB}
      PROPERTIES MACOSX_RPATH ${CMAKE_INSTALL_PREFIX}/lib)
endif ()
target_link_libraries(${OUTLIB}
    maum-common
    maum-rpc
    )

install(TARGETS ${OUTLIB}
    RUNTIME DESTINATION bin COMPONENT libraries
    LIBRARY DESTINATION libexec/log-plugins COMPONENT libraries
    ARCHIVE DESTINATION lib COMPONENT libraries)
