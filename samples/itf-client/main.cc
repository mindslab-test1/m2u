#include <getopt.h>
#include <libmaum/common/util.h>
#include <libgen.h>
#include <gitversion/version.h>
#include <boost/algorithm/string.hpp>
#include "itf-client.h"

void Usage(const char *prname) {
  std::cout << prname << std::endl
                  << " -s  --server : server endpoint " << std::endl
                  << std::endl;
  exit(0);
}

int ProcessCli(ItfClient &client) {
  std::string line;

  std::cout << "Intent Finder Client " << std::endl
            << "any text: talk text" << std::endl << std::endl;

  std::cout << "[qn/send/retry/exskill/exstep|Utter]: ";

  std::string utter = "안녕하세요 만나서 반갑습니다 자연을 사랑해요";
  int type = 0;
  int lang = 0;
  std::string chatbot = "chat1";
  std::string ex_skill = "";
  std::string ex_step = "";

  while (getline(std::cin, line)) {
    trim(line);
    std::cout << "\n";
    std::cout << "progress...\n";
    if (line == "q" || line == "Q")
      break;
    if (line == "n" || line == "N") {
    } else if (line.find("chatbot") != std::string::npos) {
      std::cout << std::endl;
      std::vector<std::string> strs;
      boost::split(strs, line, boost::is_any_of(" "));
      chatbot = strs[1];

    } else if (line == "send") {
      std::cout << std::endl;
      client.FindIntent(utter.c_str(), type, lang, chatbot.c_str());
    } else if (line == "retry") {
      std::cout << std::endl;

      client.RetryFindIntent(utter.c_str(),
                             type,
                             lang,
                             chatbot.c_str(),
                             ex_skill.c_str(),
                             ex_step.c_str());
    } else if (line == "exskill") {
      ex_skill = utter;

    } else if (line == "exstep") {
      ex_step = utter;

    } else {
      std::cout << std::endl;
      string msg = line;
      utter = line;
    }
    std::cout << "\n[qn/send/retry/exskill/exstep|Utter]: ";
  }

  std::cout << std::endl;
  return 0;
}

int main(int argc, char **argv) {

  char *base = basename(argv[0]);
  char *dir = dirname(argv[0]);
  chdir(dir);

  string filename;
  string chatbot;
  string server;

  while (true) {
    int optindex = 0;
    static option l_options[] = {
        {"version", 0, nullptr, 'v'},
        {"help", 0, nullptr, 'h'},
        {"server", 1, nullptr, 's'},
    };

    int ch = getopt_long(argc, argv, "vhs:", l_options, &optindex);
    if (ch == -1) {
      if (optindex >= argc)
        Usage(base);
      else
        break;
    }
    switch (ch) {
      case 'v': {
        printf("%s version %s\n", base, version::VERSION_STRING);
        break;
      }
      case 's': {
        server = optarg;
        break;
      }
      case 'h': {
        Usage(base);
        break;
      }
      default: {
        break;
      }
    }
  }

  string server_info = server.empty() ? "127.0.0.1:10055" : server;
  std::cout << "connect server = " << server_info << std::endl;
  ItfClient client(grpc::CreateChannel(
      server_info, grpc::InsecureChannelCredentials()));

  return ProcessCli(client);
}
