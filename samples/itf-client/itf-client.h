
#ifndef PROJECT_ITF_CLIENT_H
#define PROJECT_ITF_CLIENT_H

#include <grpc++/grpc++.h>
#include <maum/m2u/router/v3/intentfinder.grpc.pb.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using std::string;
using maum::m2u::router::v3::FindIntentRequest;
using maum::m2u::router::v3::FindIntentResponse;
using maum::m2u::router::v3::FoundIntent;
using maum::m2u::router::v3::IntentFinder;
using maum::m2u::router::v3::RetryFindIntentResponse;

class ItfClient {
 public:
  ItfClient(std::shared_ptr<Channel> channel)
      : stub_(IntentFinder::NewStub(channel)) {}

 public:
  int FindIntent(const char *utter, int type, int lang, const char *chatbot);
  int RetryFindIntent(const char *utter,
                      int type,
                      int lang,
                      const char *chatbot,
                      const char *ex_skill,
                      const char *ex_step);

 private:
  std::unique_ptr<IntentFinder::Stub> stub_;
};

#endif //PROJECT_ITF_CLIENT_H
