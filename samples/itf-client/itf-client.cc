#include "itf-client.h"
#include "google/protobuf/util/json_util.h"

using google::protobuf::util::MessageToJsonString;

int ItfClient::FindIntent(const char *utter,
                          int type,
                          int lang,
                          const char *chatbot) {
  std::cout << "FindIntent" << std::endl;

  ClientContext context;
  FindIntentRequest request;
  FindIntentResponse response;

  request.mutable_utter()->set_utter(utter);
  request.mutable_utter()->set_input_type((::maum::m2u::common::Utter_InputType) type);
  request.mutable_utter()->set_lang((::maum::common::Lang) lang);
  string result;
  if (chatbot)
    request.set_chatbot(chatbot);
  MessageToJsonString(request, &result);
  std::cout << ">>request FindIntent<<" << std::endl;
  std::cout << result << std::endl;
  Status status = stub_->FindIntent(&context, request, &response);

  if (!status.ok()) {
    std::cout << status.error_code() << ": " << status.error_message()
              << std::endl;
    return -1;
  }

  result = "";
  MessageToJsonString(response, &result);
  std::cout << ">>response FindIntent<<" << std::endl;
  std::cout << result << std::endl;
  return 0;
}

int ItfClient::RetryFindIntent(const char *utter,
                               int type,
                               int lang,
                               const char *chatbot,
                               const char *ex_skill,
                               const char *ex_step) {
  std::cout << "FindIntent" << std::endl;

  ClientContext context;
  FindIntentRequest request;
  RetryFindIntentResponse response;

  request.mutable_utter()->set_utter(utter);
  request.mutable_utter()->set_input_type((::maum::m2u::common::Utter_InputType) type);
  request.mutable_utter()->set_lang((::maum::common::Lang) lang);
  request.add_exclude_skills(ex_skill);
  request.add_skip_steps(ex_step);

  string result;
  if (chatbot)
    request.set_chatbot(chatbot);
  MessageToJsonString(request, &result);
  std::cout << ">>request RetryFindIntent<<" << std::endl;
  std::cout << result << std::endl;
  Status status = stub_->RetryFindIntent(&context, request, &response);

  if (!status.ok()) {
    std::cout << status.error_code() << ": " << status.error_message()
              << std::endl;
    return -1;
  }

  result = "";
  MessageToJsonString(response, &result);
  std::cout << ">>response RetryFindIntent<<" << std::endl;
  std::cout << result << std::endl;
  return 0;
}
