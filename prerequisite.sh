#!/usr/bin/env bash

if [ -n "${DOCKER_MAUM_BUILD}" ]; then
  echo "Docker container ${HOSTNAME} has already installed required packages for " ${DOCKER_MAUM_BUILD}
  exit 0
fi

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS, use ubuntu or centos"
  exit 1
fi

function get_requirements() {
  if [ "${OS}" = "centos" ]; then
    sudo yum -y install epel-release
    sudo yum -y groupinstall 'Development Tools'
    sudo yum -y install \
      gcc gcc-c++ \
      autoconf automake libtool make \
      cmake cmake3 \
      java-1.8.0-openjdk-devel.x86_64 \
      python-devel.x86_64 \
      flex-devel.x86_64 \
      libcurl-devel.x86_64 \
      sqlite-devel.x86_64 \
      openssl-devel.x86_64 \
      libarchive-devel.x86_64 \
      atlas-devel.x86_64 \
      lapack-devel.x86_64 \
      libuv-devel.x86_64 \
      libdb-devel.x86_64 \
      httpd nginx \
      policycoreutils-python \
      openssl-devel \
      wget \
      glibc-devel.x86_64 \
      pcre.x86_64 \
      libuuid-devel \
      zeromq-devel \
      file-devel \
      libatomic

    # docker config
    sudo yum -y remove \
      docker \
      docker-client \
      docker-client-latest \
      docker-common \
      docker-latest \
      docker-latest-logrotate \
      docker-logrotate \
      docker-engine
    #sudo yum -y install yum-utils device-mapper-persistent-data lvm2
    sudo yum-config-manager \
      --add-repo \
      https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum install docker-ce docker-ce-cli containerd.io
    sudo systemctl start docker.service

    # install pip & package
    curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
    sudo python get-pip.py
    rm get-pip.py

    # mariaDB install
    ##  maridDB repo 파일 자동 생성
    curl -sS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | sudo bash
    yum install MariaDB-server
    systemctl start mariadb
    systemctl enable mariadb
    ## 초기 mysql root pass 설정 필요시 사용
    mysql_secure_installation

    # npm
    curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
    sudo yum install -y nodejs

    # cuda-8.0
    if [ ! -d /usr/local/cuda-8.0 ]; then
        cd ~
        wget http://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-repo-rhel7-8.0.61-1.x86_64.rpm
        sudo rpm -Uvh cuda-repo-rhel7-8.0.61-1.x86_64.rpm
        sudo yum clean all
        sudo yum install -y cuda-8.0
    fi
  else
    # cuda-8.0
    if [ ! -d /usr/local/cuda-8.0 ]; then
        cd ~
        curl -O https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_8.0.61-1_amd64.deb
        sudo dpkg -i cuda-repo-ubuntu1604_8.0.61-1_amd64.deb
        cd -
        sudo apt update
        sudo apt install -y cuda-8.0
    fi

    # docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    #sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
      stable"

    sudo apt update
    sudo apt install -y \
      cuda-8.0 \
      openjdk-8-jdk \
      gcc-4.8 g++-4.8 g++ \
      make cmake \
      autoconf automake libtool \
      python-pip python-dev \
      openmpi-common \
      libboost-all-dev \
      libcurl4-openssl-dev \
      libsqlite3-dev \
      libmysqlclient-dev \
      libuv-dev libssl-dev \
      libdb-dev \
      libarchive13 libarchive-dev \
      libatlas-base-dev libatlas-dev \
      libasio-dev \
      docker-ce docker-ce-cli containerd.io \
      unzip \
      nginx \
      ffmpeg \
      flex \
      libpcre3 libpcre3-dev \
      uuid-dev \
      libzmq3-dev \
      libmagic-dev
    sudo ldconfig

    # mariaDB install
    sudo apt update
    sudo apt-get install software-properties-common
    sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
    sudo add-apt-repository "deb [arch=amd64,arm64,ppc64el] http://mariadb.mirror.liquidtelecom.com/repo/10.4/ubuntu $(lsb_release -cs) main"
    sudo apt update
    sudo apt -y install mariadb-server mariadb-client
    sudo systemctl enable mysqld
    sudo systemctl start mysqld

    ## 초기 mysql root pass 설정 필요시 사용
    #sudo mysql_secure_installation
    ## 설치 잘됐는지 확인할때 사용
    sudo systemctl status mysql
    ## 궁금하면 들어가서 보세요
    # https://computingforgeeks.com/how-to-install-mariadb-on-ubuntu/

    # nodejs
    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    sudo apt install -y nodejs
  fi
  sudo npm install -g n
  sudo n 8.11.3
  npm install -g @angular/cli@^7.3.6
  sudo pip install --upgrade pip
  sudo pip install --upgrade virtualenv
  sudo pip install --upgrade setuptools
  sudo pip install boto3 grpcio==1.9.1 requests numpy theano pymysql
  sudo pip install gensim==2.2.0
}


GLOB_BUILD_DIR=${HOME}/.maum-build
test -d ${GLOB_BUILD_DIR} || mkdir -p ${GLOB_BUILD_DIR}
sha1=$(git log -n 1 --pretty=format:%H ./prerequisite.sh)

echo "Last commit: ${sha1}"

if [ "$1" = "force" ]; then
  get_requirements
  exit 0
fi

if [ ! -f ${GLOB_BUILD_DIR}/${sha1}.done ]; then
  get_requirements
  if [ "$?" = "0" ]; then
    touch ${GLOB_BUILD_DIR}/${sha1}.done
  fi
else
  echo "prerequisite had been done!"
fi
