#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
import os

from maum.m2u.router.v3 import intentfinder_pb2 as itf
import logging.handlers
from google.protobuf import text_format as tf

reload(sys)
sys.setdefaultencoding('utf-8')

_MAUM_ROOT = os.environ['MAUM_ROOT']
logfile_base = os.path.splitext(os.path.basename(__file__))[0] + '.log'
logfile = os.path.join(_MAUM_ROOT, 'logs', logfile_base)
_FMT = '%(asctime)-15s %(name)s %(levelname)s' \
       ' %(process)d %(thread)s: %(message)s'
logger = logging.getLogger(__file__)
# 중복 로그를 방지하기 위해 handlers를 초기화 한다.
logger.handlers = []
handler = logging.handlers.RotatingFileHandler(logfile,
                                               maxBytes=50 * 1000 * 1000,
                                               backupCount=10)
handler.setFormatter(logging.Formatter(_FMT))
logger.addHandler(handler)
logger.setLevel(logging.INFO)


def classify(param):
    logger.info('echo-custom-script-remote Classify called')

    result = itf.CustomClassifyResult()
    utter = param.utter.utter

    logger.info('utter = %s', utter)
    logger.info('chatbot = %s', param.chatbot)
    logger.info('context= %s', param.system_context)
    logger.info('print = %s', param.session)

    custom_list = ['커스텀 알려줘', '커스텀 알려줘.']
    cs_list = ['cs 알려줘.', 'cs 뭐야?', 'cs 뭐야']

    # 별칭처리를 수행하는 기능
    # 예시)
    # - utter : 5만원 보내줘 길동이한테
    # utter 에서 '길동이' 뒤에 특정조사가 따라 붙으면 입출금 대상으로 판단하여
    # 별칭으로 대체한 문장과 별칭 대상이 되는 단어를 결과에 저장 하는 기능
    # - replaced_utter : 5만원 보내줘 별칭한테
    # - replacement : 길동이
    # - word : 별칭
    alias = '길동이'
    suffix_list = ['에게', '한테']
    word = '별칭'
    logger.info('alias = %s', alias)
    logger.info('suffix_list = %s', suffix_list)
    for sl in suffix_list:
        who = alias + sl
        if who in utter:
            utter = utter.replace(who, word + sl)
            print utter
            result.replaced_utter = utter
            result.replacements[word] = alias
    logger.info('replaced_utter = %s', utter)

    # 분류 기능 수행
    if utter in custom_list:
        cls_res = 'custom'
    else:
        if utter in cs_list:
            cls_res = 'cs'
        else:
            cls_res = ''
    result.classify_result = cls_res
    return result


def get_categories(request):
    """

    :param request: GetCategoriesRequest message object
    :return: CustomGetCategoriesResult message object
    """
    print 'echo-custom-script ', 'GetCategories', 'called'
    print 'get categories request', request
    print 'filename', request.rule.filename

    response = itf.CustomGetCategoriesResult()
    response.categories.append('custom')
    response.categories.append('cs')

    return response


def compose_hints(request):
    """
    힌트를 조합한다.
    :param request: 요청
    :return:
    """
    print 'echo-custom-script ', 'GetCategories', 'called'
    print '>>request : ', \
        tf.PrintMessage(request, sys.stdout, indent=2, as_utf8=True)

    response = itf.CustomGetCategoriesResult()
    response.categories.append('custom')
    response.categories.append('cs')

    return response


def get_compose_table():
    print 'echo-custom-script ', 'GetComposeTable', 'called'

    response = itf.CustomGetComposeTableResult()
    return response


if __name__ == '__main__':
    req = itf.CustomClassifyParam()
    req.utter.utter = '5만원 보내줘 길동이한테'  # 'hello.'
    req.chatbot = 'chat1'
    req.session.id = 1234
    req.system_context.user.name = 'mlab'
    print '>>request : ', \
        tf.PrintMessage(req, sys.stdout, indent=2, as_utf8=True)
    res = classify(req)
    print '>>response : ', \
        tf.PrintMessage(res, sys.stdout, indent=2, as_utf8=True)
