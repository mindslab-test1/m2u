#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from maum.m2u.router.v3 import intentfinder_pb2
from google.protobuf import json_format


def Classify(request):
    print 'echo-custom-script ', 'Classify', 'called'

    param = intentfinder_pb2.CustomClassifyParam()
    param.ParseFromString(request)
    result = intentfinder_pb2.CustomClassifyResult()
    cls_res = ''
    utter = ''
    utter = param.utter.utter

    print 'utter=' , utter
    print 'chatbot=' , param.chatbot
    print 'context=' , param.system_context
    print 'session=', param.session

    custom_list = ['커스텀 알려줘', '커스텀 알려줘.']
    cs_list = ['cs 알려줘.', 'cs 뭐야?', 'cs 뭐야']

    # 별칭처리를 수행하는 기능
    # 예시)
    # - utter : 5만원 보내줘 길동이한테
    # utter 에서 '길동이' 뒤에 특정조사가 따라 붙으면 입출금 대상으로 판단하여
    # 별칭으로 대체한 문장과 별칭 대상이 되는 단어를 결과에 저장 하는 기능
    # - replaced_utter : 5만원 보내줘 별칭한테
    # - replacement : 길동이
    # - word : 별칭
    alias = '길동이'
    suffix_list = ['에게', '한테']
    word = '별칭'
    who=''
    print 'alias', alias
    print 'suffix_list', suffix_list
    for sl in suffix_list:
        who = alias + sl
        if who in utter:
            utter = utter.replace(who, word+sl)
            print utter
            result.replaced_utter = utter
            result.replacements[word] = alias
    print 'replaced_utter', utter

    # 분류 기능 수행
    if utter in custom_list:
        cls_res = 'custom'
    else:
        if utter in cs_list:
            cls_res = 'cs'
        else:
            cls_res = ''
    result.classify_result = cls_res
    res_str = result.SerializeToString()
    print 'replaced result =>', res_str
    return res_str

def GetCategories():
    print 'echo-custom-script ', 'GetCategories', 'called'
    response = intentfinder_pb2.CustomGetCategoriesResult()
    response.categories.append('custom')
    response.categories.append('cs')
    categories = json_format.MessageToJson(response)
    print categories
    result_str = result = response.SerializeToString()
    return result_str

def ComposeHints(request):
    print 'echo-custom-script ', 'GetCategories', 'called'
    response = intentfinder_pb2.CustomGetCategoriesResult()
    response.categories.append('custom')
    response.categories.append('cs')
    categories = json_format.MessageToJson(response)
    print categories
    result = response.SerializeToString()
    return result

def GetComposeTable():
    print 'echo-custom-script ', 'GetComposeTable', 'called'

    response = intentfinder_pb2.CustomGetComposeTableResult()


    serialize = response.SerializeToString()
    return serialize

if __name__ == '__main__':
    response = GetCategories()
    req = intentfinder_pb2.CustomClassifyParam()
    res = intentfinder_pb2.CustomClassifyResult()
    req.utter.utter = '5만원 보내줘 길동이한테' #'hello.'
    req.chatbot = 'chat1'
    req.session.id = 1234
    req.system_context.user.name = 'mlab'
    reqmsg = json_format.MessageToJson(req)
    print ">>request : ", reqmsg
    input = req.SerializeToString()
    res = Classify(input)
    print ">>response : ", res
    print ">>categories : ", response
