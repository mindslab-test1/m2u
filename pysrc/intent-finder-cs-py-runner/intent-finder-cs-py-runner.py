#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from concurrent import futures
import grpc
import os
import time
import logging.handlers

from common.config import Config
from maum.m2u.router.v3 import intentfinder_pb2 as itf
from maum.m2u.router.v3 import intentfinder_pb2_grpc as itf_grpc
from stat import *  # ST_SIZE etci

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
_MAUM_ROOT = os.environ['MAUM_ROOT']
CS_PATH = os.path.join(_MAUM_ROOT, 'custom-script')

logfile_name = 'm2u-itf-cs-py-runner.log'
logger = logging.getLogger('cs-py-runner')


# python 서버의 동작

# Classify
# __import__를 통해서 동적으로 python을 로딩합니다.
# 로드된 모듈이 이미 있으면 다시 그냥 사용합니다.
# 함수이름을 이용하여 호출 대상을 명시합니다.
# 함수를 호출합니다.
# 응답값을 그대로 전송합니다.
class _CustomScriptRunnerServicer(itf_grpc.CustomScriptRunnerServicer):
    conf = Config()
    po_modules = {}
    # filename:modified-time 에 대한 dict
    file_modified = {}

    def __init__(self):
        sys.path.insert(0, CS_PATH)
        pass

    def Classify(self, request, context):
        logger.info('<<< Classify called')

        # request : CustomClassifyParam.ClassifyScriptRule 에서
        #           full_filename, filename, classify_func 가져와서 사용한다.
        rule_info, full_filename, classify_func = self.get_cs_rule_info(request)
        if not rule_info:
            result = itf.CustomClassifyResult()
            return result

        # po_module 가 load 된 상태이면 재로드 하지 않고 재사용한다.
        # self.po_modules[filename] 의 파일 시간을 체크해서 다르면 다시 import
        import_info, filename = self.do_cs_import(full_filename)
        try:
            if import_info:
                # 함수 객체를 가져온다.
                po_func = getattr(self.po_modules[filename], classify_func)
                logger.debug('get classify_func = %s', po_func)

                # operation sync id 전달.
                client_meta = context.invocation_metadata()
                op_sync = {key: val for key,
                           val in client_meta if 'x-operation-sync-id' in key}
                meta = request.system_context.user.meta
                meta['x-operation-sync-id'] = op_sync['x-operation-sync-id']

                # 함수 호출 결과를 리턴한다. return : CustomClassifyResult
                logger.debug('call classify_func')
                result = po_func(request)
            else:
                logger.info('return dummy result')
                result = itf.CustomClassifyResult()
        except Exception as e:
            logger.error('Classify : getattr or po_func Exception')
            logger.exception(e)
            result = itf.CustomClassifyResult()
            return result
        else:
            return result
        finally:
            logger.info('>>> Classify finished')

    # ClassifyScriptRule 의 정보를 가져온다.
    @staticmethod
    def get_cs_rule_info(request):
        try:
            full_filename = str(request.rule.filename)
            classify_func = str(request.rule.classify_func)
            logger.info('cs_rule = %s / %s()', full_filename, classify_func)
        except Exception as e:
            logger.error('failed to get custom_script info')
            logger.exception(e)
            return False, None, None
        else:
            if not full_filename or not classify_func:
                logger.error('script rule info is invalid')
                return False, None, None
            else:
                logger.debug('script rule info is valid')
                return True, full_filename, classify_func

    def do_cs_import(self, full_filename):
        # 확장자를 제거한다.
        split = os.path.splitext(full_filename)
        filename = split[0]
        logger.debug('filename = %s', filename)
        if not filename:
            return False, None

        try_reload = False
        try:
            # po_module 가 load 된 상태이면 재로드 하지 않고 재사용한다.
            # self.po_modules[filename] 의 파일 시간을 체크해서 다르면 다시 import
            new_import = filename not in self.po_modules \
                         or self.po_modules[filename] is None
            if new_import:
                self.po_modules[filename] = __import__(filename)
                self.print_import_info(self.po_modules[filename], new_import)

            logger.debug('po_modules = %s', self.po_modules[filename])

            # 현재 파일 시간을 가져온다.
            file_modified = self.file_modified.get(filename, 'None')
            current_file_time, \
                reimport = self.get_file_time(full_filename, file_modified)

            if not current_file_time:
                return {True, filename} if new_import else {False, None}

            # 새로 import 되는 경우에는 dict에 저장한다.
            if new_import:
                self.file_modified[filename] = current_file_time
            # dict 에 파일 시간을 가지고 있으면 가져와서 비교한 후 다르면 import 를 다시 한다.
            elif reimport:
                # reload 실패 시 이전에 load 되었던 정보를 사용하도록 한다.
                try_reload = True
                self.po_modules[filename] = reload(self.po_modules[filename])
                self.print_import_info(self.po_modules[filename], new_import)
                self.file_modified[filename] = current_file_time
                logger.debug('po_modules re-import = %s',
                             self.po_modules[filename])
        except ImportError as e:
            logger.exception(e)
            if try_reload:
                logger.error('Classify : Reload ImportError.'
                             ' use previous import info')
                return True, filename
            else:
                logger.error('Classify : ImportError')
                return False, ''
        except Exception as e:
            logger.exception(e)
            if try_reload:
                logger.error('Classify : Reload Exception')
                return True, filename
            else:
                logger.error('Classify : Import Exception')
                return False, ''
        else:
            return True, filename

    # 현재 파일 시간을 가져온다.
    @staticmethod
    def get_file_time(full_filename, modified_file_time):
        try:
            st = os.stat(os.path.join(CS_PATH, full_filename))
            current_file_time = time.asctime(time.localtime(st[ST_MTIME]))
        except IOError as e:
            logger.error('get_file_time failed to get io-error about')
            logger.exception(e)
            return '', False
        except Exception as e:
            logger.error('get_file_time failed to get exception about')
            logger.exception(e)
            return '', False
        else:
            reimport = modified_file_time != current_file_time
            level = logging.INFO if reimport else logging.DEBUG
            logger.log(level, 'file modified time path : %s',
                       os.path.join(CS_PATH, full_filename))
            logger.log(level, 'file modified information = %s',
                       current_file_time)
            return current_file_time, reimport

    # import module 에 대한 정보 출력
    @staticmethod
    def print_import_info(po_module, new_import):
        status = 'import' if new_import else 're-import'
        logger.info('%s > abspath : %s', status,
                    os.path.abspath(po_module.__file__))
        logger.info('%s > dirinfo : %s', status, dir(po_module))
        logger.debug('%s > fileinfo : %s', status, po_module.__file__)


def serve():
    conf = Config()
    conf.init('m2u.conf')
    py_port = conf.get('itf.cs.py.port')

    logfile_base = conf.get('logs.dir')
    logfile_full = os.path.join(logfile_base, logfile_name)
    _FMT = '%(asctime)-15s %(name)s %(levelname)s' \
           ' %(process)d %(thread)s: %(message)s'
    # 모든 Custom Script의 로그를 m2u-cs-all.log로 쌓으려면 주석을 푼다.
    # logging.basicConfig(filename=os.path.join(logfile_base, 'm2u-cs-all.log'),
    #                     format=_FMT,
    #                     level=logging.INFO)
    handler = logging.handlers.RotatingFileHandler(logfile_full,
                                                   maxBytes=50 * 1000 * 1000,
                                                   backupCount=10)
    handler.setFormatter(logging.Formatter(_FMT))
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    logger.info('cs runner server called')

    # max_workers 를 10 > 100 으로 늘림
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=100))
    itf_grpc.add_CustomScriptRunnerServicer_to_server(
        _CustomScriptRunnerServicer(), server)

    listen = '[::]' + ':' + py_port
    server.add_insecure_port(listen)
    server.start()
    logger.info('cs runner start. port = %s', py_port)

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
        logger.info('cs runner server interrupt.')


if __name__ == '__main__':
    serve()
