#!/usr/bin/env python
# -*- coding: utf-8 -*-

import signal
import subprocess
import argparse
import socket
from time import sleep
import grpc
import sys
import json
import random

import traceback
from common.config import Config
from maum.common import lang_pb2
from maum.m2u.da import provider_pb2 as provider
from maum.m2u.da.v1 import talk_pb2_grpc as talk_v1_grpc
from maum.m2u.da.v1 import talk_pb2 as talk_v1
from maum.m2u.da.v2 import talk_pb2_grpc as talk_v2_grpc
from maum.m2u.da.v2 import talk_pb2 as talk_v2
from maum.m2u.da.v3 import talk_pb2_grpc as talk_v3_grpc
from maum.m2u.da.v3 import talk_pb2 as talk_v3
from maum.m2u.server import pool_pb2_grpc
from maum.m2u.server import pool_pb2
from google.protobuf import empty_pb2

class DialogAgentRegister(object):
    version = '2.1'
    exec_cmd = []
    respawn = False

    chatbot = ''
    skills = []
    custom_host = None
    wait = 2.0
    port = -1
    conf = Config()
    sds_conf = Config()
    child_pid = 0
    pipe = None
    resp = None
    da_spec = None
    check_cnt = 60
    check_wait = 1

    #@ id, key, damname, param도 인자로 받을수 있도록 변수를 추가합니다
    id = ''
    key = ''
    damname = ''
    param = ''
    min_port = 10000
    max_port = 13000
    lang = ''

    def __init__(self):
        self.min_port = int(self.conf.get('dam-svcd.random.min.port'))
        self.max_port = int(self.conf.get('dam-svcd.random.max.port'))

    def set_exec(self, exec_cmd):
        self.exec_cmd = exec_cmd

    def set_respawn(self, respawn):
        self.respawn = respawn

    def set_skills(self, skills):
        self.skills = skills

    def set_chatbot(self, chatbot):
        self.chatbot = chatbot

    def set_da_spec(self, da_spec):
        if da_spec == 'v1':
            self.da_spec = provider.DAP_SPEC_V_1
        elif da_spec == 'v2':
            self.da_spec = provider.DAP_SPEC_V_2
        elif da_spec == 'v2p1':
            self.da_spec = provider.DAP_SPEC_V_2_P1
        elif da_spec == 'v3':
            self.da_spec = provider.DAP_SPEC_V_3
        #@ hzc에는 약어로 저장되지 않아 full name으로도 조건식을 추가합니다
        elif da_spec == 'DAP_SPEC_V_1':
            self.da_spec = provider.DAP_SPEC_V_1
        elif da_spec == 'DAP_SPEC_V_2':
            self.da_spec = provider.DAP_SPEC_V_2
        elif da_spec == 'DAP_SPEC_V_2_P1':
            self.da_spec = provider.DAP_SPEC_V_2_P1
        elif da_spec == 'DAP_SPEC_V_3':
            self.da_spec = provider.DAP_SPEC_V_3
        else:
            self.da_spec = provider.DAP_SPEC_UNSPECIFIED

    def do_run_forever(self):
        pass

    def run(self):
        signal.signal(signal.SIGINT, self.on_signal);
        signal.signal(signal.SIGTERM, self.on_signal);
        signal.signal(signal.SIGQUIT, self.on_signal);
        if self.respawn:
            self.do_run_forever()
        else:
            self.do_run()

    def set_custom_host(self, custom_host):
        self.custom_host = custom_host;

    def set_wait(self, wait):
        self.wait = float(wait);

    def set_port(self, port):
        self.port = int(port);

    #@ 인자로 전달받은 id, key, damname, param을 set 합니다
    def set_id(self, id):
        self.id = id;
    def set_key(self, key):
        self.key = key;
    def set_damname(self, damname):
        self.damname = damname;
    def set_param(self, param):
        self.param = param;

    def set_lang(self, lang):
        if lang == 'kor':
            self.lang = lang_pb2.ko_KR
        elif lang == 'ko_KR':
            self.lang = lang_pb2.ko_KR
        elif lang == '0':
            self.lang = lang_pb2.ko_KR
        elif lang == 'eng':
            self.lang = lang_pb2.en_US
        elif lang == 'en_US':
            self.lang = lang_pb2.en_US
        elif lang == '1':
            self.lang = lang_pb2.en_US
        else:
            self.lang = lang_pb2.ko_KR

    @staticmethod
    def get_local_ip():
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 53))
        # print(s.getsockname()[0])
        ret = s.getsockname()[0]
        s.close()
        return ret

    def check_tcp_ready(self, child_ip, child_port):
        sleep(self.check_wait)
        # 접속이 가능한지 체크합니다
        # 접속에 성공한 경우 sock.close를 한 후 True를 리턴합니다
        # 접속에 실패한 경우 1분간 재시도를 시도합니다. 최종적으로 False를 리턴합니다
        remain_cnt = self.check_cnt
        is_connect = False
        sock = None
        while remain_cnt > 0:
            remain_cnt-=1
            try:
                sock = socket.create_connection((child_ip, child_port), 60)
                sock = sock.close()
                print '#@ success connection child_ip1 :', child_ip, 'child_port :', child_port, ', sock :', sock
                is_connect = True
                break
            except:
                print '#@ cant connect child_ip :', child_ip, 'child_port :', child_port, ', Remain try cnt :', remain_cnt, ', sock :', sock
                sleep(self.check_wait)
                is_connect = False

        return is_connect

    def do_run(self):
        cmd = self.exec_cmd
        if self.port < 0:
            self.port = find_unused_port(self.min_port, self.max_port)
        if type(cmd) is list:
            cmd = cmd[0].split(' ')
        cmd.append('-p')
        cmd.append(str(self.port))

        self.pipe = subprocess.Popen(cmd, close_fds=True,
                                     stdout=sys.stdout,
                                     stderr=sys.stderr)
        if not self.pipe.pid > 0:
            print '>>>', 'Cannot exec cmd', self.exec_cmd
            exit(1)

        # TODO(gih2yun): change to pool server..
        pool_export = self.conf.get('pool.export')
        if self.custom_host:
            pool_export = self.custom_host
            print 'force custom_host = ' + pool_export

        print '>>>', 'pool endpoint', pool_export
        pool_stub = pool_pb2_grpc.DialogAgentInstancePoolStub(
            grpc.insecure_channel(pool_export))

        try:
            sleep(self.wait)

            child_ip = '127.0.0.1'
            child_port = str(self.port)
            child_addr = child_ip + ':' + child_port

            # DA와 TCP connect가 가능한지 체크합니다
            if self.check_tcp_ready(child_ip, child_port) == False :
                print '>>>', 'cannot connect to DA :', child_addr
                return

            print 'connect da ' + child_addr
            child_channel = grpc.insecure_channel(child_addr)

            if self.da_spec == provider.DAP_SPEC_V_1:
                print 'da spec v1'
                dainst_stub = talk_v1_grpc.DialogAgentProviderStub(child_channel)
            elif self.da_spec == provider.DAP_SPEC_V_2:
                print 'da spec v2'
                dainst_stub = talk_v2_grpc.DialogAgentProviderStub(child_channel)
            elif self.da_spec == provider.DAP_SPEC_V_3:
                print 'da spec v3'
                dainst_stub = talk_v3_grpc.DialogAgentProviderStub(child_channel)
            else:
                print 'da spec v1'
                dainst_stub = talk_v1_grpc.DialogAgentProviderStub(child_channel)

            empty_obj = empty_pb2.Empty()
            status = dainst_stub.IsReady(empty_obj)
            if status.state != provider.DIAG_STATE_IDLE:
                print '>>>', 'invalid state ', status.state, self.pipe.pid
                self.pipe.terminate()
                exit(1)
            print '>>>', "is ready done!"

            #@ init_param을 darun시에 전달받은 param값으로 설정해 주어야 합니다
            # provider.proto를 보면 InitParameter에 대해 정의가 되어 있습니다
            print '>>>', "Initial param is :", self.param

            #@ [step - 0] 여기에선 chatbot, skills, lang을 설정합니다
            init_param = provider.InitParameter()
            init_param.chatbot = self.chatbot
            init_param.skills.extend(self.skills)
            init_param.lang = lang_pb2.ko_KR

            init_param.sds_remote_addr = self.sds_conf.get('maum.sds.listen')

            #@ [step - 1] 여기에서 param 정보를 설정합니다
            python_obj = json.loads(self.param)

            for p in python_obj:
                # key = value 형태로 값을 넣어주세요
                init_param.params[p] = python_obj[p]

            # init_param을 확인합니다
            for ip in init_param.params:
                print '>>> init_param key :', ip, ", value :", init_param.params[ip]

            #@ [step - 2] 여기에서 Init을 시작합니다
            res = dainst_stub.Init(init_param)
            print '>>>', "init done!"

            # DA RES
            dares = pool_pb2.DialogAgentInstanceResource()
            dares.name = 'dareg-{}-{}'.format(res.name, self.port)
            dares.description = res.description
            dares.version = res.version
            dares.da_spec = self.da_spec
            dares.chatbot = self.chatbot
            dares.dam_name = self.damname
            dares.skills.extend(self.skills)
            dares.lang = self.lang

            #@ dares에 key, id, damname을 지정해 줍니다
            dares.key = self.key
            dares.dai_id = self.id
            dares.dam_name = self.damname
            dares.server_ip = DialogAgentRegister.get_local_ip()
            dares.server_port = self.port

            dares.launch_type = pool_pb2.DAL_DARUN
            dares.launcher = 'dareg {}'.format(self.version)
            dares.pid = self.pipe.pid
            dares.started_at.GetCurrentTime()
            dares.param.CopyFrom(res)

            print '#@ dares >>>', dares
            self.resp = pool_stub.Register(dares)
            print("#@ >>> register done!", self.resp, dares.da_spec)

            # p = select.poll()
            # p.register(self.pipe.stderr)
            # p.register(self.pipe.stdout)
            #
            # while self.pipe.poll() is None:
            #     try:
            #         r = p.poll(1)
            #     except select.error, err:
            #         if err.args[0] != socket.EINTR:
            #             raise
            #         r = []
            #     for fd, flags in r:
            #         print fd
            #         if fd == self.pipe.stderr.fileno():
            #             print 'E ', self.pipe.stderr.readline()
            #         if fd == self.pipe.stdout.fileno():
            #             print 'O ', self.pipe.stdout.readline()
            #     sleep(0.5)
            # if self.da_spec == provider.DAP_SPEC_V_3:
            #     dainst_stub.EventT(empty_obj)

            self.pipe.wait()

            print '>>>', 'Child process exit with', self.pipe.returncode
            self.stop()
        except grpc.RpcError as e:
            print '\n>>>', e.__doc__
            print '>>>', e.message
            print '>>>', traceback.format_exc()
            self.stop()
        except Exception as e:
            print '\n>>>', e.__doc__
            print '>>>', e.message
            print '>>>', traceback.format_exc()
            self.stop()

    def stop(self):
        if self.pipe is not None:
            if self.resp is not None:
                pool_endpoint = self.conf.get('pool.export')
                pool_stub = pool_pb2_grpc.DialogAgentInstancePoolStub(
                    grpc.insecure_channel(pool_endpoint))

                dares_key = pool_pb2.DialogAgentInstanceKey()
                dares_key.key = self.resp.key
                stat = pool_stub.Unregister(dares_key)
                print 'Unregister - ', dares_key, stat
                self.resp = None
            self.pipe.terminate()
            self.pipe = None

    def on_signal(self, signum, frame):
        print '\nSIGNAL: ', signum
        self.stop()
        sys.exit(0)


def find_unused_port(port_min, port_max):
    # socket 객체 생성
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # 사용하지 않는 포트를 찾았는지 여부를 판단하는 변수
    found = False
    print '#@ port_min :', port_min, ', port_max :', port_max

    while found != True:
      temp_port = random.randint(port_min, port_max)
      try:
        s.bind(('', temp_port))
        found = True
      except Exception as e:
        print '#@ Exception :', e
        found = False

    # s.bind(('', 0))
    addr, port = s.getsockname()
    print 'find unused port', addr, port
    # socket 객체를 close
    s.close()
    return port


def run_cmd():
    conf = Config()
    conf.init('m2u.conf') # for prod
    # conf.init('/home/XXX/maum/etc/m2u.conf') # for debug
    sds_conf = Config()
    sds_conf.init('brain-sds.conf')

    parser = argparse.ArgumentParser(
        description='DA runner')
    parser.add_argument('cmd',
                        nargs='+',
                        help='Exeutable command')
    parser.add_argument('-c', '--chatbot',
                        nargs='?',
                        dest='chatbot',
                        required=True,
                        help='Specify Chatbot to run')
    parser.add_argument('-s', '--skill',
                        nargs='*',
                        dest='skills',
                        required=True,
                        help='Skills in chatbot')
    parser.add_argument('-r', '--respawn',
                        nargs='?',
                        dest='respawn',
                        required=False,
                        help='Enable respawn')
    parser.add_argument('-v', '--version',
                        nargs='?',
                        dest='da_spec',
                        required=False,
                        default=provider.DAP_SPEC_UNSPECIFIED,
                        help='Define DA Specification')
    parser.add_argument('-l', '--hzchost',
                        nargs='?',
                        dest='custom_host',
                        required=False,
                        default=provider.DAP_SPEC_UNSPECIFIED,
                        help='Hazelcast host to access.')
    parser.add_argument('-w', '--wait',
                        nargs='?',
                        dest='wait',
                        required=False,
                        default=2,
                        help='Wait seconds for DA IsReady.')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=False,
                        default=-1,
                        help='specify port to listen fot DAI.')
    parser.add_argument('-i', '--id',
                        nargs='?',
                        dest='id',
                        required=True,
                        help='specify DAI id to run')
    parser.add_argument('-k', '--key',
                        nargs='?',
                        dest='key',
                        required=True,
                        help='specify DAI key to run')
    parser.add_argument('-d', '--damname',
                        nargs='?',
                        dest='damname',
                        required=True,
                        help='specify DAM naem to run')
    parser.add_argument('-pa', '--param',
                        nargs='?',
                        dest='param',
                        required=False,
                        default='{}',
                        help='specify DAI param to run')
    parser.add_argument('-la', '--lang',
                        nargs='?',
                        dest='lang',
                        required=True,
                        help='specify DAI lang to run')

    args = parser.parse_args()

    print args.cmd

    runner = DialogAgentRegister()
    runner.set_exec(args.cmd)
    runner.set_chatbot(args.chatbot)
    runner.set_skills(args.skills)
    runner.set_da_spec(args.da_spec)
    runner.set_custom_host(args.custom_host)
    runner.set_wait(args.wait)
    runner.set_port(args.port)
    runner.set_id(args.id)
    runner.set_key(args.key)
    runner.set_damname(args.damname)
    runner.set_param(args.param)
    runner.set_lang(args.lang)

    # runner.set_respawn(args.respawn)
    runner.set_respawn(False)

    try:
        runner.run()
    except KeyboardInterrupt:
        runner.stop()


if __name__ == '__main__':
    run_cmd()
