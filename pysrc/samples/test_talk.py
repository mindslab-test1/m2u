#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import unittest
import requests, json

import grpc
from google.protobuf import struct_pb2 as struct
from google.protobuf import empty_pb2

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.getenv('MAUM_ROOT') + '/lib/python' # for prod
# lib_path = os.path.realpath(bin_path + '/../lib/python')  # for debug
sys.path.append(lib_path)

from common.config import Config
from maum.m2u.facade import front_pb2 as front
from maum.m2u.facade import front_pb2_grpc
from maum.common import lang_pb2 as lang


class TalkClient:
    conf = Config()
    stub = None
    metadata = [(b'in.devicetoken', b'123123')]  # metadata 로 device_token 지정
    headers = {'Content-Type': 'application/json; charset=utf-8', 'x-device-token': '123123', 'chatbot': 'lguplus'}
    http_url = None

    def __init__(self):
        self.conf.init('m2u.conf')
        # self.conf.init('../../../minds/etc/m2u.conf')
        remote = 'localhost:' + self.conf.get('frontd.export.port')
        self.http_url = 'http://' + self.conf.get('front.rest.server.ip') + ':' + self.conf.get(
            'front.rest.server.port')
        channel = grpc.insecure_channel(remote)

        self.stub = front_pb2_grpc.TalkServiceStub(channel)

    def grpc_analyze_request(self, question):
        talk_query = front.TalkQuery()

        utter_request = front.UtterRequest()
        utter_request.utter = question
        utter_request.lang = lang.ko_KR
        meta_struct = struct.Struct()
        meta_struct["signal.skill"] = "123"
        utter_request.meta.CopyFrom(meta_struct)

        talk_query.query.CopyFrom(utter_request)

        talk_query.device.type = "DEV_002"
        talk_query.device.timestamp.seconds = 3
        talk_query.device.timezone = "KST+9"
        talk_query.context_reset = "false"
        talk_query.chatbot = "lguplus"

        talk_analysis = self.stub.Analyze(talk_query, metadata=self.metadata)

        return talk_analysis.answer.utter

    def grpc_make_answer(self, ):
        answer_slot = front.AnswerSlot()
        answer_slot.skill = "WEATHER"
        answer_slot.intent = "WEATHER>SUB"
        answer_slot.serviceResultcode = "COIF20000000"
        # answer_slot.slots.time = "20170601123040"
        # answer_slot.slots.area = "판교"
        # answer_slot.slots.humidity = "맑음"
        # answer_slot.slots.MaxTemp = "30.0"
        # answer_slot.slots.MinTemp = "25.0"

        answer = self.stub.MakeAnswer(answer_slot, metadata=self.metadata)
        return answer.skill

    def grpc_peek_answer(self):
        empty = empty_pb2.Empty()
        answer = self.stub.PeekAnswer(empty, metadata=self.metadata)
        return answer.skill

    def http_analyze_request(self, question):
        data = {
            "query": {
                "utter": question,
                "type": "SPEECH",
                "altUtters": ["hello"],
                "lang": "ko_KR",
                "meta": {
                    "query_key1": "value1",
                    "query_key2": 12345,
                    "signal.skill": "qw"
                }
            },
            "contextReset": "false",
            "device": {
                "type": "DEV_002",
                "model": "model123456",
                "version": "v1.2.3.4",
                "serial": "serial-194985",
                "location": {
                    "latitude": 23.1234,
                    "longitude": 123.1233,
                    "location": "seoul"
                },
                "timestamp": {
                    "seconds": 1504604908,
                    "nanos": 0
                },
                "timezone": "KST+9",
                "meta": {
                    "dev_key1": "value1",
                    "dev_key2": 12345
                }
            }
        }
        res = requests.post(self.http_url + '/nlpapi/v1/analyze', data=json.dumps(data), headers=self.headers)
        return res.json()['answer']['utter']

    def http_make_answer(self):
        data = {
            "skill": "123",
            "intent": "WEATHER>SUB",
            "serviceResultcode": "COIF20000000",
            "slots": {
                "time": "20170601123040",
                "area": "판교",
                "humidity": "맑음",
                "MaxTemp": "30.0",
                "MinTemp": "25.0"
            },
            "data": {
                "weatherData": [
                    {
                        "data1": "value1"
                    },
                    {
                        "data2": "value2"
                    }
                ]
            },
            "meta": {
                "transactionId": "TR-ID-01912384",
                "messageId": "MSG-ID-194234"
            }
        }
        res = requests.post(self.http_url + '/nlpapi/v1/make-answer/weather', data=json.dumps(data), headers=self.headers)
        return res.json()['skill']

    def http_peek_answer(self):
        res = requests.get(self.http_url + '/nlpapi/v1/peek-answer', headers=self.headers)
        return res.json()['skill']


class TalkUnitTest(unittest.TestCase):
    def setUp(self):
        self.talk_client = TalkClient()

    def test_grpc_analyze(self):
        self.assertEqual(self.talk_client.grpc_analyze_request("1"), "1(일)은 가장 작은 자연수로 0과 2 사이의 정수이다.")

    def test_grpc_make_answer(self):
        self.assertEqual(self.talk_client.grpc_make_answer(), "common")

    # def test_grpc_peek_answer(self):
    #     self.assertEqual(self.talk_client.grpc_peek_answer(), "common")

    def test_http_analyze(self):
        self.assertEqual(self.talk_client.http_analyze_request("1"), "1(일)은 가장 작은 자연수로 0과 2 사이의 정수이다.")

    def test_http_make_answer(self):
        self.assertEqual(self.talk_client.http_make_answer(), "common")

    # def test_http_peek_answer(self):
    #     self.assertEqual(self.talk_client.http_peek_answer(), "common")


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TalkUnitTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
