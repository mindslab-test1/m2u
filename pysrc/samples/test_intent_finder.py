#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import grpc

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.getenv('MAUM_ROOT') + '/lib/python' # for prod
# lib_path = os.path.realpath(bin_path + '/../lib/python')  # for debug
sys.path.append(lib_path)

from common.config import Config
from maum.m2u.server import intentfinder_pb2_grpc as itf_grpc
from maum.m2u.server import intentfinder_pb2 as itf
from maum.m2u.facade import front_pb2 as front
from maum.common import lang_pb2 as lang

class IntentFinderClient:
    conf = Config()
    stub = None

    def __init__(self):
        # self.conf.init('m2u.conf') # for prod
        # self.conf.init('/home/xxx/maum/etc/m2u.conf') # for debug
        channel = grpc.insecure_channel("localhost:9951")

        self.stub = itf_grpc.IntentFinderStub(channel)

    def test_itf(self):
        # itf input
        input = itf.FindIntentRequest()
        input.session_id = '12345'

        utter = front.UtterRequest()
        utter.utter = '안녕하세요'
        utter.lang = lang.ko_KR

        device = front.Device()
        user = front.User()

        input.utter.CopyFrom(utter)
        input.device.CopyFrom(device)
        input.user.CopyFrom(user)

        input.chatbot = 'mindslab'

        # FindIntentResponse
        output = self.stub.FindIntent(input)

        print 'Skill >>> ', output.skill.skills[0].skill

if __name__ == '__main__':
    itfc = IntentFinderClient()
    itfc.test_itf()