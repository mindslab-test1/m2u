#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import grpc

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
# lib_path = os.getenv('MAUM_ROOT') + '/lib/python' # for prod
lib_path = os.path.realpath(bin_path + '/../lib/python')  # for debug
sys.path.append(lib_path)

from common.config import Config
from maum.m2u.common import trigger_pb2
from maum.m2u.common import trigger_pb2_grpc

class DialogTriggerClient:
    conf = Config()
    stub = None

    def __init__(self):
        # self.conf.init('m2u.conf') # for prod
        # self.conf.init('/home/xxx/maum/etc/m2u.conf') # for debug
        channel = grpc.insecure_channel("10.122.66.34:9960")

        self.stub = trigger_pb2_grpc.TriggerServiceStub(channel)

    def test_dt(self):
        # trigger request
        req = trigger_pb2.TriggerRequest()
        req.utter = "안녕하세요"
        req.device_id = "123123"

        # FindIntentResponse
        self.stub.TriggerDialog(req)


if __name__ == '__main__':
    dtc = DialogTriggerClient()
    dtc.test_dt()
