#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re

reload(sys)
sys.setdefaultencoding('utf-8')

# Basic lib import
from concurrent import futures
import argparse
import grpc
import time

import logging
import logging.handlers
# pb import
from google.protobuf import empty_pb2
from google.protobuf import struct_pb2 as struct
from maum.m2u.da.v3 import talk_pb2
from maum.m2u.da.v3 import talk_pb2_grpc
from maum.m2u.da import provider_pb2
from maum.m2u.facade import types_pb2
from maum.m2u.facade import userattr_pb2

__EXE_PATH__ = os.path.realpath(sys.argv[0])
__BIN_PATH__ = os.path.dirname(__EXE_PATH__)
__LIB_PATH__ = os.path.realpath(__BIN_PATH__ + "/../lib/python")
sys.path.append(__LIB_PATH__)
__ONE_DAY_IN_SECONDS__ = 60 * 60 * 24


class DaMainServer(talk_pb2_grpc.DialogAgentProviderServicer):
    # STATE
    # state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()
    # session_update 담길 session_update_entity
    session_update_entity = {}

    logger = logging.getLogger('echo_da_logger')
    
    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE
        fomatter = logging.Formatter('%(asctime)s %(levelname)s %(filename)s[%(lineno)d] %(message)s')
        # fileHandler = logging.FileHandler('../logs/sds_player_da.log')
        streamHandler = logging.StreamHandler()
        # fileHandler.setFormatter(fomatter)
        streamHandler.setFormatter(fomatter)
        # self.logger.addHandler(fileHandler)
        self.logger.addHandler(streamHandler)
        self.logger.setLevel(logging.DEBUG)

    def IsReady(self, empty, context):
        self.logger.debug("IsReady start")
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        self.logger.debug("Init start")
        self.state = provider_pb2.DIAG_STATE_INITIALIZING

        # direct method
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # copy all
        self.init_param.CopyFrom(init_param)

        # PROVIDER
        provider = provider_pb2.DialogAgentProviderParam()
        provider.name = 'DA'
        provider.description = 'Hi, DA dialog agent'
        provider.version = '0.1'
        provider.single_turn = True
        provider.agent_kind = provider_pb2.AGENT_SDS
        provider.require_user_privacy = True

        return provider

    def Terminate(self, empty, context):
        self.logger.debug("Terminate start")
        # do nothing
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    def GetUserAttributes(self, request, context):
        self.logger.debug("GetUserAttributes", "called")
        result = userattr_pb2.UserAttributeList()
        attrs = []

        # UserAttribute의 name은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.
        # Sample 예제

        lang = userattr_pb2.UserAttribute()
        lang.name = 'lang'
        lang.title = '기본 언어 설정'
        lang.type = types_pb2.DATA_TYPE_STRING
        lang.desc = '기본으로 사용할 언어를 지정해주세요.'
        attrs.append(lang)

        result.attrs.extend(attrs)
        return result

    def GetRuntimeParameters(self, empty, context):
        self.logger.debug("GetRuntimeParameters start")
        result = provider_pb2.RuntimeParameterList()
        params = []
        # Sample 예제

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = types_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database Host'
        db_host.default_value = ' '
        db_host.required = True
        params.append(db_host)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = types_pb2.DATA_TYPE_AUTH
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = ' '
        db_pwd.required = True
        params.append(db_pwd)

        return result

    def GetProviderParameter(self, empty, context):
        self.logger.debug("GetProviderParameter start")
        provider = provider_pb2.DialogAgentProviderParam()
        provider.name = 'DA'
        provider.description = 'Hi, DA dialog agent'
        provider.version = '0.1'
        provider.single_turn = True
        provider.agent_kind = provider_pb2.AGENT_SDS
        provider.require_user_privacy = True
        return provider

    def OpenSession(self, request, context):
        self.logger.debug("OpenSession start")
        session_id = request.session.id
        self.logger.debug("OpenSession id: {}".format(str(request.session.id)))
        result = talk_pb2.TalkResponse()
        res_meta = struct.Struct()
        result.response.meta.CopyFrom(res_meta)
        qna = '안녕하세요 따라쟁이 ChatBot 입니다. 저는 질문하신 내용 그대로 답변 드리는 기능을 가지고 있습니다.'
        result.response.speech.utter = qna
        # session 정보 ,session data 10k
        result.response.session_update.id = session_id
        res_context = struct.Struct()
        res_context['session_data'] = str(self.session_update_entity)
        result.response.session_update.context.CopyFrom(res_context)

        return result

    def OpenSkill(self, request, context):
        self.logger.debug("OpenSkill start")
        self.logger.debug("Open request: {}".format(str(request)))
        result = talk_pb2.TalkResponse()
        return result

    def CloseSkill(self, request, context):
        self.logger.debug("CloseSkill start")
        result = talk_pb2.CloseSkillResponse()
        return result

    def Event(self, req, context):
        self.logger.debug("Event called")
        # req = talk_pb2.EventRequest()
        answer = "Echo DA에서는 Event를 지원하지 않고 있습니다."
        event_res = talk_pb2.TalkResponse()
        event_res.response.speech.utter = answer
        event_res.response.close_skill = True
        return event_res

    def Talk(self, talk, context):
        self.logger.debug("v3 Talk called")
        # Setting
        self.logger.debug("talk : {}".format(talk.utter.utter))
        qna = talk.utter.utter
        talk_res = talk_pb2.TalkResponse()
        talk_res.response.speech.utter = qna
        talk_res.response.close_skill = True

        return talk_res


def serve():
    parser = argparse.ArgumentParser(description="DaMainServer DA")
    parser.add_argument("-p", "--port",
                        nargs="?",
                        dest="port",
                        required=True,
                        type=int,
                        help="port to access server")
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    talk_pb2_grpc.add_DialogAgentProviderServicer_to_server(
        DaMainServer(), server)
    listen = "[::]" + ":" + str(args.port)
    server.add_insecure_port(listen)
    server.start()

    try:
        while True:
            time.sleep(__ONE_DAY_IN_SECONDS__)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    serve()
