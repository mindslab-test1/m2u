#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re

reload(sys)
sys.setdefaultencoding('utf-8')

# Basic lib import
from concurrent import futures
import argparse
import grpc
import time

import logging
import logging.handlers
# pb import
from google.protobuf import empty_pb2
from google.protobuf import struct_pb2 as struct
from maum.m2u.da.v3 import talk_pb2
from maum.m2u.da.v3 import talk_pb2_grpc
from maum.m2u.da import provider_pb2
from maum.m2u.facade import types_pb2
from maum.m2u.facade import userattr_pb2


from maum.brain.qa.deepqa import bs3_pb2
from maum.brain.qa.deepqa import bs3_pb2_grpc

__EXE_PATH__ = os.path.realpath(sys.argv[0])
__BIN_PATH__ = os.path.dirname(__EXE_PATH__)
__LIB_PATH__ = os.path.realpath(__BIN_PATH__ + "/../lib/python")
sys.path.append(__LIB_PATH__)
__ONE_DAY_IN_SECONDS__ = 60 * 60 * 24


class BertMrcDa(talk_pb2_grpc.DialogAgentProviderServicer):
    # STATE
    # state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()
    # session_update 담길 session_update_entity
    session_update_entity = {}
    passage = []

    logger = logging.getLogger('bertmrc_da_logger')

    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE
        fomatter = logging.Formatter('%(asctime)s %(levelname)s %(filename)s[%(lineno)d] %(message)s')
        # fileHandler = logging.FileHandler('../logs/sds_player_da.log')
        streamHandler = logging.StreamHandler()
        # fileHandler.setFormatter(fomatter)
        streamHandler.setFormatter(fomatter)
        # self.logger.addHandler(fileHandler)
        self.logger.addHandler(streamHandler)
        self.logger.setLevel(logging.DEBUG)

    def IsReady(self, empty, context):
        self.logger.debug("IsReady start")
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        self.logger.debug("Init start")
        self.state = provider_pb2.DIAG_STATE_INITIALIZING

        # direct method
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # copy all
        self.init_param.CopyFrom(init_param)

        # PROVIDER
        provider = provider_pb2.DialogAgentProviderParam()
        provider.name = 'DA'
        provider.description = 'Hi, DA dialog agent'
        provider.version = '0.1'
        provider.single_turn = True
        provider.agent_kind = provider_pb2.AGENT_SDS
        provider.require_user_privacy = True

        return provider

    def Terminate(self, empty, context):
        self.logger.debug("Terminate start")
        # do nothing
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    def GetUserAttributes(self, request, context):
        self.logger.debug("GetUserAttributes", "called")
        result = userattr_pb2.UserAttributeList()
        attrs = []

        # UserAttribute의 name은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.
        # Sample 예제

        lang = userattr_pb2.UserAttribute()
        lang.name = 'lang'
        lang.title = '기본 언어 설정'
        lang.type = types_pb2.DATA_TYPE_STRING
        lang.desc = '기본으로 사용할 언어를 지정해주세요.'
        attrs.append(lang)

        result.attrs.extend(attrs)
        return result

    def GetRuntimeParameters(self, empty, context):
        self.logger.debug("GetRuntimeParameters start")
        result = provider_pb2.RuntimeParameterList()
        params = []
        # Sample 예제

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = types_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database Host'
        db_host.default_value = ' '
        db_host.required = True
        params.append(db_host)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = types_pb2.DATA_TYPE_AUTH
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = ' '
        db_pwd.required = True
        params.append(db_pwd)

        return result

    def GetProviderParameter(self, empty, context):
        self.logger.debug("GetProviderParameter start")
        provider = provider_pb2.DialogAgentProviderParam()
        provider.name = 'DA'
        provider.description = 'Hi, DA dialog agent'
        provider.version = '0.1'
        provider.single_turn = True
        provider.agent_kind = provider_pb2.AGENT_SDS
        provider.require_user_privacy = True
        return provider

    def OpenSession(self, request, context):
        self.logger.debug("OpenSession start")
        session_id = request.session.id
        self.logger.debug("OpenSession id: {}".format(str(request.session.id)))
        result = talk_pb2.TalkResponse()
        res_meta = struct.Struct()
        result.response.meta.CopyFrom(res_meta)
        # qna = '안녕하세요 따라쟁이 ChatBot 입니다. 저는 질문하신 내용 그대로 답변 드리는 기능을 가지고 있습니다.'

        # passage 리스트 제공

        greetings = "MRC를 테스트 해보고 싶으시군요! 현재 MRC 는 뉴스에 최적화 되어 있습니다."
        self.passage = "마인즈랩이 173억원 규모 시리즈 C 투자를 신규 유치해 누적 투자액 263억원을 달성했다." \
                       "8일 마인즈랩에 따르면 이번 투자에는 기존 투자사인 LB인베스트먼트, BSK인베스트먼트 외에도 " \
                       "신규 투자사로 IBK-NH 스몰자이언트 사모펀드(PEF), 큐캐피탈, 하나금융투자, 중소기업은행, BNK캐피탈, " \
                       "ETRI홀딩스 등 총 8개 사가 참여했다. 마인즈랩은 이번 투자를 받으면서 약 930억원 기업 가치를 평가받았다고 설>명했다." \
                       "마인즈랩은 이번 투자 유치 금액을 △R&D 역량 강화 △AI 하이브리드 컨택센터 등 기존 사업 역량 확대 △해외 시장 >진출 등에 활용할 예정이다." \
                       "캐나다 3대 AI 연구기관으로 손꼽히는 '에이미'에 합류한 것을 시작으로 알고리즘 분야에서 글로벌 수준 역량을 갖" \
                       "출 수 있도록 R&D에 투자한다. " \
                       "기존 고객센터에 AI 음성봇과 챗봇 등을 더한 하이브리드 AI 고객센터 사업을 안착시키고, " \
                       "AI 영어교육 서비스 '마이잉글리시' 성공적 런칭과 AI 스마트팩토리 사업을 확대하는 것도 중요한 목표다." \
                       "유태준 마인즈랩 대표는 '이번 투자 유치를 통해 마인즈랩이 기술 역량을 다지고, " \
                       "마인즈랩이 자회사들과 함께 펼치고 있는 AI 사업을 적극적으로 다각화할 수 있는 발판을 마련하게 됐다'며 " \
                       "'국내외 시장에서 다양한 기회를 계속해서 창출해 나가며 우리나라를 대표하는 AI 첨단 기술 중심의 유니콘 기업이자 " \
                       "글로벌 AI 서비스 기업으로 성장하겠다'고 밝혔다."
        end_greetings = "위의 뉴스 기사에서 궁금하신 내용을 질문해보시겠어요?"

        result.response.speech.utter = greetings + self.passage + end_greetings

        # result.response.speech.utter = qna
        # session 정보 ,session data 10k
        result.response.session_update.id = session_id
        res_context = struct.Struct()
        res_context['session_data'] = str(self.session_update_entity)
        result.response.session_update.context.CopyFrom(res_context)

        return result

    def OpenSkill(self, request, context):
        self.logger.debug("OpenSkill start")
        self.logger.debug("Open request: {}".format(str(request)))
        result = talk_pb2.TalkResponse()
        return result

    def CloseSkill(self, request, context):
        self.logger.debug("CloseSkill start")
        result = talk_pb2.CloseSkillResponse()
        return result

    def Event(self, req, context):
        self.logger.debug("Event called")
        # req = talk_pb2.EventRequest()
        answer = "Echo DA에서는 Event를 지원하지 않고 있습니다."
        event_res = talk_pb2.TalkResponse()
        event_res.response.speech.utter = answer
        event_res.response.close_skill = True
        return event_res

    def Talk(self, talk, context):
        self.logger.debug("v3 Talk called")
        meta_data = struct.Struct()
        # Setting
        self.logger.debug("talk : {}".format(talk.utter.utter))
        qna = talk.utter.utter

        # passage 특정 문자열 제거 전처리
        # self.passage = self.passage.replace('$$NL$$$$NL$$', '')
        # Bert MRC 연결
        parser = argparse.ArgumentParser()
        parser.add_argument('--host', type=str, default="10.122.64.84")     # 125.132.250.243
        parser.add_argument('--port', type=int, default=35002)  # 영어 35001 한국어 35002
        parser.add_argument('-p', '--passage', type=str, default=self.passage)
        parser.add_argument('-q', '--question', type=str, default=qna)

        args = parser.parse_args()
        talk_res = talk_pb2.TalkResponse()
        try:
            output = self.run(self.passage, args.question, args.host, args.port)
            for output1 in output.answers:
                text = output1.text
                meta_data["prob"] = output1.prob
                meta_data["start_idx"] = output1.start_index
                meta_data["end_idx"] = output1.end_index
        except Exception as e:
            talk_res.response.speech.utter = e
            talk_res.response.close_skill = True
            return talk_res
        talk_res.response.speech.utter = "제가 기사를 빠르게 읽어본 결과, 정답은 아래와 같습니다.! 정답 : " + text + " " + "(" + str(
            meta_data["prob"]) + ")"
        talk_res.response.close_skill = True

        return talk_res

    def get_ans(self, stub, input_context, input_question):
        if type(input_context) == list:
            return stub.GetAnswer(bs3_pb2.InputText(context='{CONTEXT_SPLIT}'.join(input_context), question=input_question))
        else:
            return stub.GetAnswer(bs3_pb2.InputText(context=input_context, question=input_question))

    def run(self, context, question, host, port):
        channel = grpc.insecure_channel(host+':'+str(port))
        stub = bs3_pb2_grpc.BertSquadStub(channel)
        return self.get_ans(stub, context, question)


def serve():
    parser = argparse.ArgumentParser(description="DaMainServer DA")
    parser.add_argument("-p", "--port",
                        nargs="?",
                        dest="port",
                        required=True,
                        type=int,
                        help="port to access server")
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    talk_pb2_grpc.add_DialogAgentProviderServicer_to_server(
        BertMrcDa(), server)
    listen = "[::]" + ":" + str(args.port)
    server.add_insecure_port(listen)
    server.start()

    try:
        while True:
            time.sleep(__ONE_DAY_IN_SECONDS__)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    serve()
