#! /usr/bin/env python
#-*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from concurrent import futures
import time
import argparse
import grpc
from google.protobuf import empty_pb2
import pymysql
import os

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.m2u.facade import userattr_pb2
from maum.m2u.da import provider_pb2
from maum.m2u.da.v1 import talk_pb2
from maum.m2u.sds import sds_pb2

# import MySQLdb.cursors

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def time_converter(time):
    if int(time) > 12:
        return '오후 ' + str(int(time) - 12)
    elif int(time) < 12:
        return '오전 ' + (time)
    elif int(time) == 12:
        return '낮 ' + time
    elif int(time) == 00:
        return '밤 ' + time


class WeatherDA(talk_pb2.DialogAgentProviderServicer):
    # STATE
    state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = provider_pb2.DialogAgentProviderParam()
    provider.name = 'weather'
    provider.description = 'Hi, weather dialog agent'
    provider.version = '0.3'
    provider.single_turn = False
    provider.agent_kind = provider_pb2.AGENT_SDS
    provider.require_user_privacy = True
    provider.user_privacy_attributes.extend(["location"])

    # PARAMETER
    sds_path = ''
    sds_domain = ''
    db_host = ''
    db_port = 0
    db_user = ''
    db_pwd = ''
    db_database = ''
    db_table = ''
    loc_timediff = '0'

    # SDS Stub
    sds_server_addr = ''
    sds_stub = None

    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE

    #
    # INIT or TERM METHODS
    #
    def get_sds_server(self):
        # sds_channel = grpc.insecure_channel(self.init_param.sds_remote_addr)
        sds_channel = grpc.insecure_channel('127.0.0.1:9906')
        resolver_stub = sds_pb2.SpokenDialogServiceResolverStub(sds_channel)

        print 'stub'
        sq = sds_pb2.ServiceQuery()
        sq.path = self.sds_path
        sq.name = self.sds_domain
        print sq.path, sq.name

        svc_loc = resolver_stub.Find(sq)
        print 'find result', svc_loc
        # Create SpokenDialogService Stub
        print 'find result loc: ', svc_loc.server_address
        self.sds_stub = sds_pb2.SpokenDialogServiceStub(
            grpc.insecure_channel(svc_loc.server_address))
        self.sds_server_addr = svc_loc.server_address
        print 'stub sds ', svc_loc.server_address

    def IsReady(self, empty, context):
        print 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.sds_path = init_param.params['sds_path']
        print 'path'
        self.sds_domain = init_param.params['sds_domain']
        print 'domain'
        self.db_host = init_param.params['db_host']
        print 'host'
        self.db_port = int(init_param.params['db_port'])
        print 'port'
        self.db_user = init_param.params['db_user']
        print 'user'
        self.db_pwd = init_param.params['db_pwd']
        print 'pwd'
        self.db_database = init_param.params['db_database']
        print 'db'
        self.db_table = init_param.params['db_table']
        print 'table'
        # CONNECT
        self.get_sds_server()
        print 'sds called'
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Terminate(self, empty, context):
        print 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    #
    # PROPERTY METHODS
    #
    def GetProviderParameter(self, empty, context):
        print 'GetProviderParameter', 'called'
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'GetRuntimeParameters', 'called'
        params = []
        result = provider_pb2.RuntimeParameterList()

        sds_path = provider_pb2.RuntimeParameter()
        sds_path.name = 'sds_path'
        sds_path.type = userattr_pb2.DATA_TYPE_STRING
        sds_path.desc = 'DM Path'
        sds_path.default_value = 'weather-v0.2'
        sds_path.required = True
        params.append(sds_path)

        sds_domain = provider_pb2.RuntimeParameter()
        sds_domain.name = 'sds_domain'
        sds_domain.type = userattr_pb2.DATA_TYPE_STRING
        sds_domain.desc = 'DM Domain'
        sds_domain.default_value = 'Weather_v0.2'
        sds_domain.required = True
        params.append(sds_domain)

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = userattr_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database IP Address or Hostname'
        db_host.default_value = '10.122.64.66'
        db_host.required = True
        params.append(db_host)

        db_port = provider_pb2.RuntimeParameter()
        db_port.name = 'db_port'
        db_port.type = userattr_pb2.DATA_TYPE_INT
        db_port.desc = 'Database Port'
        db_port.default_value = '3306'
        db_port.required = True
        params.append(db_port)

        db_user = provider_pb2.RuntimeParameter()
        db_user.name = 'db_user'
        db_user.type = userattr_pb2.DATA_TYPE_STRING
        db_user.desc = 'Database Username'
        db_user.default_value = 'root'
        db_user.required = True
        params.append(db_user)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = userattr_pb2.DATA_TYPE_STRING
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = 'root'
        db_pwd.required = True
        params.append(db_pwd)

        db_database = provider_pb2.RuntimeParameter()
        db_database.name = 'db_database'
        db_database.type = userattr_pb2.DATA_TYPE_STRING
        db_database.desc = 'Database DB'
        db_database.default_value = 'qa'
        db_database.required = True
        params.append(db_database)

        db_table = provider_pb2.RuntimeParameter()
        db_table.name = 'db_table'
        db_table.type = userattr_pb2.DATA_TYPE_STRING
        db_table.desc = 'Database Table'
        db_table.default_value = 'aircon'
        db_table.required = True
        params.append(db_table)

        loc_timediff = provider_pb2.RuntimeParameter()
        loc_timediff.name = 'loc_timediff'
        loc_timediff.type = userattr_pb2.DATA_TYPE_STRING
        loc_timediff.desc = 'Time offset'
        loc_timediff.default_value = '0'
        loc_timediff.required = True
        params.append(loc_timediff)

        result.params.extend(params)
        return result

        # old impl SEE
        # list = provider_pb2.RuntimeParameterList()
        # # dm_path = provider_pb2.RuntimeParameter()
        # dm_path = list.add()
        # dm_path.name = 'dm-path'
        # dm_path.type = provider_pb2.DataType.DATA_TYPE_STRING
        # dm_path.desc = 'SDS source path'
        # dm_path.default = 'weather'
        # dm_path.required = 'true'
        #
        # dm_name = list.add()
        # dm_name.name = 'dm-name'
        # dm_name.type = provider_pb2.DataType.DATA_TYPE_STRING
        # dm_name.desc = 'SDS project name'
        # dm_name.default = 'Weather_v0.2'
        # dm_name.required = 'true'
        # return list

    def GetUserAttributes(self, empty, context):
        print 'GetUserAttributes', 'called'
        result = userattr_pb2.UserAttributeList()

        loc = userattr_pb2.UserAttribute()
        loc.name = 'location'
        # 이 이름은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.
        loc.type = userattr_pb2.DATA_TYPE_STRING
        loc.desc = '"오늘 날씨 어때"와 같이 질문해서 자동으로 조회해줄' \
                   ' 기본 지역을 지정해주세요."'
        result.attrs.extend([loc])
        return result

    #
    # DIALOG METHODS
    #
    def Talk(self, talk, context):
        session_id = talk.session_id
        print "Session ID : " + str(session_id)
        print "[Question] ", talk.text

        # Create DialogSessionKey & set session_key
        dsk = sds_pb2.DialogueSessionKey()
        dsk.session_key = session_id

        # Dialog Open
        sdsSession = self.sds_stub.Open(dsk)

        # DB Connection
        conn = pymysql.connect(user=self.db_user,
                               password=self.db_pwd,
                               host=self.db_host,
                               database=self.db_database,
                               charset='utf8',
                               use_unicode=False)
        curs = conn.cursor(pymysql.cursors.DictCursor)

        sq = sds_pb2.SdsQuery()
        sq.session_key = sdsSession.session_key
        sq.utter = talk.text

        # Dialog UnderStand
        sa = self.sds_stub.Understand(sq)

        # Get Zone_code
        query = "select code from zone like \'"
        val_province = ''
        val_location = ''
        val_date = ''

        if sa.filled_slots.get('Weather.province') != None:
            val_province = sa.filled_slots.get('Weather.province')
            print "[val_province]" + val_province
        if sa.filled_slots.get('Weather.location') != None:
            val_location = sa.filled_slots.get('Weather.location')
            location = val_location.split(" ")
            print "[val_location]" + val_location
        if sa.filled_slots.get('Weather.date') != None:
            val_date = sa.filled_slots.get('Weather.date')
            print "[val_date]" + val_date

        s = sa.origin_best_slu.find('(')
        e = sa.origin_best_slu.find(',')
        sub_cmd = sa.origin_best_slu[s + 1:e]
        print '[SLU1 Request] ', sub_cmd

        query = "select code from zone where zone like \'"

        if val_province and val_location:
            for item in location:
                query += item
                query += '%'
            query += '\' and sub(code, 1,2) in (select substr(code, 1,2) gun from zone where zone like \''
            query += val_province
            query += '%\')'

        elif val_province or val_location:
            if val_province:
                query += val_province
                query += '%\' and substr(code, 1, 2)!=\'00\' and substr(code, 3,2)=\'00\''
            elif val_location:
                for item in location:
                    query += item
                    query += '%'
                query += "\'"
        else:
            query += "강남구%\' and substr(code, 1, 2) in(select substr(code, 1, 2) gun from zone where zone like \'서울특별시%\')";

        print '[Get ZoneCode Query] ', query
        zone_code = ''
        curs.execute(query)
        rows = curs.fetchall()
        print '[row count] ', curs.rowcount
        if curs.rowcount == 0:
            query = 'select code from zone where zone like \''
            for item in location:
                query += '%'
                query += item
                query += '%'
            query += "\'"
            print '[Re Get zone code Query] ', query
            curs.execute(query)
            rows = curs.fetchall()
            if curs.rowcount == 0:
                print 'No Zone Code!!!'
            else:
                print 'Get Zone code!!'
                print rows
                zone_code = rows[0].get('code')
        else:
            for row in rows:
                print row
            zone_code = rows[0].get('code')

        print '[Get ZoneCode] ', zone_code

        # Get Weather Information
        query = 'select * from '
        query += self.db_database
        query += '.weather where zone_code =\''
        query += zone_code
        query += "\'"
        if curs.rowcount != 0:
            if sub_cmd == 'Weather.Info':
                print "process ", sub_cmd
                if val_date:
                    if val_date in ('오늘', '지금', '현재', '현제'):
                        query += ' and date between now() + interval '
                        query += self.loc_timediff
                        query += ' hour and now() + interval 3 + '
                        query += self.loc_timediff
                        query += ' hour order by date desc limit 1'
                    elif val_date in ('내일', '네일'):
                        query += ' and date between now() + interval 1 day + interval '
                        query += self.loc_timediff
                        query += ' hour and now() + interval 1 day + interval 3 + '
                        query += self.loc_timediff
                        query += ' hour order by date desc limit 1'
                    elif val_date in ('모레', '모래'):
                        query += " and date between now() + interval 2 day + interval "
                        query += self.loc_timediff
                        query += " hour and now() + interval 2 day + interval 3 + "
                        query += self.loc_timediff
                        query += " hour order by date desc limit 1"
                    else:
                        query += " and date between now() + interval "
                        query += self.loc_timediff
                        query += " hour and now() + interval 3 + "
                        query += self.loc_timediff
                        query += " hour order by date desc limit 1"
                else:
                    query += " and date between now() + interval "
                    query += self.loc_timediff
                    query += " hour and now() + interval 3 + "
                    query += self.loc_timediff
                    query += " hour order by date desc limit 1"

            elif sub_cmd in ('Weather.Rain', 'Weather.Snow', 'Weather.Humidity',
                             'Weather.Temperature'):
                print "process ", sub_cmd
                if val_date:
                    if val_date in ('오늘', '지금', '현재', '현제'):
                        query += "and date(date) = date(now() + interval "
                        query += self.loc_timediff
                        query += " hour) order by date asc"
                    elif val_date in ('내일', '네일'):
                        query += " and date(date) = date(now() + interval 1 day + interval "
                        query += self.loc_timediff
                        query += " hour) order by date asc"
                    elif val_date in ('모레', '모래'):
                        query += "and date(date) = date(now() + interval 2 day + interval "
                        query += self.loc_timediff
                        query += " hour) order by date asc"
                    else:
                        query += "and date(date) = date(now()+ interval "
                        query += self.loc_timediff
                        query += " hour) order by date asc"
                else:
                    query += "and date(date) = date(now()+ interval "
                    query += self.loc_timediff
                    query += " hour) order by date asc"

        print '[Get info Query]' + query

        # Create SdsSlots & set Session Key
        sdsSlots = sds_pb2.SdsSlots()
        sdsSlots.session_key = sdsSession.session_key

        # Copy filled_slot to result Slot & Fill information slots
        for k, v in sa.filled_slots.items():
            sdsSlots.slots[k] = v
        curs.execute(query)
        rows = curs.fetchall()
        col_name = {'date': 'Weather.date', 'stat': 'Weather.state',
                    'temp': 'Weather.temp', 'humi': "Weather.humi",
                    'rain_prob': 'Weather.rain_prob'}
        if curs.rowcount != 0:
            if sub_cmd == 'Weather.Info':
                for row in rows:
                    for k in row.keys():
                        if row.get(k) is not None:
                            print '[key] ' + k + ' [value] ' + str(row.get(k))
                            if not k in ('id', 'zone_code'):
                                if k != 'date':
                                    sdsSlots.slots[col_name.get(k)] = str(
                                        row.get(k))
                                else:
                                    sdsSlots.slots['Weather.time'] = str(
                                        row.get(k))
            elif sub_cmd == 'Weather.Rain':
                times = []
                infotext = ''
                for row in rows:
                    for k in row.keys():
                        if row.get(k):
                            print '[key] ' + k + ' [value] ' + str(row.get(k))
                    w_state = row.get('stat')
                    if w_state.find('비') != -1:
                        time = str(row.get('date')).split(':')[0].split(' ')[1]
                        times.append(time)
                print '[times] ', times, len(times)
                if len(times) == 0:
                    print infotext
                    infotext = '비 예보가 없습니다.'
                elif len(times) == 1:
                    infotext = time_converter(times[0]) + '시에 비 예보가 있습니다.'
                    print infotext
                elif len(times) >= 2:
                    times.sort()
                    print times[0], times[-1]
                    infotext = time_converter(
                        times[0]) + '시 부터 ' + time_converter(
                        times[-1]) + '시까지 비 예보가 있습니다.'
                sdsSlots.slots['Weather.infotext'] = infotext
            elif sub_cmd == 'Weather.Snow':
                times = []
                infotext = ''
                for row in rows:
                    for k in row.keys():
                        if row.get(k):
                            print '[key] ' + k + ' [value] ' + str(row.get(k))
                    w_state = row.get('stat')
                    if w_state.find('눈') != -1:
                        time = str(row.get('date')).split(':')[0].split(' ')[1]
                        times.append(time)
                print '[times] ', times, len(times)
                if len(times) == 0:
                    print infotext
                    infotext = '눈 예보가 없습니다.'
                elif len(times) == 1:
                    infotext = time_converter(times[0]) + '시에 눈 예보가 있습니다.'
                    print infotext
                elif len(times) >= 2:
                    times.sort()
                    print times[0], times[-1]
                    infotext = time_converter(
                        times[0]) + '시 부터 ' + time_converter(
                        times[-1]) + '시까지 눈 예보가 있습니다.'
                    print infotext
                sdsSlots.slots['Weather.infotext'] = infotext
            sdsSlots.slots[sub_cmd] = '<Count>1</Count>'
        else:
            sdsSlots.slots['Weather.Info'] = '<Count>0</Count>'

        conn.close()

        # Sedn result slot & Get response
        sdsUtter = self.sds_stub.FillSlots(sdsSlots)

        print "[System output] " + sdsUtter.response
        talk_res = provider_pb2.TalkResponse()
        talk_res.text = sdsUtter.response
        return talk_res

    def Close(self, req, context):
        print 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        ses = sds_pb2.SdsSession()
        ses.session_key = req.session_id
        dsk = self.sds_stub.Close(ses)
        return talk_stat


def serve():
    parser = argparse.ArgumentParser(description='Weather DA')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    provider_pb2.add_DialogAgentProviderServicer_to_server(
        WeatherDA(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)

    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
