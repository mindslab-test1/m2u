#!/usr/bin/env bash

#set -xv
shopt -s nocasematch

function usage() {
  echo "docker-buiid.sh [centos | ubuntu] [version] : specify OS and version to build"
  echo "docker-build.sh --help : show this message"
  echo "  Possible versions for CentOS:"
  echo "    7.6.1810 (*default)"
  echo "    7.5.1804"
  echo "    7.4.1708"
  echo "    7.3.1611"
  echo
}

if [ $# -lt 1 ]; then
  echo "Error: Illegal number of parameters"
  echo
  usage
  exit 1
fi

if [ "$1" = "--help" ]; then
  usage
  exit 0
fi

OS=$1
CODE_VERSION=
TAG=
TARGET_PATH="${OS}/"

case ${OS} in
  "centos")
    TAG=0.21
    if [ -z "$2" ]; then
      CODE_VERSION="7.6.1810"
    else
      CODE_VERSION="$2"
    fi
    IFS='.' read -ra CODES <<< "${CODE_VERSION}"
    IMAGE_VERSION=${CODES[0]}.${CODES[1]}
    ;;
  "ubuntu")
    TAG=0.21
    if [ -z "$2" ]; then
      CODE_VERSION="16.04"
    else
      CODE_VERSION="$2"
    fi
    IMAGE_VERSION=${CODE_VERSION}
    ;;
  *)
    echo "Error: OS '${OS}' is not supported."
    echo
    usage
    exit 1
    ;;
esac


echo "BUILD for ${OS}-${IMAGE_VERSION}:${TAG}"
echo

docker build \
    --build-arg CODE_VERSION=${CODE_VERSION} \
    --build-arg TAG=${TAG} \
    -t maum-${OS}-${IMAGE_VERSION}:${TAG} \
    ${TARGET_PATH}
