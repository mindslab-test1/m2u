#!/usr/bin/env bash

# This is a sample for run docker container for ubuntu maum build.
# Host machine has empty directory named ~/ubuntu
#
# three volume mapping required.
#
# [/home/maum/git]
# [/home/maum/maum]
# [/home/maum/.maum-build]
#

mkdir -p ~/ubuntu/.maum-build
mkdir -p ~/ubuntu/maum

docker run -t -i \
  -d \
  -h maum-build \
  --name u1604-build \
  -v ~/.gradle:/home/minds/.gradle \
  -v ~/ubuntu/.maum-build:/home/minds/.maum-build \
  -v ~/ubuntu/maum:/home/minds/maum\
  -v ~/git:/home/minds/git \
  -p 5022:22 \
  maum-ubuntu-16.04:0.21

mkdir -p ~/centos/.maum-build
mkdir -p ~/centos/maum

docker run -t -i \
  -d \
  -h maum-build \
  --name c76-build \
  -v ~/.gradle:/home/minds/.gradle \
  -v ~/centos/.maum-build:/home/minds/.maum-build \
  -v ~/centos/maum:/home/minds/maum\
  -v ~/git:/home/minds/git \
  -p 4022:22 \
  maum-centos-7.6:0.21
