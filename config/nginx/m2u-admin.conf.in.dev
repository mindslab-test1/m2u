user ${SYSUSER};

worker_processes  2;
daemon off;
pid nginx.pid;
error_log ${LOGS_DIR}/nginx-m2u-admin-error.log warn;

events {
  worker_connections  1024;
}

http {

  include basic.conf;
  log_format  main  '$$remote_addr - $$remote_user [$$time_local] "$$request" '
                    '$$status $$body_bytes_sent "$$http_referer" '
                    '"$$http_user_agent" "$$http_x_forwarded_for"';

  access_log ${LOGS_DIR}/nginx-m2u-admin-access.log main;

  include log.conf;

  include mime.types;
  include type-gzip.conf;

  include m2u-admin-upstream.conf;

  # Get real scheme for this connection.
  map $$http_x_forwarded_proto $$real_scheme {
    default $$http_x_forwarded_proto;
    ''      $$scheme;
  }

  map $$real_scheme $$ssl {
    http off;
    https on;
  }

  map $$http_upgrade $$connection_upgrade {
    default upgrade;
    '' close;
  }

  include expires.conf;

  server {
    listen ${M2U_ADMIN_WEB_PORT};
    server_name localhost;
    underscores_in_headers on;

    root ${MAUM_ROOT}/apps/admin-web;

    include headers.conf;
    include m2u-admin-locations.conf;
  }
}
