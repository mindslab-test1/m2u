#
# maum.ai M2U Configuration
#

maum.runtime.home = ${MAUM_ROOT}
pid.dir = ${MAUM_ROOT}/run
logs.dir = ${LOGS_DIR}
conf.dir = ${MAUM_ROOT}/etc
run.dir = ${MAUM_ROOT}/run
da.dir = ${MAUM_ROOT}/da
lg.logs.dir = ${MAUM_ROOT}/tlo

# LOG SETTINGS
logs.max-file-size = ${LOG_MAX_FILE_SIZE}
logs.daily.rotate = ${LOG_DAILY_ROTATE}
logs.rotate.count = ${LOG_ROTATE_COUNT}
logs.level.default = ${SPDLOG_DEFAULT_LEVEL}
logs.level.m2u-damd = ${SPDLOG_DAM_LEVEL}
logs.level.m2u-itfd = ${SPDLOG_ITF_LEVEL}
logs.level.m2u-routed = ${SPDLOG_ROUTER_LEVEL}
logs.level.m2u-ttsd = ${SPDLOG_TTS_LEVEL}
logs.monitor.plugin = ${SPDLOG_MONITOR_PLUGIN}
logs.monitor.write-function = ${SPDLOG_MONITOR_WRITE_FUNCTION}
logs.monitor.level = ${SPDLOG_MONITOR_LEVEL}
logs.masking.enable = ${LOGS_MASKING_ENABLE}

## M2U MAP
map.export.port = ${M2U_MAP_PORT}
map.listen = 0.0.0.0:${M2U_MAP_PORT}
map.export = ${M2U_MAP_IP}:${M2U_MAP_PORT}
map.default.expectspeech = ${M2U_MAP_DEFAULT_EXPECTSPEECH}
map.sessionmanager.cyclesec = ${M2U_MAP_SESSION_MANAGER_CYCLESEC}
map.sessionmanager.refval = ${M2U_MAP_SESSION_MANAGER_REFVAL}
map.grpc.deadline = ${M2U_MAP_CALL_DEADLINE_SEC}
map.server.max.connection.idlesec = ${M2U_MAP_SERVER_MAX_CONNECTION_IDLE_SEC}
map.user.export.speechcontext = ${M2U_MAP_USER_EXPORT_SPEECH_CONTEXT}

## M2U AUTH(MAP)
auth.export.port = ${M2U_MAP_AUTH_PORT}
auth.grpc.timeout = ${M2U_MAP_AUTH_TIMEOUT}
auth.export.ip = ${M2U_MAP_AUTH_IP}:${M2U_MAP_AUTH_PORT}

## M2U INTERNAL AUTH(MAP)
internal.auth.export.port = ${M2U_MAP_INTERNAL_AUTH_PORT}
internal.auth.grpc.timeout = ${M2U_MAP_INTERNAL_AUTH_TIMEOUT}
internal.auth.export.ip = ${M2U_MAP_INTERNAL_AUTH_IP}:${M2U_MAP_INTERNAL_AUTH_PORT}

## M2U Image Recognition Server
image.export.ip = ${M2U_MAP_IMAGE_SERVER_IP}:${M2U_MAP_IMAGE_SERVER_PORT}

## BrainQA Dialog Info
maum.qa.listen.ip = 0.0.0.0
maum.qa.listen.port = ${BRAIN_QA_PORT}

### M2U ROUTER
routed.listen = 0.0.0.0:${M2U_ROUTER_PORT}
routed.export = ${M2U_ROUTER_IP}:${M2U_ROUTER_PORT}
routed.grpc.timeout = ${M2U_ROUTER_TIMEOUT}
routed.grpc.deadline = ${M2U_ROUTER_CALL_DEADLINE_SEC}
routed.plugin.count = 0
routed.plugin.1.so = ${MAUM_ROOT}/libexec/log-plugins/libcalllog-plugin-sample.so
routed.plugin.1.entrypoint = init
router.plugin.number = 001
router.process.name = ROUTER
router.plugin.out-dir = ${MAUM_ROOT}/calllog

## M2U FRONT REST
front.rest.server.port = ${M2U_FRONT_REST_PORT}
front.rest.server.ip = ${M2U_FRONT_REST_IP}

## HAZELCAST
#hazelcast.group.name = dev
#hazelcast.group.password = dev-pass
hazelcast.group.name = m2u
hazelcast.group.password = xmqtTcmVPCAVCt3d79Vd
hazelcast.server.count = 1
hazelcast.server.ip.1 = ${HAZELCAST_SERVER_IP}
hazelcast.server.port = ${HAZELCAST_SERVER_PORT}
hazelcast.invocation.timeout = ${HAZELCAST_INVOCATION_TIMEOUT_SEC}

## BRAIN STT
brain-stt.sttd.front.remote = ${BRAIN_STT_IP}:${BRAIN_STT_PORT}
brain-stt.sttd.default.model = baseline
#brain-stt.sttd.protocol.type = legacy

## IMAGED (TODO: test again)
imaged.export.ip = ${BRAIN_IDR_IP}
imaged.export.port = ${BRAIN_IDR_PORT}
imaged.enabled = true

## TEXT ANALYZER COMMON
ta.resource.path = ${MAUM_ROOT}/resources/ta
ta.resource.path.kor = ${MAUM_ROOT}/resources/ta/kor
ta.resource.path.eng = ${MAUM_ROOT}/resources/ta/eng

## DOMAIN RESOLVER
maum-ta.cl.front.remote = ${TA_SERVER_IP}:${BRAIN_CL_PORT}

### LANG ANALYZER
brain.nlp.1.kor.remote = ${TA_SERVER_IP}:${BRAIN_NLP_1_KOR_PORT}
brain.nlp.2.kor.remote = ${TA_SERVER_IP}:${BRAIN_NLP_2_KOR_PORT}
brain.nlp.2.eng.remote = ${TA_SERVER_IP}:${BRAIN_NLP_2_ENG_PORT}

### DAM SERVICE SERVER
dam-svcd.listen = 0.0.0.0:${DAM_SERVER_PORT}
dam-svcd.export.ip = ${DAM_SERVER_IP}
dam-svcd.export.port = ${DAM_SERVER_PORT}
dam-svcd.port.start = 11000
dam-svcd.state.file = dam.state.json
dam-svcd.child.wait = 1000
dam-svcd.random.min.port = ${DA_MIN_PORT}
dam-svcd.random.max.port = ${DA_MAX_PORT}
dam-svcd.grpc.timeout = ${DAM_SERVER_TIMEOUT}
# dam-svcd.java.path = /usr/bin/java
# dam-svcd.python.path = /usr/bin/phthon2.7

### DAI SVC
daisvc.conf.dir = ${DAI_SVC_CONF_DIR}

## M2U POOL
pool.listen.port = ${M2U_POOL_PORT}
pool.export = ${M2U_POOL_IP}:${M2U_POOL_PORT}
pool.grpc.timeout = ${M2U_POOL_TIMEOUT}
pool.ping.period.ms = ${M2U_POOL_PING_CHECK_PERIOD_MS}

## M2U ADMIN
admin.listen = 0.0.0.0:${M2U_ADMIN_PORT}
admin.export = ${M2U_ADMIN_IP}:${M2U_ADMIN_PORT}
admin.export.ip = ${M2U_ADMIN_IP}
admin.export.port = ${M2U_ADMIN_PORT}
admin.grpc.timeout = ${M2U_ADMIN_TIMEOUT}

## M2U SUPERVISORD REGISTRATION
svreg.targets[] = ${M2U_ADMIN_IP}:${M2U_ADMIN_PORT}

### M2U LOGGER
logger.router.enable = ${LOGGER_ROUTER_ENABLE}
logger.router.session = ${LOGGER_ROUTER_SESSION}
logger.router.talk = ${LOGGER_ROUTER_TALK}
logger.router.classification = ${LOGGER_ROUTER_CLASSIFICATION}
logger.startsec = ${LOGGER_START_SEC}
logger.periodsec = ${LOGGER_PERIOD_SEC}
logger.session.periodsec = ${LOGGER_SESSION_PERIOD_SEC}
logger.session.monitor.expiration = ${LOGGER_SESSION_EXPIRE}
logger.zombie.chatbot.session.timeout = ${LOGGER_ZOMBIE_CHATBOT_SESSION_TIMEOUT}
logger.session.channels = ${LOGGER_SESSION_CHANNELS}
logger.lock.name = ${LOGGER_LOCK_NAME}
session.lock.name = ${SESSION_LOCK_NAME}

## DB Connection Pool Settings
logger.driver.classname = ${LOGGER_DB_DRIVER}
logger.user = ${LOGGER_DB_USER}
logger.pass = ${LOGGER_DB_PASSWORD}
logger.url = ${LOGGER_DB_URL}
logger.init.size = 10
logger.max.size = 100

## M2U INTENT-FINDER
itfd.customscript.path = ${MAUM_ROOT}/custom-script
itf.pcre.enable = true
itf.pcre.resource.path = ${MAUM_ROOT}/resources/pcre
itf.dnn.model.path = ${MAUM_ROOT}/trained/classifier

itfd.plugin.count = 0
itfd.plugin.1.so = ${MAUM_ROOT}/libexec/log-plugins/libcalllog-plugin-sample.so
itfd.plugin.1.entrypoint = init

brain-ta.cl.model.dir = ${MAUM_ROOT}/trained/classifier
brain-ta.resource.2.kor.path = ${MAUM_ROOT}/resources/nlp2-kor
brain-ta.resource.2.eng.path = ${MAUM_ROOT}/resources/nlp2-eng

brain-xdc.resource.path = ${MAUM_ROOT}/trained/han
brain-xdc.ip = ${BRAIN_XDC_HOST}
brain-xdc.port = ${BRAIN_XDC_PORT}

# intent-finder cs server ip, port
itf.cs.py.ip = ${INTENT_FINDER_CS_PY_IP}
itf.cs.py.port = ${INTENT_FINDER_CS_PY_PORT}
itf.cs.java.port = ${INTENT_FINDER_CS_JAVA_PORT}
itf.cs.js.port = ${INTENT_FINDER_CS_JS_PORT}

## M2U SPEECH SYNTHESIZER
speech-synthesizer.listen = 0.0.0.0:${M2U_TTS_PORT}
speech-synthesizer.export = ${M2U_TTS_IP}:${M2U_TTS_PORT}
speech-synthesizer.port = ${M2U_TTS_PORT}
speech-synthesizer.grpc.timeout = ${M2U_TTS_TIMEOUT}
speech-synthesizer.encoding = ${M2U_TTS_ENCODING}
speech-synthesizer.sample-rate = ${M2U_TTS_SAMPLE_RATE}
speech-synthesizer.lang = ko_KR
#speech-synthesizer.interface = legacy

## Dialog Trigger (Optional) set manually to enable this function.
# dialog.trigger.ip = 0.0.0.0
# dialog.trigger.port = ${DIALOG_TRIGGER_PORT}


## M2U Rest java conf

# M2U Rest java configuration [option : default, all]
# TODO server.port 대신 다른 name으로rest port  세팅
server.port = ${M2U_REST_PORT}

## MQ Appender
mq.appender.ip = 10.231.88.62
mq.appender.port = 28090
mq.appender.username = total
mq.appender.password = total
mq.appender.queuename = total
mq.appender.virtualhost = total

## MAP Forwarder Secure conf
map.forwarder.proxy.port = ${M2U_FORWARDER_MAP_PORT}
secure.map.proxy.export = ${M2U_SECURE_MAP_IP}:${M2U_SECURE_MAP_PORT}
secure.map.proxy.port = ${M2U_SECURE_MAP_PORT}
map.forwarder.cryptor.class.name = ${M2U_CRYPTOR_CLASS_NAME}
map.forwarder.anomaly.message.filter.class.name = ${M2U_MESSAGE_FILTER_CLASS_NAME}

## ADMIN REST CONF
admin.rest.db.connector = ${LOGGER_DB_CONNECTOR}
admin.rest.db.ip = ${LOGGER_DB_IP}
admin.rest.db.port = ${LOGGER_DB_PORT}
admin.rest.db.database = ${LOGGER_DB_DATABASE}
admin.rest.db.user = ${LOGGER_DB_USER}
admin.rest.db.password = ${LOGGER_DB_PASSWORD}
