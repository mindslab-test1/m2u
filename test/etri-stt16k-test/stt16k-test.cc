#include "stt-client.h"
#include <stdio.h>
#include <iostream>

int main(int argc, char **argv) {
  if (argc != 4) {
    std::cout << "Usage: " << argv[0] << " filename ip port" << std::endl;
    return -1;
  }

  FILE *fp = fopen(argv[1], "rb");
  std::string ip = argv[2];
  int port = std::stoi(argv[3]);

  SttClient client(ip, port);

  if (client.IsReady()) {
    int read_cnt;
    int16_t buf[500];
    while ((read_cnt = fread(buf, 2, 500, fp)) > 0) {
      client.Send((char *) buf, size_t(read_cnt * sizeof(int16_t)));
    }
    client.SendDone();
    std::string result = client.GetResult();
    std::cout << "result : " << result << std::endl;
  }

  return 0;
}
