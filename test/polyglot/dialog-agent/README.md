da.sh스크립트의 포트번호를 변경해줘야 합니다.
da가 떠 있는 포트 번호 확인하는 방법
1. `ps -ef | grep $dialog-agent` 명령을 통해 실행중인 da의 포트 번호를 확인합니다.

ex) minds    10547  9684  0 11:12 ?        00:00:03 /usr/bin/java -jar /home/minds/maum/da/dialog-agent-1.0.0-all.jar -p 30001 sample_dai-CHATBOT1-cs.custom.deposit.final_in.savings.날씨.온도-kor

2. 1번의 결과에서 -p 뒤의 숫자가 포트 번호입니다.

3. da.sh스크립트의 포트를 30001로 바꿔줍니다.