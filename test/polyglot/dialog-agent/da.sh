#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service=maum.m2u.da.v3.DialogAgentProvider
endpoint=localhost:30001
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include

## execution part
cat dialog-agent-provider-open-session.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/OpenSession \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-provider-open-skill.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/OpenSkill \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-provider-talk.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/Talk \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-provider-event.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/Event \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-provider-close-skill.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/CloseSkill \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-init.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/Init \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-is-ready.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/IsReady \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-terminate.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/Terminate \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-get-runtime-parameters.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/GetRuntimeParameters \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-agent-get-provider-parameter.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/GetProviderParameter \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}
