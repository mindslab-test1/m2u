#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service=maum.m2u.server.DialogAgentManager
endpoint=localhost:9907
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include

## execution part
cat set-dam-name.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/SetDamName \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat get-runtime-parameters.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/GetRuntimeParameters \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat get-provider-parameter.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/GetProviderParameter \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat get-user-attributes.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/GetUserAttributes \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat get-executable-da.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/GetExecutableDA \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}