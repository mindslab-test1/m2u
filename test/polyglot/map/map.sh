#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service=maum.m2u.map.MaumToYouProxyService
endpoint=localhost:9911
metadata=m2u-auth-token:1602a950-e651-11e1-84be-00145e76c700,m2u-auth-internal:m2u-auth-internal
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include

## execution part
cat dialog-open.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/EventStream \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-text-to-text.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/EventStream \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-text-to-speech.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/EventStream \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-image-to-text.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/EventStream \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-image-to-speech.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/EventStream \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat list-async-interfaces.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/ListAsyncInterfaces \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat dialog-close.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/EventStream \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

# --------------------stt서버에 보낼 때--------------------
# ${MAUM_ROOT}/etc/m2u.conf에서 stt서버 ip를 바꿔준다.
# ex) brain-stt.sttd.front.remote = 125.132.250.242:9801
# stt 서버는 ssh로 접속 가능
# ssh -p 2221 msl@125.132.250.242
# pw : msl@1234~~
# 해당 서버의 ${MAUM_ROOT}/logs 디렉토리에서 stt 서버 로그 확인가능

# cat dialog-open.json | java -jar ${jar_dir} \
# --command=call \
# --full_method=${service}/EventStream \
# --endpoint=${endpoint} \
# --metadata=${metadata} \
# --proto_discovery_root=${proto_discovery_root} \
# --add_protoc_includes=${add_protoc_includes}

# cat dialog-speech-to-text.json | java -jar ${jar_dir} \
# --command=call \
# --full_method=${service}/EventStream \
# --endpoint=${endpoint} \
# --metadata=${metadata} \
# --proto_discovery_root=${proto_discovery_root} \
# --add_protoc_includes=${add_protoc_includes}