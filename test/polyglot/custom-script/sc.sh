#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service=maum.m2u.router.v3.CustomScriptRunner
endpoint=localhost:9922
metadata=x-operation-sync-id:bbe14c2b-c122-4406-a144-15611c8d83dd
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include


TEMP_DIR=.temp

test -d $TEMP_DIR | mkdir -p $TEMP_DIR

## execution part
cat sc-classify.json | java -Djava.io.tmpdir=$TEMP_DIR \
-jar ${jar_dir} \
--command=call \
--full_method=${service}/Classify \
--endpoint=${endpoint} \
--metadata=${metadata} \
--use_reflection=false \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

rm -rf $TEMP_DIR
