#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service1=maum.m2u.map.AuthenticationProvider
service2=maum.m2u.map.AuthorizationProvider
endpoint=localhost:9951
metadata=m2u-auth-internal:m2u-auth-internal
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include

## execute part
cat map-sign-in.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service1}/SignIn \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat map-multi-factor-verify.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service1}/MultiFactorVerify \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat map-sign-out.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service1}/SignOut \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat map-is-valid-success.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service2}/IsValid \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat map-get-user-info-success.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service2}/GetUserInfo \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}