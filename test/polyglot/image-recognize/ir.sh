#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service=maum.brain.idr.ImageRecognitionService
endpoint=localhost:9951
metadata=m2u-auth-internal:m2u-auth-internal
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include

## execution part
cat ir-recognize-image.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/RecognizeImage \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}