#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service=maum.m2u.router.v3.TalkRouter
endpoint=localhost:9930
metadata=x-operation-sync-id:bbe14c2b-c122-4406-a144-15611c8d83dd
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include

## execution part
cat open-route.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/OpenRoute \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat talk-route.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/TalkRoute \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat event-route.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/EventRoute \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat feedback-route.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/Feedback \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat close-route.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/CloseRoute \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}