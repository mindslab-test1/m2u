#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service=maum.m2u.server.DialogAgentInstancePool
endpoint=localhost:9931
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include

## execution part
cat pool-is-ready.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/IsReady \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat pool-register.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/Register \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat pool-unregister.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/Unregister \
--endpoint=${endpoint} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}