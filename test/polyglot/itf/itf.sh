#!/usr/bin/env bash

## variables definition
jar_dir=${MAUM_ROOT}/lib/polyglot_deploy.jar
service=maum.m2u.router.v3.IntentFinder
endpoint=localhost:10050
metadata=x-operation-sync-id:bbe14c2b-c122-4406-a144-15611c8d83dd
proto_discovery_root=${MAUM_ROOT}/include/maum/m2u
add_protoc_includes=${MAUM_ROOT}/include

## execution part
cat itf-find-intent.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/FindIntent \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}

cat itf-retry-find-intent.json | java -jar ${jar_dir} \
--command=call \
--full_method=${service}/RetryFindIntent \
--endpoint=${endpoint} \
--metadata=${metadata} \
--proto_discovery_root=${proto_discovery_root} \
--add_protoc_includes=${add_protoc_includes}