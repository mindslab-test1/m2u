## 서비스 실행법

서비스 실행을 위해서는 `$MAUM_ROOT/trained` 경로에 hmd, dnn 학습모델이 있어야 합니다.

1. svd로 서비스 프로세스들을 실행합니다. (brain-cl프로세스도 추가적으로 실행합니다)

2. `$MAUM_ROOT/samples/default-setting-import.sh`을 실행하여 하젤케스트에 데이터를 import 합니다.

3. `$MAUM_ROOT/da` 디렉토리로 이동하여 다음 명령을 실행합니다.
```
$ m2u-darun -c CHATBOT1 -s custom cs deposit savings 날씨 온도 주식 환율 final_in etc -v v3 'java -jar dialog-agent-1.0.0-all.jar
```

4. `svctl`로 map과 map-auth 프로세스를 실행합니다.

5. `$MAUM_ROOT/bin` 디렉토리의 `m2u-map-cli`를 실행하여 테스트 진행합니다.

6. 잘 진행되는지 `itf-sample log`파일을 보고 확인합니다.
