# 설치 디렉토리의 구성

설치된 디렉토리는 다음과 같은 구성을 가지고 있습니다.

## 설치 폴더 목록
- apps
  - 웹 관련 소스들이 포함된 폴더
  - 각 하위 디렉토리 별로 서로 다른 목적의 서비스들이 존재
- bin
  - M2U 플랫폼의 각 서버를 가동시키는 파일이 포함된 폴더
- custom-script
  - 인텐트 파인더에서 사용하는 커스텀 스크립트 파일이 들어있는 폴더, 기본적으로 echo 파이썬 파일 내장
- da
  - 다이얼로그 에이전트 실행파일 폴더
- db
  - 헤이즐캐스트 맵 저장 DB 폴더
- etc
  - M2U 플랫폼에 필요한 설정 및 시스템 관련 설정 파일 폴더
- include
  - M2U 플랫폼 실행에 필요한 proto 폴더
  - C++ 써드 파티 개발을 위한 헤더 파일
- lib
  - M2U 플랫폼 실행에 필요한 라이브러리 폴더
- libexec
  - M2U 플랫폼 외부 확장 라이브러리 폴더
- logs
  - M2U 프로세스가 실행되면서 기록되는 로그가 저장되는 폴더로 기본 프로세스와 수퍼바이저에서 기록되는 로그로 분류
- resources
  - M2U 플랫폼에 필요한 리소스 파일들이 설치된 폴더
- run
  - M2U 플랫폼 실행중에 임의로 필요한 파일들이 생성/수정/삭제되는 폴더
- samples
  - 테스트 코드 폴더
- share
  - grpc 관련 리소스
- test
  - m2u 폴리글랏(polyglot) 테스트 쉘이 들어있는 폴더
- trained
  - 학습 이미지 관리 폴더
- workspace
  - 학습중인 데이터 관리 폴더


### 설정 파일들의 위치
설정파일들은 앞서 setup config를 통해 생성되는 파일과 같이 기본적으로 `$MAUM_ROOT/etc` 에 있습니다.  
다음은 그 상세한 목록입니다.


- in `$MAUM_ROOT/apps/admin-rest/server` directory:
  - config.json
  - config.production.json
  - config.staging.json
  - datasources.production.js
  - datasources.staging.json


- in `$MAUM_ROOT/etc` directory:
  - brain-sds.conf
  - brain-stt.conf
  - brain-ta.conf
  - hazelcast.xml
  - hazelcast-client.xml
  - libmaum.conf
  - m2u.conf


- in `$MAUM_ROOT/etc/logback` directory:
  - dialog-agent-instance-svc-logback.xml
  - hazelcast-repack-logback.xml
  - intent-finder-svc-logback.xml
  - logger-logback.xml
  - m2u-async-proxy-logback.xml  
  - pool-logback.xml
  - rest-logback.xml
  - service-admin-logback.xml


- in `$MAUM_ROOT/etc/nginx` directory:
  - m2u-admin.conf
  - m2u-admin-upstream.conf


- in `$MAUM_ROOT/samples` directory:
  - m2u-default-setting.json


- in `$MAUM_ROOT/etc/supervisor` directory:
  - supervisord.conf


- in `$MAUM_ROOT/etc/supervisor/conf.d` directory:
  - brain-sds.conf
  - brain-stt.conf
  - brain-ta.conf
  - m2u.conf


- in `$MAUM_ROOT/etc/supervisor/export` directory:
  - maumai-sv.conf
  - maumai-sv.service
  - maumai-sv-root.service