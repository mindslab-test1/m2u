
## M2U CONFIG VARIABLES INFO

**NAME** | **VALUE** | **DESCRIPTIONS**
----- | ----- | -----
ADMIN_REST_IP | 0.0.0.0 | Admin rest IP
ADMIN_REST_PORT | 9920 | Admin rest port
ALT_SVC_HOST | www.${HOSTIP} | nginx alt hostname(www.localhost)
AUTH_POLICY_NODE_MAX_SIZE | 10000 | Maximum number of map entries in auth policy cluster member
BRAIN_CL_IP | ${HOSTIP} | brain cl IP(localhost)
BRAIN_CL_PORT | 9820 | brain cl port
BRAIN_HMD_IP | ${HOSTIP} | brain hmd IP(localhost)
BRAIN_HMD_PORT | 9815 | brain hmd port
BRAIN_IDR_IP | 사용하는 IDR IP | brain IDR IP
BRAIN_IDR_PORT | 사용하는 IDR IP | brain IDR port
BRAIN_NLP_1_KOR_PORT | 9811 | Brain nlp 1 Korean port
BRAIN_NLP_2_KOR_PORT | 9813 | Brain nlp 2 Korean port
BRAIN_NLP_2_ENG_PORT | 9814 | Brain nlp 2 English port
BRAIN_NLP_3_KOR_IP | ${HOSTIP} | Brain nlp 3 Korean IP
BRAIN_NLP_3_KOR_PORT | 9823 | Brain nlp 3 Korean port
BRAIN_QA_PORT | 15142 | brain QA port
BRAIN_STT_IP | ${HOSTIP} | brain stt IP address(localhost)
BRAIN_STT_PORT | 9801 | brain stt port
CHATBOT_NODE_MAX_SIZE | 10000 | Maximum number of map entries in chatbot cluster member
CLASSIFICATION_RECORD_NODE_MAX_SIZE | 10000 | Maximum number of map entries in classification record cluster member
COREVOICE_TTS_SERVER_ENG_PORT | 사용하는 한글 TTS 포트 | corevoice tts server english port
COREVOICE_TTS_SERVER_IP | 사용하는 TTS IP | corevoice tts server IP
COREVOICE_TTS_SERVER_KOR_PORT | 사용하는 한글 TTS 포트 | corevoice tts server korean port
DAI_SVC_CONF_DIR | /etc/supervisor/conf.d/m2u-dai.conf | DAI SVC conf file dir path
DAI_SVC_LOGBACK | m2u-daisvc | DAI_SVC_LOGBACK
DAI_SVC_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/dialog-agent-instance-svc-LOGBACK.xml | DAI_SVC LOGBACK full path or name
DAM_DEFAULT_IP | localhost | DAM default IP address
DAM_SERVER_IP | ${HOSTIP} | DAM server IP address(localhost)
DAM_SERVER_PORT | 9907 | DAM server port
DAM_SERVER_TIMEOUT | 10000 | DAM server grpc timeout
DA_LOGBACK | m2u-da | DA_LOGBACK
DA_MAX_PORT | 30300 | DA_MAX_PORT
DA_MIN_PORT | 30000 | DA_MIN_PORT
DEVICES_NODE_MAX_SIZE | 10000 | Maximum number of map entries in devices cluster member
DIALOG_AGENT_ACTIVATION_NODE_MAX_SIZE | 10000 | Maximum number of map entries in dialog agent activation cluster member
DIALOG_AGENT_INSTANCE_EXEC_NODE_MAX_SIZE | 10000 | Maximum number of map entries in dialog agent instance exec cluster member
DIALOG_AGENT_INSTANCE_NODE_MAX_SIZE | 10000 | Maximum number of map entries in dialog agent instance cluster member
DIALOG_AGENT_INSTANCE_RESOURCE_NODE_MAX_SIZE | 10000 | Maximum number of map entries in dialog agent instance resource cluster member
DIALOG_AGENT_MANAGER_NODE_MAX_SIZE | 10000 | Maximum number of map entries in dialog agent manager cluster member
DIALOG_AGENT_NODE_MAX_SIZE | 10000 | Maximum number of map entries in dialog agent cluster member
DIALOG_TRIGGER_PORT | 9960 | dialog trigger port
EXPORT_HOST | ${HOSTIP} | Host IP or DNS name for external access
HAZELCAST_INVOCATION_TIMEOUT_SEC | 120 | Hazelcast client invocation timeout seconds
HAZELCAST_SERVER_IP | ${HOSTIP} | Hazelcast server IP address(localhost)
HAZELCAST_SERVER_PORT | 5701 | Hazelcast server port
HZC_LOGBACK | m2u-hzc | Hazelcast LOGBACK
HZC_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/hazelcast-repack-LOGBACK.xml | Hazelcast LOGBACK full path or name
HZC_MANCENTER_IP | ${HOSTIP} | Hazelcast management center IP
HZC_MANCENTER_PORT | 9988 | Hazelcast management center port
HZC_MAPSTORE_LOGBACK | m2u-hzc-mapstore | Hazelcast mapstore LOGBACK
HZC_MAUM_LOGBACK | m2u-hzc-maum | Hazelcast maum LOGBACK
HZC_PORTABLE_LOGBACK | m2u-hzc-portable | Hazelcast portable LOGBACK
INTENT_FINDER_CS_JAVA_PORT | 9923 | Intent-Finder Custom ScrIPt JAVA port
INTENT_FINDER_CS_JS_PORT | 9924 | Intent-Finder Custom ScrIPt JS port
INTENT_FINDER_CS_PY_IP | localhost | Intent-Finder Custom ScrIPt PY IP
INTENT_FINDER_CS_PY_PORT | 9925 | Intent-Finder Custom ScrIPt PY port
INTENT_FINDER_INSTANCE_NODE_MAX_SIZE | 10000 | Maximum number of map entries in intent finder instance cluster member
INTENT_FINDER_NODE_MAX_SIZE | 10000 | Maximum number of map entries in intent finder cluster member
INTENT_FINDER_POLICY_NODE_MAX_SIZE | 10000 | Maximum number of map entries in intent finder policy cluster member
ITF_INSTANCE_IP | localhost | ITF instance default IP
ITF_SVC_LOGBACK | m2u-itfsvc | ITF SVC LOGBACK
ITF_SVC_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/intent-finder-svc-LOGBACK.xml | ITF SVC LOGBACK full path or name
LOGBACK_LOG_LEVEL | DEBUG | java LOGBACK log level
LOGGER_DB_CONNECTOR | mysql | logger db connector
LOGGER_DB_DATABASE | admin_console | logger db database
LOGGER_DB_DRIVER | com.mysql.jdbc.Driver | logger db driver
LOGGER_DB_IP | ${HOSTIP} | logger db IP(localhost)
LOGGER_DB_PASSWORD | ggoggoma | logger db password
LOGGER_DB_PORT | 3306 | logger db port
LOGGER_DB_URL | jdbc:mysql://localhost:3306/admin_console | logger db url
LOGGER_DB_USER | maum | logger db user
LOGGER_FRONT_ENABLE | false | The Logger enables logging Front
LOGGER_FRONT_SESSION | false | The Logger logs Session for Front
LOGGER_FRONT_TALK | false | The Logger logs Talk for Front
LOGGER_LOCK_NAME | logger-lock | Default value of logger lock name
LOGGER_LOGBACK | m2u-logger | LOGGER LOGBACK
LOGGER_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/logger-LOGBACK.xml | logger LOGBACK full path or name
LOGGER_PERIOD_SEC | 5 | The Logger process's period time
LOGGER_ROUTER_CLASSIFICATION | true | The Logger logs Classification for Router
LOGGER_ROUTER_ENABLE | true | The Logger enables logging Router
LOGGER_ROUTER_SESSION | true | The Logger logs Session-V3 for Router
LOGGER_ROUTER_TALK | true | The Logger logs Talk-V3 for Router
LOGGER_SESSION_CHANNELS | WEB,KAKAO,MAPCLI | Default value of logger session's channels
LOGGER_SESSION_EXPIRE | 600 | Default value of logger session's expire time
LOGGER_SESSION_PERIOD_SEC | 300 | The Logger session status sending period time
LOGGER_START_SEC | 0 | The Logger process's started time
LOGGER_ZOMBIE_CHATBOT_SESSION_TIMEOUT | 2000 | The non Chatbot's session expiration time
LOGS_DIR | ${MAUM_ROOT}/logs | logs directory
LOGS_MASKING_ENABLE | true | LOGBACK masking enable option
LOG_DAILY_ROTATE | false | CPP speed log whether daily rotate or not
LOG_DIR | m2u-map | LOG DIR
LOG_MAX_FILE_SIZE | 5M | CPP speed log max file size
LOG_ROTATE_COUNT | 10 | CPP speed log rotate count
M2U_ADMIN_IP | ${HOSTIP} | m2u admin service IP address(localhost)
M2U_ADMIN_PORT | 9922 | m2u admin service port
M2U_ADMIN_TIMEOUT | 20000 | m2u admin service grpc timeout
M2U_ADMIN_WEB_IP | ${HOSTIP} | Admin web server IP
M2U_ADMIN_WEB_PORT | 9990 | Admin web server port
M2U_CRYPTOR_CLASS_NAME | ai.maum.m2u.map.services.common.MessageCryptorImpl | m2u cryptor class name
M2U_FORWARDER_MAP_PORT | 9913 | m2u forwarder map port
M2U_FRONT_HTTP_CONNECT_PORT | 8080 | m2u front http connect port
M2U_FRONT_HTTP_PORT | 17877 | m2u front http port
M2U_FRONT_IP | ${HOSTIP} | m2u front IP(localhost)
M2U_FRONT_PORT | 9901 | m2u front server port
M2U_FRONT_REST_IP | ${HOSTIP} | m2u front REST api IP
M2U_FRONT_REST_PORT | 9999 | m2u front REST api port
M2U_FRONT_TIMEOUT | 15000 | m2u front grpc timeout
M2U_MAP_AUTH_IP | ${HOSTIP} | m2u auth map IP(localhost)
M2U_MAP_AUTH_PORT | 9950 | m2u auth map port
M2U_MAP_AUTH_TIMEOUT | 20000 | m2u auth map grpc timeout
M2U_MAP_CALL_DEADLINE_SEC | 30 | The map's grpc deadline sec
M2U_MAP_DEFAULT_CHATBOT | echo | m2u map default chatbot
M2U_MAP_DEFAULT_EXPECTSPEECH | 5000 | m2u map default expectspeech
M2U_MAP_IMAGE_SERVER_IP | ${HOSTIP} | The Image Server IP(localhost)
M2U_MAP_IMAGE_SERVER_PORT | 9950 | The Image Server PORT
M2U_MAP_INTERNAL_AUTH_IP | ${HOSTIP} | m2u internal auth map IP
M2U_MAP_INTERNAL_AUTH_PORT | 9951 | m2u internal auth map port
M2U_MAP_INTERNAL_AUTH_TIMEOUT | 20000 | m2u internal auth map grpc timeout
M2U_MAP_IP | ${HOSTIP} | m2u map server IP address(localhost)
M2U_MAP_PORT | 9911 | m2u map port
M2U_MAP_SERVER_MAX_CONNECTION_IDLE_SEC | 45 | The map server max connection idle sec
M2U_MAP_SERVER_MODE | MAP | M2U Map server mode, MAP or SECURE
M2U_MAP_SESSION_MANAGER_CYCLESEC | 30 | The map's session manager cycle sec
M2U_MAP_SESSION_MANAGER_REFVAL | 6000 | The map's session manager manager ref val
M2U_MESSAGE_FILTER_CLASS_NAME | ai.maum.m2u.map.services.common.AnomalyMessageFilterImpl | m2u message filter class name
M2U_POOL_IP | ${HOSTIP} | m2u pool service's IP
M2U_POOL_PING_CHECK_PERIOD_MS | 10000 | m2u pool service's ping check period in millisecond
M2U_POOL_PORT | 9931 | m2u pool service port
M2U_POOL_TIMEOUT | 20000 | m2u pool grpc timeout
M2U_REST_PORT | 9921 | m2u rest port
M2U_ROUTER_CALL_DEADLINE_SEC | 30 | m2u router grpc client call deadline second
M2U_ROUTER_IP | ${HOSTIP} | m2u pool service's IP
M2U_ROUTER_PORT | 9930 | m2u router port
M2U_ROUTER_TIMEOUT | 15000 | m2u router grpc timeout
M2U_SECURE_MAP_IP | 127.0.0.1 | m2u secure map IP
M2U_SECURE_MAP_PORT | 9912 | m2u secure map port
M2U_SSO_IP | 0.0.0.0 | Admin sso IP
M2U_SSO_PORT | 20001 | Admin sso port
M2U_TTS_ENCODING | LINEAR16 | m2u tts encoding
M2U_TTS_IP | ${HOSTIP} | m2u tts server IP address(localhost)
M2U_TTS_PORT | 9903 | m2u tts port
M2U_TTS_SAMPLE_RATE | 16000 | m2u tts samplerate
M2U_TTS_TIMEOUT | 10000 | m2u tts grpc timeout
MAP_CALLFILE_LOGBACK | m2u-map-callLog | MAP CALLFILE LOGBACK
MAP_LOGBACK | m2u-map | MAP LOGBACK
MAP_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/m2u-async-proxy-LOGBACK.xml | MAP LOGBACK full path or name
NGINX_BIN | /usr/sbin/nginx | nginx binary full path or name
POOL_LOGBACK | m2u-pool | POOL LOGBACK
POOL_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/pool-LOGBACK.xml | pool LOGBACK full path or name
REST_LOGBACK | m2u-rest | REST LOGBACK
REST_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/rest-LOGBACK.xml | rest LOGBACK full path or name
SECURE_MAP_LOGBACK | m2u-secure-map | SECURE_MAP LOGBACK
SECURE_MAP_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/secure-map-LOGBACK.xml | SECURE MAP LOGBACK full path or name
SESSION_LOCK_NAME | session-lock | Default value of session lock name
SESSION_NODE_MAX_SIZE | 100000 | Maximum number of map entries in session cluster member
SIMPLE_CLASSIFIER_NODE_MAX_SIZE | 10000 | Maximum number of map entries in simple classifier cluster member
SIMPLE_CLASSIFIER_PORT | 9905 | simple classifier port
SKILL_NODE_MAX_SIZE | 10000 | Maximum number of map entries in skill cluster member
SPDLOG_DAM_LEVEL | debug | CPP speedlog DAM default log level
SPDLOG_DEFAULT_LEVEL | trace | CPP speedlog default log level
SPDLOG_FRONT_LEVEL | debug | CPP speedlog Front default log level
SPDLOG_ITF_LEVEL | debug | CPP speedlog ITF default log level
SPDLOG_MONITOR_LEVEL | warning | CPP speedlog monitoring level
SPDLOG_MONITOR_PLUGIN | (null) | CPP speedlog monitoring plug-in
SPDLOG_MONITOR_WRITE_FUNCTION | (null) | CPP speedlog monitoring write-function
SPDLOG_ROUTER_LEVEL | debug | CPP speedlog Router default log level
SPDLOG_TTS_LEVEL | debug | CPP speedlog TTS default log level
SVCADM_LOGBACK | m2u-svcadm | SVCADM LOGBACK
SVCADM_LOGBACK_PATH | ${MAUM_ROOT}/etc/LOGBACK/service-admin-LOGBACK.xml | svc admin LOGBACK full path or name
SVC_HOST | ${HOSTIP} | Local host IP or DNS name for binding address
TALK_NEXT_NODE_MAX_SIZE | 100000 | Maximum number of map entries in talk next cluster member
TALK_NODE_MAX_SIZE | 100000 | Maximum number of map entries in talk cluster member
TA_SERVER_IP | ${HOSTIP} | TA server IP address(localhost)
USER_INFO_NODE_MAX_SIZE | 10000 | Maximum number of map entries in user info cluster member


## libmaum CONFIG VARIABLES INFO

**NAME** | **VALUE** | **DESCRIPTIONS**
----- | ----- | -----
ENC_TEST | This variable is not shown as provided | Enctyption test
LOGS_DIR | ${MAUM_ROOT}/logs | logs directory
LOGS_MASKING_ENABLE | true | LOGBACK masking enable option
SUPERVISOR_LISTEN_PORT | 9799 | supervisor listen port for external service management
SVREG_AUTO_START | true | required svreg registration client for service
SVREG_CONFIG_FILES | m2u.conf | product config files requires svreg service


## brain-ta CONFIG VARIABLES INFO

**NAME** | **VALUE** | **DESCRIPTIONS**
----- | ----- | -----
DNN_CL_PREPROCESS_NLU_KOR | nlp2 | Dnn classifier preprocessing NLU engine option
LOGS_DIR | ${MAUM_ROOT}/logs | logs directory
LOGS_MASKING_ENABLE | true | LOGBACK masking enable option
LOG_DAILY_ROTATE | false | CPP speed log whether daily rotate or not
LOG_MAX_FILE_SIZE | 5M | CPP speed log max file size
LOG_ROTATE_COUNT | 10 | CPP speed log rotate count
NLP3_KOR_DP | true | Brain-ta nlp3-kor DP option
NLP3_KOR_MODULE_ALL | true | Brain-ta nlp3-kor MODULE-ALL option
NLP3_KOR_MORP | true | Brain-ta nlp3-kor MORP option
NLP3_KOR_NER | true | Brain-ta nlp3-kor NER option
NLP3_KOR_SPACE | true | Brain-ta nlp3-kor SPACE option
NLP3_KOR_SRL | true | Brain-ta nlp3-kor SRL option
NLP3_KOR_WSD | true | Brain-ta nlp3-kor WSD option
NLP3_KOR_ZA | true | Brain-ta nlp3-kor ZA option
SPDLOG_BRAIN_CLD_LEVEL | debug | CPP speedlog brain-cld log level
SPDLOG_BRAIN_HMD_LEVEL | debug | CPP speedlog brain-hmd log level
SPDLOG_BRAIN_WEB_LEVEL | debug | CPP speedlog brain-web log level
SPDLOG_DEFAULT_LEVEL | trace | CPP speedlog default log level
SPDLOG_FULL_HMD_LEVEL | debug | CPP speedlog brain full hmd log level
SPDLOG_MONITOR_LEVEL | warning | CPP speedlog monitoring level
SPDLOG_MONITOR_PLUGIN | (null) | CPP speedlog monitoring plug-in
SPDLOG_MONITOR_WRITE_FUNCTION | (null) | CPP speedlog monitoring write-function
SPDLOG_NLP_ENG2_LEVEL | debug | CPP speedlog nlp-eng2 log level
SPDLOG_NLP_KOR1_LEVEL | debug | CPP speedlog nlp-kor1 log level
SPDLOG_NLP_KOR2_LEVEL | debug | CPP speedlog nlp-kor2 log level
SPDLOG_NLP_KOR3_LEVEL | debug | CPP speedlog nlp-kor3 log level


## brain-sds CONFIG VARIABLES INFO

**NAME** | **VALUE** | **DESCRIPTIONS**
----- | ----- | -----
BRAIN_SDS_RESOLVER_PORT | 9860 | brain sds resolver port
BRAIN_SDS_TRAINER_PORT | 9861 | brain sds trainer port
LOGS_DIR | ${MAUM_ROOT}/logs | logs directory
LOGS_MASKING_ENABLE | true | LOGBACK masking enable option
LOG_DAILY_ROTATE | false | CPP speed log whether daily rotate or not
LOG_MAX_FILE_SIZE | 5M | CPP speed log max file size
LOG_ROTATE_COUNT | 10 | CPP speed log rotate count
SDS_MAX_PORT | 10300 | brain sds sub-sdsd max port
SDS_MIN_PORT | 10000 | brain sds sub-sdsd min port
SPDLOG_DEFAULT_LEVEL | trace | CPP speedlog default log level
SPDLOG_ENG_EDU_LEVEL | debug | CPP speedlog eng-edu log level
SPDLOG_ENG_EXT_LEVEL | debug | CPP speedlog eng-ext log level
SPDLOG_ENG_INT_LEVEL | debug | CPP speedlog eng-int log level
SPDLOG_KOR_EXT_LEVEL | debug | CPP speedlog kor-ext log level
SPDLOG_KOR_INT_LEVEL | debug | CPP speedlog kor-int log level
SPDLOG_MONITOR_LEVEL | warning | CPP speedlog monitoring level
SPDLOG_MONITOR_PLUGIN | (null) | CPP speedlog monitoring plug-in
SPDLOG_MONITOR_WRITE_FUNCTION | (null) | CPP speedlog monitoring write-function
SPDLOG_SDS_RESOLVER_LEVEL | debug | CPP speedlog sds-resolver log level


## brain-stt CONFIG VARIABLES INFO

**NAME** | **VALUE** | **DESCRIPTIONS**
----- | ----- | -----
LOGS_DIR | ${MAUM_ROOT}/logs | logs directory
LOGS_MASKING_ENABLE | true | LOGBACK masking enable option
LOG_DAILY_ROTATE | false | CPP speed log whether daily rotate or not
LOG_MAX_FILE_SIZE | 5M | CPP speed log max file size
LOG_ROTATE_COUNT | 10 | CPP speed log rotate count
SPDLOG_DEFAULT_LEVEL | trace | CPP speedlog default log level
SPDLOG_MONITOR_LEVEL | warning | CPP speedlog monitoring level
SPDLOG_MONITOR_PLUGIN | (null) | CPP speedlog monitoring plug-in
SPDLOG_MONITOR_WRITE_FUNCTION | (null) | CPP speedlog monitoring write-function
SPDLOG_STT_LEVEL | debug | CPP speedlog stt log level
SPDLOG_STT_SUB_LEVEL | debug | CPP speedlog stt-sub log level