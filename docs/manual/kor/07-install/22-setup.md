# M2U 설정하기

## M2U 플랫폼을 설정하는 순서
- 변경된 환경변수를 결정하기
- `setup config`실행하기

## setup config
`setup config`는 M2U 플랫폼 패키지를 설정할 수 있는 파이썬 명령입니다.  
`setup config -h` 명령으로 사용할 수 있는 모든 기능을 확인해볼 수 있습니다.

### setup config help
```
usage: setup [-h] [-p [PROD]] [-t [TARGET]] [-d] [-v]
             {help,deploy,prepare,config,status,clear,recipe,check,export,help,upgrade}

maum.ai common setup tool

positional arguments:
  {help,deploy,prepare,config,status,clear,recipe,check,export,help,upgrade}
                        Command to process

optional arguments:
  -h, --help            show this help message and exit
  -p [PROD]             Product name
  -t [TARGET]           Build target (dev, test, staging, prod)
  -d                    Use default value on generation.
  -v                    Verbose output at running.

```

### m2u 변경가능한 변수
```
Variables for m2u
    >> ADMIN_REST_IP = 0.0.0.0 [default]
    >> ADMIN_REST_PORT = 9920 [default]
    >> ALT_SVC_HOST = www.10.122.66.197 [default]
    >> AUTH_POLICY_NODE_MAX_SIZE = 10000 [default]
    >> BRAIN_CL_IP = 10.122.66.197 [default]
    >> BRAIN_CL_PORT = 9820 [default]
    >> BRAIN_HMD_IP = 10.122.66.197 [default]
    >> BRAIN_HMD_PORT = 9815 [default]
    >> BRAIN_IDR_IP = 10.122.64.63 [default]
    >> BRAIN_IDR_PORT = 19090 [default]
    >> BRAIN_NLP_1_KOR_PORT = 9811 [default]
    >> BRAIN_NLP_2_ENG_PORT = 9814 [default]
    >> BRAIN_NLP_2_KOR_PORT = 9813 [default]
    >> BRAIN_NLP_3_KOR_IP = 10.122.66.197 [default]
    >> BRAIN_NLP_3_KOR_PORT = 9823 [default]
    >> BRAIN_QA_PORT = 15142 [default]
    >> BRAIN_STT_IP = 10.122.66.197 [default]
    >> BRAIN_STT_PORT = 9801 [default]
    >> CHATBOT_NODE_MAX_SIZE = 10000 [default]
    >> CLASSIFICATION_RECORD_NODE_MAX_SIZE = 10000 [default]
    >> COREVOICE_TTS_SERVER_ENG_PORT = 40040 [default]
    >> COREVOICE_TTS_SERVER_IP = 10.122.64.121 [default]
    >> COREVOICE_TTS_SERVER_KOR_PORT = 40050 [default]
    >> DAI_SVC_CONF_DIR = /etc/supervisor/conf.d/m2u-dai.conf [default]
    >> DAI_SVC_LOGBACK = m2u-daisvc [default]
    >> DAI_SVC_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/dialog-agent-instance-svc-logback.xml [default]
    >> DAM_DEFAULT_IP = localhost [default]
    >> DAM_SERVER_IP = 10.122.66.197 [default]
    >> DAM_SERVER_PORT = 9907 [default]
    >> DAM_SERVER_TIMEOUT = 10000 [default]
    >> DA_LOGBACK = m2u-da [default]
    >> DA_MAX_PORT = 30300 [default]
    >> DA_MIN_PORT = 30000 [default]
    >> DEVICES_NODE_MAX_SIZE = 10000 [default]
    >> DIALOG_AGENT_ACTIVATION_NODE_MAX_SIZE = 10000 [default]
    >> DIALOG_AGENT_INSTANCE_EXEC_NODE_MAX_SIZE = 10000 [default]
    >> DIALOG_AGENT_INSTANCE_NODE_MAX_SIZE = 10000 [default]
    >> DIALOG_AGENT_INSTANCE_RESOURCE_NODE_MAX_SIZE = 10000 [default]
    >> DIALOG_AGENT_MANAGER_NODE_MAX_SIZE = 10000 [default]
    >> DIALOG_AGENT_NODE_MAX_SIZE = 10000 [default]
    >> DIALOG_TRIGGER_PORT = 9960 [default]
    >> EXPORT_HOST = 10.122.66.197 [default]
    >> HAZELCAST_INVOCATION_TIMEOUT_SEC = 120 [default]
    >> HAZELCAST_SERVER_IP = 10.122.66.197 [default]
    >> HAZELCAST_SERVER_PORT = 5701 [default]
    >> HZC_LOGBACK = m2u-hzc [default]
    >> HZC_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/hazelcast-repack-logback.xml [default]
    >> HZC_MANCENTER_IP = 10.122.66.197 [default]
    >> HZC_MANCENTER_PORT = 9988 [default]
    >> HZC_MAPSTORE_LOGBACK = m2u-hzc-mapstore [default]
    >> HZC_MAUM_LOGBACK = m2u-hzc-maum [default]
    >> HZC_PORTABLE_LOGBACK = m2u-hzc-portable [default]
    >> INTENT_FINDER_CS_JAVA_PORT = 9923 [default]
    >> INTENT_FINDER_CS_JS_PORT = 9924 [default]
    >> INTENT_FINDER_CS_PY_IP = localhost [default]
    >> INTENT_FINDER_CS_PY_PORT = 9925 [default]
    >> INTENT_FINDER_INSTANCE_NODE_MAX_SIZE = 10000 [default]
    >> INTENT_FINDER_NODE_MAX_SIZE = 10000 [default]
    >> INTENT_FINDER_POLICY_NODE_MAX_SIZE = 10000 [default]
    >> ITF_INSTANCE_IP = localhost [default]
    >> ITF_SVC_LOGBACK = m2u-itfsvc [default]
    >> ITF_SVC_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/intent-finder-svc-logback.xml [default]
    >> LOGBACK_LOG_LEVEL = DEBUG [default]
    >> LOGGER_DB_CONNECTOR = mysql [default]
@@@@@@ LOGGER_DB_DATABASE = NfMAJo7+hbWOcOwx3VpFaNZ51FfIisV5n7PrPzbrIyjR2bOHC8MRzewaaok79Qxf <<encrypted>>
    >> LOGGER_DB_DRIVER = com.mysql.jdbc.Driver [default]
@@@@@@ LOGGER_DB_IP = WPtzzAyE1B7Tt1H9IFgPjfN6aeTet0bfVb39iKivjenwxXIONZaC6Z09Vy2YOD+U <<encrypted>>
@@@@@@ LOGGER_DB_PASSWORD = Vk1e43lVIqoBKR9tcBXyEMIy+gARItZnyB/YpM7CxMNK5DfRj6FtGl2oKEjy7XO6 <<encrypted>>
@@@@@@ LOGGER_DB_PORT = IRLRYDCj95Kw1KEIw8F/ymtst30AL0V2Fi7DNF9pu9uqUki7+i5Q/w/hZgYNdnmq <<encrypted>>
@@@@@@ LOGGER_DB_URL = d+kdfTPPY9wDKktvTOOE4eRSHoaDr5rLz7wHy5Xr90kyV/hZPWjs7rbLemC8tOUAjNMw0lF+cytgTzY6zZj0iUfXDnIltJ36Ghji/opv/IA= <<encrypted>>
@@@@@@ LOGGER_DB_USER = vrnlN6FfVe0LAg4baRM8s+vVbjc/Y+L4nHLZ2rug1w4JGgBgcHEuSy6HBkckbubS <<encrypted>>
    >> LOGGER_FRONT_ENABLE = false [default]
    >> LOGGER_FRONT_SESSION = false [default]
    >> LOGGER_FRONT_TALK = false [default]
    >> LOGGER_LOCK_NAME = logger-lock [default]
    >> LOGGER_LOGBACK = m2u-logger [default]
    >> LOGGER_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/logger-logback.xml [default]
    >> LOGGER_PERIOD_SEC = 5 [default]
    >> LOGGER_ROUTER_CLASSIFICATION = true [default]
    >> LOGGER_ROUTER_ENABLE = true [default]
    >> LOGGER_ROUTER_SESSION = true [default]
    >> LOGGER_ROUTER_TALK = true [default]
    >> LOGGER_SESSION_CHANNELS = WEB,KAKAO,MAPCLI [default]
    >> LOGGER_SESSION_EXPIRE = 600 [default]
    >> LOGGER_SESSION_PERIOD_SEC = 300 [default]
    >> LOGGER_START_SEC = 0 [default]
    >> LOGGER_ZOMBIE_CHATBOT_SESSION_TIMEOUT = 2000 [default]
    >> LOGS_DIR = ${MAUM_ROOT}/logs [default]
    >> LOGS_MASKING_ENABLE = true [default]
    >> LOG_DAILY_ROTATE = false [default]
    >> LOG_DIR = m2u-map [default]
    >> LOG_MAX_FILE_SIZE = 5M [default]
    >> LOG_ROTATE_COUNT = 10 [default]
    >> M2U_ADMIN_IP = 10.122.66.197 [default]
    >> M2U_ADMIN_PORT = 9922 [default]
    >> M2U_ADMIN_TIMEOUT = 20000 [default]
    >> M2U_ADMIN_WEB_IP = 10.122.66.197 [default]
    >> M2U_ADMIN_WEB_PORT = 9990 [default]
    >> M2U_CRYPTOR_CLASS_NAME = ai.maum.m2u.map.services.common.MessageCryptorImpl [default]
    >> M2U_FORWARDER_MAP_PORT = 9913 [default]
    >> M2U_FRONT_HTTP_CONNECT_PORT = 8080 [default]
    >> M2U_FRONT_HTTP_PORT = 17877 [default]
    >> M2U_FRONT_IP = 10.122.66.197 [default]
    >> M2U_FRONT_PORT = 9901 [default]
    >> M2U_FRONT_REST_IP = 10.122.66.197 [default]
    >> M2U_FRONT_REST_PORT = 9999 [default]
    >> M2U_FRONT_TIMEOUT = 15000 [default]
    >> M2U_MAP_AUTH_IP = 10.122.66.197 [default]
    >> M2U_MAP_AUTH_PORT = 9950 [default]
    >> M2U_MAP_AUTH_TIMEOUT = 20000 [default]
    >> M2U_MAP_CALL_DEADLINE_SEC = 30 [default]
    >> M2U_MAP_DEFAULT_CHATBOT = echo [default]
    >> M2U_MAP_DEFAULT_EXPECTSPEECH = 5000 [default]
    >> M2U_MAP_IMAGE_SERVER_IP = 10.122.66.197 [default]
    >> M2U_MAP_IMAGE_SERVER_PORT = 9951 [default]
    >> M2U_MAP_INTERNAL_AUTH_IP = 10.122.66.197 [default]
    >> M2U_MAP_INTERNAL_AUTH_PORT = 9951 [default]
    >> M2U_MAP_INTERNAL_AUTH_TIMEOUT = 20000 [default]
    >> M2U_MAP_IP = 10.122.66.197 [default]
    >> M2U_MAP_PORT = 9911 [default]
    >> M2U_MAP_SERVER_MAX_CONNECTION_IDLE_SEC = 45 [default]
    >> M2U_MAP_SERVER_MODE = MAP [default]
    >> M2U_MAP_SESSION_MANAGER_CYCLESEC = 30 [default]
    >> M2U_MAP_SESSION_MANAGER_REFVAL = 6000 [default]
    >> M2U_MESSAGE_FILTER_CLASS_NAME = ai.maum.m2u.map.services.common.AnomalyMessageFilterImpl [default]
    >> M2U_POOL_IP = 10.122.66.197 [default]
    >> M2U_POOL_PING_CHECK_PERIOD_MS = 10000 [default]
    >> M2U_POOL_PORT = 9931 [default]
    >> M2U_POOL_TIMEOUT = 20000 [default]
    >> M2U_REST_PORT = 9921 [default]
    >> M2U_ROUTER_CALL_DEADLINE_SEC = 30 [default]
    >> M2U_ROUTER_IP = 10.122.66.197 [default]
    >> M2U_ROUTER_PORT = 9930 [default]
    >> M2U_ROUTER_TIMEOUT = 15000 [default]
    >> M2U_SECURE_MAP_IP = 127.0.0.1 [default]
    >> M2U_SECURE_MAP_PORT = 9912 [default]
    >> M2U_SSO_IP = 0.0.0.0 [default]
    >> M2U_SSO_PORT = 20001 [default]
    >> M2U_TTS_ENCODING = LINEAR16 [default]
    >> M2U_TTS_IP = 10.122.66.197 [default]
    >> M2U_TTS_PORT = 9903 [default]
    >> M2U_TTS_SAMPLE_RATE = 16000 [default]
    >> M2U_TTS_TIMEOUT = 10000 [default]
    >> MAP_CALLFILE_LOGBACK = m2u-map-callLog [default]
    >> MAP_LOGBACK = m2u-map [default]
    >> MAP_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/m2u-async-proxy-logback.xml [default]
    >> NGINX_BIN = /usr/sbin/nginx [default]
    >> POOL_LOGBACK = m2u-pool [default]
    >> POOL_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/pool-logback.xml [default]
    >> REST_LOGBACK = m2u-rest [default]
    >> REST_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/rest-logback.xml [default]
    >> SECURE_MAP_LOGBACK = m2u-secure-map [default]
    >> SECURE_MAP_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/secure-map-logback.xml [default]
    >> SESSION_LOCK_NAME = session-lock [default]
    >> SESSION_NODE_MAX_SIZE = 100000 [default]
    >> SIMPLE_CLASSIFIER_NODE_MAX_SIZE = 10000 [default]
    >> SIMPLE_CLASSIFIER_PORT = 9905 [default]
    >> SKILL_NODE_MAX_SIZE = 10000 [default]
    >> SPDLOG_DAM_LEVEL = debug [default]
    >> SPDLOG_DEFAULT_LEVEL = trace [default]
    >> SPDLOG_FRONT_LEVEL = debug [default]
    >> SPDLOG_ITF_LEVEL = debug [default]
    >> SPDLOG_MONITOR_LEVEL = warning [default]
    >> SPDLOG_MONITOR_PLUGIN =  [default]
    >> SPDLOG_MONITOR_WRITE_FUNCTION =  [default]
    >> SPDLOG_ROUTER_LEVEL = debug [default]
    >> SPDLOG_TTS_LEVEL = debug [default]
    >> SVCADM_LOGBACK = m2u-svcadm [default]
    >> SVCADM_LOGBACK_PATH = ${MAUM_ROOT}/etc/logback/service-admin-logback.xml [default]
    >> SVC_HOST = 10.122.66.197 [default]
    >> TALK_NEXT_NODE_MAX_SIZE = 100000 [default]
    >> TALK_NODE_MAX_SIZE = 100000 [default]
    >> TA_SERVER_IP = 10.122.66.197 [default]
    >> USER_INFO_NODE_MAX_SIZE = 10000 [default]

Variables for libmaum
    >> ENC_TEST = ZIWLmNtETzgp/GpXfbZPK5q08He5W8LBLzdJD3iZjRmB7XG7mP/CXFXgwobYlKdQHyuRa7dYGIXjqukQYE3e98uKBqp+kQ4HK7XS2liY9eI= <<encrypted>>
    >> LOGS_DIR = ${MAUM_ROOT}/logs [default]
    >> LOGS_MASKING_ENABLE = true [default]
    >> SUPERVISOR_LISTEN_PORT = 9799 [default]
    >> SVREG_AUTO_START = true [default]
    >> SVREG_CONFIG_FILES = m2u.conf [default]

Variables for brain-ta
    >> DNN_CL_PREPROCESS_NLU_KOR = nlp2 [default]
    >> LOGS_DIR = ${MAUM_ROOT}/logs [default]
    >> LOGS_MASKING_ENABLE = true [default]
    >> LOG_DAILY_ROTATE = false [default]
    >> LOG_MAX_FILE_SIZE = 5M [default]
    >> LOG_ROTATE_COUNT = 10 [default]
    >> NLP3_KOR_DP = true [default]
    >> NLP3_KOR_MODULE_ALL = true [default]
    >> NLP3_KOR_MORP = true [default]
    >> NLP3_KOR_NER = true [default]
    >> NLP3_KOR_SPACE = true [default]
    >> NLP3_KOR_SRL = true [default]
    >> NLP3_KOR_WSD = true [default]
    >> NLP3_KOR_ZA = true [default]
    >> SPDLOG_BRAIN_CLD_LEVEL = debug [default]
    >> SPDLOG_BRAIN_HMD_LEVEL = debug [default]
    >> SPDLOG_BRAIN_WEB_LEVEL = debug [default]
    >> SPDLOG_DEFAULT_LEVEL = trace [default]
    >> SPDLOG_FULL_HMD_LEVEL = debug [default]
    >> SPDLOG_MONITOR_LEVEL = warning [default]
    >> SPDLOG_MONITOR_PLUGIN =  [default]
    >> SPDLOG_MONITOR_WRITE_FUNCTION =  [default]
    >> SPDLOG_NLP_ENG2_LEVEL = debug [default]
    >> SPDLOG_NLP_KOR1_LEVEL = debug [default]
    >> SPDLOG_NLP_KOR2_LEVEL = debug [default]
    >> SPDLOG_NLP_KOR3_LEVEL = debug [default]

Variables for brain-sds
    >> BRAIN_SDS_RESOLVER_PORT = 9860 [default]
    >> BRAIN_SDS_TRAINER_PORT = 9861 [default]
    >> LOGS_DIR = ${MAUM_ROOT}/logs [default]
    >> LOGS_MASKING_ENABLE = true [default]
    >> LOG_DAILY_ROTATE = false [default]
    >> LOG_MAX_FILE_SIZE = 5M [default]
    >> LOG_ROTATE_COUNT = 10 [default]
    >> SDS_MAX_PORT = 10300 [default]
    >> SDS_MIN_PORT = 10000 [default]
    >> SPDLOG_DEFAULT_LEVEL = trace [default]
    >> SPDLOG_ENG_EDU_LEVEL = debug [default]
    >> SPDLOG_ENG_EXT_LEVEL = debug [default]
    >> SPDLOG_ENG_INT_LEVEL = debug [default]
    >> SPDLOG_KOR_EXT_LEVEL = debug [default]
    >> SPDLOG_KOR_INT_LEVEL = debug [default]
    >> SPDLOG_MONITOR_LEVEL = warning [default]
    >> SPDLOG_MONITOR_PLUGIN =  [default]
    >> SPDLOG_MONITOR_WRITE_FUNCTION =  [default]
    >> SPDLOG_SDS_RESOLVER_LEVEL = debug [default]

Variables for brain-stt
    >> LOGS_DIR = ${MAUM_ROOT}/logs [default]
    >> LOGS_MASKING_ENABLE = true [default]
    >> LOG_DAILY_ROTATE = false [default]
    >> LOG_MAX_FILE_SIZE = 5M [default]
    >> LOG_ROTATE_COUNT = 10 [default]
    >> SPDLOG_DEFAULT_LEVEL = trace [default]
    >> SPDLOG_MONITOR_LEVEL = warning [default]
    >> SPDLOG_MONITOR_PLUGIN =  [default]
    >> SPDLOG_MONITOR_WRITE_FUNCTION =  [default]
    >> SPDLOG_STT_LEVEL = debug [default]
    >> SPDLOG_STT_SUB_LEVEL = debug [default]
 ```

위 명령어의 각 항목이 모두 설정가능한 변수들과 기본값들입니다.  
`setup config -p [product]` 명령어를 실행하면 제품별로 설정가능한 변수들을 모두 나열합니다.  
```
제품 리스트
 - m2u
 - libmaum
 - brain-ta
 - brain-sds
 - brain-stt
```

### 변경가능한 변수들 알아보기
아래는 변경가능한 변수들 중 일부에 대한 설명으로 자세한 내용은 `m2u-setup-config-variables`를 통해 확인할 수 있습니다.  

- ADMIN_REST_IP : m2u에서 기본으로 제공되는 REST IP가 설정 됩니다.
- ADMIN_REST_PORT : m2u에서 기본으로 제공되는 REST port가 설정 됩니다.
- ALT_SVC_HOST : nginx를 이용하여 외부에서 m2u admin에 접속할 때 사용되는 IP를 지정합니다.
- AUTH_POLICY_NODE_MAX_SIZE : HAZELCAST에서 사용하는 node의 갯수 입니다. 해당 node의 25%가 차게 되면 HAZELCAST DB에서 빼서 로컬에 저장합니다. SESSION_NODE_MAX_SIZE, TALK_NODE_MAX_SIZE, TALK_NEXT_NODE_MAX_SIZE를 제외하고는 10000으로 통일되어 있습니다.
- BRAIN_CL_IP : brain cl의 IP가 설정 됩니다.
- BRAIN_CL_PORT : brain cl의 port가 설정 됩니다.
- BRAIN_STT_IP : STT 서비스가 다른 호스트에 있을 경우에 이를 지정합니다. 경우에 따라서 STT 서비스가 Load Balancer 를 통해서 동작하고 있는 경우라면 로드 밸런서의 IP 주소를 명시해야 합니다.
- COREVOICE_TTS_SERVER_ENG_PORT : TTS 영어 포트를 지정합니다.
- COREVOICE_TTS_SERVER_IP : TTS 서비스의 IP를 지정합니다. 기본적으로 MindsLab 사내 TTS 서버 값이 들어 있습니다.
- COREVOICE_TTS_SERVER_KOR_PORT : TTS 한국어 포트를 지정합니다.
- DAI_SVC_CONF_DIR : Dialog Agent Instance Service의 설정이 저장되는 디렉토리 경로 입니다.
- DAM_SERVER_IP : 대화 에이전트 관리자가 다른 곳에 있을 경우 그 IP를 명시합니다. 콘솔에서는 DAM 들을 별도로 관리하므로 여기서 굳이 지정할 필요가 없습니다.
- DAM_SERVER_TIMEOUT : grpc timeout 관련한 변수 값입니다. 모든 TIMEOUT 설정 관련 변수는 같은 특징입니다.
- EXPORT_HOST : 외부에서 접근할 때 사용할 IP를 지정합니다. 예를 들어서 내부에서는 127.0.0.1 로 접근할 수 있더라도 외부에서는 호스트의 외부 IP를 사용해야 합니다. 외부 IP를 명시적으로 지정합니다.
- HAZELCAST_SERVER_IP : IMDG HAZELCAST IP를 지정 합니다. HAZELCAST가 별도 서버로 분리돼 있을 경우 해당 서버 IP를 지정해 주면 됩니다.
- HAZELCAST_SERVER_PORT : IMDG HAZELCAST는 기본값으로 5701을 사용합니다.
- HZC_MANCENTER_IP : IMDG HAZELCAST 관리센터 IP를 지정합니다. 기본값으로 호스트IP를 사용하는데 호스트IP는 자신의 개인IP가 들어가므로 EXPORT IP와 잘 구분해야 합니다.
- INTENT_FINDER_CS_PY_IP : custom script 서버 IP를 지정합니다.
- INTENT_FINDER_CS_PY_PORT : custom script 서버 port를 지정합니다.
- LOGGER_DB_DRIVER : 사용하는 DB 프로그램에 따라 달라집니다. MindsLab 내부에서는 mysql을 기본적으로 사용합니다.
- LOGGER_DB_IP : 사용하는 DB가 설치된 위치의 IP를 지정하며 설정에서는 암호화 처리 됩니다. 합니다.
- LOGGER_DB_PASSWORD : DB를 사용하는 user의 password를 명시하며 설정에서는 암호화 처리 됩니다.
- LOGGER_DB_USER : DB를 사용하는 user를 명시하며 설정에서는 암호화 처리 됩니다.
- LOG_DIR : 로그가 쌓이는 디렉토리를 지정할 수 있습니다.
- LOG_MAX_FILE_SIZE : 하나의 로그 파일당 최대 용량을 지정 합니다.
- M2U_ADMIN_IP : M2U 웹 관리 페이지에 접속 할 때의 IP값 입니다.
- M2U_ADMIN_PORT : M2U 웹 관리 페이지의 서비스 포트를 지정할 수 있습니다. 이 포트의 뒤에는 nginx가 서비스로 동작하므로 서비스를 변경해야 합니다.
- M2U_FRONT_HTTP_PORT : HTTP2 기반의 포트를 지정할 수 있습니다.
- M2U_FRONT_PORT : 프론트 서비스의 포트로 기본적으로 9901를 사용합니다. 이 포트는 외부에서 접근할 수 있으며 grpc로 서비스를 진행하고 있습니다.
- M2U_FRONT_REST_PORT : 프론트 서비스에 대한 HTTP1/1 기반의 REST 포트를 지정할 수 있습니다.
- M2U_MAP_AUTH_IP : 내부 테스트를 위한 MAP AUTH IP
- M2U_MAP_AUTH_PORT : 내부 테스트를 위한 MAP AUTH 포트
- M2U_MAP_DEFAULT_CHATBOT : 기본적으로 echo 응답을 줄 수 있는 챗봇명 입니다.
- M2U_MAP_IMAGE_SERVER_PORT : MAP AUTH와 같은 포트 사용, 추후에 분리 됩니다.
- NGINX_BIN : nginx 실행 경로입니다.
- SVC_HOST : 서비스하는 호스트가 물고 있을 IP를 명시합니다. 통상 기본 이더넷 장치를 명시하거나 0.0.0.0 일 수 있습니다.
- TA_SERVER_IP : TA 서비스가 다른 호스트에 있을 경우에 이를 지정합니다.


### 설정 파일들 생성하기
특정한 파라미터의 값을 변경하고 싶으면 `./setup config` 명령을 실행 후 `변수명 = 변경 값` 형태로 입력하면 됩니다.  
```
./setup config
```
예를 들어, `M2U_FRONT_IP`를 변경하고 싶다면 아래와 같이 커맨드를 입력합니다.
```
Input variable (KEY=VALUE), done | Ctrl+D if done, [ENTER] to refresh
> M2U_FRONT_IP = 변경 값
```

### 생성되는 파일들
다음은 `./setup config`명령을 통해서 생성되는 파일들의 목록입니다.

```
++ generating config files for m2u
-- dir: etc
    etc/m2u.conf generated.
    etc/hazelcast.xml generated.
    etc/hazelcast-client.xml generated.
-- dir: etc/supervisor/conf.d
    etc/supervisor/conf.d/m2u.conf generated.
-- dir: apps/admin-rest/server
    apps/admin-rest/server/config.json generated.
    apps/admin-rest/server/config.production.json generated.
    apps/admin-rest/server/config.staging.json generated.
    apps/admin-rest/server/datasources.production.js generated.
    apps/admin-rest/server/datasources.staging.json generated.
-- dir: etc/nginx
    etc/nginx/m2u-admin.conf generated.
    etc/nginx/m2u-admin-upstream.conf generated.
-- dir: samples
    samples/m2u-default-setting.json generated.
-- dir: etc/logback
    etc/logback/m2u-async-proxy-logback.xml generated.
    etc/logback/hazelcast-repack-logback.xml generated.
    etc/logback/intent-finder-svc-logback.xml generated.
    etc/logback/logger-logback.xml generated.
    etc/logback/pool-logback.xml generated.
    etc/logback/rest-logback.xml generated.
    etc/logback/service-admin-logback.xml generated.
    etc/logback/dialog-agent-instance-svc-logback.xml generated.

++ Configuration is stored at ${MAUM_ROOT}/etc/setup.d/m2u.store.json
```

```
++ generating config files for libmaum
-- dir: etc
    etc/libmaum.conf generated.
-- dir: etc/supervisor
    etc/supervisor/supervisord.conf generated.
-- dir: etc/supervisor/conf.d
    etc/supervisor/conf.d/svreg.conf generated.
-- dir: etc/supervisor/export
    etc/supervisor/export/maumai-sv.conf generated.
    etc/supervisor/export/maumai-sv.service generated.
    etc/supervisor/export/maumai-sv-root.service generated.

++ Configuration is stored at ${MAUM_ROOT}/etc/setup.d/libmaum.store.json
```

```
++ generating config files for brain-ta
-- dir: etc
    etc/brain-ta.conf generated.
-- dir: etc/supervisor/conf.d
    etc/supervisor/conf.d/brain-ta.conf generated.

++ Configuration is stored at ${MAUM_ROOT}/etc/setup.d/brain-ta.store.json
```

```
++ generating config files for brain-sds
-- dir: etc
    etc/brain-sds.conf generated.
-- dir: etc/supervisor/conf.d
    etc/supervisor/conf.d/brain-sds.conf generated.

++ Configuration is stored at ${MAUM_ROOT}/etc/setup.d/brain-sds.store.json
```

```
++ generating config files for brain-stt
-- dir: etc
    etc/brain-stt.conf generated.
-- dir: etc/supervisor/conf.d
    etc/supervisor/conf.d/brain-stt.conf generated.

++ Configuration is stored at ${MAUM_ROOT}/etc/setup.d/brain-stt.store.json
```

생성되는 파일들에서 디렉토리는 기본적으로 `$MAUM_ROOT`이고 생성된 파일들의 원본은 `파일명.in`입니다.  
원본에는 $로 변수값이 들어있는 영역들이 있는데 해당 변수 값들이 상기 기본 값에서 변경한 값으로 대체되는 것입니다.