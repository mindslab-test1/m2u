## 헤이즐캐스트 데이터베이스에 대한 소개
헤이즐케스트는 hazelcast.com 에서 오픈소스 또는 엔터프라이즈 형태로 공급하는 IMDG(In Memory Data Grid) 입니다.  
이는 Java, C++, nodejs 등 다양한 언어에서 메모리 상에 List, Set, Map 형태의 데이터를 처리할 수 있도록 해주고 있습니다. 또한 자체적인 클러스터를 구성하여 안정적으로 서비스를 구성할 수 있도록 해주고 있습니다.

## 헤이즐캐스트 DB 실행
헤이즐캐스트에 대한 설정은 `$MAUM_ROOT/etc/hazelcast.xml` 파일을 참조하도록 합니다.  
포트는 5701를 사용하여 외부에서도 접근할 수 있도록 허용하고 있습니다.  
`svd`를 이용하면 기본적으로 `m2u-hzc`와 `m2u-hzc-2` 프로세스가 작동합니다.

## 헤이즐캐스트 매니지먼트 센터 실행

`$MAUM_ROOT/etc/hazelcast.xml`파일에는 또한 헤이즐캐스트 맨센터라고 부르는 매니지먼트 센터 실행에 관련된 옵션도 있습니다.  
헤이즐캐스트 맨센터 포트는 9988로 고정이고 `svctl`을 이용하여 추가적으로 `m2u-hzcmancenter`프로세스를 작동시킬 수 있습니다.  
만약 2개의 서버에 헤이즐캐스트 프로세스를 1개씩 실행하여 클러스터링으로 구성할 경우 맨센터 IP는 사용할 서버의 IP로 통일을 해줘야 하며 이에 대해서는 `$MAUM_ROOT/etc/hazelcast.xml`에서 설정할 수 있습니다.  
```
<management-center enabled="true">http://10.122.64.150:9988/mancenter</management-center>
```
그리고 헤이즐캐스트를 클러스터링으로 3개 이상의 멤버로 구성할 경우 헤이즐캐스트 맨센터는 사용이 불가하며 대체 수단으로 `$MAUM_ROOT/bin`에서 `hzc-console`을 이용하여 헤이즐캐스트 데이터를 확인할 수 있습니다.
```
헤이즐캐스트 맨센터 콘솔 명령어

1. 특정 map 선택
  - ns dialog-agent-manager   // ns는 name space의 줄임말임

2. 데이터 확인
  - m.size     // ns에 저장돼 있는 데이터 수 확인
  - m.values   // ns에 저장돼 있는 데이터 값 확인

3. 기타 명령어 확인
  - .help
```

## 헤이즐캐스트 클러스터링 구성
헤이즐캐스트는 `$MAUM_ROOT/etc/hazelcast.xml` 설정 파일에서 IP만 추가하면 간단하게 클러스터링 구성이 가능 합니다.  
`$MAUM_ROOT/etc/hazelcast.xml`에서 클러스터링을 구성할 서버 IP를 아래와 같이 `<member>`와 `<member-list>`에 추가하고 헤이즐캐스트 프로세스를 실행만 해주면 됩니다.
```
<tcp-ip enabled="true">
  <member>10.122.66.197</member>   // local server IP
  <member>10.122.66.198</member>   // add server1 IP
  <member>10.122.66.199</member>   // add server2 IP
  <member-list>
    <member>10.122.66.197</member>
    <member>10.122.66.198</member>
    <member>10.122.66.199</member>
  </member-list>
</tcp-ip>
```
위와 같이 IP를 추가하여 클러스터링을 구성하게 되면 프로세스별 port는 헤이즐캐스트가 기본 port를 기준으로 자동 할당을 해줍니다.  

서버 1대에서도 클러스터링 구성은 가능하며 `M2U를 설치하면 기본적으로 본인 서버의 IP가 멤버로 구성`돼 있으며 헤이즐캐스트도 2개가 시작되기 때문에 자동으로 클러스터링이 기본 구성 됩니다.  
만약 헤이즐캐스트를 추가하고 싶다면 `svctl`에서 헤이즐캐스트 프로세스만 추가로 실행시켜 주면 됩니다.
```
brain-cl                         STOPPED   Not started
brain-full-hmd                   STOPPED   Not started
brain-hmd                        STOPPED   Not started
brain-nlp1kor                    STOPPED   Not started
brain-nlp2eng                    STOPPED   Not started
brain-nlp2kor                    STOPPED   Not started
brain-nlp3kor                    STOPPED   Not started
brain-sds                        STOPPED   Not started
brain-stt                        STOPPED   Not started
brain-we                         STOPPED   Not started
m2u-admproxy                     RUNNING   pid 23114, uptime 0:00:04
m2u-admrest                      RUNNING   pid 23116, uptime 0:00:04
m2u-daisvc                       RUNNING   pid 23123, uptime 0:00:04
m2u-dam                          RUNNING   pid 23120, uptime 0:00:04
m2u-front                        STOPPED   Not started
m2u-hzc                          RUNNING   pid 23139, uptime 0:00:04
m2u-hzc-2                        RUNNING   pid 23117, uptime 0:00:04
m2u-hzc-3                        STOPPED   Not started                  // start로 실행
m2u-hzcmancenter                 STOPPED   Not started
m2u-itf-cs-py-run                RUNNING   pid 23133, uptime 0:00:04
m2u-itfsvc                       RUNNING   pid 23121, uptime 0:00:04
m2u-logger                       RUNNING   pid 23126, uptime 0:00:04
m2u-map                          RUNNING   pid 23122, uptime 0:00:04
m2u-mapauth                      RUNNING   pid 23125, uptime 0:00:04
m2u-pool                         RUNNING   pid 23119, uptime 0:00:04
m2u-rest                         RUNNING   pid 23113, uptime 0:00:04
m2u-router                       RUNNING   pid 23115, uptime 0:00:04
m2u-svcadm                       RUNNING   pid 23134, uptime 0:00:04
m2u-tts                          STOPPED   Not started
svreg                            RUNNING   pid 23136, uptime 0:00:04
supervisor> start m2u-hzc-3
```

## 헤이즐캐스트 클러스터링 구성시 데이터 안정성를 위한 backup-count 설정
헤이즐캐스트는 데이터 안정성을 위해 `$MAUM_ROOT/etc/hazelcast.xml` 설정 파일에서 map 항목마다 `backup-count(backup되는 수)`를 설정할 수 있습니다.  
`backup-count`는 `최소 0부터 최대 6까지 설정`할 수 있으며 설정된 수 만큼의 멤버에게 본인의 데이터를 백업하게 됩니다.  
M2U를 설치하면 `backup-count는 기본적으로 3으로 설정`돼 있으며 `서버와 멤버의 구성 조합`에 따라 적절한 값으로 변경해주면 됩니다.  
```
예시)

<hazelcast>
  <map name="default">
    <backup-count>3</backup-count>
  </map>
</hazelcast>
```

## 헤이즐캐스트 클러스터링 구성 공식 가이드
http://docs.hazelcast.org/docs/latest/manual/html-single/#discovering-members-by-tcp
