# 설치하기

## 필요한 설치 복사하기

USB를 삽입하면, `sudo fdisk -l` 명령을 통해서 마운트 된 위치를 확인할 수 있습니다.  
여기서는 `/media/usb` 라는 위치에 파일들이 존재한다고 가정하겠습니다.  

먼저 설치할 대상 시스템에 설치를 위한 디렉토리를 생성합니다.

```bash
mkdir ~/maum-install
cd ~/maum-install
cp /media/usb/*.tar.gz .
```

모든 설치파일을 복제하였으면 설치 준비가 완료된 것입니다.  
설치 파일들은 모두 다음과 같습니다.  

- m2u-v2.4.1-all-centos.tar.gz
- minds-res-stt-20170501.tar.gz
- minds-res-nlp1-kor-custom-20170501.tar.gz
- minds-res-word_embedding-20170501.tar.gz
- minds-res-nlp2-kor-20170501.tar.gz
- minds-res-nlp1-kor-20170807.tar.gz
- minds-res-nlp3-kor-20180322.tar.gz
- minds-res-nlp2-eng-20170501.tar.gz
- minds-res-sds-eng-20170816.tar.gz
- minds-res-sds-kor-20170816.tar.gz

`minds-res-*`형식의 파일은 M2U가 동작하기 위한 리소스 파일들입니다.

아래는 폐쇄망에 설치할 때 M2U 사용에 필요한 추가적인 패키지 설치 파일들 입니다.
- local-dependency.tar.gz
- local-rpm.tar.gz
- nginx.tar.gz
- python-packages.tar.gz

패키지 설치 파일들은 홈 디렉토리에 위치해야 M2U 설치 시 함께 설치할 수 있습니다.
```bash
cp local-dependency.tar.gz local-rpm.tar.gz nginx.tar.gz python-packages.tar.gz ~/
```

기본 외부 패키지를 설치하기 위한 외부 파일은 스트립트 형태로 제공됩니다.


## M2U 패키지 설치하기
아래의 모든 예에서 설치디렉토리는 `/srv/maum`로 사용합니다.

### M2U 설치
m2u-version.tar.gz에는 기본적으로 maum 디렉토리 구조를 가지고 있습니다.  
설치하고 싶은 아무 디렉토리에서나 풀기만 하면 바로 그 위치에 설치가 됩니다.  
여기에서는 `/srv` 아래에서 풀면, `/srv/maum` 디렉토리를 만들면서 풀리게 됩니다.

```
cd m2u-install
tar zxvf m2u-v2.1.4-all-centos.tar.gz -C /srv/
chown -R maum:maum /srv/maum
```

### 리소스 설치하기
리소스 파일들은 `minds-res`로 시작하는 파일들입니다.  
이 파일들은 모두 M2U에 필요한 주요 엔진별 리소스들을 가지고 있으며 M2U가 설치되는 상황에 따라 제공될 수도 있고, 제공되지 않을 수도 있습니다.  
STT 관련 제품과 라이센스를 구매하지 않은 경우에는 STT 리소스는 제공되지 않습니다.

위 리소스들은 `/srv/maum/resources` 디렉토리에 풀려야 합니다.  
이를 풀기 위해서 아래의 명령을 사용하며 각 tar 파일에는 resources 디렉토리가 포함되어 있습니다.  
따라서 설치 위치를 `/srv/maum`로 지정해야 합니다.
```bash
tar minds-res-sds-kor-20170816.tar.gz -C /srv/maum
```
위 명령을 수행하면 `/srv/maum` 디렉토리 아래에 resources/sds-kor 디렉토리를 자동으로 생성합니다.  
나머지 리소스 파일들에 대해서도 위 명령을 수행해주어야 합니다.  

한꺼번에 명령을 수행하려면 다음과 같이 수행하면 됩니다.
```
cd maum-install
for t in minds-res*.tar.gz
do
  tar -zxf $t -C /srv/maum
done
```

### 필요한 외부 패키지 설치하기
M2U 플랫폼이 동작하기 위해서는 M2U 플랫폼 제품 이외에도 시스템 운영에 필요한 외부 패키지들도 함께 설치되어야 합니다.  
이들 패키지는 `/srv/maum/init-offline.sh` 또는 `/srv/maum/m2u-init.sh`을 통해서 설치합니다.

##### Ubuntu 및 REDHAT, CentOS 패키지
- gcc 4.8.5
- g++ 4.8.5
- openmpi-common 
- libboost-all-dev 
- libcurl4-openssl-dev 
- libsqlite3-dev 
- libmysqlclient-dev 
- mysql-server-5.7 
- libuv-dev libssl-dev 
- libarchive13 libarchive-dev 
- libatlas-base-dev libatlas-dev 
- libasio-dev 
- unzip 
- nginx
- ffmpeg 
- flex 
- uuid-dev 
- libbz2 
- libzmq3

##### Python Packages
- virtualenv
- boto3
- grpcio
- requests
- numpy
- theano
- gensim


#### 온라인으로 설치하기
아래 명령을 실행하면 각 OS에 맞춰서 M2U에 필요한 외부 패키지를 자동으로 설치하게 됩니다.  
외부 패키지들은 크게 OS에 제공하는 것들, 파이썬 운영에 필요한 것들, nodejs 운영에 필요한 것들로 나뉘게 됩니다.
```
cd /srv/maum
./m2u-init.sh
```

#### 오프라인으로 설치하기
아래 명령은 인터넷에 연결되어 있지 않은 폐쇄망에서 실행 가능한 쉘 스크립트입니다.  
이는 추가적으로 제공되는 4개 파일을 설치하고 필요한 추가적인 설정을 마치도록 되어 있습니다.  
```
cd /srv/maum
./init-offline.sh
```
폐쇄망이나 독립된 환경의 경우 네트워크가 활성화 되지 않으면 m2u 설치 후 `setup config` 명령으로 초기화할 때 네트워크 관련 에러가 발생될 수 있습니다.  
이러한 상황에서는 네트워크를 활성화 시켜줘야 합니다.

<!--
(2018.4.27 추가)  
폐쇄망이나 standalone 환경에서 m2u설치 후 `setup config` 시 network 관련하여 에러가 발생되고 있습니다.  
이는 ethernet이 활성화 되지 않아 발생되는 현상으로 아래 명령어를 통해 ethernet 활성화 후 `setup config` 를 진행해야 합니다.

```
nmcli d             // network manager command line으로 네트워크 확인

장치         유형      상태           연결    
docker0      bridge    연결됨         docker0
enp5s0       ethernet  연결 끊겼음     HANA
lo           loopback  관리되지 않음  --
```

```
sudo ip link set enp5s0 up                          // ip link 명령으로 ethernet 활성화
sudo ip addr add 10.10.10.2/24 dev enp5s0 brd +     // ip addr 명령으로 IP 부여
sudo ip route add default via 10.10.10.1            // ip route 명령으로 route 설정
```
-->