# 설치하기 전에 필요한 환경

## 최소 지원 OS
- Linux Ubuntu 16.04
- CentOS 7
- Redhat 7

## 설치할 시스템에 m2u 플랫폼을 위한 계정을 생성하기

m2u 플랫폼은 우분투 및 CentOS 리눅스를 기반으로 동작합니다.

마인즈랩에서 제공하는 모든 제품은 maum 이라는 계정을 사용하며 이를 위한 계정을 만들어야 합니다.  
또한 마인즈랩의 모든 제품은 기본적으로 수퍼유저를 사용하여 실행되지 않고 반드시 `maum`계정만을 이용해서 동작하도록 되어 있으며
보안 상의 문제를 야기할 수 있기 때문에 모든 프로그램들은 수펴유저가 아닌 상태에서 실행되도록 설계되어 있습니다.

만일 시스템에 다른 기본 사용자가 없으면 1000 번 uid를 이용해서 시스템 계정이 생성될 것입니다. 여기에 제약이 있지는 않습니다.

OS를 설치할 때 기본 계정으로 `maum`를 지정하지 않은 경우라면 아래의 명령어를 통해서 maum 라는
계정을 설정해주는 주어야 합니다. 만일 다른 계정을 선택해서 사용해도 시스템이 동작하는 데는 무리가
없습니다.

예를 들어 AWS의 기본 우분투 인스턴스는 `ubuntu`를 사용하는데 아래의 명령을 통해서 필요한 `maum`
계정을 생성할 수 있습니다.

```bash
sudo groupadd maum
sudo cat /etc/group | grep maum
sudo useradd -gmaum -Gminds,sudo -m -s /bin/bash -c "maum.ai Platform user" maum
sudo passwd maum
```

### 기본 시스템 환경변수 설정하기
새로 만들어진 maum 계정이 동작하는지 확인하기 위해서 먼저 로그인을 시도합니다.
```bash
sudo su - maum
```

새로 만들어진 maum 계정을 위해서는 기본적인 환경 변수가 필요합니다. 이를 위한 설정을
다음 환경변수를 `.profile`에 적절하게 넣어주어야 합니다.

- `PATH`에는 m2u 플랫폼이 설치된 디렉토리의 `bin`디렉토리를 가지고 있어야 합니다.
- `LD_LIBRARY_PATH`에는 m2u 플랫폼의 설치 디렉토리의 `lib`을 참조해야 합니다.
- `MAUM_ROOT`는 설치된 ROOT 디렉토리를 지정합니다.

본 매뉴얼에서는 `/srv/maum`를 기본 설치 디렉토리로 지정하여서 설치합니다. 다음의 예제도
여기에 따라서 설명합니다.

```bash
cat > .profile << EOT
export MAUM_ROOT=/srv/maum
export PATH=$PATH:$MAUM_ROOT/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MAUM_ROOT/lib
EOT
```
