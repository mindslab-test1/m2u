# M2U 서비스의 설정

M2U 플랫폼의 자체 설정 파일은 `$MAUM_ROOT/etc/m2u.conf` 파일입니다.

## 설정파일
```
#
# maum.ai M2U Configuration
#

maum.runtime.home = ${MAUM_ROOT}
pid.dir = ${MAUM_ROOT}/run
logs.dir = /home/minds/maum/logs
conf.dir = ${MAUM_ROOT}/etc
run.dir = ${MAUM_ROOT}/run
da.dir = ${MAUM_ROOT}/da
lg.logs.dir = ${MAUM_ROOT}/tlo

# LOG SETTINGS
#logs.max-file-size = 5M
#logs.rotate.count = 10

## M2U MAP
map.export.port = 9911
map.listen = 0.0.0.0:9911
map.export = 192.168.255.34:9911
map.default.chatbot = echo
map.default.expectspeech = 5000

## M2U AUTH(MAP)
auth.export.port = 9950
auth.grpc.timeout = 20000
auth.export.ip = 192.168.255.34:9950

## M2U Image Recognition Server
image.export.ip = 192.168.255.34:9950

## M2U FRONT REST
front.rest.server.port = 9999

## M2U FRONT
frontd.listen = 0.0.0.0:9901
frontd.export.ip = 192.168.255.34
frontd.export.port = 9901
frontd.export = 192.168.255.34:9901

## BrainQA Dialog Info
maum.qa.listen.ip = 0.0.0.0
maum.qa.listen.port = 15142

## M2U FRONT AUTH
## FIXME auth 분리해 내기..
frontd.auth.rest.url = http://localhost:3000
## not working
frontd.auth.rest.url.path = /api/v1/SvcUsers/LSA-check-access
frontd.auth.rest.url.apikey = auth-apikey

## TIMEOUT
frontd.grpc.timeout = 15000

### SESSION EXPIRATION
frontd.session.monitor.interval = 5
frontd.session.monitor.expiration = 600
logger.session.monitor.expiration = 600

### M2U ROUTER
routed.listen = 0.0.0.0:9930
routed.grpc.timeout = 15000

## DEBUG LOG
log.talk.elapsed.time.enabled = true

## HAZELCAST
#hazelcast.group.name = dev
#hazelcast.group.password = dev-pass
hazelcast.group.name = m2u
hazelcast.group.password = xmqtTcmVPCAVCt3d79Vd
hazelcast.server.count = 1
hazelcast.server.ip.1 = 192.168.255.34
hazelcast.server.port = 5701

## BRAIN STT
brain-stt.sttd.front.remote = 192.168.255.34:9801
brain-stt.sttd.default.model = baseline

## STT16K CLIENT API
etri.stt16k.server.kr.ip = 127.0.0.1
etri.stt16k.server.kr.port = 2001
etri.stt16k.server.en.ip = 127.0.0.1
etri.stt16k.server.en.port = 2002

## CoreVoice TTS
corevoice.tts.server.kr.ip = 10.122.64.121
corevoice.tts.server.kr.port = 40050
corevoice.tts.server.en.ip = 10.122.64.121
corevoice.tts.server.en.port = 40040

## IMAGED (TODO: test again)
imaged.export.ip = 10.122.64.63
imaged.export.port = 19090
imaged.enabled = true

## TEXT ANALYZER COMMON
ta.resource.path = ${MAUM_ROOT}/resources/ta
ta.resource.path.kor = ${MAUM_ROOT}/resources/ta/kor
ta.resource.path.eng = ${MAUM_ROOT}/resources/ta/eng

## DOMAIN RESOLVER
maum-ta.cl.front.remote = 192.168.255.34:9820

### LANG ANALYZER
brain.nlp.1.kor.remote = 192.168.255.34:9811
brain.nlp.2.kor.remote = 192.168.255.34:9813
brain.nlp.2.eng.remote = 192.168.255.34:9814

### SIMPLE CLASSIFIER
## REMOVE
dr.scd.listen = 0.0.0.0:9905
dr.scd.export.ip = 192.168.255.34
dr.scd.export.port = 9905

### DAM SERVICE SERVER
dam-svcd.listen = 0.0.0.0:9907
dam-svcd.export.ip = 192.168.255.34
dam-svcd.export.port = 9907
dam-svcd.port.start = 11000
dam-svcd.state.file = dam.state.json
dam-svcd.child.wait = 1000
dam-svcd.random.min.port = 30000
dam-svcd.random.max.port = 30040
dam-svcd.grpc.timeout = 10000
# dam-svcd.java.path = /usr/bin/java
# dam-svcd.python.path = /usr/bin/phthon2.7

## M2U POOL
pool.listen.port = 9931
pool.export = 192.168.255.34:9931
pool.grpc.timeout = 20000

## M2U ADMIN
admin.listen = 0.0.0.0:9922
admin.export = 192.168.255.34:9922
admin.export.ip = 192.168.255.34
admin.export.port = 9922
admin.grpc.timeout = 20000

## M2U SUPERVISORD REGISTRATION
svreg.targets[] = 192.168.255.34:9922

## M2U LOGGER
logger.driver.classname = com.mysql.jdbc.Driver
logger.user = maum
logger.pass = ggoggoma
logger.url = jdbc:mysql://localhost:3306/admin_console
logger.init.size = 10
logger.max.size = 100


## for external logging
frontd.plugin.count = 1
frontd.plugin.1.so = ${MAUM_ROOT}/lib/libcalllog-plugin-sample.so
frontd.plugin.1.entrypoint = init


## M2U INTENT-FINDER
itfd.customscript.path = ${MAUM_ROOT}/custom-script
itf.pcre.enable = true
itf.pcre.resource.path = ${MAUM_ROOT}/resources/pcre
brain-ta.cl.model.dir = ${MAUM_ROOT}/trained/classifier
brain-ta.resource.2.kor.path = ${MAUM_ROOT}/resources/nlp2-kor
brain-ta.resource.2.eng.path = ${MAUM_ROOT}/resources/nlp2-eng
# do not delete trailing '/'
dr.scd.lib.kor = ${MAUM_ROOT}/libexec/simple-classifier/kor/
dr.scd.lib.eng = ${MAUM_ROOT}/libexec/simple-classifier/eng/

## M2U SPEECH SYNTHESIZER
speech-synthesizer.listen = 0.0.0.0:9903
speech-synthesizer.export = 192.168.255.34:9903
speech-synthesizer.port = 9903
speech-synthesizer.grpc.timeout = 10000
speech-synthesizer.encoding = LINEAR16
speech-synthesizer.sample-rate = 16000


## Dialog Trigger (Optional) set manually to enable this function.
# dialog.trigger.ip = 0.0.0.0
# dialog.trigger.port = 9960


## M2U Rest java conf

# M2U Rest java configuration [option : default, all]
# TODO server.port 대신 다른 name으로rest port  세팅
server.port = 9921

## M2U Logger
logger.startsec = 0
logger.periodsec = 5
logger.zombie.chatbot.session.timeout = 2000

```

## 설정파일의 변경
설정파일의 내용을 변경하려면 `$MAUM_ROOT/bin`에서 `setup config`명령을 사용해서 처리해야 합니다.

다음은 수작업으로 변경이 가능한 몇가지 항목에 대한 내용을 설명합니다.

- logs.dir

  로그가 쌓이는 디렉토리를 지정할 수 있습니다.

- da.dir

  DA 실행 파일들의 디렉토리를 설정할 수 있습니다.

- logs.max-file-size

  로그 파일의 최대 크기를 바꿀 수 있습니다. M, G가 허용됩니다.

- logs.rotate.count

  로그 로테이트의 개수를 지정할 수 있습니다. 기본값은 10개입니다.

- frontd.session.monitor.interval

  세션이 만료되었는지를 점검하는 주기입니다. 기본 5초 마다 점검합니다.

- frontd.session.monitor.expiration

  기본값은 600초, 즉 10분입니다. 더 줄일 수 있습니다.

- log.talk.elapsed.time.enabled

  대화 처리의 각 구간별로 처리될 수 있는 시간을 조정할 수 있습니다.
  기본적으로 기록을 남깁니다.

- hazelcast.server.count

  헤이즐캐스트의 서버가 하나 이상일 경우에 지정합니다.
  헤이즐캐스트 서버가 1개 이상일 경우에는
  `hazelcast.server.ip.1`, `hazelcast.server.ip.2`와 같은 변수를 지정하여
  처리합니다.

- brain-stt.sttd.front.remote

  STT 서버의 로드밸런스 IP와 포트를 지정합니다.

- brain-stt.sttd.default.model

  STT 기본 학습 이미지를 사용합니다. 각 서비스 그룹 단위로 서로 다른 STT 학습 이미지를
  지정할 수 있습니다. 아무런 설정이 되지 않은 경우에 사용할 기본 학습 이미지를 정의합니다.
  기본값은 `baseline` 입니다.

- corevoice.tts.server.kr.ip

  TTS 한국어 서비스의 IP를 지정합니다.

- corevoice.tts.server.kr.port

  TTS 한국어 포트를 지정합니다. 포트에 따라서 음색이 달라질 수 있습니다.
  자세한 것은 코어보이스에 문의해야 합니다.


- corevoice.tts.server.en.ip

  TTS 영어 서비스의 IP를 지정합니다.

- corevoice.tts.server.en.port

  TTS 영어 포트를 지정합니다. 포트에 따라서 음색이 달라질 수 있습니다.
  자세한 것은 코어보이스에 문의해야 합니다.
