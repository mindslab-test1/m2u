# TA 설정 파일

## TA 설정 파일의 예
`$MAUM_ROOT/etc/brain-ta.conf` 파일은 다음과 같은 내용을 포함하고 있습니다.

```
#
# maum.ai BRAIN NLP Configuration
#

maum.runtime.home = ${MAUM_ROOT}
pid.dir = ${MAUM_ROOT}/run
logs.dir = ${MAUM_ROOT}/logs
conf.dir = ${MAUM_ROOT}/etc
run.dir = ${MAUM_ROOT}/run

# LOG SETTINGS
#logs.max-file-size = 5M
#logs.rotate.count = 10

## TEXT ANALYZER COMMON
brain-ta.resource.1.kor.path = ${MAUM_ROOT}/resources/nlp1-kor
brain-ta.resource.1.kor.custom.path = ${MAUM_ROOT}/resources/nlp1-kor-custom
brain-ta.resource.1.kor.custom.dicgen = ${MAUM_ROOT}/resources/nlp1-kor-custom/dicgen
brain-ta.resource.2.kor.path = ${MAUM_ROOT}/resources/nlp2-kor
brain-ta.resource.2.eng.path = ${MAUM_ROOT}/resources/nlp2-eng
brain-ta.resource.1.eng.split.path = ${MAUM_ROOT}/resources/nlp1-eng-split
brain-ta.resource.word_embedding =${MAUM_ROOT}/resources/word_embedding
brain-ta.resource.3.kor.path = ${MAUM_ROOT}/resources/nlp3-kor

## BRAIN-NLP
brain-ta.nlp.1.kor.port = 9811
brain-ta.nlp.1.kor.timeout = 100000
brain-ta.nlp.2.kor.port = 9813
brain-ta.nlp.2.kor.timeout = 1000
brain-ta.nlp.2.eng.port = 9814
brain-ta.nlp.2.eng.timeout = 100000
brain-ta.nlp.word_embedding = 9822
brain-ta.nlp.3.kor.port = 9823
brain-ta.nlp.3.kor.timeout = 100000
brain-ta.nlp.3.kor.level = 0

brain-ta.nlp.3.kor.space = true
brain-ta.nlp.3.kor.morp = true
brain-ta.nlp.3.kor.ner = true
brain-ta.nlp.3.kor.wsd = false
brain-ta.nlp.3.kor.dp = false
brain-ta.nlp.3.kor.srl = false
brain-ta.nlp.3.kor.za = false
brain-ta.nlp.3.kor.module.all = false

brain-ta.hmd.front.port = 9815
brain-ta.hmd.cpp.port = 9816
brain-ta.hmd.front.timeout = 500000
brain-ta.hmd.nlp.kor.remote = 127.0.0.1:9811
brain-ta.hmd.nlp.eng.remote = 127.0.0.1:9814
brain-ta.hmd.model.dir = ${MAUM_ROOT}/trained/hmd

brain-ta.cl.front.port = 9820
brain-ta.cl.front.timeout = 5000
brain-ta.cl.model.dir = ${MAUM_ROOT}/trained/classifier
brain-ta.cl.state.file = ta-cl.db.json
brain-ta.cl.random.min.port = 60001
brain-ta.cl.random.max.port = 60040
```

## TA 설정 파일에 대한 설명
위 설정파일의 항목은 거의 대부분 변경이 불가능한 사항들입니다.
임의대로 변경한 경우에는 오동작할 수 있습니다.

포트는 다양한 곳에서 참조되므로 특별한 이유가 없는 한 변경하지 않는 것이 좋습니다.
