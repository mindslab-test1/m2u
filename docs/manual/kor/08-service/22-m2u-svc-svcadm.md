# M2U 서비스 관리자

## m2u-svcadm
- 프로세스 이름: `$MAUM_ROOT/lib/m2u-service-admin-2.1.0-all.jar`
- 로그파일: `$MAUM_ROOT/logs/m2u-svcadm.log`
- DB: HAZELCAST

M2U 서비스 관리자는 M2U admin에서 각 서비스에 필요한 정보를 전달하고, 요청값에 따라 데이터를 변경하는 역할을 수행합니다.  

아래 목록과 같이 각 서비스 동작에 필요한 정보를 Hazelcast에서 조회하여 결과값으로 전달하거나
청값을 받아 Hazelcast에 등록과 수정 그리고 삭제가 가능하도록 관리자 역할을 수행합니다.
- Chatbot
- Intent Finder Policy
- Intent Finder Instance
- Dialog Agent
- Dialog Agent Manager
- Dialog Agent Instance
- Simple Classifier
- Supervisor Interface Manager
- Authentication Policy

※ Hazelcast와의 연결을 위해 m2u-hzc가 반드시 실행되어야합니다.
