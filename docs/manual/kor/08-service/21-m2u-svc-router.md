# 라우터 서비스

## m2u-front
- 프로세스 이름: `$MAUM_ROOT/bin/m2u-routed`
- 서비스 포트: 9930
- 로그파일: `$MAUM_ROOT/logs/m2u-routed.log`

이 서비스는 M2U 플랫폼에서 대화가 진행되도록 MAP, ITF, DA를 연계합니다.

- 새 대화 세션를 생성 합니다.
- MAP이 요청한 발화나 이벤트를 수신합니다.
- 챗봇에 지정된 ITF Policy를 지원하는 ITF Instance와 연계하여 의도를 파악합니다.
- 파악된 의도를 처리하는 Dialog Agent Instance를 연계하여 발화나 이벤트를 전달합니다.
- Dialog Agent Instance의 응답에 따라 Skill을 전환합니다.
- 최종 처리된 결과을 MAP에 응답합니다.
- 대화 내역을 저장합니다.
- 대화 종료 시 세션을 무효화합니다.