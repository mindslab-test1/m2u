# 분류 및 탐지 서비스

## brain-ta:brain-cl
- 프로세스 이름: `$MAUM_ROOT/bin/brain-cld`
- 서비스 포트: 9820
- 로그파일: `$MAUM_ROOT/logs/brain-cld.log`
- 리소스
  - `$MAUM_ROOT/resourcs/nlp2-kor`
  - `$MAUM_ROOT/resourcs/nlp2-eng`

`brain-ta:brain-cl` 서비스는 다음 기능을 수행합니다.
- DNN 기반한 텍스트 분석 서비스
- DNN 분류 학습 모델 조회
- DNN 분류 학습 모델별 서비스 실행
- DNN 분류 학습 모델별 서비스 종료
- DNN 분류 학습 이미지 등록
- DNN 분류 학습 이미지 삭제

Classifier는 현재 한국어와 영어 2개의 언어를 기본적으로 서비스할 수 있습니다.

학습 리소스는 반드시 다음 디렉토리 구조로 유지해야 정상적으로 동작합니다.
- [modelname]-[lang] 구조 형식을 유지해야 합니다.
  - `nlp2-kor`
  - `nlp2-eng`
- 각 디렉토리에는 필수적으로 4개의 파일이 필요합니다.

```
minds@web01:~/maum/trained/classifier$ ls -m stock-kor/
sa_model.txt, vocab_sa.txt
```

## mindsta-cltrn.service
- 프로세스 이름: /srv/minds/bin/cltrnd
- 서비스 포트: 9821
- 리소스
  - /srv/minds/resourcs/nlp2-kor
  - /srv/minds/resourcs/nlp2-eng
- 의존하는 서비스
  - mindsta-nlpkor2.service
  - mindsta-nlpeng2.service

DNN 기반의 분류학습 이미지를 생성합니다.
- DNN 기반한 학습 실행, 영어 및 한국어
- DNN 기반한 학습 실행 결과 다운로드
- DNN 기반한 학습 중지

이 서비스를 사용하기 위해서는 GPU가 반드시 필요합니다.

## brain-hmd
- 프로세스 이름: `$MAUM_ROOT/bin/brain-hmdd`
- 서비스 포트: 9815
- 의존하는 서비스
  - brain-ta:brain-nlp1kor
  - brain-nlp2eng

`brain-hmd` 서비스는 문장에 대한 HMD 탐지 결과를 반환합니다.

## brain-full-hmdd
- 프로세스 이름: `$MAUM_ROOT/bin/brain-full-hmdd`
- 서비스 포트: 9816
- 의존하는 서비스
  - brain-nlp3kor
  - brain-nlp2eng
