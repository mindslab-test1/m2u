# M2U Logger 서비스

## m2u-logger
- 프로세스 이름: `$MAUM_ROOT/lib/m2u-logger-2.1.0-all.jar`
- 서비스 포트: 9911
- 로그파일: `$MAUM_ROOT/logs/m2u-logger.log`

Logger는 대화시작을 위해 세션이 생성되고 생성된 세션으로 대화를 시작하고 최종 대화를 종료하기까지에 대한
모든 Transaction을 Database에 기록하고 저장하는 서비스입니다.
만료된 세션은 Hazelcast에서 삭제를 통해 서비스가 원활하게 유지되도록 합니다.
세션의 관리 뿐만 아니라 토크의 이력도 관리합니다  

Logger는 다음의 기능을 수행합니다.
- Hazelcast에 등록된 chatbot 기준으로 세션수를 조회 
- 만료된 세션은 RDB에 기록하고 Hazelcast에서 삭제합니다

Logger에서 세션을 관리하는 로직은 다음과 같습니다.
1) Hazelcast에서 chatbot 정보를 획득
2) 해당 chatbot의 session_timeout 값을 조회하여 만료된 세션이 있는지 확인
3) RDB에 만료된 정보를 기록
4) Hazelcast에 정보를 삭제
