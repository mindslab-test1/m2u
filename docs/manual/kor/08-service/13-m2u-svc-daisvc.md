# dialog agent instance 관리자

## m2u-daisvc
- 프로세스 이름: `${MAUM_ROOT}/lib/m2u-daisvc-2.1.0-all.jar`
- 로그 파일: `${MAUM_ROOT}/logs/m2u-daisvc.log`
- DB : HAZELCAST

dialog agent instance를 관리합니다.
supervisor control에 hazelcast에 등록된 dialog agent instance 정보대로 실행시킵니다.

- 우선 Hazelcast와의 연결을 위해 m2u-hzc가 반드시 실행되어야합니다.
- dialog agent instance를 추가하여 hazelcast에 등록되면 해당 정보를 기준으로
  `${MAUM_ROOT}/etc/supervisor/conf.d/m2u-dai.conf` 파일이 생성되며
   이미 파일이 존재하더라도 새로 덮어씁니다.
- 해당 dialog agent instance를 XmlRpc를 이용해 supervisor에 등록합니다.
- 그리고 위 파일을 정보를 이용하여 daireg.py를 통해 pool service에 등록합니다.
- pool service에서는 dai 등록 이후 생성된 정보를 Hazelcast에 저장합니다.
