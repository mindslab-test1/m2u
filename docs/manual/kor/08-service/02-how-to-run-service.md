# 서비스 사용하기

## `svd`를 이용한 서비스
m2u 플랫폼의 서비스는 `${MAUM_ROOT}/bin` 위치의 `svd`와 `svctl`을 통해 관리됩니다.
m2u 플랫폼 실행에 관련된 모든 서비스를 시작하고 종료시킬 수 있고, 서비스의 상태를 모니터링할 수 있습니다.

### 서비스 시작을 위한 준비
`${MAUM_ROOT}/bin` 으로 이동하여 `./setup config` 명령을 통해서 m2u 관련 기본 정보를 설정합니다.
 `svctl`을 실행합니다.

### 서비스 시작
서비스를 `start` 명령을 통해 시작할 수 있습니다.
다음은 m2u 플랫폼의 모든 서비스를 시작하는 예입니다.

```
supervisor> start m2u:*
```

### 개별 서비스의 시작
설정 파일을 변경하게 되면, 필요한 경우 서비스를 다시 재시작해야 할 수 있습니다. 이경우에는 `restart` 또는 `stop`, `start` 명령을 통해서 다시 시작할 수 있습니다.
 다음은 `m2u.conf`의 hzc 서버 설정을 변경한 이후에 다시 시작하는 예입니다.

```
# edit
vi ${MAUM_ROOT}/etc/m2u.conf
## EDIT LINE `hazelcast.server.ip.1 = 10.10.20.2`
supervisor> stop m2u:m2u-hzc
supervisor> start m2u:m2u-hzc
## or
supervisor> restart m2u:m2u-hzc
```


### 서비스 실행 상태 보기
서비스의 실행 상태를 모니터링 하기 위해서는 `status` 명령을 통해서 볼 수 있습니다.

```
supervisor> status
supervisor> status m2u:*

```

### 시스템 모니터링 하기
보통 시스템에서 로그를 보기 위해서는 `tail -f`를 사용합니다.
`svctl`을 사용하여 모니터링 하기 위해서는 `tail` 명령을 사용하여 프로세스이름이나 속성 값을 지정하여 조회할 수 있습니다.
다음은 실시간으로 m2u-hzc 서비스에 대한 모니터링 예시입니다.
각각 프로세스에 대한 표준 출력을 조회하고, 가장 최근 표준 출력의 100 bytes를 조회하고,
가장 최근 표준 오류의 1600 bytes를 조회하는 명령입니다.

```
supervisor> tail -f m2u:m2u-hzc
supervisor> tail -100 m2u:m2u-hzc
supervisor> tail m2u:m2u-hzc stderr
```
