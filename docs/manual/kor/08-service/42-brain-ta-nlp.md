# NLP 서비스들

## brain-ta:brain-nlp1kor
- 프로세스 이름: `MAUM_ROOT/bin/brain-nlp-kor1d`
- 서비스 포트: 9811
- 리소스: `MAUM_ROOT/resourcs/nlp1-kor`

`brain-ta:brain-nlp1kor` 서비스는 다음 기능을 수행합니다.
- 한국어 형태소 분석
- 한국어 Named Entity 분석
- 사용자 정의 사전의 제공 및 학습

## brain-nlp2kor
- 프로세스 이름: `MAUM_ROOT/bin/brain-nlp-kor2d`
- 서비스 포트: 9813
- 리소스: `MAUM_ROOT/resourcs/nlp2-kor`

`brain-nlp2kor` 서비스는 다음 기능을 수행합니다.
- 한국어 형태소 분석
- 한국어 Named Entity 분석
- DNN 기반한 감성 태깅
- DNN 분류를 위한 학습 분석 기초 제공

## brain-nlp2eng
- 프로세스 이름: `MAUM_ROOT/bin/brain-nlp-eng2d`
- 서비스 포트: 9814
- 리소스: `MAUM_ROOT/resourcs/nlp2-eng`

`brain-nlp2eng` 서비스는 다음 기능을 수행합니다.
- 영어 형태소 분석
- 한국어 언어 Named Entity 분석
- DNN 기반한 감성 태깅
- DNN 분류를 위한 학습 분석 기초 제공

## 주의사항
- HMD 서비스를 실행할 때에는 nlpkor1 서비스와 nlpeng2 서비스가 항상 실행되어 있어야 합니다.
- DNN Classifier 학습기 서비스를 실행할 때는 nlpkor2 서비스와 nlpeng2 서비스가
항상 실행되어야 합니다.
