# 서비스

M2U 플랫폼은 여러 개의 서버들로 운영됩니다.
각각의 서버들은 각각 동작적인 서비스를 수행합니다.
이들 서버들은 모두 systemd를 통해서 시작되고 관리됩니다.

## grpc

M2U 플랫폼의 모든 서비스들은 grpc 서버들입니다.
grpc는 구글에서 만든 RPC 플랫폼으로 HTTP/2를 기반으로 동작합니다.
또한 메시지 규격은 구글의 protobuf 규격에 의존하여 동작합니다.

결론적으로 대부분의 서비스들은 grpc 서비스들입니다.

## http REST

일부 서비스들은 HTTP/1.1 REST 형식으로 동작하기도 합니다.
주로 웹 관리도구와 통신하는 서버들이 그렇게 동작합니다.

## nginx

M2U 플랫폼은 일부 웹 서비스를 위해서 nginx를 사용합니다.
nginx는 정적인 HTML, CSS 등 웹 리소스와 자바스크립트 파일들은
직접 제공하고 REST API는 리버스 프락시 방식으로 REST 서버와 연동하여
제공합니다.

## 참조할 문서들
- [서비스 개요:전체 서비스들 및 포트들](01-overview.md)
- [서비스 사용하기](02-how-to-run-service.md)
- M2U 서비스들
  - [M2U 관리 프록시](11-m2u-console.md)
  - [M2U 관리 레스트](11-m2u-console.md)
  - [M2U 대화 에이전트 관리자](12-m2u-svc-dam.md)
  - [M2U 프론트](13-m2u-svc-front.md)
  - [M2U 하젤케스트 서버](14-m2u-svc-hzc.md)
  - [M2U ITF 관리자](15-m2u-svc-itfsvc.md)
  - [M2U 로거](16-m2u-svc-logger.md)
  - [M2U 맵](17-m2u-svc-map.md)
  - [M2U 풀](18-m2u-svc-pool.md)
  - [M2U 레스트](19-m2u-svc-rest.md)
  - [M2U 라우터](20-m2u-svc-router.md)
  - [M2U 서비스 관리자](21-m2u-svc-svcadm.md)
- [M2U 서비스의 설정](90-m2u-config.md)
- 음성인식 서비스들
  - [음성인식 서비스](31-brain-stt-svc-stt.md)
  - [음성인식 서비스의 설정](32-brain-stt-config.md)
  - [음성인식 서비스 리소스들](33-brain-stt-resources.md)
- [브레인 TA](40-brain-ta.md)
  - [텍스트 분류 서비스](41-brain-ta-classifier.md)
  - [NLP 서비스](42-brain-ta-nlp.md)
  - [텍스트분석 서비스의 설정](44-brain-ta-config.md)
  - [텍스트분석 서비스의 리소스들](45-brain-ta-resources.md)
- [브레인 SDS](51-brain-sds.md)

<!---
- [NLP Wise QA](33-ml-ta-wise-nlp.md)
--->
