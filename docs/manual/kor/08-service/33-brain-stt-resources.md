# BRAIN STT 리소스들

## STT 리소스
STT 실행을 위한 리소스는 `$MAUM_ROOT/resource/stt` 디렉토리 아래에서 관리됩니다.


## STT 학습 이미지 리소스
STT 학습을 실행 중인 경우는 `$MAUM_ROOT/trained/stt` 디렉토리 아래에
학습된 이미지 리소스들이 관리됩니다.

학습 리소스는 반드시 다음 디렉토리 구조로 유지해야 정상적으로 동작합니다.
- [modelname]-[lang]-[sample rate] 구조 형식을 유지해야 합니다.
  - `baseline-kor-8000`
  - `thebank-eng-16000`
- 각 디렉토리에는 필수적으로 4개의 파일이 필요합니다.

````
minds@stt001:~/maum/trained/stt/baseline-kor-8000$ ls -m
dnn.bin, dnn.prior.bin, sfsm.bin, sym.bin
````

### 리소스 중 STT 동작에 대한 제어
- resources/stt/config/frontend_dnn.8k.cfg
- resources/stt/config/frontend_dnn.16k.cfg
- resources/stt/config/stt_laser.8k.cfg
- resources/stt/config/stt_laser.16k.cfg


위 4개 파일에 포함된 설정 파일을 변경함으로써 미세한 동작을 제어할 수 있습니다.
여기에 대한 자세한 사항은 별도 문의해야 합니다.

## STT 학습 리소스
STT 학습을 실행 중인 경우는 `$MAUM_ROOT/workspace/stt-training` 디렉토리 아래에 학습 리소스들이
관리됩니다.
