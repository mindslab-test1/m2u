# M2U 분류 서비스

## 분류 서비스의 순서

M2U 분류 서비스는 하나 이상의 서비스로 구성되어 있습니다.
M2U 프론트 서비스는 사용자의 발화가 입력되면 다음 순서로 동작합니다.

- ITF가 HMD, DNN Classifier, PCRE, CUSTOM SCRIPT FINAL 이 있습니다.
- 의도파악 수행 후 RunStyle 타입에 따라 Skill이나 Hint에 결과가 저장됩니다.
- 한국어의 경우, 필요한 경우 NER 분석을 위해서 NLP Kor#1 호출

이렇게 만들어진 결과물을 이용해서 적당한 서비스로 분기처리합니다.

## brain-ta:brain-cl

- 프로세스 이름: `$MAUM_ROOT/bin/brain-cld`
- 서비스 포트: 9820
- 로그파일: `$MAUM_ROOT/logs/brain-cld.log`
- 리소스
  - `$MAUM_ROOT/resourcs/nlp2-kor`
  - `$MAUM_ROOT/resourcs/nlp2-eng`

`brain-ta:brain-cl` 서비스는 다음 기능을 수행합니다.
- DNN 기반한 텍스트 분석 서비스
- DNN 분류 학습 모델 조회
- DNN 분류 학습 모델별 서비스 실행
- DNN 분류 학습 모델별 서비스 종료
- DNN 분류 학습 이미지 등록
- DNN 분류 학습 이미지 삭제

Classifier는 현재 한국어와 영어 2개의 언어를 기본적으로 서비스할 수 있습니다.

학습 리소스는 반드시 다음 디렉토리 구조로 유지해야 정상적으로 동작합니다.
- [modelname]-[lang] 구조 형식을 유지해야 합니다.
  - `nlp2-kor`
  - `nlp2-eng`
- 각 디렉토리에는 필수적으로 4개의 파일이 필요합니다.

## brain-ta:brain-nlp1kor

- 프로세스 이름: `$MAUM_ROOT/bin/brain-nlp-kor1d`
- 서비스 포트: 9811
- 리소스: `$MAUM_ROOT/resourcs/nlp1-kor`

`brain-ta:brain-nlp1kor` 서비스는 다음 기능을 수행합니다.
- 한국어 형태소 분석
- 한국어 Named Entity 분석
- 사용자 정의 사전의 제공 및 학습
