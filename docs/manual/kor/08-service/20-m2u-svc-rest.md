# API 레스트 서비스

## 개요
- Dialog Service의 v1, v2, v3를 모두 지원하는 `Rest Api Server` 입니다.

## 참고사항
- 현재 정식 배포된 사항은 아니며 이후 `fornt service가 제거`된 이후 통합적으로 운영이될 service입니다.
- 테스트 진행 상황
  - v1 : test 진행 중
  - v2 : 개발 미진행
  - v3 : test 완료
- 사용 포트
  - `{MAUN_ROOT}/etc/m2u.conf` : front.rest.server.port 
- 참조 서버
  - v1 : `{MAUN_ROOT}/etc/m2u.conf` : frontd.export  
  - v2 : `{MAUN_ROOT}/etc/m2u.conf` : 
  - v3 : `{MAUN_ROOT}/etc/m2u.conf` : map.export
- 사용 언어 : JAVA(v1.8)
 
## 제공 Api List
### V1
- Api url : `/api/dialog/all-chatbots`
  - 기능 : 서비스그룹 목록 조회
- Api url : `/api/dialog/chatbots`
  - 기능 : 서비스그룹 목록 조회
- Api url : `/api/dialog/chatbot/{service}`
  - 기능 : 특정 서비스그룹 조회
- Api url : `/api/dialog/chatbot/{service}/user-attrs`
  - 기능 : 사용자 확장속성 조회
- Api url : `/api/dialog/chatbot/{service}/new`
  - 기능 : 새로운 대화 세션 생성
- Api url : `/api/dialog/chatbot/{service}/session`
  - 기능 : 인증된 사용자의 대화 세션 생성
- Api url : `/api/dialog/session/{sessionId}/talk`
  - 기능 : 텍스트 대화
- Api url : `/api/dialog/session/{sessionId}/feedback`
  - 기능 : 대화 피드백
- Api url : `/api/dialog/session/{sessionId}`
  - 기능 : 세션 종료
- Api url : `/api/dialog/chatbot/{service}/pastSession`
  - 기능 : 과거 세션 조회
- Api url : `/api/dialog/session/{username}/pastTalk`
  - 기능 : 세션별 과거 대화 조회

### V2

### V3
- Api url : `/api/v3/auth/signIn`
  - 기능 : 로그인
- Api url : `/api/v3/auth/signOut`
  - 기능 : 로그아웃
- Api url : `/api/v3/dialog/open`
  - 기능 : 새로운 대화 세션 생성
- Api url : `/api/v3/dialog/close`
  - 기능 : 대화 세션 종료
- Api url : `/api/v3/dialog/textToTextTalk`
  - 기능 : Text 대화 



 

