# 대화 에이전트 관리자

## m2u-dam
- 프로세스 이름: `$MAUM_ROOT/bin/m2u-damd`
- 서비스 포트: 9907
- 로그파일: `$MAUM_ROOT/logs/m2u-damd.log`
- 런타임 파일: `$MAUM_ROOT/run/dam.state.json`
- DB: HAZELCAST

대화 에이전트들을 관리합니다.
Dialog Agent Manager를 줄여서 DAM이라고 부르고, 'daem'으로 발음합니다.

- 대화 에이전트들을 등록합니다. 대화 에이전트는 grpc 스펙을 만족하기만 하면 다양한 언어로 만들 수
있습니다. C++, Java, Python, Node 등으로 개발하여 테스트되었습니다.
- 대화 에이전트는 생명주기를 가지고 있습니다.
- DAM은 대화 에이전트로부터 실행에 필요한 여러가지 정보를 꺼내서 관리자 콘솔로 보내줍니다.
- DAM은 관리자로부터 각 에이전트의 실행에 필요한 정보를 전달받아서 DA를 생성합니다.
- DA가 죽으면 다시 실행합니다.
- 실행된 DA를 front 서버에 등록해줍니다.
