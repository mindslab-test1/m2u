# BRAIN STT 서비스

## brain-stt
- 프로세스 이름: `$MAUM_ROOT/bin/brain-sttd`
- 서비스 포트: 9801
- 로그파일: `$MAUM_ROOT/logs/brain-sttd.log`
- 리소스: `$MAUM_ROOT/resourcs/stt`
- 학습 리소스: `$MAUM_ROOT/trained/stt`

BRAIN-STT 서비스는 다음 기능을 수행합니다.
- 8K, 16K 음성 인식 서비스
- STT 학습 모델 조회
- STT 학습 모델별 서비스 실행
- STT 학습 모델별 서비스 종료
- STT 학습 이미지 등록
- STT 학습 이미지 삭제

BRAIN-STT는 현재 한국어와 영어 2개의 언어를 기본적으로 서비스할 수 있으며,
음성 샘플링 레이트는 8000, 16000을 모두 지원합니다.
