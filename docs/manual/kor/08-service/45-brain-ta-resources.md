# TA 리소스들

## 실행 리소스들
TA 실행을 위한 리소스는 `$MAUM_ROOT/resource` 디렉토리에 아래에서 관리됩니다.

각 리소스와 관련된 서비스 목록은 다음과 같습니다.
- `$MAUM_ROOT/resourcs/nlp1-kor`
  - nlp-kor1
  - HMD 한국어
- `$MAUM_ROOT/resourcs/nlp1-kor-custom`
  - nlp-kor1
- `$MAUM_ROOT/resourcs/nlp2-kor`
  - nlp-kor2
  - DNN Classifier
  - DNN 분류 학습기
- `$MAUM_ROOT/resourcs/nlp2-eng`
  - nlp-eng2
  - DNN Classifier
  - DNN 분류 학습기
- `$MAUM_ROOT/resourcs/nlp3-kor`
  - nlp-kor3

## 학습 이미지 리소스들
DNN 학습을 실행 중인 경우는 `$MAUM_ROOT/trained/classifier` 디렉토리 아래에 학습된 이미지 리소스들이 관리됩니다.

학습 리소스는 반드시 다음 디렉토리 구조로 유지해야 정상적으로 동작합니다.
- [modelname]-[lang] 구조 형식을 유지해야 합니다.
  - stock-kor
  - weather-eng
- 각 디렉토리에는 필수적으로 2개의 파일이 필요합니다.

````
minds@ta001:~/maum/trained/stt/stock-kor$ ls -m
sa_model.txt, vocab_sa.txt
````

## 학습 리소스들
DNN 분류 학습을 실행 중인 경우는 `$MAUM_ROOT/workspace/classifier` 디렉토리 아래에 학습 리소스들이 관리됩니다.
