# M2U 프론트 서비스

## m2u-front
- 프로세스 이름: `$MAUM_ROOT/bin/m2u-frontd`
- 서비스 포트: 9901
- 로그파일: `$MAUM_ROOT/logs/m2u-frontd.log`

이 프로세스는 M2U 플랫폼의 대화를 진행하는 프론트 서비스입니다.

- 새 대화 세션의 생성
- 음성 대화의 진행
- 텍스트 대화의 진행
- 과거 대화에 대한 조회
- 현재 세션의 대화 조회
- 서비스 그룹의 관리 및 조회ㅁ
- 대화에 대한 로그 저장

## m2u-rest
- 프로세스 이름: `$MAUM_ROOT/apps/web-rest/bin/rest`
- 서비스 포트: 9999

텍스트 대화 및 음성 대화에 대한 HTTP1/1 REST 인터페이스를 제공합니다.
