# SDS 서비스

## sds:brain-sds
- 프로세스 이름: `$MAUM_ROOT/bin/brain-sds-resolver`
- 서비스 포트: 9860
- 로그파일: `$MAUM_ROOT/logs/brain-sds-resolver.log`
- 런타임 파일: `$MAUM_ROOT/run/sds.state.json`
- 학습 리소스1: `$MAUM_ROOT/trained/sds-group`
- 학습 리소스2: `$MAUM_ROOT/trained/sds-model`


대화에이전트들이 대화를 진행하는데 필요한 SDS 기능을 서버 형태로 제공합니다.
SDS는 크게 2개 기능을 제공합니다.
- NLU: 대화를 이해하고 채워진 슬롯과 채워지지 않은 슬롯을 반환합니다.
- NLG: 상황에 맞게 문장을 생성합니다.

- DNN 기반한 텍스트 분석 서비스
- SDS 학습 모델 조회
- SDS 학습 모델별 서비스 실행
- SDS 학습 모델별 서비스 종료

아직은 SDS 학습 모델을 등록하는 기능은 없어서 수동으로 처리해야 합니다.
