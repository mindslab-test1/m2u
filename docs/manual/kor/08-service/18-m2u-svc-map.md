# M2U-async-proxy 서비스

## m2u-map
- 프로세스 이름: `$MAUM_ROOT/lib/m2u-map-2.1.0-all.jar`
- 서비스 포트: 9911
- 로그파일: `$MAUM_ROOT/logs/m2u-map.log`

MAP은 M2U-Async-Proxy의 약어로서 Client에서 보내는 GRPC 통신을 수신하고 
비동기 응답 Handler 체계를 구축한 서비스 입니다.
MAP은 Client로 부터 요청을 받으면 여러 서비스와 연동을 하여 결과를 전달합니다. 

MAP은 다음의 연동 기능을 수행합니다.
- 인증 서버와 연동 기능
- Router로 대화 전송 기능
- 이미지 인식 서버와 연동 기능
- STT 서버와 연동 기능
- TTS 서버와 연동 기능

그리고 MAP은 자체적인 유량제어를 통해 서비스 안정성을 제공합니다.
또한 CallLog 기능을 제공하여 서비스에 맞는 로그저장기능을 제공합니다.