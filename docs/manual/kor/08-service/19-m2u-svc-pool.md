# 풀 서비스

## m2u-pool
- 프로세스 이름: `$MAUM_ROOT/lib/m2u-pool-2.1.0-all.jar`
- 서비스 포트: 9931
- 로그파일: `$MAUM_ROOT/logs/m2u-pool.log`

이 서비스는 실행 중 자원에 대한 정보를 등록 및 취소 합니다.

- 실행 중인 Dialog Agent Instance의 정보(Chatbot, Skill, IP, Port, PID, Launcher, 시작 시간 등)를 등록할 수 있습니다.
- 실행 중인 Dialog Agent Instance가 중지될 때 해당 정보를 등록 취소할 수 있습니다.
- 등록된 정보는 router, front 등 에서 조회하여 사용합니다.