# Hazelcast In Memory Data Grid(IMDG)

## m2u-hzc{-n}
- 프로세스 이름: `$MAUM_ROOT/lib/m2u-hzc-repack-2.1.0-all.jar`
- 서비스 포트: 5701~5703
- 로그파일: `$MAUM_ROOT/logs/m2u-hzc.out`, `$MAUM_ROOT/logs/m2u-hzc.err`
- 백업파일: `$MAUM_ROOT/db/hazelcast-{n}/*.db`

이 서비스는 M2U 설정을 다른 서비스들과 공유하고, 진행 중인 대화의 이력을 저장합니다.

- 여러 서버에 여러 노드(프로세스)로 구성된 Cluster(분산저장소)를 통하여 분산된 환경에서 동일한 데이터를 공유합니다.
- 물리적인 디스크가 아닌 메모리를 이용하기 때문에 처리 속도가 매우 빠릅니다.
- 일부 노드가 다운되어 동작하지 않더라도 자체적으로 전체 데이터를 복구하여 운영될 수 있습니다.
- 서비스의 설정 정보가 별도로 백업되어 Cluster 전체를 재기동 시켜도 설정을 다시 로드 합니다.
- DAM은 대화 에이전트로부터 실행에 필요한 여러가지 정보를 꺼내서 관리자 콘솔로 보내줍니다.
- Dialog Agent Manager, Dialog Agent의 설정을 저장하고 공유합니다.
- Simple Classifier, Intent Finder Policy, Intent Finder Instance의 설정을 저장하고 공유합니다.
- Chatbot, Dialog Agent Instance의 설정을 저장하고 공유합니다.
- 진행 중인 대화의 세션정보, 대화, 분류 내역을 저장합니다.
