# 서비스 개요: 전체 서비스들 및 포트들

모든 서비스들은 grpc, REST, nginx로 분류됩니다.

아래는 그 전체적인 목록입니다.

## 서비스 전체 목록 및 포트

실행 구분 |  서비스 이름 | svd 유닛 | 구분 | 기본 사용 포트 | 관련 서비스
----------| ------- | -------| ------- | ----- | -----
m2u | M2U 관리 프록시 | m2u:m2u-admproxy | nginx | 9990 | 
m2u | M2U 관리 레스트 | m2u:m2u-admrest | rest | 9920 | 
m2u | M2U 대화 에이전트 관리자 | m2u:m2u-dam | grpc | 9907 | 
m2u | M2U 프론트 | m2u:m2u-front | grpc | 9901 | 
m2u | M2U 하젤케스트 서버 | m2u:m2u-hzc | TCP/IP 소켓 | 5701 | 
m2u | M2U ITF 관리자 | m2u:m2u-itfm | grpc | 9940 | 
m2u | M2U 로거 | m2u:m2u-logger | grpc | No | 
m2u | M2U 맵 | m2u:m2u-map | grpc | 9911 | 
m2u | M2U 풀 | m2u:m2u-pool | grpc | 9931 | 
m2u | M2U 레스트 | m2u:m2u-rest | grpc | 9999 | 
m2u | M2U 라우터 | m2u:m2u-router | grpc | 9930 | 
m2u | M2U 서비스 관리자 | m2u:m2u-svcadm | grpc | 9922 | 
hzc | 하젤케스트 맨센터 | m2u-hzcmancenter | TCP/IP 소켓 | 9988 | 
itf | ITF 샘플 | m2u-itf-sample | grpc | 50121 | 
tts | M2U TTS | m2u-tts | grpc | 9903
svreg | 서비스 레지 | svreg | grpc | No | M2U 서비스 관리자
sds | BRAIN SDS | sds:brain-sds | grpc | 9860 | 
ta | DNN 분류기 서버 | brain-ta:brain-cl | grpc | 9820 | 
ta | DNN 분류 학습기 | mindsta-cltrn.service | grpc | 9821 | 
ta | HMD 서버 | brain-hmd | grpc | 9815 | 
ta | NLP#1 한국어 서버 | brain-ta:brain-nlp1kor | grpc | 9811 | 
ta | NLP#2 한국어 서버 | brain-nlp2kor | grpc | 9813 | 
ta | NLP#2 영어 서버 | brain-nlp2eng | grpc | 9814 | 
ta | NLP#3 한국어 서버 | brain-nlp3kor | grpc | 9823 | 
ta | 워드임베딩 서버 | brain-we | grpc | 9822 |
stt | STT 서버 | brain-stt | grpc | 9801 | 
stt | STT 학습기 서버 | mindsstt-trn.service | grpc | 9802 | 
stt | STT 평가 서버 | mindsstt-eval.service | grpc | 9803 | 

## 전체 서비스와 제품

### BRAIN-STT
- brain-stt

### BRAIN-TA
- brain-nlp1kor
- brain-nlp2kor
- brain-nlp2eng
- brain-nlp3kor
- brain-cl
- brain-full-hmd
- brain-hmd
- brain-we


### M2U
- m2u:m2u-admproxy
- m2u:m2u-admrest
- m2u:m2u-dam
- m2u:m2u-front
- m2u:m2u-hzc
- m2u:m2u-itfm
- m2u:m2u-logger
- m2u:m2u-map
- m2u:m2u-pool
- m2u:m2u-rest
- m2u:m2u-router
- m2u:m2u-svcadm
- m2u-hzcmancenter
- m2u-itf-sample
- m2u-tts
- svreg

### BRAIN-SDS
- sds:brain-sds
