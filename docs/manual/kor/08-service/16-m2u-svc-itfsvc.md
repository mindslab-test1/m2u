# intent finder instance 관리자

## m2u-itfsvc
- 프로세스 이름: `${MAUM_ROOT}/lib/m2u-itfsvc-2.1.0-all.jar`
- 로그 파일: `${MAUM_ROOT}/logs/m2u-itfsvc.log`
- DB : HAZELCAST

intent finder instance를 관리합니다.
supervisor control에 hazelcast에 등록된 intent finder instance 정보대로 실행시킵니다.

- intent finder instance를 추가하여 hazelcast에 등록되면 해당 정보를 기준으로
  `${MAUM_ROOT}/etc/supervisor/conf.d/m2u-itf.conf` 파일이 생성되며
   이미 파일이 존재하더라도 새로 덮어씁니다.
   위 파일을 정보를 기준으로 supervisor control에서 해당 intent finder instance 프로세스를 자동으로 실행합니다.
- intent finder instance를 시작하여 실행하는 도중에 오류가 발생하면 프로세스를 중지 한 후 재시작합니다.
- Hazelcast와의 연결을 위해 m2u-hzc가 반드시 실행되어야합니다.
