# STT 설정 파일

## STT 설정 파일
`$MAUM_ROOT/etc/brain-stt.conf` 파일은 다음과 같은 내용을 포함하고 있습니다.


```
#
# maum.ai BRAIN STT Configuration
#

maum.runtime.home = ${MAUM_ROOT}
pid.dir = ${MAUM_ROOT}/run
logs.dir = ${MAUM_ROOT}/logs
conf.dir = ${MAUM_ROOT}/etc
run.dir = ${MAUM_ROOT}/run

# LOG SETTINGS
#logs.max-file-size = 5M
#logs.rotate.count = 10

## STTD
brain-stt.sttd.front.export.ip = 127.0.0.1
brain-stt.sttd.front.port = 9801
brain-stt.sttd.front.timeout = 2147483647
brain-stt.sttd.state.file = stt.state.json
## 0 : auto, N : concurrent channel count
brain-stt.sttd.max.stream = 60
brain-stt.sttd.use.gpu = true
brain-stt.sttd.num.cores = 1

## CONFIG FILE
brain-stt.config.frontend.dnn.8k  = ${MAUM_ROOT}/resources/stt/config/frontend_dnn.8k.cfg
brain-stt.config.frontend.dnn.16k = ${MAUM_ROOT}/resources/stt/config/frontend_dnn.16k.cfg
brain-stt.config.frontend.dnn.epd = ${MAUM_ROOT}/resources/stt/config/frontend_dnn.epd.cfg
brain-stt.config.8k.laser         = ${MAUM_ROOT}/resources/stt/config/stt_laser.8k.cfg
brain-stt.config.16k.laser        = ${MAUM_ROOT}/resources/stt/config/stt_laser.16k.cfg

brain-stt.spl.postproc.tagging  = ${MAUM_ROOT}/resources/stt/tools/tagging.release.bin
brain-stt.spl.postproc.chunking = ${MAUM_ROOT}/resources/stt/tools/chunking.release.bin
brain-stt.spl.postproc.userdic  = ${MAUM_ROOT}/resources/stt/tools/user_dic.txt
brain-stt.silence.dnn.data      = ${MAUM_ROOT}/resources/stt/tools/sil_dnn.data
brain-stt.spl.temp.dir = ${MAUM_ROOT}/run/spl

## STT TRAINED DATA
### each directory has 4 files with extension.
### sfsm.bin, sym.bin, dnn(.bin), dnn.prior.bin
### if sam.bin, lda.bin exists, it overrides default vam, norm configuration.
brain-stt.trained.root = ${MAUM_ROOT}/trained/stt
brain-stt.trained.default.model = baseline

## STT RESOURCES
brain-stt.resource.root = ${MAUM_ROOT}/resources/stt
brain-stt.resource.tools = ${MAUM_ROOT}/resources/stt/tools

brain-stt.resource.dnn-image.kor.vam.sh  = ${MAUM_ROOT}/resources/stt/dnn-image/stt_release.sam.bin.sh
brain-stt.resource.dnn-image.kor.norm.sh = ${MAUM_ROOT}/resources/stt/dnn-image/final.dnn.lda.bin.sh
brain-stt.resource.dnn-image.kor.vam.lg  = ${MAUM_ROOT}/resources/stt/dnn-image/stt_release.sam.bin.lg
brain-stt.resource.dnn-image.kor.norm.lg = ${MAUM_ROOT}/resources/stt/dnn-image/final.dnn.lda.bin.lg
brain-stt.resource.dnn-image.kor.vam.16k  = ${MAUM_ROOT}/resources/stt/dnn-image/stt_release.sam.bin.16k
brain-stt.resource.dnn-image.kor.norm.16k = ${MAUM_ROOT}/resources/stt/dnn-image/final.dnn.lda.bin.16k

brain-stt.resource.dnn-image.eng.vam.16k  = ${MAUM_ROOT}/resources/stt/dnn-image/stt_release.sam.bin.eng.16k
brain-stt.resource.dnn-image.eng.norm.16k = ${MAUM_ROOT}/resources/stt/dnn-image/final.dnn.lda.bin.eng.16k
brain-stt.resource.dnn-image.eng.vam.8k  = ${MAUM_ROOT}/resources/stt/dnn-image/stt_release.sam.bin.eng_4th
brain-stt.resource.dnn-image.eng.norm.8k = ${MAUM_ROOT}/resources/stt/dnn-image/final.dnn.lda.bin.eng_4th
```

## STT 설정 파일에 대한 설명
위 설정파일의 항목은 거의 대부분 변경이 불가능한 사항들입니다.
임의대로 변경한 경우에는 오동작할 수 있습니다.

포트는 다양한 곳에서 참조되므로 특별한 이유가 없는 한 변경하지 않는 것이 좋습니다.
