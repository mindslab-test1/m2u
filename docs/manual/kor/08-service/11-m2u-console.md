# M2U 관리 콘솔

## m2u-admrest
- 프로세스 이름: node $MAUM_ROOT/apps/admin-rest/server/server.js
- 서비스 포트: 9920

관리자 웹 UI에 대응하는 웹 서비스를 제공합니다.
인증 기능을 수행합니다.

## m2u-admproxy
- 프로세스 이름: nginx -p $MAUM_ROOT/run/m2u-admin -c $MAUM_ROOT/etc/nginx/m2u-admin.conf
- 서비스 포트: 9990

관리자 웹 서비스 화면을 제공합니다.
