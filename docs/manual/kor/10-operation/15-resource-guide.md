# 리소스 가이드

아래 결과를 적정량 산정을 위한 고려 사항들을 기술합니다.

## Front 서버
- `minds-front.service`를 주로 올립니다.
- `minds-rest.service`를 경우에 따라서 올립니다.
- `minds-admin.service`, `minds-adminlb.service`, `minds-console.service`를 올립니다.


- CPU: 2 코어 이상
- Memory: 8 기가 이상

## 분류기 서버
- `minds-sc.service`를 주로 올립니다.
- `mindsta-nlpkor1.service`를 경우에 따라서 올립니다.
- `mindsta-cl.service`를 올립니다.


- CPU: 4 코어 이상
- 메모리: 32 기가 이상

주의사항: DNN 분류기 서비스는 하나의 모델 당 매우 많은 메모리를 요구합니다.
한국어의 경우 2기가, 영어의 경우 5기가를 요구합니다.
하드웨어 및 분산 배치를 고려할 때 실행할 DNN 분류기의 개수를 고려해야 합니다.

## 대화 에이전트 서버
- `minds-sds.service`를 주로 올립니다.
- `minds-dam.service`를 경우에 따라서 올립니다.


- CPU: 4 코어 이상
- 메모리: 16 기가 이상

실행할 DA에 따라서 매우 다양한 결과가 존재할 수 있습니다.
