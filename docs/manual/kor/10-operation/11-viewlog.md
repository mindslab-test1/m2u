# 로그보기

## 2가지 로그
M2U 플랫폼의 로그는 크게 수퍼바이저가 남기는 로그와 각 프로세스들이 남기는 로그로 구분됩니다.


### 수퍼바이저 로그
수퍼바이저 로그는 `svd`나 `svctl`을 통해서 전송된 메시지들을 기록하는 것입니다.  
이는 통상적으로 `$MAUM_ROOT/logs/supervisor`에 기록되며 이 로그는 M2U 플랫폼이 `svd`를 사용하여 서비스를 기동하기 때문에 생깁니다.


### 프로세스별 로그
기본적으로 프로세스 로그는 `$MAUM_ROOT/logs` 디렉토리에 남습니다.  
`m2u.conf` 파일을 수정하여 `logs_dir` 값을 수정한 경우에는 변경된 디렉토리에 로그가 남습니다.  
각 프로세스의 로그 파일이름은 일정한 규칙을 가지고 있습니다.  
다음은 각 프로세스별 로그 파일과 수퍼바이저 로그의 이름입니다.

**서비스이름** | **프로세스명** | **로그 파일 이름**
------- | ------ | -------
M2U 관리 프록시 | m2u:m2u-admproxy | nginx-m2u-admin-access.log </br> nginx-m2u-admin-error.log
M2U 관리 레스트 | m2u:m2u-admrest | m2u-admrest.err </br> m2u-admrest.out
M2U 대화에이전트 관리자 | m2u:m2u-dam | m2u-damd.log
M2U 프론트 | m2u:m2u-front | m2u-frontd.log
M2U 하젤케스트 서버| m2u:m2u-hzc | m2u-hzc.err </br> m2u-hzc.out
M2U ITF 관리자 | m2u:m2u-itfm  | m2u-itfm.log
M2U 로거 | m2u:m2u-logger | m2u-logger.log
M2U 맵 | m2u:m2u-map | m2u-map-$date-$time.log
M2U 풀 | m2u:m2u-pool | m2u-map.log
M2U 레스트 | m2u:m2u-rest | m2u-rest.log
M2U 라우터 | m2u:m2u-router | m2u-routed.log
M2U 서비스 관리자 | m2u:m2u-svcadm | m2u-svcadm.log
하젤케스트 맨센터 | m2u-hzcmancenter | m2u-hzcmancenter.err </br> m2u-hzcmancenter.out
ITF 샘플 | m2u-itf-sample | itf-sample.err </br> itf-sample.out
M2U TTS | m2u-tts | m2u-ttsd.log
서비스 레지 | svreg | svreg.err
DNN 분류기 | brain-cl | brain-cld.log
BRAIN FULL HMD 서비스 | brain-full-hmd | brain-full-hmdd.log
BRAIN HMD 서비스 | brain-hmd | brain-hmd.err </br> brain-hmd.out
NLP KOR #1 서비스 | brain-nlp1kor | brain-nlp-kor1d.log
NLP KOR #2 서비스 | brain-nlp2kor | brain-nlp-kor2d.log
NLP ENG #2 서비스 | brain-nlp2eng | brain-nlp-eng2d.log
NLP KOR #3 서비스 | brain-nlp3kor | brain-nlp-kor3d.log
BRAIN SDS 서비스 | brain-sds | brain-sds-resolver.log
STT 서버 | brain-stt | brain-sttd.log
워드 임베딩 | brain-we | brain-we.err </br> brain-we.out


### nginx 로그
`m2u-admproxy` 서비스는 nginx로 구동됩니다.  
nginx의 접근 로그는 다른 방식의 이름 규칙을 가지고 있고 이 로그도 기본적으로 `$MAUM_ROOT/logs` 디렉토리에 `nginx-m2u-admin-access.log` 파일로 남습니다.  
에러 로그는 `$MAUM_ROOT/logs/nginx-m2u-admin-error.log`이고 nginx의 규격으로 로그를 남기며 이러한 nginx 로그에 대한 설정은 `$MAUM_ROOT/etc/nginx/m2u-admin.conf` 에 설정되어 있습니다.  


## 로그 보기
각 log는 아래 명령어와 같이 리눅스 bash 쉘의 tail 명령어를 이용하여 실시간으로 확인할 수 있습니다.
필요한 경우 파일의 범위를 제약해서 볼 수 있습니다.
```
tail -f $MAUM_ROOT/logs/*.log
tail -f $MAUM_ROOT/logs/*
```


### journalctl 을 이용해서 로그 보기

자세한 것은 journalctl 매뉴얼을 참조하시기 바랍니다.
<!---
(TODO) 더 보완해야 합니다.
--->
