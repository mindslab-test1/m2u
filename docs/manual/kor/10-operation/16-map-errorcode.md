# MAP 에러코드

MAP은 에러가 발생한 경우 Client에게 상세한 에러정보를 Client에게 전달합니다.
GRPC 통신 도중 발생한 에러인 경우 500대 코드를 전달하고
Client 오류나 인증오류인 경우 300대 코드를 전달하고
ROUTER에서 에러가 발생한 경우 400대 코드를 전달합니다. 

## MAP sendException 동작 정리
MAP에서 에러 또는 Exception 이 발생하게 되면 아래와 같이 동작합니다.
Client에게 필요한 정보를 모두 전달할 수 있도록 에러코드, MAP 에러위치, 에러메시지, EventStream, json 데이터까지 전달합니다. 

```
map.sendException(
StatusCode.GRPC_STT_ERROR  //code 상태 코드
, exIndex(2)               //exIndex 예외 발생한 위치를 식별할 수 있는 정보
, msg                      //exMessage 예외 메시지
, callEvent                //refEvent 관련 EventStream
, tempPayloadJson          //json 페이로드 데이터
);
```

## Client 에러메시지 수신의 예
MAP에서 sendException을 호출하면 Client로 아래와 같이 json 메시지가 전달됩니다.

```
{
  "exceptionId": "b7913376-526f-4852-8c6f-389d8af95026",
  "statusCode": "ROUTER_CHATBOT_NOT_FOUND",
  "exMessage": "Failed to create a New Session",
  "exIndex": 801,
  "payload": {
    "list": [{
      "module": "M2U",
      "process": 3.0,
      "code": "MEM_KEY_NOT_FOUND",
      "errorIndex": 90301.0,
      "message": "Failed to create a New Session",
      "details": []
     }]
  },
  "thrownAt": "2018-07-03T08:39:29.748Z",
  "streamId": "c2517a75-9217-487d-85b3-a56ef6aa0c4e",
  "operationSyncId": "TzuLyJbwScON0yu7MNtv4A1STUHAVED0",
  "calledInterface": {
    "interface": "Dialog",
    "operation": "Open"
    }
  }
}
```

## Client에서 에러메시지를 수신하기 위한 소스코드 예시
Client에서는 onException 콜백함수를 등록하여 MAP에서 전달하는 에러메시지를 수신합니다.

```
  /**
   * 예외가 발생한 경우에 발생할 콜백
   * @param {MapException} e MapException
   */
  onException: function (e) {
    console.log('statusCode : '+ e.statusCode);
    console.log('exIndex : '+ e.exIndex);
    console.log('exMessage : '+ e.exMessage);
    console.log('operationSyncId : '+ e.operationSyncId);
    console.log('streamId : '+ e.streamId);
    console.log('payload : ' + JSON.stringify(e.payload));
  }
```

## Client에서 필요한 정보
1. statusCode : 해당 코드로 어떤 에러인지 구별가능
2. exIndex : 해당 값으로 MAP의 어디서 에러가 났는지 디버깅 정보제공
3. exMessage : 해당 메세지로 코드에 대한 상세 내용 파악
4. operationSyncId : Client가 보낸 EventStream에 해당하는 ID로서 어떤 EventStream에 에러가 발생했는지 파악 가능
5. streamId : MAP에서 보낸 Stream의 ID
6. payload : Exception에 해당하는 json 전문이 전달된다 (client에서 필요한 경우 payload값을 parsing해서 사용한다)

## 상세한 에러코드

내부이름 | 에러코드 | 메시지 | 발생 상황
-----|-----|--|--|--
STATUS_NOT_SPECIFIED     | 0     | - | 지정한 에러코드가 없는 경우
GRPC_STT_ERROR     | 501     | STT error occurred. | STT GRPC 통신 에러
GRPC_IDR_ERROR     | 502     | IDR error occurred. | IDR GRPC 통신 에러
GRPC_TTS_ERROR     | 503     | TTS error occurred. | TTS GRPC 통신 에러
GRPC_AUTH_SIGN_IN_ERROR     | 504     | signIn error occurred. | 인증 sign in GRPC 통신 에러
GRPC_AUTH_SIGN_OUT_ERROR     | 505     | signOut error occurred. | 인증 sign out GRPC 통신 에러
GRPC_AUTH_MULTIFACTOR_VERIFY_ERROR     | 506     | multiFactorVerify error occurred. | 인증 multifactor verify GRPC 통신 에러
GRPC_AUTH_IS_VALID_ERROR     | 507     | isValid error occurred. | 인증 is valid GRPC 통신 에러
GRPC_AUTH_GET_USER_INFO_ERROR     | 508     | getUserInfo error occurred. | 인증 get user info GRPC 통신 에러
GRPC_AUTH_UPDATE_USER_SETTINGS_ERROR     | 509     | updateUserSettings error occurred. | 인증 updateUserSettings GRPC 통신 에러
GRPC_AUTH_GET_USER_SETTINGS_ERROR     | 510     | getUserSettings error occurred. | 인증 getUserSettings GRPC 통신 에러
GRPC_ROUTER_OPEN_ERROR     | 511     | Router error occurred | Router GRPC 통신 에러
GRPC_ROUTER_TALK_ERROR     | 512     | Router error occurred | Router GRPC 통신 에러
GRPC_ROUTER_EVENT_ERROR     | 513     | Router error occurred | Router GRPC 통신 에러
GRPC_ROUTER_CLOSE_ERROR     | 514     | Router error occurred | Router GRPC 통신 에러
GRPC_ROUTER_FEEDBACK_ERROR     | 515     | Router error occurred | Router GRPC 통신 에러
GRPC_STT_TRANSCRIPT_NULL_ERROR     | 516     | There is no stt's transcript | STT transcript null 에러
AUTH_IS_VAILD_FAILED     | 301     | isValid error occurred. | isVaild Exception이 발생한 경우
AUTH_INVALID_AUTH_TOKEN     | 302     | Token has expired. Please retry verification. | isValid 결과 false인 경우(토큰만료)
AUTH_FAILED     | 303     | checkAuth error occurred. | 인증 실패
AUTH_INVALID_HEADER     | 304     | Invalid authentication header value. | 인증시 필요한 헤더정보가 잘못 설정된 경우
AUTH_CHECK_AUTH_FAILED     | 305     | checkAuth error occurred. | 토큰 유효성 체크 오류
MAP_NO_STREAM_PARAM     | 310     | No parameter for eventRouter | 스트림 형식의 요청에서 파라미터가 없는 경우
MAP_IF_NOT_FOUND     | 311     | Interface not found | 요청하는 AsyncInterface가 존재하지 않는 경우
MAP_IF_STREAMING_NOT_MATCH     | 312     | invalid stream's interface. end for break | 인터페이스의 스트림형식과 실제 구현된 대상 인터페이스의 스트림 형식이 맞지 않는 경우
MAP_IF_DUPLICATED_STREAMING     | 313     | Currently streaming is processing, new streaming requested | Streaming 처리 중에 또다른 요청이 들어온 경우
MAP_EVENT_CASE_NOT_SET     | 314     | Event case not set | 등록되지 않은 Event case인 경우
MAP_CURRENTLY_HAS_NO_STREAM     | 315     | Currently there is no stream id | 현재 진행중인 StreamId가 없는 경우
MAP_STREAM_ID_NOT_MATCH     | 316     | invalid stream id for cancel | 현재 진행중인 StreamId와 일치하지 않는 경우
MAP_PAYLOAD_ERROR     | 317     | json merge error occurred. | MessageFormat 관련 처리에 에러가 발생한 경우
MAP_CLASS_NOT_FOUND     | 318     | map class not found. | classLoader 에러 발생한 경우
ROUTER_SESSION_NOT_FOUND     | 401     | Session is not found. | 요청한 EventStream의 Session값이 없는 경우
ROUTER_SESSION_INVALID     | 402     | Session is invalidated. | 요청한 EventStream의 Session값이 유효하지 않은 경우
ROUTER_CHATBOT_NOT_FOUND     | 403    | Chatbot is not assigned | Chatbot을 찾지 못한 경우
ROUTER_DA_NOT_FOUND     | 404    | DAI not found for Chatbot, Skill | 발화에 대한 DA를 못찾은 경우
ROUTER_DA_ERROR     | 405    | DAI not found for Chatbot, Skill | 발화에 대한 DA를 못찾은 경우
ROUTER_ITF_NOT_FOUND     | 406    | Cannot find intent finder instance for | ITF를 못찾은 경우
ROUTER_ITF_ERROR     | 407    | Failed to Resolve a skill. | 해당 skill을 Resolve 처리하지 못한 경우
MAP_TOTAL_SESSION_COUNT_EXCEEDED     | 408    | The number of sessions has been exceeded. | hzc 총 세션수가 기준범위를 초과한 경우 (유량제어)
MAP_SYSTEM_MAINTENANCE     | 409    | 작업중입니다 | 시스템 점검중