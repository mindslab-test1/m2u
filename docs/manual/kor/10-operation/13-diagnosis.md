# 에러 상황 진단

## 각 에러별로 문제가 발생한 상황을 확인해보기

내부이름 | 에러코드 | 검토한 상황 및 로그
-----|-----|--
USER_INVLIDATED | 101 | m2u-frontd.log 확인<br/>인증 제공자로 부터 반환되는 결과물 확인
SERVICE_GROUP_NOT_FOUND | 102 | m2u-frontd.log 확인, 콘솔에 정의된 서비스그룹 확인, 요청한 서비스그룹과 등록된 서비스 그룹이 일치하는지 확인
SESSION_NOT_FOUND | 103 | (정상적인 에러)
EMPTY_IN | 201| (정상적인 에러)
DOMAIN_CLASSIFIER_FAILED |301 | m2u-frontd.log, brain-cld.log 확인<br/>도메인 분류가 정상적인지 확인합니다.
DIALOG_AGENT_NOT_FOUND | 302 | 콘솔 확인 등록된 대화 에이전트가 존재하는지 확인합니다.
LAST_DIALG_AGENT_LOST | 304 |  대화 에이전트의 정상 동작 여부를 확인합니다.
CHATBOT_NOT_FOUND | 305 | (현재 버전에서는 발생하지 않습니다.)
HUMAN_AGENT_NOT_FOUND | 306 | HAD 관련 동작 설정 및 대화 에이전트가 정상적으로 설정되어 있는지 확인합니다.
LAST_AGENT_NOT_FOUND | 312|  `journalctl`로 `minds-dam.service` 로그 확인<br> m2u-frontd.log 확인<br/>대화 에이전트 정상 동작 여부 확인
DUPLICATED_DOMAIN<br/>_RESOLVED | 313 | 대화 에이전트 및 SDS 기능 동작 확인
SECONDARY_DIALOG_AGENT<br/>_IS_MULTI_TURN | 314 | 멀티턴이 발생한 경우
INTERNAL_ERROR | 500 | `journalctl`로 `minds-dam.service` 로그 확인<br> m2u-frontd.log 확인<br/>대화 에이전트 정상 동작 여부 확인

## 각 로그파일별로 확인해 볼 수 있는 문제들

- m2u-frontd.log
  - 입력 대화
  - 출력 대화
  - 분류기 실행 결과
  - DA 호출 결과
- brain-cld.log
  - 도메인 분류기 실행 결과
