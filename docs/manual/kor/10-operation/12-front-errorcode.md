# 에러코드
M2U 플랫폼의 에러는 대화를 통해서 확인할 수 있습니다.
대화가 성공적으로 진행되지 않은 경우에 대화 메시지를 통해서 에러 메시지를 내보냅니다.
또한 이 대화 메시지에 대한 에러를 숫자로 만든 코드로 내보냅니다.

## 에러 상황에 대한 인지
`m2u-text` 도구로 대화를 테스트하면 에러 코드를 확인해볼 수 있습니다.

```
minds@10:~$ ./m2u-text
>>> 1: ask_mindslab, 마인즈랩에 대한 질의응답 봇입니다
	base {ask_mindslab_base, kor, 1, 마인즈랩 문의 Base}
	mindslab {mindslab, kor, 2, 회사 문의 챗봇}

>>> 2: minds_assistant, 마인즈랩 인공지능 비서 마인즈 봇입니다
	calculator {calculator, kor, 1, 사칙 연산}
	translation {translation, kor, 1, 단문 번역}
	base {minds_assistant_base, kor, 1, 마인즈랩 비서 Base}
	memo {memo, kor, 1, 메모}
	check_time {check_time, kor, 1, 세계시간 확인}
	check_date {check_date, kor, 1, 날짜 및 요일 조회}
	conversion_unit {conversion_unit, kor, 1, 단위 변환}
	check_weather {check_weather, kor, 1, 날씨 조회}

>>> 3: no_wakeup, no_wakeup
	news_summary {news_summary, kor, 1, 1}

Select Service Group [1-3|ql]: 2

Help:
q: Stop chatting
n: Restart session
e: Set language as English
k: Set language as Korean
any text: talk talk talk

{1049720} [qnke|User]: 오늘 서울 날씨 어때?
AI <<< : [/] : 현재 지원되지 않는 기능입니다. 앞으로 더 많은 기능이 추가될 예정이니 기대해주세요
	meta[in.lang] = {kor}
	meta[in.sessionid] = {1049720}
	meta[out.talk.error] = {302}

{1049720} [qnke|User]:
AI <<< : [/] : 안녕하세요. 대화를 입력해주세요.
	meta[in.lang] = {kor}
	meta[in.sessionid] = {1049720}
	meta[out.talk.error] = {201}
```

위의 경우 에러코드는 `302`입니다. 에러 상황에서도 항상 정상적인 대화는 가능해야 하기 때문에
M2U 플랫폼은 정상적으로 텍스트 메시지를 내보냅니다. 이 경우에 메시지는
`현재 지원되지 않는 기능입니다. 앞으로 더 많은 기능이 추가될 예정이니 기대해주세요` 입니다.
어떤 에러의 경우에는 메시지를 내보내지 않을 수도 있습니다.


## 상세한 에러코드

내부이름 | 에러코드 | 메시지한글 | 메시지영문 | 발생상황
----|----|----|----|----
OK     | 0     | (정상적인 응답) |  | 에러 아님
USER_INVLIDATED | 101 |  |  | 세션을 열 때 요청한 사용자가 정상적이지 않을 때 발생합니다.
SERVICE_GROUP_NOT_FOUND | 102 |  |  | 세션을 열때 요청한 서비스 그룹이 없을 때 발생합니다.
SESSION_NOT_FOUND | 103 | 이전 대화가 종료되었습니다. 새로운 대화를 시작해주세요 | The last conversation has been closed! Please start a new conversation | 대화를 진행할 때 요청한 세션이 종료되었을 경우에 발생합니다.
EMPTY_IN | 201| 안녕하세요. 대화를 입력해주세요. | Hello! Please send me a message | 대화 메시지에 빈 문자열이 입력되었을 경우
DOMAIN_CLASSIFIER_FAILED |301 | 현재 지원되지 않는 기능입니다. 앞으로 더 많은 기능이 추가될 예정이니 기대해주세요. | The service is currently not available. We will be adding more services. Please visit us again. | 입력 발화에 대한 도메인 분류를 정상적으로 수행할 수 없을 경우
DIALOG_AGENT_NOT_FOUND | 302 | (위와 같음) | (위와 같음) |  또는 분류된 도메인을 처리해줄 수 있는 대화 에이전트를 발견할 수 없을 때 발생합니다.
LAST_DIALG_AGENT_LOST | 304 |  |  | 멀터 턴의 경우 이전 대화 에이전트가 동작을 멈춘 상태에 발생합니다.
CHATBOT_NOT_FOUND | 305 | 챗봇이 응답하지 앖습니다. 잠시 후 다시 시도해주세요 | Chatbot is not responding. Please try later | DNN 봇(챗봇)으로 강제로 대화를 보내려는 데 현재 서비스그룹에 챗봇이 존재하지 않을 경우
HUMAN_AGENT_NOT_FOUND | 306 |  |  | 인간 상담사 연결 기능을 지정했는데, 이를 처리할 인간상담사 대화 에이전트가 존재하지 않은 경우
LAST_AGENT_NOT_FOUND | 312| 기존 대화를 발견할 수 없습니다. 관리자에게 문의하세요. | Oops, I can't track the last conversation. Please report this to the administrator | 기존 대화를 찾는데, 찾을 수 없을 경우에 발생합니다.
DUPLICATED_DOMAIN<br/>_RESOLVED | 313 | 죄송합니다. 이전 대화를 불러올 수 없습니다. 관리자에게 문의해주세요 | Sorry, i can't retrieve the last conversation. Please report this matter to the administrator | 대화 실패로 2차 대화를 시도하는 중에 같은 도메인이 다시 식별된 경우에 발생합니다.
SECONDARY_DIALOG_AGENT<br/>_IS_MULTI_TURN | 314 | 이전 대화가 아직 진행 중입니다. 이전 대화를 먼저 마쳐주세요 | The last conversation has not finished yet. Please finish the current conversation first. | 멀티턴 대화 진행 중에 응답할 수 없어서 새로운 도메인을 찾았는데 또 다른 멀티턴이 발생한 경우
INTERNAL_ERROR | 500 | 원하시는 답변을 찾지 못했어요. 다시 말씀해주시겠어요? | Sorry, I couldn't find the answer to your question. Could you ask me again? | **매우 심각** 대화 에이전트에서 비정상적인 예외를 내보낸 경우입니다. 원인은 매우 다양할 수 있으나 통상적으로 대화 에이전트가 예외처리가 안되어서 답변을 생성하지 못하고 예외를 던진 경우에 발생합니다.
