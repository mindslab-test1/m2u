# 운영하기

M2U 플랫폼을 이용하여 서비스를 운영하기 위해서는 다음 몇가지 지식들이 필요합니다.

운영 중에 문제점이 발생하면 적절한 진단을 하고 이에 대한 대응을 해야 합니다.

문제를 확인하기 위해서는 2가지 방법이 있습니다.

- `maum-text`를 통해서 대화를 실행하면서 에러 코드를 보기
- 로그를 통해서 각 서브시스템 단위로 오동작 여부 확인하기
<!---
- [분산 배치하기](operation/01-deploy.md)
- [분산 로그 집중 처리](operation/02-collecting-logs.md)
--->
- [로그보기](11-viewlog.md)
- [에러코드](12-front-errorcode.md)
- [에러상황진단](13-diagnosis.md)
- [장애원인별 대응 방안](14-countermeasures.md)
- [적정 자원 사용량](15-resource-guide.md)
