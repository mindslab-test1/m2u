## Intent Finder CustomScript 개발자용 매뉴얼

### 개발 환경
  1. echo_custom_script.py 파일을 만듭니다.
  2. 생성한 파일에 custom script 분류 기능을 구현합니다.
  3. ${MAUM_ROOT}/custom-script 위치로 옮깁니다.
   - import 되어야할 내용은 아래와 같습니다.
     ```python
     # -*- coding: utf-8 -*-
     import sys
     
     reload(sys)
     sys.setdefaultencoding('utf-8')
     from concurrent import futures
     import grpc
     import os
     from google.protobuf import empty_pb2
     from google.protobuf import struct_pb2 as struct
     from maum.m2u.router.v3 import intentfinder_pb2
     from maum.m2u.common import dialog_pb2
     from google.protobuf import json_format
     ```
   - 분류 기능 수행 할 `Classify`라는 이름의 함수를 구현합니다. 
     - parameter : string 형태의 `CustomClassifyFuncParam` 메세지
     - return : 분류 처리 후 스킬 결과
       ```proto
       message CustomClassifyFuncParam {
         // 필수, 사용자 발화 및 발화와 관련된 주변 정보들을 포함합니다.
         maum.m2u.common.Utter utter = 1;
         // 필수 필드, 사용자, 디바이스, 위치 정보
         maum.m2u.common.SystemContext system_context = 2;
         // 필수, 세션 정보
         maum.m2u.common.Session session = 3;
         // 호출하는 챗봇의 이름
         string chatbot = 11;
         // 두번째 이후 호출할 때 제외할 SKILL의 이름들을 정의
         repeated string exclude_skills = 21;
         // NLP Result
         maum.brain.nlp.Document :Q!
         document = 31;
       }
       ```
       ```proto
       message Utter {
          // 입력의 유형
          enum InputType {
            // STT를 통한 입력
            SPEECH = 0;
            // 타이핑에 의한 입력
            KEYBOARD = 1;
            // CARD 등에 지정된 텍스트를 보내는 경우
            TOUTCH = 2;
            // 이미지 인식의 결과를 보내는 경우, 이미지 애노테이션을 통한 처리
            IMAGE = 3;
            // OCR 이미지 인식의 결과를 보내는 경우
            IMAGE_DOCUMENT = 4;
            // 이미지 인식의 결과를 보내는 경우
            VIDEO = 5;
            // 세션을 생성하는 등의 최초 이벤트, 프로그램 등의 명시적인 이벤트로 처리하는 방식
            OPEN_EVENT = 100;
          }
          // 입력 유형
          InputType input_type = 2;
       
          // 언어
          maum.common.Lang lang = 3;
       
          // 현재의 발화에 대한 추가적인 메시지가 존재할 수 있습니다.
          // ETRI STT의 경우에는 이 메시지가 없습니다.
          repeated string alt_utters = 4;
       
          // 추가적인 입력 데이터
          // IMAGE, IMAGE_DOCUMENT. VIDEO, EVENT의 경우
          // 추가적인 데이터 메시지 데이터를 정의해서 보낼 수 있습니다.
          // 이미지 문서 인식의 경우에는 JSON 형태의 meta 데이터를
          // 여기에 담아서 넣어야 합니다.
          google.protobuf.Struct meta = 101;
       } 
       ```
       * 파라미터에 담긴 utter 정보를 기준으로 분류기능을 구현하고 스킬결과를 반환합니다.
       
       ###### 분류 기능 구현 예시입니다.
       ```python
       def Classify(request):
           param = intentfinder_pb2.CustomClassifyFuncParam()
           # ParseFromString 함수는 직렬화 된문자열 형태로 넘겨받은 request를 
           # CustomClassifyFuncParam message 형태에 맞게 파싱합니다.
           param.ParseFromString(request)
           result = ''
           custom_list = ['커스텀 알려줘', '커스텀 알려주세요']
           if param.utter.input_type == dialog_pb2.Utter.IMAGE:
               return 'input_type is image'
           else:
               if param.utter.utter in custom_list:
                   result = 'custom'
               else:
                   result = ''    
           return result
       ```
       
       ###### main에서 직접 CustomClassifyFuncParam에 값을 넣어 결과를 테스트하는 예시입니다.
       ```python
       if __name__ == '__main__':
           req = intentfinder_pb2.CustomClassifyFuncParam()
           req.utter.utter = '커스텀 알려줘.'
           req.utter.input_type = dialog_pb2.Utter.IMAGE
           req.chatbot = 'chat1'
           req.session.id = 1234
           req.system_context.user.name = 'mlab'
           
           # SerializeToString 함수는 CustomClassifyFuncParam 메세지를 문자열로 직렬화합니다. 
           input = req.SerializeToString()           
           res = Classify(input)
           print res
       ```
  ※ 추후에 참조하는 파일과 함수명은 `m2u-default-setting.json`를 통해 변경 할 수 있습니다.

### 빌드 환경
  - `${MAUM_ROOT}/custom-script` 위치의 `echo_custom_script.py` 파일을
     intent finder 의 custom script 분류 시 import 하고  `Classify` 함수를 호출합니다.     


### 테스트 환경
  - intent finder policy의 step 정보에 custom script가 적용되어있음을 전제로 테스트를 진행합니다.
    // ITF instance 실행
    ```commandline
    ${MAUM_ROOT}/bin/m2u-itfd -n <intent finder instance명> -i <서버주소:포트>
    ```
    ```commandline
    ${MAUM_ROOT}/bin/m2u-map-cli --chatbot <챗봇명>
    // 테스할 메뉴를 선택하여 발화를 입력합니다.
    ```
  - 분류과정과 결과를 로그를 확인할 수 있습니다.
    ```commandline
      cd ${MAUM_ROOT}/logs/m2u-itfd-<instance명>.log
    ```
  
  ###### 로그 결과를 아래와 같이 확인 할 수 있습니다.
   - 아래 로그를 통해 `${MAUM_ROOT}/custom-script` 위치의 `echo_custom_script` 코드 내 
      `Classify` 함수가 호출되는 것을 확인 할 수 있습니다.
   ```commandline
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Initialize CustomScriptManager
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: custom script path=sys.path.append("${MAUM_ROOT}/custom-script")
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetPrefix: /usr
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetExecPrefix: /usr
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetProgramFullPath: /usr/bin/python
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetPath: /usr/lib/python2.7/:/usr/lib/python2.7/plat-x86_64-linux-gnu:/usr/lib/python2.7/lib-tk:/usr/lib/python2.7/lib-old:/usr/lib/python2.7/lib-dynload
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetVersion: 2.7.12 (default, Dec  4 2017, 14:50:18) 
   [GCC 5.4.0 20160609]
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetPlatform: linux2
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetCopyright: Copyright (c) 2001-2016 Python Software Foundation.
   All Rights Reserved.

   Copyright (c) 2000 BeOpen.com.
   All Rights Reserved.

   Copyright (c) 1995-2001 Corporation for National Research Initiatives.
   All Rights Reserved.

   Copyright (c) 1991-1995 Stichting Mathematisch Centrum, Amsterdam.
   All Rights Reserved.
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetCompiler: 
   [GCC 5.4.0 20160609]
   [04-26 10:46:19.616] m2u-itfd[3630 5451] D: Py_GetBuildInfo: default, Dec  4 2017, 14:50:18
   [04-26 10:46:19.617] m2u-itfd[3630 5451] D: Initialize CustomScriptManager...end
   [04-26 10:46:19.617] m2u-itfd[3630 5451] D: CallPythonFunc filename=echo_custom_script, classify_func=Classify
   [04-26 10:46:19.617] m2u-itfd[3630 5451] W: Result of call: custom

   [04-26 10:46:19.617] m2u-itfd[3630 5451] D:  result=true, RunResult

   ```
