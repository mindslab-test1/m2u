# Intent Finder 실행 매뉴얼

## 1. Intent Finder 서비스
Intent Finder는 사용자 발화를 입력받아 SimpleClassifier, HMD, DNN, Custom Script, Final 5가지 분류방식으로 의도를 파악할 수 있습니다.
  - `intent finder instance`는 하나의 `intent finder policy`를 참조합니다.
  - `intent finder policy`는 여러 `step`으로 구성됩니다.
  - `step`은 `name, description, runStyle, categories, type` 정보를 기준으로 각 분류모델을 참조하여 의도파악을 수행합니다.
<br><br>

## 2. Intent Finder 테스트를 위한 모델 준비
### 분류기 모델 파일 로드
 - PCRE 모델은 Amin Webpage에서 Simple Classifier 등록한 정보를 기준으로 `hazelcast`에서 읽어옵니다.
 - DNN 모델은 `${MAUM_ROOT}/trained/classifier` 디렉토리 안에 위치시킵니다.
 - HMD 모델은 `${MAUM_ROOT}/trained/hmd` 디렉토리 안에 위치시킵니다.
 - custom script의 python 코드는 ${MAUM_ROOT}/custom-script에 `.py` 파일로 위치시킵니다.
 ※ 해당하는 `intent finder policy` 정보와 일치하는 모델파일이 존재하지않으면  `intent finder instance`는 실행되지 않습니다.

### json 파일로 의도파악 policy 설정
샘플 파일은  ${MAUM_ROOT}/samples/test/m2u-default-setting.json 입니다.
 json 파일을 수정하여 적용하려면 `intent finder policy`의 각 `step` 별 `name, description, runStyle, categories, type`을 설정합니다.
<br><br>

## 3. Intent Finder Policy Step별 분류 타입
#### CUSTOM SCRIPT
 - type은 `CLM_CLASSIFY_SCRIPT` 입니다.
 - `customScriptMod` 영역에
   - filename: 참조할 확장자를 제외한 custom script 파일명
   - classifyFunc : 호출할 함수명

#### PCRE
 - type은 `CLM_PCRE` 입니다.
 - `pcreMod` 영역에
   - model: 참조할 simple classifier 모델명
 - `intent-finder-policy` 영역 밖에 `simple-classifier` 영역에
   - simple classifier 모델명으로 지정했던 부분을 참조합니다.
   - skillList 안에 regex는 정규표현식을, name은 해당하는 카테고리명

#### HMD
 - type은 `CLM_HMD` 입니다.
 - `hmdMod` 영역에
   - model: 참조할 HMD 모델명
            모델명.hmdmatrix 파일을 참조합니다. 

#### DNN
 - type은 `CLM_DNN_CLASSIFIER` 입니다.
 - `dnnClMod` 영역에
   - model: 참조할 DNN 모델명

#### FINAL
 - type은 `CLM_FINAL_RULE` 입니다.
 - `finalRule` 영역에
   - finalSkill: 파이널 분류값

## 4. hazelcast에 json 파일을 import
  Intent Finder Policy 정보를 포함한 json 파일을 hazelcast에 import 합니다.
  ```
  java -cp ${MAUM_ROOT}/conf:${MAUM_ROOT}/lib/m2u-hzc-repack-0.2-all.jar
       ai.maum.m2u.hazelcast.HazelcastSettingsCarrier import <json 파일 경로> 
  ```
<br><br>

## 5. Intent Finder 실행 테스트
###### Console 환경에서 test하는 방법
   1. Intent Finder 서버 실행
       `${MAUM_ROOT}/bin/m2u-itfd -n <intent finder instance명> -i <서버주소:포트>` 
   2. Intent Finder Client 실행
  >1) `${MAUM_ROOT}/bin/m2u-map-cli --chatbot <챗봇명>` 실행
  >2) 'd'을(Dialog) 입력합니다
  >3) '3'을(TextToTextTalk) 입력합니다
  >4) 분류를 수행할 발화를 입력합니다. ex) 길동이에게 오만원 보내줘.

   3. Intent Finder 분류 결과 기록
        `${MAUM_ROOT}/logs/m2u-itfd-<intent finder instance명>.log` 파일에 분류 수행 과정이 기록되어 확인 할 수 있습니다.
<br><br>


## \[참고\] RUN STYLE TYPE
* IFC_UNKOWN 
  - 의도파악단계 건너뜁니다.
* IFC_SKILL_FOUND_RETURN
  - Response 결과에 SKILL이 존재하지 않을때 의도파악 분류를 수행하여 분류값을 찾으면 Response에 SKILL저장하고 의도파악단계 종료합니다.
    <br>분류값을 찾지못하면 다음 step 수행합니다.
* IFC_SKILL_FOUND_COLLECT_HINTS
  - Response 결과에 SKILL이 존재하지 않을때 의도파악 분류를 수행하여 분류값을 찾으면 Response에 SKILL저장하고 이후 step부터는 run_style이 IFC_HINT 일 경우만 분류 수행합니다.
    <br>나머지 run_style은 의도파악 단계 SKIP합니다. 
    <br>분류값을 찾지못하면 다음 step 수행합니다.
* IFC_HINT 
  - 의도파악 분류를 수행하여 분류값을 찾으면 Response에 Hint에 step 이름과 skill 결과 map 형태로 저장합니다.
    <br>분류값을 찾지못하면 다음 step 수행합니다.

![ ITF run_style 관련 flow chart ](https://user-images.githubusercontent.com/33645517/38599110-fd872cec-3d99-11e8-8e39-f0765ff90e41.png "run_style flowchart")

