# M2U 플랫폼

[M2U](https://maum.ai/)는
인공지능 기반 대화 서비스를 제공하는 플랫폼입니다.
M2U는 대화형 정보 유통을 선도하는 새로운 UX와 패러다임을 제공합니다.

M2U는
* 다양한 방법으로 대화 에이전트들 만들어서 제공할 수 있는
개발 프레임워크를 제공합니다.
  * DB, REST 기반의 API를 비롯해서 IT 기술로 만들어진 어떤 방식으로든지
    필요한 데이터를 처리할 수 있습니다.
* 학습 기반의 음성 인식을 내장하고 있어서 사람의 음성 언어를 이해합니다.
* 학습 기반의 NLP와 분류기를 내장하고 있어서 사람의 언어를 이해합니다.
* 대화 관리 기능을 내장하고 있어서 대화의 맥락을 이해하면서
  사람과 대화를 이끌어 갑니다.
* 챗봇 엔진을 내장하고 있어서 사람과 대화를 이끌어 갑니다.
* 어려운 질문을 이해하고 대답할 수 있는 QA 시스템을 포함하고 있습니다.
* 웹, 모바일 앱, IoT, 전화 등 다양한 장치를 통해서 접근할 수 있습니다.
* 다양한 장치에 따른 유연한 출력 형식을 제공하여 기존의 앱 및 웹 사용자와
  인터페이스할 수 있는 대화형 앱의 방향을 제시합니다.

M2U는 AWS, Azure와 같은 클라우드 형태로 서비스를 제공할 수 있으며,
동시에 기업 환경에 온프레미스(On-premise) 환경에서도 잘 동작합니다.
M2U는 마이크로서비스 아키텍처를 기반으로 설계되었고 최적의 운영환경을 제공합니다.


<!--
자주 찾는 문서를 위한 바로가기

| M2U | 대화 에이전트 개발 | 학습기반의 STT, TA |
:----- | :----- | :----- |
| [빠른 시작](TODO) | [Dialog Agent Provider](da/basic/da-api-n-grpc.md) | [DNN 분류기](classifier/dnn-classifier.md) |
| [설정](TODO) | [SDS-시나리오 대화 만들기](TODO) |  |
| [콘솔 사용하기](TODO) | [지식기반 시스템 만들기](TODO) | [STT 설정](TODO) |
-->

## [M2U 시작하기](01-basics/01-README.md)
M2U는 대화를 처리하기 위해서 몇가지 핵심적인 개념을 가지고 있습니다.
- [M2U 기본 개념들](01-basics/02-glossory.md): 챗봇, 대화 에이전트, 대화 에이전트 인스턴스,
  대화 세션에 대해서 알아봅니다.
- [대화의 분류](01-basics/03-intent-finder.md): 사용자의 대화 의도를 이해하고 적절하게 분류하는
  방법에 대해서 알아봅니다.
- 대화 장치들<!--(01-basics/11-devices.md)-->: M2U에 연결하여 대화를 처리할 수 있는 다양한 장치들에 대해서
  알아봅니다.
  - 대화 API<!--(01-basics/12-dialog-service-api.md)-->: 다양한 장치들이 대화에 연결하는 기본 인터페이스
  에 대해서 알아봅니다.
- 대화 만들기<!--(01-basics/21-how-to-build-dialog.md)-->: M2U 플랫폼에서 기본적으로 대화를 만드는 방법에
  대해서 알아봅니다.
- 아키텍처<!--(01-basics/22-architecture.md)-->: M2U의 전체 아키텍처에 대해서 알아봅니다.
- grpc에 기반한 아키텍처 구성<!--(01-basics/23-grpc-n-microservice.md)-->: [grpc](https://grpc.io)
  와 마이크로 서비스에 대한 일반적인 개념에 대해서 알아봅니다.

<!--
디렉토리 구조 정리
da: da 개발과 관련된 사항들, DA API
dam: da관리, dam, svcgroup
console: 콘솔 사용하기
install: 설치하기
service: 각 서비스에 대한 설명, systemd 관련 사항들
front: front 서버에 대한 설명, front와 관련된 설명들
-->

## 대화 에이전트 개발의 기초

### 대화 에이전트 개발하기 <!--(02-da/README.md)-->
<!-- - [Hello World](da/basic/hello-world.md)-->
<!-- - [대화 만들기와 grpc 이해하기](da/basic/da-api-n-grpc.md)-->
- [`darun`을 이용한 대화 테스트하기](02-da/10-basic/03-test-with-darun.md)
<!-- - [SDS를 이용한 대화 만들기](da/basic/da-using-sds.md)-->
<!-- - [대화에서 데이터베이스 사용하기](da/basic/da-using-db.md)-->
<!-- - [대화에서 REST API 사용하기](da/basic/da-using-rest.md)-->
<!-- - [대화에서 챗봇 사용하기](da/basic/da-using-rest.md)-->
<!-- - [대화에서 FAQ 만들기](da/basic/da-making-faq.md)-->
<!-- - [대화 에이전트 관리자 vs `darun`](da/basic/compare-darun-dam.md)-->

<!--
### [대화 분류하기](classifier/README.md)
- [PCRE를 이용한 간단한 분류기](classifier/01-pcre.md)
- [DNN 분류기](classifier/02-dnn-classifier.md)
- [HMD 분류기](classifier/03-hmd-classifier.md)
- [커스텀 스크립트](classifier/04-custom-script.md)
- [파이널 스킬](classifier/05-final-skill.md)
 
### 대화에이전트와 챗봇
- [대화에이전트](dam/da.md)
- [대화에이전트 관리자란](dam/dam.md)
- [챗봇 개념](dam/chatbot.md)
- [대화 에이전트 인스턴스 개념](dam/dialog-agent-instance.md)
- [대화에이전트가 대화에이전트 실행하기](dam/run-da-in-dam.md)
- [대화에이전트의 등록](dam/register-da.md)

### 콘솔을 사용하여 대화 에이전트를 서비스로 만들기
- [대화에이전트 관리자 등록하기](console/dialog-service/register-dam.md)
- [대화에이전트 추가하기](console/dialog-service/add-da.md)
- [챗봇 만들기](console/dialog-service/create-chatbot.md)
- [챗봇내에 대화 에이전트 인스턴스 추가히기](console/dialog-service/add-dai-to-chatbot.md)
-->

## 고급 대화 에이전트 개발

<!--
### 인증 제공자
M2U 플랫폼은 자체적으로 사용자를 처리하지 않습니다.
대신 사용자에 대한 인증 정보를 외부에서 처리할 수 있는 구조를 갖고 있습니다.
인증 제공자는 인증과 사용자에 대한 처리를 대신하는 역할을 수행합니다.

- [인증 제공자에 대한 개념](extension/user/auth-provider.md)
- [인증 제공자 만들기](extension/user/make-auth-provider.md)
- [사용자 기본 정보의 제공](extension/user/basic-user-info.md)
- [챗봇 단위 사용자 정보 제공](extension/user/extended-user-info.md)
- [사용자 제공 정보의 입력 체계](extension/user/ux-extended-user-info.md)

### 대화 사용자를 이해하는 대화 만들기
- [DA에서 인증 정보를 요구하기](da/advanced/request-userinfo.md)
- [DA에서 인증 정보 활용하기](da/advanced/using-userinfo.md)

## 관리자 콘솔
- [콘솔의 개요 및 주요 기능](console/README.md)
- [콘솔 UI 인터페이스 구성](console/console-ui-basic.md)

### 콘솔에 로그인하기
- [콘솔 계정의 체계](console/account.md)
- [로그인](console/console-login.md)
- [권한 및 팀, 그룹](console/authorization.md)

### 웹으로 대화하기
- [웹 챗봇 선택하기](console/web-dialog-client/select-chatbot.md)
- [다양한 대화](console/web-dialog-client/dialog.md)
- [대화 디버그](console/web-dialog-client/debug-dialog.md)

### 대시보드
- [대시보드](console/dashboard/dashboard.md)
- [대시보드 꾸미기](console/dashboard/edit-dashboard.md)

### 대화 로그 보기
- [실시간 대화 세션 보기](console/session/sessions.md)
- [실시간 대화 내용 보기](console/session/view-dialog.md)
- [과거 대화 검색](console/dilaog-log/search-sessions.md)
- [과거 대화 내용 보기](console/dilaog-log/view-dialog.md)

### 통계 보기
- [실시간 대화 세션 보기](console/session/sessions.md)
- [실시간 대화 내용 보기](console/session/view-dialog.md)
- [과거 대화 검색](console/dilaog-log/search-sessions.md)
- [과거 대화 내용 보기](console/dilaog-log/view-dialog.md)

### 모니터링 하기
- [모니터링 대상 추가](console/monitor/add-objects.md)
- [서비스 모니터링](console/monitor/monitor-service.md)
- [자원 모니터링](console/monitor/monitor-resources.md)
- [실행 로그 보기](console/monitor/monitor-logging.md)
-->


## 관리자용 문서들

### [설치하기](07-install/00-index.md)
- [설치하기 전에 필요한 환경](07-install/01-prerequisite.md)
- [하젤캐스트 설치하기](07-install/11-database-hazelcast.md)
- [설치하기](07-install/21-install-files.md)
- [설정하기](07-install/22-setup.md)
- [설정변수](07-install/23-m2u-config-variables.md)
- [설치 디렉토리의 구성](07-install/41-directory-strucure.md)
- [설정파일의 구조](07-install/42-config-file-format.md)
<!--
- [기본 구성하기](install/31-basic-deployment.md))
- [HA 구성하기](install/32-ha-deployment.md)
- [설정파일들 목록](install/33-config-files.md)

### 빌드하기
- [우분투 M2U 빌드](build/01-ubuntu-build.md)
- [센트오에스 도커컨테이너에서 M2U 빌드](build/02-docker-centos-build.md)

### Admin 사용
- [챗봇 만들기](da/basic/make-chatbot.md)
-->

### [서비스](08-service/00-index.md)
- [서비스 개요:전체 서비스들 및 포트들](08-service/01-overview.md)
- [서비스 사용하기](08-service/02-how-to-run-service.md)
- M2U 서비스들
  - [M2U 관리 레스트](08-service/11-m2u-console.md)
  - [M2U 관리 프록시](08-service/11-m2u-console.md)
  - [M2U 대화 에이전트 관리자](08-service/12-m2u-svc-dam.md)
  - [M2U 대화 에이전트 인스턴스 관리자](08-service/13-m2u-svc-daisvc.md)
  - [M2U 프론트](08-service/14-m2u-svc-front.md)
  - [M2U 하젤케스트 서버](08-service/15-m2u-svc-hzc.md)
  - [M2U ITF 관리자](08-service/16-m2u-svc-itfsvc.md)
  - [M2U 로거](08-service/17-m2u-svc-logger.md)
  - [M2U 맵](08-service/18-m2u-svc-map.md)
  - [M2U 풀](08-service/19-m2u-svc-pool.md)
  - [M2U 레스트](08-service/20-m2u-svc-rest.md)
  - [M2U 라우터](08-service/21-m2u-svc-router.md)
  - [M2U 서비스 관리자](08-service/22-m2u-svc-svcadm.md)
- [M2U 서비스의 설정](08-service/23-m2u-config.md)
- 음성인식 서비스들
  - [음성인식 서비스](08-service/31-brain-stt-svc-stt.md)
  - [음성인식 서비스의 설정](08-service/32-brain-stt-config.md)
  - [음성인식 서비스 리소스들](08-service/33-brain-stt-resources.md)
- [브레인 TA](08-service/40-brain-ta.md)
  - [텍스트 분류 서비스](08-service/41-brain-ta-classifier.md)
  - [NLP 서비스](08-service/42-brain-ta-nlp.md)
  - [텍스트분석 서비스의 설정](08-service/44-brain-ta-config.md)
  - [텍스트분석 서비스의 리소스들](08-service/45-brain-ta-resources.md)
- [브레인 SDS](08-service/51-brain-sds.md)
<!-- - [NLP Wise QA](service/33-ml-ta-wise-nlp.md)-->

### Intent Finder
- [실행 매뉴얼](09-intent-finder/00-intent-finder-test.md)
- [커스텀 스크립트](09-intent-finder/01-customscript-dev-manual.md)

### [운영하기](10-operation/00-index.md)
<!-- - [분산 배치하기](10-operation/01-deploy.md)-->
<!-- - [분산 로그 집중 처리]-->
- [로그보기](10-operation/11-viewlog.md)
- [에러코드](10-operation/12-front-errorcode.md)
- [에러상황진단](10-operation/13-diagnosis.md)
- [장애원인별 대응 방안](10-operation/14-countermeasures.md)
- [적정 자원 사용량](10-operation/15-resource-guide.md)
