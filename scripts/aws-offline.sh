#!/bin/bash

export MAUM_ROOT=${PWD}

# only amazon-linux
if [ ! -f ~/python-packages.tar.gz ] || [ -f additional-pkg.tar.gz ]; then
  echo "There is no initial packages."
  echo "Please set python-packages.tar.gz & additional-pkg.tar.gz on home directory."
  exit 0
fi

echo "### STEP 0. install essential packages"
sudo yum -y groupinstall 'Development Tools'
sudo yum -y install \
  gcc gcc-c++ \
  glibc-devel.x86_64 \
  java-1.8.0-openjdk-devel.x86_64 \
  python27-devel.x86_64 \
  flex-devel.x86_64 \
  libcurl-devel.x86_64 \
  sqlite-devel.x86_64 \
  openssl-devel.x86_64 \
  libarchive-devel.x86_64 \
  atlas-devel.x86_64 \
  lapack.x86_64 \
  httpd nginx \
  policycoreutils-python \
  mysql-server \
  libuuid-devel \
  zeromq \
  bzip2

echo "### STEP 1. install additional packages"
(cd ~ && tar zxvf ~/additional-pkg.tar.gz && cd ~/additional-pkg && sudo rpm -Uvh --force *.rpm)

echo "### STEP 2. install python packages"
(cd ~ && tar zxvf ~/python-packages.tar.gz && cd ~/python-packages && ./install.sh)

cd $MAUM_ROOT

echo "### STEP 3. check folder exits"
if [ ! -d "$MAUM_ROOT/logs/supervisor" ]; then
  mkdir -p $MAUM_ROOT/logs/supervisor
fi
