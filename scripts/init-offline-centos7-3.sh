#!/bin/bash

export MAUM_ROOT=${PWD}

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS detected. Set CentOS as default."
  OS=centos
fi

# only centos or redhat
if [ ! -f ~/local-rpm.tar.gz ] && [ ! -f ~/local-dependency.tar.gz ] &&[ ! -f ~/nginx.tar.gz ] && [ ! -f ~/python-packages.tar.gz ]; then
  echo "There is no initial packages."
  echo "Please set local-rpm.tar.gz & local-dependency.tar.gz & nginx.tar.gz & python-packages.tar.gz on home directory."
  exit 0
fi

echo "### ### ### closed network install started. ### ### ###"
echo "### STEP 0. set base yum repository"
(cd ~ && tar zxvf ~/local-rpm.tar.gz && cd ~/local-rpm && ./repo.sh)
echo "### STEP 1. set local yum repository"
(cd ~ && tar zxvf ~/local-dependency.tar.gz && cd ~/local-dependency && ./repo.sh)
(cd ~ && tar zxvf ~/nginx.tar.gz && cd ~/nginx && ./repo.sh)
echo "### STEP 2. install python packages"
(cd ~ && tar zxvf ~/python-packages.tar.gz && cd ~/python-packages && ./install.sh)
echo "### STEP 3. install prerequisite packages"
cd ~/local-dependency

sudo yum install -y \
  java-1.8.0-openjdk-devel.x86_64 \
  libcurl-devel.x86_64 \
  sqlite-devel.x86_64 \
  libarchive-devel.x86_64 \
  atlas.x86_64 \
  httpd nginx \
  nodejs-8.11.3-1nodesource.x86_64.rpm \
  gcc \
  mysql* \
  flex flex-devel-2.5.37-3.el7.x86_64.rpm \
  libarchive-3.1.2-10.el7_2.x86_64.rpm \
  libarchive-devel-3.1.2-10.el7_2.x86_64.rpm \
  libuuid-devel

sudo rpm -Uvh \
         libuv-1.10.2-1.el7.x86_64.rpm \
         bzip2-1.0.6-13.el7.x86_64.rpm \
         gflags-2.1.1-6.el7.x86_64.rpm \
         libatomic-4.8.5-28.el7.x86_64.rpm

# zeromq
sudo rpm -Uvh \
         libsodium13-1.0.5-1.el7.x86_64.rpm \
         openpgm-5.2.122-2.el7.x86_64.rpm \
         zeromq-4.1.4-5.el7.x86_64.rpm

sudo yum install -y \
  yajl-2.0.4-4.el7.x86_64.rpm

# policycoreutils-python
sudo yum install -y \
  libsemanage-2.5-5.1.el7_3.x86_64.rpm \
  libsemanage-python-2.5-5.1.el7_3.x86_64.rpm

sudo yum install -y \
  policycoreutils-2.5-11.el7_3.x86_64.rpm \
  policycoreutils-python-2.5-11.el7_3.x86_64.rpm

# docker
#sudo rpm -ivh \
#  oci-systemd-hook-0.1.7-2.git2788078.el7.x86_64.rpm \
#  docker-common-1.12.6-16.el7.centos.x86_64.rpm \
#  container-selinux-2.12-2.gite7096ce.el7.noarch.rpm \
#  skopeo-containers-0.1.19-1.el7.x86_64.rpm \
#  docker-client-1.12.6-16.el7.centos.x86_64.rpm \
#  oci-register-machine-0-3.11.gitdd0daef.el7.x86_64.rpm \
#  libseccomp-2.3.1-2.el7.x86_64.rpm \
#  docker-1.12.6-16.el7.centos.x86_64.rpm
#sudo yum install -y docker*

# lapack
sudo yum install -y lapack.x86_64

# openssl-devel
sudo rpm -ivh \
  zlib-devel-1.2.7-17.el7.x86_64.rpm \
  keyutils-libs-devel-1.5.8-3.el7.x86_64.rpm \
  libcom_err-devel-1.42.9-9.el7.x86_64.rpm \
  libverto-devel-0.2.5-4.el7.x86_64.rpm \
  libsepol-devel-2.5-6.el7.x86_64.rpm \
  pcre-devel-8.32-15.el7_2.1.x86_64.rpm \
  libselinux-devel-2.5-6.el7.x86_64.rpm
sudo yum install -y openssl-devel.x86_64

cd -

#echo "### STEP 4. Load docker images"
# docker config
#sudo groupadd docker
#sudo gpasswd -a ${USER} docker
#sudo service docker restart
#echo "Need relogin to use docker without sudo permission"

if [ ! -d "$MAUM_ROOT/logs/supervisor" ]; then
  mkdir -p $MAUM_ROOT/logs/supervisor
fi

