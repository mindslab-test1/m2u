#! /bin/bash

if [ ! -d ${MAUM_ROOT} ]; then
  echo ${MAUM_ROOT} is not directory.
  echo Use valid directory.
  exit 1
fi

if ps -ef | grep svd | grep -q python; then
  echo Please shutdown supervisor service
  echo "Please run '"${MAUM_ROOT}"/bin/svctl shutdown'"
  exit 1
fi

GROUP_NAME=`/usr/bin/id -ng`

read -p "Do you want to backup? [Y] " yn
yn=${yn:-Y}
if [ "$yn" = "Y" ] || [ "$yn" = "y" ]; then
  echo "Backup current major folders(apps, bin, include, lib, etc)."
  tar czf m2u-backup-$(date +"%Y%m%d%H%M%S").tgz \
      ${MAUM_ROOT}/apps ${MAUM_ROOT}/bin ${MAUM_ROOT}/include \
      ${MAUM_ROOT}/lib ${MAUM_ROOT}/etc
fi

echo "First delete the previous directories(apps, bin, include, lib)."
(cd ${MAUM_ROOT} && sudo rm -rf apps bin include lib)

echo "Second copying the m2u output files to "${MAUM_ROOT}" and changing owner."
sudo cp -r ./* ${MAUM_ROOT}
sudo chown -R ${USER}:${GROUP_NAME} ${MAUM_ROOT}

echo "Please run '"${MAUM_ROOT}"/bin/setup config'"
