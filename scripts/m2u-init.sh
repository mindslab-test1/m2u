#!/bin/bash

export MAUM_ROOT=${PWD}

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS detected. Set CentOS as default."
  OS=centos
fi

# install essential package
if [ "${OS}" = "centos" ]; then
  if [ ! -f /etc/yum.repos.d/epel.repo ]; then
    wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    sudo rpm -ivh epel-release-latest-7.noarch.rpm
    rm epel-release-latest-7.noarch.rpm
  fi
  sudo yum -y install epel-release
  sudo yum -y groupinstall 'Development Tools'
  sudo yum -y install \
    gcc gcc-c++ \
    glibc-devel.x86_64 \
    java-1.8.0-openjdk-devel.x86_64 \
    python-devel.x86_64 \
    flex-devel.x86_64 \
    libcurl-devel.x86_64 \
    sqlite-devel.x86_64 \
    openssl-devel.x86_64 \
    libarchive-devel.x86_64 \
    atlas-devel.x86_64 \
    lapack.x86_64 \
    libuv.x86_64 \
    libatomic-4.8.5-28.el7.x86_64 \
    httpd nginx \
    policycoreutils-python \
    mysql-server \
    libuuid-devel \
    zeromq \
    bzip2 \
    libatomic
    #docker

  # install pip & package
  curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
  sudo python get-pip.py
  rm get-pip.py
  # npm
  curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
  sudo yum install -y nodejs

  # We don't have 'db' directory before executing m2u process
  # sudo chcon -Rt svirt_sandbox_file_t ${MAUM_ROOT}/db
else
  sudo apt-get update
  sudo apt-get install -y \
    gcc-4.8 g++-4.8 g++ \
    python-pip python-dev \
    openmpi-common \
    libboost-all-dev \
    libcurl4-openssl-dev \
    libsqlite3-dev \
    libmysqlclient-dev \
    mysql-server-5.7 \
    libuv-dev libssl-dev \
    libarchive13 libarchive-dev \
    libatlas-base-dev libatlas-dev \
    libasio-dev \
    unzip \
    nginx \
    ffmpeg \
    flex \
    uuid-dev \
    libbz2-1.0 \
    libzmq3-dev
    #docker.io
  sudo ldconfig

  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

sudo npm install -g n
sudo n 8.11.3
npm install -g @angular/cli@^7.3.6
sudo pip install --upgrade pip
sudo pip install --upgrade virtualenv
sudo pip install --upgrade setuptools
sudo pip install boto3 grpcio==1.9.1 requests numpy theano pymysql protobuf==3.5.2
sudo pip install gensim==2.2.0

# docker config
#sudo groupadd docker
#sudo gpasswd -a ${USER} docker
#sudo service docker restart
#echo "Need relogin to use docker without sudo permission"

# check folder exits
if [ ! -d "$MAUM_ROOT/logs/supervisor" ]; then
  mkdir -p $MAUM_ROOT/logs/supervisor
fi
