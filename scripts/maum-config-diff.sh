#!/bin/bash

# Check whether ${MAUM_ROOT} is directory or not
if [ ! -d ${MAUM_ROOT} ]; then
  echo ${MAUM_ROOT} is not directory.
  echo Use valid directory.
  exit 1
fi

# Define Variables and make *.in files list
VERSION_NAME=`${MAUM_ROOT}/bin/m2u-routed -v | sed 's/m2u-routed version //'`
IN_FILES=`bin/setup list 2> /dev/null`

# Make backup directories
mkdir -p ${MAUM_ROOT}/${VERSION_NAME}.backups

## remove prev check file
test -f ${VERSION_NAME}.diff && rm -f ${VERSION_NAME}.diff

for in_file in ${IN_FILES}
do
    cp ${MAUM_ROOT}/${in_file} ${MAUM_ROOT}/${VERSION_NAME}.backups/
    diff -u ./${in_file} $MAUM_ROOT/${in_file} >> ${VERSION_NAME}.diff
done

if [ -s ${VERSION_NAME}.diff ]; then
    echo "Please check the "${VERSION_NAME}".diff file and run './m2u-reinstall.sh'"
else
    echo "No Changes at in_files!! Please run './m2u-reinstall.sh'"
fi

ls -al ${VERSION_NAME}.diff
