'use strict';

/**
 ACL에서 사용할 dynmaic role을 정의합니다. static role은 사용하지 않습니다.

 - Built-in dynamic roles
 Role.OWNER              $owner            Owner of the object
 Role.AUTHENTICATED      $authenticated    authenticated user
 Role.UNAUTHENTICATED    $unauthenticated  Unauthenticated user
 Role.EVERYONE           $everyone          Everyone

 - 추가된 dynamic roles
 Role.ADMIN              admin            최고관리자
 Role.API                API              인증 없이 API key로 검증 (server/config.json에 API_KEYS)
 **/

const API_KEY = require('../config').restApiKey;

module.exports = function(app) {
  let utils = app.utils;
  let models = app.models;
  let Role = models.Role;

  Role.AUTHENTICATED = '$authenticated';
  Role.registerResolver(Role.AUTHENTICATED, (ROLE, ctx, next) => {
    utils.getCurrentUser().then(user => next(null, !!user))
  });
  Role.UNAUTHENTICATED = '$unauthenticated';
  Role.registerResolver(Role.UNAUTHENTICATED, (ROLE, ctx, next) => {
    utils.getCurrentUser().then(user => next(null, !user))
  });
  Role.EVERYONE = '$everyone';
  Role.registerResolver(Role.EVERYONE, (ROLE, ctx, next) => next(null, true));
  Role.API = '$API';
  Role.registerResolver(Role.API, (ROLE, ctx, next) => {
    let key = ctx.remotingContext.req.query.key;
    if (key && API_KEY == key) {
      next(null, true);
    } else {
      // console.error(`\x1b[41m[API] UNAUTHENTICATED ${key}\x1b[0m`);
      next(null, false); // DENY
    }
  });
};
