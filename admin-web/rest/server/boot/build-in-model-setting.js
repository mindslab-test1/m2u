module.exports = function(app) {
  // ACL setting
  var Acl = app.models.ACL;
  if (Acl.dataSource.connector.name == 'oracle') {
    Acl.mixin("IdGenerator");
    Acl.definition.settings.forceId = false;
  }
  // Role setting
  var Role = app.models.Role;
  if (Role.dataSource.connector.name == 'oracle') {
    Role.mixin("IdGenerator");
    Role.definition.settings.forceId = false;
  }
  // RoleMapping setting
  var RoleMapping = app.models.RoleMapping;
  if (RoleMapping.dataSource.connector.name == 'oracle') {
    RoleMapping.mixin("IdGenerator");
    RoleMapping.definition.settings.forceId = false;
  }
}
