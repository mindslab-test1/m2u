'use strict';
const models = require('../server').models;
const app = require('../server').app;

module.exports = function (Engine) {

  Engine.getBookmarkList = function (name, next) {

    let where = {where: {name: name}};

    models.Engine.findOne(where, (err, engine) => {
      if (err) {
        return next(err, null);
      }

      if (engine === null) {
        var err = new Error('Not Found');
        err.status = 404;
        err.code = 'NOT_FOUND';
        return next(err, null);
      }
      return next(null, engine.bookmarkList);
    })
  };

  Engine.remoteMethod('getBookmarkList', {
      accepts: {arg: 'name', type: 'string', required: true},
      returns: {arg: 'data', type: 'object', root: true}
    }
  );
};
