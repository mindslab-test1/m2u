'use strict';

module.exports = function(PasswordHistory) {
  PasswordHistory.disableRemoteMethodByName('create', true);
  PasswordHistory.disableRemoteMethodByName('upsert', true);
  PasswordHistory.disableRemoteMethodByName('deleteById', true);
  PasswordHistory.disableRemoteMethodByName('updateAll', true);
  PasswordHistory.disableRemoteMethodByName('updateAttributes', false);
  PasswordHistory.disableRemoteMethodByName('createChangeStream', true);
};
