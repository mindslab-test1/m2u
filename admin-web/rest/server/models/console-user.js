'use strict';
const bcrypt = require('bcryptjs');
const models = require('../server').models;
const app = require('../server').app;

const aDayInMilliseconds = 24 * 60 * 60 * 1000;

module.exports = function (ConsoleUser) {
  ConsoleUser.observe('before save', (ctx, next) => {
    if (ctx.hasOwnProperty("data")) {
      ctx.data.lastUpdated = Date.now();
    }
    next();
  });

  ConsoleUser.afterRemote('login', function (ctx, auth, next) {
    models.SignInHistory.create(
      Object.assign({
        username: ctx.args.credentials.username,
        signed: true
      }, getRemoteInformation(ctx)));
    next();
  });

  ConsoleUser.afterRemoteError('login', function (ctx, next) {
    models.SignInHistory.create(
      Object.assign({
        username: ctx.args.credentials.username,
        signed: false,
        msg: ctx.error.message
      }, getRemoteInformation(ctx)));
    next();
  });

  function getRemoteInformation(ctx) {
    return {
      remote: ctx.req.connection.remoteAddress
        || ctx.req.headers['x-forwarded-for'],
      agent: ctx.req.headers['user-agent']
    };
  }

  ConsoleUser.beforeRemote('changePassword', function (ctx, auth, next) {
    return new Promise((resolve, reject) => {
      models.PasswordHistory.find({where: {userId: ctx.args.id}},
        (err, result) => {
          if (result) {
            if (result.some(entry => {
              return bcrypt.compareSync(ctx.args.newPassword, entry.password)
            })) {
              const err = new Error('Password used too recently');
              Object.assign(err, {
                code: 'RECENT_PASSWORD',
                statusCode: 400,
              });
              reject(err);
            } else {
              ConsoleUser.findById(ctx.args.id, (err, result) => {
                if (result) {
                  ctx.hashedValue = result.password;
                }
              });
              resolve();
            }
          } else {
            reject(err);
          }
        })
    });
  });

  ConsoleUser.afterRemote('changePassword', function (ctx, auth, next) {
    if (ctx.args.hasOwnProperty('oldPassword')) {
      const userId = ctx.args.id;
      const filter = {
        where: {userId: userId},
        order: 'lastUpdated ASC'
      };
      models.PasswordHistory.find(filter, (err, result) => {
        if (result) {
          if (result.length < 5) {
            let entry = Object.assign({
              userId: userId,
              password: ctx.hashedValue
            }, getRemoteInformation(ctx));
            models.PasswordHistory.create(entry);
          } else {
            let entry = Object.assign({
              password: ctx.hashedValue,
              lastUpdated: Date.now()
            }, getRemoteInformation(ctx));
            models.PasswordHistory.update({id: result[0].id}, entry);
          }
        }
      });
    }
    next();
  });

  ConsoleUser.isPasswordStale = function (UserId, FreshDaysBy, next) {
    if (typeof FreshDaysBy !== 'number') {
      FreshDaysBy = 90;
    }
    return new Promise((resolve, reject) => {
      ConsoleUser.findById(UserId, (err, result) => {
        if (result) {
          let gap = (Date.now() - result.lastUpdated.valueOf())
            / aDayInMilliseconds;
          resolve(gap < FreshDaysBy);
        } else {
          resolve(false);
        }
      });
    });
  };

  ConsoleUser.remoteMethod(
    'isPasswordStale',
    {
      accepts: [
        {arg: 'UserId', type: 'number', required: true},
        {arg: 'FreshDaysBy', type: 'number', required: false},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.getUserRoles = function (UserId, next) {
    return new Promise((resolve, reject) => {
      let query = {
        and: [{principalType: 'USER'}, {principalId: UserId}]
      };
      models.RoleMapping.find({where: query}, (err, mappings) => {
        if (err) {
          reject(err);
        } else {
          resolve(mappings.map(item => item.roleId));
        }
      })
    })
    .then(roleIds => new Promise((resolve, reject) => {
      if (roleIds.length < 1) {
        resolve({});
        return;
      }
      let ids = roleIds.map(id => {
        return {id: id};
      });
      let where = {where: {or: ids}};
      models.Role.find(where, (err, roles) => {
        if (err) {
          reject(err);
        }
        let _roles = {};
        roles.forEach(role => _roles[role.name] = true);
        resolve(_roles);
      })
    }));
  };

  ConsoleUser.remoteMethod(
    'getUserRoles',
    {
      accepts: [
        {arg: 'UserId', type: 'number', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.getCurrentRoles = function (next) {
    return ConsoleUser.getUserRoles(ConsoleUser.app.userId, next);
  };

  ConsoleUser.remoteMethod(
    'getCurrentRoles',
    {
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.getRoleList = function (next) {
    return new Promise(
      (resolve) => resolve(require('../lib/acl-assets').roleList));
  };

  ConsoleUser.remoteMethod(
    'getRoleList',
    {
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.createWithRoles = function (Args, next) {
    let Account = Args.Account;
    let Roles = Args.Roles;

    return new Promise((resolve, reject) => {
      ConsoleUser.create(Account, (err, user) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve(user);
        }
      })
    })
    .then(user => new Promise((resolve, reject) => {
      if (Roles.length < 1) {
        resolve();
        return;
      }
      let where = {
        where: {
          or: Object.keys(Roles).filter(key => Roles[key] === true).map(key => {
            return {name: key};
          })
        }
      };
      models.Role.find(where, (err, roles) => {
        if (err) {
          reject(err);
          return;
        }
        resolve({userId: user.id, roles: roles});
      })
    }))
    .then(ws => {
      let roleMappings = [];
      ws.roles.forEach(role => {
        roleMappings.push(
          new Promise((resolve, reject) =>
            role.principals.create({
              principalType: 'USER',
              principalId: ws.userId
            }, (err, principal) => {
              if (err) {
                reject(err);
                return;
              }
              resolve(principal);
            })
          )
        )
      });
      return Promise.all(roleMappings);
    })
    .then(() => Promise.resolve({result: true}));
  };

  ConsoleUser.remoteMethod(
    'createWithRoles',
    {
      accepts: [
        {
          arg: 'Account',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
        {arg: 'Roles', type: 'object', required: false, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.patchAttributesWithRoles = function (Args, next) {
    let Account = Args.Account;
    let Roles = Args.Roles;

    return new Promise((resolve, reject) => {
      ConsoleUser.updateAll({id: Account.id}, Account, (err, count) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve();
        }
      })
    })
    .then(() => new Promise((resolve, reject) => {
      if (Roles.length < 1) {
        resolve();
        return;
      }
      let where = {
        where: {
          or: Object.keys(Roles).map(key => {
            return {name: key};
          })
        }
      };
      models.Role.find(where, (err, roles) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(roles);
      })
    }))
    .then(roles => new Promise((resolve, reject) => {
      if (!roles || roles.length < 1) {
        resolve();
        return;
      }
      let deleteRoles = roles.filter(role => Roles[role.name] === false);
      if (deleteRoles.length < 1) {
        resolve(roles);
        return;
      }
      // console.log('delete:' , deleteRoles.map(role => role.name));
      let where = {
        and: [
          {principalType: 'USER'},
          {principalId: Account.id},
          {
            or: deleteRoles.map(role => {
              return {roleId: role.id};
            })
          }
        ]
      };
      models.RoleMapping.destroyAll(where, (err) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(roles);
      });
    }))
    .then(roles => {
      if (!roles || roles.length < 1) {
        Promise.resolve();
        return;
      }
      let addRoles = roles.filter(role => Roles[role.name] === true);
      if (addRoles.length < 1) {
        Promise.resolve();
        return;
      }
      // console.log('add:' , addRoles.map(role => role.name));
      let roleMappings = [];
      addRoles.forEach(role => {
        roleMappings.push(
          new Promise((resolve, reject) =>
            role.principals.create({
              principalType: 'USER',
              principalId: Account.id
            }, (err, principal) => {
              if (err) {
                reject(err);
                return;
              }
              resolve(principal);
            })
          )
        )
      });
      return Promise.all(roleMappings);
    })
    .then(() => Promise.resolve({result: true}));
  };

  ConsoleUser.remoteMethod(
    'patchAttributesWithRoles',
    {
      accepts: [
        {
          arg: 'Account',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
        {arg: 'Roles', type: 'object', required: false, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /** login with sso **/
  ConsoleUser.ssoLogin = function (username, email, name, req, res) {
    return new Promise((resolve, reject) => {
      console.log('ssoLogin 들어옴');
      models.ConsoleUser.findOne({where: {username: username}})
      .then(findUser => {
        //console.log(findUser);
        if (findUser === null) {
          console.log('create!!!');
          models.ConsoleUser.create({
            username: username,
            password: username,
            email: email,
            activated: 1
          }, function (err, userObj) {
            if (err) {
              console.log('error', err);
              return reject(err);
            }

            let chain = new Promise((resolve2, reject2) => {
              let where = {
                where: {name: 'dashboard^R'}
              };
              models.Role.find(where, (err, roles) => {
                if (err) {
                  reject2(err);
                  return;
                }

                // console.log('roles : ', roles);
                resolve2({user: userObj, roles: roles});
              });
            });
            let tempRoles = {roles: {}};
            let tempUser = {};

            chain = chain.then(ws => {
              tempUser = ws.user;
              tempRoles = ws.roles;
              // console.log('ws : ', ws);
              let roleMappings = [];
              ws.roles.forEach(role => {
                roleMappings.push(
                  new Promise((resolve3, reject3) =>
                    role.principals.create({
                      principalType: 'USER',
                      principalId: ws.user.id
                    }, (err, principal) => {
                      // console.log('principal : ', principal);
                      if (err) {
                        reject3(err);
                        return;
                      }
                      resolve3(principal);
                    })
                  )
                )
              });
              return Promise.all(roleMappings);
            });

            chain.then(data => {
              models.ConsoleUser.login(
                {
                  username: userObj.username,
                  password: userObj.username
                }, 'user', function (err, loginResult) {
                  if (err) {
                    console.log(err);
                    return reject(err);
                  }

                  let userData = loginResult;
                  let tempRole = {
                    roles: undefined
                  };
                  tempRole = tempRoles[0].name;
                  // console.log('userResult : ', loginResult.user);
                  let user = {
                    username: tempUser.username,
                    email: tempUser.email,
                    activated: tempUser.activated,
                    id: tempUser.id,
                    roles: {
                      'dashboard^R': true
                    }
                  };

                  // console.log('loginResult :', loginResult)
                  // console.log('user : ', user);
                  res.cookie('$LoopBackSDK$created',
                    JSON.stringify(loginResult.created), {
                      encode: function (value) {
                        return value;
                      }
                    });
                  res.cookie('$LoopBackSDK$id', loginResult.id);
                  res.cookie('$LoopBackSDK$ttl', loginResult.ttl);
                  res.cookie('$LoopBackSDK$userId', loginResult.userId);
                  res.cookie('$LoopBackSDK$rememberMe', true);
                  res.cookie('$LoopBackSDK$user', JSON.stringify(user), {
                    encode: function (value) {
                      return value;
                    }
                  });
                  return resolve(loginResult);
                }
              )
            });
          });
        } else {
          console.log('try login');
          models.ConsoleUser.login(
            {
              username: findUser.username,
              password: findUser.username
            }, 'user', function (err, loginResult) {
              if (err) {
                console.log(err);
                return reject(err);
              }
              // console.log('findUser : ', findUser);
              let query = {
                and: [{principalType: 'USER'}, {principalId: findUser.id}]
              };
              let chain = new Promise((resolve1, reject1) => {
                models.RoleMapping.find({where: query}, (err, mappings) => {
                  if (err) {
                    reject1(err);
                  } else {
                    // console.log('mappings : ', mappings);
                    resolve1(mappings.map(item => item.roleId));
                  }
                });
              });

              let tempRoles;
              let chain2 = chain.then(
                roleIds => new Promise((resolve2, reject2) => {
                  // console.log('roleIds : ', roleIds);
                  if (roleIds.length < 1) {
                    resolve({});
                    return;
                  }
                  let ids = roleIds.map(id => {
                    return {id: id};
                  });
                  let where = {where: {or: ids}};

                  models.Role.find(where, (err, roles) => {
                    if (err) {
                      reject2(err);
                    }
                    let _roles = {};
                    roles.forEach(role => _roles[role.name] = true);
                    tempRoles = _roles;
                    // console.log('_roles: ', _roles);
                    resolve2(_roles);
                  })
                }));

              chain2.then(() => {
                // console.log('chain roles :', roles);
                // console.log('temp roles : ', tempRoles);

                let user = {
                  username: findUser.username,
                  email: findUser.email,
                  activated: findUser.activated,
                  id: findUser.id,
                  roles: tempRoles
                };
                // console.log('user: ', user);
                res.cookie('$LoopBackSDK$created',
                  JSON.stringify(loginResult.created), {
                    encode: function (value) {
                      return value;
                    }
                  });
                res.cookie('$LoopBackSDK$id', loginResult.id);
                res.cookie('$LoopBackSDK$ttl', loginResult.ttl);
                res.cookie('$LoopBackSDK$userId', loginResult.userId);
                res.cookie('$LoopBackSDK$rememberMe', 'true');
                res.cookie('$LoopBackSDK$user', JSON.stringify(user), {
                  encode: function (value) {
                    return value;
                  }
                });
                return resolve(findUser);
              });
            });
        }
      })
      .catch(err => reject(err));
    });
  };

  ConsoleUser.remoteMethod(
    'ssoLogin', {
      http: {verb: 'post'},
      accepts: [
        {arg: 'username', type: 'string'},
        {arg: 'email', type: 'string'},
        {arg: 'name', type: 'string'},
        {arg: 'req', type: 'object', http: {source: 'req'}},
        {arg: 'res', type: 'object', http: {source: 'res'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );
};
