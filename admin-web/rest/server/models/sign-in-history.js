'use strict';

module.exports = function(SignInHistory) {
  SignInHistory.disableRemoteMethodByName('create', true);
  SignInHistory.disableRemoteMethodByName('upsert', true);
  SignInHistory.disableRemoteMethodByName('deleteById', true);
  SignInHistory.disableRemoteMethodByName('updateAll', true);
  SignInHistory.disableRemoteMethodByName('updateAttributes', false);
  SignInHistory.disableRemoteMethodByName('createChangeStream', true);
};
