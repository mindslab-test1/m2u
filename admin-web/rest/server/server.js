'use strict';

/** will not using loopback-context
# leave it at first line of entry script
require('cls-hooked');

# middleware config
"loopback-context#per-request": {
  "params": {
    "enableHttpContext": true
  }
},
**/

const loopback = require('loopback');
const boot = require('loopback-boot');
const app = module.exports = loopback();


// request logger
if (process.env.NODE_ENV !== 'production') {
  app.use(require('body-parser').urlencoded({extended: true}), (req, res, next) => {
    console.log(req.method, req.url, req.body);
    next();
  });
}

app.start = function() {
  return app.listen(function() {
    app.emit('started');
    let baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);

    if (app.get('loopback-component-explorer')) {
      let explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }

    // request tester
    if (process.env.NODE_ENV !== 'production') {
      app.all('/ping', require('body-parser').urlencoded({extended: true}), (req, res) => {
        let html = `
        <html>
        <body>
        <h1>Header</h1>
        <pre>${JSON.stringify(req.headers, null, 4)}</pre>
        <h1>Body (urlencoded)</h1>
        <pre>${JSON.stringify(req.body, null, 4)}</pre>
        </body>
        </html>
        `;
        res.send(html);
        console.log('=====PING-HEADER=====\n', req.headers, '\n=====PING-BODY=====\n', req.body, '\n=====PING-END=====');
      });
      console.log('Test a request at %s/ping', baseUrl);
    }
  });
};



// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) {
    app.start();
  }
});
