'use strict';

module.exports = function(Model) {
  Model.observe('before save', function beforeSave(ctx, next) {

    let instance = ctx.instance || ctx.data;

    if (ctx.isNewInstance && instance.getDataSource().connector.name === 'oracle') {
      var ds = instance.getDataSource();
      var modelName = ctx.Model.definition.name;
      var modelNameUpper = modelName.toUpperCase();

      let sql = `select ${modelNameUpper}_ID_SEQUENCE.NEXTVAL as ID from dual`;

      ds.connector.query(sql, function(err, results) {
        if (err) {
          console.error(err);
          return next(err);
        }
        instance.id = results[0].ID;
        return next();
      });
    } else {
      return next();
    }
  });
};
