import {Component, OnInit, OnDestroy, ViewEncapsulation, AfterViewInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../core/actions';
import {GrpcApi} from '../core/sdk/index';
import {AppObjectManagerService} from '../shared/index';


@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MonitoringComponent implements OnInit, OnDestroy, AfterViewInit {
  spinnerFlag = false;
  svdglist = [];

  svdp = {
    name: undefined,
    group: undefined,
    description: undefined,
    start: undefined,
    stop: undefined,
    now: undefined,
    state: undefined,
    statename: undefined,
    spawnerr: undefined,
    existstatus: undefined,
    logfile: undefined,
    stdout_logfile: undefined,
    stderr_logfile: undefined,
    pid: undefined
  };

  svdpkey = {
    grp_name: undefined,
    host: undefined,
    ip: undefined,
    port: undefined,
    name: undefined
  };

  constructor(private store: Store<any>,
              private grpc: GrpcApi,
              private objectManager: AppObjectManagerService) {
    this.getDrawData();
  }

  ngOnInit() {
    this.getPrcsList();
  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
  }

  doChange(date, type) {
    this.getDrawData();
  }

  getPrcsList() {
    this.grpc.getSupervisorServerGroupList().subscribe(
      result => {
        if (this.svdglist.length === 0) {
          this.svdglist = result.svd_svr_grp_list;
        } else {
          this.setDiffrenceChanges(result.svd_svr_grp_list[0]);
        }
      },
      err => {
        let message = 'Something went wrong. [ ' + err.message + '(' + err.code + ') ]';
      });
  }

  periodChange(type) {
    let self = this;

    this.spinnerFlag = true;

    setTimeout(function () {
      self.getDrawData();
      self.spinnerFlag = false;
    }, 0);


  }

  getDrawData() {
    let self = this;
    self.store.dispatch({type: ROUTER_LOADED});
  }

  sendStart(ip, port, group, name) {
    this.svdpkey.grp_name = group;
    this.svdpkey.host = '';
    this.svdpkey.ip = ip;
    this.svdpkey.port = port;
    this.svdpkey.name = name;
    this.grpc.startSupervisorProcess(this.svdpkey).subscribe(
      result => {
        this.getPrcsList();
      },
      err => {
        let message = 'Something went wrong. [ ' + err.message + '(' + err.code + ') ]';
      });
    return false;
  }

  sendStop(ip, port, group, name) {
    this.svdpkey.grp_name = group;
    this.svdpkey.host = '';
    this.svdpkey.ip = ip;
    this.svdpkey.port = port;
    this.svdpkey.name = name;
    this.grpc.stopSupervisorProcess(this.svdpkey).subscribe(
      result => {
        // alert(result.message);
        this.getPrcsList();
        // this.svdglist[0].svd_proc_list[0].state = 200;
      },
      err => {
        let message = 'Something went wrong. [ ' + err.message + '(' + err.code + ') ]';
      });
    return false;
  }

  setDiffrenceChanges(data) {
    let self = this;
    data.svd_proc_list.forEach((svd_proc, idx) => {
      if (self.svdglist[0].svd_proc_list[idx].state !== svd_proc.state) {
        self.svdglist[0].svd_proc_list[idx].state = svd_proc.state;
      }
    })
  }
}
