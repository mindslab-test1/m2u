import { DA_DELETE, DA_UPDATE, DA_CREATE, DA_UPDATE_ALL } from './actions';

export function daReducer(state = [], action) {
  switch (action.type) {
    case DA_CREATE:
      return [action.payload, ...state];

    case DA_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.name === item.da_info.name ? updatedItem : item);

    case DA_UPDATE_ALL:
      return action.payload;

    case DA_DELETE:
      let deletedIds = action.payload;
      return state.filter(item => deletedIds.indexOf(item.da_info.name) === -1) ;

    default:
      return state;
  }
};
