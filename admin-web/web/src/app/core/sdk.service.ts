import { Injectable } from '@angular/core';
import { LoopBackConfig, LoopBackAuth } from '../core/sdk/index';

@Injectable()
export class SDKService {
  constructor(
    private auth: LoopBackAuth
  ) {}

  getURL(appends: any[], query: any = {}) {
    let url = `${LoopBackConfig.getPath()}/${LoopBackConfig.getApiVersion()}/${appends.join('/')}`;
    Object.assign(query, {access_token: this.auth.getAccessTokenId()});
    Object.keys(query).forEach((key, index) => {
      url += `${(index === 0 ? '?' : '&')}${key}=${window['encodeURIComponent'](query[key])}`;
    });
    return url;
  }

  openURL(...args) {
    window.location.href = this.getURL.apply(this, args);
  }

  openBlankURL(...args) {
    window.open(this.getURL.apply(this, args), '_blank');
  }
};
