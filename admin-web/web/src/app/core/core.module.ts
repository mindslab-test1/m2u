import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SDKBrowserModule} from './sdk/index';
import {CookieModule} from 'ngx-cookie';
import {CsvService} from 'angular2-json2csv';
import {StoreModule} from '@ngrx/store';
import {
  routerReducer,
  authReducer, damReducer, daReducer, scReducer, cbReducer, daiReducer, itfReducer, apReducer, accountReducer,
  AuthGuard, AuthService, ClipboardService, SDKService, EngineReducer,
} from './index';

@NgModule({
  imports: [
    CommonModule,
    CookieModule.forRoot(),
    SDKBrowserModule.forRoot(), // loopback SDK

    // reducers
    StoreModule.forRoot({
      router: routerReducer,
      auth: authReducer,
      dams: damReducer,
      das: daReducer,
      cbs: cbReducer,
      scs: scReducer,
      dais: daiReducer,
      itfs: itfReducer,
      aps: apReducer,
      accounts: accountReducer,
      engines: EngineReducer,
    }),

    // dev tool
    // StoreDevtoolsModule.instrumentOnlyWithExtension(), // http://zalmoxisus.github.io/redux-devtools-extension/
  ],
  providers: [
    AuthGuard,
    AuthService,
    CsvService,
    ClipboardService,
    SDKService,
  ],
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
