import { DAI_DELETE, DAI_UPDATE, DAI_CREATE, DAI_UPDATE_ALL } from './actions';

export function daiReducer(state = [], action) {
  switch (action.type) {
    case DAI_CREATE:
      return [action.payload, ...state];

    case DAI_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.dai_id === item.dai_id ? updatedItem : item);

    case DAI_UPDATE_ALL:
      return action.payload;

    case DAI_DELETE:
      let deletedIds = action.payload;
      return state.filter(item => deletedIds.indexOf(item.dai_id) === -1) ;

    default:
      return state;
  }
};
