import {AP_DELETE, AP_UPDATE, AP_CREATE, AP_UPDATE_ALL} from './actions';

export function apReducer(state = [], action) {
  switch (action.type) {
    case AP_CREATE:
      return [action.payload, ...state];

    case AP_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.name === item.name ? updatedItem : item);

    case AP_UPDATE_ALL:
      return action.payload;

    case AP_DELETE:
      let deletedIds = action.payload;
      return state.filter(item => deletedIds.indexOf(item.name) === -1);

    default:
      return state;
  }
};
