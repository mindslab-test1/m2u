import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {ConsoleUserApi, LoopBackAuth} from '../core/sdk/index';
import {AUTH_UPDATE} from './actions';
import {ErrorDialogComponent} from '../shared/dialogs/error-dialog/error-dialog.component';
import {MatDialog} from "@angular/material/dialog";

const DefaultSeconds = 30 * 60;

@Injectable()
export class AuthService {
  static ROLE = {
    EVERYONE: '$everyone',
    UNAUTHENTICATED: '$unauthenticated',
    AUTHENTICATED: '$authenticated',
    ADMIN: 'admin',
    CREATE: '^C',
    READ: '^R',
    UPDATE: '^U:block', // 임시로 막음.
    DELETE: '^D',
    EXECUTE: '^X:block', // 임시로 막음.
  };

  /**
   * Check whether the role is allowed.
   * @param {id: string|type: string} id or role type.
   * @param {type: string|role: string[]} role type or array of roles.
   * @param {roles: string[]|null} array of roles or null.
   * @return {boolean} The resulting allowed.
   */
  static isAllowed(id: any, type: any, roles?: any): boolean {
    if (arguments.length === 3) {
      return roles[id + type] === true || roles[AuthService.ROLE.ADMIN] === true;
    } else if (arguments.length === 2) {
      return arguments[1][arguments[0]] === true;
    }
  }

  /**
   * Check whether the role is not allowed.
   * @param {id: string|type: string} id or role type.
   * @param {type: string|role: string[]} role type or array of roles.
   * @param {roles: string[]|null} array of roles or null.
   * @return {boolean} The resulting allowed.
   */
  static isNotAllowed(id: any, type: any, roles?: any): boolean {
    if (arguments.length === 3) {
      return roles[id + type] !== true && roles[AuthService.ROLE.ADMIN] !== true;
    } else if (arguments.length === 2) {
      return arguments[1][arguments[0]] !== true;
    }
  }

  constructor(private store: Store<any>,
              private router: Router,
              private dialog: MatDialog,
              private consoleUserApi: ConsoleUserApi,
              private authApi: LoopBackAuth) {
  }

  isTokenValid: any = (ticks: number = Date.now()): boolean => {
    let token = this.authApi.getToken();
    return !!token.id && ticks < new Date(token.created).getTime() + token.ttl * 1000;
  };

  validateToken() {
    if (!this.isTokenValid()) {
      this.clear();
    }
  }

  elongateTokenTTL: any = (seconds: number = DefaultSeconds): void => {
    let token = this.authApi.getToken();
    if (token.id) {
      token.created = new Date().toISOString();
      token.ttl = seconds;
      this.authApi.setToken(token);
    }
  };

  // 권한 체크에 범용적으로 이용 할 수 있는 메소드
  check(rule: (auth: any, router: any) => boolean): Observable<boolean> {
    return this.store.select(s => s).map(s => rule(s.auth, s.router));
  }

  checkWithACL(acl: any = []): Observable<boolean> {
    let roles = acl.roles;

    if (!roles || roles.length === 0) {
      return Observable.of(true);
    }

    this.validateToken();

    return this.check((auth, router) => {
      let user = auth.user;
      let passed = roles.some(role => {
        switch (role) {
          case AuthService.ROLE.EVERYONE:
            return true;

          case AuthService.ROLE.UNAUTHENTICATED:
            return !user;

          case AuthService.ROLE.AUTHENTICATED:
            return !!user;

          default:
            return !!user && user.roles && (user.roles[AuthService.ROLE.ADMIN] === true || user.roles[role] === true);
        }
      });

      if (passed) {
        this.elongateTokenTTL();
      }

      return passed;
    });
  }

  goHome(): void {
    this.store.select(s => s.auth).take(1)
    .subscribe(auth => {
      if (auth.user) {
        let freshDaysBy = 60;
        this.consoleUserApi.isPasswordStale(this.consoleUserApi.getCurrentId(), freshDaysBy)
        .subscribe(
          res => {
            if (res) {
              this.router.navigateByUrl('/dashboard');
            } else {
              let ref = this.dialog.open(ErrorDialogComponent);
              ref.componentInstance.title = 'Failed';
              ref.componentInstance.message = `It's been over 90 days since you changed
              your password. Change password please.`;
              this.router.navigateByUrl('/profile/view');
            }
          },
          err => {
            console.error(err.message);
          });
      } else {
        this.router.navigateByUrl('/auth/signin');
      }
    });
  }

  // authentication methods
  clear(): void {
    this.authApi.clear();
    this.store.dispatch({type: AUTH_UPDATE, payload: {user: null}});
  }

  sync(callback: (user: any) => void = null): void {
    this.validateToken();
    let user = this.consoleUserApi.isAuthenticated() && this.consoleUserApi.getCachedCurrent() || null;

    // failed to fetch user
    if (!!user && user.activated) {
      this.elongateTokenTTL();
      this.store.dispatch({type: AUTH_UPDATE, payload: {user: user}});
      if (callback) {
        callback(user);
      }
    } else {
      this.clear();
      if (callback) {
        callback(null);
      }
    }
  }

  login(credentials, errorCallback: (err: any) => void = null): void {
    this.consoleUserApi.login(credentials).map(auth => auth).toPromise()
    .then(auth => this.consoleUserApi.getCurrentRoles().map(roles => auth.user.roles = roles).toPromise())
    .then(() => this.sync(() => this.goHome()))
    .catch(err => {
      this.clear();
      if (errorCallback) {
        errorCallback(err);
      }
    });
  }

  logout(): void {
    this.consoleUserApi.logout()
    .subscribe(() => {
      this.clear();
      this.router.navigateByUrl('/auth/signin');
    }, err => {
      this.clear();
      this.router.navigateByUrl('/auth/signin');
    });
  }
}
