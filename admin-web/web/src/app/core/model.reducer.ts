import { MODEL_CREATE, MODEL_DELETE, MODEL_UPDATE, MODEL_UPDATE_ALL } from './actions';

export function modelReducer(state = [], action) {
  switch (action.type) {
    case MODEL_CREATE:
      return [action.payload, ...state];

    case MODEL_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.id === item.id ? updatedItem : item);

    case MODEL_UPDATE_ALL:
      return action.payload;

    case MODEL_DELETE:
      let updatedIds = action.payload;
      return state.filter(item => updatedIds.indexOf(item.id) === -1);

    default:
      return state;
  }
};
