import {SC_DELETE, SC_UPDATE, SC_CREATE, SC_UPDATE_ALL} from './actions';

export function scReducer(state = [], action) {
  switch (action.type) {
    case SC_CREATE:
      return [action.payload, ...state];

    case SC_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.name === item.name ? updatedItem : item);

    case SC_UPDATE_ALL:
      return action.payload;

    case SC_DELETE:
      let deletedIds = action.payload;
      return state.filter(item => deletedIds.indexOf(item.name) === -1);

    default:
      return state;
  }
};
