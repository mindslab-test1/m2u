import { DAM_DELETE, DAM_UPDATE, DAM_CREATE, DAM_UPDATE_ALL } from './actions';

export function damReducer(state = [], action) {
  switch (action.type) {
    case DAM_CREATE:
      return [action.payload, ...state];

    case DAM_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.dam_info.name === item.dam_info.name ? updatedItem : item);

    case DAM_UPDATE_ALL:
      return action.payload;

    case DAM_DELETE:
      let deletedItems = action.payload;
      return state.filter(item => deletedItems.indexOf(item.dam_info.name) === -1) ;

    default:
      return state;
  }
};
