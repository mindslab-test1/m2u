import { ITF_DELETE, ITF_UPDATE, ITF_CREATE, ITF_UPDATE_ALL } from './actions';

export function itfReducer(state = [], action) {
  switch (action.type) {
    case ITF_CREATE:
      return [action.payload, ...state];

    case ITF_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.name === item.name ? updatedItem : item);

    case ITF_UPDATE_ALL:
      return action.payload;

    case ITF_DELETE:
      let deletedIds = action.payload;
      return state.filter(item => deletedIds.indexOf(item.name) === -1) ;

    default:
      return state;
  }
};
