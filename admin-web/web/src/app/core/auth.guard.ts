import {Injectable} from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  Router
} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {ROUTER_UPDATE} from './actions';
import {AuthService} from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  init: Observable<any> = null;

  constructor(private store: Store<any>,
              private router: Router,
              private authService: AuthService) {
    // sync auth state at first load, and delay canActivate,
    // because canActivate needs auth info to check ACL of routes
    this.init = Observable.fromPromise(
      new Promise(resolve => {
        this.authService.sync(resolve);
      })
    );
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    // console.log('canActivate triggered for', route.routeConfig);
    let paths = route.pathFromRoot
      .map(m => m.url)
      .reduce((arr, url) => arr.concat(url), [])
      .map(url => url.path);

    console.log('canActivate: ' + paths.join('/'));

    // after initialized
    return this.init.flatMap(() => {

      // console.log('canActivate check', route.routeConfig);

      // check router's data.ACL configuration
      let data: any = route.data,
        params: any = route.root.firstChild.params;

      return this.authService.checkWithACL(data.ACL).take(1)
        .do(resolved => {

          // console.log('AuthGuard resolved:', resolved, route, data.ACL);

          // update router state
          if (resolved) {
            this.store.dispatch({type: ROUTER_UPDATE, payload: route});

            // failed to access
          } else {
            // if data.fallback is exists
            if (!!data.fallback && data.fallback instanceof Array) {
              let parentPaths = route.parent.pathFromRoot
                .map(m => m.url)
                .reduce((arr, url) => arr.concat(url), [])
                .map(url => url.path);
              let fallbackPaths = parentPaths.concat(data.fallback);

              console.log('AuthGuard fallback:', fallbackPaths.join('/'));
              return this.router.navigate(fallbackPaths);
            }

            console.error('AuthGuard stuck');
            this.authService.goHome();
          }
        });
    });
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }
}
