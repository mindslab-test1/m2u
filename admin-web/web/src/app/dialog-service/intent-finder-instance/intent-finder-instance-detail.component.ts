import {Component, OnInit} from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';

import {ROUTER_LOADED} from '../../core/actions';
import {AuthService} from '../../core/auth.service';
import {AlertComponent, AppObjectManagerService, getErrorString} from '../../shared/index';
import {ConsoleUserApi} from '../../core/sdk/index';
import {AppEnumsComponent} from '../../shared/values/app.enums';
import {ErrorDialogComponent} from '../../shared/dialogs/error-dialog/error-dialog.component';
import {GrpcApi} from '../../core/sdk';
import {Location} from '@angular/common';

@Component({
  selector: 'app-intent-finder-instance-detail',
  templateUrl: './intent-finder-instance-detail.component.html',
  styleUrls: ['./intent-finder-instance-detail.component.scss'],
})

/**
 * IntentFinder Instance Detail 보여주는 Component
 */
export class IntentFinderInstanceDetailComponent implements OnInit {
  actions: any;
  itfi: any;
  itfp: any;

  constructor(private store: Store<any>,
              private objectManager: AppObjectManagerService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private consoleUserApi: ConsoleUserApi,
              private grpc: GrpcApi,
              public appEnum: AppEnumsComponent,
              private location: Location) {
  }

  ngOnInit(): void {
    let id = 'itf';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      test: {
        icon: 'streetview',
        text: 'Test',
        callback: this.test,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.READ, roles)
      }
    };

    // objectManager안에 itfi 정보를 맴버변수 itfi에 저장합니다
    this.itfi = this.objectManager.get('itfi');
    // itfi 정보가 없는 경우엔 이전페이지로 이동합니다
    if (this.itfi === undefined) {
      this.backPage();
      return;
    }

    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * IntentFinder Instance 수정 페이지로 이동
   */
  edit: any = (): void => {
    // upsert 페이지에 전달하기 위한 itfi_name을 설정합니다
    this.objectManager.set('itfi_name', this.itfi.name);
    this.router.navigateByUrl('dialog-service/itfi-upsert#edit');
  };

  /**
   * IntentFinder Instance 삭제 API 호출
   */
  delete: any = (): void => {
    let names = [];
    names.push(this.itfi.name);

    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      } else {
        let keyList = names.map(key => {
          return {name: key};
        });
        this.grpc.deleteIntentFinderInstances(keyList).subscribe(
          res => {
            this.snackBar.open(`Selected Intent Finder Instance Successfully Deleted.`,
              'Confirm', {duration: 3000});
            this.backPage();
          },
          err => {
            let eref = this.dialog.open(ErrorDialogComponent);
            eref.componentInstance.title = 'Failed';
            ref.componentInstance.message = `Failed to Delete a Intent Finder Instance. [${getErrorString(err)}]`;
            return;
          }
        )
      }
    });
  };

  getIntentFinderPolicyInfo(){
    return new Promise(resolve => {
      this.grpc.getIntentFinderPolicyInfo(this.itfi.policy_name).subscribe(res => {
        if (res) {
          this.itfp = res;
          resolve();
        }
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        console.error('err', err.message);
      });
    });
  }

  /**
   * IntnetFinder Test 화면 이동
   */
  test: any = () => {
    this.objectManager.set('itfi', this.itfi.name);
    this.router.navigateByUrl('dialog-service/itfi-test');
  };

  /**
   * IntentFinder Instance 리스트 조회 페이지로 이동
   */
  backPage: any = (): void => {
    // this.router.navigate(['../itfi'], {relativeTo: this.route});
    this.objectManager.clean('itfi');
    this.location.back();
  };

  navigateItfp() {
    this.getIntentFinderPolicyInfo().then(() => {
      this.objectManager.set('itfp', this.itfp);
      this.router.navigateByUrl('dialog-service/itfp-detail');
    });
  }
}
