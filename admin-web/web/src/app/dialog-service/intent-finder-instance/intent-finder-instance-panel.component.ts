import {Component, OnInit} from '@angular/core';
import {AppEnumsComponent} from '../../shared/values/app.enums';

@Component({
  selector: 'app-intent-finder-instance-panel',
  templateUrl: './intent-finder-instance-panel.component.html',
  styleUrls: ['./intent-finder-instance-panel.component.scss']
})

/**
 * IntentFinder Instance Panel 보여주는 Component
 */
export class IntentFinderInstancePanelComponent implements OnInit {
  itfiInfo: any;

  constructor(public appEnum: AppEnumsComponent) {
  }

  ngOnInit(): void {
  }
}
