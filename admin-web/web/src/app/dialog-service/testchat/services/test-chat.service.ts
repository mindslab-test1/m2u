import 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AppComponent} from '../../../app.component';
import {HttpClient} from '@angular/common/http';


const HEADERS = {headers: {'m2u-auth-internal': 'm2u-auth-internal'}};

@Injectable()
export class TestChatService {
  apiUrl: string;
  wsUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = AppComponent.getApiUrl();
    this.wsUrl = AppComponent.getWsUrl();
  }

  signIn(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/auth/signIn', param, HEADERS);
  }

  signOut(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/auth/signOut', param, HEADERS);
  }

  open(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/dialog/open', param, HEADERS);
  }

  close(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/dialog/close', param, HEADERS);
  }

  textToTextTalk(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/dialog/textToTextTalk', param, HEADERS);
  }

  eventToTextTalk(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/dialog/eventToTextTalk', param, HEADERS);
  }

  getSpeechToSpeechTalkUrl(): string {
    return this.wsUrl + '/v3/ws/speechToSpeechTalk';
  }

  getSpeechToTextTalkUrl(): string {
    return this.wsUrl + '/v3/ws/speechToTextTalk';
  }
}
