import {
  Component,
  OnInit,
  AfterContentInit,
  AfterViewInit,
  ViewChild,
  ViewChildren,
  QueryList, ChangeDetectorRef
} from '@angular/core';
import {Store} from '@ngrx/store';
import {ActivatedRoute} from '@angular/router';
import {MatListItem, MatSidenav, MatSnackBar} from '@angular/material';
import {ROUTER_LOADED} from '../../../core/actions';
import {AppObjectManagerService, getErrorString} from '../../../shared/index';
import {GrpcApi} from '../../../core/sdk/index';
import {ToolsComponent} from '../../../shared/components/tools/tools.component';
import {TestChatPanelComponent} from './test-chat-panel.component';
import {TestChatComponent} from './test-chat.component';


@Component({
  selector: 'app-test-chat-view',
  templateUrl: './test-chat-view.component.html',
  styleUrls: ['./test-chat-view.component.scss'],
})

export class TestChatViewComponent implements OnInit, AfterViewInit {
  panelToggleText: string = 'Hide Info Panel';
  chatbots: any;
  selectedChatbot: any;
  chatbot_name: string;
  spec: string;
  talks = [];

  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('chatMain') chatMain: TestChatComponent;
  @ViewChild('tools') tools: ToolsComponent;
  @ViewChildren('chatbotList') chatbotList: QueryList<MatListItem>;

  constructor(private store: Store<any>,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private grpc: GrpcApi,
              private cdr: ChangeDetectorRef,
              private objectManager: AppObjectManagerService) {
  }

  ngOnInit() {

    this.route.queryParams.subscribe(
      querys => {
        this.chatbot_name = querys['chatbot'];
      }
    )

    this.grpc.getChatbotAllList().subscribe(
      result => {
        if (result.chatbot_list.length > 0) {
          result.chatbot_list.sort((a, b) => a.title > b.title);
          this.chatbots = result.chatbot_list;

          if (this.chatbot_name) {
            this.chatbots.forEach((chatbot, index) => {
              if (chatbot.name === this.chatbot_name) {
                this.selectedChatbot = chatbot;
              }
            });
          } else {
            this.selectedChatbot = this.chatbots[0];
          }
        } else {
          this.snackBar.open('There is No Chatbot.', 'Confirm', {duration: 3000});
        }
        this.store.dispatch({type: ROUTER_LOADED});
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });
  }

  ngAfterViewInit() {
    this.chatbotList.changes.subscribe((r) => {
      this.fillInfo();
    });
  }


  togglePanel() {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  }

  fillInfo(chatbot ?, idx ?) {
    this.sidenav.open();
    this.panelToggleText = 'Hide Info Panel';
    if (chatbot) {
      this.selectedChatbot = chatbot;
      this.talks = []; // init talks
    }


    this.chatbots.forEach((item, index) => {
      let chatbotEle = <HTMLElement>(document.getElementById('chatbot_' + index));
      if (item.name === this.selectedChatbot['name']) {
        chatbotEle.style.setProperty('background-color', '#00c8dd');
        chatbotEle.style.setProperty('color', '#ffffff');
      } else {
        chatbotEle.style.setProperty('background-color', '#ffffff');
        chatbotEle.style.setProperty('color', '#000000');
      }
    });
    this.cdr.detectChanges();
  }

  changePanelWidth(spec) {
    if (spec === 'v3') {
      (<HTMLElement>document.getElementById('sidenav')).style.setProperty('width', '40%');
      this.spec = spec;
    } else if (spec === 'v1') {
      (<HTMLElement>document.getElementById('sidenav')).style.setProperty('width', '40%');
      this.spec = spec;
    }
  }

  setTalks(talks) {
    this.talks = [...talks];
  }
}
