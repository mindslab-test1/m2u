import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {
  ErrorStateMatcher, MatDialog, MatSnackBar, ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { ROUTER_LOADED } from '../../core/actions';
import {
  AlertComponent, AppEnumsComponent, AppObjectManagerService, FormErrorStateMatcher, getErrorString
} from '../../shared/index';
import { GrpcApi } from '../../core/sdk/index';
import { ErrorDialogComponent } from 'app/shared/dialogs/error-dialog/error-dialog.component';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { DragulaService } from 'ng2-dragula';

type matchType = {
  words: string[], range: number[], skill: string, regex: string
}

@Component({
  selector: 'app-simple-classifier-upsert',
  templateUrl: './simple-classifier-upsert.component.html',
  styleUrls: ['./simple-classifier-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class SimpleClassifierUpsertComponent implements OnInit {
  dialogTitle: string;
  role: any;
  sc = {
    name: undefined,
    lang: undefined,
    description: undefined,
    skill_cnt: undefined,
    reg_cnt: undefined,
    skill_list: [],
    pos_tagging: false,
    named_entity: false,

  };
  scList = [];
  org_sc: any;

  selectedSkill: any;
  newSkill: any;
  newRegExp: any;
  skill_description: any;
  submitButton: any;

  oriSentList: string[] = [];
  posSentList: string[] = [];
  nerSentList: string[] = [];
  posAndNerSentList: string[] = [];

  nluSentence: string;
  posResult: string;
  nerResult: string;
  posNerResult: string;
  sentences: string[] = [];
  matchSet: matchType[] = [];

  nameFormControl = new FormControl('', [Validators.required,
    () => !!this.sc.name && this.scList.find(
      sc => sc === this.sc.name) ? {'duplicated': true} : null]);

  skillFormControl = new FormControl(undefined, [Validators.required]);

  regExFormControl = new FormControl('', [Validators.required]);

  constructor(private store: Store<any>, private snackBar: MatSnackBar,
    private route: ActivatedRoute, private router: Router, public dialog: MatDialog,
    public appEnum: AppEnumsComponent, public formErrorStateMatcher: FormErrorStateMatcher,
    private objectManager: AppObjectManagerService, private dragulaService: DragulaService,
    private cdr: ChangeDetectorRef, private grpc: GrpcApi) {
    const bag: any = this.dragulaService.find('evented-bag');
    if (bag !== undefined) {
      this.dragulaService.destroy('evented-bag');
      this.dragulaService.setOptions('evented-bag', {revertOnSpill: true});
    }
  }

  ngOnInit() {
    this.route.fragment.subscribe(role => {
      let pro = new Promise((resolve, reject) => {
        this.grpc.getSimpleClassifierAllList().subscribe(scs => {
          this.scList = [];
          scs.sc_list.map(sc => this.scList.push(sc.name));
          resolve();
        }, err => {
          let message = `Something went wrong. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000})
          reject();
        });
      });

      pro.then(() => {
        this.role = role;
        switch (this.role) {
          case 'add':
            this.submitButton = true;
            this.dialogTitle = 'Add Simple Classifier';
            let scv = this.objectManager.get('sc');
            if (scv !== undefined) {
              this.sc = scv;
              this.sc.lang = this.appEnum.langCodeKeys[this.sc.lang];
              this.nameFormControl.patchValue(this.sc.name);
              if (this.scList.find(v => v === this.sc.name)) {
                this.nameFormControl.setErrors({'duplicated': true});
                this.nameFormControl.markAsTouched();
              }
              this.cdr.detectChanges();
              this.check();
            } else {
              this.sc.lang = this.appEnum.langCodeKeys[0];
            }
            break;
          case 'edit':
            let sc = this.objectManager.get('sc');
            if (sc === undefined) {
              this.backPage();
              return;
            }
            delete sc.skill_cnt;
            delete sc.reg_cnt;
            delete sc.checked;
            this.sc = sc;
            this.org_sc = JSON.parse(JSON.stringify(this.sc));
            this.dialogTitle = 'Edit Simple Classifier';
            break;
          default:
            this.backPage();
            // let message = 'Role is not properly assigned.';
            // this.snackBar.open(message, 'Confirm', {duration: 10000});
            return;
        }
        this.store.dispatch({type: ROUTER_LOADED});
      }).catch((err) => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Something went wrong. [${getErrorString(err)}]`;
        this.store.dispatch({type: ROUTER_LOADED});
      });
    });
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    if (this.check()) {
      delete this.sc.skill_cnt;
      delete this.sc.reg_cnt;
      if (this.role === 'edit') {
        if (!this.isModified()) {
          let ref = this.dialog.open(ErrorDialogComponent);
          ref.componentInstance.title = 'Not Changed.';
          ref.componentInstance.message = `Values are not changed. Please make some modification(s) first.`;
        } else {
          this.openDialog('edit');
        }
      } else {
        this.openDialog('add');
      }
    }
  }

  isModified() {
    return JSON.stringify(this.sc) !== JSON.stringify(this.org_sc);
  }

  onAdd(): void {
    this.grpc.insertSimpleClassifierInfo(this.sc).subscribe(res => {
      this.snackBar.open(`Simple Classifier '${this.sc.name}' created.`, 'Confirm', {duration: 3000});
      this.backPage();
    }, err => {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Failed.';
      ref.componentInstance.message = `Failed to create a Simple Classifier. [${getErrorString(err)})]`;
    });
  }

  onEdit(): void {
    delete this.sc['isChecked'];
    this.grpc.updateSimpleClassifierInfo(this.sc).subscribe(res => {
      this.snackBar.open(`Simple Classifier '${this.sc.name}' updated.`, 'Confirm', {duration: 3000});
      this.backPage();
    }, err => {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Failed.';
      ref.componentInstance.message = `Failed to update the Simple Classifier. [${getErrorString(err)})]`;
    });
  }

  addSkill() {
    if (this.isFormError(this.skillFormControl) || this.newSkill === undefined || this.newSkill === '') {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Please check the Skill.`;
    } else if (this.sc.skill_list.find(item => this.newSkill === item.name)) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Duplicated';
      ref.componentInstance.message = `Duplicate are Skills.`;
    } else {
      this.sc.skill_list.push({
        name: this.newSkill,
        regex: [],
        skill_description: this.skill_description
      });
      this.newSkill = '';
      this.check();
    }
  }

  inputAddSkill(event) {
    if (event.keyCode === 13) { // enter
      this.addSkill();
      event.preventDefault(); // 현재 이벤트의 기본 동작을 중단한다.
    }
  }

  deleteSkill(index) {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete ${this.sc.skill_list[index].name} ?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.sc.skill_list.splice(index, 1);
        this.selectedSkill = undefined;
        this.check();
      }
    });
  }

  addRegEx() {
    if (this.isFormError(this.regExFormControl) || this.newRegExp === undefined || this.newRegExp === '') {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Please check the regEx`;
    } else if (this.selectedSkill.regex.find(item => this.newRegExp === item)) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Duplicate are regEx.`;
    } else {
      this.selectedSkill.regex.push(this.newRegExp);
      this.newRegExp = '';
      this.check();
    }
  }

  inputAddRegEx(event) {
    if (event.keyCode === 13) {
      this.addRegEx();
      event.preventDefault();
    }
  }

  editRegEx(index) {
    let changeRegExps: HTMLInputElement = <HTMLInputElement> (document.getElementsByName('regex-edit')[index]);
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Edit RegEx ${changeRegExps.value} ?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.selectedSkill.regex.splice(index, 1, changeRegExps.value);
      } else {
        changeRegExps.disabled = true;
        changeRegExps.value = this.selectedSkill.regex[index];
      }
    });
  }

  setEditableRegEx(index) {
    let changeRegExps: HTMLInputElement = <HTMLInputElement> (document.getElementsByName('regex-edit')[index]);
    changeRegExps.disabled = false;
  }

  deleteRegEx(index) {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete ${this.selectedSkill.regex[index]} ?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.selectedSkill.regex.splice(index, 1);
        this.check();
      }
    });
  }

  testRegEx(index) {
    let regex = this.selectedSkill.regex[index];
    let regexObj = new RegExp(regex);
    this.matchSet = [];
    for (let s in this.sentences) {
      new Promise((resolve) => {
        let sent: string = this.sentences[s];
        let matchResult: matchType = {
          words: [], range: [-1, -1], skill: '', regex: ''
        };
        let res = regexObj.exec(sent);
        for (let v in res) {
          if (v === '0') {
            matchResult.skill = this.selectedSkill.name;
            matchResult.regex = this.selectedSkill.regex[index];
            matchResult.range = [res['index'], res[0].length + res['index'] - 1];
            matchResult.words.push(res[v]);
          } else if (/\d/.test(v)) {
            matchResult.words.push(res[v]);
          }
        }
        resolve([matchResult, sent, s]);
      }).then((rsList: any[]) => {
        if (rsList) {
          let res: matchType = rsList[0];
          let line: string = rsList[1];
          let indexNum = rsList[2];
          let start: number = res.range[0];
          let end: number = res.range[1];
          if (start === -1 || end === -1) { // matching 된 단어가 없을 때
            switch (indexNum) {
              case '0':
                this.oriSentList[0] = '<span>' + line + '</span>';
                break;
              case '1':
                this.posSentList[0] = '<span>' + line + '</span>';
                break;
              case '2':
                this.nerSentList[0] = '<span>' + line + '</span>';
                break;
              case '3':
                this.posAndNerSentList[0] = '<span>' + line + '</span>';
                break;
              default:
                break;
            }
          } else {
            let matchInfo = {
              skill: 'category : [' + res.skill + ']\r\n', match: '', group: []
            };
            for (let i in res.words) {
              if (i === '0') {
                matchInfo.match = 'match  : [' + res.words[0] + ']\r\n';
              } else {
                matchInfo.group.push('group#' + i + ' : [' + res.words[i] + ']\r\n');
              }
            }
            let match: string = matchInfo.skill + matchInfo.match + '\r\n';
            for (let i in matchInfo.group) {
              match += matchInfo.group[i];
            }
            let tagStr: string = '<span>' + line.substring(0, start) + '</span>';
            tagStr += '<span class="match-word" data-toggle="tooltip" data-html="true" ';
            tagStr += 'data-placement="bottom" title="' + match + '">' + line.substring(start, end + 1) + '</span>';
            tagStr += '<span>' + line.substring(end + 1) + '</span>';
            switch (indexNum) {
              case '0':
                this.oriSentList[0] = tagStr;
                break;
              case '1':
                this.posSentList[0] = tagStr;
                break;
              case '2':
                this.nerSentList[0] = tagStr;
                break;
              case '3':
                this.posAndNerSentList[0] = tagStr;
                break;
              default:
                break;
            }
          }
          this.cdr.detectChanges();
        }
      });
    }
  }

  getButtonName() {
    return this.role === 'add' ? 'Add' : 'Save';
  }

  check() {
    if (this.isFormError(this.nameFormControl) || this.sc.name === undefined) {
      this.submitButton = true;
    } else if (this.sc.lang === undefined) {
      this.submitButton = true;
    } else if (this.sc.skill_list.length === 0) {
      this.submitButton = true;
    } else if (this.sc.skill_list.some(skill => skill.regex.length === 0)) {
      this.submitButton = true;
    } else {
      this.submitButton = false;
      return true;
    }
  }

  openDialog(data) {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `${this.role === 'add' ? 'Add' : 'Edit'} '${this.sc.name}'?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (data === 'add') {
          this.onAdd();
        } else {
          this.onEdit();
        }
      }
    });
  }

  backPage: any = () => {
    this.objectManager.clean('sc');
    // 이전 detail 화면으로 이동합니다
    window.history.back();
  }

  /**
   * Nlu Tagging Result Sentent Input에 입력 후 엔터시 getNluResult호출하도록 하는 함수
   * @param event
   */
  inputAddSentence(event: any): void {
    if (event.keyCode === 13) { // enter
      this.getNluResult();
      event.preventDefault(); // 현재 이벤트의 기본 동작을 중단한다.
    }
  }

  /**
   * NLP3에 analyze, pos,ner,pos&ner TaggedList Grpc 요청하는 함수
   */
  getNluResult(): void {
    let inputTextParam = {
      text: this.nluSentence,
      lang: 'kor',
      split_sentence: true,
      use_tokenizer: false,
      level: 1,
      use_space: false,
      keyword_frequency_level: 0
    }
    this.sentences = [];
    this.oriSentList[0] = (this.nluSentence ? this.nluSentence : '');
    this.sentences.push(this.oriSentList[0]);
    this.posSentList[0] = 'Loading .....';
    this.nerSentList[0] = 'Loading .....';
    this.posAndNerSentList[0] = 'Loading .....';
    this.grpc.analyze(inputTextParam).subscribe(analyzeResult => {
      delete analyzeResult.$metadata;
      // this.posResult = 'Loading.....';
      // this.nerResult = 'Loading.....';
      // this.posNerResult = 'Loading.....';;
      forkJoin(this.grpc.getPosTaggedList(analyzeResult), this.grpc.getNerTaggedList(analyzeResult),
        this.grpc.getPosNerTaggedList(analyzeResult)).subscribe(([posResult, nerResult, posNerResult]) => {
        console.log('!@#fork in!@#');
        // this.posResult = posResult.result;
        // this.nerResult = nerResult.result;
        // this.posNerResult = posNerResult.result;
        this.posSentList[0] = (posResult.result ? posResult.result[0] : undefined);
        this.nerSentList[0] = (nerResult.result ? nerResult.result[0] : undefined);
        this.posAndNerSentList[0] = (posNerResult.result ? posNerResult.result[0] : undefined);
        this.sentences[1] = this.posSentList ? this.posSentList[0] : '';
        this.sentences[2] = this.nerSentList ? this.nerSentList[0] : '';
        this.sentences[3] = this.posAndNerSentList ? this.posAndNerSentList[0] : '';
      }, ([posError, nerError, posNerError]) => {
        if (posError) {
          // this.posResult = 'PosTagging Error';
          this.posSentList[0] = 'Something went wrong in part-of-speech tagging of a sentence.';
        }
        if (nerError) {
          // this.nerResult = 'NerTagging Error';
          this.nerSentList[0] = 'Something went wrong in named entity tagging of a sentence.';
        }
        if (posNerError) {
          // this.posNerResult = 'PosNerTagging Error';
          this.posAndNerSentList[0] = 'Something went wrong in part-of-speech and named entity tagging of a sentence';
        }
      })
    }, analyzeErr => {
      // this.posResult = 'Analyze Error';
      // this.nerResult = 'Analyze Error';
      // this.posNerResult = 'Analyze Error';
      this.posSentList[0] = 'Check NLP Server Connection.';
      this.nerSentList[0] = 'Check NLP Server Connection.';
      this.posAndNerSentList[0] = 'Check NLP Server Connection.';
    })
  }
}
