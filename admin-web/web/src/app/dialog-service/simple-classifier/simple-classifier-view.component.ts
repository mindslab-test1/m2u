import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSidenav, MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';
import {Store} from '@ngrx/store';

import {ROUTER_LOADED, SC_DELETE, SC_UPDATE_ALL} from '../../core/actions';
import {AuthService} from '../../core/auth.service';
import {
  AlertComponent,
  AppEnumsComponent,
  AppObjectManagerService,
  getErrorString,
  MatTableComponent, UploaderButtonComponent
} from '../../shared/index';
import {ConsoleUserApi, GrpcApi} from '../../core/sdk/index';
import {SimpleClassifierPanelComponent} from './simple-classifier-panel.component';
import {MatTableDataSource} from '@angular/material/table';
import * as XLSX from 'xlsx';
import { FileUploader } from 'ng2-file-upload';

type AOA = any[][];
type exelSc = {
  category: string,
  regex: string,
};

type oriSc = {
  name: string,
  regex: Array<string>,
};

type scRow = {
  name: string,
  lang: string,
  skill_list: oriSc[]
}

@Component({
  selector: 'app-simple-classifier-view',
  templateUrl: './simple-classifier-view.component.html',
  styleUrls: ['./simple-classifier-view.component.scss'],
})

export class SimpleClassifierViewComponent implements OnInit, OnDestroy {
  page_title: any; // = 'Dialog Agents';
  page_description: any; // = 'Add Dialog Agent Managers and update information about Dialog Agent Managers.';

  scs: any;
  actions: any;
  table: any;
  filterKeyword: FormControl;
  panelToggleText: string;

  sc = {
    name: undefined,
    lang: undefined,
    description: undefined,
    skill_cnt: undefined,
    reg_cnt: undefined,
    skill_list: [],
    pos_tagging: false,
    named_entity: false,

  };

  data: AOA = [ [1, 2], [3, 4] ];

  subscription = new Subscription();
  fileToUpload: File = null;
  element: HTMLElement;

  @ViewChild('tableComponent') tableComponent: MatTableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('infoPanel') infoPanel: SimpleClassifierPanelComponent;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  dataSource: MatTableDataSource<any>;
  header;
  rows: any[] = [];
  // dialogRef: MatDialogRef<CommonDialogComponent>;

  constructor(private store: Store<any>,
              private router: Router,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    let id = 'sc';
    let roles = this.consoleUserApi.getCachedCurrent().roles;
    this.element = document.getElementsByClassName('fileSelector')[0] as HTMLElement;
    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      upload: {
        icon: 'cloud_upload',
        id: 'uploadFile',
        callback: this.upload,
        text: 'upload',
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      download: {
        icon: 'cloud_download',
        text: 'download',
        callback: this.download,
      }
      // test: {
      //   text: 'Test',
      //   callback: this.test,
      //   hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      // }
    };
    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {attr: 'name', name: 'Name', isSort: true, onClick: this.navigateScDetailView, width: '30%'},
      {attr: 'lang', name: 'Lang', onlyImage: this.getImage},
      {attr: 'temp_skill_list_length', name: '# Skills', isSort: true, width: '20%'},
      {attr: 'reg_cnt', name: '# Reg. Exps', isSort: true, width: '20%'},
    ];

    this.tableComponent.onRowClick = this.fillRowInfo;
    // subscribe store
    this.subscription.add(
      this.store.select('scs')
        .subscribe(scs => {
          if (scs) {
            // 초기 name 순으로 정렬
            scs.sort((a, b) => {
              return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;});
            this.rows = scs;
            this.rows.forEach(row => row.isChecked = false);
            this.whenCheckChanged();
            setTimeout(() => {
              this.fillRowInfo(this.rows[0]);
            }, this.rows.length > 0 ? 500 : 0);
          }
        })
    );

    this.grpc.getSimpleClassifierAllList().subscribe(
      result => {
        this.scs = result.sc_list;
        this.scs.forEach(sc => {
          sc.temp_skill_list_length = sc.skill_list.length;
          sc.reg_cnt = sc.skill_list.map(skill => skill.regex.length).reduce((t, n) => t + n);
        });
        this.scs.sort((a, b) => a.name > b.name);
        this.store.dispatch({type: SC_UPDATE_ALL, payload: this.scs});
        this.store.dispatch({type: ROUTER_LOADED});
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      }
    );

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges
      .debounceTime(300)
      .distinctUntilChanged()
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  /** action callbacks **/
  add: any = () => {
    this.router.navigateByUrl('dialog-service/sc-upsert#add');
  }

  edit: any = () => {
    let items = this.rows.filter(row => row.isChecked);
    delete items[0].isChecked;
    delete items[0].temp_skill_list_length;
    this.objectManager.set('sc', items[0]);
    this.router.navigateByUrl('dialog-service/sc-upsert#edit');
  }

  upload: any = () => {
    console.log('upload');
    this.data = [];
    let elem: HTMLInputElement = document.getElementById('selectFile') as HTMLInputElement;
    elem.click();
  }

  download: any = () => {
    console.log('download');
    this.fileDownload();
  }

  delete: any = () => {
    let names = this.rows.filter(row => row.isChecked).map(row => row.name);
    if (names.length === 0) {
      this.snackBar.open('Select items to delete.', 'Confirm', {duration: 3000});
      return;
    }

    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return;
        }
        let keyList = names.map(name => {
          return {name: name};
        })
        this.grpc.deleteSimpleClassifier(keyList).subscribe(
          res => {
            let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
            this.store.dispatch({type: SC_DELETE, payload: deletedItems});
            this.snackBar.open(`Simple Classifier '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
          },
          err => {
            let message = `Failed to Delete Simple Classifier. [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      });
  }

  togglePanel: any = () => {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  }

  fillRowInfo: any = (row: any) => {
    this.infoPanel.scInfo = row;
    if (row === undefined) {
      this.sidenav.close();
      this.panelToggleText = 'Show Info Panel';
    } else {
      this.sidenav.open();
      this.panelToggleText = 'Hide Info Panel';
    }
  }

  whenCheckChanged: any = () => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        this.actions.download.disabled = true;
        break;
      case 1:
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = false;
        this.actions.download.disabled = false;
        break;
      default:
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = true;
        this.actions.download.disabled = false;
        break;
    }
  }

  onFileChange(evt: any) {
    let fullName: string;
    let obm = {};
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) {
      let message = `Cannot use multiple file'`;
      this.snackBar.open(message, 'Confirm', { duration: 4000 });
      throw new Error('Cannot use multiple files');
    }
    new Promise((resolve) => {
      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        fullName = target.files[0].name;
        let part = fullName.split('.')[0].split(/__/);
        let fileName = part[0];
        let fileLang = part[1];
        if ( fileName === undefined || (fileLang !== '0' && fileLang !== '1') ) {
          let message = `Cannot upload file. Check file format!'`;
          this.snackBar.open(message, 'Confirm', { duration: 4000 });
          throw new Error('Cannot upload file. Check file format!');
        }
        /* read workbook */
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
        /* grab first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];

        /* save data */
        this.data = <AOA>(XLSX.utils.sheet_to_json(ws));
        obm = {
          name: fileName, lang: fileLang
        };
        resolve();
      };
      reader.readAsBinaryString(target.files[0]);
    }).then(() => {

      let skillList = {};
      let skill: string;
      let scList: oriSc[] = [];
      this.data.forEach(elem => {
        skill = elem['category'];
        if ( !skillList[skill] ) {
          skillList[skill] = [];
        }
        skillList[skill].push(elem['regex']);
      });

      Object.keys(skillList).map(key => {
        let values: string[] = [];
        skillList[key].forEach(x => values.push(x));
        let one: oriSc = {name: key, regex: values};
        scList.push(one);
      });
      obm['skill_list'] = scList;
      this.objectManager.set('sc', obm);
      this.router.navigateByUrl('dialog-service/sc-upsert#add')
    });
  }

  delay(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  async Process(scs: scRow) {
      let scList: Array<oriSc>;
      let lang = scs.lang === 'kor' ? '0' : '1';
      let name = scs.name + '__' + lang + '.xlsx';
      scList = scs.skill_list;
      let skillOne: exelSc;
      let skillList: Array<exelSc> = [];
      scList.forEach(sc => {
        sc.regex.forEach(sk => {
          skillOne = { category: sc.name, regex: sk };
          skillList.push(skillOne);
        });
      });
      const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(skillList);
      const workbook: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
      XLSX.writeFile(workbook, name, { type: 'file', bookType: 'xlsx' });
      await this.delay(10);
  }

  async fileDownload() {
    let items = this.rows.filter(row => row.isChecked);
    for ( const scs of items ) {
      await this.Process(scs);
    }
    let message = `Selected simple classifier models download Successfully.`;
    this.snackBar.open(message, 'Confirm', { duration: 4000 });
  }

  navigateScDetailView: any = (row: any) => {
    delete row.isChecked;
    delete row.temp_skill_list_length;
    this.objectManager.set('sc', row);
    this.router.navigateByUrl('dialog-service/sc-detail');
  }

  getImage: any = (row: any) => {
    if (row.lang === 'kor') {
      return '/assets/img/korean_05.png';
    } else if (row.lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
