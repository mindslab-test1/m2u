import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar, MatDialog} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';

import {Subscription} from 'rxjs/Subscription';
import {ROUTER_LOADED, SC_DELETE} from '../../core/actions';
import {AuthService} from '../../core/auth.service';
import {AppObjectManagerService, AlertComponent, AppEnumsComponent, getErrorString} from '../../shared/index';
import {ConsoleUserApi, GrpcApi} from '../../core/sdk/index';

@Component({
  selector: 'app-simple-classifier-detail',
  templateUrl: './simple-classifier-detail.component.html',
  styleUrls: ['./simple-classifier-detail.component.scss'],
})

export class SimpleClassifierDetailComponent implements OnInit {
  sc: any;
  title: any;
  actions: any;
  subscription = new Subscription();

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit() {
    let id = 'sc';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      }
    };
    this.sc = this.objectManager.get('sc');
    if (this.sc === undefined) {
      this.backPage();
      return;
    }
    this.title = this.sc.name + ':' + this.appEnum.langCode[this.sc.lang];
    this.store.dispatch({type: ROUTER_LOADED});
  }

  edit: any = () => {
    this.router.navigateByUrl('dialog-service/sc-upsert#edit');
  }

  delete: any = () => {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete '${this.sc.name}?`;
    ref.afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return;
        }
        this.grpc.deleteSimpleClassifier([{name: this.sc.name}]).subscribe(
          res => {
            let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
            this.snackBar.open(`Simple Classifier '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
            this.backPage();
          },
          err => {
            let message = `Failed to Delete Simple Classifier. [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      });
  }

  backPage: any = () => {
    this.objectManager.clean('sc');
    this.router.navigate(['../sc'], {relativeTo: this.route});
  }
}

