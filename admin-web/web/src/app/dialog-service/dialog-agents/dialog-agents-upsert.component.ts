import {Component, OnInit, ViewChild} from '@angular/core';
import {
  MatDialog,
  MatSelect,
  MatSnackBar,
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {Store} from '@ngrx/store';
import {GrpcApi} from '../../core/sdk/index';
import {ROUTER_LOADED} from '../../core/actions';
import {
  AppObjectManagerService, AlertComponent, AppEnumsComponent, FormErrorStateMatcher, getErrorString
} from '../../shared/index';
import {ErrorDialogComponent} from '../../shared/dialogs/error-dialog/error-dialog.component';


@Component({
  selector: 'app-dialog-agent-upsert',
  templateUrl: './dialog-agents-upsert.component.html',
  styleUrls: ['./dialog-agents-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class DialogAgentsUpsertComponent implements OnInit {

  dialogTitle: string;
  da = {
    da_info: {
      name: undefined,
      description: undefined,
      version: undefined,
      da_executable: undefined,
      type: undefined,
      da_spec: undefined,
      da_prod_spec: undefined,
      full_command: undefined,
      extra_command: undefined,
      runtime_env: undefined,
      runtime_env_version: undefined,
      require_user_privacy: undefined
    },
    daai_list: []
  };
  org_da: any;
  role: string;
  daExecutables: any;
  damList: any;
  daList = [];
  selectedExecutable: any;
  userAttrTable: any;
  submitButton: any;

  nameFormControl = new FormControl('', [
    Validators.required,
    () => !!this.da.da_info.name && this.daList.find(da => da === this.da.da_info.name)
      ? {'duplicated': true} : null
  ]);

  @ViewChild('executableSelect') executableSelect: MatSelect;

  constructor(private store: Store<any>,
              private snackBar: MatSnackBar,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              public appEnum: AppEnumsComponent,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    this.userAttrTable = {
      header: [
        {attr: 'name', name: 'Name', sortable: true},
        {attr: 'type', name: 'Type', sortable: true},
        {attr: 'desc', name: 'Description', sortable: true},
      ],
      rows: [],
    };

    this.route.fragment.subscribe(
      role => {
        this.role = role;
        switch (this.role) {
          case 'add':
            this.submitButton = true;
            this.dialogTitle = 'Add Dialog Agent';
            break;
          case 'edit':
            let da = this.objectManager.get('da');
            if (da === undefined) {
              this.backPage();
              return;
            }
            delete da.da_info.lang;
            delete da.da_info.required;
            this.da = da;
            this.org_da = JSON.parse(JSON.stringify(this.da));
            this.dialogTitle = 'Edit Dialog Agent';
            // this.updateUserAttributes();
            break;
          default:
            this.backPage();
            // let message = 'Role is not properly assigned.';
            // this.snackBar.open(message, 'Confirm', {duration: 10000});
            return;
        }
        this.retrieveData();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Something went wrong. [${getErrorString(err)}]`;
        this.store.dispatch({type: ROUTER_LOADED});
      });
  }

  retrieveData() {
    this.grpc.getDialogAgentManagerAllList().subscribe(
      result => {
        this.damList = result.damwd_list;
        if (this.damList.length < 1) {
          this.snackBar.open('Dialog Agent Manager is not exist. Please add it first.', 'Confirm', {duration: 10000});
          this.backPage();
          return;
        }
        this.damList.forEach(dam => {
          dam.ip_port = dam.dam_info.ip + ':' + dam.dam_info.port;
          dam.disabled = true;
          dam.checked = false;
        });
        this.damList.sort((a, b) => a.dam_info.name > b.dam_info.name);
        this.grpc.getExecutableDA().subscribe(
          result2 => {
            result2.dae_list.forEach(dae => {
              dae.lang = this.appEnum.runtimeEnvironment[dae.runtime_env];
              dae.version = '1.0';
            });
            this.daExecutables = result2.dae_list;
            if (this.role === 'edit') {
              let executable = this.daExecutables.find(item => item.name === this.da.da_info.da_executable);
              if (executable) {
                this.selectedExecutable = executable;
                this.damList.forEach(dam => {
                  dam.disabled = executable.distributions.indexOf(dam.dam_info.name) === -1;
                  let daai = this.da.daai_list.find(_daai => dam.dam_info.name === _daai.dam_name);
                  dam.checked = daai ? daai.active : false;
                });
              } else {
                let message = `Executable '${this.da.da_info.da_executable}' is not exist.`;
                this.snackBar.open(message, 'Confirm', {duration: 10000});
              }
            }
            this.store.dispatch({type: ROUTER_LOADED});
          },
          err => {
            let message = `Something went wrong. [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
            this.backPage();
          });
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.backPage();
      });

    this.grpc.getDialogAgentAllList().subscribe(
      allList => {
        this.daList = [];
        allList.da_list.map(da => this.daList.push(da.da_info.name));
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000})
      });
  }

  onSelectionChanged($event) {
    this.da.da_info.da_executable = this.selectedExecutable.name;
    this.da.da_info.version = this.selectedExecutable.version;
    this.da.da_info.type = this.selectedExecutable.type;
    this.da.da_info.da_spec = this.selectedExecutable.da_spec;
    this.da.da_info.da_prod_spec = this.selectedExecutable.da_prod_spec;
    this.da.da_info.full_command = this.selectedExecutable.full_command;
    this.da.da_info.runtime_env = this.selectedExecutable.runtime_env;
    this.da.da_info.require_user_privacy = this.selectedExecutable.require_user_privacy;

    this.damList.forEach(dam => {
      dam.disabled = this.selectedExecutable.distributions.indexOf(dam.dam_info.name) === -1;
      dam.checked = false;
    });
    this.check();
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    if (this.check()) {
      if (this.role === 'edit' && !this.isModified()) {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Not Changed';
        ref.componentInstance.message = `Values are not changed. Please make some modification(s) first.`;
      } else {
        if (this.role === 'add') {
          this.openDialog('add');
        } else {
          this.openDialog('edit');
        }
      }
    }
  }

  isModified() {
    this.aggregateDialogAgentInfo();
    return Object.keys(this.da.da_info).some(key =>
      this.da.da_info[key] !== this.org_da.da_info[key]) ||
      this.da.daai_list.some(daai =>
        this.org_da.daai_list.findIndex(org_daai =>
          Object.keys(daai).every(key => daai[key] === org_daai[key])
        ) === -1
      );
  }

  aggregateDialogAgentInfo() {
    this.da.daai_list = [];
    this.damList.forEach(dam => {
      if (!dam.disabled) {
        this.da.daai_list.push({
          da_name: this.da.da_info.name,
          dam_name: dam.dam_info.name,
          active: dam.checked
        });
      }
    });
  }

  onAdd(): void {
    this.aggregateDialogAgentInfo();
    this.grpc.insertDialogAgentInfo(this.da).subscribe(
      res => {
        this.snackBar.open(`Dialog Agent '${this.da.da_info.name}:${this.da.da_info.version}' created.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Failed to create a Dialog Agent. [${getErrorString(err)}]`;
      }
    );
  }

  onEdit(): void {
    delete this.da['isChecked'];
    delete this.da.da_info['da_prod_spec_enum'];
    this.grpc.updateDialogAgentInfo(this.da).subscribe(
      res => {
        this.snackBar.open(`Dialog Agent '${this.da.da_info.name}:${this.da.da_info.version}' updated.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Failed to update the Dialog Agent. [${getErrorString(err)}]`;
      }
    );
  }

  getButtonName() {
    return this.role === 'add' ? 'Add' : 'Save';
  }

  check() {
    console.log(this.da.da_info.name, this.selectedExecutable, this.da.da_info.da_prod_spec);
    if (this.isFormError(this.nameFormControl)) {
      this.submitButton = true;
    } else if (this.da.da_info.name === undefined || this.selectedExecutable === undefined || this.da.da_info.da_prod_spec === undefined) {
      this.submitButton = true;
    } else {
      this.submitButton = false;
      return true;
    }
  }

  openDialog(data) {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = data === 'add' ? `Add '${this.da.da_info.name}?'` : `Save '${this.da.da_info.name}?'`
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (data === 'add') {
          this.onAdd();
        } else {
          this.onEdit();
        }
      }
    });
  }

  backPage: any = () => {
    this.objectManager.clean('da');
    this.location.back();
  }
}
