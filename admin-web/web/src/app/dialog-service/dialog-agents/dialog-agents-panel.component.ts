import {Component, OnInit} from '@angular/core';
import {AppEnumsComponent} from '../../shared/index';

@Component({
  selector: 'app-da-panel',
  templateUrl: './dialog-agents-panel.component.html',
  styleUrls: ['./dialog-agents-panel.component.scss']
})
export class DialogAgentsPanelComponent implements OnInit {
  daInfo: any;

  constructor(private appEnum: AppEnumsComponent) {
  }

  ngOnInit() {
  }

}
