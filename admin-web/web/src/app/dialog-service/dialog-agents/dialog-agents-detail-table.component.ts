import {Component, Input} from '@angular/core';
import {TableComponent} from '../../shared/components/table/table.component';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-dialog-agent-detail-table',
  templateUrl: './dialog-agents-detail-table.component.html',
  styleUrls: ['../../shared/components/table/table.component.scss', './dialog-agents-detail-table.component.scss']
})
export class DialogAgentsDetailTableComponent extends TableComponent {
}
