import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar, MatDialog} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs/Subscription';

import {ROUTER_LOADED, DAI_UPDATE_ALL} from '../../core/actions';
import {AuthService} from '../../core/auth.service';
import {AppObjectManagerService, AlertComponent, AppEnumsComponent, getErrorString} from '../../shared/index';
import {ConsoleUserApi, GrpcApi} from '../../core/sdk/index';


@Component({
  selector: 'app-dialog-agents-detail',
  templateUrl: './dialog-agents-detail.component.html',
  styleUrls: ['./dialog-agents-detail.component.scss'],
})

export class DialogAgentsDetailComponent implements OnInit {
  title: string;
  da: any;
  actions: any;
  daDamTable: any;
  daChatbotTable: any;
  runtimeTable: any;
  subscription = new Subscription();

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              public appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi,
              private location: Location) {
  }

  ngOnInit() {
    let daObj = this.objectManager.get('da');

    if (daObj === undefined) {
      this.backPage();
      return;
    }
    this.da = daObj.da_info;
    this.title = `DA:${this.da.name}(${this.da.lang})`;

    let id = 'da';
    let roles = this.consoleUserApi.getCachedCurrent().roles;
    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'DELETE',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      }
      // {
      //   icon: 'sync_disabled',
      //   text: 'Disable',
      //   callback: this.disable,
      //   hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      // }
    };

    this.runtimeTable = {
      header: [
        {attr: 'name', name: 'Name', sortable: true},
        {attr: 'type', name: 'Type', sortable: true},
        {attr: 'desc', name: 'Description', sortable: true},
        {attr: 'default_value', name: 'Default Value', sortable: true},
        {attr: 'required', name: 'Required', sortable: true},
      ],
      rows: [],
    };

    this.daDamTable = {
      header: [
        {attr: 'dam_info.name', name: 'Name', sortable: true},
        {attr: 'dam_info.active', name: 'Active', sortable: true, asIcon: true},
        {attr: 'dam_info.port', name: 'Port'},
      ],
      rows: [],
    };

    this.daChatbotTable = {
      header: [
        {attr: 'chatbot_name', name: 'Chatbot', sortable: true},
        {attr: 'dai_name', name: 'DA Instance'},
        {attr: 'damState', name: 'DAM'}
        // {attr: 'state', name: 'State', asIcon: true},
        // {attr: 'action', name: 'Action'}
      ],
      rows: [],
    };


    let chain = new Promise((resolve, reject) =>
      this.grpc.getDialogAgentActivationInfoList(this.da.name).subscribe(
        daDam => {
          this.daDamTable.rows = daDam.damwd_list;
          resolve();
        },
        err => reject(err)));

    chain = chain.then(() => new Promise((resolve, reject) =>
      this.grpc.getDialogAgentInfo(this.da.name).subscribe(
        da_with_dai_info => resolve(da_with_dai_info.chatbot_dai_list),
        err => reject(err)
      )));

    chain = chain.then((chatbot_dai_list: any[]) => new Promise((resolve, reject) =>
      this.grpc.getDialogAgentInstanceAllList().subscribe(
        result => {
          chatbot_dai_list.forEach(chatbot_dai => {
            let dais = result.dai_list.filter(dai => (dai.chatbot_name === chatbot_dai.chatbot_name)
              && (dai.name === chatbot_dai.dai_name));
            chatbot_dai.damState = [];
            dais[0].dai_exec_info.forEach(dai => {
              chatbot_dai.damState.push({damName: dai.dam_name, damActive: dai.active, damCnt: dai.cnt});
              this.store.dispatch({type: DAI_UPDATE_ALL, payload: dais});
            });
          });
          this.daChatbotTable.rows = chatbot_dai_list;
          resolve();
        },
        err => reject(err))));

    chain = chain.then((chatbot_dai_list: any[]) => new Promise((resolve, reject) =>
      this.grpc.getRuntimeParameters({da_name: this.da.name}).subscribe(
        result => {
          result.params.forEach(param => {
            if (param.type === 'DATA_TYPE_AUTH') {
              param.default_value = '******';
            }
            param.type = this.appEnum.dataType[param.type];
          });
          this.runtimeTable.rows = result.params;
          this.store.dispatch({type: ROUTER_LOADED});
          resolve();
        },
        err => reject(err))));

    chain.catch(err => {
      this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  edit: any = () => {
    this.objectManager.set('dam', this.da);
    this.router.navigateByUrl('dialog-service/da-upsert#edit');
  }

  delete: any = () => {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete '${this.da.name}?`;
    ref.afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return;
        }
        this.grpc.deleteDialogAgent([{name: this.da.name}]).subscribe(
          res => {
            let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
            this.backPage();
            this.snackBar.open(`Dialog Agent '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
          },
          err => {
            this.snackBar.open(getErrorString(err, `Failed to Delete Dialog Agent '${this.da.name}'.`)
              , 'Confirm', {duration: 10000});
          }
        );
      });
  }

  backPage: any = () => {
    this.objectManager.clean('da');
    this.location.back();
  }
}
