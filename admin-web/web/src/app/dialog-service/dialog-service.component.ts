import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../core/actions';


@Component({
  selector: 'app-dialog-service',
  templateUrl: './dialog-service.component.html',
  styleUrls: ['./dialog-service.component.scss']
})

export class DialogServiceComponent implements OnInit, OnDestroy {

  // subscriptions: Subscription;

  constructor(// private store: Store<any>,
              // private router: Router,
  ) {
  }

  ngOnInit() {
    // this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
  }

}
