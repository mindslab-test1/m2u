import {AfterViewInit, Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {MatSnackBar, MatDialog} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Store} from '@ngrx/store';

import {ROUTER_LOADED} from '../../core/actions';
import {AuthService} from '../../core/auth.service';
import {AppObjectManagerService, AlertComponent} from '../../shared/index';
import {ConsoleUserApi, GrpcApi} from '../../core/sdk/index';
import {AppEnumsComponent} from '../../shared/values/app.enums';
import {ErrorDialogComponent} from '../../shared/dialogs/error-dialog/error-dialog.component';
import {getErrorString} from '../../shared/values/error-string';

@Component({
  selector: 'app-intent-finder-policy-detail',
  templateUrl: './intent-finder-policy-detail.component.html',
  styleUrls: ['./intent-finder-policy-detail.component.scss'],
})
/**
 * IntentFinderPolicy Detail 화면
 */
export class IntentFinderPolicyDetailComponent implements OnInit, AfterViewInit {
  actions: any;
  itfp: any;

  stepBeforeClickData: HTMLInputElement;
  stepCurrentClickData: HTMLInputElement;

  // 현재 선택된 step의 category 목록
  selectedStepCategory: any = [];

  // categories를 제외한 화면상 보여줄 데이터
  customScriptData: any;

  constructor(private store: Store<any>,
              private objectManager: AppObjectManagerService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private consoleUserApi: ConsoleUserApi,
              private grpc: GrpcApi,
              private cdr: ChangeDetectorRef,
              public appEnum: AppEnumsComponent,
              private location: Location) {
  }

  ngOnInit(): void {
    this.itfp = this.objectManager.get('itfp');
    if (this.itfp.name === undefined) {
      this.backPage();
      return;
    }

    let id = 'itf';
    let roles = this.consoleUserApi.getCachedCurrent().roles;
    this.settingUIStep();
    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
    };
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * 화면이 load 된 이후에, 첫 번째 step에 대한 카테고리를 화면에 보여준다.
   */
  ngAfterViewInit(): void {
    this.getCategories(this.itfp.steps[0], 0);
    this.cdr.detectChanges();
  }

  /**
   * IntentFinderPolicy edit 화면으로 이동한다.
   */
  edit: any = () => {
    this.router.navigateByUrl('dialog-service/itfp-upsert#edit');
  };

  /**
   * 현재 보고 있는 IntentFinderPolicy를 삭제한다.
   */
  delete: any = () => {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete '${this.itfp.name}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      } else {
        let KeyList = [
          {name: this.itfp.name}
        ];
        this.grpc.deleteIntentFinderPolicies(KeyList).subscribe(
          res => {
            this.snackBar.open(`Selected Intent Finder Policies Successfully Deleted.`,
              'Confirm', {duration: 3000});
            this.router.navigate(['../itfp'], {relativeTo: this.route});
          },
          err => {
            let eref = this.dialog.open(ErrorDialogComponent);
            eref.componentInstance.title = 'Failed';
            eref.componentInstance.message = `Failed to Delete a Intent Finder Policies.  [${getErrorString(err)}]`;
            return;
          });
      }
    });
  };

  /**
   *  HTML로 image가 들어있는 url을 리턴해준다.
   * @returns {string} => {폴더 디렉토리 경로}
   */
  getImageUrl(): string {
    if (this.itfp.lang === 'ko_KR') {
      return '/assets/img/korean_05.png';
    } else if (this.itfp.lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  };

  /**
   * 기존 IntentFinderPolicy steps에 화면에서만 쓰는 변수를 추가 후 세팅하여,
   * 사용자에게 Step 과 Hint 모델정보를 보여준다.
   */
  settingUIStep(): void {
    this.itfp.steps.forEach( (step, index) => {
      step['seq'] = index + 1;

      // UI에 Skill, Hint 여부 표시 하기 위하여 세팅
      if (step.run_style === 'IFC_SKILL_FOUND_RETURN'
        || step.run_style === 'IFC_SKILL_FOUND_COLLECT_HINTS') {
        step.stepRunStyle = 'S';
        step.is_hint = false;
      } else if (step.run_style === 'IFC_HINT') {
        step.stepRunStyle = 'H';
        step.is_hint = true;
      }

      // UI에 Step에 등록된 Model 정보 표시
      switch (step.test_rule) {
        case 'custom_script_mod':
          step.type = 'CLM_CLASSIFY_SCRIPT';
          step.stepType = `<CustomScript - ${step.custom_script_mod.filename}>`;
          break;
        case 'pcre_mod':
          step.type = 'CLM_PCRE';
          step.stepType = `<PCRE - ${step.pcre_mod.model}>`;
          break;
        case 'dnn_cl_mod':
          step.type = 'CLM_DNN_CLASSIFIER';
          step.stepType = `<DNN - ${step.dnn_cl_mod.model}>`;
          break;
        case 'hmd_mod':
          step.type = 'CLM_HMD';
          step.stepType = `<HMD - ${step.hmd_mod.model}>`;
          break;
        case 'final_rule':
          step.type = 'CLM_FINAL_RULE';
          step.stepType = `<Final rule>`;
          break;
      }
    });
  }

  /**
   * IntentFinderPolicy의 steps에서 현재 선택된 step에 대한 카테고리를 보여준다.
   * step 클릭시 선택된 step만 색상을 변경해준다.
   * @param step => 현재 클릭한 step의 정보
   * @param {number} index => {step list에서 선택한 step에 대한 index 값}
   */
  getCategories(step: any, index: number): void {
    this.stepBeforeClickData = this.stepCurrentClickData;
    if (this.stepBeforeClickData !== undefined) {
      this.stepBeforeClickData.style.backgroundColor = '#ffffff';
    }
    this.stepCurrentClickData = <HTMLInputElement> document.getElementsByName('step')[index];
    this.stepCurrentClickData.style.backgroundColor = '#c1d9ff';

    this.selectedStepCategory = step.categories;

    if (step.custom_script_mod !== null) {
      this.customScriptData = step.custom_script_mod;
    }
  }

  /**
   * IntentFinderPolicy View 화면으로 이동한다.
   */
  backPage: any = () => {
    this.location.back();
  };
}
