import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {getErrorString, MatTableComponent} from '../../shared/index';
import {GrpcApi} from '../../core/sdk/index';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-chatbots-dainstances-panel',
  templateUrl: './chatbots-dainstance-panel.component.html',
  styleUrls: ['./chatbots-dainstance-panel.component.scss']
})
export class ChatbotsDainstancesPanelComponent implements OnInit {
  daiExecInfos: any;
  table: any;
  daiName: any;
  running: any;
  stop: any;


  dataSource: MatTableDataSource<any>;
  header;
  rows: any[] = [];
  @ViewChild('tableComponent') tableComponent: MatTableComponent;

  constructor(private grpc: GrpcApi,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.header = [
      {attr: 'dam_name', name: 'DAM', isSort: true},
      {attr: 'pid', name: 'PID'},
      {attr: 'port', name: 'Port', isSort: true},
      // {attr: 'status', name: 'Status', asIcon: true},
    ];
  }

  setContent(dai: any) {
    if (dai !== undefined) {
      this.grpc.getDialogAgentInstanceResourceList(dai.dai_id).subscribe(
        result => {
          let daiExecInfos = dai.dai_exec_info;
          let dairs = result.dair_list;
          daiExecInfos.forEach(info => {
            info.instances = [];
            dairs.filter(row => row.dam_name === info.dam_name).forEach(ele => {
              info.instances.push({pid: ele ? ele.pid : '-', port: ele ? ele.server_port : '-'});
            });
          });
          this.running = daiExecInfos.reduce((t, n) => t + n.key.length, 0);
          this.stop = daiExecInfos.reduce((t, n) => t + n.cnt, 0) - this.running;
          this.daiName = dai.name;
          this.rows = [];
          daiExecInfos.forEach(dai => {

            dai.instances.forEach(ins => {
              let row = {
                'dam_name': dai.dam_name,
                'pid': ins.pid,
                'port': ins.port
              }
              this.rows.push(row);
            })
          })
          this.daiExecInfos = daiExecInfos;
        },
        err => {
          let message = `Something went wrong. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    }
  }
}


