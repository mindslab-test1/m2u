import {
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatListOption,
  MatSelectionList,
  MatSelectionListChange,
  MatSnackBar,
  ShowOnDirtyErrorStateMatcher,
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {
  AppEnumsComponent,
  AppObjectManagerService,
  FormErrorStateMatcher
} from '../../shared/index';
import {GrpcApi} from '../../core/sdk/index'
import {ROUTER_LOADED} from '../../core/actions';
import {getErrorString} from '../../shared/values/error-string';
import {POSITIVE_INTEGER_REGEX, VARIABLE_REGEX} from '../../shared/values/values';
import {AlertComponent} from '../../shared/dialogs/alert/alert.component';


@Component({
  selector: 'app-chatbots-upsert',
  templateUrl: './chatbots-upsert.component.html',
  styleUrls: ['./chatbots-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

/**
 * Chatbot 등록 수정 Component
 */
export class ChatbotsUpsertComponent implements OnInit {
  informationGroup: FormGroup;
  itfGroup: FormGroup;
  sttModelGroup: FormGroup;
  title: string;
  role: string;

  chatbotName: string;
  chatbotTitle: string;
  sessionTimeoutStr: string;

  itfp = null;
  itfiList = [
    {
      name: 'none',
      ip: '',
      port: '',
      description: '',
      policy_name: ''
    }
  ];
  chatbotNameList = [];
  sttModelList = [];
  doneFlag = false;
  originalChatbot: any;

  itfpList = [];

  @ViewChild(MatSelectionList) sttModels: MatSelectionList;
  @ViewChildren('options') options: QueryList<MatListOption>;

  constructor(private store: Store<any>,
              private route: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar,
              public dialog: MatDialog,
              private cdr: ChangeDetectorRef,
              private appEnum: AppEnumsComponent,
              private _formBuilder: FormBuilder,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi) {
    this.sessionTimeoutStr = '600';
    this.informationGroup = this._formBuilder.group({
      name: [undefined,
        [
          Validators.required,
          () => !new RegExp(VARIABLE_REGEX).test(this.chatbotName) ? {'variable': true} : null,
          () => this.getDuplicatedName() ? {'duplicated': true} : null
        ]
      ],
      title: [undefined,
        [
          Validators.required,
          () => !new RegExp(VARIABLE_REGEX).test(this.chatbotName) ? {'variable': true} : null,
        ]
      ],
      description: '',
      session_timeout: ['600',
        [
          Validators.required,
          () => !new RegExp(POSITIVE_INTEGER_REGEX).test(this.sessionTimeoutStr) ?
            {'isPositiveInteger': true} : null
        ]
      ],
      external_system_info: '',
      active: true,
    });

    this.itfGroup = this._formBuilder.group({
      intent_finder_policy: ['none', Validators.required],
      default_skill: [undefined, Validators.required],
    });

    this.sttModelGroup = this._formBuilder.group({
      stt_models: [
        []
      ]
    });
  }

  getDuplicatedName() {
    if (this.role === 'edit' && this.originalChatbot) {
      this.chatbotNameList.splice(this.chatbotNameList.indexOf(this.originalChatbot.name));
    }
    return this.chatbotNameList.find(chatbotName => chatbotName === this.chatbotName);
  }

  ngOnInit(): void {
    this.checkDuplicateChatbotName();
    this.route.fragment.subscribe(
      role => {
        this.role = role;
        switch (this.role) {
          case 'add':
            this.title = 'Add Chatbot';
            break;
          case 'edit':
            let cb = this.objectManager.get('cb');
            if (cb === undefined) {
              this.backPage();
              return;
            }
            this.grpc.getChatbotInfo(cb.name).subscribe(
              chatbot => {
                delete chatbot.auth_provider;
                delete chatbot.detail;
                delete chatbot.input;
                delete chatbot.output;
                delete chatbot.use_invocation_trigger;
                delete chatbot.invocation_keywords;
                delete chatbot.launch_skill;
                delete chatbot.open_skills;

                this.originalChatbot = JSON.parse(JSON.stringify(chatbot));

                this.informationGroup.patchValue({
                  name: chatbot.name,
                  title: chatbot.title,
                  description: chatbot.description,
                  session_timeout: chatbot.session_timeout,
                  external_system_info: chatbot.external_system_info,
                  active: chatbot.active,
                });

                this.informationGroup.get('name').disable();

                this.itfGroup.patchValue({
                  intent_finder_policy: chatbot.intent_finder_policy,
                  default_skill: chatbot.default_skill,
                });

                this.getIntentFinderInstanceList().then((itfiList: any[]) => {
                  let itfpSet = new Set();
                  itfiList.map(itfi => {
                    itfpSet.add(itfi.policy_name);
                  });

                  this.itfpList = ['none'];
                  this.itfpList = this.itfpList.concat(Array.from(itfpSet));
                  this.getIntentFinderPolicyInfo(chatbot.intent_finder_policy);
                });

                this.sttModelGroup.patchValue({
                  stt_models: chatbot.stt_models
                });

                this.options.changes.subscribe((r) => {
                  this.options.toArray().forEach(option => {
                    this.sttModelGroup.get('stt_models').value.some(sttModel => {
                      if (option.value.model === sttModel.model && option.value.lang === sttModel.lang
                        && option.value.sample_rate === sttModel.sample_rate) {
                        option.selected = true;
                        this.cdr.detectChanges();
                        return true;
                      }
                    });
                  });
                });
                this.isDone();
              },
              err => {
                let msg = `Something went wrong. [${getErrorString(err)}]`;
                this.snackBar.open(msg, 'Confirm', {duration: 10000});
                this.store.dispatch({type: ROUTER_LOADED});
              }
            );
            this.title = 'Edit Chatbot';
            break;
          default:
            this.backPage();
            let message = 'Role is not properly assigned.';
            this.snackBar.open(message, 'Confirm', {duration: 10000});
            return;
        }
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });
    // this.sttModels.
    this.sttModels.selectionChange.subscribe((s: MatSelectionListChange) => {
      let sttModelList: any[] = this.sttModelGroup.get('stt_models').value;

      if (s.option.selected) {
        sttModelList.push(s.option.value);
      } else {
        sttModelList.splice(sttModelList.indexOf(s.option.value), 1);
      }
      this.cdr.detectChanges();
    });

  }

  /**
   * MatStepper의 단계가 변화될 때 이벤트를 감지한다.
   * 각각의 step에 맞게 grpc를 통하여 데이터를 불러온다.
   * 1 : IntentFinderInstance
   * 2 : SttModels
   * @param event
   */
  changeStep(event: any) {
    if (event.selectedIndex === 1) {
      this.getIntentFinderInstanceList().then((itfiList: any[]) => {
        let itfpSet = new Set();
        itfiList.map(itfi => {
          itfpSet.add(itfi.policy_name);
        });

        this.itfpList = ['none'];
        this.itfpList = this.itfpList.concat(Array.from(itfpSet));
      });
    } else if (event.selectedIndex === 2) {
      this.getSttModels();
    }
  }

  /**
   * chatbotName 의 중복 예외처리를 위하여 chatbotNameList에 chatbot List의 Name을 저장한다.
   */
  checkDuplicateChatbotName() {
    this.grpc.getChatbotAllList().subscribe(
      chatbots => {
        this.chatbotNameList = [];
        chatbots.chatbot_list.map(chatbot => this.chatbotNameList.push(chatbot.name));
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        console.error('err', err.message);
      });
  }

  /***
   * IntentFinderInstance를 클릭하였을 때, 해당 IntentFinderPolicy의 정보를 불러온다.
   */
  selectItfPolicy(itfp: any) {
    this.itfGroup.patchValue({
      intent_finder_policy: itfp
    });

    this.itfGroup.patchValue({
      default_skill: undefined
    });

    if (this.itfGroup.get('intent_finder_policy').value === 'none') {
      this.itfp = null;
    } else {
      this.getIntentFinderPolicyInfo(itfp);
    }
    this.isDone();
  }

  getIntentFinderInstanceList() {
    return new Promise((resolve, reject) => {
      this.grpc.getIntentFinderInstanceList().subscribe(
        res => {
          this.itfiList = res.itfi_list;
          resolve(this.itfiList);
        },
        err => {
          let message = `Something went wrong. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          console.error('err', err.message);
          reject();
        }
      )
    });
  }

  /**
   * Intent Finder Instance 클릭시, Intent Finder Policy 정보 저장
   */
  getIntentFinderPolicyInfo(policyName: string): void {
    if (policyName !== 'none') {
      this.grpc.getIntentFinderPolicyInfo(policyName).subscribe(res => {
        if (res) {
          this.itfp = res;
          this.settingUIStep();
        }
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        console.error('err', err.message);
      });
    }
  }

  /**
   * 기존 IntentFinderPolicy steps에 화면에서만 쓰는 변수를 추가 후 세팅하여,
   * 사용자에게 Step 과 Hint 모델정보를 보여준다.
   */
  settingUIStep() {
    this.itfp.steps.forEach((step, index) => {
      step['seq'] = index + 1;

      // UI에 Skill, Hint 여부 표시 하기 위하여 세팅
      if (step.run_style === 'IFC_SKILL_FOUND_RETURN'
        || step.run_style === 'IFC_SKILL_FOUND_COLLECT_HINTS') {
        step.stepRunStyle = 'S';
        step.is_hint = false;
      } else if (step.run_style === 'IFC_HINT') {
        step.stepRunStyle = 'H';
        step.is_hint = true;
      }

      // UI에 Step에 등록된 Model 정보 표시
      switch (step.test_rule) {
        case 'custom_script_mod':
          step.type = 'CLM_CLASSIFY_SCRIPT';
          step.stepType = `<Custom Script>`;
          break;
        case 'pcre_mod':
          step.type = 'CLM_PCRE';
          step.stepType = `<PCRE - ${step.pcre_mod.model}>`;
          break;
        case 'dnn_cl_mod':
          step.type = 'CLM_DNN_CLASSIFIER';
          step.stepType = `<DNN - ${step.dnn_cl_mod.model}>`;
          break;
        case 'hmd_mod':
          step.type = 'CLM_HMD';
          step.stepType = `<HMD - ${step.hmd_mod.model}>`;
          break;
        case 'final_rule':
          step.type = 'CLM_FINAL_RULE';
          step.stepType = `<Final rule>`;
          break;
      }
    });
  }

  /**
   * Name 입력시 Title에 Name과 동일한 값을 세팅해준다.
   * 처음 한번만 title 입력 칸에 setting 해준다.
   * @param {string} name
   */
  setTitle(name: string) {
    if (this.role === 'add' && this.informationGroup.get('title').value === undefined) {
      this.informationGroup.patchValue({
        title: name
      });
    }
    this.cdr.detectChanges();
  }

  /**
   *
   * @param {string} lang ==> {IntentFinderPolicy 의 Language}
   * @returns {string} => Image의 URL 주소
   */
  getImageURL(lang: string): string {
    return lang === 'ko_KR' ? '/assets/img/korean_05.png' : '/assets/img/english_05.png';
  }

  getSttModels() {
    let chain = new Promise((resolve, reject) =>
      this.grpc.getSupervisorServerGroupList().subscribe(
        result => {
          if (result.svd_svr_grp_list.length === 0) {
            resolve();
          } else {
            let stt = result.svd_svr_grp_list[0].svd_proc_list.find(process => process.name === 'brain-stt');
            resolve(stt.state !== 0);
          }
        },
        err => reject(err)));

    chain = chain.then(isBrainSttActive => new Promise((resolve, reject) =>
      this.grpc.getSTTModels().subscribe(result => {
          this.store.dispatch({type: ROUTER_LOADED});
          // STT model
          if (isBrainSttActive) {
            this.sttModelList = result.models.sort((a, b) => a.model > b.model)
          }
          resolve();
        },
        err => reject(err))));

    chain.catch(err => {
      let message = `Something went wrong. [${getErrorString(err)}]`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  isDone() {
    if (this.informationGroup.valid && this.itfGroup.valid) {
      this.doneFlag = true;
    } else {
      this.doneFlag = false;
    }
  }

  submit() {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `${this.role === 'add' ? 'Add' : 'Edit'} '${this.informationGroup.get('name').value}'?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (this.role === 'add') {
          this.onAdd();
        } else {
          let chatbot = {
            name: this.informationGroup.get('name').value,
            title: this.informationGroup.get('title').value,
            description: this.informationGroup.get('description').value,
            active: this.informationGroup.get('active').value,
            intent_finder_policy: this.itfGroup.get('intent_finder_policy').value,
            default_skill: this.itfGroup.get('default_skill').value,
            session_timeout: this.informationGroup.get('session_timeout').value,
            external_system_info: this.informationGroup.get('external_system_info').value,
            stt_models: this.sttModelGroup.get('stt_models').value
          };

          if (JSON.stringify(chatbot) === JSON.stringify(this.originalChatbot)) {
            this.snackBar.open('Values are not changed. Please make some modification(s) first.', 'Confirm', {duration: 2000});
            return false;
          } else {
            this.onEdit();
          }
        }
      }
    });
  }

  onAdd() {
    let param =
      Object.assign({}, this.informationGroup.getRawValue(), this.itfGroup.getRawValue()
        , this.sttModelGroup.getRawValue());
    param.session_timeout = Number(param.session_timeout);

    this.grpc.insertChatbotInfo(param).subscribe(
      res => {
        this.snackBar.open(`Chatbot '${param.name}' created.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let message = `Failed to create a Chatbot. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    )
  }

  onEdit() {
    let param =
      Object.assign({}, this.informationGroup.getRawValue(), this.itfGroup.getRawValue()
        , this.sttModelGroup.getRawValue());

    if (typeof param.session_timeout === 'string') {
      param.session_timeout = Number(param.session_timeout);
    }

    this.grpc.updateChatbotInfo(param).subscribe(
      result => {
        this.snackBar.open(`Chatbot '${param.name}' updated.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let message = `Failed to update a Chatbot. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    );
  }

  backPage: any = (): void => {
    this.objectManager.clean('cb');
    this.router.navigate(['../chatbots'], {relativeTo: this.route});
  };

}
