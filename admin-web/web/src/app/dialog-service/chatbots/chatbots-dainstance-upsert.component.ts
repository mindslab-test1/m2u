import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatSelectionList,
  MatSnackBar,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {GrpcApi} from '../../core/sdk/index'
import {ROUTER_LOADED} from '../../core/actions';
import {
  AlertComponent,
  AppEnumsComponent,
  AppObjectManagerService,
  FormErrorStateMatcher,
  getErrorString
} from '../../shared/index';

const intType = '(^[0-9]*$)';

@Component({
  selector: 'app-chatbots-dainstance-upsert',
  templateUrl: './chatbots-dainstance-upsert.component.html',
  styleUrls: ['./chatbots-dainstance-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class ChatbotsDaInstanceUpsertComponent implements OnInit {
  dialogTitle: string;
  role: string;
  chatbotName: string;
  chatbot: any;
  dai = {
    dai_id: undefined,
    chatbot_name: undefined,
    da_name: undefined,
    sc_name: undefined,
    skill_names: [],
    name: undefined,
    lang: undefined,
    description: undefined,
    params: {},
    dai_exec_info: []
  };
  org_dai: any;
  dams: any;

  name: any;
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  // typeFormControl = new FormControl('', [
  //   Validators.pattern(intType)
  // ]);

  skillList = [];

  das: any;
  selectedDa = undefined;
  runParams = [];

  chatbotSkillList = [];
  defaultDaiSkillList = [];
  allChecked = false;
  @ViewChild(MatSelectionList) skillSelectionList: MatSelectionList;

  constructor(private store: Store<any>,
              private route: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private cdr: ChangeDetectorRef,
              public appEnum: AppEnumsComponent,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      query => {
        this.role = query['role'];
        this.chatbotName = query['chatbot'];
        if (!this.role || !this.chatbotName) {
          this.backPage();
          return;
        }
        switch (this.role) {
          case 'add':
            this.dialogTitle = 'Add DA Instance to ' + this.chatbotName;
            break;
          case 'edit':
            let dai = this.objectManager.get('dai');
            let includeCnt = 0;
            if (dai === undefined) {
              this.backPage();
              return;
            }
            delete dai.skill_count;
            delete dai.checked;
            this.dai = dai;
            this.org_dai = JSON.parse(JSON.stringify(this.dai));
            this.dialogTitle = 'Edit DA Instance of ' + this.chatbotName;
            this.dai.skill_names.forEach(name => {
              this.defaultDaiSkillList.push(name);
            });
            break;
          default:
            this.backPage();
            // let message = 'Role is not properly assigned.';
            // this.snackBar.open(message, 'Confirm', {duration: 10000});
            break;
        }
        this.retrieveData();
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      }
    )
  }

  retrieveData() {
    this.grpc.getDialogAgentManagerAllList().subscribe(
      result => {
        let dams = result.damwd_list;
        dams.forEach(dam => {
          dam.label = `${dam.dam_info.name}(${dam.dam_info.ip}:${dam.dam_info.port})`;
          dam.disabled = true;
          dam.checked = false;

          // dai edit 시 해당 dai에서 설정한 dam에 띄울 count 정보를 출력해주기 위한 처리.
          let info = (this.dai.dai_exec_info).filter(x => (x.dam_name === dam.dam_info.name));
          if (info.length > 0 ) { // dam name과  일치하는 정보가 존재할 경우
            dam.instances = info.length > 0 ? info["0"].cnt : undefined;
          } else { // dam name과  일치하는 정보가 존재하지 않을 경우
            dam.instances = undefined;
          }
        });
        dams.sort((a, b) => a.dam_info.name > b.dam_info.name);
        this.dams = dams;
        this.grpc.getDialogAgentAllList().subscribe(
          result2 => {
            result2.da_list.forEach(da => {
              da.da_info.viewValue = da.da_info.name + '(' + this.appEnum.runtimeEnvironment[da.da_info.runtime_env]
                + ':' + da.da_info.runtime_env_version + ')';
            });
            result2.da_list.sort((a, b) => {
              return a.da_info.name < b.da_info.name ? -1 : a.da_info.name > b.da_info.name ? 1 : 0;
            });

            this.das = result2.da_list;
            if (this.role === 'edit') {
              this.selectedDa = this.das.find(da => da.da_info.name === this.dai.da_name);
              this.onSelectedDaChange();
            }

            if (this.chatbotName) {
              this.grpc.getChatbotInfo(this.chatbotName).subscribe(
                chatbot => {
                  this.chatbot = chatbot;
                  // default skill 추가
                  this.chatbotSkillList.push(this.chatbot.default_skill);
                  if (this.chatbot.intent_finder_policy !== 'none') {
                    this.grpc.getIntentFinderPolicyInfo(this.chatbot.intent_finder_policy).subscribe(
                      itfpRes => {
                        itfpRes.steps.forEach(step => {
                          if (!step.is_hint) {
                            step.categories.forEach(category => {
                              if (this.chatbotSkillList.indexOf(category) === -1) {
                                this.chatbotSkillList.push(category);
                              }
                            })
                          }
                        });
                        this.chatbotSkillList.sort();

                        // 전체 선택된 경우일 경우
                        if (this.chatbotSkillList.length === this.defaultDaiSkillList.length) {
                          this.allChecked = true;
                        }
                      })
                  }
                },
                err => {
                  let message = `Something went wrong. [${getErrorString(err)}]`;
                  this.snackBar.open(message, 'Confirm', {duration: 10000});
                  this.store.dispatch({type: ROUTER_LOADED});
                }
              );
            } else {
              let message = `Something went wrong. Chatbot undefined`;
              this.snackBar.open(message, 'Confirm', {duration: 10000});
              this.store.dispatch({type: ROUTER_LOADED});
            }
          }, err => {
            let message = `Something went wrong. [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
            this.store.dispatch({type: ROUTER_LOADED});
          });
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });
  }

  onSelectedLangChange() {
    if (this.chatbot.intent_finder_policy !== 'none') {
      this.dai.skill_names = [];
      this.skillSelectionList.deselectAll();
      this.skillList = [];
    }
  }

  onSelectedDaChange() {
    this.dai.da_name = this.selectedDa.da_info.name;
    this.grpc.getRuntimeParameters({da_name: this.dai.da_name}).subscribe(
      paramList => {
        this.runParams = paramList.params.filter(list => (list.name !== '' && list.default_value !== '')
          && (list.name !== undefined && list.default_value !== undefined)
          && (list.name !== null && list.default_value !== null));
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      }
    );

    this.dams.forEach(dam => {
      let daai = this.selectedDa.daai_list.find(item => item.dam_name === dam.dam_info.name);
      if (daai) {
        dam.disabled = false;
        dam.checked = daai.active;
        if (dam.instances === undefined && dam.checked) {
          dam.instances = 1;
        }
      } else {
        dam.disabled = true;
        dam.checked = false;
        dam.instances = undefined;
      }
    });
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    let resultName = [];
    this.runParams.forEach(param => {
      if (param.required) {
        if (param.default_value === 'undefined' || param.default_value === '') {
          resultName.push(param.name);
          // return false;
        }
      }
    });
    if (resultName.length > 0) {
      this.snackBar.open(`Please input '${resultName}' field(s).`, 'confirm', {duration: 2000});
    } else if (this.isFormError(this.nameFormControl) || this.dai.name === undefined) {
      this.snackBar.open('Please input the name.', 'confirm', {duration: 2000});
    } else if (this.dai.lang === undefined) {
      this.snackBar.open('Please select the language.', 'confirm', {duration: 2000});
    } else if (this.skillSelectionList.selectedOptions.selected.length === 0) {
      this.snackBar.open('Please select the skill.', 'confirm', {duration: 2000});
    } else if (this.dai.da_name === undefined) {
      this.snackBar.open('Please select the da.', 'confirm', {duration: 2000});
      // } else if (this.isFormError(this.typeFormControl)) {
      //   this.snackBar.open('Please input type integer.', 'confirm', {duration: 2000});
    } else {
      const ref = this.dialog.open(AlertComponent);
      ref.componentInstance.message = `Add '${this.dai.name}'?`;
      ref.afterClosed().subscribe(result => {
        if (result) {
          if (this.role === 'add') {
            this.onAdd();
          } else {
            this.onEdit();
          }
        }
      });
    }
  }

  aggregateDialogAgentInfo() {
    this.dai.chatbot_name = this.chatbot.name;
    this.dai.dai_exec_info = [];
    this.dai.skill_names = []; // 초기화
    this.skillSelectionList.selectedOptions.selected.forEach(selected => {
      this.dai.skill_names.push(selected.value);
    });
    this.dams.forEach(dam => {
      if (!dam.disabled) {
        this.dai.dai_exec_info.push({
          dai_id: this.role === 'edit' ? this.dai.dai_id : undefined,
          dam_name: dam.dam_info.name,
          active: dam.checked,
          cnt: dam.checked ? dam.instances : 0
        });
      }
    });
    this.dai.params = {};
    this.runParams.forEach(param => this.dai.params[param.name] = param.default_value);
  }

  onAdd(): void {
    delete this.dai['isChecked'];
    this.aggregateDialogAgentInfo();
    this.grpc.insertDialogAgentInstanceInfo(this.dai).subscribe(
      result => {
        this.snackBar.open(`DAI '${this.dai.name}' created.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let message = `Failed to create a DAI. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    );
  }

  onEdit(): void {
    delete this.dai['isChecked'];
    this.aggregateDialogAgentInfo();
    this.grpc.updateDialogAgentInstanceInfo(this.dai).subscribe(
      result => {
        this.snackBar.open(`DAI '${this.dai.name}' updated.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let message = `Failed to update a DAI. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    );
  }

  getButtonName() {
    if (this.role === 'edit') {
      return 'Save';
    } else {
      return 'Add';
    }
  }

  backPage: any = () => {
    this.objectManager.clean('cb');
    let param = `chatbot=${encodeURI(this.chatbotName)}`;
    this.router.navigateByUrl('dialog-service/chatbots-dainstances-manage?' + param);
  }

  checkAll() {
    if (this.allChecked) {
      this.skillSelectionList.selectAll();
    } else {
      this.skillSelectionList.deselectAll()
    }
  };
}
