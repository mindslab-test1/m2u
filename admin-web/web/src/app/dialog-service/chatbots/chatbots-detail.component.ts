import {Component, OnInit} from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs/Subscription';

import {ROUTER_LOADED} from '../../core/actions';
import {
  AlertComponent,
  AppEnumsComponent,
  AppObjectManagerService,
  getErrorString
} from '../../shared/index';
import {AuthService} from '../../core/auth.service';
import {ConsoleUserApi, GrpcApi} from '../../core/sdk/index';

@Component({
  selector: 'app-chatbots-detail',
  templateUrl: './chatbots-detail.component.html',
  styleUrls: ['./chatbots-detail.component.scss'],
})

/**
 * Chatbot Detail 페이지 Component
 */
export class ChatbotsDetailComponent implements OnInit {
  actions: any;
  subscription = new Subscription();
  chatbot = {
    name: undefined,
    title: undefined,
    description: undefined,
    active: false,
    intent_finder_policy: undefined,
    default_skill: undefined,
    stt_models: [],
    session_timeout: undefined,
    external_system_info: undefined
  };

  intentFinderPolicy = {
    name: undefined,
    default_skill: [],
  };

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              public appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit(): void {
    let id = 'chbt';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      addDai: {
        icon: 'add_circle_outline',
        text: 'Add DAI',
        callback: this.addDaInstance,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      manageDai: {
        icon: 'build',
        text: 'Manage DAI',
        callback: this.manageDaInstances,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.READ, roles)
      }
    };

    this.route.queryParams.subscribe(
      querys => {
        let chatbot_name = querys['chatbot'];
        if (chatbot_name === undefined || chatbot_name === null || chatbot_name.length < 1) {
          this.backPage();
          return;
        }
        this.grpc.getChatbotInfo(chatbot_name).subscribe(
          cb_info => {
            this.chatbot = cb_info;
            if (this.chatbot.intent_finder_policy === 'none') {
              this.intentFinderPolicy = {
                name: 'none',
                default_skill: this.chatbot.default_skill
              };
            } else {
              this.grpc.getIntentFinderPolicyInfo(this.chatbot.intent_finder_policy).subscribe(
                itfp => {
                  this.intentFinderPolicy = itfp;
                  this.settingItfTable(this.intentFinderPolicy['steps']);
                },
                err => {
                  let message = `Something went wrong. [${getErrorString(err)}]`;
                  this.snackBar.open(message, 'Confirm', {duration: 10000});
                  this.store.dispatch({type: ROUTER_LOADED});
                });
            }
            this.store.dispatch({type: ROUTER_LOADED});
          },
          err => {
            let message = `Something went wrong. [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
            this.store.dispatch({type: ROUTER_LOADED});
          });
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      }
    );
  }

  /**
   * IntentFinder Table에 들어갈 값들을 세팅 해준다.
   * Classifying Method, runStyle을 화면에 보기 좋게 세팅 해준다.
   * @param steps => {하나의 IntentFinder Policy 에 대한 Steps}
   */
  settingItfTable(steps) {
    steps.forEach(step => {
      // UI에 Skill, Hint 여부 표시 하기 위하여 세팅
      if (step.run_style === 'IFC_SKILL_FOUND_RETURN'
        || step.run_style === 'IFC_SKILL_FOUND_COLLECT_HINTS') {
        step.stepRunStyle = 'Skill';
        step.is_hint = false;
      } else if (step.run_style === 'IFC_HINT') {
        step.stepRunStyle = 'Hint';
        step.is_hint = true;
      }

      // UI에 Step에 등록된 Model 정보 표시
      switch (step.test_rule) {
        case 'custom_script_mod':
          step.type = 'CLM_CLASSIFY_SCRIPT';
          step.stepType = `Custom Script`;
          break;
        case 'pcre_mod':
          step.type = 'CLM_PCRE';
          step.stepType = `PCRE`;
          break;
        case 'dnn_cl_mod':
          step.type = 'CLM_DNN_CLASSIFIER';
          step.stepType = `DNN`;
          break;
        case 'hmd_mod':
          step.type = 'CLM_HMD';
          step.stepType = `HMD`;
          break;
        case 'final_rule':
          step.type = 'CLM_FINAL_RULE';
          step.stepType = `Final rule`;
          break;
      }
    });
  }

  /**
   * 수정페이지로 이동
   */
  edit: any = (): void => {
    this.objectManager.set('cb', this.chatbot);
    this.router.navigateByUrl('dialog-service/chatbots-upsert#edit');
  };

  /**
   * DA Instance 추가 페이지로 이동
   */
  addDaInstance: any = (): void => {
    this.objectManager.set('cb', this.chatbot);
    let params = `role=add&chatbot=${encodeURI(this.chatbot.name)}`;
    this.router.navigateByUrl('dialog-service/chatbots-dainstance-upsert?' + params);
  };

  /**
   * DA Instance 관리페이지로 이동
   */
  manageDaInstances: any = (): void => {
    this.objectManager.set('cb', this.chatbot);
    let params = `chatbot=${encodeURI(this.chatbot.name)}`;
    this.router.navigateByUrl('dialog-service/chatbots-dainstances-manage?' + params);
  };

  /**
   * Chatbot 삭제 API 호출
   */
  delete: any = (): void => {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete '${this.chatbot.name}'?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      this.grpc.deleteChatbot([{name: this.chatbot.name}]).subscribe(
        res => {
          let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
          this.snackBar.open(`Chatbot '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
          this.backPage();
        },
        err => {
          let message = `Failed to Delete Chatbot. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  /**
   * 뒤로가기
   */
  backPage: any = (): void => {
    this.objectManager.clean('cb');
    this.router.navigate(['../chatbots'], {relativeTo: this.route});
  };

  /**
   * Icon 가져오기
   * @param {boolean} state
   * @returns {string}
   */
  getIcon: any = (state: boolean): string => {
    return state === true ? 'check_circle' : 'block';
  };

  /**
   * 이미지 경로 가져오기
   * @param {string} lang
   * @returns {string}
   */
  getImage(lang: string): string {
    if (lang === 'ko_KR' || lang === 'kor') {
      return '/assets/img/korean_05.png';
    } else if (lang === 'eu_US' || lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  }

  naviagateItfp() {
    this.objectManager.clean('cb');
    this.objectManager.set('itfp', this.intentFinderPolicy);
    this.router.navigateByUrl('dialog-service/itfp-detail');
  }
}
