import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar, MatDialog} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs/Subscription';

import {ROUTER_LOADED, DAM_DELETE} from '../../core/actions';
import {AuthService} from '../../core/auth.service';
import {AppObjectManagerService, AlertComponent, getErrorString} from '../../shared/index';
import {ConsoleUserApi, GrpcApi} from '../../core/sdk/index';
import {DialogAgentManagerDetailTableComponent} from './dialog-agent-manager-detail-table.component';

@Component({
  selector: 'app-dialog-agent-manager-detail',
  templateUrl: './dialog-agent-manager-detail.component.html',
  styleUrls: ['./dialog-agent-manager-detail.component.scss'],
})

export class DialogAgentManagerDetailComponent implements OnInit {
  dam: any;
  actions: any;
  table: any;
  subscription = new Subscription();

  @ViewChild('damDetailTableComponent') damDetailTableComponent: DialogAgentManagerDetailTableComponent;

  constructor(private store: Store<any>,
              private objectManager: AppObjectManagerService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit() {
    this.dam = this.objectManager.get('dam');

    if (this.dam.name === undefined) {
      this.backPage();
      return;
    }

    let id = 'dam';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      activate: {
        icon: this.dam.active ? 'stop' : 'play_arrow',
        text: this.dam.active ? 'Deactivate' : 'Activate',
        callback: this.activate,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      },
      restart: {
        icon: 'autorenew',
        text: 'Restart',
        callback: this.restart,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      },
      // disable: {
      //   icon: 'sync_disabled',
      //   text: 'Disable',
      //   callback: this.disable,
      //   hidden: true
      // }
    };

    this.table = {
      header: [
        {attr: 'da_info.name', name: 'Dialog Agent', sortable: true, searchable: true, width: 200},
        {attr: 'daa_info.active', name: 'Active', sortable: true, asIcon: true, width: 80},
        // {attr: 'da_info.registeredAt', name: 'Registered At', sortable: true, searchable: true, width: 120},
        {attr: 'chatbot_name', name: 'Chatbot', inArray: 'chatbot_dai_list', searchable: true, width: 180},
        {attr: 'dai_name', name: 'DA Instances', inArray: 'chatbot_dai_list', width: 180},
        {attr: 'state', name: 'State', inArray: 'chatbot_dai_list', width: 80},
        {attr: 'summary', name: 'Summary', inArray: 'chatbot_dai_list', width: 120},
      ],
      rows: [],
    };

    this.grpc.getDialogAgentWithDialogAgentInstanceList(this.dam.name).subscribe(
      result => {
        this.table.rows = result.da_with_dai_info;
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });
  }

  edit: any = () => {
    this.router.navigateByUrl('dialog-service/dam-upsert#edit');
  }

  delete: any = () => {
    const ref = this.dialog.open(AlertComponent);

    ref.componentInstance.message = `Delete '${this.dam.name}?`;
    ref.afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return;
        }
        this.grpc.deleteDialogAgentManager([{name: this.dam.name}]).subscribe(
          res => {
            let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
            this.backPage();
            this.snackBar.open(`Dialog Agent Manager '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
          },
          err => {
            let message = `Failed to Delete Dialog Agent Manager '${this.dam.name}'.\n[${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      });
  }

  activate: any = () => {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `${this.dam.active ? 'Deactivate' : 'Activate'} '${this.dam.name}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      if (this.dam.active) {
        this.grpc.stopDialogAgentManager([{name: this.dam.name}]).subscribe(
          res => {
            this.dam.active = false;
            this.setActivateIcons();
            let updateItem = res.result_list.filter(item => item.result).map(item => item.name);
            this.snackBar.open(`Dialog Agent Manager(s) '${updateItem}' Deactivated.`, 'Confirm', {duration: 3000});
          },
          err => {
            let message = `Failed to Deactivate Dialog Agent Manager(s). [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      } else {
        this.grpc.startDialogAgentManager([{name: this.dam.name}]).subscribe(
          res => {
            this.dam.active = true;
            this.setActivateIcons();
            let updateItem = res.result_list.filter(item => item.result).map(item => item.name);
            this.snackBar.open(`Dialog Agent Manager(s) '${updateItem}' Activated.`, 'Confirm', {duration: 3000});
          },
          err => {
            let message = `Failed to Activate Dialog Agent Manager(s). [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      }

    });
  }

  restart: any = () => {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `${'Restart'} '${this.dam.name}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      this.grpc.restartDialogAgentManager([{name: this.dam.name}]).subscribe(
        res => {
          this.dam.active = true;
          this.setActivateIcons();
          let updateItem = res.result_list.filter(item => item.result).map(item => item.name);
          this.snackBar.open(`Dialog Agent Manager(s) '${updateItem}' Stopped.`, 'Confirm', {duration: 3000});
        },
        err => {
          let message = `Failed to Stop Dialog Agent Manager(s). [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  }

  setActivateIcons() {
    if (this.dam.active) {
      this.actions.activate.text = 'Deactivate';
      this.actions.activate.icon = 'stop';
    } else {
      this.actions.activate.text = 'Activate';
      this.actions.activate.icon = 'play_arrow';
    }
  }

  getIcon() {
    if (this.dam.active) {
      return 'check_circle';
    } else {
      return 'block';
    }
  }

  backPage: any = () => {
    this.objectManager.clean('dam');
    this.router.navigate(['../dam'], {relativeTo: this.route});
  }
}
