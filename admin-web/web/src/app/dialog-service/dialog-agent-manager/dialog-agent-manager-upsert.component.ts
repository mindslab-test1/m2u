import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog, MatSnackBar, ErrorStateMatcher, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {Store} from '@ngrx/store';

import {GrpcApi} from '../../core/sdk/index'
import {ROUTER_LOADED} from '../../core/actions';
import {AppObjectManagerService, AlertComponent, FormErrorStateMatcher, getErrorString} from '../../shared/index';
import {ErrorDialogComponent} from '../../shared/dialogs/error-dialog/error-dialog.component';
import {IP_REGEX, PORT_REGEX} from '../../shared/values/values'

@Component({
  selector: 'app-dialog-agent-manager-upsert',
  templateUrl: './dialog-agent-manager-upsert.component.html',
  styleUrls: ['./dialog-agent-manager-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class DialogAgentManagerUpsertComponent implements OnInit {
  nameFormControl = new FormControl(undefined, [
    Validators.required
  ]);
  ipFormControl = new FormControl(undefined, [
    Validators.required,
    Validators.pattern(IP_REGEX)
  ]);
  portFormControl = new FormControl(undefined, [
    Validators.required,
    Validators.pattern(PORT_REGEX)
  ]);

  dialogTitle: string;
  dam = {
    name: undefined,
    ip: undefined,
    port: undefined,
    description: undefined,
    active: false,
    default: false
  };
  org_dam: any;
  role: string;
  submitButton: any;

  constructor(private store: Store<any>,
              private grpc: GrpcApi,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              public snackBar: MatSnackBar,
              private objectManager: AppObjectManagerService,
              public formErrorStateMatcher: FormErrorStateMatcher) {
  }

  ngOnInit() {
    this.route.fragment.subscribe(
      role => {
        this.role = role;
        switch (this.role) {
          case 'add':
            this.submitButton = true;
            this.dam.port = '9907';
            this.dialogTitle = 'Add Dialog Agent Manager';
            break;
          case 'duplicate':
          case 'edit':
            let dam = this.objectManager.get('dam');
            if (dam === undefined) {
              this.backPage();
              return;
            }
            this.dam = dam;
            this.org_dam = JSON.parse(JSON.stringify(this.dam));
            if (this.role === 'edit') {
              this.dialogTitle = 'Edit Dialog Agent Manager';
            } else {
              this.dialogTitle = 'Duplicate Dialog Agent Manager';
            }
            break;
          default:
            this.backPage();
            // let message = 'Role is not properly assigned.';
            // this.snackBar.open(message, 'Confirm', {duration: 10000});
            return;
        }
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Something went wrong. [${getErrorString(err)}]`;
        this.store.dispatch({type: ROUTER_LOADED});
      }
    )
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    if (this.role === 'edit') {
      if (Object.keys(this.dam).every(key => {
          return this.dam[key] === this.org_dam[key];
        })) {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Not Changed';
        ref.componentInstance.message = `Values are not changed.Please make some modification(s) first.`;
      } else {
        if (this.check()) {
          this.openDialog('edit');
        }
      }
    } else {
      if (this.role === 'duplicate' &&
        (this.dam.name === this.org_dam.name ||
          (this.dam.ip === this.org_dam.ip && this.dam.port === this.org_dam.port))) {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Duplicated';
        ref.componentInstance.message = `Please enter new name and ip:port.`;
      } else {
        if (this.check()) {
          this.openDialog('add');
        }
      }
    }
  }

  onAdd(): void {
    this.dam.port = parseInt(this.dam.port, 10);
    this.grpc.insertDialogAgentManagerInfo(this.dam).subscribe(
      res => {
        this.snackBar.open(`Dialog Agent Manager '${this.dam.name }' created.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed'
        let message = getErrorString(err);
        if (message === `gRPC:IP,  port duplicate(6)`) {
          ref.componentInstance.title = 'Duplicated';
          message = 'IP, Port Duplicated.'
        } else if (message === `gRPC:name duplicate(6)`) {
          ref.componentInstance.title = 'Duplicated';
          message = 'DAM Name Duplicated.'
        }
        console.log(message);
        ref.componentInstance.message = `Add Failed. ${message}`;
      });
  }

  onEdit(): void {
    this.dam.port = parseInt(this.dam.port, 10);
    this.grpc.updateDialogAgentManagerInfo(this.dam).subscribe(
      res => {
        this.snackBar.open(`Dialog Agent Manager '${this.dam.name}' updated.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Failed to update Dialog Agent Manager '${this.dam.name}. [ ${getErrorString(err)}} ]`;
      });
  }

  getButtonName() {
    if (this.role === 'edit') {
      return 'Save';
    } else {
      return 'Add';
    }
  }

  openDialog(data) {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = data === 'add' ? `Add '${this.dam.name}?'` : `Save '${this.dam.name}?'`
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (data === 'add') {
          this.onAdd();
        } else {
          this.onEdit();
        }
      }
    });
  }

  check() {
    if (this.isFormError(this.nameFormControl)) {
      this.submitButton = true;
    } else if (this.isFormError(this.ipFormControl)) {
      this.submitButton = true;
    } else if (this.isFormError(this.portFormControl)) {
      this.submitButton = true;
    } else if (this.dam.name === undefined || this.dam.ip === undefined || this.dam.port === undefined) {
      this.submitButton = true;
    } else {
      this.submitButton = false;
      return true;
    }
  }

  backPage: any = () => {
    this.objectManager.clean('dam');
    this.router.navigate(['../dam'], {relativeTo: this.route});
  }
}
