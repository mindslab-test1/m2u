import {Component, Input, ViewChild} from '@angular/core';
import {TableComponent} from '../../shared/components/table/table.component';
import {PaginationControlsComponent, PaginatePipe, PaginationService} from 'ngx-pagination';

@Component({
  selector: 'app-dialog-agent-manager-detail-table',
  templateUrl: './dialog-agent-manager-detail-table.component.html',
  styleUrls: ['./dialog-agent-manager-detail-table.component.scss']
})
export class DialogAgentManagerDetailTableComponent extends TableComponent {


  getStateIcon(row, col, instance) {
    return instance.state === '0' ? 'cached' : 'block';
  }

}
