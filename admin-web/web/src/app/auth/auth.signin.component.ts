import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { ROUTER_LOADED } from '../core/actions';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../shared/index';

import { Router } from '@angular/router';
import { AuthService } from '../core/auth.service';
import { MatSnackBar } from '@angular/material';
import { LoopConfigFileApi } from '../core/sdk/services/custom/LoopConfigFile';

@Component({
  selector: 'app-auth-signin',
  templateUrl: './auth.signin.component.html'
})
export class AuthSignInComponent implements OnInit {

  user: FormGroup;
  checkEmailVerify;

  constructor(
      private store: Store<any>,
      private fb: FormBuilder,
      private authService: AuthService,
      private router: Router,
      private configApi: LoopConfigFileApi,
      private snackBar: MatSnackBar,
    ) {
  }

  ngOnInit() {
    this.user = this.fb.group({
      username: ['', [
        Validators.required,
      ]],
      password: ['', [
        Validators.required,
      ]]
    });

    // this.configApi.getDataSourceConfig().subscribe((result: any) => {
    //   this.checkEmailVerify = result.userEmailVerify;
    this.store.dispatch({type: ROUTER_LOADED});
    // });
  }

  login() {
    if (this.user.valid) {
      this.authService.login(this.user.value, err => {
        CustomValidators.setCustomError(
            this.user, 'Invalid password or username.',
            err, {code: 'LOGIN_FAILED'}
          );
        CustomValidators.setCustomError(
            this.user, 'The email has not been verified.',
            err, {code: 'LOGIN_FAILED_EMAIL_NOT_VERIFIED'}
          );
        CustomValidators.setCustomError(
            this.user, 'The user has been deactivated.',
            err, {code: 'LOGIN_FAILED_DEACTIVATED'}
          );
        CustomValidators.setCustomError(
            this.user, 'The user has not been granted.',
            err, {code: 'LOGIN_FAILED_NOT_GRANTED'}
          );
      });
    }
  }
}

