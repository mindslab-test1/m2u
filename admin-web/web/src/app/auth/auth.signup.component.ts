import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { ROUTER_LOADED } from '../core/actions';

import { Router } from '@angular/router';
import { ConsoleUserApi } from '../core/sdk/index';
import { MatSnackBar } from '@angular/material';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from '../shared/index';

@Component({
  selector: 'app-auth-signup',
  templateUrl: './auth.signup.component.html'
})
export class AuthSignUpComponent implements OnInit {

  user: FormGroup;
  loading = false;

  constructor(
      private store: Store<any>,
      private fb: FormBuilder,
      private consoleUserApi: ConsoleUserApi,
      private router: Router,
      private snackBar: MatSnackBar,
    ) { }

  ngOnInit() {
    this.user = this.fb.group({
      username: ['', [
        Validators.required,
        Validators.minLength(4),
      ]],
      email: ['', [
        Validators.required,
        CustomValidators.email,
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(4),
      ]],
      passwordConfirmation: ['', [
        Validators.required,
        CustomValidators.matchWithControl('password'),
      ]],
      // agreement: ['no', [
      //   CustomValidators.matchWithValue('yes', 'Need to agree to the Terms of Service.'),
      // ]]
    });

    // finish loading
    this.store.dispatch({type: ROUTER_LOADED});
  }

  register() {
    if (!this.loading && this.user.valid) {
      this.loading = true;

      // register without useless props
      let data = Object.assign({}, this.user.value);
      delete data.passwordConfirmation;
      // delete data.agreement;

      this.consoleUserApi.create(data)
        .subscribe(res => {
          this.snackBar.open('Thanks! You can sign-in after verifying your email address.', 'Confirm', {duration: 10000});
          this.router.navigateByUrl('/auth/signin');
        }, err => {
          CustomValidators.setServerErrors(this.user, err);
          CustomValidators.setCustomError(
              this.user, 'Sorry! Cannot send a verification email from server.',
              err, {code: 'EMESSAGE'}
            );
          CustomValidators.setCustomError(
              this.user, 'Sorry! Cannot send a verification email from server.',
              err, {code: 'EENVELOPE'}
            );
          this.loading = false;
        });

    } else {
      // for submission without any update of form
      CustomValidators.markAllAsDirty(this.user);
    }
  }
}
