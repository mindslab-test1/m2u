import {ApplicationRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Store} from '@ngrx/store';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

import {BaseChartDirective} from 'ng2-charts';
import {AppObjectManagerService, getErrorString} from '../../shared';
import {StatisticsApi} from '../../core/sdk';
import {ExcelService} from '../../shared/services/excel.service';
import {Subscription} from 'rxjs/Subscription';
import {ROUTER_LOADED} from '../../core/actions';
import {AppEnumsComponent} from '../../shared/values/app.enums';
import {AccuracyEditTableComponent} from './accuracy-edit-table.component';

@Component({
  selector: 'app-accuracy',
  templateUrl: './accuracy.component.html',
  styleUrls: ['./accuracy.component.scss']
})
export class AccuracyComponent implements OnInit {

  table = {header: [], rows: [], paginationOff: false};
  keyword: string;
  selectedSearchOption: string;
  panelToggleText: string;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: AccuracyEditTableComponent;

  activeAnalysis = false;
  activeDownload = true;
  activeLineTab = true;

  periodTypes: any;
  now = new Date();
  requestParam = {
    startAt: new FormControl(new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate() - 7)),
    endAt: new FormControl(this.now),
    periodType: ''
  };
  startAt: string;
  endAt: string;

  // lineChart
  lineChartData: Array<any> = [
    {data: [], label: 'Accuracy'}
  ];
  lineChartLabels: Array<any> = [];
  lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {min: 0, max: 100},
        scaleLabel: {
          display: true,
          labelString: '%',
        }
      }]
    }
  };
  lineChartLegend = true;
  lineChartType = 'line';

  successResult = [];
  successLineResult = [];
  successQuery = false;
  tabLabels = ['Records', 'Time Series'];
  tabLabel = 'Records';

  isLoading = false;
  availableData = true;
  availableChartData = false;

  @ViewChild(BaseChartDirective) chart;

  constructor(private store: Store<any>,
              private router: Router,
              private objectManager: AppObjectManagerService,
              private excelService: ExcelService,
              private snackBar: MatSnackBar,
              private ref: ApplicationRef,
              private statisticsApi: StatisticsApi,
              public appEnum: AppEnumsComponent) {
    this.periodTypes = this.appEnum.periodTypeKeys;
    this.requestParam.periodType = this.periodTypes[0];
  }

  ngOnInit() {
    this.table = {
      header: [
        {attr: 'date', name: 'date', sortable: true},
        {attr: 'sessionID', name: 'sessionID', sortable: true},
        {attr: 'talkID', name: 'talkID', sortable: true},
        {attr: 'inText', name: 'inText', sortable: true, styleTextLimitLong: true},
        {attr: 'outText', name: 'outText', sortable: true, styleTextLimitLong: true},
        {attr: 'engine', name: 'engine', sortable: true, styleTextLimitShort: true},
        {attr: 'category', name: 'category', sortable: true, styleTextLimitShort: true},
        {attr: 'result', name: 'result', sortable: true},
        {attr: 'satisfaction', name: 'satisfaction', sortable: true, styleStar: true},
        {attr: 'accuracy', name: 'accuracy', sortable: true, styleAccuracy: true},
      ],
      rows: [],
      paginationOff: false
    };
    this.changeDateEvent();
    this.tableComponent.refreshAllChart = this.excuteAccuracyLineQuery;
    this.tableComponent.refreshFilterChart = this.filterDrawLineChart;
    this.tableComponent.filterFlag = false;
    this.tableComponent.onRowClick = this.chatLogDetailView;
    this.store.dispatch({type: ROUTER_LOADED});
  }

  onSearch() {
    this.tableComponent.filterFlag = false;
    let query = {
      'queryParams': {
        'startAt': this.startAt,
        'endAt': this.endAt
      }
    };

    this.isLoading = true;
    this.availableData = true;
    this.availableChartData = true;

    this.statisticsApi.executeOverallQuery(query).subscribe(
      res => {
        if (res.length === 0) {
          // data가 없음
          this.availableData = false;
          this.successQuery = false;
          this.activeDownload = true;
          this.isLoading = false;
          this.onReset();
        } else {
          res.forEach(row => {
            try {
              let outMeta = JSON.parse(row.metaInfo);
              row.engine = outMeta['out.da.engine.name'];
              row.category = outMeta['out.da.category'];
            } catch (err) {
              row.engine = '';
              row.category = '';
            }
            delete row.metaInfo;
          });
          this.availableData = true;
          this.successResult = res;
          this.rectifyValues(this.successResult);
          this.onReset();
          this.successQuery = true;
          this.activeDownload = false;
          // table.rows가 완성되고 나면 '로딩중' 해제
          this.isLoading = false;
        }
      },
      err => {
        // 에러 발생 시 '로딩중' 해제
        this.isLoading = false;
        this.availableData = true;
        this.snackBar.open(
          getErrorString(err, 'Something went wrong.'),
          'Confirm',
          {duration: 10000}
        );
      }
    );

    this.excuteAccuracyLineQuery();
  };

  excuteAccuracyLineQuery: any = () => {
    let query = {
      'queryParams': {
        'startAt': this.startAt,
        'endAt': this.endAt
      }
    };

    // Time Series 탭일 때
    this.statisticsApi.executeAccuracyLineQuery(query).subscribe(
      res => {
        if (res.length === 0) {
          this.availableChartData = false;
        } else {
          this.availableChartData = true;
          this.successLineResult = res;
          this.drawLineChart();
          this.refreshLineChart();
        }
      },
      err => {
        this.snackBar.open(
          getErrorString(err, 'Something went wrong.'),
          'Confirm',
          {duration: 10000}
        );
      });
  };

  onDownload() {
    let name = 'Accuracy-' + new Date().toLocaleString();
    let rows = JSON.parse(JSON.stringify(this.table.rows));
    rows.filter(item => delete item.accrChanged);
    this.excelService.exportAsExcelFile(rows, name, false);
  }

  onKeyPress: any = (event: any) => {
    if (event.keyCode === 13) { // enter
      this.onDetailSearch();
      // this.onSearch();
    }
  };

  /**
   * UI 로직으로 처리되어 있기 때문에, UI상 chart를 새로 바꿈
   */
  filterDrawLineChart: any = () => {
    let drawDataArr = [];

    // drawDataArr {date: yyyy-mm-dd, accuracy: 0/1/null}로 세팅
    if (this.table.rows.length > 0) {
      this.table.rows.forEach((row, index) => {
        let date = row.date.split(' ');
        drawDataArr.push({date: date[0], accuracy: row.accuracy});
      });


      // resultArr에 날짜별로 accuracy의 average 데이터를 넣어준다.
      let resultArr = [];

      drawDataArr.forEach(drawData => {
        if (resultArr.find(result => result.date === drawData.date) === undefined) {
          let tempArr = drawDataArr.filter(item => item['date'] === drawData['date']);
          let sum = 0;
          let count = 0;
          let avg = 0;
          let allNullFlag = true; // 0, 1 이 한번이라도 나오는지 체크 하기 위한 flag
          tempArr.forEach((temp, index) => {
            if (temp.accuracy !== null) {
              sum += temp.accuracy;
              allNullFlag = false;
              count++;
            }

            // 해당 날짜의 마지막 배열 요소 값이고, 전체가 N/A가 아닐때만 resultArr에 데이터 추가
            if (index === tempArr.length - 1 && !allNullFlag) {
              avg = sum / count;
              resultArr.push({date: temp.date, accuracy: parseFloat(avg.toFixed(2)), sum: sum, count: count});
            }
          });
        }
      });

      if (resultArr.length > 0) {
        // 날짜를 오름차순으로 보여주기 위하여 정렬 사용
        resultArr.sort((a, b) => {
          return (a.date < b.date) ? -1 : (a.date > b.date) ? 1 : 0;
        });

        this.availableChartData = true;
        this.successLineResult = resultArr.sort();
        this.drawLineChart();
        this.refreshLineChart();
      } else {
        this.availableChartData = false;
      }
    } else {
      this.availableChartData = false;
    }
  };

  /**
   * DetailSearch가 새로 sql을 처리한것이 아닌 UI 로직으로 처리 되어있음
   */
  onDetailSearch() {
    this.tableComponent.filterFlag = true;
    if (!!this.selectedSearchOption && this.table.rows.length !== 0) {
      this.isLoading = true;
      this.availableData = true;
      let tempList = [];

      // isLoading 변수를 UI에 적용시키기 위해 비동기 메소드 사용
      setTimeout(() => {
        tempList = this.successResult.filter(
          row => {
            if (row[this.selectedSearchOption] !== undefined) {
              switch (this.selectedSearchOption) {
                case 'date':
                  return row[this.selectedSearchOption].includes(this.keyword);
                case 'sessionID':
                  return (row[this.selectedSearchOption] + '').includes(this.keyword);
                case 'talkID':
                  return (row[this.selectedSearchOption] + '').includes(this.keyword);
                case 'inText':
                  return row[this.selectedSearchOption].toLowerCase()
                    .includes(this.keyword.toLowerCase());
                case 'outText':
                  return row[this.selectedSearchOption].toLowerCase()
                    .includes(this.keyword.toLowerCase());
                case 'engine':
                  return row[this.selectedSearchOption].toLowerCase()
                    .includes(this.keyword.toLowerCase());
                case 'category':
                  return row[this.selectedSearchOption].toLowerCase()
                    .includes(this.keyword.toLowerCase());
                case 'result':
                  return row[this.selectedSearchOption].toLowerCase()
                    .includes(this.keyword.toLowerCase());
                case 'satisfaction':
                  return row[this.selectedSearchOption] + '' === this.keyword;
                case 'accuracy':
                  let keyword_tmp = this.keyword;
                  switch (keyword_tmp.toLowerCase()) {
                    case 'o':
                      keyword_tmp = '1';
                      break;
                    case 'x':
                      keyword_tmp = '0';
                      break;
                    case 'n/a':
                      keyword_tmp = 'null';
                      break;
                    default:
                      break;
                  }
                  return row[this.selectedSearchOption] + '' === keyword_tmp;
                default :
                  break;
              }
            } else {
              return false;
            }
          }
        );
        if (tempList.length === 0) {
          this.activeDownload = true;
          this.availableChartData = false;
          this.availableData = false;
        } else {
          this.activeDownload = false;
          this.table.rows = tempList;
          this.filterDrawLineChart();
        }
        // table.rows가 완성되고 나면 '로딩중' 해제
        this.isLoading = false;
      }, 0);
    }
  }

  onReset() {
    this.keyword = '';
    this.selectedSearchOption = '--';
    this.table.rows = this.successResult;
  }

  changeDateEvent(event?) {
    if (!!this.requestParam.startAt.value && !!this.requestParam.endAt.value) {
      let date = this.requestParam.startAt.value;
      this.startAt = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
      date = this.requestParam.endAt.value;
      this.endAt = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
      this.activeAnalysis = false;
    }
  }

  chatLogDetailView: any = (row: any) => {
    this.objectManager.set('chatSession', row);
    this.router.navigateByUrl('evaluation/accuracy');
  };

  chartHovered(e) {
    console.log('hover', e);
  }

  // events
  chartClicked(e: any): void {
    console.log('click', e);
  }

  check(event) {
    if (event.source.id === 'periodType') {
      this.activeAnalysis = false;
      this.requestParam.periodType = event.value;
      if (this.successQuery) {
        if (event.value === 'DAILY') {
          this.onAnalysisDaily();
        } else if (event.value === 'MONTHLY') {
          this.onAnalysisMonthly();
        }
      }
    }
  }

  onAnalysis = () => {
    let periodUnit = this.requestParam.periodType;
    switch (periodUnit) {
      // DAILY
      case this.periodTypes[0] :
        this.onAnalysisDaily();
        break;
      // MONTHLY
      case this.periodTypes[1] :
        this.onAnalysisMonthly();
        break;
    }
  };

  onAnalysisMonthly = () => {
    this.lineChartLabels = [];
    this.lineChartData[0].data = [];
    let month_old = '';
    let sum = 0;
    let count = 0;

    this.successLineResult.forEach(item => {
      let month = item.date.slice(0, 7);
      if (month_old === '') {
        month_old = month;
      } else if (month !== month_old) {
        this.lineChartLabels.push(month_old);
        this.lineChartData[0].data.push(((sum / count) * 100).toFixed(2));
        month_old = month;
        sum = 0;
        count = 0;
      }
      sum += item['sum'];
      count += item['count'];
    });
    this.lineChartLabels.push(month_old);
    this.lineChartData[0].data.push(((sum / count) * 100).toFixed(2));
  };

  onAnalysisDaily = () => {
    this.lineChartLabels = [];
    this.lineChartData[0].data = [];
    this.successLineResult.forEach(item => {
      this.lineChartLabels.push(item.date);
      this.lineChartData[0].data.push(item.accuracy * 100);
    });
  };

  drawLineChart() {
    switch (this.requestParam.periodType) {
      // DAILY
      case this.periodTypes[0]:
        this.onAnalysisDaily();
        break;
      // MONTHLY
      case this.periodTypes[1]:
        this.onAnalysisMonthly();
        break;
      default:
        break;
    }
  }

  onTabClick(event) {
    this.tabLabel = event.tab.textLabel;

    // Records 탭일 때
    if (this.tabLabel === this.tabLabels[0]) {
      this.activeLineTab = true;

      // api result 가 있을 때
      if (this.successQuery) {

      }
      // Time Series 탭일 때
    } else if (this.tabLabel === this.tabLabels[1]) {
      this.activeLineTab = false;
    }
  }

  // Force refresh line chart
  refreshLineChart() {
    this.ref.tick();
    this.chart.refresh();
  }

  rectifyValues(res): any {
    res.forEach(row => {
      let channelNumber = `(${row.channel})`;
      let value = this.appEnum.accessFromValues[row.channel];
      row.channel = value ? this.appEnum.accessFrom[value] : `Other`;
      row.channel += channelNumber;
    });
    return res;
  }

}
