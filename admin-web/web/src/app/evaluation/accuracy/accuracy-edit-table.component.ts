import {AfterViewInit, Component, Input} from '@angular/core';
import {AlertComponent, TableComponent} from '../../shared';
import {MatDialog, MatSnackBar} from '@angular/material';
import {PaginationService} from 'ngx-pagination';
import {Router} from '@angular/router';
import {StatisticsApi} from '../../core/sdk/services/custom';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-accuracy-edit-table',
  templateUrl: './accuracy-edit-table.component.html',
  styleUrls: ['./accuracy-edit-table.component.scss']
})
export class AccuracyEditTableComponent
  extends TableComponent
  implements AfterViewInit {

  // if editting Accuracy
  editting = false;
  // inserting Accuracy query
  queryArgArr: any = [];
  @Input() refreshAllChart: () => Observable<any>;
  @Input() refreshFilterChart: () => Observable<any>;
  filterFlag: boolean;

  constructor(protected paginationService: PaginationService,
              protected router: Router,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private statisticsApi: StatisticsApi) {
    super(paginationService, router);
  }

  ngAfterViewInit() {
    // paging
    if (this.paginationControls) {
      this.subscription.add(
        this.paginationControls.pageChange
          .subscribe(page => {
            if (this.editting) {
              // check if Editting is done
              this.openDialog(page);
            } else {
              this.render(page, this.filterValue);
            }
          })
      );
    }
  }

  queryArgConstructor(sessionId, talkId, accuracy, row) {
    let queryArg = {sessionId: '', talkId: '', accuracy: '', row: undefined};
    queryArg.sessionId = sessionId;
    queryArg.talkId = talkId;
    queryArg.accuracy = accuracy;
    queryArg.row = row;
    return queryArg;
  }

  getStyleStar(row, col) {
    let val = super.getContent(row, col);
    return parseInt(val, 10) * 20 + '%';
  }

  onEditAccuracy(event, row) {
    this.editting = true;

    if (event.srcElement.innerText === '○') {
      row.accuracy = 1;
    } else if (event.srcElement.innerText === 'X') {
      row.accuracy = 0;
    } else if (event.srcElement.innerText === 'N/A') {
      row.accuracy = null;
    }

    row.accrChanged = true;

    let queryArg =
      this.queryArgConstructor(row.sessionID, row.talkID, row.accuracy, row);

    let data = this.queryArgArr.find(item => item.row === row);

    if (data) {
      this.queryArgArr.splice(this.queryArgArr.indexOf(data), 1, queryArg);
    } else {
      this.queryArgArr.push(queryArg);
    }
  }

  onSaveAccuracy() {
    this.editting = false;

    let query = {
      'queryParams': this.queryArgArr
    };

    delete this.queryArgArr.row;
    this.statisticsApi.updateAccuracy(query).subscribe(
      res => {
        this.snackBar.open(`${res.changedRows} rows of the accuracy updated.`, 'Confirm', {duration: 3000});
        this.initValues();
        if (this.filterFlag && this.refreshFilterChart) {
          this.refreshFilterChart();
        } else if (!this.filterFlag && this.refreshAllChart) {
          this.refreshAllChart();
        }
      }, err => {
        this.initValues();
      }
    );
  }

  initValues() {
    this.queryArgArr = [];
    this.filteredRows.forEach(row => {
      row.accrChanged = false;
    });
  }

  openDialog(page) {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = 'Are you done editting the accuracy?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        // page rendering when editting is done
        this.render(page, this.filterValue);
      }
    });
  }

}
