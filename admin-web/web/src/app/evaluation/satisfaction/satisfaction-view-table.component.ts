import {
  Component
} from '@angular/core';
import {TableComponent} from '../../shared';

@Component({
  selector: 'app-satisfaction-view-table',
  templateUrl: './satisfaction-view-table.component.html',
  styleUrls: ['./satisfaction-view-table.component.scss']
})
export class SatisfactionViewTableComponent
  extends TableComponent {

  getContent(row, col) {
    let val = this.getAttr(row, col.attr);

    if (col.getValue) {
      val = col.getValue(val, row);
    } else if (col.format) {
      val = col.format(val);
    }

    if (col.styleStar) {
      let int_val = parseInt(val, 10);
      if (int_val > 5) {
        int_val = 5;
      }
      val = int_val * 20 + '%';
    }

    if (col.styleAccuracy) {
      if (val !== 'N/A') {
        let val_int = parseInt(val, 10);
        if (val_int === 1) {
          val = '○';
        } else if (val_int === 0) {
          val = 'X';
        }
      }
    }
    return val;
  }

}
