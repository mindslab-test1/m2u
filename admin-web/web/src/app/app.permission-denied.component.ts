import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AuthService} from './core/auth.service';
import {ROUTER_LOADED} from './core/actions';


@Component({
  selector: 'app-permission-denied',
  templateUrl: './app.permission-denied.component.html',
  styleUrls: ['./app.permission-denied.component.scss']
})

export class AppPermissionDeniedComponent implements OnInit {

  constructor(private store: Store<any>,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.store.dispatch({type: ROUTER_LOADED});
  }

  resignIn() {
    this.authService.logout();
  }

}
