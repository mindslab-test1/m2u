import {Component, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {FormBuilder, FormControl} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

import {StatisticsApi} from '../../core/sdk/index';
import {ROUTER_LOADED} from '../../core/actions';
import {AppEnumsComponent} from '../../shared/values/app.enums';
import {AppObjectManagerService, getErrorString, TableComponent} from '../../shared/index';
import {ExcelService} from "../../shared";

@Component({
  selector: 'app-chat-log-session-v3-list',
  templateUrl: './chat-log-session-v3-list.component.html',
  styleUrls: ['./chat-log-session-v3-list.component.scss']
})

export class ChatLogSessionV3ListComponent implements OnInit {
  now = new Date();
  endDate = new FormControl(this.now);
  startDate = new FormControl(new Date(this.now.getFullYear(), this.now.getMonth() - 1, this.now.getDate()));
  searchOption = {
    id: 'Id',
    deviceId: 'DeviceId',
    deviceType: 'DeviceType',
    chatbot: 'Chatbot',
    channel: 'Channel',
  }
  searchOptionKeys = Object.keys(this.searchOption);

  table = {header: [], rows: [], paginationOff: false};
  keyword: string;
  selectedSearchOption: string;
  panelToggleText: string;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;

  constructor(private store: Store<any>,
              private router: Router,
              private fb: FormBuilder,
              private objectManager: AppObjectManagerService,
              private snackBar: MatSnackBar,
              private appEnum: AppEnumsComponent,
              private excelService: ExcelService,
              private statisticsApi: StatisticsApi) {
  }

  ngOnInit() {
    let prevListInfo = this.objectManager.get('sessionList_info');

    if (prevListInfo) {
      if (prevListInfo.fromSessionDetailPage
        && prevListInfo.startDate && prevListInfo.endDate) {
        this.startDate = new FormControl(new Date(prevListInfo.startDate));
        this.endDate = new FormControl(new Date(prevListInfo.endDate));
      }
      // 기간 set 후에 저장된 값을 clean
      this.objectManager.clean('sessionList_info');
    }

    this.table = {
      header: [
        {attr: 'ID', name: 'Id', sortable: true},
        {attr: 'CHATBOT', name: 'Chatbot', sortable: true},
        {attr: 'DEVICEID', name: 'DeviceId', sortable: true},
        {attr: 'DEVICETYPE', name: 'DeviceType', sortable: true},
        {attr: 'CHANNEL', name: 'Channel', sortable: true},
        {attr: 'PEER', name: 'PeerIp', sortable: true},
        {attr: 'STARTAT', name: 'StartAt', sortable: true},
        {attr: 'LASTTALKEDAT', name: 'LastTalkedAt', sortable: true},
      ],
      rows: [],
      paginationOff: false
    };
    this.tableComponent.onRowClick = this.chatLogDetailView;

    setTimeout(() => {
      this.getSessionList();
      this.store.dispatch({type: ROUTER_LOADED});
    }, 0);
  }

  getSessionList: any = () => {
    if (this.startDate.value > this.endDate.value) { // 시작날짜가 종료날짜보다 클경우
      this.snackBar.open('End Date should be greater than Start Date', 'Confirm', {duration: 10000});
      let data = <HTMLInputElement>document.getElementById('end-date-input');
      data.focus();
    } else {
      let queryParams = {
        startAt: this.startDate.value.getFullYear()
        + '-' + (this.startDate.value.getMonth() + 1)
        + '-' + this.startDate.value.getDate(),
        endAt: this.endDate.value.getFullYear()
        + '-' + (this.endDate.value.getMonth() + 1)
        + '-' + (this.endDate.value.getDate()),
        searchOption: this.selectedSearchOption,
        keyword: this.keyword
      };

      this.statisticsApi.executeSessionV3Query(queryParams).subscribe(
        resultSet => {
          // resultSet.forEach(row => {
          //   let channelNumber = `(${row.channel})`;
          //   // let value = this.appEnum.accessFromValues[row.channel];
          //   // row.channel = value ? this.appEnum.accessFrom[value] : `Other`;
          //   // row.channel += channelNumber;
          // });
          this.table.rows = resultSet;
        },
        err => {
          this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
        });
    }
  };

  onKeyPress: any = (event: any) => {
    if (event.keyCode === 13) { // enter
      this.getSessionList();
    }
  };

  chatLogDetailView: any = (row: any) => {
    this.objectManager.set('chatSessionV3', row);

    let sessionList_info = {
      startDate: this.startDate.value,
      endDate: this.endDate.value,
      // fromSessionDetailPage: Session Detail Page로부터 넘어왔는지 여부
      fromSessionDetailPage: false
    }
    // 설정한 기간을 저장
    this.objectManager.set('sessionListV3_info', sessionList_info);
    this.router.navigateByUrl('dialog-monitoring/chat-log-v3-list');
  }

  excelDownload() {
    if (this.table.rows.length > 0) {
      let name = 'SessionV3List'
      this.excelService.exportAsExcelFile(this.table.rows, name, true);
    } else {
      this.snackBar.open('No Data to download!', 'Confirm', {duration: 3000});
    }
  }
}
