import {Component, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {FormBuilder, FormControl} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

import {StatisticsApi} from '../../core/sdk/index';
import {ROUTER_LOADED} from '../../core/actions';
import {AppEnumsComponent} from '../../shared/values/app.enums';
import {AppObjectManagerService, getErrorString, TableComponent} from '../../shared/index';

@Component({
  selector: 'app-chat-log-session-list',
  templateUrl: './chat-log-session-list.component.html',
  styleUrls: ['./chat-log-session-list.component.scss']
})

export class ChatLogSessionListComponent implements OnInit {
  now = new Date();
  endDate = new FormControl(this.now);
  startDate = new FormControl(new Date(this.now.getFullYear(), this.now.getMonth() - 1, this.now.getDate()));
  searchOption = {
    id: 'Id',
    chatbot: 'Chatbot',
    accessFrom: 'Channel',
    peerIp: 'PeerIp',
    user: 'User',
  }
  searchOptionKeys = Object.keys(this.searchOption);

  table = {header: [], rows: [], paginationOff: false};
  keyword: string;
  selectedSearchOption: string;
  panelToggleText: string;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;

  constructor(private store: Store<any>,
              private router: Router,
              private fb: FormBuilder,
              private objectManager: AppObjectManagerService,
              private snackBar: MatSnackBar,
              private appEnum: AppEnumsComponent,
              private statisticsApi: StatisticsApi) {
  }

  ngOnInit() {
    let prevListInfo = this.objectManager.get('sessionList_info');

    if (prevListInfo) {
      if (prevListInfo.fromSessionDetailPage
        && prevListInfo.startDate && prevListInfo.endDate) {
        this.startDate = new FormControl(new Date(prevListInfo.startDate));
        this.endDate = new FormControl(new Date(prevListInfo.endDate));
      }
      // 기간 set 후에 저장된 값을 clean
      this.objectManager.clean('sessionList_info');
    }

    this.table = {
      header: [
        {attr: 'ID', name: 'Id', sortable: true},
        {attr: 'STARTAT', name: 'StartAt', sortable: true},
        {attr: 'ENDAT', name: 'EndAt', sortable: true},
        {attr: 'CHATBOT', name: 'Chatbot', sortable: true},
        {attr: 'CHANNEL', name: 'Channel', sortable: true},
        {attr: 'PEERIP', name: 'PeerIp', sortable: true},
        {attr: 'TALKCOUNT', name: 'TalkCount', sortable: true},
        {attr: 'USER', name: 'User', sortable: true},
      ],
      rows: [],
      paginationOff: false
    };
    this.tableComponent.onRowClick = this.chatLogDetailView;

    setTimeout(() => {
      this.getSessionList();
      this.store.dispatch({type: ROUTER_LOADED});
    }, 0);
  }

  getSessionList: any = () => {
    if (this.startDate.value > this.endDate.value) { // 시작날짜가 종료날짜보다 클경우
      this.snackBar.open('End Date should be greater than Start Date', 'Confirm', {duration: 10000});
      let data = <HTMLInputElement>document.getElementById('end-date-input');
      data.focus();
    } else {
      let queryParams = {
        startAt: this.startDate.value.getFullYear()
        + '-' + (this.startDate.value.getMonth() + 1)
        + '-' + this.startDate.value.getDate(),
        endAt: this.endDate.value.getFullYear()
        + '-' + (this.endDate.value.getMonth() + 1)
        + '-' + (this.endDate.value.getDate()),
        searchOption: this.selectedSearchOption,
        keyword: this.keyword
      };

      // Channel 은 DB 로 들어갈시 숫자로 변형 되서 들어가서, 정규표현 필요
      if (queryParams.searchOption === 'accessFrom') {
        let accessFroms = [
          'ANONYMOUS', 'WEBBROWSER', 'TEXTMESSENGER', 'SPEAKER',
          'TELEPHONE', 'SMS', 'MOBILE', 'ANDROID', 'IPHONE',
          'SNS', 'KAKAOTALK', 'FACEBOOK', 'EXTERNALAPI'
        ];
        // 공백 제거 후 대문자로 변환
        let searchKeyword = this.keyword.replace(/ /gi, '').toUpperCase();

        // regular expression 활용하여 키워드 검색 후 DB 데이터로 치환
        let pattern = new RegExp(`${searchKeyword}.*`, 'g');
        accessFroms.some(accessFrom => {
          if (pattern.test(accessFrom)) {
            queryParams.keyword = this.appEnum.accessFromSearchValues[accessFrom];
            return true;
          }
        });
      }

      this.statisticsApi.executeSessionQuery(queryParams).subscribe(
        resultSet => {
          resultSet.forEach(row => {
            let channelNumber = `(${row.channel})`;
            let value = this.appEnum.accessFromValues[row.channel];
            row.channel = value ? this.appEnum.accessFrom[value] : `Other`;
            row.channel += channelNumber;
          });
          this.table.rows = resultSet;
        },
        err => {
          this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
        });
    }
  };

  onKeyPress: any = (event: any) => {
    if (event.keyCode === 13) { // enter
      this.getSessionList();
    }
  };

  chatLogDetailView: any = (row: any) => {
    this.objectManager.set('chatSession', row);

    let sessionList_info = {
      startDate: this.startDate.value,
      endDate: this.endDate.value,
      // fromSessionDetailPage: Session Detail Page로부터 넘어왔는지 여부
      fromSessionDetailPage: false
    }
    // 설정한 기간을 저장
    this.objectManager.set('sessionList_info', sessionList_info);
    this.router.navigateByUrl('dialog-monitoring/chat-log-list');
  }
}
