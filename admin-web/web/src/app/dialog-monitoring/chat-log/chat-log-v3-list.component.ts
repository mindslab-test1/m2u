import {Component, OnInit, OnDestroy, ViewChild, TemplateRef} from '@angular/core';
import {Store} from '@ngrx/store';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {MatSnackBar, MatSidenav} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';

import {StatisticsApi} from '../../core/sdk/index';
import {ROUTER_LOADED} from '../../core/actions';
import {AppObjectManagerService, TableComponent, getErrorString} from '../../shared/index';
import {ChatLogV3PanelComponent} from './chat-log-v3-panel.component';
import {ExcelService} from "../../shared";
import {AuthService} from "../../core/auth.service";

@Component({
  selector: 'app-chat-log-v3-list',
  templateUrl: './chat-log-v3-list.component.html',
  styleUrls: ['./chat-log-v3-list.component.scss']
})

export class ChatLogV3ListComponent implements OnInit {
  chatSession: any;
  panelToggleText: string;
  table = {header: [], rows: [], paginationOff: false};
  filterKeyword: FormControl;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('infoPanel') infoPanel: ChatLogV3PanelComponent;

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private objectManager: AppObjectManagerService,
              private snackBar: MatSnackBar,
              private excelService: ExcelService,
              private statisticsApi: StatisticsApi) {
  }

  ngOnInit() {
    let chatSessionObj = this.objectManager.get('chatSessionV3');
    this.chatSession = chatSessionObj;

    if (chatSessionObj === undefined) {
      this.backPage();
      return;
    }

    this.table = {
      header: [
        {attr: 'SEQ', name: 'Seq'},
        {attr: 'SKILL', name: 'Skill'},
        {attr: 'INTEXT', name: 'Question', styleTextLimit: true},
        {attr: 'OUTTEXT', name: 'Answer', styleTextLimit: true},
        {attr: 'STARTTIME', name: 'StartTime'},
        {attr: 'ENDTIME', name: 'EndTime'},
      ],
      rows: [],
      paginationOff: false
    };
    this.tableComponent.onRowClick = this.fillRowInfo;

    setTimeout(() => {
      this.getChatList();
      this.store.dispatch({type: ROUTER_LOADED});
    }, 0);
  };

  getChatList: any = () => {
    this.statisticsApi.executeChatV3ListQuery({sessionId: this.chatSession.ID})
      .subscribe(
        resultSet => {
          this.table.rows = resultSet;
          setTimeout(() => {
            this.fillRowInfo(this.table.rows[0]);
          }, this.table.rows.length > 0 ? 500 : 0);
        },
        err => {
          this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
        }
      );
  };

  fillRowInfo: any = (row: any) => {
    this.infoPanel.chatInfo = row;
    if (row === undefined) {
      this.sidenav.close();
      this.panelToggleText = 'Show Info Panel';
    } else {
      this.sidenav.open();
      this.panelToggleText = 'Hide Info Panel';
    }

  };

  togglePanel: any = () => {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  }

  backPage: any = () => {
    this.objectManager.clean('chatSessionV3');

    // fromSessionDetailPage boolean을 true로 저장
    let sessionList_info = this.objectManager.get('sessionListV3_info');
    if (sessionList_info) {
      sessionList_info.fromSessionDetailPage = true;
    }
    this.router.navigate(['../view-session-v3-list'], {relativeTo: this.route});
  };
}
