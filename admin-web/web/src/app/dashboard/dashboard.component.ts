import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';

import {ROUTER_LOADED} from '../core/actions';
import {GrpcApi} from 'app/core/sdk';
import {getErrorString} from 'app/shared';
import {AppObjectManagerService} from '../shared/services/app-object-manager.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

/**
 * Dashboard 화면
 * Chatbot 정보와 server 정보 표현
 */
export class DashboardComponent implements OnInit, OnDestroy {
  Handler: any;
  chatbotList = [];
  svdList = [];

  constructor(private store: Store<any>,
              private router: Router,
              private dialog: MatDialog,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    this.getChatbotAllList();
    this.getSupervisorServerGroupList();
    this.Handler = setInterval(() => {
      this.getChatbotAllList();
      this.getSupervisorServerGroupList();
    }, 10000);
  }

  /**
   * Chatbot의 전체 리스트를 가져온다.
   */
  getChatbotAllList() {
    this.grpc.getChatbotAllList().subscribe(
      result => {
        result.chatbot_list.forEach(chatbot => {
          chatbot.daInstances = [];
          if (chatbot.korean_cnt > 0) {
            chatbot.daInstances.push({lang: 'kor', count: chatbot.korean_cnt});
          }
          if (chatbot.english_cnt > 0) {
            chatbot.daInstances.push({lang: 'eng', count: chatbot.english_cnt});
          }
        });
        this.chatbotList = result.chatbot_list;
        this.store.dispatch({type: ROUTER_LOADED});
      }, err => {
        console.error(`Something went wrong. [${getErrorString(err)}]`);
        this.store.dispatch({type: ROUTER_LOADED});
      });
  }

  /**
   * 현재 접속중인 서버의 정보를 가져온다.
   */
  getSupervisorServerGroupList() {
    this.grpc.getSupervisorServerGroupList().subscribe(
      res => {
        this.svdList = res.svd_svr_grp_list;
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        console.error(`Something went wrong. [${getErrorString(err)}]`);
        this.store.dispatch({type: ROUTER_LOADED});
      });
  }


  /**
   * chatbot의 상태에 따라 class 이름을 다르게 반환한다.
   * @param chatbot => {하나의 챗봇 객체}
   * @returns {string} => {class 이름}
   */
  getClassName(chatbot: any): string {
    return chatbot.daInstances.length !== 0 ? 'box primary' : 'box';
  }

  /**
   * 언어의 종류에 따라 다른 img Url 을 반환한다.
   * @param daInstance => 하나의 daInstance 객체
   * @returns {string} => imgUrl
   */
  getImageUrl(daInstance): string {
    if (daInstance.lang === 'kor') {
      return '/assets/img/korean_05.png';
    } else {
      return '/assets/img/english_05.png';
    }
  }

  /**
   * 해당 페이지로 이동할시, 별도의 정보를 조회해올 필요가 없을경우 이 함수를 탄다.
   * @param url => {이동할 주소}
   * @param param => {Url 뒤에 붙을 parameter 정보}
   */
  navigateUrlQueryParams(url, param?) {
    if (param) {
      url += param;
    }

    this.router.navigateByUrl(url);
  };

  /**
   * 해당 페이지로 이동할시, 별도의 정보를 조회해올 필요가 있을경우 이 함수를 탄다.
   * @param type => {데이터를 조회해올 객체의 종류}
   * @param params => {데이터를 조회해올때 필요한 매개변수}
   */
  navigateUrl(type, params) {
    switch (type) {
      case 'itfi':
        this.grpc.getIntentFinderInstanceInfo(params).subscribe(
          res => {
            this.objectManager.set('itfi', res);
            this.router.navigateByUrl('dialog-service/itfi-detail');
          },
          err => {
            console.error(`Something went wrong. [${getErrorString(err)}]`);
          }
        );
        break;
      case 'chatbotadd':
        this.router.navigateByUrl('dialog-service/chatbots-upsert#add');
    }
  }

  /**
   * 페이지 종료시 Handler를 초기화한다.
   */
  ngOnDestroy() {
    if (this.Handler) {
      clearInterval(this.Handler);
    }
  }
}
