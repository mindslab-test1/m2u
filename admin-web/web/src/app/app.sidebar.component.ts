import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app.sidebar.component.html',
})
export class AppSidebarComponent implements OnInit {

  @Input() depth = 1;
  @Input() navs: any[] = [];

  constructor(private router: Router) {
  }

  ngOnInit() {
    console.log(this.navs);
  }

  click(nav, $event) {

    // 해당 메뉴를 토글한다.
    nav.open = !nav.open;

    // 만약 Depth가 가장 깊은 메뉴라면 그곳으로 이동한다.
    if (!nav.children) {
      this.router.navigate(nav.realpaths);
    }

    $event.preventDefault();
  }
}
