import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'secondsToTime'})
export class SecondsToTimePipe implements PipeTransform {
  times = {
    year: 31557600,
    month: 2629746,
    day: 86400,
    h: 3600,
    m: 60,
    s: 1
  };

  transform(seconds) {
    let time_string = '';
    let plural = '';
    for (let key in this.times) {
      if (Math.floor(seconds / this.times[key]) > 0) {
        plural = (Math.floor(seconds / this.times[key]) > 1 && key.length > 1) ? 's' : '';
        time_string += Math.floor(seconds / this.times[key]).toString() + key.toString() + plural + ' ';
        seconds = seconds - this.times[key] * Math.floor(seconds / this.times[key]);
      }
    }
    return time_string || '0s';
  }
};
