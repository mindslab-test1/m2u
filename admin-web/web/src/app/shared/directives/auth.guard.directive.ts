import {OnInit, OnDestroy, Directive, ElementRef, Input} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {AuthService} from '../../core/auth.service';
import {LoopConfigFileApi} from '../../core/sdk/services/custom/LoopConfigFile';

@Directive({
  selector: '[appAuthGuard]'
})
export class AuthGuardDirective implements OnInit, OnDestroy {

  @Input() appAuthGuard: string[] | any = [];

  subscription: Subscription;
  originalDisplay: any;

  constructor(private el: ElementRef,
              private authService: AuthService,
              private configApi: LoopConfigFileApi) {
    this.originalDisplay = el.nativeElement.style.display;
  }

  ngOnInit() {
    this.subscription = this.authService.checkWithACL(this.appAuthGuard.ACL)
      .subscribe(granted => {
        // console.log(granted, this.appAuthGuard, this.el.nativeElement);
        let displayCss = this.originalDisplay;

        this.el.nativeElement.style.display = granted ? displayCss : 'none';
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
