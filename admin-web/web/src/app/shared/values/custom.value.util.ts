export class CustomValueUtil {
  static isStringEmpty = (data: string) => {
    return (data === '' || data === null || data === 'undefined' || data === undefined);
  }

  static isStringEmptyList = (data: string[]) => {
    let checkVal = true;
    data.forEach(oneData => {
      if (CustomValueUtil.isStringEmpty(oneData)) {
        checkVal = checkVal && true;
      } else {
        checkVal = checkVal && false;
        return false;
      }
    });
    return checkVal;
  }
}
