export function getErrorString(error, sentence?: string) {
  let message;
  if (typeof error.code === 'number') {
    message = `${error.detail || error.message}(${error.code})`;
  } else if (typeof error.statusCode === 'number') {
    message = `${error.message}(${error.statusCode})`;
  } else {
    message = error;
  }
  return sentence ? `${sentence} [${message}]` : message;
};
