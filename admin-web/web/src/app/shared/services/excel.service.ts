import {Injectable} from '@angular/core';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

  constructor() {
  }

  public exportAsExcelFile(json: any[], name: string, withTs: boolean): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};

    if (name) {
      if (withTs) {
        name += new Date().getTime().toString();
      }
    } else {
      name = new Date().getTime().toString();
    }

    XLSX.writeFile(workbook, name + EXCEL_EXTENSION);
  }
}
