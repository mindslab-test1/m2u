import {Injectable} from '@angular/core';

@Injectable()
export class AppObjectManagerService {

  private objects = {};

  constructor() {
  }

  set(type: string, obj: any): void {
    this.objects[type] = obj;
  }

  get(type: string): any {
    return this.objects[type];
  }

  clean(type: string): void {
    delete this.objects[type];
  }
}
