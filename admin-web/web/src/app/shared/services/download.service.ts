import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CsvService} from 'angular2-json2csv';

@Injectable()
export class DownloadService {

  constructor(private csvService: CsvService) {
  }

  fromObservable(observable: Observable<any>) {
    observable
      .subscribe((data: any) => {
        let raw = data.text();
        this.makeBlob(raw);
      });
  }

  private makeBlob(data: string, name?: string) {
    name = name || new Date().getTime().toString();
    let blob = new Blob([data], {type: 'text/plain'});
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.download = name;

    setTimeout(() => {
      a.click();
      window.URL.revokeObjectURL(url);
    }, 500);
  }

  fromArray(array: any[], name?: string, withTs = false) {
    if (name) {
      if (withTs) {
        name += new Date().getTime().toString();
      }
    } else {
      name = new Date().getTime().toString();
    }
    this.csvService.download(array, name);
  }
}
