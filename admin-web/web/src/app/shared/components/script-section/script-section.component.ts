import {Component, Input, ViewChildren, QueryList, ElementRef, Renderer} from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-script-section',
  templateUrl: 'script-section.component.html',
  styleUrls: ['script-section.component.scss']
})
export class ScriptSectionComponent {

  @ViewChildren('tr') childScript: QueryList<ElementRef>;
  @Input() scripts: Array<any> = [];
  @Input() clickScript = (item, idx) => {};


  constructor(private store: Store<any>) {
  }

  changeSelectedLine(i) {
    this.childScript.forEach( (item, index) => {
      if (index === i) {
        item.nativeElement.className = 'blueColor';
      } else {
        item.nativeElement.className = '';
        if (index + 5 === i) {
          item.nativeElement.scrollIntoView();
        }
      }
    });
  }
  onDblClickScript(item, idx) {
    this.clickScript(item, idx);
  }
}
