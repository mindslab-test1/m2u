import {
  Component,
  EventEmitter,
  OnInit,
  OnDestroy,
  Input,
  Output,
  ViewChild,
  TemplateRef,
  AfterViewInit
} from '@angular/core';
import {DatePipe} from '@angular/common';
import {PaginationControlsComponent, PaginatePipe, PaginationService} from 'ngx-pagination';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {LoopBackFilter} from '../../../core/sdk/index';
import {FileSizePipe} from '../../pipes/filesize.pipe';
import {SecondsToTimePipe} from '../../pipes/seconds-to-time.pipe';
import {Router} from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnInit, AfterViewInit, OnDestroy {
  static tableNumber = 0;
  tableId: string;
  subscription: Subscription = new Subscription();
  @Input() tableName: string;
  // configurations
  @Input() paginationOff = false; // for sync fetch
  @Input() header: any[] = [];
  @Input() checkable = false;
  @Input() filter: string |
    ((filterValue: any, row: any) => boolean) |
    ((filterValue: any) => LoopBackFilter) = null;
  filterValue: any = null;
  sorter: { col: any, order: boolean } = null;

  // for async filter
  @Input() fetch: (args: any, reset: boolean) => Observable<{ items: any[], total: number }>;
  fetching = false;
  selectedIds: number[] = [];

  // for paging
  @ViewChild('paginationControls') paginationControls: PaginationControlsComponent;
  @Input() itemsPerPage = 10;
  paginatePipe: PaginatePipe;

  @Input() onCheckChanged: () => Observable<void>;
  @Input() onRowClick: (args: any) => Observable<any>;

  // rows
  _rows: any[] | { items: any[], total: number } = null;
  @Input()
  set rows(rows) {
    this._rows = rows;

    // call render in sync table when rows are reset
    if (!this.fetch) {
      this.render(0, this.filterValue);
    }
  }

  get rows() {
    return this._rows;
  }

  // lazyInit
  private inited = false;
  private renderQueue = null;
  private isCheckChangedAllowed = true;

  // visible rows
  filteredRows: any[] = [];

  get checkedAll(): boolean {
    return this.filteredRows.length > 0 && this.filteredRows.every(row => row.checked);
  }

  constructor(protected paginationService: PaginationService, protected router: Router) {
    this.tableId = (TableComponent.tableNumber++).toString();
    this.paginatePipe = new PaginatePipe(this.paginationService);
  }

  ngOnInit() {
    // Initialize header
    let datePipe = new DatePipe('en-US');
    let fileSizePipe = new FileSizePipe();
    let secondsToTimePipe = new SecondsToTimePipe();
    this.header.map(col => {
      if (col.format && typeof col.format === 'string') {
        let format = col.format.split(':')[0];
        switch (format) {
          case 'date':
            let dateFormat = col.format.split(':')[1] || 'short';
            col.format = value => value && datePipe.transform(value, dateFormat);
            break;

          case 'fileSize':
            col.format = value => value && fileSizePipe.transform(value);
            break;

          case 'secondsToTime':
            col.format = value => value && secondsToTimePipe.transform(value);
            break;

          default:
            // Error case
            delete col.format;
            break;
        }
      }
      return col;
    });

    // predefined filters
    if (typeof this.filter === 'string') {
      switch (this.filter) {
        case 'search':
          let searchableAttrs = this.header
            .filter(col => col.searchable && col.attr)
            .map(col => col.attr);

          this.filter = (_value, row) => {
            let value = _value.toString().toLowerCase();
            return searchableAttrs
              .some(attr => this.getAttr(row, attr).toString().toLowerCase().indexOf(value) !== -1);
          };
          break;

        // undefined filter
        default:
          this.filter = null;
      }
    }

    // inited
    this.inited = true;
    if (this.renderQueue) {
      this._render.apply(this, this.renderQueue);
    }
  }

  ngAfterViewInit() {
    // paging
    if (this.paginationControls) {
      this.subscription.add(
        this.paginationControls.pageChange
          .subscribe(page => this.render(page, this.filterValue))
      );
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  allowCheckChanged() {
    this.isCheckChangedAllowed = true;
  }

  disallowCheckChanged() {
    this.isCheckChangedAllowed = false;
  }

  fireCheckChanged() {
    if (this.onCheckChanged && this.isCheckChangedAllowed) {
      this.onCheckChanged();
    }
  }

  check(row: any, checked: boolean) {
    row.checked = checked;

    // remember checked ids when using async fetch
    if (this.fetch && row.id) {
      let index = this.selectedIds.indexOf(row.id);
      if (checked && index === -1) {
        this.selectedIds.push(row.id);
      } else {
        if (index !== -1) {
          this.selectedIds.splice(index, 1);
        }
      }
    }

    this.fireCheckChanged();
  }

  rowClick(row) {
    if (this.onRowClick) {
      this.onRowClick(row);
    }
  }

  colClick(row, col) {
    if (col.onClick) {
      col.onClick(row);
    }
  }

  render(...args) {
    if (!this.inited) {
      this.renderQueue = args;
    } else {
      this._render.apply(this, args);
    }
  }

  _render(page = 0, filterValue?: any, callback?: (rows: any[]) => void) {
    // remember filterValue
    this.filterValue = typeof filterValue === 'undefined' ? this.filterValue : filterValue;

    // async paging
    if (this.fetch) {
      // need to reset store?
      let reset = false;
      if (page === 0) {
        page = 1;
        reset = true;
      }
      // console.log('page changed', page);

      let savedRows: any = this.rows || {items: [], total: 0};

      // calculate paging
      let pagingLimit = this.itemsPerPage,
        pagingOffset = (page - 1) * pagingLimit;
      let fetchedTotal = reset ? 0 : savedRows.items.length,
        fetchOffset = fetchedTotal,
        fetchLimit = reset ?
          this.itemsPerPage * page - fetchOffset :
          Math.min(savedRows.total, this.itemsPerPage * page) - fetchOffset;

      // console.log(`reset: ${reset}, ${fetchedTotal} fetched, let's fetch ${fetchOffset} ~ ${fetchLimit}`);

      // need to fetch?
      if (fetchLimit > 0) {
        // make args for loopback angular sdk
        let asyncFilter: any = this.filter;
        let args: any = this.filterValue && asyncFilter ? asyncFilter(this.filterValue) : {};
        args.offset = fetchOffset;
        args.limit = fetchLimit;

        // sorting
        if (this.sorter) {
          let attr = this.sorter.col.sortableAttr || this.sorter.col.attr,
            order = this.sorter.order ? 'desc' : 'asc';
          args.order = `${attr} ${order}`;
        }

        // start fetching
        this.fetching = true;

        this.fetch(args, reset)
          .take(1)
          .subscribe((updatedRows: { items: any[], total: number }) => {

            // finish fetching
            this.fetching = false;

            this.rows = savedRows = reset ?
              updatedRows :
              {
                items: savedRows.items.concat(updatedRows.items),
                total: updatedRows.total
              };

            let pagedItems = savedRows.items.slice(pagingOffset, pagingOffset + pagingLimit);
            this.filteredRows = this.paginatePipe.transform(pagedItems, {
              id: this.tableId,
              itemsPerPage: pagingLimit,
              currentPage: page,
              totalItems: savedRows.total,
            });

            // restore checked rows
            this.filteredRows.forEach(row => {
              row.checked = !row.uncheckable && this.selectedIds.indexOf(row.id) !== -1;
            });

            // console.log(this.tableId, 'table updated (async, fetched)');
            if (callback) {
              callback(this.filteredRows);
            }
          });

        // not need to fetch
      } else {
        let pagedItems = savedRows.items.slice(pagingOffset, pagingOffset + pagingLimit);
        this.filteredRows = this.paginatePipe.transform(pagedItems, {
          id: this.tableId,
          itemsPerPage: pagingLimit,
          currentPage: page,
          totalItems: savedRows.total,
        });
        // console.log(this.tableId, 'table updated (async)');

        // restore checked rows
        this.filteredRows.forEach(row => {
          row.checked = !row.uncheckable && this.selectedIds.indexOf(row.id) !== -1;
        });

        if (callback) {
          callback(this.filteredRows);
        }
      }

      // sync paging
    } else {
      let savedRows: any = this.rows;

      // call fetch when page updated
      if (page === 0) {
        page = 1;
      }
      // console.log('page changed', page);

      this.filteredRows = savedRows;

      // filtering
      if (filterValue && this.filter) {
        let syncFilter: any = this.filter;
        this.filteredRows = this.filteredRows.filter(row => syncFilter(filterValue, row));
      }

      // sorting
      if (this.sorter) {
        let attr = this.sorter.col.sortableAttr || this.sorter.col.attr,
          sign = this.sorter.order ? 1 : -1;
        this.filteredRows.sort((a, b) => {
          let aVal = this.getAttr(a, attr),
            bVal = this.getAttr(b, attr);
          return (aVal < bVal) ? -sign : ((aVal > bVal) ? sign : 0);
        });
      }

      // pagination
      if (!this.paginationOff) {
        this.filteredRows = this.paginatePipe.transform(this.filteredRows, {
          id: this.tableId,
          itemsPerPage: this.itemsPerPage,
          currentPage: page
        });
      }

      if (callback) {
        callback(this.filteredRows);
      }
      // console.log(this.tableId, 'table updated (sync)');
    }
  }

  sort(index) {
    let col = this.header[index];

    // toggle
    if (this.sorter && this.sorter.col === col) {
      this.sorter.order = !this.sorter.order;

      // new sorter
    } else {
      this.sorter = {col: col, order: true};
    }

    // reset page
    this.render(0, this.filterValue);
  }

  checkAll($event) {
    this.disallowCheckChanged();
    this.filteredRows.forEach(row => {
      if (!row.uncheckable) {
        this.check(row, $event.checked);
      }
    });
    this.allowCheckChanged();
    this.fireCheckChanged();
  }

  getAttr(row, attr) {
    let val = '';
    // for relation model attr and model value
    if (attr) {
      let attrs = attr.split('.');
      while (row && attrs.length > 0) {
        row = row[attrs.shift()];
      }
      val = row;
    }

    // for null value
    if (val === undefined || val === null) {
      val = '';
    }

    return val;
  }

  getContent(row, col) {
    let val = this.getAttr(row, col.attr);

    if (col.getValue) {
      val = col.getValue(val, row);
    } else if (col.format) {
      val = col.format(val);
    }

    return val;
  }

  getClass(row, col) {
    let _class = '';
    if (col.asIcon) {
      _class += this.getColIcon(row, col);
    }

    if (!!col.onClick) {
      _class += 'idCursor';
    }

    return _class;
  }

  getCustomIcon(active, positive_icon, negative_icon) {
    return active ?
      typeof positive_icon === 'string' ? positive_icon : 'check_circle' :
      typeof negative_icon === 'string' ? negative_icon : 'block';
  }

  getIcon(active) {
    return this.getCustomIcon(active, null, null);
  }

  getColIcon(row, col) {
    return this.getIcon(this.getContent(row, col));
  }

  getImage(row, col) {
    if (col.withImage) {
      return col.withImage(row);
    }
  }

}
