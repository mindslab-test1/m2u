import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit, OnDestroy {

  @Input() name: string;
  @Input() actions: any;
  @Input() goBack: any;
  @Input() showMoreVert = true;

  get actionsArray() {
    let arr = [];
    for (let k in this.actions) {
      if (k) {
        this.actions[k].type = k;
        arr.push(this.actions[k]);
      }
    }
    return arr;
  }

  subscription = new Subscription();

  constructor(private store: Store<any>) {
  }

  ngOnInit() {
    if (this.name) {
      return;
    }
    this.subscription.add(
      this.store.select(s => s.router)
      .subscribe(router => {
        let currentNav = (function findLastActiveNav(navs, activeNav) {
          let childActiveNav = navs && navs.find(nav => nav.active);
          if (childActiveNav) {
            return findLastActiveNav(childActiveNav.children, childActiveNav);
          } else {
            return activeNav;
          }
        })(router.navs, null);
        this.name = currentNav && currentNav.name || null;
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  doAction(type) {
    let act = this.actions[type];
    if (act) {
      act.callback();
    }
  }

  doGoBack() {
    // 이전화면으로 이동합니다
    window.history.back();
  }
}
