import {Component, Input, OnChanges, OnInit, ViewChild, TemplateRef} from '@angular/core';
import {FileUploader, FileUploaderOptions} from 'ng2-file-upload';
import {TableComponent} from '../table/table.component';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
})

export class UploaderComponent implements OnInit, OnChanges {
  uploader = new FileUploader({
    url: '',
    autoUpload: true
  });
  hasDropZoneOver = false;
  successCount = 0;
  @Input() url = '';
  @Input() fileTypes: string[] = null;
  @Input() accept = '.*';
  @Input() onComplete: () => void = null;
  @Input() onUpload: (item: any) => void = null;
  @Input() onError: (error: any) => void = null;

  // queue
  table: any;
  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('statusTemplate') statusTemplate: TemplateRef<any>;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;

  constructor() {
    // call event hooks
    this.uploader.onErrorItem = (item: any, res: any) => {
      let response = JSON.parse(res) || {};
      if (response.error) {
        item.error = response.error;
        if (this.onError) {
          this.onError(item.error);
        }
      }
    };
    this.uploader.onSuccessItem = (item: any, res: any) => {
      if (this.onUpload) {
        let file = JSON.parse(res);
        this.onUpload(file);
      }
      this.successCount++;
    };
    this.uploader.onCompleteAll = () => {
      if (this.successCount > 0 && this.onComplete) {
        this.onComplete();
      }
      this.successCount = 0;
    };
  }

  ngOnInit() {
    // queue table
    this.table = {
      header: [
        {
          attr: 'file.name',
          name: 'Name',
          sortable: true,
          width: '300px',
        },
        {
          attr: 'file.type',
          name: 'Type',
          sortable: true,
          width: '150px',
        },
        {
          attr: 'file.size',
          name: 'Size',
          format: 'fileSize',
          sortable: true,
          width: '150px',
        },
        {
          name: 'Status',
          template: this.statusTemplate,
          sortable: true,
          sortableAttr: 'isSuccess',
        },
        {
          name: 'Action',
          template: this.actionTemplate,
          width: '100px',
        },
      ],
    };
  }

  ngOnChanges(changes: any) {
    let options: FileUploaderOptions = {};
    // set backend url
    if (changes.url) {
      options.url = changes.url['currentValue'];
    }

    // apply filter
    if (changes.fileTypes) {
      options.allowedMimeType = changes.fileTypes['currentValue'];
    }
    this.uploader.setOptions(options);
  }

  fileOverDropZone(e: any) {
    this.hasDropZoneOver = e;
  }
}
