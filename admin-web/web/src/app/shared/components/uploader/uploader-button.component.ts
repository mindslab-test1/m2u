import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FileUploader, FileUploaderOptions} from 'ng2-file-upload';

@Component({
  selector: 'app-uploader-button',
  templateUrl: './uploader-button.component.html',
})

export class UploaderButtonComponent implements OnInit, OnChanges {
  uploader = new FileUploader({
    url: '',
    autoUpload: true
  });
  hasDropZoneOver = false;
  successCount = 0;
  @Input() url = '';
  @Input() fileTypes: string[] = null;
  @Input() accept = '.*';
  @Input() onComplete: () => void = null;
  @Input() onUpload: (item: any) => void = null;
  @Input() onError: (error: any) => void = null;

  constructor() {
    // call event hooks
    this.uploader.onErrorItem = (item: any, res: any) => {
      if (res === '') {
        if (this.onError) {
          this.onError(item);
        }
        return;
      }

      let response = JSON.parse(res) || {};
      if (response.error) {
        item.error = response.error;
        if (this.onError) {
          this.onError(item.error);
        }
      }
    };

    this.uploader.onSuccessItem = (item: any, res: any) => {
      if (this.onUpload) {
        let file = JSON.parse(res);
        this.onUpload(file);
      }
      this.successCount++;
    };
    this.uploader.onCompleteAll = () => {
      if (this.successCount > 0 && this.onComplete) {
        this.onComplete();
      }
      this.successCount = 0;
    };
  }

  ngOnInit() {
  }

  ngOnChanges(changes: any) {
    let options: FileUploaderOptions = {};
    // set backend url
    if (changes.url) {
      options.url = changes.url['currentValue'];
    }

    // apply filter
    if (changes.fileTypes) {
      options.allowedMimeType = changes.fileTypes['currentValue'];
    }
    this.uploader.setOptions(options);
  }
}
