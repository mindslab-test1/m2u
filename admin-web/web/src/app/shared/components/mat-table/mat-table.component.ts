import {
  AfterViewInit, Component, DoCheck, EventEmitter, Input, IterableDiffers,
  OnInit, Output, ViewChild,
} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-mat-table',
  templateUrl: './mat-table.component.html',
  styleUrls: ['./mat-table.component.scss'],
})

export class MatTableComponent implements OnInit, AfterViewInit, DoCheck {

  @Input() headers: any[] = [];
  @Input() rows: any[] = [];
  differ: any;

  @Input() dataSource: MatTableDataSource<any>;
  @Input() filterPlaceholder: any;
  @Input() isFilter: any;

  @Input() isShowPaginator = true;

  @Input() onRowClick: (args: any) => Observable<any>;

  @Output() outputSelectedRow = new EventEmitter<any>();
  @Output() outputCheckedRow = new EventEmitter<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  isCheckedAll: boolean;
  @Output() outputRowInput = new EventEmitter<Object>();

  constructor(private differs: IterableDiffers) {
    this.differ = differs.find([]).create(null);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  ngDoCheck() {
    let changesRow = this.differ.diff(this.rows);

    if (changesRow) {
      this.dataSource = new MatTableDataSource(changesRow.collection);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    console.log('filter : ', this.dataSource.filter);
  }

  getColumn(headers) {
    let arr: any[] = [];
    headers.filter(col => col.attr).forEach((col) => {
      arr.push(col.attr);
    });
    return arr;
  }

  onClickCheckAll() {
    /* 페이징 적용했을 때 사용하는 부분 */
    let pageIndex: number = this.paginator.pageIndex;
    let pageSize: number = this.paginator.pageSize;

    let start: number = pageIndex * pageSize;
    let end: number = (pageIndex + 1) * pageSize - 1;

    if (this.paginator.length <= end) {
      end = this.paginator.length - 1;
    }

    /*공통 적용부분*/
    for (let i = start; i < end + 1; i++) {
      this.rows[i].isChecked = this.isCheckedAll;
    }
    this.isChecked();
  }

  isChecked() {
    let checkedRows = this.rows.filter(data => data.isChecked);

    if (checkedRows.length !== 0) {
      this.outputCheckedRow.emit(true);
    } else {
      this.outputCheckedRow.emit(false);
    }
  }

  colClick(row) {
    this.outputSelectedRow.emit(row);
  }

  getButtonClass(buttonColor) {
    return buttonColor === 'accent' ? 'accent-button' : 'primary-button';
  }

  changeInput(event, row, attr) {
    const attrArr = attr.split('.');
    const newAttr = attrArr[attrArr.length - 1];
    let data = Object.assign({}, row);
    data[newAttr] = event.target.value;
    this.outputRowInput.emit(data);
  }

  getAttr(row, attr) {
    let val = '';

    // for relation model attr and model value
    if (attr) {
      let attrs = attr.split('.');
      while (row && attrs.length > 0) {
        row = row[attrs.shift()];
      }
      val = row;
    }

    if (val) {
      return val;
    } else {
      // for null value
      return '';
    }
  }

  rowClick(row) {
    if (this.onRowClick) {
      this.onRowClick(row);
    }
  }

  onClick(row, col) {
    if (col.onClick) {
      col.onClick(row);
    }
  }


  getContent(row, col) {
    let val = this.getAttr(row, col.attr);

    if (col.getValue) {
      val = col.getValue(val, row);
    } else if (col.format) {
      val = col.format(val);
    }

    return val;
  }

  getCustomIcon(active, positive_icon, negative_icon) {
    return active ?
      typeof positive_icon === 'string' ? positive_icon : 'check_circle' :
      typeof negative_icon === 'string' ? negative_icon : 'block';
  }

  getIcon(active) {
    return this.getCustomIcon(active, null, null);
  }

  getColIcon(row, col) {
    return this.getIcon(this.getContent(row, col));
  }

  getClass(row, col) {
    let _class = '';
    if (col.asIcon) {
      _class += this.getColIcon(row, col);
    }

    if (!!col.onClick) {
      _class += 'id-cursor';
    }

    return _class;
  }

  getImage(row, col) {
    if (col.withImage) {
      return col.withImage(row);
    } else if (col.onlyImage) {
      return col.onlyImage(row);
    }
  }

}
