import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatCommonModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatLineModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import {PrettyJsonModule} from 'angular2-prettyjson';
import {NgxPaginationModule} from 'ngx-pagination';
import {ChartsModule} from 'ng2-charts';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {FileUploadModule} from 'ng2-file-upload';
import {NgDragDropModule} from 'ng-drag-drop';
import {DragulaModule, DragulaService} from 'ng2-dragula/ng2-dragula';
// import shared components
import {
  AlertComponent,
  AppEnumsComponent,
  AppObjectManagerService,
  AuthGuardDirective,
  CommitDialogComponent,
  CustomValidators,
  CustomValueUtil,
  DownloadService,
  ErrorDialogComponent,
  ExcelService,
  FileSizePipe,
  FilterPipe,
  FormControlErrorsComponent,
  FormControlInlineComponent,
  FormControlInputComponent,
  FormControlSectionComponent,
  FormErrorStateMatcher,
  KeysPipe,
  KeywordSectionComponent,
  MatTableComponent,
  Nl2BrPipe,
  PromptComponent,
  ScriptSectionComponent,
  SecondsToTimePipe,
  TableComponent,
  TagInputComponent,
  TagInputItemComponent,
  TitleCase,
  ToolsComponent,
  UploaderButtonComponent,
  UploaderComponent
} from './index';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxPaginationModule,
    FileUploadModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    MatCommonModule,
    MatSidenavModule,
    MatMenuModule,
    MatSnackBarModule,
    MatDialogModule,
    MatPaginatorModule,
    MatLineModule,
    MatToolbarModule,
    MatSelectModule,
    MatCardModule,
    MatRippleModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatProgressBarModule,
    MatTooltipModule,
    ChartsModule,
    PerfectScrollbarModule,
    MatStepperModule,
    NgDragDropModule.forRoot(),
    DragulaModule,
    PrettyJsonModule,
    MatChipsModule
  ],
  exports: [
    FormsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxPaginationModule,
    FileUploadModule,
    MatPaginatorModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatTableModule,
    MatTabsModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatCommonModule,
    MatSidenavModule,
    MatMenuModule,
    MatSnackBarModule,
    MatDialogModule,
    MatLineModule,
    MatToolbarModule,
    MatSelectModule,
    MatCardModule,
    MatRippleModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatProgressBarModule,
    MatTooltipModule,
    ChartsModule,
    PerfectScrollbarModule,
    MatStepperModule,
    NgDragDropModule,
    DragulaModule,
    PrettyJsonModule,

    TableComponent, ToolsComponent, TagInputComponent, TagInputItemComponent,
    FormControlErrorsComponent, FormControlInputComponent, FormControlSectionComponent, FormControlInlineComponent,
    AlertComponent, PromptComponent, CommitDialogComponent, ErrorDialogComponent,
    FilterPipe, TitleCase, Nl2BrPipe, FileSizePipe, SecondsToTimePipe, KeysPipe,
    AuthGuardDirective,
    UploaderComponent, UploaderButtonComponent,
    ScriptSectionComponent,
    KeywordSectionComponent, MatTableComponent
  ],
  providers: [
    AppObjectManagerService, DownloadService, ExcelService,
    AppEnumsComponent, CustomValidators, CustomValueUtil, FormErrorStateMatcher,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    DragulaService
  ],
  declarations: [
    TableComponent, ToolsComponent, TagInputComponent, TagInputItemComponent,
    FormControlErrorsComponent, FormControlInputComponent, FormControlSectionComponent, FormControlInlineComponent,
    AlertComponent, PromptComponent, CommitDialogComponent, ErrorDialogComponent,
    FilterPipe, TitleCase, Nl2BrPipe, FileSizePipe, SecondsToTimePipe, KeysPipe,
    AuthGuardDirective, UploaderComponent, UploaderButtonComponent,
    ScriptSectionComponent,
    KeywordSectionComponent, MatTableComponent
  ],
  entryComponents: [
    AlertComponent, PromptComponent,
    CommitDialogComponent, ErrorDialogComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
  ]
})
export class SharedModule {
}
