import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-commit-dialog',
  templateUrl: './commit-dialog.component.html',
})
export class CommitDialogComponent implements OnInit {

  @Input() title: any = '';
  model: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<any>,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.model = this.fb.group({
      title: [this.title, [Validators.required]],
      message: ['', []],
    });
  }

  submit() {
    if (this.model.valid) {
      let result = this.model.value.title.trim();
      let message = this.model.value.message.trim();
      if (message !== '') {
        result += '\n\n' + message;
      }
      this.dialogRef.close(result);
    }
  }

}
