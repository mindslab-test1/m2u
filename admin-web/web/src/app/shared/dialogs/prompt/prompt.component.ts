import {Component, OnInit, Input} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-prompt',
  templateUrl: './prompt.component.html',
})
export class PromptComponent implements OnInit {

  @Input() title: string = null;
  @Input() message: string = null;
  @Input() type = 'text';
  @Input() label = 'Input';
  @Input() value: any = '';
  control: FormControl;

  constructor(public dialogRef: MatDialogRef<any>,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.control = this.fb.control(this.value, [Validators.required]);
  }

  submit() {
    if (this.control.valid) {
      this.dialogRef.close(this.control.value);
    }
  }

}
