import {AuthGuard} from './core/auth.guard';
import {AuthService} from './core/auth.service';
import {
  AppDummyComponent,
  AppUnderConstructionComponent,
  AppPermissionDeniedComponent,
  AppPageNotFoundComponent,
  CategoryEntryComponent,
  AuthComponent,
  AuthSignInComponent,
  AuthSignUpComponent,
  DialogServiceComponent,
  DialogAgentManagerViewComponent,
  DialogAgentsViewComponent,
  DialogAgentManagerUpsertComponent,
  DialogAgentManagerDetailComponent,
  DialogAgentsUpsertComponent,
  DialogAgentsDetailComponent,
  SimpleClassifierViewComponent,
  SimpleClassifierDetailComponent,
  SimpleClassifierUpsertComponent,
  ChatbotsViewComponent,
  ChatbotsDaInstanceDetailComponent,
  ChatbotsUpsertComponent,
  ChatbotsDaInstanceUpsertComponent,
  ChatbotsDetailComponent,
  ChatbotsDainstancesManageComponent,
  IntentFinderPolicyViewComponent,
  IntentFinderPolicyUpsertComponent,
  IntentFinderPolicyDetailComponent,
  IntentFinderInstanceViewComponent,
  IntentFinderInstanceDetailComponent,
  IntentFinderInstanceUpsertComponent,
  IntentFinderInstanceTestComponent,
  TestChatViewComponent,
  DashboardComponent,
  MonitoringComponent,
  AccountsComponent,
  AccountsViewComponent,
  AccountsUpsertComponent,
  AccountProfileComponent,
  DialogStatisticsComponent,
  ChatLogSessionListComponent,
  ChatLogListComponent,
  StatisticsComponent,
  ResponseComponent,
  EngineResultComponent,
  SessionComponent,
  ChannelComponent,
  EngineConfigurationViewComponent,
  EngineConfigurationUpsertComponent,
  EvaluationComponent,
  SatisfactionComponent,
  AccuracyComponent,
  AccuracyEditTableComponent,
  ProcessComponent,
  ServersViewComponent,
  ProcessesViewComponent,
} from './index';
import {ChatLogSessionV3ListComponent} from "./dialog-monitoring/chat-log/chat-log-session-v3-list.component";
import {ChatLogV3ListComponent} from "./dialog-monitoring/chat-log/chat-log-v3-list.component";

export const AppRoutes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/auth/signin',
  },
  {
    path: 'dummy',
    component: AppDummyComponent,
  },
  {
    path: 'denied',
    component: AppPermissionDeniedComponent,
  },
  {
    path: 'auth',
    component: AuthComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          AuthService.ROLE.UNAUTHENTICATED
        ]
      },
      hasNoLayout: true,
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'signin',
      },
      {
        path: 'signin',
        component: AuthSignInComponent,
      },
      {
        path: 'signup',
        component: AuthSignUpComponent,
      },
    ],
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          'dashboard' + AuthService.ROLE.READ
        ]
      },
      fallback: ['denied'],
      nav: {
        name: 'Dashboard',
        comment: '전체적인 상태를 살펴봅니다.',
        icon: 'dashboard',
      },
    }
  },
  {
    path: 'dialog-service',
    component: DialogServiceComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          'dam' + AuthService.ROLE.READ,
          'da' + AuthService.ROLE.READ,
          'sc' + AuthService.ROLE.READ,
          'ta' + AuthService.ROLE.READ,
          'itf' + AuthService.ROLE.READ,
          'aupl' + AuthService.ROLE.READ,
          'stt' + AuthService.ROLE.READ,
          'chbt' + AuthService.ROLE.READ,
          'fcd' + AuthService.ROLE.READ,
        ]
      },
      nav: {
        name: 'Dialog Service',
        comment: '대화서비스 관리',
        icon: 'chat',
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dam',
      },
      {
        path: 'dam',
        component: DialogAgentManagerViewComponent,
        data: {
          ACL: {
            roles: [
              'dam' + AuthService.ROLE.READ,
            ]
          },
          nav: {
            name: 'Dialog Agent Manager',
            comment: '대화에이전트 관리자',
            icon: 'dns',
          },
        },
      },
      {
        path: 'dam-upsert',
        component: DialogAgentManagerUpsertComponent,
        data: {
          ACL: {
            roles: [
              'dam' + AuthService.ROLE.CREATE,
              'dam' + AuthService.ROLE.UPDATE
            ]
          },
          fallback: ['dam']
        },
      },
      {
        path: 'dam-detail',
        component: DialogAgentManagerDetailComponent,
        data: {
          ACL: {
            roles: [
              'dam' + AuthService.ROLE.READ
            ]
          },
          fallback: ['dam']
        },
      },
      {
        path: 'da',
        component: DialogAgentsViewComponent,
        data: {
          ACL: {
            roles: [
              'da' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Dialog Agents',
            comment: '대화에이전트',
            icon: 'person',
          },
        }
      },
      {
        path: 'da-upsert',
        component: DialogAgentsUpsertComponent,
        data: {
          ACL: {
            roles: [
              'da' + AuthService.ROLE.CREATE,
              'da' + AuthService.ROLE.UPDATE,
            ]
          },
          fallback: ['da']
        }
      },
      {
        path: 'da-detail',
        component: DialogAgentsDetailComponent,
        data: {
          ACL: {
            roles: [
              'da' + AuthService.ROLE.READ
            ]
          },
          fallback: ['da']
        }
      },
      {
        path: 'sc',
        component: SimpleClassifierViewComponent,
        data: {
          ACL: {
            roles: [
              'sc' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Simple Classifier',
            comment: '단순분류기',
            icon: 'subject',
          },
        }
      },
      {
        path: 'sc-detail',
        component: SimpleClassifierDetailComponent,
        data: {
          ACL: {
            roles: [
              'sc' + AuthService.ROLE.READ
            ]
          },
          fallback: ['sc']
        }
      },
      {
        path: 'sc-upsert',
        component: SimpleClassifierUpsertComponent,
        data: {
          ACL: {
            roles: [
              'sc' + AuthService.ROLE.CREATE,
              'sc' + AuthService.ROLE.UPDATE
            ]
          },
          fallback: ['sc']
        }
      },
      {
        path: 'itfp',
        component: IntentFinderPolicyViewComponent,
        data: {
          ACL: {
            roles: [
              'itf' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Intent Finder Policy',
            comment: '의도 추론기',
            icon: 'view_list',
          },
        }
      },
      {
        path: 'itfp-detail',
        component: IntentFinderPolicyDetailComponent,
        data: {
          ACL: {
            roles: [
              'itf' + AuthService.ROLE.READ
            ]
          },
          fallback: ['itf']
        }
      },
      {
        path: 'itfp-upsert',
        component: IntentFinderPolicyUpsertComponent,
        data: {
          ACL: {
            roles: [
              'itf' + AuthService.ROLE.CREATE,
              'itf' + AuthService.ROLE.UPDATE
            ]
          },
          fallback: ['itf']
        }
      },
      {
        path: 'itfi',
        component: IntentFinderInstanceViewComponent,
        data: {
          ACL: {
            roles: [
              'itf' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Intent Finder Instance',
            comment: '의도 추론기',
            icon: 'view_list',
          },
        }
      },
      {
        path: 'itfi-detail',
        component: IntentFinderInstanceDetailComponent,
        data: {
          ACL: {
            roles: [
              'itf' + AuthService.ROLE.READ
            ]
          },
          fallback: ['itfi']
        }
      },
      {
        path: 'itfi-upsert',
        component: IntentFinderInstanceUpsertComponent,
        data: {
          ACL: {
            roles: [
              'itf' + AuthService.ROLE.CREATE,
              'itf' + AuthService.ROLE.UPDATE
            ]
          },
          fallback: ['itfi']
        }
      },
      {
        path: 'itfi-test',
        component: IntentFinderInstanceTestComponent,
        data: {
          ACL: {
            roles: [
              'itf' + AuthService.ROLE.READ,
            ]
          },
          fallback: ['itfi']
        }
      },
      {
        path: 'chatbots',
        component: ChatbotsViewComponent,
        data: {
          ACL: {
            roles: [
              'chbt' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Chatbots',
            comment: '챗봇 관리',
            icon: 'dashboard',
          },
        }
      },
      {
        path: 'chatbots-upsert',
        component: ChatbotsUpsertComponent,
        data: {
          ACL: {
            roles: [
              'chbt' + AuthService.ROLE.CREATE,
              'chbt' + AuthService.ROLE.UPDATE
            ]
          },
          fallback: ['chatbots']
        }
      },
      {
        path: 'chatbots-detail',
        component: ChatbotsDetailComponent,
        data: {
          ACL: {
            roles: [
              'chbt' + AuthService.ROLE.READ
            ]
          },
          fallback: ['chatbots']
        }
      },
      {
        path: 'chatbots-dainstances-manage',
        component: ChatbotsDainstancesManageComponent,
        data: {
          ACL: {
            roles: [
              'dai' + AuthService.ROLE.READ
            ]
          },
          fallback: ['chatbots']
        }
      },
      {
        path: 'chatbots-dainstance-detail',
        component: ChatbotsDaInstanceDetailComponent,
        data: {
          ACL: {
            roles: [
              'dai' + AuthService.ROLE.READ
            ]
          },
          fallback: ['chatbots-dainstances-manage']
        }
      },
      {
        path: 'chatbots-dainstance-upsert',
        component: ChatbotsDaInstanceUpsertComponent,
        data: {
          ACL: {
            roles: [
              'dai' + AuthService.ROLE.CREATE,
              'dai' + AuthService.ROLE.UPDATE
            ]
          },
          fallback: ['chatbots-dainstances-manage']
        }
      },
      {
        path: 'testchat',
        component: TestChatViewComponent,
        data: {
          ACL: {
            roles: [
              'fcd' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Test Chat',
            comment: '테스트 채팅',
            icon: 'record_voice_over',
          },
        }
      },
    ],
  },
  {
    path: 'dialog-monitoring',
    component: CategoryEntryComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          'svd' + AuthService.ROLE.READ
        ]
      },
      nav: {
        name: 'Dialog Monitoring',
        comment: '대화 모니터링',
        icon: 'network_check',
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'view-session-v3-list',
      },
      // {
      //   path: 'statistics',
      //   component: DialogStatisticsComponent,
      //   data: {
      //     ACL: {
      //       roles: [
      //         AuthService.ROLE.AUTHENTICATED
      //       ]
      //     },
      //     nav: {
      //       name: 'Statistics',
      //       comment: '대화 통계 보기',
      //       icon: 'insert_chart',
      //     },
      //   }
      // },
      {
        path: 'view-session-v3-list',
        component: ChatLogSessionV3ListComponent,
        data: {
          ACL: {
            roles: [
              AuthService.ROLE.AUTHENTICATED
            ]
          },
          nav: {
            name: 'View Sessions V3',
            comment: '세션 목록 보기',
            icon: 'insert_chart',
          },
        }
      },
      {
        path: 'view-session-list',
        component: ChatLogSessionListComponent,
        data: {
          ACL: {
            roles: [
              AuthService.ROLE.AUTHENTICATED
            ]
          },
          nav: {
            name: 'View Sessions',
            comment: '세션 목록 보기',
            icon: 'insert_chart',
          },
        }
      },
      {
        path: 'chat-log-v3-list',
        component: ChatLogV3ListComponent,
        data: {
          ACL: {
            roles: [
              AuthService.ROLE.AUTHENTICATED
            ]
          }
        }
      },
      {
        path: 'chat-log-list',
        component: ChatLogListComponent,
        data: {
          ACL: {
            roles: [
              AuthService.ROLE.AUTHENTICATED
            ]
          }
        }
      },
      // {
      //   path: 'search-realtime-dialog',
      //   component: AppUnderConstructionComponent,
      //   data: {
      //     ACL: {
      //       roles: [
      //         AuthService.ROLE.AUTHENTICATED
      //       ]
      //     },
      //     nav: {
      //       name: 'Search Dialog',
      //       comment: '실시간 대화 검색',
      //       icon: 'build',
      //     },
      //   }
      // },
    ],
  },
  {
    path: 'monitoring',
    component: ProcessComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          'svd' + AuthService.ROLE.READ,
        ]
      },
      fallback: ['denied'],
      nav: {
        name: 'Process Monitoring',
        comment: '프로세스 상태 보기',
        icon: 'airplay',
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'server',
      },
      {
        path: 'server',
        component: ServersViewComponent,
        data: {
          ACL: {
            roles: [
              'svd' + AuthService.ROLE.READ,
            ]
          },
          nav: {
            name: 'Servers',
            comment: 'Server 목록 보기',
            icon: 'computer',
          }
        }
      },
      {
        path: 'process',
        component: ProcessesViewComponent,
        data: {
          ACL: {
            roles: [
              'svd' + AuthService.ROLE.READ,
            ]
          },
          nav: {
            name: 'Processes',
            comment: 'Process 목록 보기',
            icon: 'view_module',
          },
        }
      }
    ]
  },
  {
    path: 'management',
    component: AccountsComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          AuthService.ROLE.ADMIN,
          'egn' + AuthService.ROLE.READ
        ]
      },
      nav: {
        name: 'Management',
        comment: '계정/엔진 관리',
        icon: 'account_box',
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'accounts',
      },
      {
        path: 'accounts',
        component: AccountsViewComponent,
        data: {
          ACL: {
            roles: [
              AuthService.ROLE.ADMIN
            ]
          },
          nav: {
            name: 'Accounts',
            comment: '계정 보기',
            icon: 'account_box',
          },
        }
      },
      {
        path: 'accounts-upsert',
        component: AccountsUpsertComponent,
        data: {
          ACL: {
            roles: [
              AuthService.ROLE.ADMIN
            ]
          },
          fallback: ['view']
        }
      },
      {
        path: 'engine-configuration',
        component: EngineConfigurationViewComponent,
        data: {
          ACL: {
            roles: [
              'egn' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Engine Configure',
            comment: 'DA 엔진 Config',
            icon: 'subject',
          },
        }
      },
      // {
      //   path: 'engine-detail',
      //   component: EngineConfigureDetailComponent,
      //   data: {
      //     ACL: {
      //       roles: [
      //         'sc' + AuthService.ROLE.READ
      //       ]
      //     },
      //     fallback: ['sc']
      //   }
      // },
      {
        path: 'engine-upsert',
        component: EngineConfigurationUpsertComponent,
        data: {
          ACL: {
            roles: [
              'egn' + AuthService.ROLE.CREATE,
              'egn' + AuthService.ROLE.UPDATE
            ]
          },
          fallback: ['engine-configuration']
        }
      }
    ],
  },
  {
    path: 'profile',
    component: AccountsComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          'svd' + AuthService.ROLE.READ
        ]
      },
      nav: {
        name: 'Profile',
        comment: '프로필',
        icon: 'account_box',
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'view',
      },
      {
        path: 'view',
        component: AccountProfileComponent,
        data: {
          ACL: {
            roles: [
              'svd' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'View Profile',
            comment: '프로필 보기',
            icon: 'account_box',
          }
        }
      }
    ],
  },
  {
    path: 'evaluation',
    component: EvaluationComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          'stfc' + AuthService.ROLE.READ,
          'accr' + AuthService.ROLE.READ
        ]
      },
      nav: {
        name: 'Evaluation',
        comment: '평가',
        icon: 'account_box',
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'satisfaction',
      },
      {
        path: 'satisfaction',
        component: SatisfactionComponent,
        data: {
          ACL: {
            roles: [
              'stfc' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Satisfaction',
            comment: '만족도',
            icon: 'account_box',
          }
        }
      },
      {
        path: 'accuracy',
        component: AccuracyComponent,
        data: {
          ACL: {
            roles: [
              'accr' + AuthService.ROLE.UPDATE
            ]
          },
          nav: {
            name: 'Accuracy',
            comment: '정확도',
            icon: 'build',
          }
        }
      }
    ],
  },
  {
    path: 'statistics',
    component: StatisticsComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          'svd' + AuthService.ROLE.READ
        ]
      },
      nav: {
        name: 'Statistics',
        comment: '통계',
        icon: 'insert_chart',
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'response'
      },
      {
        path: 'response',
        component: ResponseComponent,
        data: {
          ACL: {
            roles: [
              'da' + AuthService.ROLE.AUTHENTICATED
            ]
          },
          nav: {
            name: 'Response',
            comment: '응답결과',
            icon: 'multiline_chart',
          }
        }
      },
      {
        path: 'engine-result',
        component: EngineResultComponent,
        data: {
          ACL: {
            roles: [
              'egn' + AuthService.ROLE.READ
            ]
          },
          nav: {
            name: 'Engine Result',
            comment: '엔진 결과',
            icon: 'bubble_chart',
          }
        }
      },
      {
        path: 'channel',
        component: ChannelComponent,
        data: {
          ACL: {
            roles: [
              'egn' + AuthService.ROLE.AUTHENTICATED
            ]
          },
          nav: {
            name: 'Channel',
            comment: '채널분포',
            icon: 'input',
          }
        }
      },
      // { // #@ session statistics 추가
      //   path: 'session',
      //   component: SessionComponent,
      //   data: {
      //     ACL: {
      //       roles: [  // #@ Rest 권한 설정
      //         AuthService.ROLE.AUTHENTICATED
      //       ]
      //     },
      //     nav: {
      //       name: 'Session',
      //       comment: 'Session',
      //       icon: 'insert_chart',
      //     }
      //   }
      // }
    ],
  },
  {
    path: '**',
    component: AppPageNotFoundComponent,
    data: {
      hasNoLayout: true,
    },
  },
];

export const AppNavs = getNavs(AppRoutes);

// routes로 부터 네비게이션 추출
export function getNavs(routes: any[], navs: any[] = [], paths: string[] = []): any[] {
  if (!routes) {
    return navs;
  }

  let childNavs = routes

  // chlidren도 없고 nav도 아닌 abstract route를 필터링
  .filter(route => {
    return (route.data && route.data.nav) || route.children;
  })

  // 재귀적으로 nav 생성
  .map(route => {
    let childPaths = paths.concat([route.path]).filter(p => p !== '');

    // nav가 아님 (abstract route)
    if (!route.data || !route.data.nav) {
      // console.log(route.children);
      return getNavs(route.children, [], childPaths);
    }

    // nav인 경우
    let childNav: any = Object.assign({}, {
      paths: childPaths,
      active: false,
      open: false,
      ACL: route.data.ACL,
    }, route.data.nav);

    if (!childNav.ACL) {
      delete childNav.ACL;
    }

    if (route.children) {
      let childNavChildren = getNavs(route.children, [], childPaths);
      if (childNavChildren.length !== 0) {
        childNav.children = childNavChildren;
      }
    }

    return [childNav];
  })

  // flatten
  .reduce((arr, arr2) => arr.concat(arr2), []);

  return navs.concat(childNavs);
}
