import {AfterViewInit, ApplicationRef, Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormControl} from '@angular/forms';
import {BaseChartDirective} from 'ng2-charts';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../core/actions';
import {StatisticsApi} from '../../core/sdk/services/custom/Statistics';
import {ExcelService} from '../../shared/services/excel.service';
import {AppEnumsComponent} from '../../shared/values/app.enums';
import {TableComponent} from '../../shared/index';
import {Subscription} from 'rxjs/Subscription';
import {getErrorString} from '../../shared';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})

export class SessionComponent implements OnInit, OnDestroy, AfterViewInit {

  connectorType: string;
  selectedSearchOption: string;
  keyword: string;
  now = new Date();
  endDate = new FormControl(this.now);
  startDate = new FormControl(new Date(this.now.getFullYear(), this.now.getMonth() - 1, this.now.getDate()));
  table: any;
  dailytable: any;
  subscription = new Subscription();

  // 라인차트 변수 선언
  lineChartLabels = []; // 라인차트의 시간(가로)

  lineChartData = [ // 라인차트의 세션(세로)
    {data: [], label: ''}
  ];
  lineChartOptions = {
    responsive: true
  };
  lineChartLegend = true;
  lineChartType = 'line';

  requestParam = {
    startAt: new FormControl(''),
    endAt: new FormControl(''),
    periodType: ''
  };
  startAt: string;
  endAt: string;

  activeDownload = true;  // 다운로드 버튼 활성화 변수 (false가 되면 활성화)

  successResult = [];
  successQuery = false;
  totalResult = 0;

  tabLabels = ['Session Statistics'];
  tabLabel = '';

  resList = ['동시 세션(수)'];

  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;
  chart: Array<any> = [];

  constructor(private store: Store<any>,
              public appEnum: AppEnumsComponent,
              private statisticsApi: StatisticsApi,
              private excelService: ExcelService,
              private ref: ApplicationRef) {
    this.tabLabel = this.tabLabels[0];
  }

  ngOnInit() {
    this.onAnalysis();
  }

  ngOnDestroy() {
    console.log('response destroy');
  }

  ngAfterViewInit() {
    this.parseCharts();
  }

  parseCharts() {
    // Get chart object
    this.charts.forEach((child) => {
      this.chart.push(child);
    });
  }

  // events
  chartHovered(e) {
    console.log('hover', e);
  }

  // events
  chartClicked(e: any): void {
    console.log('click', e);
  }

  // #@ 탭 클릭 함수
  onTabClick(event) {
    this.tabLabel = event.tab.textLabel;
    console.log('#@ tabLabel', this.tabLabel);
    // TODO 추후 사용
  }

  // #@ 날짜 변경 이벤트 함수
  changeDateEvent(event) {
    // 시작 날짜와 종료 날짜가 있어야 다운로드 버튼 활성화 처리
    if (!!this.requestParam.startAt.value && !!this.requestParam.endAt.value) {
      let date = this.requestParam.startAt.value;
      this.startAt = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
      date = this.requestParam.endAt.value;
      this.endAt = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
      this.activeDownload = false;
    }
  }

  // #@ 다운로드 함수
  onDownload() {
    // 일자 별 누적 세션 수 통계 추출
    let query = {
      'queryParams': {
        'startAt': this.startAt,
        'endAt': this.endAt
      }
    };

    let self = this;
    this.statisticsApi.executeDailySessionDownloadQuery(query).subscribe(
      res => {
        console.log('#@ executeDailySessionDownloadQuery :', res);
        let name = 'Session-' + new Date().toLocaleString();
        self.excelService.exportAsExcelFile(res, name, false);
      }
    );
  }

  // #@ 분석 시작 함수
  onAnalysis() {
    this.onAnalyzeTimeGraph();
  }

  // 1.시간 별 동시 세션 수
  onAnalyzeTimeGraph() {
    this.lineChartLabels = [];      // 라인차트의 시간(가로)
    this.lineChartData = [];        // 라인차트의 세션(세로)
    let category_res = {};          // 임시 카테고리
    let tempDailySessionData = [];  // 임시 테이블의 세션 데이터
    let tempDailySessionCnt = [];   // 임시 테이블의 세션 수

    this.resList.forEach(category => {
      category_res[category] = [];
      this.lineChartData.push({label: category, data: []}); // #@ 1.라인차트의 데이터 주입
    });

    // rest API
    this.statisticsApi.executeTimeSessionQuery().subscribe(res => {
        tempDailySessionData = res;

        console.log('#@ executeTimeSessionQuery res2 :', tempDailySessionData);

        tempDailySessionData.forEach(resDate => {
          this.lineChartLabels.push(resDate.time);
          tempDailySessionCnt.push(resDate.sessionCnt);
        });

        this.lineChartData.forEach(line => {
          line.data = tempDailySessionCnt;
        });

        this.refreshLineChart();
        this.onAnalyzeTimeTable(tempDailySessionData);
      }, err => console.log(err)
    );


    // 초기 설정을 위해 호출 한다(호출 안하면 에러)
    this.onAnalyzeTimeTable(tempDailySessionData);
    this.onAnalyzeDailyTable();
  }

  // 1.시간 별 동시 세션 수 테이블
  onAnalyzeTimeTable(resSessionData) {
    this.table = {
      header: [
        {attr: 'time', name: '시간', sortable: true},
        {attr: 'sessionCnt', name: '동시 세션(수)', sortable: true}
      ],
      rows: [],
      paginationOff: true
    };

    this.table.rows = resSessionData;
  }

  // 2.일자 별 누적 세션 수 테이블
  onAnalyzeDailyTable() {
    this.dailytable = {
      header: [
        {attr: 'date', name: '일', sortable: true},
        {attr: 'totCnt', name: '토탈 세션(수)', sortable: true},
        {attr: 'totTime', name: '총 접속시간', sortable: true},
        {attr: 'totCall', name: '총 호출(수)', sortable: true}
      ],
      rows: [],
      paginationOff: true
    };

    // 2.rest API
    this.statisticsApi.executeDailySessionQuery().subscribe(
      res => {
        console.log('#@ executeDailySessionQuery res :', res);
        this.dailytable.rows = res;
        this.store.dispatch({type: ROUTER_LOADED});
      }, err => console.log(err)
    );
  }

  // Force refresh line chart
  refreshLineChart() {
    this.ref.tick();
    this.chart[0].refresh();
  }
}
