import {
  ApplicationRef, Component, OnDestroy, OnInit, AfterViewInit, QueryList,
  ViewChildren
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../core/actions';
import {AppEnumsComponent} from '../../shared/values/app.enums';
import {EngineApi} from '../../core/sdk/services/custom/Engine';
import {StatisticsApi} from '../../core/sdk/services/custom/Statistics';
import {BaseChartDirective} from 'ng2-charts';

@Component({
  selector: 'app-engine-result',
  templateUrl: './engine-result.component.html',
  styleUrls: ['./engine-result.component.scss'],
})


export class EngineResultComponent implements OnInit, OnDestroy, AfterViewInit {
  public pieChartLabels = [];
  public pieChartData: number[] = [];
  public pieChartConfig: any = {
    drawFlag: false,
    chartType: 'pie',
    legend: false,
    options: {
      title: {
        display: true,
        text: ''
      }
    }
  };

  public lineChartLabels = [];
  public lineChartData = [
    {data: [], label: ''}];

  public lineChartOptions = {
    responsive: true
  };
  public lineChartLegend = true;
  public lineChartType = 'line';

  periodTypes: any;
  requestParam = {
    engine: undefined,
    bookmark: undefined,
    categories: undefined,
    startAt: new FormControl(''),
    endAt: new FormControl(''),
    periodType: ''
  };
  startAt: string;
  endAt: string;

  engineList = [];

  currentEngine = {
    bookmarkList: [],
    categories: [],
    name: undefined
  };
  selectedBookmark = '';

  activeAnalysis = true;
  activePeriod = true;

  engineResult = [];
  successQuery = false;
  totalResult = 0;

  tabLabels = ['Pie Chart', 'Time Series'];
  tabLabel = '';

  bookmarks = [];

  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;
  chart: Array<any> = [];
  availableData = true;

  constructor(private store: Store<any>,
              public appEnum: AppEnumsComponent,
              private statisticsApi: StatisticsApi,
              private engineApi: EngineApi,
              private ref: ApplicationRef) {
    this.periodTypes = this.appEnum.periodTypeKeys;
    this.tabLabel = this.tabLabels[0];
    this.requestParam.periodType = this.periodTypes[0];
  }

  ngOnInit() {
    this.engineApi.find().subscribe(res => {
      res.forEach(value => this.engineList.push(value));
    });

    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {

  }

  ngAfterViewInit() {
    this.parseCharts();
  }

  parseCharts() {
    // Get chart object
    this.charts.forEach((child) => {
      this.chart.push(child);
    });
  }

  chartHovered(e) {
    console.log('hover', e);
  }

  // events
  chartClicked(e: any): void {
    console.log('click', e);
  }

  check(event) {

    if (event.source.id === 'name') {
      this.successQuery = false;
      this.requestParam.engine = this.currentEngine.name;
    } else if (event.source.id === 'bookmark') {
      this.successQuery = false;
      this.requestParam.bookmark = this.selectedBookmark;
      this.requestParam.categories = this.currentEngine.categories.filter(category =>
         this.selectedBookmark.indexOf(category.displayName) !== -1);
    } else if (event.source.id === 'periodType') {
      this.activeAnalysis = false;
      this.requestParam.periodType = event.value;
      if (this.successQuery && this.availableData) {
        if (event.value === 'DAILY') {
          this.onAnalyzeDaily();
        } else if (event.value === 'MONTHLY') {
          this.onAnalyzeMonthly();
        }
        this.refreshLineChart();
      }
    }
  }

  changeDateEvent() {
    this.activeAnalysis = true;
    this.successQuery = false;
    if (!!this.requestParam.startAt.value && !!this.requestParam.endAt.value) {
      if (this.activePeriod || !!this.requestParam.periodType) {
        let date = this.requestParam.startAt.value;
        this.startAt = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        date = this.requestParam.endAt.value;
        this.endAt = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        this.activeAnalysis = false;
      }
    }
  }

  onAnalysis() {
    console.log('analyze :', this.requestParam);
    let query = {
      'queryParams': {
        'engine': this.requestParam.engine,
        'startAt': this.startAt,
        'endAt': this.endAt,
        'categories': this.requestParam.categories
      }
    };

    this.statisticsApi.executeEngineQuery(query).subscribe(
      res => {
        this.successQuery = true;
        this.bookmarks = [];
        this.engineResult = res;

        if (res.length === 1 && res[0].date === null) {
          this.availableData = false;
        } else {
          this.availableData = true;
          this.uniformizeDates(this.engineResult, this.startAt, this.endAt);
          this.bookmarks = this.requestParam.bookmark.split(',');
          if (this.tabLabel === this.tabLabels[0]) {
            this.drawPieChart();
            this.refreshPieChart();
          } else if (this.tabLabel === this.tabLabels[1]) {
            this.drawLineChart();
            this.refreshLineChart();
          }
        }
      }, err => console.log(err)
    );
  }

  drawPieChart() {
    this.pieChartLabels = [];
    this.pieChartData = [];

    let self = this;
    this.totalResult = 0;
    this.pieChartLabels = this.bookmarks;

    this.pieChartLabels.forEach(column => {
      self.pieChartData.push(self.engineResult[self.engineResult.length - 1][column]);
      self.totalResult += self.engineResult[self.engineResult.length - 1][column];
    });
    this.pieChartConfig.options.title.text = `Total : ${self.totalResult}`;
  }

  drawLineChart() {
    switch (this.requestParam.periodType) {
      // DAILY
      case this.periodTypes[0]:
        this.onAnalyzeDaily();
        break;
      // MONTHLY
      case this.periodTypes[1]:
        this.onAnalyzeMonthly();
        break;
      default:
        break;
    }
  }

  onAnalyzeDaily() {
    this.lineChartLabels = [];
    this.lineChartData = [];
    let category_res = {};
    this.bookmarks.forEach(category => {
      category_res[category] = [];
      this.lineChartData.push({label: category, data: []});
    });

    this.engineResult.forEach(resp => {
      if (!!resp.date) {
        let date = resp.date.slice(0, 10);
        this.lineChartLabels.push(date);

        this.bookmarks.forEach(category => {
          category_res[category].push(resp[category]);
        });
      }
    });
    this.chart[1].chart.data.labels = this.lineChartLabels;
    this.lineChartData.forEach(line => {
      line.data = category_res[line.label];
    });
    this.chart[1].chart.data.datasets = this.lineChartData;
  }

  onAnalyzeMonthly() {
    this.lineChartLabels = [];
    this.lineChartData = [];
    let month_old = '';
    let cnt_monthly = {};

    this.bookmarks.forEach(category => {
      this.lineChartData.push({label: category, data: []});
      cnt_monthly[category] = 0;
    });

    this.engineResult.forEach(resp => {
      if (!!resp.date) {
        let month = resp.date.slice(0, 7);
        if (month_old === '') {
          month_old = month;
        } else if (month !== month_old) {

          this.lineChartLabels.push(month_old);

          this.chart[1].chart.data.labels = this.lineChartLabels;
          this.lineChartData.forEach(item => {
            item.data.push(cnt_monthly[item.label]);
          });
          month_old = month;
          this.bookmarks.forEach(category => {
            cnt_monthly[category] = 0;
          });
        }
        this.bookmarks.forEach(category => {
          cnt_monthly[category] += resp[category];
        });
      }
    });

    this.lineChartLabels.push(month_old);
    this.chart[1].chart.data.labels = this.lineChartLabels;

    this.lineChartData.forEach(item => {
      item.data.push(cnt_monthly[item.label]);
    });

    this.chart[1].chart.data.datasets = this.lineChartData;
  }

  getTotal(data) {
    return Math.round(data / this.totalResult * 100);
  }

  onTabClick(event) {
    this.tabLabel = event.tab.textLabel;

    // Pie Chart 탭일 때
    if (this.tabLabel === this.tabLabels[0]) {
      this.activePeriod = true;

      // api result 가 있을 때
      if (this.successQuery) {
        this.drawPieChart();
        this.refreshPieChart();
      }
      // Time Series 탭일 때
    } else if (this.tabLabel === this.tabLabels[1]) {
      this.activePeriod = false;
      this.onAnalysis();
    }
  }

  // Force refresh pie chart
  refreshPieChart() {
    this.ref.tick();
    this.chart[0].refresh();
  }

  // Force refresh line chart
  refreshLineChart() {
    this.ref.tick();
    this.chart[1].refresh();
  }

  uniformizeDates(res, startDate, endDate) {
    if (res.length < 1) {
      return;
    }

    let curr = new Date(startDate);
    let end = new Date(endDate);
    let dates = (end.getTime() - curr.getTime()) / (24 * 60 * 60 * 1000) + 2;

    if (res.length === dates) {
      return;
    }

    let index = 0;
    let fill = JSON.parse(JSON.stringify(res[0]));
    let date;

    Object.keys(fill).forEach(key => fill[key] = 0);
    do {
      date = curr.getFullYear() + '-'
        + (curr.getMonth() < 9 ? '0' : '') + (curr.getMonth() + 1) + '-'
        + (curr.getDate() < 10 ? '0' : '') + curr.getDate();

      if (date !== res[index].date) {
        fill.date = date;
        res.splice(index, 0, JSON.parse(JSON.stringify(fill)));
      }
      curr.setDate(curr.getDate() + 1);
      ++index;
    } while (curr <= end);
  }
}
