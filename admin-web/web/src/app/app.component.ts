import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  Input,
  ViewEncapsulation
} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';
import {MatSidenav, MatMenu} from '@angular/material';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core'

import {AuthService} from './core/auth.service';
import {ClipboardService} from './core/clipboard.service';
import {ROUTER_LOADED} from './core/actions';
// import {NotificationApi} from './core/sdk/index';
import {environment} from '../environments/environment';
import {LoopBackConfig} from './core/sdk/lb.config';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit, OnDestroy {
  private static apiUrl = environment.apiPath;
  private static wsUrl = environment.apiPath;

  // API url
  public static getApiUrl(): string {
    return AppComponent.apiUrl;
  }

  // Websocket url
  public static getWsUrl(): string {
    return AppComponent.wsUrl;
  }

  navs: any[] = [];
  currentNav: any;
  isLoading = true;
  hasNoLayout = true;

  user: any = null;
  notifications = [];
  lastTwinkle: number;

  subscription = new Subscription();
  notiHandler: any = null;
  expireHandler: any = null;

  @ViewChild('sideNav') sideNav: MatSidenav;
  @ViewChild('authMenu') authMenu: MatMenu;

  constructor(private store: Store<any>,
              private router: Router,
              private idle: Idle,
              private authService: AuthService,
              // private notificationApi: NotificationApi,
              public clipboardService: ClipboardService) {
    let location = window.location;
    console.log('environment.production :', environment.production);
    if (environment.production) {
      // production mode
      AppComponent.wsUrl = (location.protocol === 'https:' ? 'wss://' : 'ws://')
          + location.host + environment.apiPath;
      LoopBackConfig.setBaseURL('');
      // console.log('production loopBack path :', LoopBackConfig.getPath());
      // console.log('production API path:', AppComponent.getApiUrl());
      // console.log('production WS path :', AppComponent.getWsUrl());
    } else {
      // development mode
      if (!!environment.apiBaseUrl) {
        AppComponent.apiUrl = environment.apiBaseUrl + environment.apiPath;
        LoopBackConfig.setBaseURL(environment.apiBaseUrl);
      }
      if (!!environment.wsBaseUrl) {
        AppComponent.wsUrl = environment.wsBaseUrl + environment.apiPath;
      } else {
        AppComponent.wsUrl = (location.protocol === 'https:' ? 'wss://' : 'ws://')
            + location.host + environment.apiPath;
      }
      console.log('development loopBack path :', LoopBackConfig.getPath());
      console.log('development API path:', AppComponent.getApiUrl());
      console.log('development WS path :', AppComponent.getWsUrl());
    }
  }

  ngOnInit() {
    // user
    this.subscription.add(
        this.store.select(s => s.auth)
            .subscribe(auth => {
              this.user = auth.user;
            })
    );

    this.idle.setIdle(1);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.lastTwinkle = Date.now());
    this.idle.setTimeout(false);
    this.idle.watch();

    // start to check noti
    this.notiHandler = setInterval(() => this.checkNoti(), 30000);
    this.expireHandler = setInterval(() => this.checkExpired(), 10000);

    // 라우팅에 따른 UI 상태
    this.subscription.add(
        this.store.select(s => s.router)
            .subscribe(_router => {
              // sidenav의 메뉴
              this.navs = _router.navs;

              // sidebar의 메뉴
              this.currentNav = _router.navs.find(n => n.active) || null;

              // 로딩 중인지?
              this.isLoading = _router.loading;

              // 레이아웃이 필요 없는지?
              this.hasNoLayout = !!_router.hasNoLayout;

              // reset notification
              this.checkNoti();
            })
    );

  }

  checkNoti() {
    // if (this.user) {
    //   this.notificationApi.find({
    //     where: {
    //       userId: this.user.id
    //     }
    //   })
    //     .subscribe(notis => {
    //       this.notifications = notis;
    //     });
    // } else {
    //   this.notifications = [];
    // }
  }

  checkExpired() {
    if (this.user && !this.authService.isTokenValid()) {
      if (this.authService.isTokenValid(this.lastTwinkle)) {
        this.authService.elongateTokenTTL();
      } else {
        this.authService.logout();
      }
    }
  }

  confirmNoti(id) {
    // this.notificationApi.deleteById(id)
    //   .subscribe(() => {
    //     this.notifications.splice(0, 1);
    //   });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    if (this.notiHandler) {
      clearInterval(this.notiHandler);
    }
    if (this.expireHandler) {
      clearInterval(this.expireHandler);
    }
  }

  profile() {
    this.router.navigateByUrl('profile/view');
  }

  logout() {
    this.authService.logout();
  }

  goto(paths, $event) {
    if (this.sideNav.opened) {
      this.sideNav.onClose.take(1)
          .subscribe(() => {
            if (!paths) {
              this.authService.goHome();
            } else {
              this.router.navigate(paths);
            }
          });
      this.sideNav.close();
    } else {
      if (!paths) {
        this.authService.goHome();
      } else {
        this.router.navigate(paths);
      }
    }
    $event.preventDefault();
  }

  toggleSideNav($event) {
    this.sideNav.toggle();
    $event.preventDefault();
  }
}

