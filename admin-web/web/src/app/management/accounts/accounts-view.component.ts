import {Component, OnInit, OnDestroy, ViewChild, TemplateRef} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSnackBar, MatSidenav} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED, ACNT_UPDATE_ALL, ACNT_DELETE} from '../../core/actions';
import {AuthService} from '../../core/auth.service';
import {AppObjectManagerService, AlertComponent, TableComponent, getErrorString} from '../../shared/index';
import {ConsoleUserApi} from '../../core/sdk/index'
import {AccountsPanelComponent} from './accounts-panel.component';

@Component({
  selector: 'app-accounts-view',
  templateUrl: './accounts-view.component.html',
  styleUrls: ['./accounts-view.component.scss'],
})

export class AccountsViewComponent implements OnInit, OnDestroy {
  page_title: any; // = 'page titles is here.';
  page_description: any; // = 'page description is here.';

  actions: any;
  table: any;
  filterKeyword: FormControl;
  panelToggleText: string;
  username: string;

  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('infoPanel') infoPanel: AccountsPanelComponent;

  constructor(private store: Store<any>,
              private router: Router,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private authService: AuthService,
              private consoleUserApi: ConsoleUserApi,
              private objectManager: AppObjectManagerService) {
  }

  ngOnInit() {
    let user = this.consoleUserApi.getCachedCurrent();
    this.username = user.username;
    let roles = user.roles;
    if (AuthService.isNotAllowed(AuthService.ROLE.ADMIN, roles)) {
      this.authService.logout();
      return;
    }
    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: true,
        hidden: AuthService.isNotAllowed(AuthService.ROLE.ADMIN, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: AuthService.isNotAllowed(AuthService.ROLE.ADMIN, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: AuthService.isNotAllowed(AuthService.ROLE.ADMIN, roles)
      }
    };

    this.table = {
      header: [
        {attr: 'username', name: 'Name', sortable: true, searchable: true, /*onClick: this.navigateAccountDetailView*/},
        {attr: 'email', name: 'Email', sortable: true, searchable: true},
        {attr: 'activated', name: 'Activated', asIcon: true, sortable: true},
      ],
      rows: [],
    };
    this.tableComponent.checkable = !(this.actions.delete.hidden && this.actions.edit.hidden);
    this.tableComponent.onRowClick = this.fillRowInfo;
    this.tableComponent.onCheckChanged = this.whenCheckChanged;

    // subscribe store
    this.subscription.add(
      this.store.select('accounts')
        .subscribe(accounts => {
          if (accounts) {
            this.table.rows = accounts;
            this.table.rows.forEach(row => {
              row.checked = false;
            });
            this.whenCheckChanged();
            setTimeout(() => {
              this.fillRowInfo(this.table.rows[0]);
            }, this.table.rows.length > 0 ? 500 : 0);
          }
        })
    );

    this.consoleUserApi.find().subscribe(
      accounts => {
        this.store.dispatch({type: ACNT_UPDATE_ALL, payload: accounts});
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges
        .debounceTime(300)
        .distinctUntilChanged()
        .subscribe(keyword => this.tableComponent.render(0, keyword))
    );
  }

  /** action callbacks **/
  add: any = () => {
    this.objectManager.clean('account');
    this.router.navigateByUrl('management/accounts-upsert?role=add');
  };

  edit: any = () => {
    let items = this.table.rows.filter(row => row.checked);
    if (items.length === 0) {
      this.snackBar.open('Select items to Edit.', 'Confirm', {duration: 3000});
      return;
    } else if (items.length >= 2) {
      this.snackBar.open('Please select one.', 'Confirm', {duration: 3000});
      return;
    }
    this.objectManager.set('account', items[0]);
    this.router.navigateByUrl('management/accounts-upsert?role=edit');
  };

  delete: any = () => {
    let deleteRows = this.table.rows.filter(row => row.checked);
    if (deleteRows.length === 0) {
      this.snackBar.open('Select items to delete.', 'Confirm', {duration: 3000});
      return;
    }
    if (deleteRows[0].username === this.username) {
      this.snackBar.open(`You shouldn't delete your own account.`, 'Confirm', {duration: 3000});
      return;
    }

    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete '${deleteRows[0].username}?`;
    ref.afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return;
        }

        this.consoleUserApi.deleteById(deleteRows[0].id).subscribe(
          res => {
            // if (res.count === 1) {
            this.store.dispatch({type: ACNT_DELETE, payload: [deleteRows[0].id]});
            this.snackBar.open(`Dialog Agent Manager '${deleteRows[0].username}' Deleted.`, 'Confirm', {duration: 3000});
            // }
          },
          err => {
            let message = `Failed to Delete Dialog Agent Manager '${deleteRows[0].username}'. [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        )
        ;


      });
  };

  togglePanel: any = () => {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  }

  fillRowInfo: any = (row: any) => {
    this.infoPanel.setContent(row);
    if (row === undefined) {
      this.sidenav.close();
      this.panelToggleText = 'Show Info Panel';
    } else {
      this.sidenav.open();
      this.panelToggleText = 'Hide Info Panel';
    }
  }

  whenCheckChanged: any = () => {
    let checkedRows = this.table.rows.filter(row => row.checked);
    switch (checkedRows.length) {
      case 0:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
      case 1:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = checkedRows[0].username === this.username;
        this.actions.edit.disabled = false;
        break;
      default:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
    }
  };

  // navigateAccountDetailView: any = (row: any) => {
  //   this.objectManager.set('account', row);
  //   this.router.navigateByUrl('accounts/detail');
  // }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
