import {Component, Input} from '@angular/core';
import {TableComponent} from '../../shared/components/table/table.component';

@Component({
  selector: 'app-accounts-upsert-table',
  templateUrl: './accounts-upsert-table.component.html',
  styleUrls: ['../../shared/components/table/table.component.scss', './accounts-upsert-table.component.scss']
})
export class AccountsUpsertTableComponent extends TableComponent {

  @Input() inUpsert = false;

  isDisabled(row, col) {
    return typeof this.getAttr(row, col.attr) !== 'boolean';
  }

  getAllowedClass(row, col) {
    return this.getContent(row, col) ? 'allowed-icon mat-accent' : 'allowed-icon';
  }

  getAllowedIcon(row, col) {
    return this.getContent(row, col) ? 'check_box' : 'check_box_outline_blank';
  }

  onRolesChanged(event, row, col) {
    if (col.attr) {
      let attrs = col.attr.split('.');
      while (row && attrs.length > 1) {
        row = row[attrs.shift()];
      }
      row[attrs.shift()] = event.checked;
    }
  }
}
