import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {Store} from '@ngrx/store';

import {AuthService} from '../../core/auth.service';
import {ConsoleUserApi} from '../../core/sdk/index'
import {getErrorString} from '../../shared/index';


@Component({
  selector: 'app-accounts-panel',
  templateUrl: './accounts-panel.component.html',
  styleUrls: ['./accounts-panel.component.scss']
})

export class AccountsPanelComponent implements OnInit {
  user: any;
  table: any;
  roleList: any;
  isAdmin: boolean;
  roles: any;

  constructor(private snackBar: MatSnackBar,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit() {
    if (AuthService.isNotAllowed(AuthService.ROLE.ADMIN, this.consoleUserApi.getCachedCurrent().roles)) {
      return;
    }

    this.table = {
      header: [
        {attr: 'abbr', name: 'Name', width: 83},
        {attr: 'allowed.C', name: 'C', checkbox: true, width: 24},
        {attr: 'allowed.R', name: 'R', checkbox: true, width: 29},
        {attr: 'allowed.U', name: 'U', checkbox: true, width: 29},
        {attr: 'allowed.D', name: 'D', checkbox: true, width: 29},
        {attr: 'allowed.X', name: 'X', checkbox: true, width: 29},
      ],
      rows: [],
    };
  }

  setContent(user: any) {
    if (!user) {
      return;
    }
    new Promise((resolve, reject) => {
        if (!!this.roleList) {
          resolve();
          return;
        }
        this.consoleUserApi.getRoleList().subscribe(roleList => {
            let _roleList: any[] = [];
            roleList.forEach(role => {
              let entry = {id: role[0], abbr: role[2], tip: role[3], allowed: {}};
              role[1].split('').forEach(value => entry.allowed[value] = false);
              _roleList.push(entry);
            });
            this.roleList = _roleList;
            resolve();
          },
          err => reject(err));
      }
    ).then(() => {
      return this.consoleUserApi.getUserRoles(user.id).map(roles => {
        this.isAdmin = AuthService.isAllowed(AuthService.ROLE.ADMIN, roles);
        this.roles = JSON.parse(JSON.stringify(this.roleList));
        this.roles.forEach(entry => {
          Object.keys(entry.allowed).forEach(key =>
            entry.allowed[key] = roles[`${entry.id}^${key}`] === true
          )
        });
      }).toPromise();
    }).then(() => {
      this.table.rows = this.roles;
      this.user = user;
    }).catch(err => {
      if (!!err) {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
      } else {
        // this.backPage();
        // let message = 'Role is not properly assigned.';
        // this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    });

  }
}
