import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog, MatSnackBar, ErrorStateMatcher, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {Store} from '@ngrx/store';

import {AuthService} from '../../core/auth.service';
import {ConsoleUserApi} from '../../core/sdk/index'
import {ROUTER_LOADED} from '../../core/actions';
import {AppObjectManagerService, AlertComponent, FormErrorStateMatcher, getErrorString} from '../../shared/index';
import {EMAIL_REGEX} from '../../shared/values/values';

@Component({
  selector: 'app-accounts-upsert',
  templateUrl: './accounts-upsert.component.html',
  styleUrls: ['./accounts-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class AccountsUpsertComponent implements OnInit {
  dialogTitle: string;
  table: any;
  role: string;
  account = {
    username: undefined,
    email: undefined,
    password: undefined,
    activated: false
  };
  orgAccount: any;
  accountId: number;
  roleList: any;
  orgRoleList: any;
  isAdmin = false;
  roles: any;

  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(private store: Store<any>,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              private authService: AuthService,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi) {
  }

 ngOnInit() {
    if (AuthService.isNotAllowed(AuthService.ROLE.ADMIN, this.consoleUserApi.getCachedCurrent().roles)) {
      this.authService.logout();
      return;
    }

    this.table = {
      header: [
        {attr: 'abbr', name: 'Name', width: 80},
        {attr: 'allowed.C', name: 'C', checkbox: true, width: 29},
        {attr: 'allowed.R', name: 'R', checkbox: true, width: 29},
        {attr: 'allowed.U', name: 'U', checkbox: true, width: 29},
        {attr: 'allowed.D', name: 'D', checkbox: true, width: 29},
        {attr: 'allowed.X', name: 'X', checkbox: true, width: 29},
      ],
      rows: [],
    };

    new Promise((resolve, reject) => {
      this.route.queryParams.subscribe(query => {
          this.role = query['role'];
          switch (this.role) {
            case 'add':
              this.dialogTitle = 'Add a Account';
              break;
            case 'edit':
              let account = this.objectManager.get('account');
              if (account === undefined || typeof account.id !== 'number' || account.id < 1) {
                reject(null);
                return;
              }
              delete account.checked;
              account.password = '';
              this.account = account;
              this.accountId = account.id;
              this.orgAccount = JSON.stringify(this.account);
              this.dialogTitle = 'Edit the Account: ' + this.account.username;
              break;
            default:
              reject(null);
              return;
          }
          resolve();
        },
        err => reject(err)
      );
    })
      .then(() =>
        this.consoleUserApi.getRoleList().map(roleList => {
          let _roleList: any[] = [];
          roleList.forEach(role => {
            let entry = {id: role[0], abbr: role[2], tip: role[3], allowed: {}};
            role[1].split('').forEach(value => entry.allowed[value] = false);
            _roleList.push(entry);
          });
          this.roleList = _roleList;
        }).toPromise())
      .then(() => {
        if (this.role !== 'edit') {
          return new Promise<void>(resolve => resolve());
        }
        return this.consoleUserApi.getUserRoles(this.accountId).map(roles => {
          this.isAdmin = AuthService.isAllowed(AuthService.ROLE.ADMIN, roles);
          this.roleList.forEach(entry => {
            Object.keys(entry.allowed).forEach(key =>
              entry.allowed[key] = roles[`${entry.id}^${key}`] === true
            )
          });
          this.orgRoleList = JSON.stringify(this.roleList);
        }).toPromise<void>();
      })
      .then(() => {
        this.table.rows = this.roleList;
        this.store.dispatch({type: ROUTER_LOADED});
      })
      .catch(err => {
        if (!!err) {
          this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
          this.store.dispatch({type: ROUTER_LOADED});
        } else {
          this.backPage();
          // let message = 'Role is not properly assigned.';
          // this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      });
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    console.log(this.roleList);

    if (this.role === 'edit') {
      if (this.orgAccount === JSON.stringify(this.account)
        && this.orgRoleList === JSON.stringify(this.roleList)) {
        this.snackBar.open('Values are not changed.\nPlease make some modification(s) first.', 'Confirm', {duration: 2000});
      } else if (this.isFormError(this.nameFormControl)) {
        this.snackBar.open('Please check the name.', null, {duration: 2000});
      } else if (this.isFormError(this.emailFormControl)) {
        this.snackBar.open('Please check the email.', null, {duration: 2000});
        // } else if (this.selectedPermissions.length === 0) {
        //   this.snackBar.open('Please add at least a permission.', null, {duration: 2000});
      } else {
        const ref = this.dialog.open(AlertComponent);
        ref.componentInstance.message = `Save '${this.account.username}'?`;

        ref.afterClosed().subscribe(result => {
          if (result) {
            this.onEdit();
          }
        });
      }
    } else {
      if (this.isFormError(this.nameFormControl)) {
        this.snackBar.open('Please check the name.', null, {duration: 2000});
      } else if (this.isFormError(this.emailFormControl)) {
        this.snackBar.open('Please check the email.', null, {duration: 2000});
      } else if (this.isFormError(this.passwordFormControl)) {
        this.snackBar.open('Please check the password.', null, {duration: 2000});
        // } else if (this.selectedPermissions.length === 0) {
        //   this.snackBar.open('Please add at least a permission.', null, {duration: 2000});
      } else {
        if (this.account.username != null && this.account.email !== '') {
          const ref = this.dialog.open(AlertComponent);
          ref.componentInstance.message = `Add '${this.account.username}?'`;
          ref.afterClosed().subscribe(result => {
            if (result) {
              this.onAdd();
            }
          });
        } else {
          if (this.account.username === undefined) {
            this.snackBar.open('Please check the name.', null, {duration: 2000});
          } else if (this.account.email === '') {
            this.snackBar.open('Please check the email.', null, {duration: 2000});
          }
        }
      }
    }
  }

  aggregateRoles() {
    let roles = {};
    if (this.isAdmin) {
      roles[AuthService.ROLE.ADMIN] = true;
      if (this.role === 'edit') {
        let orgRoles = JSON.parse(this.orgRoleList);
        orgRoles.forEach(entry => Object.keys(entry.allowed).forEach(key => {
            if (entry.allowed[key]) {
              roles[`${entry.id}^${key}`] = false;
            }
          }
        ))
      }
    } else {
      if (this.role === 'edit') {
        let orgRoles = JSON.parse(this.orgRoleList);
        this.roleList.forEach(entry =>
          Object.keys(entry.allowed).forEach(key => {
              if (entry.allowed[key] !== orgRoles.find(role => role.id === entry.id).allowed[key]) {
                roles[`${entry.id}^${key}`] = entry.allowed[key];
              }
            }
          ))
      } else {
        this.roleList.forEach(entry =>
          Object.keys(entry.allowed).forEach(key => {
              if (entry.allowed[key] === true) {
                roles[`${entry.id}^${key}`] = true;
              }
            }
          ))
      }
    }
    this.roles = roles;
  }

  onAdd(): void {
    this.aggregateRoles();
    this.consoleUserApi.createWithRoles(this.account, this.roles).subscribe(
      res => {
        this.snackBar.open(`Account '${this.account.username }' created.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Add Failed.'), 'Confirm', {duration: 10000});
      });
  }

  onEdit(): void {
    let account = JSON.parse(JSON.stringify(this.account));
    if (account.password === '') {
      delete account.password;
    }
    this.aggregateRoles()
    this.consoleUserApi.patchAttributesWithRoles(account, this.roles).subscribe(
      res => {
        this.snackBar.open(`Account '${this.account.username}' updated.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        this.snackBar.open(getErrorString(err, `Failed to update Account '${this.account.username}'.`),
          'Confirm', {duration: 10000});
      });
  }

  getButtonName() {
    if (this.role === 'edit') {
      return 'Save';
    } else {
      return 'Add';
    }
  }

  backPage: any = () => {
    this.objectManager.clean('account');
    this.router.navigate(['../accounts'], {relativeTo: this.route});
  }
}
