import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-engine-configuration-panel',
  templateUrl: './engine-configuration-panel.component.html',
  styleUrls: ['./engine-configuration-panel.component.scss']
})
export class EngineConfigurationPanelComponent implements OnInit {

  engineInfo: any;

  constructor() { }

  ngOnInit() {
  }

}
