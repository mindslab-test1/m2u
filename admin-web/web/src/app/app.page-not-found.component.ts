import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ROUTER_LOADED } from './core/actions';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './app.page-not-found.component.html',
  styleUrls: ['./app.page-not-found.component.scss']
})
export class AppPageNotFoundComponent implements OnInit {

  constructor(
    private store: Store<any>,
  ) { }

  ngOnInit() {
    this.store.dispatch({type: ROUTER_LOADED});
  }

}
