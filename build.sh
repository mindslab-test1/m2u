#!/usr/bin/env bash

function usage() {
  echo "build.sh help : this message"
  echo "buiid.sh MAUM_ROOT: build all targets"
  echo "buiid.sh MAUM_ROOT: [all | (libmaum | libdb | stt | ta | xdc | sds | m2u | hzc | pool | map | mapauth | rest | da | svcadm | admweb | logger | itfsvc | daisvc)] : build specific target"
  echo "  libmaum: build and install libmaum"
  echo "  libdb: build and install hzc."
  echo "  stt: build and install stt"
  echo "  ta: build and install ta"
  echo "  xdc: build and install xdc"
  echo "  sds: build and install sds"
  echo "  m2u: build and install m2u platform"
  echo "  hzc: build and install hazelcast re-packaging"
  echo "  pool: build and install m2u pool service"
  echo "  map: build and install map"
  echo "  mapauth: build and install map-auth"
  echo "  rest: build and install rest"
  echo "  da: build and install dialog-agent"
  echo "  svcadm: build and install service-admin"
  echo "  admweb: build and install admin-web & rest"
  echo "  logger: build and install dialog-logger"
  echo "  itfsvc: build and install intent-finder-supervisor-control"
  echo "  daisvc: build and install dialog-agent-instance-supervisor-control"
  echo "  ex) build.sh /srv/maum stt ta m2u"
  echo "  ex) build.sh /srv/maum libmaum ta m2u"
  echo "build.sh tar: archive MAUM_ROOT folder as tar.gz"
  echo "build.sh tar stt-only: libmaum stt proc rest web config"
  echo "build.sh tar ta-only: libmaum ta proc rest web config"
  echo "  ex ) build.sh tar stt ta m2u"
  echo "build.sh clean-deploy: clean tar temp directories."
  echo "build.sh clean-cache: clean build cache directories."
  echo "build.sh clean-cmake: clean build cmake directories."
}

if [ "$#" -lt 1 ]; then
  echo "Illegal number of parameters"
  echo
  usage
  exit 1
fi

if [ "$1" = "help" ]; then
  usage
  exit 0
fi

if [ "$1" = "clean-deploy" ]; then
  echo "Remove all deploy directories."
  rm -rf ${PWD}/deploy-*
  exit 0
fi

if [ "$1" = "clean-cache" ]; then
  echo "Remove all cache build outputs."
  echo rm -rf ${HOME}/.maum-build
  rm -rf ${HOME}/.maum-build/*

  test -e ${MAUM_ROOT}/.brain-han-train-installed \
  && echo rm -rf ${MAUM_ROOT}/.brain-han-train-installed \
  && rm -rf ${MAUM_ROOT}/.brain-han-train-installed/

  test -d ${HOME}/.virtualenvs/venv_han \
  && echo rm -rf ${HOME}/.virtualenv/venv_han \
  && rm -rf ${HOME}/.virtualenvs/venv_han/*
  exit 0
fi

function clean_build_dir() {
  echo "Remove all build proto files in directory named as $1"
  find ./ -type d -a -name "$1*"
  for DD in $(find ./ -type d -a -name "$1*"); do
    echo "cleaning proto build output for" $DD
    (cd $DD; if [ -d proto ]; then (cd proto; make clean); fi;)
    (cd $DD; if [ -d pysrc/proto ]; then (cd pysrc/proto; make clean); fi;)
  done

  echo "Remove all build directories named as $1"
  find ./ -type d -a -name "$1*" | xargs rm -rf
}

if [ "$1" = "clean-cmake" ]; then
  clean_build_dir build-debug
  clean_build_dir build-deploy-debug
  exit 0
fi


GLOB_BUILD_DIR=${HOME}/.maum-build

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS detected. Set centos as default."
  OS=centos
fi

if [ "${OS}" = "centos" ]; then
  __CC=/usr/bin/gcc
  __CXX=/usr/bin/g++
  CMAKE=/usr/bin/cmake3
else
  __CC=/usr/bin/gcc-4.8
  __CXX=/usr/bin/g++-4.8
  CMAKE=/usr/bin/cmake
fi


IS_TAR=0
if [ "$1" = "tar" ]; then
   temp_dir=$(mktemp -d ${PWD}/deploy-XXXXXX)
   export MAUM_ROOT=${temp_dir}/maum
   echo "temp dir ${MAUM_ROOT} created"
   IS_TAR=1
   export MAUM_BUILD_DEPLOY=true
else
  export MAUM_ROOT=$1
fi

test -d ${MAUM_ROOT} || mkdir -p ${MAUM_ROOT}


if [ ! -d ${MAUM_ROOT} ]; then
  echo ${MAUM_ROOT} is not directory.
  echo Use valid directory.
  exit 1
fi

## argument parameters
shift

TARGETS=()
if [ "$#" = 0 ]; then
  TARGETS+=('libmaum')
  TARGETS+=('libdb')
  TARGETS+=('stt')
  TARGETS+=('ta')
  TARGETS+=('xdc')
  TARGETS+=('sds')
  TARGETS+=('m2u')
  TARGETS+=('hzc')
  TARGETS+=('pool')
  TARGETS+=('map')
  TARGETS+=('mapauth')
  TARGETS+=('rest')
  TARGETS+=('da')
  TARGETS+=('svcadm')
  TARGETS+=('logger')
  TARGETS+=('itfsvc')
  TARGETS+=('admweb')
  TARGETS+=('daisvc')
elif [ "$1" = "all" ]; then
  TARGETS+=('libmaum')
  TARGETS+=('libdb')
  TARGETS+=('stt')
  TARGETS+=('ta')
  TARGETS+=('xdc')
  TARGETS+=('sds')
  TARGETS+=('m2u')
  TARGETS+=('hzc')
  TARGETS+=('pool')
  TARGETS+=('map')
  TARGETS+=('mapauth')
  TARGETS+=('rest')
  TARGETS+=('da')
  TARGETS+=('svcadm')
  TARGETS+=('logger')
  TARGETS+=('itfsvc')
  TARGETS+=('admweb')
  TARGETS+=('daisvc')
elif [ "$1" = "stt-only" ]; then
  TARGETS+=('libmaum')
  TARGETS+=('libdb')
  TARGETS+=('stt')
  TARGETS+=('sds')
  TARGETS+=('m2u')
  TARGETS+=('map')
  TARGETS+=('pool')
  TARGETS+=('mapauth')
  TARGETS+=('rest')
  TARGETS+=('da')
  TARGETS+=('svcadm')
  TARGETS+=('logger')
  TARGETS+=('itfsvc')
  TARGETS+=('admweb')
  TARGETS+=('daisvc')
elif [ "$1" = "ta-only" ]; then
  TARGETS+=('libmaum')
  TARGETS+=('libdb')
  TARGETS+=('ta')
  TARGETS+=('sds')
  TARGETS+=('m2u')
  TARGETS+=('hzc')
  TARGETS+=('map')
  TARGETS+=('pool')
  TARGETS+=('mapauth')
  TARGETS+=('rest')
  TARGETS+=('da')
  TARGETS+=('svcadm')
  TARGETS+=('logger')
  TARGETS+=('itfsvc')
  TARGETS+=('admweb')
  TARGETS+=('daisvc')
elif [ "$1" = "hzc" ]; then
  TARGETS+=('hzc')
else
  TARGETS=("${@:1}")
fi

HAS_STT=0
for e in "${TARGETS[@]}"
do
  if [ "$e" = "stt" ] ; then
    HAS_STT=1
    break
  fi
done

HAS_TA=0
for e in "${TARGETS[@]}"
do
  if [ "$e" = "ta" ] ; then
    HAS_TA=1
    break
  fi
done

if [ ${IS_TAR} = 1 ]; then
  TARGETS+=('tar')
fi

echo "Build targets are ${TARGETS[*]}"

IMAGE_NAME=all
if [ ${HAS_STT} = 1 -a ${HAS_TA} = 0 ]; then
  IMAGE_NAME=stt
fi

if [ ${HAS_STT} = 0 -a ${HAS_TA} = 1 ]; then
  IMAGE_NAME=ta
fi

maum_root=${MAUM_ROOT}

REPO_ROOT=$(pwd)
# repo root is used in submodule build scirpt
export REPO_ROOT

APPS_ROOT=${maum_root}/apps

function make_folder() {
  test -d ${APPS_ROOT}/$1 || mkdir -p ${APPS_ROOT}/$1
}

LAST_OUT=.last_build_outdir && [[ "${IS_TAR}" == "1" ]] && LAST_OUT=.last_deploy_outdir

if [ -f ${LAST_OUT} ]; then
  last_root=$(cat ${LAST_OUT})

  if [ "${last_root}" != "${maum_root}" ]; then
    build_base="build-debug" && [[ "${IS_TAR}" = "1" ]] && build_base="build-deploy-debug"

    echo "clean last ${build_base} directories"
    clean_build_dir ${build_base}
  fi
fi
echo ${maum_root} > ${LAST_OUT}

# hazelcast cpp client
function build_haz_client() {
  local src_dir=${REPO_ROOT}/submodule/hazelcast-cpp-client
  local hzcl_sha1=$(cd ${src_dir} > /dev/null && git log -n1 --pretty=%H -- .)
  local GCC_VER=$(${__CC} -dumpversion)
  local CACHE_PREFIX=${GLOB_BUILD_DIR}/cache/hzc-${hzcl_sha1}-${GCC_VER}

  local LIB_HZC_SO_VER=libHazelcastClient3.12_64.so
  local LIB_HZC_SO=libHazelcastClient.so

  if [ ! -f ${CACHE_PREFIX}/lib/libHazelcastClient.so ]; then
    local build_dir=${REPO_ROOT}/submodule/hazelcast-cpp-client/ReleaseShared64-
    cd ${REPO_ROOT}/submodule/hazelcast-cpp-client
    git clean -fdx
    test -f ${build_dir} || mkdir -p ${build_dir}
    cd ${build_dir}
    ${CMAKE} \
      -DHZ_LIB_TYPE=SHARED \
      -DHZ_BIT=64 \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_CXX_COMPILER:FILEPATH=${__CXX} \
      -DCMAKE_C_COMPILER:FILEPATH=${__CC} ..

    make -j ${NPROC}
    cd ${src_dir}
    test -d ${CACHE_PREFIX} || mkdir -p ${CACHE_PREFIX}
    test -d ${CACHE_PREFIX}/include || mkdir -p ${CACHE_PREFIX}/include
    cp -rf ${src_dir}/hazelcast/include/* ${CACHE_PREFIX}/include
    cp -R ${src_dir}/hazelcast/generated-sources/include/hazelcast/* ${CACHE_PREFIX}/include/hazelcast/
    cp -rf ${src_dir}/external/release_include/easylogging++ ${CACHE_PREFIX}/include/
    test -d ${CACHE_PREFIX}/lib || mkdir -p ${CACHE_PREFIX}/lib
    cp ${build_dir}/${LIB_HZC_SO_VER} ${CACHE_PREFIX}/lib
    (cd ${CACHE_PREFIX}/lib; ln -sf ${LIB_HZC_SO_VER} ${LIB_HZC_SO})
  fi

  local cache_ver=($(sha1sum -b $(readlink -f ${CACHE_PREFIX}/lib/${LIB_HZC_SO}) 2> /dev/null))
  local prev_ver=""
  if [ -f ${maum_root}/lib/${LIB_HZC_SO} ] ; then
    prev_ver=($(sha1sum -b $(readlink -f ${maum_root}/lib/${LIB_HZC_SO}) 2> /dev/null))
  else
    prev_ver="not_exist"
  fi

  if [ "${cache_ver}" != "${prev_ver}" ] ; then
    (cd ${CACHE_PREFIX}; cp -dpr include lib ${maum_root})
    sudo ldconfig ${maum_root}
  fi
}

function install_hazman() {
# install hzc management center
  HZCMAN_DIR=${HOME}/.maum-build/cache
  HZCMAN_NM=hazelcast-management-center-3.12.7
  HZCMAN_WAR=hazelcast-mancenter-3.12.7.war
  if [ ! -f ${maum_root}/lib/${HZCMAN_WAR} ]; then
    if [ ! -f ${HZCMAN_DIR}/${HZCMAN_NM}/${HZCMAN_WAR} ]; then
      cd ${HZCMAN_DIR}
      echo "## install hzc management center."
      curl -0 https://download.hazelcast.com/management-center/${HZCMAN_NM}.tar.gz -o ${HZCMAN_NM}.tar.gz
      tar -zxvf ${HZCMAN_NM}.tar.gz
    fi
    cp ${HZCMAN_DIR}/${HZCMAN_NM}/${HZCMAN_WAR} ${maum_root}/lib
    (cd ${maum_root}/lib; ln -sf ${HZCMAN_WAR} hazelcast-mancenter.war)
    echo "## copy "${HZCMAN_DIR}"/"${HZCMAN_NM}"/"${HZCMAN_WAR}" to " ${maum_root}/lib "complete!"
  fi
}

# hazelcast server distributions jar
function build_hazelcast() {
  local src_dir=${REPO_ROOT}/javasrc/hazelcast-repack
  local build_dir=${src_dir}/build/install/m2u-hzc-repack-shadow/lib
  local deploy_dir=${maum_root}/lib
  (cd ${src_dir};
   ./gradlew clean;
   ./gradlew installShadowDist;
   cp ${build_dir}/m2u-hzc-repack-2.1.0-all.jar ${deploy_dir}/;)

  install_hazman
}

function build_pool() {
  local src_dir=${REPO_ROOT}/javasrc/m2u-pool
  local build_dir=${src_dir}/build/install/m2u-pool-shadow/lib
  local deploy_dir=${maum_root}/lib
  (cd ${src_dir}; \
   ./gradlew clean; \
   ./gradlew installShadowDist; \
   cp ${build_dir}/m2u-pool-2.1.0-all.jar ${deploy_dir}/;)
}


function build_libdb() {
  build_haz_client
}

function build_libmaum() {
  # submodule build
  (cd ${REPO_ROOT}/submodule/libmaum && CC=${__CC} CXX=${__CXX} ./build.sh)

  if [ "${HAS_STT}" = "0" ]; then
    echo "build stt proto only"
    (cd ${REPO_ROOT}/submodule/brain-stt && CC=${__CC} CXX=${__CXX} ./build.sh proto)
  fi
  if [ "${HAS_TA}" = "0" ]; then
    echo "build ta proto only"
    (cd ${REPO_ROOT}/submodule/brain-ta && CC=${__CC} CXX=${__CXX} ./build.sh proto)
  fi
}


function build_stt() {
  (cd ${REPO_ROOT}/submodule/brain-stt && CC=${__CC} CXX=${__CXX} ./build.sh)
}

function build_ta() {
  (cd ${REPO_ROOT}/submodule/brain-ta && CC=${__CC} CXX=${__CXX} ./build.sh)
}

function build_xdc() {
  (cd ${REPO_ROOT}/submodule/brain-xdc && CC=${__CC} CXX=${__CXX} ./build.sh)
}

function build_sds() {
  (cd ${REPO_ROOT}/submodule/brain-sds && CC=${__CC} CXX=${__CXX} ./build.sh)
}

function build_m2u() {
  GCC_VER=$(${__CC} -dumpversion)

  build_base="build-debug" && [[ "${IS_TAR}" = "1" ]] && build_base="build-deploy-debug"
  build_dir=${REPO_ROOT}/${build_base}-${GCC_VER}

  test -d ${build_dir} || mkdir -p ${build_dir}
  cd ${build_dir}

  ${CMAKE} \
   -DCMAKE_PREFIX_PATH=${maum_root} \
   -DCMAKE_INSTALL_PREFIX=${maum_root} \
   -DCMAKE_BUILD_TYPE=Debug \
   -DCMAKE_CXX_COMPILER:FILEPATH=${__CXX} \
   -DCMAKE_C_COMPILER:FILEPATH=${__CC} ..
  make install -j 2
  # make install -j ${NPROC}
}

function build_map() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}/javasrc/m2u-async-proxy
  shadow_dist_base=${REPO_ROOT}/javasrc/m2u-async-proxy/build/install/m2u-map-shadow
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  ./gradlew clean;
  ./gradlew installShadowDist -Pconfig=prod
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  cp ${shadow_dist_base}/lib/* ${out_dir}/lib
  cd ${base_dir}
}

# map-auth server distributions jar
function build_map_auth() {
  local src_dir=${REPO_ROOT}/javasrc/map-auth-dummy
  local build_dir=${src_dir}/build/install/m2u-map-auth-shadow/lib
  local deploy_dir=${maum_root}/lib
    (cd ${src_dir};
   ./gradlew clean;
   ./gradlew installShadowDist;
   cp ${build_dir}/m2u-map-auth-2.1.0-all.jar ${deploy_dir}/;)
}

# rest server distributions jar
function build_rest() {
  local src_dir=${REPO_ROOT}/javasrc/rest
  local build_dir=${src_dir}/build/libs
  local deploy_dir=${maum_root}/lib
    (cd ${src_dir};
   ./gradlew clean;
   ./gradlew installShadowDist;
   cp ${build_dir}/m2u-rest-2.1.0-all.jar ${deploy_dir}/;)
}

# dialog-agent server distributions jar
function build_dialog_agent() {
  local src_dir=${REPO_ROOT}/javasrc/dialog-agent
  local build_dir=${src_dir}/build/install/m2u-dialog-agent-shadow/lib
  local deploy_dir=${maum_root}/da
  (cd ${src_dir};
   ./gradlew clean;
   ./gradlew installShadowDist;
   cp ${build_dir}/m2u-dialog-agent-2.1.0-all.jar ${deploy_dir}/;)
}

function build_svcadm() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}/javasrc/service-admin
  shadow_dist_base=${REPO_ROOT}/javasrc/service-admin/build/install/m2u-service-admin-shadow
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  ./gradlew clean;
  ./gradlew installShadowDist -Pconfig=prod
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  cp ${shadow_dist_base}/lib/* ${out_dir}/lib
  # 추후 외부에서 관리가 필요한지 생각해 봐야함
  #test -d ${out_dir}/conf || mkdir -p ${out_dir}/conf
  #test -d ${out_dir}/bin || mkdir -p ${out_dir}/bin
  #cp ${shadow_dist_base}/conf/* ${out_dir}/conf
  #cp ${shadow_dist_base}/bin/* ${out_dir}/bin
  cd ${base_dir}
}

function build_admweb() {
  #--rest--
  base=${REPO_ROOT}/admin-web/rest
  target=admin-rest
  local target_dir=${MAUM_ROOT}/apps/${target}
  test -d ${target_dir} || mkdir -p ${target_dir}
  (cd ${base} && \
  rm -rf ./node_modules package-lock.json && \
  npm remove @angular/cli && \
  npm install && \
  npm run prod-sdk && \
  npm prune --production)
  (test -d ${target_dir} && sudo rm -rf ${target_dir})
  (cd ${base} && rsync -ar package.json server node_modules ${target_dir})
  if [ "${OS}" = "centos" ] && [ -z ${DOCKER_MAUM_BUILD} ]; then
    sudo chcon -Rv unconfined_u:object_r:var_t ${MAUM_ROOT}/apps
  fi

  #--web--
  base=${REPO_ROOT}/admin-web/web
  target=admin-web
  target_dir=${MAUM_ROOT}/apps/${target}
  sudo rm -rf ${target_dir}
  cd ${REPO_ROOT}
  test -d ${target_dir} || mkdir -p ${target_dir}
  (cd ${base} && \
    rm -rf ./node_modules package-lock.json && \
    npm install && \
    node --max_old_space_size=12288 ./node_modules/.bin/ng build --prod --environment=prod -op=${target_dir} --output-hashing=all --sourcemap=false --build-optimizer=false)
  if [ "${OS}" = "centos" ]; then
    if [ -z ${DOCKER_MAUM_BUILD} ]; then
      sudo chcon -Rv unconfined_u:object_r:httpd_sys_content_t ${MAUM_ROOT}/apps/${target}
    fi
  else
    sudo chown -R www-data.www-data ${MAUM_ROOT}/apps/${target}
  fi
}

function build_logger() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}/javasrc/logger
  shadow_dist_base=${REPO_ROOT}/javasrc/logger/build/install/m2u-logger-shadow
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  ./gradlew clean
  ./gradlew installShadowDist -Pconfig=prod
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  cp ${shadow_dist_base}/lib/* ${out_dir}/lib
  cd ${base_dir}
}

function build_appender() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}/javasrc/samples/appender
  jar_dist_base=${build_base}/call-log-appender/build
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  ./gradlew call-log-appender:clean
  ./gradlew call-log-appender:build
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  cp ${jar_dist_base}/libs/*.jar ${out_dir}/lib
  cd ${base_dir}
}

function build_itfsvc() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}/javasrc/intent-finder-svc
  shadow_dist_base=${REPO_ROOT}/javasrc/intent-finder-svc/build/install/m2u-itfsvc-shadow
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  ./gradlew clean;
  ./gradlew installShadowDist -Pconfig=prod
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  cp ${shadow_dist_base}/lib/* ${out_dir}/lib
  cd ${base_dir}
}

function build_daisvc() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}/javasrc/dialog-agent-instance-svc
  shadow_dist_base=${REPO_ROOT}/javasrc/dialog-agent-instance-svc/build/install/m2u-daisvc-shadow
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  ./gradlew clean;
  ./gradlew installShadowDist -Pconfig=prod
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  cp ${shadow_dist_base}/lib/* ${out_dir}/lib
  cd ${base_dir}
}

function make_tar() {
  upperdir=$(dirname ${maum_root})
  tardir=$(basename ${maum_root})

  tag=$(git describe --abbrev=7 --tags)
  tarfile=m2u-${tag}-${IMAGE_NAME}-${OS}.tar.gz

  echo "------------------------------------"
  echo "Generating TAR ${tarfile} ..."
  echo "------------------------------------"

  GZIP=-1 tar -zcf ${upperdir}/${tarfile} -C ${upperdir} ${tardir} \
    --exclude "${tardir}/workspace/stt-training/*" \
    --exclude "${tardir}/workspace/classifier/*" \
    --exclude "${tardir}/resources/*" \
    --exclude "${tardir}/lib/*.a" \
    --exclude "${tardir}/logs/*" \
    --exclude "${tardir}/trained/stt/*" \
    --exclude "${tardir}/trained/hmd/*" \
    --exclude "${tardir}/trained/classifier/*" \
    --exclude "${tardir}/share/cmake/*" \
    --exclude "${tardir}/apps/*/node_modules/*.o" \
    --exclude "${tardir}/apps/*/node_modules/*.c" \
    --exclude "${tardir}/apps/*/node_modules/*.cpp" \
    --exclude "${tardir}/apps/*/node_modules/*.cc" \
    --exclude "${tardir}/apps/*/node_modules/*.h" \
    --exclude "${tardir}/apps/*/node_modules/*.hh" \
    --exclude "${tardir}/apps/*/node_modules/*.hpp" \
    --exclude "${tardir}/apps/*/node_modules/*.in" \
    --exclude "${tardir}/apps/*/node_modules/*.am" \
    --exclude "${tardir}/apps/*/node_modules/*.ac" \
    --exclude "${tardir}/apps/*/node_modules/*.go" \
    --exclude "${tardir}/apps/*/node_modules/*.a" \
    --exclude "${tardir}/apps/*/node_modules/*.S" \
    --exclude "${tardir}/apps/*/node_modules/*.pas" \
    --exclude "${tardir}/apps/*/node_modules/*.sln" \
    --exclude "${tardir}/apps/*/node_modules/*.vcxproj" \
    --exclude "${tardir}/apps/*/node_modules/*.vcproj" \
    --exclude "${tardir}/apps/*/node_modules/*SConscript" \
    --exclude "${tardir}/apps/*/node_modules/*configure" \
    --exclude "${tardir}/apps/*/node_modules/grpc/third_party/*" \
    --exclude "${tardir}/bin/protoc" \
    --exclude "${tardir}/bin/grpc_*_plugin" \
    --exclude "${tardir}/*built" \
    --exclude "${tardir}/*extracted" \
    --exclude "${tardir}/*installed"

  echo m2u-${tag} > ${maum_root}/etc/revision
  test -d ${REPO_ROOT}/out || mkdir -p ${REPO_ROOT}/out
  mv ${upperdir}/${tarfile} ${REPO_ROOT}/out
  echo "------------------------------------"
  echo "TAR file out/${tarfile} created."
  echo "------------------------------------"

  # make_all_res_tar
  echo "please clean TEMP DIR : ${upperdir}"
}

### BUILD MAIN
./prerequisite.sh

for t in "${TARGETS[@]}"
do
  case "$t" in
    libmaum) build_libmaum;;
    libdb) build_libdb;;
    stt) build_stt;;
    ta) build_ta;;
    xdc) build_xdc;;
    sds) build_sds;;
    m2u) build_m2u;;
    hzc) build_hazelcast;;
    pool) build_pool;;
    map) build_map;;
    mapauth) build_map_auth;;
    rest) build_rest;;
    da) build_dialog_agent;;
    svcadm) build_svcadm;;
    admweb) build_admweb;;
    logger) build_logger;;
    appender) build_appender;;
    tar) make_tar;;
    itfsvc) build_itfsvc;;
    daisvc) build_daisvc;;
    *) echo "INVALID TARGET $t";;
  esac
done
