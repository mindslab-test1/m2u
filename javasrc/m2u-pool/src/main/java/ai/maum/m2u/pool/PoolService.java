package ai.maum.m2u.pool;

import ai.maum.rpc.ResultStatus;
import ai.maum.utils.HazelcastConnector;
import ai.maum.utils.PropertyManager;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import maum.rpc.Status;
import maum.rpc.Status.ProcessOfM2u;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Server that manages startup/shutdown of a {@code } server.
 *
 * @author Dong Hoon Song
 * @date 2018.3.28
 */

public class PoolService {

  private static final Logger logger = LoggerFactory.getLogger(PoolService.class);

  static {
    ResultStatus.setModuleAndProcess(Status.Module.M2U, ProcessOfM2u.M2U_POOL.getNumber());
  }

  private Server server;

  /**
   * logVersions
   */
  private static void logVersion(boolean console) {
    try {
      String version = "{Version information is not found.}";
      Enumeration<URL> resources = PoolService.class.getClassLoader()
          .getResources("META-INF/MANIFEST.MF");
      while (resources.hasMoreElements()) {
        Manifest manifest = new Manifest(resources.nextElement().openStream());
        Attributes attrs = manifest.getMainAttributes();
        if (attrs.getValue("BuildTime") != null) {
          version = attrs.getValue("Title") + " Version " + attrs.getValue("Version") + " / "
              + attrs.getValue("BuildTime");
          break;
        }
      }
      if (console) {
        logger.info(version);
      } else {
        logger.info(version);
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }

  private void start() throws IOException {
    if (!HazelcastConnector.getInstance().getReady()) {
      logger.info("Hazelcast Server is not ready.");
      logger.info("Stop starting Server.");
      stop();
      return;
    }
    PoolServiceImpl poolService = new PoolServiceImpl();
    logger.info("Clean-Up Dangling skills before starting.");
    poolService.cleanUpDanglingSkills();

    /* The port on which the server should run */
    int port = PropertyManager.getInt("pool.listen.port");
    int grpcTimeout = PropertyManager.getInt("pool.grpc.timeout");

    ServerBuilder serverBuilder = NettyServerBuilder
        .forPort(port)
        .maxConnectionIdle(grpcTimeout, TimeUnit.MILLISECONDS)
        .maxConnectionAge(grpcTimeout, TimeUnit.MILLISECONDS);
    server = serverBuilder
        .addService(poolService)
        .addService(ProtoReflectionService.newInstance())
        .build()
        .start();
    logger.info("Server started, listening on {}", port);
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      // Use stderr here since the logger may have been reset by its JVM shutdown hook.
      logger.info("*** shutting down gRPC server since JVM is shutting down");
      PoolService.this.stop();
      logger.info("*** server shut down");
    }));
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
    if (HazelcastConnector.getInstance() != null) {
      HazelcastConnector.getInstance().release();
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }
  }

  /**
   * Main launches the server from the command line.
   */
  public static void main(String[] args) throws IOException, InterruptedException {
    logVersion(false);

    final PoolService server = new PoolService();
    final PingCheckService pingMonitor = new PingCheckService();
    server.start();
    pingMonitor.start(server.server.getPort());
    server.blockUntilShutdown();
  }
}
