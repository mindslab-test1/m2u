package ai.maum.m2u.pool;

import ai.maum.m2u.common.portable.DialogAgentInstanceResourcePortable;
import ai.maum.utils.HazelcastConnector;
import ai.maum.utils.PropertyManager;
import ai.maum.utils.Utils;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import maum.m2u.server.DialogAgentInstancePoolGrpc;
import maum.m2u.server.DialogAgentInstancePoolGrpc.DialogAgentInstancePoolBlockingStub;
import maum.m2u.server.Pool.DialogAgentInstanceKey;
import maum.m2u.server.Pool.DialogAgentInstanceResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 주기적으로 DA서버들에 접속 테스트를 하고 서버 자체가 Shutdown 되었으면
 * 해당 서버의 모든 DAIR을 Pool에서 unregister하여
 * Router에서 DA접속시도 시간(20초)를 줄인다.
 */
public class PingCheckService {

  private static final Logger logger = LoggerFactory.getLogger(PingCheckService.class);
  private static final String DAIR = "dialog-agent-instance-resource";
  private static final String PingPeriodProperty = "pool.ping.period.ms";
  private static final int ConnectionTimeout = 5000;

  private long period = -1;
  private int poolPort;
  private Thread pingThread;

  /**
   * DA서버 접속 테스트 Task
   */
  class PingTask implements Runnable {

    /**
     * DAIR의 Target Server IP에 접속 테스트를 한 후
     * Timeout이 발생할 경우 해당 서버의 모든 DAIR을 Pool에서 unregister한다.
     */
    private void doCheck() {
      DialogAgentInstancePoolBlockingStub poolStub = null;
      ManagedChannel channel = null;

      // DAIR의 Target Server IP 목록을 구한다.
      HazelcastInstance hzc = HazelcastConnector.getInstance().client;
      IMap<String, DialogAgentInstanceResourcePortable> dairMap = hzc.getMap(DAIR);
      Collection<DialogAgentInstanceResourcePortable> targets = dairMap.values();

      Map<String, Boolean> reachables = new HashMap<>();
      // Target Server가 접속 가능한 지 테스트한다.
      for (DialogAgentInstanceResourcePortable target : targets) {
        DialogAgentInstanceResource dair = target.getProtobufObj();
        String hostname = dair.getServerIp();
        if (!reachables.containsKey(dair.getServerIp())) {
          boolean reachable = true;
          Socket socket = new Socket();

          try {
            InetSocketAddress addr = new InetSocketAddress(hostname, dair.getServerPort());
            socket.connect(addr, ConnectionTimeout);
          } catch (IOException e) {
            logger.error(Utils.getStackTrace(e));

            // Timeout이 발생할 경우 해당 서버가 shutdown되어 접속 불가한 것으로 판단한다.
            if (e instanceof SocketTimeoutException) {
              reachable = false;
              logger.debug("{} is unreachable.", hostname);
            }
          } finally {
            try {
              socket.close();
            } catch (IOException e) {
              logger.error(Utils.getStackTrace(e));
            }
          }

          reachables.put(hostname, reachable);
        }

        // Shutdown 된 서버의 DAIR를 unregister한다.
        if (!reachables.get(hostname)) {
          logger.debug("Unregister DAI [{}] on unreachable {}.", dair.getName(), hostname);
          if (channel == null) {
            channel = ManagedChannelBuilder
                .forAddress("localhost", poolPort)
                .usePlaintext()
                .build();
            poolStub = DialogAgentInstancePoolGrpc.newBlockingStub(channel);
          }
          poolStub.unregister(DialogAgentInstanceKey.newBuilder().setKey(dair.getKey()).build());
        }
      }

      if (channel != null) {
        try {
          channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
          logger.error(Utils.getStackTrace(e));
        }
      }
    }

    /**
     * 주어진 주기 마다 DA서버가 접속 가능한지 테스트한다.
     */
    @Override
    public void run() {
      while (true) {
        try {
          Thread.sleep(period);
          doCheck();
        } catch (InterruptedException e) {
          logger.error(Utils.getStackTrace(e));
        }
      }
    }
  }

  /**
   * 접속 테스트 서비스를 시작한다.
   * 주기가 0보다 크게 설정됐을 때만 동작한다.
   *
   * @param port DAIR unregister하기 위한 Pool의 포트 번호.
   */
  void start(int port) {
    // 주기를 읽어 온다.
    try {
      period = PropertyManager.getInt(PingPeriodProperty);
    } catch (NoSuchElementException e) {
      logger.debug(Utils.getStackTrace(e));
    }

    // 주기가 0보다 클 때만 동작한다.
    if (period > 0) {
      poolPort = port;
      pingThread = new Thread(new PingTask());
      pingThread.setDaemon(true);
      pingThread.start();
      logger.info("Ping-check service started. period: {}ms", period);
    } else {
      logger.info("------------------------------------------------");
      logger.info("  '{}' is not set or invalid.", PingPeriodProperty);
      logger.info("  Ping-check service is not activated.");
      logger.info("------------------------------------------------");
    }
  }
}
