package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableFactory;


public class PoolPortableFactory implements PortableFactory {

  @Override
  public Portable create(int classId) {
    if (classId == PortableClassId.DIALOG_AGENT_INSTANCE_RESOURCE) {
      return new DialogAgentInstanceResourcePortable();
    } else if (classId == PortableClassId.DIALOG_AGENT_INSTANCE_RESOURCE_SKILL) {
      return new DialogAgentInstanceResourceSkillPortable();
    } else {
      throw new IllegalArgumentException(classId + " unsupported classId");
    }
  }
}
