package ai.maum.m2u.pool;

import ai.maum.m2u.common.portable.DialogAgentInstanceResourcePortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceResourceSkillPortable;
import ai.maum.utils.HazelcastConnector;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.util.Collection;
import java.util.Vector;
import maum.m2u.server.DialogAgentInstancePoolGrpc;
import maum.m2u.server.Pool.DialogAgentInstanceKey;
import maum.m2u.server.Pool.DialogAgentInstanceResource;
import maum.m2u.server.Pool.DialogAgentInstanceResourceSkill;
import maum.m2u.server.Pool.DialogAgentInstanceStat;
import maum.m2u.server.Pool.PoolState;
import maum.m2u.server.Pool.RegisterResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PoolServiceImpl extends DialogAgentInstancePoolGrpc.DialogAgentInstancePoolImplBase {

  static final Logger logger = LoggerFactory.getLogger(PoolServiceImpl.class);
  static private final String DAIR = "dialog-agent-instance-resource";
  static private final String DAIR_SKILL = "dialog-agent-instance-resource-skill";

  @SuppressWarnings("unchecked")
  public void cleanUpDanglingSkills() {
    HazelcastInstance hzc = HazelcastConnector.getInstance().client;
    IMap<String, DialogAgentInstanceResourcePortable> dairMap = hzc.getMap(DAIR);
    IMap<String, DialogAgentInstanceResourceSkillPortable> dairSkillMap = hzc.getMap(DAIR_SKILL);
    Collection<DialogAgentInstanceResourceSkillPortable> skillMappings = dairSkillMap.values();

    // skill MAP에 dair key 취합
    Vector<String> dairKeys = new Vector<>();
    for (DialogAgentInstanceResourceSkillPortable skillMapping : skillMappings) {
      String dairKey = skillMapping.getProtobufObj().getDairKey();
      if (!dairKeys.contains(dairKey)) {
        dairKeys.add(dairKey);
      }
    }

    // DAIR Map에 dair key가 없으면 DAIR-Skill Map에서 해당 key를 모두 삭제한다.
    for (String dairKey : dairKeys) {
      if (!dairMap.containsKey(dairKey)) {
        // DAIR-Skill Map에서 삭제
        hzc.getMap(DAIR_SKILL).removeAll(new SqlPredicate("dair_key= '" + dairKey + "'"));
        logger.debug("===== Remove All DAIR '{}' from DAIR-Skill", dairKey);
      }
    }
  }

  /**
   * <pre>
   * *
   * Pool Service가 기동을 마치고 서비스 상태인지 확인한다.
   * Hazelcast와 Pool Service는 m2u 동작에 필수 서비스 이므로
   * 주요 서비스 기동 전에 Pool Service가 준비되었는지 확인한 후 진행하기 위해 호출한다.
   *
   * 이 RPC가 호출 되면 서비스가 준비되었으므로 항상 true를 리턴한다.
   * </pre>
   */
  @Override
  public void isReady(com.google.protobuf.Empty request,
      io.grpc.stub.StreamObserver<maum.m2u.server.Pool.PoolState> response) {
    logger.info("===== isReady");
    try {
      response.onNext(PoolState.newBuilder().setIsReady(true).build());
      response.onCompleted();
    } catch (Exception e) {
      logger.error("===== isRead FAILED");
      logger.error("{} => ", e.getMessage(), e);
      response.onError(new StatusException(Status.INTERNAL
          .withDescription(e.getMessage())
          .withCause(e)));
    }
  }

  /**
   * 실행되고 있는 DAI를 pool에 등록한다.
   */
  @Override
  public void register(DialogAgentInstanceResource request,
      StreamObserver<RegisterResponse> response) {

    try {
      cleanUpDanglingSkills();
      HazelcastInstance hzc = HazelcastConnector.getInstance().client;

      // request proto object를 Hazelcast portable로 변환
      DialogAgentInstanceResourcePortable dairp = new DialogAgentInstanceResourcePortable();
      dairp.setProtobufObj(request);

      // DAIR-Skill Map에 추가
      IMap<String, DialogAgentInstanceResourceSkillPortable> dairSkillMap = hzc.getMap(DAIR_SKILL);
      DialogAgentInstanceResource dair = dairp.getProtobufObj();
      String dairKey = dair.getKey();
      DialogAgentInstanceResourceSkill.Builder builder =
          DialogAgentInstanceResourceSkill.newBuilder()
              .setDairKey(dairKey)
              .setChatbot(dair.getChatbot())
              .setLang(dair.getLang());
      // 모든 skill에 대해서 DAIR-Skill Map에 추가
      for (String skill : dair.getSkillsList()) {
        DialogAgentInstanceResourceSkillPortable dairsp = new DialogAgentInstanceResourceSkillPortable();
        builder.setSkill(skill);
        dairsp.setProtobufObj(builder.build());
        dairSkillMap.set(dairKey + "_" + skill, dairsp);
      }

      // Portable를 Hazelcast DAIR map에 저장
      IMap<String, DialogAgentInstanceResourcePortable> dairMap = hzc.getMap(DAIR);
      dairMap.set(dairKey, dairp);

      // 결과를 Return
      response.onNext(RegisterResponse.newBuilder()
          .setKey(dairKey)
          .setRegisteredAt(dairp.getProtobufObj().getStartedAt())
          .build()
      );
      response.onCompleted();
      logger.info("===== register DAIR '{}' SUCCESS", dairKey);
    } catch (Exception e) {
      logger.error("===== register DAIR '{}' FAILED", request.getKey());
      logger.error("{} => ", e.getMessage(), e);
      response.onError(new StatusException(Status.INTERNAL
          .withDescription(e.getMessage())
          .withCause(e)));
    }
  }

  /**
   * DAI를 pool에서 제거한다.
   */
  @Override
  @SuppressWarnings("unchecked")
  public void unregister(DialogAgentInstanceKey request,
      StreamObserver<DialogAgentInstanceStat> response) {

    try {
      // Portable를 Hazelcast DAIR map에 저장
      HazelcastInstance hzc = HazelcastConnector.getInstance().client;
      IMap<String, DialogAgentInstanceResourcePortable> dairMap = hzc.getMap(DAIR);
      DialogAgentInstanceStat.Builder builder = DialogAgentInstanceStat.newBuilder();

      // 요청한 Key가 있는지 확인
      String dair_key = request.getKey();
      if (dairMap.containsKey(dair_key)) {
        DialogAgentInstanceResourcePortable dair = dairMap.remove(dair_key);

        builder.setKey(dair.getProtobufObj().getKey())
            .setRegisteredAt(dair.getProtobufObj().getStartedAt());

        logger.info("===== unregister DAIR '{}' SUCCESS", dair.getProtobufObj().getKey());
      } else {
        // 요청한 Key가 없으면
        logger.info("===== unregister DAIR key '{}' is not found.", dair_key);
      }
      cleanUpDanglingSkills();

      // 결과를 Return
      response.onNext(builder.build());
      response.onCompleted();
    } catch (Exception e) {
      logger.error("===== unregister DAIR FAILED", request.getKey());
      logger.error("{} => ", e.getMessage(), e);
      response.onError(new StatusException(Status.INTERNAL
          .withDescription(e.getMessage())
          .withCause(e)));
    }
  }
}

