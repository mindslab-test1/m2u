package ai.maum.m2u.mapclient;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.MaumToYouProxyServiceGrpc;
import org.slf4j.LoggerFactory;

public class TestGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(SampleMain.class);

  private ManagedChannel channel;
  private MaumToYouProxyServiceGrpc.MaumToYouProxyServiceStub mapStub;

  private static final String M2U_AUTH_SIGN_IN_HEADER = "m2u-auth-sign-in";
  private static final String M2U_AUTH_TOKEN_HEADER = "m2u-auth-token";
  public static final String M2U_INTERNAL_AUTH_HEADER = "m2u-auth-internal";

  public TestGrpcInterfaceManager(String host, int port) {
    this(ManagedChannelBuilder.forAddress(host, port)
        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
        // needing certificates.
        .usePlaintext(true));
  }

  TestGrpcInterfaceManager(ManagedChannelBuilder<?> channelBuilder) {
    channel = channelBuilder.build();
    mapStub = MaumToYouProxyServiceGrpc.newStub(channel);

  }

  public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  /**
   * 이벤트 스트림 콜
   */
  public MapDirective callEvent(MapEvent req, String authToken) {
    //logger.info("==========TestGrpcInterfaceManager.signIn = {}", req);

    try {
      // 헤더가 중첩으로 쌓이는 현상이 발생하여 stub 초기화
      mapStub = MaumToYouProxyServiceGrpc.newStub(channel);

      // GRPC 헤더에  추가
      Metadata fixedHeaders = new Metadata();
      // Sigin Service 호출시 M2U_AUTH_SIGN_IN_HEADER가 추가되어야 합니다.
      Metadata.Key<String> signInkey = Metadata.Key
          .of(M2U_AUTH_SIGN_IN_HEADER, Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> authTokenkey = null;
      Metadata.Key<String> internalAuthHeaderKey = null;

      if (!"".equals(authToken)) {
        // authToken토큰을 발급받은 이후에는 모든 Service 호출시 M2U_AUTH_TOKEN_HEADER에 토큰값을 할당해야 합니다.
        authTokenkey = Metadata.Key
            .of(M2U_AUTH_TOKEN_HEADER, Metadata.ASCII_STRING_MARSHALLER);
        fixedHeaders.put(authTokenkey, authToken);
      }

      internalAuthHeaderKey = Metadata.Key
          .of(M2U_INTERNAL_AUTH_HEADER, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(internalAuthHeaderKey, M2U_INTERNAL_AUTH_HEADER);

      // M2U_AUTH_SIGN_IN_HEADER에 값이 없어도 되지만 OperationSyncId를 할당해 주었습니다.
      fixedHeaders.put(signInkey, req.getEvent().getOperationSyncId());
      mapStub = MetadataUtils.attachHeaders(mapStub, fixedHeaders);

      // 비동기 처리를 위해서 CountDownLatch를 사용하였습니다.
      final CountDownLatch finishLatch = new CountDownLatch(1);
      HashMap<String, MapDirective> result = new HashMap<>();

      StreamObserver<MapDirective> responseObserver = new StreamObserver<MapDirective>() {
        @Override
        public void onNext(MapDirective res) {
          result.put("result", res);
        }

        @Override
        public void onError(Throwable t) {
          logger
              .error("#@ AuthenticationProviderGrpc.signIn onError [{}]", Status.fromThrowable(t), t);
          finishLatch.countDown();
        }

        @Override
        public void onCompleted() {
          finishLatch.countDown();
        }
      };

      StreamObserver<MapEvent> requestObserver = mapStub
          .eventStream(responseObserver);

      try {
        requestObserver.onNext(req);
        requestObserver.onCompleted();

        // Receiving happens asynchronously
        finishLatch.await(1, TimeUnit.MINUTES);

        return result.get("result");
      } catch (RuntimeException e) {
        // Cancel RPC
        requestObserver.onError(e);
        throw e;
      } catch (InterruptedException e) {
        logger.error("callEvent e : " , e);
        return null;
      }
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("callEvent e1 : " , e1);
      }
      return null;
    }
  }

  public StreamObserver<MapEvent> callStreamEvent(MapEvent event,
                                                  StreamObserver<MapDirective> receiver,
                                                  String authToken) {
    mapStub = MaumToYouProxyServiceGrpc.newStub(channel);

    Metadata.Key<String> authTokenkey = null;
    // authToken토큰을 발급받은 이후에는 모든 Service 호출시 M2U_AUTH_TOKEN_HEADER에 토큰값을 할당해야 합니다.
    authTokenkey = Metadata.Key
        .of(M2U_AUTH_TOKEN_HEADER, Metadata.ASCII_STRING_MARSHALLER);
    Metadata fixedHeaders = new Metadata();
    fixedHeaders.put(authTokenkey, authToken);
    mapStub = MetadataUtils.attachHeaders(mapStub, fixedHeaders);

    StreamObserver<MapEvent> sender = mapStub.eventStream(receiver);
    sender.onNext(event);
    return sender;
  }


}
