package ai.maum.m2u.mapclient;

import static com.google.protobuf.util.Timestamps.fromMillis;
import static java.lang.System.currentTimeMillis;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.ListValue;
import com.google.protobuf.Message.Builder;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import com.google.protobuf.util.JsonFormat;
import io.grpc.stub.StreamObserver;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import maum.brain.stt.Speech.SpeechRecognitionParam;
import maum.brain.stt.Speech.StreamingRecognizeResponse;
import maum.brain.tts.Speech.SpeechSynthesizerParam;
import maum.common.Audioencoding.AudioEncoding;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.CardOuterClass.Card;
import maum.m2u.common.DeviceOuterClass.Device;
import maum.m2u.common.DeviceOuterClass.Device.Capability;
import maum.m2u.common.Dialog.OpenUtter;
import maum.m2u.common.Dialog.Utter;
import maum.m2u.common.Dialog.Utter.InputType;
import maum.m2u.common.DirectiveOuterClass.AvatarSetExpressionPayload;
import maum.m2u.common.DirectiveOuterClass.LauncherAuthorizePayload;
import maum.m2u.common.DirectiveOuterClass.LauncherFillSlotsPayload;
import maum.m2u.common.DirectiveOuterClass.LauncherLaunchAppPayload;
import maum.m2u.common.DirectiveOuterClass.LauncherLaunchPagePayload;
import maum.m2u.common.DirectiveOuterClass.LauncherViewMapPayload;
import maum.m2u.common.EventOuterClass.DialogAgentForwarderParam;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Authentication.SignInPayload;
import maum.m2u.map.Authentication.SignInResultPayload;
import maum.m2u.map.Authentication.SignOutPayload;
import maum.m2u.map.Authentication.SignOutResultPayload;
import maum.m2u.map.Authentication.UserKey;
import maum.m2u.map.Authentication.UserSettings;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.EventStream.EventContext;
import maum.m2u.map.Map.EventStream.EventParam;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapDirective.TestDirectiveCase;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.Map.MapException;
import maum.m2u.map.Payload.ExpectSpeechPayload;
import maum.m2u.map.Payload.RenderTextPayload;
import org.slf4j.LoggerFactory;

public class SampleMain {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(SampleMain.class);

  static final String FIX_STREAM_ID = "c2980a44-22db-11e8-9df1-e0d55e228300";
  static final String FIX_OERPATION_SYNC_ID = "c2980a45-22db-11e8-9df1-e0d55e228300";
  static final String FIX_AUTH_TOKEN = "1602a950-e651-11e1-84be-00145e76c700";

  private TestGrpcInterfaceManager client = null;
  private Device myDevice;
  private Location myLocation;
  private String authToken = null;

  public SampleMain() throws InvalidProtocolBufferException {
    client = new TestGrpcInterfaceManager("127.0.0.1", 9911);
    myDevice = Device.newBuilder()
        .setId("ID_123456").setType("PC").setVersion("0.1")
        .setChannel("CHANNEL_12345")
        .setSupport(
            Capability.newBuilder()
                .setSupportRenderText(true)
                .setSupportRenderCard(true)
                .setSupportSpeechSynthesizer(true)
                .setSupportPlayAudio(true)
                .setSupportPlayVideo(true)
                .setSupportAction(true)
                .setSupportMove(true)
                .setSupportExpectSpeech(true))
        .setTimestamp(fromMillis(currentTimeMillis())).setTimezone("KST+9")
        .setMeta(Struct.newBuilder().putFields("manufacture",
            Value.newBuilder().setStringValue("YOUR COMPANY").build()))
        .build();
    myLocation = Location.newBuilder()
        .setLatitude(10)
        .setLongitude(120).setLocation("마인즈랩").build();
  }

  // 간단하게 UUID로 구현하였습니다. 중복이 우려될 경우 해당부분 수정하셔서 사용하시면 됩니다.
  private static String getUUID() {
    return UUID.randomUUID().toString();
  }

  private static String printToJson(MessageOrBuilder msg)
      throws InvalidProtocolBufferException {
    return JsonFormat.printer().includingDefaultValueFields().print(msg);
  }

  public void run() {
    try {
      signIn();
      open();
      speechToSpeechTalk();
      close();
      signOut();
      client.shutdown();
    } catch (InvalidProtocolBufferException e) {
      logger.error("run e : " , e);
    } catch (InterruptedException e) {
      logger.error("run e : " , e);
    }
  }

  private void signIn() throws InvalidProtocolBufferException {
    SignInPayload.Builder signInPayload = SignInPayload.newBuilder().setUserkey("admin")
        .setPassphrase("1234");

    // Struct Ojbect에 Payload Object를 merge하는 예재 소스입니다.
    String json = printToJson(signInPayload);
    Struct.Builder stb = Struct.newBuilder();
    JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

    MapEvent.Builder signInParam = MapEvent.newBuilder()
        .setEvent(
            EventStream.newBuilder()
                .setInterface(
                    // setInterface에 하드코딩 되어 있는 부분은 각 EventParam마다 고정값입니다.[이하동일]
                    AsyncInterface.newBuilder().setInterface("Authentication")
                        .setOperation("SignIn")
                        .setType(OperationType.OP_EVENT).setStreaming(false).build())
                // setStreamId, setOperationSyncId[global id] 부분은 중복되지 않는 임의의값을 사용하시면 됩니다.[이하동일]
                .setStreamId(getUUID())
                .setOperationSyncId(getUUID())
                // setPayload Service parameter 필드입니다.[이하동일]
                // setBeginAt 동일하게 셋팅해주시면 됩니다.[이하동일]
                .setPayload(stb.clone()).setBeginAt(fromMillis(currentTimeMillis())));

    MapDirective signInResult = client.callEvent(signInParam.build(), "");

    logger.debug("#### signInParam ####");
    logger.debug("{}", printToJson(signInParam));
    logger.debug("#### signInResult ####");
    // payload를 꺼내는 예제입니다.
    // payload내부의 데이터가 oneof type인 경우 has{fieldName} method를 사용하여 존재여부 확인후 사용하시면 됩니다.
    logger.debug("{}", printToJson(signInResult.getDirective().getPayload()));
    // SignIn의 result값에서 authToken을 꺼내는 예제입니다.
    // .getFieldsOrThrow("authSuccess").getStructValueOrBuilder()
    //  - proto message를 꺼내는 예제입니다.[proto에서 _규칙을 사용하지만 protobuf 내부적으로 camel case로 변환 됩니다.

    // getFieldsOrThrow("authToken").getStringValue() : proto에서 일반 데이터를 꺼내는 예제입니다.
    authToken = signInResult.getDirective().getPayload().getFieldsOrThrow("authSuccess")
        .getStructValueOrBuilder().getFieldsOrThrow("authToken").getStringValue();
  }

  private void signOut() throws InvalidProtocolBufferException {
    /**
     *  signOut process
     *
     */
    SignOutPayload.Builder signOutPayload = SignOutPayload.newBuilder()
        .setAuthToken(authToken);
    Struct.Builder stb = Struct.newBuilder();
    String json = printToJson(signOutPayload);
    JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

    MapEvent.Builder signOutParam = MapEvent.newBuilder()
        .setEvent(EventStream.newBuilder().setInterface(
            AsyncInterface.newBuilder().setInterface("Authentication").setOperation("SignOut")
                .setType(OperationType.OP_EVENT).setStreaming(false))
            .setStreamId(getUUID())
            .setOperationSyncId(getUUID())
            .setPayload(stb.clone()).setBeginAt(fromMillis(currentTimeMillis())));

    MapDirective signOutResult = client.callEvent(signOutParam.build(), "");

    logger.debug("#### signOutParam ####");
    logger.debug("{}", printToJson(signOutParam));
  }

  private void open() throws InvalidProtocolBufferException {
    /**
     *  dialogOpen process
     *
     */
    OpenUtter.Builder openPayload = OpenUtter.newBuilder().setUtter("UTTER1")
        .setChatbot("CHATBOT1");
    String json = printToJson(openPayload);
    Struct.Builder stb = Struct.newBuilder();
    JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

    MapEvent.Builder openParam = MapEvent.newBuilder()
        .setEvent(EventStream.newBuilder().setInterface(
            AsyncInterface.newBuilder().setInterface("Dialog").setOperation("Open")
                .setType(OperationType.OP_EVENT).setStreaming(false))
            .setStreamId(getUUID())
            .setOperationSyncId(getUUID())
            .addContexts(EventContext.newBuilder().setDevice(myDevice))
            .addContexts(
                // setLocation은 DialogWeb service에 접근하는경우 셋팅해줍니다.[이하동일]
                EventContext.newBuilder().setLocation(myLocation))
            .setPayload(stb.clone()).setBeginAt(fromMillis(currentTimeMillis())));
    MapDirective openResult = client.callEvent(openParam.build(), authToken);

    logger.debug("#### openParam ####");
    logger.debug("{}", printToJson(openParam));
    logger.debug("#### openResult ####");
    logger.debug("{}", printToJson(openResult));
  }

  private void close() throws InvalidProtocolBufferException {
    /**
     *  dialogClose process
     *
     */
    MapEvent.Builder closeParam = MapEvent.newBuilder()
        .setEvent(EventStream.newBuilder()
            .setInterface(
                AsyncInterface.newBuilder().setInterface("Dialog").setOperation("Close")
                    .setType(OperationType.OP_EVENT).setStreaming(false))
            .setStreamId(FIX_STREAM_ID)
            .setOperationSyncId(FIX_OERPATION_SYNC_ID)
            .addContexts(EventContext.newBuilder().setDevice(myDevice))
            .addContexts(EventContext.newBuilder().setLocation(myLocation)));

    MapDirective closeResult = client.callEvent(closeParam.build(), authToken);

    logger.debug("#### closeParam ####");
    logger.debug("{}", printToJson(closeParam));
  }

  private void textToTextTalk() throws InvalidProtocolBufferException {
    /**
     *  TextToTextTalk process
     *
     */
    Utter.Builder taklPayload = Utter.newBuilder().setUtter("안녕하세요.").setInputType(
        InputType.valueOf("KEYBOARD")).setLang(Lang.ko_KR);
    String json = printToJson(taklPayload);
    Struct.Builder stb = Struct.newBuilder();
    JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

    MapEvent.Builder talkParam = MapEvent.newBuilder()
        .setEvent(EventStream.newBuilder().setInterface(
            AsyncInterface.newBuilder().setInterface("Dialog").setOperation("TextToTextTalk")
                .setType(OperationType.OP_EVENT).setStreaming(false))
            .setStreamId(getUUID())
            .setOperationSyncId(getUUID())
            .addContexts(EventContext.newBuilder().setDevice(myDevice))
            .addContexts(EventContext.newBuilder().setLocation(myLocation))
            .setPayload(stb.clone()).setBeginAt(fromMillis(currentTimeMillis())));

    MapDirective talkResult = client.callEvent(talkParam.build(), authToken);

    logger.debug("#### talkParam ####");
    logger.debug("{}", printToJson(talkParam));
    logger.debug("#### talkResult ####");
    logger.debug("{}", printToJson(talkResult));
  }

  private void speechToSpeechTalk() throws InvalidProtocolBufferException {
    MapEvent ev = MapEvent.newBuilder().setEvent(EventStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface("Dialog")
            .setOperation("SpeechToSpeechTalk")
            .setStreaming(true))
        .setStreamId(getUUID())
        .setOperationSyncId(getUUID())
        .setParam(EventParam.newBuilder()
            .setSpeechRecognitionParam(SpeechRecognitionParam.newBuilder()
                .setEncoding(AudioEncoding.LINEAR16)
                .setSampleRate(16000)
                .setModel("baseline")
                .setLang(Lang.ko_KR)
                .setSingleUtterance(true)))
        .addContexts(EventContext.newBuilder().setDevice(myDevice))
        .addContexts(EventContext.newBuilder().setLocation(myLocation))).build();

    StreamObserver<MapDirective> receiver = new MapDirectiveRecvObserver();
    StreamObserver<MapEvent> sender = client.callStreamEvent(ev, receiver, authToken);

    // Client로 부터 수신받은 BinaryMessage를 MapEvent ByteStream로 변경
    try {
      InputStream is = new FileInputStream("sample.pcm");
      int readBytes;
      byte[] data = new byte[3200];

      while ((readBytes = is.read(data, 0, data.length)) != -1) {
        MapEvent evBytes = MapEvent.newBuilder().setBytes(
            ByteString.copyFrom(data, 0, readBytes)).build();
        // 일부 전송
        sender.onNext(evBytes);
        Thread.sleep(100);
      }
      // 전송 완료
      sender.onCompleted();
    } catch (InterruptedException e) {
      logger.error("speechToSpeechTalk e : " , e);
    } catch (FileNotFoundException e) {
      logger.error("speechToSpeechTalk e : " , e);
    } catch (IOException e) {
      logger.error("speechToSpeechTalk e : " , e);
    }
  }

  void setUserSettings() throws InvalidProtocolBufferException {
    Struct.Builder st = Struct.newBuilder();
    st.putFields("userkey",
        Value.newBuilder().setStringValue("UpdateUserSettings").build());
    st.putFields("passphrase",
        Value.newBuilder().setStringValue("1234").build());
    st.putFields("authParams",
        Value.newBuilder().setListValue(ListValue.newBuilder()).build());
    UserSettings.Builder userSettings = UserSettings.newBuilder().setAuthToken(FIX_AUTH_TOKEN)
        .setSettings(st);

    String json = printToJson(userSettings);
    Struct.Builder stb = Struct.newBuilder();
    JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

    MapEvent.Builder updateUserSettingsParam = MapEvent.newBuilder()
        .setEvent(
            EventStream.newBuilder().setInterface(
                AsyncInterface.newBuilder().setInterface("Authentication")
                    .setOperation("UpdateUserSettings")
                    .setType(OperationType.OP_EVENT).setStreaming(false))
                .setStreamId(getUUID())
                .setOperationSyncId(getUUID())
                .setPayload(stb.clone()).setBeginAt(fromMillis(currentTimeMillis())));

    MapDirective updateUserSettingsResult = client.callEvent(updateUserSettingsParam.build(), "");

    logger.debug(
        "#### updateUserSettingsParam ####");
    logger.debug("{}", printToJson(updateUserSettingsParam));
    logger.debug("#### updateUserSettingsResult ####");
    logger.debug("{}", printToJson(updateUserSettingsResult.getDirective()));
  }

  private void getUserSettings() throws InvalidProtocolBufferException {
    UserKey.Builder userKey = UserKey.newBuilder().setAuthToken(FIX_AUTH_TOKEN);

    String json = printToJson(userKey);
    Struct.Builder stb = Struct.newBuilder();

    stb.clear();
    JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

    MapEvent.Builder userKeyParam = MapEvent.newBuilder()
        .setEvent(
            EventStream.newBuilder()
                .setInterface(
                    AsyncInterface.newBuilder().setInterface("Authentication")
                        .setOperation("GetUserSettings")
                        .setType(OperationType.OP_EVENT).setStreaming(false))
                .setStreamId(getUUID())
                .setOperationSyncId(getUUID())
                .setPayload(stb.clone()).setBeginAt(fromMillis(currentTimeMillis())));

    MapDirective userKeyResult = client.callEvent(userKeyParam.build(), "");

    logger.debug("#### getUserSettingsParam ####");
    logger.debug("{}", printToJson(userKeyParam));
    logger.debug("#### getUserSettingsResult ####");
    logger.debug("{}", printToJson(userKeyResult.getDirective()));
  }

  /**
   * MAP으로 부터 수신받은 MapDirective, MapException, Bytes 등에 대한 처리
   * 클라이언트는 하나의 수신 resolver를 통해서 출력을 처리하면 된다.
   */
  class MapDirectiveRecvObserver implements StreamObserver<MapDirective> {

    /**
     * 텍스트 출력 콜백
     * @param text RnderText 호출에 따르는 Payload
     */
    public void onViewRenderText(RenderTextPayload text) {
      logger.info("#@ IN Recv [{}]", text);
    }

    /**
     * 카드 출력 콜백
     * @param card 카드 메시지
     */
    public void onViewRenderCard(Card card) {
      logger.info("#@ IN Recv [{}]", card);
    }

    /**
     * 메타 데이터 수신 콜백
     * @param hidden 메타 데이터
     */
    public void onViewRenderHidden(Struct hidden) {
      logger.info("#@ IN Recv [{}]", hidden);
    }

    /**
     * 음성인식 진행 중일 때 콜백
     * @param o 음성인식 응답
     */
    void onSpeechRecognizerRecognizing(StreamingRecognizeResponse o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 음성인식 완료되었을 때 콜백
     * @param o 음성인식 응답
     */
    void onSpeechRecognizerRecognized(StreamingRecognizeResponse o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 대화 처리 중일 때 콜백
     */
    void onDialogProcessing() {
      logger.info("#@ IN Recv [{}], dialog processing");
    }

    /**
     * 음성합성 출력할 때 콜백
     */
    void onSpeechSynthesizerPlayStarted(SpeechSynthesizerParam o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 음성 입력 대기 수신할 때 콜백
     * @param o 음성입력 대기에 필요한 추가적인 Payload 데이터
     */
    void onMicrophoneExpectSpeech(ExpectSpeechPayload o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 아바타 설정 명령어 수신할 때 콜백
     * @param {AvatarSetExpressionPayload} o 아바타 출력을 위한 페이로드
     */
    void onAvatarSetExpression(AvatarSetExpressionPayload o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 인증을 실행하라는 명렁어 수신할때 콜백
     * @param o 인증수팽 명령
     * @param p 인증 수행 처리해야할 다시 전송할 파라미터
     */
    void onLauncherAuthorize(LauncherAuthorizePayload o, DialogAgentForwarderParam p) {
      logger.info("#@ IN Recv [{}] [{}]", o, p);
    }

    /**
     * 단말이 가지고 있는 슬롯을 채워서 보내달라는 콜백
     * @param o
     * @param p
     */
    void onLauncherFillSlots(LauncherFillSlotsPayload o,
                             DialogAgentForwarderParam p) {
      logger.info("#@ IN Recv [{}] [{}]", o, p);
    }

    /**
     * 클라이언트에게 LaunchApp을 실행해달라는 콜백
     * @param o
     */
    void onLauncherLaunchApp(LauncherLaunchAppPayload o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 클라이언트에게 LaunchPage을 실행해달라는 콜백
     * @param o
     */
    void onLauncherLaunchPage(LauncherLaunchPagePayload o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 클라이언트에게 LauncherViewMap을 실행해달라는 콜백
     * @param  o
     */
    void onLauncherViewMap(LauncherViewMapPayload o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 이미지 인식이 완료되었을 때
     */
    void onImageDocumentRecognized() {
      logger.info("#@ IN Recv []");
    }

    /**
     * 로그인에 대한 응답 콜백
     * @param o 인증 결과 PAYLOAD
     */
    void onAuthenticationSignInResult(SignInResultPayload o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 로그아웃에 대한 응답 콜백
     * @param o 응답 메시지
     */
    void onAuthenticationSignOutResult(SignOutResultPayload o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 다중 인증에 대한 응답이 올 때 발생하는 콜백
     * @param o 인증 결과 PAYLOAD
     */
    void onAuthenticationMultiFactorVerifyResult(SignInResultPayload o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 사용자 속성 정의에 대한 응답이 올 때 발생하는 콜백
     * @param o 사용자 속성 정의 결과 PAYLOAD
     */
    void onAuthenticationUpdateUserSettingsResult(UserSettings o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 사용자 속성 조회에 대한 응답이 올 때 발생하는 콜백
     * @param o 사용자 속성 정의 결과 PAYLOAD
     */
    void onAuthenticationGetUserSettingsResult(UserSettings o) {
      logger.info("#@ IN Recv [{}]", o);
    }

    /**
     * 예외가 발생한 경우에 발생할 콜백
     * @param e MapException
     */
    void onException(MapException e) {
      logger.info("#@ IN Recv [{}]", e);
    }

    /**
     * 호출이 완료될 때 나올 콜백
     * @param opid 오퍼레이션 ID
     */
    void onOperationCompleted(String opid) {
      logger.info("#@ IN Recv [{}]", opid);
    }

    private void mergePayload(Struct payload, Builder builder) {
      try {
        String json = printToJson(payload);
        JsonFormat.parser().merge(json, builder);
      } catch (InvalidProtocolBufferException e) {
      }
    }

    String operationSyncId = null;

    @Override
    public void onNext(MapDirective directive) {
      // 1. Map으로 부터 Directive를 수신 합니다
      // 2. MapDirective를 Client로 전달합니다
      if (directive.hasDirective()) {
        DirectiveStream dir = directive.getDirective();
        if (operationSyncId == null) {
          operationSyncId = dir.getOperationSyncId();
        }
        AsyncInterface aif = dir.getInterface();
        String ifname = aif.getInterface() + "." + aif.getOperation();
        switch (ifname) {
          case "View.RenderText": {
            RenderTextPayload.Builder builder = RenderTextPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onViewRenderText(builder.build());
            break;
          }
          case "View.RenderCard": {
            Card.Builder builder = Card.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onViewRenderCard(builder.build());
            break;
          }
          case "View.RenderHidden": {
            onViewRenderHidden(dir.getPayload());
            break;
          }
          case "SpeechRecognizer.Recognizing": {
            StreamingRecognizeResponse.Builder builder = StreamingRecognizeResponse.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onSpeechRecognizerRecognizing(builder.build());
            break;
          }
          case "SpeechRecognizer.Recognized": {
            StreamingRecognizeResponse.Builder builder = StreamingRecognizeResponse.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onSpeechRecognizerRecognized(builder.build());
            break;
          }
          case "Dialog.Processing": {
            onDialogProcessing();
            break;
          }
          case "SpeechSynthesizer.Play": {
            onSpeechSynthesizerPlayStarted(dir.getParam().getSpeechSynthesizerParam());
            break;
          }
          case "Microphone.ExpectSpeech": {
            ExpectSpeechPayload.Builder builder = ExpectSpeechPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onMicrophoneExpectSpeech(builder.build());
            break;
          }
          case "Avatar.SetExpression": {
            AvatarSetExpressionPayload.Builder builder = AvatarSetExpressionPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onAvatarSetExpression(builder.build());
            break;
          }
          case "ImageDocument.Recognized": {
            onImageDocumentRecognized();
            break;
          }
          case "Authentication.SignInResult": {
            SignInResultPayload.Builder builder = SignInResultPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onAuthenticationSignInResult(builder.build());
            break;
          }
          case "Authentication.SignOutResult": {
            SignOutResultPayload.Builder builder = SignOutResultPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onAuthenticationSignOutResult(builder.build());
            break;
          }
          case "Authentication.MultiFactorVerifyResult": {
            SignInResultPayload.Builder builder = SignInResultPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onAuthenticationMultiFactorVerifyResult(builder.build());
            break;
          }
          case "Authentication.UpdateUserSettingsResult": {
            UserSettings.Builder builder = UserSettings.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onAuthenticationUpdateUserSettingsResult(builder.build());
            break;
          }
          case "Authentication.GetUserSettingsResult": {
            UserSettings.Builder builder = UserSettings.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onAuthenticationGetUserSettingsResult(builder.build());
            break;
          }
          case "Launcher.Authorize": {
            LauncherAuthorizePayload.Builder builder = LauncherAuthorizePayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onLauncherAuthorize(builder.build(), dir.getParam().getDaForwardParam());
            break;
          }
          case "Launcher.FillSlots": {
            LauncherFillSlotsPayload.Builder builder = LauncherFillSlotsPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onLauncherFillSlots(builder.build(), dir.getParam().getDaForwardParam());
            break;
          }
          case "Launcher.LaunchApp": {
            LauncherLaunchAppPayload.Builder builder = LauncherLaunchAppPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onLauncherLaunchApp(builder.build());
            break;
          }
          case "Launcher.LaunchPage": {
            LauncherLaunchPagePayload.Builder builder = LauncherLaunchPagePayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onLauncherLaunchPage(builder.build());
            break;
          }
          case "Launcher.ViewMap": {
            LauncherViewMapPayload.Builder builder = LauncherViewMapPayload.newBuilder();
            mergePayload(dir.getPayload(), builder);
            onLauncherViewMap(builder.build());
            break;
          }
        }
      } else if (directive.hasException()) {
        onException(directive.getException());
      } else if (directive.hasStreamBreak()) {
        logger.info("Stream Break {}", directive.getStreamBreak());
      } else if (directive.hasStreamEnd()) {
        logger.info("Stream Break {}", directive.getStreamEnd());
      } else if (directive.getTestDirectiveCase().equals(TestDirectiveCase.BYTES)) {
        logger.info("Stream BODY BYTES{}", directive.getBytes());
      }
    }

    @Override
    public void onError(Throwable throwable) {
      logger.error("onError throwable : " , throwable);
      logger.info("#@ IN MapToClient onError [{}]", throwable);


      logger.debug("#### MapDirective ERROR ####");
      if (client != null) {
        logger.info("#@ End restToMapChannel close by onError");
        // _restToMapChannel을 close 합니다
        try {
          client.shutdown();
        } catch (InterruptedException e) {
          logger.error("onError e : " , e);
        }
      }
    }

    @Override
    public void onCompleted() {
      logger.debug("#### MapDirective COMPLETED ####");
      logger.debug("#@ IN MapToClient onCompleted.. ####");

      // 모든 응답이 다 끝난 경우에 처리
      onOperationCompleted(operationSyncId);
    }
  }


  public static void main(String[] args) {
    try {
      SampleMain sample = new SampleMain();
      sample.run();
    } catch (Exception e) {
      logger.error("main e : " , e);
    }
  }
}
