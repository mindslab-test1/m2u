package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.DialogAgentInstanceDAO;
import ai.maum.m2u.admin.dao.DialogAgentManagerDAO;
import ai.maum.m2u.admin.itfc.GrpcInterfaceManager;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.util.UUID;
import maum.m2u.console.Da.DialogAgentManager;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import maum.m2u.console.DaInstance.DialogAgentInstance;
import maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo;
import maum.m2u.console.DaInstance.DialogAgentInstanceList;
import maum.m2u.console.DaInstance.DialogAgentInstanceResourceList;
import maum.m2u.console.DaInstance.RunDialogAgentInstanceParamList;
import maum.m2u.da.Provider.InitParameter;
import maum.m2u.server.Dam;
import maum.m2u.server.Dam.DialogAgentInstances;
import maum.m2u.server.Dam.DialogAgentManagerStat;
import maum.m2u.server.Dam.RunDialogAgentInstanceParam;
import maum.m2u.server.Pool.DialogAgentInstanceResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DialogAgentInstanceAdminImpl extends
    maum.m2u.console.DialogAgentInstanceAdminGrpc.DialogAgentInstanceAdminImplBase {

  static final Logger logger = LoggerFactory
      .getLogger(DialogAgentInstanceAdminImpl.class);

  static DialogAgentInstanceDAO daiDao = new DialogAgentInstanceDAO();
  static DialogAgentManagerDAO damDao = new DialogAgentManagerDAO();

  @Override
  public void getDialogAgentInstanceAllList(com.google.protobuf.Empty req,
      StreamObserver<DialogAgentInstanceList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentInstanceAllList");

    try {
      DialogAgentInstanceList.Builder dailb = daiDao
          .getDialogAgentInstanceAllList();

      res.onNext(dailb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getDialogAgentInstanceByChatbotName(Key req,
      StreamObserver<DialogAgentInstanceList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentInstanceByChatbotName");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentInstanceByChatbotName = {}", req.getName());

    try {
      DialogAgentInstanceList.Builder dailb = daiDao
          .getDialogAgentInstanceByChatbotName(req);

      res.onNext(dailb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getDialogAgentInstanceListToDamFilter(Key req,
      StreamObserver<DialogAgentInstanceList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentInstanceListToDamFilter");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentInstanceListToDamFilter = {}", req.getName());

    try {
      DialogAgentInstanceList.Builder dailb = daiDao
          .getDialogAgentInstanceListToDamFilter(req);

      res.onNext(dailb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getDialogAgentInstanceInfo(Key req,
      StreamObserver<DialogAgentInstance> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentInstanceInfo");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentInstanceInfo = {}", req.getName());

    try {
      DialogAgentInstance.Builder daib = daiDao.getDialogAgentInstance(req);

      res.onNext(daib.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void insertDialogAgentInstanceInfo(DialogAgentInstance req,
      StreamObserver<DialogAgentInstance> res) {
    logger.info("[m2u-svcadm-grpc] insertDialogAgentInstanceInfo");
    logger.trace("[m2u-svcadm-grpc] insertDialogAgentInstanceInfo = {}", req);

    try {
      DialogAgentInstance.Builder daib = req.toBuilder();

      daib.setDaiId(UUID.randomUUID().toString());

      int index = 0;
      for (DialogAgentInstanceExecutionInfo entry : daib.getDaiExecInfoList()) {
        DialogAgentInstanceExecutionInfo.Builder daie = entry.toBuilder();
        daie.setDaiId(daib.getDaiId());

        logger.debug("entry : {}", entry);

        logger.debug("entry.getActive() : {}", entry.getActive());
        if (entry.getActive()) {

          //    daie.setActive(false);
          Key.Builder key = Key.newBuilder();
          key.setName(entry.getDamName());

          DialogAgentManager.Builder damInfo = damDao.getDialogAgentManager(key.build());
          logger.debug("found dam = {}", damInfo);

          for (int i = 0; i < entry.getCnt(); i++) {
            RunDialogAgentInstanceParam.Builder rdaip = RunDialogAgentInstanceParam
                .newBuilder();
            rdaip.setKey(UUID.randomUUID().toString());
            rdaip.setAgent(daib.getDaName());
            rdaip.setVersion(daib.getVersion());
            rdaip.setDescription(daib.getDescription());

            daie.addKey(rdaip.getKey());
          }
        }
        daib.setDaiExecInfo(index, daie);
        index++;
      }
      DialogAgentInstance dai = daib.build();
      logger.debug("calling insertDialogAgentInstance {}", dai);

      try {
        daiDao.insertDialogAgentInstance(dai);
      } catch (Exception e) {
        res.onError(e);
      }

      res.onNext(daib.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void updateDialogAgentInstanceInfo(DialogAgentInstance req,
      StreamObserver<DialogAgentInstance> res) {
    logger.info("[m2u-svcadm-grpc] updateDialogAgentInstanceInfo");
    logger.trace("[m2u-svcadm-grpc] updateDialogAgentInstanceInfo = {}", req);

    try {
      DialogAgentInstance.Builder daib = req.toBuilder();

      int index = 0;
      for (DialogAgentInstanceExecutionInfo entry : daib.getDaiExecInfoList()) {
        DialogAgentInstanceExecutionInfo.Builder daie = entry.toBuilder();
        daie.setDaiId(daib.getDaiId());

        logger.debug("entry : {}", entry);
        logger.debug("entry.getActive() : {}", entry.getActive());
        if (entry.getActive()) {
          for (int i = 0; i < entry.getCnt(); i++) {
            RunDialogAgentInstanceParam.Builder rdaip = RunDialogAgentInstanceParam.newBuilder();
            rdaip.setKey(UUID.randomUUID().toString());
            rdaip.setAgent(daib.getDaName());
            rdaip.setVersion(daib.getVersion());
            rdaip.setDescription(daib.getDescription());

            daie.addKey(rdaip.getKey());
          }
        }
        daib.setDaiExecInfo(index, daie);
        index++;
      }

      daiDao.updateDialogAgentInstance(daib.build());

      res.onNext(daib.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void deleteDialogAgentInstance(KeyList req,
      StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteDialogAgentInstance");
    logger.trace("[m2u-svcadm-grpc] deleteDialogAgentInstance = {}", req);

    try {

      ResultList.Builder rlb = ResultList.newBuilder();

      for (Key key : req.getKeyListList()) {
        logger.debug("key {}", key);
        Result.Builder result = Result.newBuilder();
        DialogAgentInstance.Builder dai = daiDao.getDialogAgentInstance(key);
        logger.debug("found dai {}", dai);
        result.setName(key.getName());

        if (dai == null) {
          result.setResult(false);
        } else {
          // Dai삭제시 Daie를 먼저 삭제해줌 이후 DAM에서 Listen하여 daie stop(순서중요)
          daiDao.delDialogAgentInstance(key);
          result.setResult(true);
        }
        rlb.addResultList(result);
      }

      res.onNext(rlb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getDialogAgentInstanceResourceList(Key req,
      StreamObserver<DialogAgentInstanceResourceList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentInstanceResourceList");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentInstanceResourceList = {}", req.getName());

    try {

      DialogAgentInstanceDAO sc = new DialogAgentInstanceDAO();
      DialogAgentInstanceResourceList resultList = sc
          .getDialogAgentInstanceResourceList(req);

      res.onNext(resultList);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }

  }

  @Override
  public void getDialogAgentInstanceParamList(Key req,
      StreamObserver<RunDialogAgentInstanceParamList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentInstanceParamList");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentInstanceParamList = {}", req.getName());

    try {
      RunDialogAgentInstanceParamList rdaipl = daiDao
          .getDialogAgentInstanceParamList(req);

      res.onNext(rdaipl);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getDialogAgentInstanceResourceToDamFilter(Key req,
      StreamObserver<DialogAgentInstanceResourceList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentInstanceResourceToDamFilter");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentInstanceResourceToDamFilter = {}", req.getName());

    try {

      DialogAgentInstanceDAO sc = new DialogAgentInstanceDAO();

      DialogAgentInstanceResourceList resultList = sc
          .getDialogAgentInstanceResourceListToDamFilter(req);

      res.onNext(resultList);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}
