package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.AuthticationPolicyDAO;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.console.ChatbotOuterClass.AuthticationPolicy;
import maum.m2u.console.ChatbotOuterClass.AuthticationPolicyList;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthticationPolicyAdminImpl extends
    maum.m2u.console.AuthticationPolicyAdminGrpc.AuthticationPolicyAdminImplBase {

  static final Logger logger = LoggerFactory.getLogger(AuthticationPolicyAdminImpl.class);

  static AuthticationPolicyDAO apDao = new AuthticationPolicyDAO();

  @Override
  public void getAuthticationPolicyAllList(com.google.protobuf.Empty req,
                                           StreamObserver<AuthticationPolicyList> res) {
    logger.info("[m2u-svcadm-grpc-] getAuthticationPolicyAllList");

    try {
      AuthticationPolicyList.Builder aplb = apDao.getAuthticationPolicyAllList();

      res.onNext(aplb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }

  }

  @Override
  public void getAuthticationPolicyInfo(Key req,
                                        StreamObserver<AuthticationPolicy> res) {
    logger.info("[m2u-svcadm-grpc] getAuthticationPolicyAllList");
    logger.trace("[m2u-svcadm-grpc] getAuthticationPolicyAllList param = {}", req.getName());

    try {
      AuthticationPolicy.Builder apb = apDao.getAuthticationPolicy(req);

      res.onNext(apb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   */
  @Override
  public void insertAuthticationPolicyInfo(AuthticationPolicy req,
                                           StreamObserver<AuthticationPolicy> res) {
    logger.info("[m2u-svcadm-grpc] insertAuthticationPolicyInfo");
    logger.trace("[m2u-svcadm-grpc] insertAuthticationPolicyInfo param = {}", req);

    try {
      AuthticationPolicy.Builder apb = AuthticationPolicy.newBuilder();

      apDao.insertAuthticationPolicy(req);

      res.onNext(apb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void updateAuthticationPolicyInfo(AuthticationPolicy req,
                                           StreamObserver<AuthticationPolicy> res) {
    logger.info("[m2u-svcadm-grpc] updateAuthticationPolicyInfo");
    logger.trace("[m2u-svcadm-grpc] updateAuthticationPolicyInfo param = {}", req);

    try {
      AuthticationPolicy.Builder apb = AuthticationPolicy.newBuilder();

      apDao.updateAuthticationPolicy(req);

      res.onNext(apb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void deleteAuthticationPolicy(KeyList req, StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteAuthticationPolicy");
    logger.trace("[m2u-svcadm-grpc] deleteAuthticationPolicy param = {}", req);

    try {
      ResultList.Builder resultList = ResultList.newBuilder();

      for (Key key : req.getKeyListList()) {
        Result.Builder result = Result.newBuilder();
        AuthticationPolicy.Builder apb = apDao.getAuthticationPolicy(key);
        result.setName(key.getName());
        if (apb == null) {
          result.setResult(false);
        } else {
          apDao.delAuthticationPolicy(key);
          result.setResult(true);
        }
        resultList.addResultList(result);
      }

      res.onNext(resultList.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}

