package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.common.utils.GrpcUtills;
import ai.maum.m2u.admin.config.PropertyManager;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.rpc.ResultStatus;
import com.google.protobuf.Empty;
import com.hazelcast.core.HazelcastInstance;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import maum.m2u.server.DialogAgentInstancePoolGrpc;
import maum.m2u.server.DialogAgentInstancePoolGrpc.DialogAgentInstancePoolBlockingStub;
import maum.m2u.server.Pool.PoolState;
import maum.rpc.Status;
import maum.rpc.Status.ProcessOfM2u;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaumServiceAdminServer {

  static final Logger logger = LoggerFactory.getLogger(MaumServiceAdminServer.class);

  static {
    ResultStatus.setModuleAndProcess(Status.Module.M2U, ProcessOfM2u.M2U_SVCADM.getNumber());
  }

  private static HazelcastInstance hzcInstance;
  private Server server;
  String absRootPath;

  private void CheckPoolReady() throws RuntimeException {
    logger.info("Is Pool Service Ready?");

    String host;
    int port;

    try {
      String[] poolRemote = PropertyManager.getString("pool.export").split(":");

      if (poolRemote.length != 2) {
        throw new Exception();
      }

      host = poolRemote[0];
      port = Integer.parseInt(poolRemote[1]);

      logger.info("Pool Remote: {}:{}", host, port);
    } catch (Exception e) {
      throw new IllegalArgumentException("Pool remote is invalid.", e);
    }

    int waits[] = {5000, 5000, 20000, 0};
    int wait_seconds = 0;

    DialogAgentInstancePoolBlockingStub poolStub;
    for (int i = 0; i < 4; ++i) {
      try {
        final ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext()
            .build();
        poolStub = DialogAgentInstancePoolGrpc.newBlockingStub(channel);

        PoolState poolState = poolStub.isReady(Empty.newBuilder().build());

        //grpc RuntimeException 예외 처리
        if (channel != null) {
          GrpcUtills.closeChannel(channel);
        }

        if (poolState.getIsReady()) {
          logger.info("Pool Servcie is Ready!");
          return;
        }
      } catch (Exception e) {
        logger.error("{}", e.getCause().getMessage());
      }

      wait_seconds += waits[i];
      logger.info(" Wait more to {}ms.", wait_seconds);
      try {
        Thread.sleep(waits[i]);
      } catch (InterruptedException e) {
        logger.error("{}", e.getMessage());
      }
    }

    throw new IllegalStateException("Pool Service is not Ready");
  }

  private void start() {
    logger.info("maum.ai M2U ADMIN Server starting");

    try {
      this.hzcInstance = HazelcastConnector.getInstance().client;

      CheckPoolReady();

      //NameSpaceInitializer.initializing();

      int port = PropertyManager.getInt("admin.export.port");
      int grpcTimeout = PropertyManager.getInt("admin.grpc.timeout");

      ServerBuilder sb = NettyServerBuilder.forPort(port)
          .maxConnectionIdle(grpcTimeout, TimeUnit.MILLISECONDS)
          .maxConnectionAge(grpcTimeout, TimeUnit.MILLISECONDS);

      sb.addService(new DialogAgentManagerAdminImpl());
      sb.addService(new DialogAgentAdminImpl());
      sb.addService(new SimpleClassifierAdminImpl());
      sb.addService(new ChatbotAdminImpl());
      sb.addService(new AuthticationPolicyAdminImpl());
      sb.addService(new IntentFinderAdminImpl());
      sb.addService(new IntentFinderPolicyAdminImpl());
      sb.addService(new IntentFinderInstanceAdminImpl());
      sb.addService(new DialogAgentInstanceAdminImpl());
      sb.addService(new SupervisorMonitorImpl());
      sb.addService(new CustomScriptAdminImpl());
      sb.addService(new DnnAdminImpl());

      // grpc cli instance
      sb.addService(ProtoReflectionService.newInstance());

      server = sb.build().start();

      logger.info("M2U ADMIN Server started, listening on {}", port);

      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          // Use stderr here since the logger may have been reset by its
          // JVM shutdown hook.
          logger.info("*** shutting down gRPC server since JVM is shutting down");
          MaumServiceAdminServer.this.stop();
          logger.info("*** server shut down");
        }
      });
    } catch (Exception e) {
      logger.error("", e);
      logger.error("********************************");
      logger.error("   {}", e.getMessage() != null ? e.getMessage() : e.getClass().getSimpleName());
      logger.error("   Exit Service Admin.");
      logger.error("********************************");
      System.exit(-1);
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
    if (HazelcastConnector.getInstance() != null) {
      HazelcastConnector.getInstance().release();
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }
  }

  /**
   * Main launches the server from the command line.
   */
  public static void main(String[] args) throws IOException, InterruptedException {
    final MaumServiceAdminServer server = new MaumServiceAdminServer();
    String jarAbsPath = server.getClass().getProtectionDomain().getCodeSource().getLocation()
        .getPath();
    int sIdx = jarAbsPath.lastIndexOf(File.separator + "lib" + File.separator);
    if (sIdx == -1) {
      server.absRootPath = jarAbsPath;
    } else {
      server.absRootPath = jarAbsPath.substring(0, sIdx);
    }

    server.start();
    server.blockUntilShutdown();
  }
}
