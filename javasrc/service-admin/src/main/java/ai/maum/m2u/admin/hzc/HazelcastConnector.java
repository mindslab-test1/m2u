package ai.maum.m2u.admin.hzc;

import ai.maum.m2u.admin.config.PropertyManager;
import ai.maum.m2u.common.portable.PortableClassId;
import ai.maum.m2u.common.portable.ServiceAdminPortableFactory;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class HazelcastConnector {

  static final private Logger logger = LoggerFactory.getLogger(HazelcastConnector.class);
  public static HazelcastInstance client;

  private HazelcastConnector() {
    if (HazelcastConnector.client == null || !HazelcastConnector.client.getLifecycleService().isRunning()) {
      String name = PropertyManager.getString("hazelcast.group.name");
      String password = PropertyManager.getString("hazelcast.group.password");
      String address = PropertyManager.getString("hazelcast.server.ip.1");
      String port = PropertyManager.getString("hazelcast.server.port");
      List<String> addrList = new ArrayList<>();

      if (address != null && !"".equals(address) && port != null && !"".equals(port)) {
        addrList.add(address.concat(":").concat(port));
      }

      this.connect(name, password, addrList);
      logger.info("###this.connect(name, password, addrList) = {}", addrList.toString());
    }
  }

  public static HazelcastConnector getInstance() {
    return Singleton.instance;
  }

  private void connect(String name, String password, List<String> addrList) {
    ClientConfig clientConfig = new ClientConfig();

    clientConfig.setGroupConfig(new GroupConfig(name, password));
    clientConfig.getNetworkConfig().setAddresses(addrList);

    try {
      int timeout = PropertyManager.getInt("hazelcast.invocation.timeout");
      if (timeout > 0) {
        clientConfig.setProperty("hazelcast.client.invocation.timeout.seconds",
            PropertyManager.getString("hazelcast.invocation.timeout"));
      }
    } catch (Exception e) {
      logger.warn("Hazelcast connect {} => ", e.getMessage(), e);
    }

    clientConfig.getSerializationConfig().addPortableFactory(
        PortableClassId.FACTORY_ID, new ServiceAdminPortableFactory());

    HazelcastConnector.client = HazelcastClient.newHazelcastClient(clientConfig);

    logger.info("hzc connection success");
  }

  public void release() {
    logger.info("hzc release success");
    HazelcastConnector.client.shutdown();
  }

  private static class Singleton {

    private static final HazelcastConnector instance = new HazelcastConnector();
  }
}
