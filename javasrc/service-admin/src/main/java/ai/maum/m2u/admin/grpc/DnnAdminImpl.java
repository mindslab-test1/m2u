package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.config.PropertyManager;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import maum.common.LangOuterClass;
import maum.common.LangOuterClass.LangCode;
import maum.m2u.console.Classifier.DnnModel;
import maum.m2u.console.Classifier.DnnModel.Builder;
import maum.m2u.console.Classifier.DnnModelCategories;
import maum.m2u.console.Classifier.DnnModelList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DnnAdminImpl extends maum.m2u.console.DnnAdminGrpc.DnnAdminImplBase {
  static final String P_PATH = "itf.dnn.model.path";
  static final Logger logger = LoggerFactory.getLogger(DnnAdminImpl.class);

  @Override
  public void getDnnModels(com.google.protobuf.Empty req, StreamObserver<DnnModelList> res) {
    logger.info("[m2u-svcadm-grpc] getDnnModels");
    try {
      List<DnnModel> modelList = new ArrayList<DnnModel>();
      Builder bd = DnnModel.newBuilder();
      String envPath = PropertyManager.getString(P_PATH);
      logger.debug("config path : {}", envPath);
      String modelPath = envPath.replace("${MAUM_ROOT}", System.getenv("MAUM_ROOT"));
      File dir = new File(modelPath);
      File[] fileList = dir.listFiles();
      logger.debug("DNN model Path : {}", modelPath);
      int idx = 0;
      for (final File file : fileList) {
        String dnnName = file.getName();
        if (file.isDirectory()) {
          if (file.getName().equals(".") || file.getName().equals("..")) {
            continue;
          }
          if (dnnName.endsWith("-kor")) {
            idx = dnnName.lastIndexOf('-');
            bd.setLang(LangCode.kor); //kor
            bd.setModel(dnnName.substring(0, idx));
            modelList.add(bd.build());
          } else if (dnnName.endsWith("-eng")) {
            idx = dnnName.lastIndexOf('-');
            bd.setLang(LangCode.eng); //eng
            bd.setModel(dnnName.substring(0, idx));
            modelList.add(bd.build());
          }
        }
      }
      DnnModelList models = null;
      models = DnnModelList.newBuilder().addAllModels(modelList).build();
      res.onNext(models);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{}=>", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getDnnCategories(DnnModel req, StreamObserver<DnnModelCategories> res) {
    logger.info("[m2u-svcadm-grpc] getDnnCategories");
    try {
      List<String> categoryList = new ArrayList<String>();
      DnnModelCategories.Builder bd = DnnModelCategories.newBuilder();
      BufferedReader br = null;
      DnnModelCategories categories = null;
      String envPath = PropertyManager.getString(P_PATH);
      String model = req.getModel();
      int lang = req.getLangValue();
      String modelPath = envPath.replace("${MAUM_ROOT}", System.getenv("MAUM_ROOT"));
      modelPath += ('/' + model + '-' + LangOuterClass.LangCode.forNumber(lang));
      File dir = new File(modelPath);
      File[] fileList = dir.listFiles();
      br = new BufferedReader(new InputStreamReader(
          new FileInputStream(modelPath + "/vocab_sa.txt"), "EUC-KR"));
      logger.debug("Dnn Model Path : {}", modelPath);
      String line;
      while ((line = br.readLine()) != null) {
        categoryList.add(line);
      }
      logger.debug("Get {} categories done", req.getModel());
      categories = DnnModelCategories.newBuilder()
          .addAllCategories(categoryList)
          .setModel(model).setLangValue(lang).build();
      res.onNext(categories);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{}=>", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}
