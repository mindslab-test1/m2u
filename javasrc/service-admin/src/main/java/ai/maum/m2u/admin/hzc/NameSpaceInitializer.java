package ai.maum.m2u.admin.hzc;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.common.portable.AuthticationPolicyPortable;
import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.DialogAgentActivationInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import ai.maum.m2u.common.portable.DialogAgentManagerPortable;
import ai.maum.m2u.common.portable.DialogAgentPortable;
import ai.maum.m2u.common.portable.IntentFinderPortable;
import ai.maum.m2u.common.portable.SimpleClassifierPortable;
import com.google.protobuf.Timestamp;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import maum.common.LangOuterClass.LangCode;
import maum.m2u.console.ChatbotOuterClass.Chatbot;
import maum.m2u.console.Classifier.IntentFinder;
import maum.m2u.console.Classifier.IntentFinder.Classifiers;
import maum.m2u.console.Classifier.SimpleClassifier.ListSkill;
import maum.m2u.console.Da.DialogAgentManager;
import maum.m2u.common.Mediatype.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NameSpaceInitializer {

  static final Logger logger = LoggerFactory.getLogger(NameSpaceInitializer.class);

  public static void initializing() {


    logger.info("===== NameSpaceInitializer.initializing");

    HazelcastInstance client = HazelcastConnector.getInstance().client;

    IMap<String, AuthticationPolicyPortable> authPolicyMap = client.getMap(MAP.AUTH_POLICY);
    IMap<String, ChatbotPortable> chatbotMap = client.getMap(MAP.CHATBOT);
//    IMap<String, ChatbotDetailPortable> chatbotDtlMap = client.getMap(MAP.CHATBOT_DETAIL);
    IMap<String, DialogAgentManagerPortable> damMap = client.getMap(MAP.DIALOG_AGENT_MANAGER);
    IMap<String, DialogAgentPortable> daMap = client.getMap(MAP.DIALOG_AGENT);
    IMap<String, DialogAgentActivationInfoPortable> daActMap = client.getMap(MAP.DIALOG_AGENT_ACTIVATION);
    IMap<String, DialogAgentInstancePortable> daInstMap = client.getMap(MAP.DIALOG_AGENT_INSTANCE);
//    IMap<String, DialogAgentInstanceResourcePortable> daInstRscMap = client.getMap(MAP.DIALOG_AGENT_INSTANCE_RESOURCE);
//    IMap<String, DialogAgentInstanceExecutionInfoPortable> daInstExecMap = client.getMap(MAP.DIALOG_AGENT_INSTANCE_EXECUTION);
    IMap<String, IntentFinderPortable> itntFndrMap = client.getMap(MAP.INTENT_FINDER);
//    IMap<String, SkillPortable> skillMap = client.getMap(MAP.SKILL);
    IMap<String, SimpleClassifierPortable> scMap = client.getMap(MAP.SIMPLE_CLASSIFIER);

    int authPolicyCnt = authPolicyMap.size();
    int chatbotCnt = chatbotMap.size();
//    int chatbotDtlCnt = chatbotDtlMap.size();
    int damCnt = damMap.size();
    int daCnt = daMap.size();
    int daActCnt = daActMap.size();
    int daInstCnt = daInstMap.size();
//    int daInstRscCnt  = daInstRscMap.size();
//    int daInstExecCnt = daInstExecMap.size();
    int itntFndrCnt = itntFndrMap.size();
//    int skillCnt      = skillMap.size();
    int scCnt = scMap.size();

    logger.debug("===== Check Map Size ===============");
    logger.debug("  authPolicyMap : {}", authPolicyCnt);
    logger.debug("  chatbotMap    : {}", chatbotCnt);
//    logger.debug("  chatbotDtlMap : {}", chatbotDtlCnt);
    logger.debug("  damMap        : {}", damCnt);
    logger.debug("  daMap         : {}", daCnt);
    logger.debug("  daActMap      : {}", daActCnt);
    logger.debug("  daInstMap     : {}", daInstCnt);
//    logger.debug("  daInstRscMap  : {}", daInstRscCnt);
//    logger.debug("  daInstExecMap : {}", daInstExecCnt);
    logger.debug("  itntFndrMap   : {}", itntFndrCnt);
//    logger.debug("  skillMap      : {}", skillCnt);
    logger.debug("  scMap         : {}", scCnt);
    logger.debug("====================================");

    String chatBotName = "echoBot";

    if (authPolicyCnt <= 0) {

    }

    if (chatbotCnt <= 0) {

      ChatbotPortable portable = new ChatbotPortable();
      String key = chatBotName;
      /*
      portable.setName(key);
      portable.setTitle(chatBotName+"-title");
      portable.setDescription("This is the chatbot that is initially set up for testing purposes.");
      portable.setInput(1);
      portable.setOutput(1);
      portable.setActive(true);
      portable.setAuthProvider("");
      portable.setIntentFinder("");
      */
      Chatbot.Builder protobufObj = portable.getProtobufObj().newBuilder();
      protobufObj.setName(key);
      protobufObj.setTitle(chatBotName + "-title");
      protobufObj.setDescription("This is the chatbot that is initially set up for testing purposes.");
      protobufObj.setInput(MediaType.TEXT);
      protobufObj.setOutput(MediaType.TEXT);
      protobufObj.setActive(false);
      protobufObj.setAuthProvider("");
      protobufObj.setIntentFinderPolicy("");

      maum.m2u.console.ChatbotOuterClass.ChatbotDetail.Builder subProtobufObj = protobufObj.getDetail().newBuilder();
      subProtobufObj.addGreetingMsg("hi~hi~");
      subProtobufObj.addGreetingMsg("hello~");
      subProtobufObj.setHowToMsg("Learn by yourself and use it.");
      subProtobufObj.setAdvertisingMsg("ad");
      subProtobufObj.setBgColor("red");
      subProtobufObj.setBgImage("images/test.png");
      protobufObj.setDetail(subProtobufObj.build());

      portable.setProtobufObj(protobufObj.build());

      chatbotMap.set(key, portable);
    }

    /*
    if(chatbotDtlCnt <= 0){
      ChatbotDetailPortable portable = new ChatbotDetailPortable();
      String key = chatBotName;

//      portable.setName(key);
//      portable.setGreetingMsg(new String[]{"hi~hi~","hello~"});
//      portable.setHowToMsg("Learn by yourself and use it.");
//      portable.setAdvertisingMsg("ad");
//      portable.setCategories(new String[]{});
//      portable.setTags(new String[]{});
//      portable.setBgColor("red");
//      portable.setBgImage("images/test.png");

      chatbotDtlMap.set(key, portable);
    }
    */

    if (damCnt <= 0) {
      DialogAgentManagerPortable portable = new DialogAgentManagerPortable();
      DialogAgentManager.Builder protobufObj = portable.getProtobufObj().newBuilder();
      String key = "localDam";
      protobufObj.setName(key);
      protobufObj.setDescription("");
      protobufObj.setIp("127.0.0.1");
      protobufObj.setPort(9907);
      protobufObj.setActive(false);
      protobufObj.setDefault(false);

      long seconds = System.currentTimeMillis() / 1000;
      Timestamp now = Timestamp.newBuilder().setSeconds(seconds).build();

      protobufObj.setCreateAt(now);
      protobufObj.setModifyAt(now);
      portable.setProtobufObj(protobufObj.build());

      damMap.set(key, portable);
    }

    /*if(daCnt <= 0){
      DialogAgentPortable portable = new DialogAgentPortable();
      Da.DialogAgent.Builder protobufObj = portable.getProtobufObj().newBuilder();
      String key = "echoDa";
      protobufObj.setName(key);
      protobufObj.setDescription("");
      protobufObj.setDaExecutable("echo.py");
      protobufObj.setVersion("0.1");
      portable.setProtobufObj(protobufObj.build());
      daMap.set(key, portable);
    }*/

    /*if(daActCnt <= 0){
      DialogAgentActivationInfoPortable portable = new DialogAgentActivationInfoPortable();
      Da.DialogAgentActivationInfo.Builder protobufObj = portable.getProtobufObj().newBuilder();
      String key = "localDam";
      protobufObj.setDamName(key);
      protobufObj.setDaName("echoDa");
      protobufObj.setActive(false);
      portable.setProtobufObj(protobufObj.build());
      daActMap.set(key, portable);
    }*/

    if (scCnt <= 0) {
      SimpleClassifierPortable portable = new SimpleClassifierPortable();
      maum.m2u.console.Classifier.SimpleClassifier.Builder protobufObj = portable.getProtobufObj().newBuilder();
      String key = "echoSc";
      protobufObj.setName(key);
      protobufObj.setLang(LangCode.kor);
      protobufObj.setDescription("");

      long seconds = System.currentTimeMillis() / 1000;
      Timestamp now = Timestamp.newBuilder().setSeconds(seconds).build();

      protobufObj.setCreateAt(now);
      protobufObj.setModifyAt(now);

      ListSkill.Builder lr = ListSkill.newBuilder();
      lr.setName("echo_test");
      lr.addRegex(".");
      protobufObj.addSkillList(lr);

     /* lr = ListSkill.newBuilder();
      lr.setName("restaurant");
      lr.addRegex("배고파");
      lr.addRegex("맛있는");
      protobufObj.addSkillList(lr);*/

      portable.setProtobufObj(protobufObj.build());

      scMap.set(key, portable);
    }

    /*
    if(skillCnt <= 0){
      SkillPortable portable = new SkillPortable();
      String key = "";
      key = "weather";
      portable.setName(key);
      portable.setScName("echo-test-sc");
      portable.setRegex(new String[]{"날씨","바깥"});
      skillMap.put(key, portable);

      portable = new SkillPortable();
      key = "restaurant";
      portable.setName(key);
      portable.setScName("echo-test-sc");
      portable.setRegex(new String[]{"배고파","맛있는"});
      skillMap.put(key, portable);
    }
    */

    if (itntFndrCnt <= 0) {
      IntentFinderPortable portable = new IntentFinderPortable();
      IntentFinder.Builder protobufObj = portable.getProtobufObj().newBuilder();
      String key = "echoIf";
      protobufObj.setName(key);
      protobufObj.setDescription("");
      Classifiers.Builder langvallist = Classifiers.newBuilder();
      langvallist.setLang(LangCode.kor);
      langvallist.setName("echoSc");
      langvallist.setTypeValue(1);
      langvallist.addSkills("echo_test");
      protobufObj.addClassifiers(langvallist);

      langvallist = Classifiers.newBuilder();
      langvallist.setLang(LangCode.kor);
      //   langvallist.addValue("weather");
      //   protobufObj.addSkill(langvallist);

      portable.setProtobufObj(protobufObj.build());
      itntFndrMap.put(key, portable);
    }

    /*if(daInstCnt <= 0){
      DialogAgentInstancePortable portable = new DialogAgentInstancePortable();
      DialogAgentInstance.Builder protobufObj = portable.getProtobufObj().newBuilder();
      String key = "DE3S-FGES-EGSW-3SDE";
      protobufObj.setDaiId(key);
      protobufObj.setChatbotName("echoBot");
      protobufObj.setScName("echoSc");
      protobufObj.setDaName("echoDa");
      protobufObj.setSkillName("restaurant");
      protobufObj.setName("echo-test-restaurant");
      protobufObj.setLang(LangCode.kor);
      protobufObj.setDescription("");
      protobufObj.putParams("runparam_sample_key","runparam_sample_value"); //RuntimeParameter

      // Add - DialogAgentInstanceExecutionInfo
      maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo.Builder daiei = maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo.newBuilder();
//      maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo.Builder daiei = protobufObj.getDaiExecInfo(0).toBuilder();
      daiei.setDamName("localDam");
      daiei.setActive(false);
      daiei.setCnt(2);

      protobufObj.addDaiExecInfo(daiei.build());

      // Add - UserAttribute
      maum.m2u.facade.Userattr.UserAttributeList.Builder userattrlist = maum.m2u.facade.Userattr.UserAttributeList.newBuilder();
      maum.m2u.facade.Userattr.UserAttribute.Builder userattr = maum.m2u.facade.Userattr.UserAttribute.newBuilder();
      userattr.setName("attr_01_name");
      userattr.setType(DataType.DATA_TYPE_STRING);
      userattr.setDesc("attr_01_desc");
      userattr.setTitle("attr_01_title");
      userattrlist.addAttrs(userattr);
      protobufObj.setUserAttrs(userattrlist);

      portable.setProtobufObj(protobufObj.build());
      daInstMap.set(key, portable);
    }*/

    /*
    if(daInstExecCnt <= 0){
      DialogAgentInstanceExecutionInfoPortable portable = new DialogAgentInstanceExecutionInfoPortable();
      String key = "DE3S-FGES-EGSW-3SDE";
      portable.setDaiId(key);
      portable.setDamName("dam-01");
      portable.setActive(true);
      portable.setCnt(2);
      daInstExecMap.set(key, portable);
    }
    */

    /*if(daInstRscCnt <= 0){

    }*/

    authPolicyCnt = authPolicyMap.size();
    chatbotCnt = chatbotMap.size();
//    chatbotDtlCnt = chatbotDtlMap.size();
    damCnt = damMap.size();
    daCnt = daMap.size();
    daActCnt = daActMap.size();
    daInstCnt = daInstMap.size();
//    daInstRscCnt  = daInstRscMap.size();
//    daInstExecCnt = daInstExecMap.size();
    itntFndrCnt = itntFndrMap.size();
//    skillCnt      = skillMap.size();
    scCnt = scMap.size();
    logger.debug("  authPolicyMap : " + authPolicyCnt);
    logger.debug("  chatbotMap    : " + chatbotCnt);
//    logger.debug("  chatbotDtlMap : " + chatbotDtlCnt);
    logger.debug("  damMap        : " + damCnt);
    logger.debug("  daMap         : " + daCnt);
    logger.debug("  daActMap      : " + daActCnt);
    logger.debug("  daInstMap     : " + daInstCnt);
//    logger.debug("  daInstRscMap  : " + daInstRscCnt);
//    logger.debug("  daInstExecMap : " + daInstExecCnt);
    logger.debug("  itntFndrMap   : " + itntFndrCnt);
//    logger.debug("  skillMap      : " + skillCnt);
    logger.debug("  scMap         : " + scCnt);
    logger.debug("====================================");

  }
}
