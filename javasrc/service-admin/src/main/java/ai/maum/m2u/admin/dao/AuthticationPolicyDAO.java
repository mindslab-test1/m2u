package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.AuthticationPolicyPortable;
import ai.maum.m2u.common.portable.ChatbotPortable;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import java.util.Collection;
import java.util.Map.Entry;
import maum.m2u.console.ChatbotOuterClass.AuthticationPolicy;
import maum.m2u.console.ChatbotOuterClass.AuthticationPolicyList;
import maum.m2u.console.Da.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthticationPolicyDAO {

  static final Logger logger = LoggerFactory.getLogger(AuthticationPolicyDAO.class);

  private IMap<String, AuthticationPolicyPortable> AuthticationPolicyMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.AUTH_POLICY);

  private IMap<String, ChatbotPortable> ChatbotMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.CHATBOT);

  public AuthticationPolicyList.Builder getAuthticationPolicyAllList() {
    logger.info("[m2u-svcadm-dao] getAuthticationPolicyAllList");

    AuthticationPolicyList.Builder apl = AuthticationPolicyList.newBuilder();

    for (Entry<String, AuthticationPolicyPortable> entry : this.AuthticationPolicyMap.entrySet()) {

      AuthticationPolicy.Builder api = entry.getValue().getProtobufObj().toBuilder();

      Collection<ChatbotPortable> result = ChatbotMap
          .values(new SqlPredicate("auth_provider = '" + entry.getValue().getProtobufObj().getName() + "'"));
      for (ChatbotPortable params2 : result) {
        api.addMapped(params2.getProtobufObj().getName());
      }

      apl.addAuthPolList(api);

    }

    return apl;
  }

  public AuthticationPolicy.Builder getAuthticationPolicy(Key req) {
    logger.info("[m2u-svcadm-dao] getAuthticationPolicy");
    logger.trace("[m2u-svcadm-dao] getAuthticationPolicy = {}", req.getName());

    AuthticationPolicyPortable app = this.AuthticationPolicyMap.get(req.getName());
    AuthticationPolicy.Builder ap = AuthticationPolicy.newBuilder();
    if (app != null) {
      ap = app.getProtobufObj().toBuilder();
    }
    return ap;
  }

  public void insertAuthticationPolicy(AuthticationPolicy req) {
    logger.info("[m2u-svcadm-dao] insertAuthticationPolicy");
    logger.trace("[m2u-svcadm-dao] insertAuthticationPolicy = {}", req);

    AuthticationPolicyPortable app = new AuthticationPolicyPortable();

    app.setProtobufObj(req);

    this.AuthticationPolicyMap.set(app.getProtobufObj().getName(), app);
    Common.invokeBackupSignal(MAP.AUTH_POLICY);
  }

  public void updateAuthticationPolicy(AuthticationPolicy req) {
    logger.info("[m2u-svcadm-dao] updateAuthticationPolicy");
    logger.trace("[m2u-svcadm-dao] updateAuthticationPolicy = {}", req);

    AuthticationPolicyPortable app = this.AuthticationPolicyMap.get(req.getName());

    app.setProtobufObj(req);

    this.AuthticationPolicyMap.set(app.getProtobufObj().getName(), app);
    Common.invokeBackupSignal(MAP.AUTH_POLICY);
  }

  public void delAuthticationPolicy(Key key) {
    logger.info("[m2u-svcadm-dao] delAuthticationPolicy");
    logger.trace("[m2u-svcadm-dao] delAuthticationPolicy = {}", key.getName());

    this.AuthticationPolicyMap.delete(key.getName());
    Common.invokeBackupSignal(MAP.AUTH_POLICY);
  }
}
