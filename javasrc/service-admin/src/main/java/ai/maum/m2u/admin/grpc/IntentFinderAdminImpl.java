package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.IntentFinderDAO;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.console.Classifier.IntentFinder;
import maum.m2u.console.Classifier.IntentFinderList;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntentFinderAdminImpl extends
    maum.m2u.console.IntentFinderAdminGrpc.IntentFinderAdminImplBase {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderAdminImpl.class);

  static IntentFinderDAO ifDao = new IntentFinderDAO();

  @Override
  public void getIntentFinderAllList(com.google.protobuf.Empty req,
                                     StreamObserver<IntentFinderList> res) {
    logger.info("[m2u-svcadm-grpc] getIntentFinderAllList");

    try {
      IntentFinderList.Builder iflb = ifDao.getIntentFinderAllList();

      res.onNext(iflb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }

  }

  @Override
  public void getIntentFinderInfo(Key req,
                                  StreamObserver<IntentFinder> res) {
    logger.info("[m2u-svcadm-grpc] getIntentFinderInfo");
    logger.trace("[m2u-svcadm-grpc] getIntentFinderInfo = {}", req.getName());

    try {
      IntentFinder.Builder ifb = ifDao.getIntentFinder(req);

      res.onNext(ifb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void insertIntentFinderInfo(IntentFinder req,
                                     StreamObserver<IntentFinder> res) {
    logger.info("[m2u-svcadm-grpc] insertIntentFinderInfo");
    logger.trace("[m2u-svcadm-grpc] insertIntentFinderInfo = {}", req);

    try {
      IntentFinder.Builder ifb = IntentFinder.newBuilder();

      ifDao.insertIntentFinder(req);

      res.onNext(req);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void updateIntentFinderInfo(IntentFinder req,
                                     StreamObserver<IntentFinder> res) {
    logger.info("[m2u-svcadm-grpc] updateIntentFinderInfo");
    logger.trace("[m2u-svcadm-grpc] updateIntentFinderInfo = {}", req);

    try {
      ifDao.updateIntentFinder(req);

      res.onNext(req);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void deleteIntentFinder(KeyList req, StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteIntentFinder");
    logger.trace("[m2u-svcadm-grpc] deleteIntentFinder = {}", req);

    try {

      ResultList.Builder rlb = ResultList.newBuilder();

      for (Key key : req.getKeyListList()) {
        Result.Builder result = Result.newBuilder();
        IntentFinder.Builder da = ifDao.getIntentFinder(key);
        result.setName(key.getName());
        if (da == null) {
          result.setResult(false);
        } else {
          ifDao.delIntentFinder(key);
          result.setResult(true);
        }
        rlb.addResultList(result);
      }

      res.onNext(rlb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}

