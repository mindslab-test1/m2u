package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.PortableClassId;

public class Common {

  public static void invokeBackupSignal(String target) {
    HazelcastConnector.getInstance().client
        .getTopic(PortableClassId.NsBackupSignal)
        .publish(target);
  }
}
