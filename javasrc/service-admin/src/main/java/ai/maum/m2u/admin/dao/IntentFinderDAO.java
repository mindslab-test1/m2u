package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.IntentFinderPortable;
import ai.maum.m2u.common.portable.SimpleClassifierPortable;
import ai.maum.m2u.common.portable.SkillPortable;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import java.util.Collection;
import java.util.Map.Entry;
import maum.m2u.console.Classifier.IntentFinder;
import maum.m2u.console.Classifier.IntentFinderList;
import maum.m2u.console.Da.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntentFinderDAO {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderDAO.class);

  private IMap<String, IntentFinderPortable> IntentFinderMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.INTENT_FINDER);
  private IMap<String, SkillPortable> SkillMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.SKILL);
  private IMap<String, ChatbotPortable> ChatbotMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.CHATBOT);
  private IMap<String, SimpleClassifierPortable> SimpleClassifierMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.SIMPLE_CLASSIFIER);

  public void insertIntentFinder(IntentFinder req) {
    logger.info("[m2u-svcadm-dao] insertIntentFinder");
    logger.trace("[m2u-svcadm-dao] insertIntentFinder = {}", req);

    IntentFinderPortable ifp = new IntentFinderPortable();

    ifp.setProtobufObj(req);


    this.IntentFinderMap.set(ifp.getProtobufObj().getName(), ifp);
    this.IntentFinderMap.flush();
  }

  public void updateIntentFinder(IntentFinder req) {
    logger.info("[m2u-svcadm-dao] IntentFinder");
    logger.trace("[m2u-svcadm-dao] IntentFinder = {}", req);

    IntentFinderPortable ifp = new IntentFinderPortable();

    ifp.setProtobufObj(req);

    this.IntentFinderMap.set(ifp.getProtobufObj().getName(), ifp);
    this.IntentFinderMap.flush();
  }

  public void delIntentFinder(Key req) {
    logger.info("[m2u-svcadm-dao] delIntentFinder");
    logger.trace("[m2u-svcadm-dao] delIntentFinder = {}", req.getName());
    
    this.IntentFinderMap.delete(req.getName());
  }

  public IntentFinder.Builder getIntentFinder(Key req) {
    logger.info("[m2u-svcadm-dao] getIntentFinder");
    logger.trace("[m2u-svcadm-dao] getIntentFinder = {}", req.getName());

    IntentFinderPortable ifp = this.IntentFinderMap.get(req.getName());

    if (ifp == null) return null;

    return ifp.getProtobufObj().toBuilder();
  }

  public IntentFinderList.Builder getIntentFinderAllList() {
    logger.info("[m2u-svcadm-dao] getIntentFinderAllList");

    IntentFinderList.Builder iflb = IntentFinderList.newBuilder();

    for (Entry<String, IntentFinderPortable> ifp : this.IntentFinderMap.entrySet()) {

      if (ifp != null) {

        IntentFinder.Builder ifb = ifp.getValue().getProtobufObj().toBuilder();

        Collection<ChatbotPortable> result = ChatbotMap
            .values(new SqlPredicate("intentFinder = '" + ifp.getValue().getProtobufObj().getName() + "'"));

        for (ChatbotPortable params2 : result) {
          ifb.addMapped(params2.getProtobufObj().getName());
        }

        logger.debug("## ifb.toString() : {}", ifb.toString());


        iflb.addItfList(ifb);
      }
    }
    return iflb;
  }
}