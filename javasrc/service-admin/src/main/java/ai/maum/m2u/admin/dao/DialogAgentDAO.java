package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.DialogAgentActivationInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import ai.maum.m2u.common.portable.DialogAgentManagerPortable;
import ai.maum.m2u.common.portable.DialogAgentPortable;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;
import maum.m2u.console.Da.ChatbotDai;
import maum.m2u.console.Da.DialogAgent;
import maum.m2u.console.Da.DialogAgent.Builder;
import maum.m2u.console.Da.DialogAgentActivationInfo;
import maum.m2u.console.Da.DialogAgentInfo;
import maum.m2u.console.Da.DialogAgentList;
import maum.m2u.console.Da.DialogAgentWithDialogAgentInstance;
import maum.m2u.console.Da.DialogAgentWithDialogAgentInstanceList;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DialogAgentDAO {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentDAO.class);

  private IMap<String, DialogAgentPortable> DialogAgentMap = HazelcastConnector
      .getInstance().client.getMap(MAP.DIALOG_AGENT);
  private IMap<String, DialogAgentActivationInfoPortable> DialogAgentActivationInfoMap = HazelcastConnector
      .getInstance().client.getMap(MAP.DIALOG_AGENT_ACTIVATION);
  private IMap<String, ChatbotPortable> ChatbotMap = HazelcastConnector
      .getInstance().client.getMap(MAP.CHATBOT);
  private IMap<String, DialogAgentInstancePortable> DialogAgentInstanceMap = HazelcastConnector
      .getInstance().client.getMap(MAP.DIALOG_AGENT_INSTANCE);
  private IMap<String, DialogAgentManagerPortable> DialogAgentManagerMap = HazelcastConnector
      .getInstance().client.getMap(MAP.DIALOG_AGENT_MANAGER);

  public DialogAgentWithDialogAgentInstanceList.Builder getDialogAgentWithDialogAgentInstanceList(
      Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentWithDialogAgentInstanceList");
    logger.trace("[m2u-svcadm-dao] getDialogAgentWithDialogAgentInstanceList = {}", req.getName());

    DialogAgentWithDialogAgentInstanceList.Builder dawdail = DialogAgentWithDialogAgentInstanceList
        .newBuilder();

    Collection<DialogAgentActivationInfoPortable> result =
        this.DialogAgentActivationInfoMap.values(
            new SqlPredicate("dam_name = '" + req.getName() + "'"));
    for (DialogAgentActivationInfoPortable params : result) {
      DialogAgentWithDialogAgentInstance.Builder dawdai = DialogAgentWithDialogAgentInstance
          .newBuilder();

      DialogAgentPortable dap = DialogAgentMap.get(params.getProtobufObj().getDaName());

      Builder da = dap.getProtobufObj().toBuilder();

      dawdai.setDaInfo(da);
      dawdai.setDaaInfo(params.getProtobufObj().toBuilder());

      Collection<DialogAgentInstancePortable> daipc =
          this.DialogAgentInstanceMap.values(
              new SqlPredicate("da_name = '" + params.getProtobufObj().getDaName() + "'"));
      for (DialogAgentInstancePortable daip : daipc) {
        ChatbotDai.Builder cd = ChatbotDai.newBuilder();
        cd.setChatbotName(daip.getProtobufObj().getChatbotName());
        cd.setDaiName(daip.getProtobufObj().getName());
        dawdai.addChatbotDaiList(cd);
      }

      dawdail.addDaWithDaiInfo(dawdai);
    }
    return dawdail;
  }

  public void insertDialogAgent(DialogAgent req) {
    logger.info("[m2u-svcadm-dao] insertDialogAgent");
    logger.trace("[m2u-svcadm-dao] insertDialogAgent = {}", req);

    DialogAgentPortable da = new DialogAgentPortable();

    da.setProtobufObj(req);

    this.DialogAgentMap.set(da.getProtobufObj().getName(), da);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT);
  }

  public void insertDialogAgentActivationInfo(DialogAgentActivationInfo req) {
    logger.info("[m2u-svcadm-dao] insertDialogAgentActivationInfo");
    logger.trace("[m2u-svcadm-dao] insertDialogAgentActivationInfo = {}", req);

    String query = "da_name='" + req.getDaName() + "' and dam_name='" + req.getDamName() + "'";
    Set<String> gabages = this.DialogAgentActivationInfoMap.keySet(new SqlPredicate(query));
    for (String gabage : gabages) {
      logger.debug("[m2u-svcadm-dao] delete gabage daai [{}].", gabage);
      this.DialogAgentActivationInfoMap.delete(gabage);
    }

    DialogAgentActivationInfoPortable dambda = new DialogAgentActivationInfoPortable();
    String key = req.getDaName() + "_" + req.getDamName();

    dambda.setProtobufObj(req);
    this.DialogAgentActivationInfoMap.set(key, dambda);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_ACTIVATION);
  }

  public void updateDialogAgent(DialogAgent req) {
    DialogAgentPortable da = new DialogAgentPortable();

    da.setProtobufObj(req);

    this.DialogAgentMap.set(da.getProtobufObj().getName(), da);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT);
  }

  public void updateDialogAgentActivationInfo(DialogAgentActivationInfo req) {
    logger.info("[m2u-svcadm-dao] DialogAgentActivationInfo");
    logger.trace("[m2u-svcadm-dao] DialogAgentActivationInfo = {}", req);

    String key = req.getDaName() + "_" + req.getDamName();

    String query = "da_name='" + req.getDaName() + "' and dam_name='" + req.getDamName() + "'";
    Set<String> gabages = this.DialogAgentActivationInfoMap.keySet(new SqlPredicate(query));
    for (String gabage : gabages) {
      if (!gabage.equals(key)) {
        logger.debug("[m2u-svcadm-dao] delete gabage daai [{}].", gabage);
        this.DialogAgentActivationInfoMap.delete(gabage);
      }
    }
    
    DialogAgentActivationInfoPortable dambda = new DialogAgentActivationInfoPortable();

    dambda.setProtobufObj(req);
    this.DialogAgentActivationInfoMap.set(key, dambda);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_ACTIVATION);
  }

  public Builder getDialogAgent(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgent");
    logger.trace("[m2u-svcadm-dao] getDialogAgent = {}", req);

    DialogAgentPortable params = this.DialogAgentMap.get(req.getName());
    if (params == null) {
      return null;
    }
    Builder da = params.getProtobufObj().toBuilder();

    return da;
  }

  public DialogAgentList.Builder getDialogAgentList() {
    logger.info("[m2u-svcadm-dao] getDialogAgentList");

    DialogAgentList.Builder dal = DialogAgentList.newBuilder();
    for (Entry<String, DialogAgentPortable> entry : this.DialogAgentMap.entrySet()) {
      DialogAgentInfo.Builder dai = DialogAgentInfo.newBuilder();
      DialogAgent da = entry.getValue().getProtobufObj();
      Collection<DialogAgentActivationInfoPortable> result = this.DialogAgentActivationInfoMap
          .values(new SqlPredicate("da_name='" + da.getName() + "'"));

      for (DialogAgentActivationInfoPortable params : result) {
        DialogAgentActivationInfo daai = params.getProtobufObj();
        if (daai.getActive()) {
          dai.addDaaiList(daai);
        }
      }

      dai.setDaInfo(da);
      dal.addDaList(dai.build());
    }

    logger.trace("{}", dal);

    return dal;
  }

  public DialogAgentWithDialogAgentInstance.Builder getDialogAgentInfo(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentInfo");
    logger.trace("[m2u-svcadm-dao] getDialogAgentInfo = {}", req.getName());

    DialogAgentWithDialogAgentInstance.Builder dawdai = DialogAgentWithDialogAgentInstance
        .newBuilder();
    DialogAgentPortable dap = this.DialogAgentMap.get(req.getName());
    if (dap != null) {
      Builder da = dap.getProtobufObj().toBuilder();

      Collection<DialogAgentInstancePortable> daipc = this.DialogAgentInstanceMap
          .values(new SqlPredicate("da_name = '" + req.getName() + "'"));

      for (DialogAgentInstancePortable daip : daipc) {
        ChatbotPortable cp = this.ChatbotMap.get(daip.getProtobufObj().getChatbotName());

        ChatbotDai.Builder sgd = ChatbotDai.newBuilder();

        sgd.setChatbotName(cp.getProtobufObj().getName());
        sgd.setDaiName(daip.getProtobufObj().getName());
        sgd.setState(daip.getProtobufObj().getState());
        sgd.setDaProdSpec(daip.getProtobufObj().getDaProdSpec());
        logger.debug("chatbot dai: {} ", sgd);
        dawdai.addChatbotDaiList(sgd);

      }

      dawdai.setDaInfo(da);
    }
    logger.debug("dialog agent with agent instance : {}", dawdai);
    return dawdai;
  }

  /**
   * DA + DAAINFO 삭제
   *
   * @param req [KeyList = da_name]
   */
  public ResultList.Builder delDialogAgentList(KeyList req) {
    logger.info("[m2u-svcadm-dao] delDialogAgentList");
    logger.trace("[m2u-svcadm-dao] delDialogAgentList = {}", req);

    ResultList.Builder resultList = ResultList.newBuilder();
    for (Key key : req.getKeyListList()) {
      Result.Builder result = Result.newBuilder();
      Builder da = getDialogAgent(key);
      result.setName(key.getName());
      if (da == null) {
        result.setResult(false);
      } else {
        DialogAgentPortable params = this.DialogAgentMap.get(key.getName());

        if (params != null) {
          String query = "da_name='" + key.getName() + "'";
          logger.debug("get da activation info {}", query);
          Collection<DialogAgentActivationInfoPortable> daaipc =
              this.DialogAgentActivationInfoMap.values(new SqlPredicate(query));

          for (DialogAgentActivationInfoPortable daaip : daaipc) {
            logger.debug("da activation info {}", daaip.getProtobufObj());
            String daaipKey = daaip.getProtobufObj().getDaName() +
                "_" + daaip.getProtobufObj().getDamName();

            logger.debug("delete from da activation info {}", daaipKey);
            this.DialogAgentActivationInfoMap.delete(daaipKey);
          }

          this.DialogAgentMap.delete(key.getName());
          result.setResult(true);
        } else {
          result.setResult(false);
        }
      }
      resultList.addResultList(result);
    }
    Common.invokeBackupSignal(MAP.DIALOG_AGENT);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_ACTIVATION);

    return resultList;
  }

  public DialogAgentActivationInfoPortable updateActiveDialogAgent(String type, Key req,
      boolean active) {
    logger.info("[m2u-svcadm-dao] updateActiveDialogAgent");
    logger.trace("[m2u-svcadm-dao] type {}, req {}, active {}", type, req.getName(), active);

    String query = type + req.getName() + "'";
    logger.debug("get da activation info {}", query);
    Collection<DialogAgentActivationInfoPortable> daActInfos =
        DialogAgentActivationInfoMap.values(new SqlPredicate(query));
    logger.trace("get da activation info result {}", daActInfos);

    for (DialogAgentActivationInfoPortable daaiPort : daActInfos) {
      DialogAgentActivationInfo.Builder dawdai = daaiPort.getProtobufObj().toBuilder();
      logger.debug("da activation info {}", dawdai);

      String key = dawdai.getDamName() + "_" + dawdai.getDaName();
      dawdai.setActive(active);
      daaiPort.setProtobufObj(dawdai.build());
      logger.debug("da activation info map, put {} {}", key, daaiPort);

      DialogAgentActivationInfoPortable daaip = DialogAgentActivationInfoMap.put(key, daaiPort);
      Common.invokeBackupSignal(MAP.DIALOG_AGENT_ACTIVATION);

      return daaip;
    }
    return null;
  }

  public DialogAgentList.Builder getDialogAgentListToDamFilter(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentListToDamFilter");
    logger.trace("[m2u-svcadm-dao] getDialogAgentListToDamFilter = {}", req.getName());

    DialogAgentList.Builder dalb = DialogAgentList.newBuilder();
    String query = "dam_name = '" + req.getName() + "'";
    logger.debug("get da activation info {}", query);
    Collection<DialogAgentActivationInfoPortable> daieip =
        this.DialogAgentActivationInfoMap.values(new SqlPredicate(query));
    logger.debug("da actication info result {}", daieip);
    for (DialogAgentActivationInfoPortable daaiPort : daieip) {
      logger.debug("da actication info {}", daaiPort.getProtobufObj());
      DialogAgentInfo.Builder dai = DialogAgentInfo.newBuilder();
      Builder da = this.DialogAgentMap.get(daaiPort.getProtobufObj().getDaName())
          .getProtobufObj().toBuilder();
      logger.debug("dialog agent", da);
      dai.setDaInfo(da);
      dalb.addDaList(dai);
    }

    return dalb;
  }
}
