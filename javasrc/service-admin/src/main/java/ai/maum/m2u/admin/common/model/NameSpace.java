package ai.maum.m2u.admin.common.model;

public class NameSpace {

  public static final class MAP {
    public static final String AUTH_POLICY = "auth-policy";
    public static final String CHATBOT = "chatbot";
    public static final String DIALOG_AGENT_MANAGER = "dialog-agent-manager";
    public static final String DIALOG_AGENT = "dialog-agent";
    public static final String DIALOG_AGENT_ACTIVATION = "dialog-agent-activation";
    public static final String DIALOG_AGENT_INSTANCE = "dialog-agent-instance";
    public static final String DIALOG_AGENT_INSTANCE_RESOURCE = "dialog-agent-instance-resource";
    public static final String DIALOG_AGENT_INSTANCE_RESOURCE_SKILL = "dialog-agent-instance-resource-skill";
    public static final String DIALOG_AGENT_INSTANCE_EXECUTION = "dialog-agent-instance-exec";
    public static final String INTENT_FINDER = "intent-finder";
    public static final String INTENT_FINDER_POLICY = "intent-finder-policy";
    public static final String INTENT_FINDER_INSTANCE = "intent-finder-instance";
    public static final String SKILL = "skill";
    public static final String SIMPLE_CLASSIFIER = "simple-classifier";
  }
}
