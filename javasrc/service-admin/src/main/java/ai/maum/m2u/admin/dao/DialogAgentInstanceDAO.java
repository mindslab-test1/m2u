package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.DialogAgentInstanceExecutionInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceResourcePortable;
import ai.maum.m2u.common.portable.DialogAgentManagerPortable;
import io.grpc.Status;
import io.grpc.StatusException;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import java.util.Collection;
import java.util.Map.Entry;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.DaInstance.DialogAgentInstance;
import maum.m2u.console.DaInstance.DialogAgentInstance.Builder;
import maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo;
import maum.m2u.console.DaInstance.DialogAgentInstanceList;
import maum.m2u.console.DaInstance.DialogAgentInstanceResourceList;
import maum.m2u.console.DaInstance.RunDialogAgentInstanceParamList;
import maum.m2u.da.Provider.InitParameter;
import maum.m2u.server.Dam.RunDialogAgentInstanceParam;
import maum.m2u.server.Pool.DialogAgentInstanceKey;
import maum.m2u.server.Pool.DialogAgentInstanceResource;
import maum.m2u.server.Pool.DialogAgentInstanceStat;
import maum.m2u.server.Pool.RegisterResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DialogAgentInstanceDAO {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentInstanceDAO.class);

  private IMap<String, DialogAgentInstancePortable> DialogAgentInstanceMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.DIALOG_AGENT_INSTANCE);

  private IMap<String, DialogAgentInstanceResourcePortable> DialogAgentInstanceResourceMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.DIALOG_AGENT_INSTANCE_RESOURCE);

  private IMap<String, DialogAgentInstanceExecutionInfoPortable> DialogAgentInstanceExecutionInfoMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.DIALOG_AGENT_INSTANCE_EXECUTION);

  private IMap<String, DialogAgentManagerPortable> DialogAgentManagerMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.DIALOG_AGENT_MANAGER);

  public void insertDialogAgentInstance(DialogAgentInstance req) throws Exception{
    logger.info("[m2u-svcadm-dao] insertDialogAgentInstance");
    logger.trace("[m2u-svcadm-dao] insertDialogAgentInstance = {}", req);

    DialogAgentInstancePortable dai = new DialogAgentInstancePortable();
    dai.setProtobufObj(req);

    // DialogAgentInstance 이름 중복체크
    Collection<DialogAgentInstancePortable> daipc =
        this.DialogAgentInstanceMap.values(new SqlPredicate("name = '" + req.getName() + "'"));
    if (!daipc.isEmpty()) {
      logger.error("DialogAgentInstance name already exists!");
      throw new StatusException(
          Status.ALREADY_EXISTS.withDescription("DialogAgentInstance name already exists!"));
    }

    // Dam에서 Daik삭제를 Listen하기 때문에 daiei보다 Dai를 먼저 삭제해준다.(순서중요함)
    this.DialogAgentInstanceMap.set(dai.getProtobufObj().getDaiId(), dai);

    for (DialogAgentInstanceExecutionInfo entry : req.getDaiExecInfoList()) {
      DialogAgentInstanceExecutionInfoPortable daiei = new DialogAgentInstanceExecutionInfoPortable();
      daiei.setProtobufObj(entry);

      String key = daiei.getProtobufObj().getDaiId() + "_" + daiei.getProtobufObj().getDamName();

      this.DialogAgentInstanceExecutionInfoMap.set(key, daiei);
    }
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_INSTANCE);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_INSTANCE_EXECUTION);
  }

  public void updateDialogAgentInstance(DialogAgentInstance req) {
    logger.info("[m2u-svcadm-dao] updateDialogAgentInstance");
    logger.trace("[m2u-svcadm-dao] updateDialogAgentInstance = {}", req);

    DialogAgentInstancePortable dai = new DialogAgentInstancePortable();
    dai.setProtobufObj(req);

    this.DialogAgentInstanceMap.set(dai.getProtobufObj().getDaiId(), dai);

    for (DialogAgentInstanceExecutionInfo entry : req.getDaiExecInfoList()) {
      DialogAgentInstanceExecutionInfoPortable sc1 = new DialogAgentInstanceExecutionInfoPortable();
      sc1.setProtobufObj(entry);

      String key = sc1.getProtobufObj().getDaiId() + "_" + sc1.getProtobufObj().getDamName();
      this.DialogAgentInstanceExecutionInfoMap.set(key, sc1);
    }
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_INSTANCE);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_INSTANCE_EXECUTION);
  }

  public void delDialogAgentInstance(Key req) {
    logger.info("[m2u-svcadm-dao] delDialogAgentInstance");
    logger.trace("[m2u-svcadm-dao] delDialogAgentInstance = {}", req.getName());

    this.DialogAgentInstanceMap.delete(req.getName());
    Collection<DialogAgentInstanceExecutionInfoPortable> result = this.DialogAgentInstanceExecutionInfoMap
        .values(new SqlPredicate("dai_id = '" + req.getName() + "'"));

    for (DialogAgentInstanceExecutionInfoPortable params : result) {
      // Daiei에는 실제 키값이 daiId_damId로 들어가있음
      String key = params.getProtobufObj().getDaiId() + "_" + params.getProtobufObj().getDamName();

      this.DialogAgentInstanceExecutionInfoMap.delete(key);
    }
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_INSTANCE);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_INSTANCE_EXECUTION);
  }

  public Builder getDialogAgentInstance(Key req) {
    logger.info("[m2u-svcadm-dao] delDialogAgentInstance");
    logger.trace("[m2u-svcadm-dao] delDialogAgentInstance = {}", req.getName());

    try {
      DialogAgentInstancePortable daip = this.DialogAgentInstanceMap.get(req.getName());
      Builder daib = daip.getProtobufObj().toBuilder();
      return daib;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return null;
  }

  public DialogAgentInstanceList.Builder getDialogAgentInstanceAllList() {
    logger.info("[m2u-svcadm-dao] getDialogAgentInstanceAllList");

    DialogAgentInstanceList.Builder dailb = DialogAgentInstanceList.newBuilder();

    for (Entry<String, DialogAgentInstancePortable> daip : this.DialogAgentInstanceMap.entrySet()) {

      Builder daib = daip.getValue().getProtobufObj().toBuilder();

      dailb.addDaiList(daib);
    }
    return dailb;
  }

  public DialogAgentInstanceList.Builder getDialogAgentInstanceByChatbotName(Key key) {
    logger.info("[m2u-svcadm-dao] getDialogAgentInstanceByChatbotName");
    logger.trace("[m2u-svcadm-dao] getDialogAgentInstanceByChatbotName = {}", key.getName());

    DialogAgentInstanceList.Builder dailb = DialogAgentInstanceList.newBuilder();

    Collection<DialogAgentInstancePortable> result =
        this.DialogAgentInstanceMap.values(
            new SqlPredicate("chatbot_name = '" + key.getName() + "'"));

    for (DialogAgentInstancePortable daip : result) {

      Builder daib = daip.getProtobufObj().toBuilder();

      dailb.addDaiList(daib);
    }
    return dailb;
  }

  public RegisterResponse.Builder register(DialogAgentInstanceResource req) {
    RegisterResponse.Builder rrb = RegisterResponse.newBuilder();
    logger.info("[m2u-svcadm-dao] register");
    logger.trace("[m2u-svcadm-dao] register = {}", req);

    DialogAgentInstanceResourcePortable dairp = new DialogAgentInstanceResourcePortable();

    dairp.setProtobufObj(req);

    this.DialogAgentInstanceResourceMap.set(dairp.getProtobufObj().getKey(), dairp);
    this.DialogAgentInstanceResourceMap.flush();

    rrb.setKey(dairp.getProtobufObj().getKey());
    rrb.setRegisteredAt(dairp.getProtobufObj().getStartedAt());

    return rrb;
  }

  public DialogAgentInstanceStat.Builder unregister(DialogAgentInstanceKey req) {
    DialogAgentInstanceStat.Builder daisb = DialogAgentInstanceStat.newBuilder();
    logger.info("[m2u-svcadm-dao] unregister");
    logger.trace("[m2u-svcadm-dao] unregister = {}", req);

    DialogAgentInstanceResourcePortable dairp = new DialogAgentInstanceResourcePortable();

    logger.debug("key : {}", req.getKey());
    this.DialogAgentInstanceResourceMap.delete(req.getKey());

    daisb.setKey(dairp.getProtobufObj().getKey());
    daisb.setRegisteredAt(dairp.getProtobufObj().getStartedAt());

    return daisb;
  }


  public boolean updateActiveDialogAgentInstance(Key req, boolean active) {
    logger.info("[m2u-svcadm-dao] updateActiveDialogAgentInstance");
    logger.trace("[m2u-svcadm-dao] updateActiveDialogAgentInstance = key : {}, active : {}", req.getName(), active);

    DialogAgentInstancePortable daip = this.DialogAgentInstanceMap.get(req.getName());
    DialogAgentInstance.Builder daib = daip.getProtobufObj().toBuilder();
    logger.debug("{} DialogAgentInstance {}", daib, req.getName());

    int index = 0;
    for (DialogAgentInstanceExecutionInfo entry : daib.getDaiExecInfoList()) {
      logger.debug("DialogAgentInstanceExecutionInfo entry {}", entry);
      String key = daib.getDaiId() + "_" + entry.getDamName();
      logger.debug("da exec key = {}", key);

      DialogAgentInstanceExecutionInfoPortable daiExecInfo =
          this.DialogAgentInstanceExecutionInfoMap.get(key);

      entry.toBuilder().setActive(active).build();
      daiExecInfo.setProtobufObj(entry);
      logger.debug("new DialogAgentInstanceExecutionInfo {}", entry);

      this.DialogAgentInstanceExecutionInfoMap.set(key, daiExecInfo);
      daib.setDaiExecInfo(index, entry);
      logger.debug("update {} {}", index, entry);


      index++;
    }
    daip.setProtobufObj(daib.build());

    boolean result = true;
    if (active) {
      this.DialogAgentInstanceMap.put(daip.getProtobufObj().getName(), daip);

    } else {
      this.DialogAgentInstanceMap.delete(daip.getProtobufObj().getName());
      result = false;
    }

    Common.invokeBackupSignal(MAP.DIALOG_AGENT_INSTANCE);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_INSTANCE_EXECUTION);

    return result;
  }

  public KeyList.Builder getKeyList(String type, String name) {
    logger.info("[m2u-svcadm-dao] getKeyList");
    logger.trace("[m2u-svcadm-dao] getKeyList = type : {}, name : {}", type, name);

    KeyList.Builder key_list = KeyList.newBuilder();
    if (type.equals("dam_name = '")) {
      Collection<DialogAgentInstanceExecutionInfoPortable> result = this.DialogAgentInstanceExecutionInfoMap
          .values(new SqlPredicate(type + name + "'"));

      for (DialogAgentInstanceExecutionInfoPortable params : result) {
        Key.Builder key = Key.newBuilder();
        key.setName(params.getProtobufObj().getDaiId());
        key_list.addKeyList(key);
      }
    } else if (type.equals("da_name = '")) {
      Collection<DialogAgentInstancePortable> result = this.DialogAgentInstanceMap
          .values(new SqlPredicate(type + name + "'"));

      for (DialogAgentInstancePortable params : result) {
        Key.Builder key = Key.newBuilder();
        key.setName(params.getProtobufObj().getDaiId());
        key_list.addKeyList(key);
      }
    }
    return key_list;
  }

  public DialogAgentInstanceList.Builder getDialogAgentInstanceListToDamFilter(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentInstanceListToDamFilter");
    logger.trace("[m2u-svcadm-dao] getDialogAgentInstanceListToDamFilter = {}", req.getName());

    DialogAgentInstanceList.Builder dail = DialogAgentInstanceList.newBuilder();

    Collection<DialogAgentInstanceExecutionInfoPortable> daieip = this.DialogAgentInstanceExecutionInfoMap
        .values(new SqlPredicate("dam_name = '" + req.getName() + "'"));
    for (DialogAgentInstanceExecutionInfoPortable entry2 : daieip) {
      dail.addDaiList(
          this.DialogAgentInstanceMap.get(entry2.getProtobufObj().getDaiId()).getProtobufObj()
              .toBuilder());
    }

    return dail;
  }

  public DialogAgentInstanceResourceList getDialogAgentInstanceResourceList(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentInstanceResourceList");
    logger.trace("[m2u-svcadm-dao] getDialogAgentInstanceResourceList = {}", req.getName());

    DialogAgentInstanceResourceList.Builder dair_list =
        DialogAgentInstanceResourceList.newBuilder();
    DialogAgentInstance dai = getDialogAgentInstance(req).build();

    for (DialogAgentInstanceExecutionInfo entry : dai.getDaiExecInfoList()) {
      logger.debug("exec entry {}", entry);
      for (String key : entry.getKeyList()) {
        logger.debug("key :: {}, {}", key);
        // DialogAgentInstanceResource에 dai_id로 조회 가능하도록 수정
        Collection<DialogAgentInstanceResourcePortable> dairp = this.DialogAgentInstanceResourceMap
            .values(new SqlPredicate("dai_id = '" + req.getName() + "' and key = '" + key +  "'"));

        for (DialogAgentInstanceResourcePortable entry2 : dairp) {
          dair_list.addDairList(entry2.getProtobufObj());
        }
      }
    }

    return dair_list.build();
  }

  public DialogAgentInstanceResourceList getDialogAgentInstanceResourceListToDamFilter(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentInstanceResourceListToDamFilter");
    logger.trace("[m2u-svcadm-dao] getDialogAgentInstanceResourceListToDamFilter = {}", req.getName());

    DialogAgentInstanceResourceList.Builder dair_list = DialogAgentInstanceResourceList
        .newBuilder();
    DialogAgentInstance dai = getDialogAgentInstance(req).build();

    for (DialogAgentInstanceExecutionInfo entry : dai.getDaiExecInfoList()) {
      for (String key : entry.getKeyList()) {
        dair_list.addDairList(this.DialogAgentInstanceResourceMap.get(key).getProtobufObj());
      }
    }

    return dair_list.build();
  }

  public RunDialogAgentInstanceParamList getDialogAgentInstanceParamList(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentInstanceParamList");
    logger.trace("[m2u-svcadm-dao] getDialogAgentInstanceParamList = {}", req.getName());

    RunDialogAgentInstanceParamList.Builder daip_list = RunDialogAgentInstanceParamList
        .newBuilder();

    logger.debug("Key {}", req);
    DialogAgentInstanceList.Builder dailb = DialogAgentInstanceList.newBuilder();

    Collection<DialogAgentInstanceExecutionInfoPortable> daieip = this.DialogAgentInstanceExecutionInfoMap
        .values(new SqlPredicate("dam_name = '" + req.getName() + "'"));
    for (DialogAgentInstanceExecutionInfoPortable entry : daieip) {
      if (entry.getProtobufObj().getActive()) {

        DialogAgentInstancePortable daip = this.DialogAgentInstanceMap
            .get(entry.getProtobufObj().getDaiId());

        for (String Key : entry.getProtobufObj().getKeyList()) {
          RunDialogAgentInstanceParam.Builder rdaip = RunDialogAgentInstanceParam.newBuilder();

          rdaip.setKey(Key);

          rdaip.setAgent(daip.getProtobufObj().getDaName());
          rdaip.setVersion(daip.getProtobufObj().getVersion());
          rdaip.setDescription(daip.getProtobufObj().getDescription());
          rdaip.setDaProdSpec(daip.getProtobufObj().getDaProdSpec());

          InitParameter.Builder iparam = InitParameter.newBuilder();
          iparam.putAllParams(daip.getProtobufObj().getParamsMap());
          iparam.setChatbot(daip.getProtobufObj().getChatbotName());
          for (int skillIndex = 0; skillIndex < daip.getProtobufObj().getSkillNamesCount();
              skillIndex++) {
            iparam.setSkills(skillIndex, daip.getProtobufObj().getSkillNames(skillIndex));
          }
          iparam.setLang(daip.getProtobufObj().getLang());
          logger.info("=== rdaip :: {}", rdaip);
          rdaip.setParam(iparam);
          daip_list.addRdaipList(rdaip);
        }
      }
    }
    return daip_list.build();
  }
}
