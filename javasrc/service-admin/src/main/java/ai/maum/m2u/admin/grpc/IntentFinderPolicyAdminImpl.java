package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.IntentFinderPolicyDAO;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.console.Classifier.IntentFinderPolicyList;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import maum.m2u.router.v3.Intentfinder.IntentFinderPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntentFinderPolicyAdminImpl extends
    maum.m2u.console.IntentFinderPolicyAdminGrpc.IntentFinderPolicyAdminImplBase {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderAdminImpl.class);

  static IntentFinderPolicyDAO ifpDao = new IntentFinderPolicyDAO();

  /**
   * grpc client로 부터 IntentFinderPolicy 전체 리스트 조회 요청을 받는다. Hazelcast로부터 데이터를 조회하여,
   * IntentFinderPolicy를 grpc client로 전달 해준다.
   *
   * @param req => {com.google.protobuf.Empty}
   * @param res => {IntentFinderPolicyList}
   */
  @Override
  public void getIntentFinderPolicyAllList(com.google.protobuf.Empty req,
      StreamObserver<IntentFinderPolicyList> res) {
    logger.info("[m2u-svcadm-grpc] getIntentFinderPolicyAllList");

    try {
      IntentFinderPolicyList.Builder ifplb = ifpDao.getIntentFinderPolicyAllList();

      res.onNext(ifplb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   * Grpc Client로부터 ITF Policy Name으로 Policy 정보 조회 관련 요청 받음
   *
   * @param req Key => {IntentFinderPolicy name}
   * @param res StreamObserver<IntentFinderPolicy>
   */
  @Override
  public void getIntentFinderPolicyInfo(Key req,
      StreamObserver<IntentFinderPolicy> res) {
    logger.info("[m2u-svcadm-grpc] getIntentFinderPolicyInfo");
    logger.trace("[m2u-svcadm-grpc] getIntentFinderPolicyInfo = {}", req.getName());

    try {
      IntentFinderPolicy.Builder ifplb = ifpDao.getIntentFinderPolicy(req);

      res.onNext(ifplb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }



  /**
   * grpc client로 부터 IntentFinderPolicy insert 요청을 받는다. Hazelcast로부터 데이터를 insert한 후, grpc lient로부터
   * insert한 값을 다시 전달 해준다.
   *
   * @param req => {IntentFinderPolicy}
   * @param res => {IntentFinderPolicy}
   */
  @Override
  public void insertIntentFinderPolicyInfo(IntentFinderPolicy req,
      StreamObserver<IntentFinderPolicy> res) {
    logger.info("[m2u-svcadm-grpc] insertIntentFinderPolicyInfo");
    logger.trace("[m2u-svcadm-grpc] insertIntentFinderPolicyInfo = {}", req);

    try {
      ifpDao.insertIntentFinderPolicy(req);

      res.onNext(req);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   * grpc client로 부터 IntentFinderPolicy update 요청을 받는다. Hazelcast로부터 데이터를 update 한후, update 한 값을 다시
   * 전달 해준다.
   *
   * @param req => {IntentFinderPolicy}
   * @param res => {IntentFinderPolicy}
   */
  @Override
  public void updateIntentFinderPolicyInfo(IntentFinderPolicy req,
      StreamObserver<IntentFinderPolicy> res) {
    logger.info("[m2u-svcadm-grpc] updateIntentFinderPolicyInfo");
    logger.trace("[m2u-svcadm-grpc] updateIntentFinderPolicyInfo = {}", req);

    try {
      ifpDao.updateIntentFinderPolicy(req);

      res.onNext(req);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }


  /**
   * Grpc Client로부터 ITF Policies Delete 관련 요청 받음
   *
   * @param req => {KeyList} Key의 배열 형태, Key 는 IntentFinderPolicy 의 name 이다.
   * @param res => {ResultList} Result의 배열 형태, Result는 IntentFinderPolicy name 과 삭제 성공 유무를 가지고 있다.
   */
  @Override
  public void deleteIntentFinderPolicies(KeyList req, StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteIntentFinderPolicies");
    logger.trace("[m2u-svcadm-grpc] deleteIntentFinderPolicies = {}", req);

    try {
      ResultList.Builder kyb = ResultList.newBuilder();
      for (Key key : req.getKeyListList()) {
        Result.Builder result = Result.newBuilder();
        IntentFinderPolicy.Builder ifp = ifpDao.getIntentFinderPolicy(key);
        result.setName(key.getName());
        if (ifp == null) {
          result.setResult(false);
        } else {
          ifpDao.deleteIntentFinderPolicy(key);
          result.setResult(true);
        }
        kyb.addResultList(result);
      }

      res.onNext(kyb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

}
