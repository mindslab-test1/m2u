package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.DialogAgentDAO;
import ai.maum.m2u.admin.dao.DialogAgentInstanceDAO;
import ai.maum.m2u.admin.dao.DialogAgentManagerDAO;
import ai.maum.m2u.admin.itfc.GrpcInterfaceManager;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.console.Da.DialogAgentManager;
import maum.m2u.console.Da.DialogAgentManagerList;
import maum.m2u.console.Da.DialogAgentWithDialogAgentInstanceList;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import maum.m2u.server.Dam.DamKey;
import maum.m2u.server.Dam.DialogAgentManagerRegisterResult;
import maum.m2u.server.Dam.DialogAgentManagerStat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DialogAgentManagerAdminImpl extends
    maum.m2u.console.DialogAgentManagerAdminGrpc.DialogAgentManagerAdminImplBase {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentManagerAdminImpl.class);

  static DialogAgentManagerDAO damDao = new DialogAgentManagerDAO();
  static DialogAgentDAO daDao = new DialogAgentDAO();
  static DialogAgentInstanceDAO daiDao = new DialogAgentInstanceDAO();

  @Override
  public void getDialogAgentManagerAllList(com.google.protobuf.Empty req,
                                           StreamObserver<DialogAgentManagerList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentManagerAllList");

    try {
      DialogAgentManagerList.Builder daml = damDao.getDialogAgentManagerAllList();

      res.onNext(daml.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }

  }

  @Override
  public void getDialogAgentManagerInfo(Key req,
                                        StreamObserver<DialogAgentManager> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentManagerInfo");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentManagerInfo = {}", req.getName());

    try {
      DialogAgentManager.Builder damb = damDao
          .getDialogAgentManager(req);

      res.onNext(damb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }

  }

  @Override
  public void getDialogAgentWithDialogAgentInstanceList(
      Key req,
      StreamObserver<DialogAgentWithDialogAgentInstanceList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentWithDialogAgentInstanceList");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentWithDialogAgentInstanceList = {}", req.getName());

    try {
      DialogAgentWithDialogAgentInstanceList.Builder dawdl =
          daDao .getDialogAgentWithDialogAgentInstanceList(req);

      res.onNext(dawdl.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }

  }

  @Override
  public void insertDialogAgentManagerInfo(DialogAgentManager req,
                                           StreamObserver<DialogAgentManager> res) {
    logger.info("[m2u-svcadm-grpc] insertDialogAgentManagerInfo");
    logger.trace("[m2u-svcadm-grpc] insertDialogAgentManagerInfo = {}", req);

    try {

      Key.Builder key = Key.newBuilder();
      key.setName(req.getName());

      DialogAgentManager.Builder damb = damDao.getDialogAgentManager(key.build());

      if (damDao.checkIpPort(req)) {
        res.onError(new StatusException(
            Status.ALREADY_EXISTS.withDescription("IP, port duplicate")));
        throw new Exception("IP, port duplicate");
      }

      if (damb.getName().equals(req.getName())) {
        res.onError(new StatusException(
            Status.ALREADY_EXISTS.withDescription("name duplicate")));
        logger.error("name duplicated {}", damb.getName());
        throw new Exception("name duplicate");
      } else {
        logger.debug("calling insertDialogAgentManager {}", req);

        try {
          //setDamName
          GrpcInterfaceManager client = new GrpcInterfaceManager(req.getIp(),
              req.getPort());
          DamKey.Builder damKey = DamKey.newBuilder();
          damKey.setName(req.getName());
          DialogAgentManagerStat stat = client.setDamName(damKey.build());
          client.shutdown();

          // rpc setName에서 error 발생한 경우
          if (stat == null) {
            res.onError(new StatusException(
                Status.UNAVAILABLE.withDescription("Unavailable DAM server")));
            logger.error("Unavailable DAM server {}:{}", req.getIp(), req.getPort());
            throw new Exception("Unavailable DAM server");
          } else if (stat.getResultCode() !=
              DialogAgentManagerRegisterResult.DIALOG_AGENT_MANAGER_ADD_SUCCESS) {
            logger.debug("Failed setDamName: " + stat.getDetailMessage()
                + " [" + stat.getResultCode() + "]");
            throw new Exception("Failed setDamName: " + stat.getDetailMessage()
                + " [" + stat.getResultCode() + "]");
          } else {
            damDao.insertDialogAgentManager(req);
          }
        } catch (Exception e) {
          throw e;
        }
      }

      res.onNext(damb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(
              Status.UNKNOWN.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void updateDialogAgentManagerInfo(DialogAgentManager req,
                                           StreamObserver<DialogAgentManager> res) {
    logger.info("[m2u-svcadm-grpc] updateDialogAgentManagerInfo");
    logger.trace("[m2u-svcadm-grpc] updateDialogAgentManagerInfo = {}", req);

    try {
      DialogAgentManager.Builder damb = damDao.updateDialogAgentManager(req);

      if (damb == null) {
        throw new Exception("unExist update data");
      }

      res.onNext(damb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void deleteDialogAgentManager(KeyList req,
                                       StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteDialogAgentManager");
    logger.trace("[m2u-svcadm-grpc] deleteDialogAgentManager = {}", req);

    try {

      ResultList.Builder resultList = ResultList.newBuilder();
      DialogAgentManagerDAO dialogAgentManagerDAO = new DialogAgentManagerDAO();

      for (Key key : req.getKeyListList()) {
        Result.Builder result = Result.newBuilder();
        DialogAgentManager.Builder dam = dialogAgentManagerDAO.getDialogAgentManager(key);

        DamKey.Builder damKey = DamKey.newBuilder();
        damKey.setName(dam.getName());

        result.setName(key.getName());
        if (dam == null) {
          result.setResult(false);
        } else {
          damDao.delDialogAgentManager(key);
          result.setResult(true);
        }
        resultList.addResultList(result);
      }

      res.onNext(resultList.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}

