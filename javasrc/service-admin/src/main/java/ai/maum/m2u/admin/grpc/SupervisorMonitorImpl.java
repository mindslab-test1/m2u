package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.itfc.SupervisordInterfaceManager;
import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import java.util.Map;
import maum.supervisor.Monitor.Result;
import maum.supervisor.Monitor.StdTailLog;
import maum.supervisor.Monitor.SupervisorProcess;
import maum.supervisor.Monitor.SupervisorProcessKey;
import maum.supervisor.Monitor.SupervisorServerGroup;
import maum.supervisor.Monitor.SupervisorServerGroupKey;
import maum.supervisor.Monitor.SupervisorServerGroupList;
import maum.supervisor.SupervisorMonitorGrpc.SupervisorMonitorImplBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SupervisorMonitorImpl extends SupervisorMonitorImplBase {

  static final Logger logger = LoggerFactory.getLogger(SupervisorMonitorImpl.class);

  private static SupervisordInterfaceManager svdItfcManager;

  public SupervisorMonitorImpl() {
    this.svdItfcManager = SupervisordInterfaceManager.getInstance();
  }

  @Override
  public void informStartUp(SupervisorServerGroup req, StreamObserver<Result> res) {
    logger.info("[m2u-svcadm-grpc] informStartUp");
    logger.trace("[m2u-svcadm-grpc] informStartUp = {}", req);

    boolean isSuccess = false;
    String message = "";

    Map<String, Object> returnValue = svdItfcManager.addSupervisordInterfaceInstance(req);

    Result.Builder result = Result.newBuilder();
    result.setResult((boolean) returnValue.get("result"));
    result.setMessage((String) returnValue.get("message"));

    res.onNext(result.build());
    res.onCompleted();
  }

  @Override
  public void getSupervisorServerGroupList(Empty req,
                                           StreamObserver<SupervisorServerGroupList> res) {
    logger.info("[m2u-svcadm-grpc] getSupervisorServerGroupList");

    SupervisorServerGroupList result = null;
    try {
      result = svdItfcManager.getSupervisordServerGroupList(req);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(e);
    }
    if (result != null) {
      res.onNext(result);
      res.onCompleted();
    } else {
      res.onError(new NullPointerException());
    }
  }

  @Override
  public void getSupervisorServerGroup(SupervisorServerGroupKey req,
                                       StreamObserver<SupervisorServerGroup> res) {
    SupervisorServerGroup result = null;
    try {
      result = svdItfcManager.getSupervisordServerGroup(req);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(e);
    }
    if (result != null) {
      res.onNext(result);
      res.onCompleted();
    } else {
      res.onError(new NullPointerException());
    }
  }

  @Override
  public void getSupervisorProcess(SupervisorProcessKey req,
                                   StreamObserver<SupervisorProcess> res) {
    logger.info("[m2u-svcadm-grpc] getSupervisorProcess");
    logger.trace("[m2u-svcadm-grpc] getSupervisorProcess = {}", req.getName());

    SupervisorProcess result = null;
    try {
      result = svdItfcManager.getSupervisordProcess(req);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(e);
    }
    if (result != null) {
      res.onNext(result);
      res.onCompleted();
    } else {
      res.onError(new NullPointerException());
    }
  }

  @Override
  public void startSupervisorProcess(SupervisorProcessKey req, StreamObserver<Result> res) {
    logger.info("[m2u-svcadm-grpc] startSupervisorProcess");
    logger.trace("[m2u-svcadm-grpc] startSupervisorProcess = {}", req);

    Result result = null;
    try {
      result = svdItfcManager.startSupervisorProcess(req);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(e);
    }
    if (result != null) {
      res.onNext(result);
      res.onCompleted();
    } else {
      res.onError(new NullPointerException());
    }
  }

  @Override
  public void stopSupervisorProcess(SupervisorProcessKey req, StreamObserver<Result> res) {
    logger.info("[m2u-svcadm-grpc] stopSupervisorProcess");
    logger.trace("[m2u-svcadm-grpc] stopSupervisorProcess = {}", req);

    Result result = null;
    try {
      result = svdItfcManager.stopSupervisorProcess(req);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(e);
    }
    if (result != null) {
      res.onNext(result);
      res.onCompleted();
    } else {
      res.onError(new NullPointerException());
    }
  }

  @Override
  public void getTailProcessStdoutLog(SupervisorProcessKey req, StreamObserver<StdTailLog> res) {
    logger.info("[m2u-svcadm-grpc] getTailProcessStdoutLog");
    logger.trace("[m2u-svcadm-grpc] getTailProcessStdoutLog = {}", req);

    StdTailLog result = null;
    try {
      result = svdItfcManager.getTailProcessStdoutLog(req);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(e);
    }
    if (result != null) {
      res.onNext(result);
      res.onCompleted();
    } else {
      res.onError(new NullPointerException());
    }
  }

}