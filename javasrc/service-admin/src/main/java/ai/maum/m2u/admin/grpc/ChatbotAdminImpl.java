package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.ChatbotDAO;
import ai.maum.m2u.admin.dao.DialogAgentInstanceDAO;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.console.ChatbotOuterClass.Chatbot;
import maum.m2u.console.ChatbotOuterClass.ChatbotList;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import maum.m2u.console.DaInstance.DialogAgentInstance;
import maum.m2u.console.DaInstance.DialogAgentInstanceList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChatbotAdminImpl extends maum.m2u.console.ChatbotAdminGrpc.ChatbotAdminImplBase {

  static final Logger logger = LoggerFactory.getLogger(ChatbotAdminImpl.class);

  static ChatbotDAO cbDao = new ChatbotDAO();
  static DialogAgentInstanceDAO dialogAgentInstanceDAO = new DialogAgentInstanceDAO();

  @Override
  public void getChatbotAllList(com.google.protobuf.Empty req,
      StreamObserver<ChatbotList> res) {
    logger.info("[m2u-svcadm-grpc] getChatbotAllList");

    try {
      ChatbotList.Builder cblb = cbDao.getChatbotAllList();

      res.onNext(cblb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }

  }

  @Override
  public void getChatbotInfo(Key req,
      StreamObserver<Chatbot> res) {
    logger.info("[m2u-svcadm-grpc] getChatbotInfo");
    logger.trace("[m2u-svcadm-grpc] getChatbotInfo = {}", req.getName());

    try {
      Chatbot.Builder cbb = cbDao.getChatbot(req);

      res.onNext(cbb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   */
  @Override
  public void insertChatbotInfo(Chatbot req,
      StreamObserver<Chatbot> res) {
    logger.info("[m2u-svcadm-grpc] insertChatbotInfo");
    logger.trace("[m2u-svcadm-grpc] insertChatbotInfo = {}", req.getName());

    try {
      Chatbot.Builder cbb = Chatbot.newBuilder();

      cbDao.insertChatbot(req);

      res.onNext(cbb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void updateChatbotInfo(Chatbot req,
      StreamObserver<Chatbot> res) {
    logger.info("[m2u-svcadm-grpc] updateChatbotInfo");
    logger.trace("[m2u-svcadm-grpc] updateChatbotInfo = {}", req.getName());

    try {
      Chatbot.Builder cbb = Chatbot.newBuilder();

      cbDao.updateChatbot(req);

      res.onNext(cbb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void deleteChatbot(KeyList req, StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteChatbot");
    logger.trace("[m2u-svcadm-grpc] deleteChatbot = {}", req);

    try {
      ResultList.Builder rlb = ResultList.newBuilder();

      for (Key key : req.getKeyListList()) {
        Result.Builder rb = Result.newBuilder();
        Chatbot.Builder cbb = cbDao.getChatbot(key);
        rb.setName(key.getName());
        if (cbb == null) {
          rb.setResult(false);
        } else {
          // 챗봇에 등록된 DAI를 조회하여 삭제 한뒤 챗봇을 삭제한다.
          Key.Builder chatbotNameKey = Key.newBuilder();
          chatbotNameKey.setName(key.getName());
          DialogAgentInstanceList.Builder dialogAgentInstanceList = dialogAgentInstanceDAO
              .getDialogAgentInstanceByChatbotName(chatbotNameKey.build());

          for (DialogAgentInstance dialogAgentInstance : dialogAgentInstanceList.getDaiListList()) {
            Key.Builder daiIdKey = Key.newBuilder();
            daiIdKey.setName(dialogAgentInstance.getDaiId());
            dialogAgentInstanceDAO.delDialogAgentInstance(daiIdKey.build());
          }

          cbDao.delChatbot(key);
          rb.setResult(true);
        }
        rlb.addResultList(rb);
      }

      res.onNext(rlb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}


