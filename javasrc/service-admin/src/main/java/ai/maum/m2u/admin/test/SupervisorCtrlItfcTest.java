package ai.maum.m2u.admin.test;

import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import maum.supervisor.Monitor.Result;
import maum.supervisor.Monitor.SupervisorServerGroup;
import maum.supervisor.Monitor.SupervisorServerGroupList;
import maum.supervisor.SupervisorMonitorGrpc;
import maum.supervisor.SupervisorMonitorGrpc.SupervisorMonitorBlockingStub;

public class SupervisorCtrlItfcTest {

  private static final Logger logger = Logger.getLogger(SupervisorCtrlItfcTest.class.getName());

  private final ManagedChannel channel;
  private final SupervisorMonitorBlockingStub svmBlockingStub;

  public SupervisorCtrlItfcTest(String host, int port) {
    this(ManagedChannelBuilder.forAddress(host, port)
        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
        // needing certificates.
        .usePlaintext(true));
  }

  /**
   * Construct client for accessing RouteGuide server using the existing channel.
   */
  SupervisorCtrlItfcTest(ManagedChannelBuilder<?> channelBuilder) {
    channel = channelBuilder.build();
    svmBlockingStub = SupervisorMonitorGrpc.newBlockingStub(channel);
  }

  /**
   * Greet server. If provided, the first element of {@code args} is the name to use in the
   * greeting.
   */
  public static void main(String[] args) throws Exception {
    SupervisorCtrlItfcTest client = new SupervisorCtrlItfcTest("localhost", 9701);

    Scanner sc = new Scanner(System.in);

    try {

      while (sc.hasNext()) {

        String inputCmd = sc.nextLine().trim();
        String cmd = "";
        int paramLen = 0;
        String[] inputArr = inputCmd.split(" ");
        if (inputArr != null && inputArr.length > 1) {
          cmd = inputArr[0].trim();
          paramLen = inputArr.length - 1;
        } else {
          cmd = inputCmd.trim();
        }

        String[] param = new String[paramLen];
        for (int i = 0; i < paramLen; i++) {
          param[i] = inputArr[i + 1];
        }

        if (!cmd.equals("q")) {
          if (cmd.equals("add"))
            client.informStartUp(param[0], param[1], Integer.parseInt(param[2]));
          if (cmd.equals("get")) client.getSupervisordServerGroupList();
        } else {
          System.exit(0);
        }
      }

    } finally {
      client.shutdown();
    }
  }

  public void shutdown() throws InterruptedException {
    channel.shutdown().shutdown();
  }

  public void shutdownWait() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  private void informStartUp(String host, String ip, int port) {
    SupervisorServerGroup.Builder svSvrGrp = SupervisorServerGroup.newBuilder();
    svSvrGrp.setHost(host);
    svSvrGrp.setIp(ip);
    svSvrGrp.setPort(port);
    Result res;
    try {
      res = svmBlockingStub.informStartUp(svSvrGrp.build());
      System.out.println(res.getResult() + " : " + res.getMessage());
    } catch (StatusRuntimeException e) {
      logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
    }
  }

  public void getSupervisordServerGroupList() {
    Empty.Builder empty = Empty.newBuilder();
    SupervisorServerGroupList res;
    try {
      res = svmBlockingStub.getSupervisorServerGroupList(empty.build());
    } catch (StatusRuntimeException e) {
      logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
    }
  }
}
