package ai.maum.m2u.admin.itfc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import java.util.concurrent.TimeUnit;
import maum.m2u.common.Userattr.UserAttributeList;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.server.Dam.DialogAgentExecutables;
import maum.m2u.server.Dam.DialogAgentManagerStat;
import maum.m2u.server.Dam.DialogAgentName;
import maum.m2u.server.DialogAgentManagerGrpc;
import maum.m2u.server.SimpleClassifierGrpc;
import maum.m2u.server.Simpleclassifier;
import org.slf4j.LoggerFactory;

public class GrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(GrpcInterfaceManager.class);

  private final ManagedChannel channel;
  private final DialogAgentManagerGrpc.DialogAgentManagerBlockingStub damStub;
  private final SimpleClassifierGrpc.SimpleClassifierBlockingStub scStub;

  public GrpcInterfaceManager(String host, int port) {
    this(ManagedChannelBuilder.forAddress(host, port)
        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
        // needing certificates.
        .usePlaintext(true));
  }

  /**
   * Construct client for accessing RouteGuide server using the existing channel.
   */
  GrpcInterfaceManager(ManagedChannelBuilder<?> channelBuilder) {
    channel = channelBuilder.build();
    damStub = DialogAgentManagerGrpc.newBlockingStub(channel);
    scStub = SimpleClassifierGrpc.newBlockingStub(channel);
  }

  /**
   * Greet server. If provided, the first element of {@code args} is the name to use in the
   * greeting.
   */
  public static void main(String[] args) throws Exception {

  }

  public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  public RuntimeParameterList getRuntimeParameters(DialogAgentName request) {
    logger.info("[m2u-svcadm] getRuntimeParameters");
    logger.trace("[m2u-svcadm] request = {}", request);

    try {
      return damStub.getRuntimeParameters(request);
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus(), e);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("{} => ", e.getMessage(), e);
      }
      return null;
    }
  }

  public UserAttributeList getUserAttributes(DialogAgentName request) {
    logger.info("[m2u-svcadm] getUserAttributes");
    logger.trace("[m2u-svcadm] request = {}", request);

    try {
      return damStub.getUserAttributes(request);
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus(), e);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("{} => ", e.getMessage(), e);
      }
      return null;
    }
  }

  public DialogAgentExecutables getExecutableDA(com.google.protobuf.Empty request) {
    logger.info("[m2u-svcadm] getExecutableDA");

    try {
      return damStub.getExecutableDA(request);
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus(), e);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("{} => ", e.getMessage(), e);
      }
      return null;
    }
  }

  // specify dam name
  public DialogAgentManagerStat setDamName(maum.m2u.server.Dam.DamKey request) {
    logger.info("[m2u-svcadm] setDamName");
    logger.trace("[m2u-svcadm] request = {}", request);

    try {
      return damStub.setDamName(request);
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus(), e);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("{} => ", e.getMessage(), e);
      }
      return null;
    }
  }

  public Simpleclassifier.SimpleClassifierStat makeSimpleClassifier(Simpleclassifier.SimpleClassifierResource request) {
    logger.info("[m2u-svcadm] makeSimpleClassifier");
    logger.trace("[m2u-svcadm] makeSimpleClassifier = {}", request);

    try {
      return scStub.makeSimpleClassifier(request);
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus(), e);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("{} => ", e.getMessage(), e);
      }
      return null;
    }
  }
}