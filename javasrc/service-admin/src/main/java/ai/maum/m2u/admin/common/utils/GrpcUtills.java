package ai.maum.m2u.admin.common.utils;

import io.grpc.ManagedChannel;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GrpcUtills {

  static final Logger logger = LoggerFactory.getLogger(GrpcUtills.class);

  public static void closeChannel(ManagedChannel channel) {
    logger.info("Call close channel shutdown awaitTermination");
    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      logger.error("#@ closeChannel Exception [{}]", e);
    }
  }
}
