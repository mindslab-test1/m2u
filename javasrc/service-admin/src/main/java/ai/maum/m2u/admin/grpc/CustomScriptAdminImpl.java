package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.config.PropertyManager;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import maum.m2u.console.Classifier.CustomScriptResultList;
import maum.m2u.console.Da.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomScriptAdminImpl extends maum.m2u.console.CustomScriptAdminGrpc.CustomScriptAdminImplBase {

  static final String P_PATH = "itfd.customscript.path";
  static final Logger logger = LoggerFactory.getLogger(CustomScriptAdminImpl.class);

  @Override
  public void getCustomScriptModels(com.google.protobuf.Empty req, StreamObserver<CustomScriptResultList> res) {
    logger.info("[m2u-svcadm-grpc] getCustomModels");
    List<String> modelList = new ArrayList<String>();
    try {
      String fileName;
      String envPath = PropertyManager.getString(P_PATH);
      String dirPath = envPath.replace("${MAUM_ROOT}", System.getenv("MAUM_ROOT"));
      File dir = new File(dirPath);
      File[] fileList = dir.listFiles();
      logger.debug("CustomScript Path : {}", dirPath);
      int idx = 0;
      for (final  File file : fileList) {
        if (file.isDirectory())
          continue;
        fileName = file.getName();
        if (!fileName.endsWith(".categories"))
          continue;
        idx = fileName.lastIndexOf(".");
        modelList.add(fileName.substring(0, idx));
        logger.debug("Get CustomScript Model : {}", fileName);
      }
      CustomScriptResultList models = CustomScriptResultList.newBuilder().addAllEntries(modelList).build();
      res.onNext(models);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{}=>", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getCustomScriptCategories(Key req, StreamObserver<CustomScriptResultList> res) {
    logger.info("[m2u-svcadm-grpc] getCustomScriptCategories");
    List<String> categoryList = new ArrayList<String>();
    try {
      BufferedReader br = null;
      String envPath = PropertyManager.getString(P_PATH);
      String dirPath = envPath.replace("${MAUM_ROOT}", System.getenv("MAUM_ROOT"));
      File dir = new File(dirPath);
      logger.debug("CustomScript Path : {}", dirPath);
      br = new BufferedReader(new FileReader(dirPath + "/" + req.getName() + ".categories"));
      String line;
      while ((line = br.readLine()) != null) {
        categoryList.add(line);
      }
      logger.debug("Get {}.categories done", req.getName());
      CustomScriptResultList categories = CustomScriptResultList.newBuilder().addAllEntries(categoryList)
          .build();
      res.onNext(categories);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{}=>", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}
