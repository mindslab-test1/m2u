package ai.maum.m2u.admin.hzc;

import ai.maum.m2u.admin.dao.Common;
import ai.maum.m2u.admin.itfc.GrpcInterfaceManager;
import ai.maum.m2u.common.portable.AuthticationPolicyPortable;
import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.DialogAgentActivationInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceExecutionInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceResourcePortable;
import ai.maum.m2u.common.portable.DialogAgentManagerPortable;
import ai.maum.m2u.common.portable.DialogAgentPortable;
import ai.maum.m2u.common.portable.IntentFinderInstancePortable;
import ai.maum.m2u.common.portable.IntentFinderPolicyPortable;
import ai.maum.m2u.common.portable.ServiceAdminPortableFactory;
import ai.maum.m2u.common.portable.SimpleClassifierPortable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import maum.m2u.console.ChatbotOuterClass.AuthticationPolicy;
import maum.m2u.console.ChatbotOuterClass.Chatbot;
import maum.m2u.console.Classifier.SimpleClassifier;
import maum.m2u.console.Da.DialogAgent;
import maum.m2u.console.Da.DialogAgentActivationInfo;
import maum.m2u.console.Da.DialogAgentManager;
import maum.m2u.console.DaInstance.DialogAgentInstance;
import maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo;
import maum.m2u.router.v3.Intentfinder.IntentFinderInstance;
import maum.m2u.router.v3.Intentfinder.IntentFinderPolicy;
import maum.m2u.server.Dam.DamKey;
import maum.m2u.server.Dam.DialogAgentManagerRegisterResult;
import maum.m2u.server.Dam.DialogAgentManagerStat;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unchecked")
public class HazelcastSettingsCarrier {

  static final Logger logger = LoggerFactory.getLogger(HazelcastSettingsCarrier.class);

  private static final String HzcAuthPolicy = "auth-policy";
  private static final String HzcChatbotDetail = "chatbot-detail";
  private static final String HzcDAManager = "dialog-agent-manager";
  private static final String HzcDA = "dialog-agent";
  private static final String HzcDAActivation = "dialog-agent-activation";
  private static final String HzcDAInstance = "dialog-agent-instance";
  private static final String HzcDAInstExec = "dialog-agent-instance-exec";
  private static final String HzcDAIR = "dialog-agent-instance-resource";
  private static final String HzcIntentFinderInstExec = "intent-finder-instance-exec";

  private static final String HzcSC = "simple-classifier";
  private static final String HzcChatbot = "chatbot";

  private static final String HzcIntentFinderInstance = "intent-finder-instance";
  private static final String HzcIntentFinderPolicy = "intent-finder-policy";

  private static String getSystemConfPath(String paramPath) {
    String path = System.getenv("MAUM_ROOT");
    path += paramPath;
    return path;
  }

  private static void delay(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      logger.error("delay e : ", e);
    }
  }

  private static boolean ImportIntentFinderInstance(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportIntentFinderInstance");
    IMap<String, IntentFinderInstancePortable> hzcMap = hz.getMap(HzcIntentFinderInstance);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      IntentFinderInstancePortable portable = new
          IntentFinderInstancePortable();
      IntentFinderInstance.Builder builder = IntentFinderInstance.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        System.out.println("  + " + "intentFinderInstance=" + portable.toString());
        hzcMap.set(builder.getName(), portable);
      } catch (Exception e) {
        logger.error("ImportIntentFinderInstance e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportIntentFinderPolicy(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportIntentFinderPolicy");
    IMap<String, IntentFinderPolicyPortable> hzcMap = hz.getMap(HzcIntentFinderPolicy);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      IntentFinderPolicyPortable portable = new IntentFinderPolicyPortable();
      IntentFinderPolicy.Builder builder = IntentFinderPolicy
          .newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getName(), portable);
      } catch (Exception e) {
        logger.error("ImportIntentFinderPolicy e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportSimpleClassifier(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportSimpleClassifier");
    IMap<String, SimpleClassifierPortable> hzcMap = hz.getMap(HzcSC);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      SimpleClassifierPortable portable = new SimpleClassifierPortable();
      SimpleClassifier.Builder builder = SimpleClassifier.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getName(), portable);
      } catch (Exception e) {
        logger.error("ImportSimpleClassifier e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportChatbot(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportChatbot");
    IMap<String, ChatbotPortable> hzcMap = hz.getMap(HzcChatbot);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      ChatbotPortable portable = new ChatbotPortable();
      Chatbot.Builder builder = Chatbot.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getName(), portable);
      } catch (Exception e) {
        logger.error("ImportChatbot e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgentManager(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportDialogAgentManager");

    IMap<String, DialogAgentManagerPortable> hzcMap = hz.getMap(HzcDAManager);
    List<DialogAgentManagerPortable> dams = new ArrayList<>();
    boolean nameCheckPassed = true;

    hzcMap.clear();
    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentManagerPortable portable = new DialogAgentManagerPortable();
      DialogAgentManager.Builder builder = DialogAgentManager.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        String damName = builder.getName();
        if (hzcMap.containsKey(damName)) {
          System.out.println("\n**You may need to delete 'dam.state.json' from ${MAUM_ROOT}/run"
              + " and execute 'svctl restart m2u-dam' on the "
              + damName + "[" + builder.getIp() + ":" + builder.getPort() + "] server.");
          nameCheckPassed = false;
        }
        portable.setProtobufObj(builder.build());
        dams.add(portable);
      } catch (Exception e) {
        System.out.println("Failed to read DAM json.");
        logger.error("ImportDialogAgentManager e : ", e);
        return false;
      }
    }

    if (!nameCheckPassed) {
      return false;
    }

    for (DialogAgentManagerPortable portable : dams) {
      try {
        DialogAgentManager dam = portable.getProtobufObj();
        System.out.println("Call setDamName to "
            + dam.getName() + "[" + dam.getIp() + ":" + dam.getPort() + "]");
        GrpcInterfaceManager client = new GrpcInterfaceManager(dam.getIp(), dam.getPort());
        DamKey.Builder damKey = DamKey.newBuilder();
        damKey.setName(dam.getName());
        DialogAgentManagerStat stat = client.setDamName(damKey.build());
        client.shutdown();
        if (stat.getResultCode() !=
            DialogAgentManagerRegisterResult.DIALOG_AGENT_MANAGER_ADD_SUCCESS) {
          System.out.println("Failed setDamName: " + stat.getDetailMessage()
              + " [" + stat.getResultCode() + "]");
          System.out.println("\n**You may need to delete 'dam.state.json' from ${MAUM_ROOT}/run"
              + " and execute 'svctl restart m2u-dam' on the "
              + dam.getName() + "[" + dam.getIp() + ":" + dam.getPort() + "] server.");
          return false;
        }
        hzcMap.set(dam.getName(), portable);
      } catch (Exception e) {
        System.out.println("Failed setDamName: " + e.getMessage());
        return false;
      }
    }

    return true;
  }

  private static boolean ImportAuthPolicy(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportAuthPolicy");
    IMap<String, AuthticationPolicyPortable> hzcMap = hz.getMap(HzcAuthPolicy);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      AuthticationPolicyPortable portable = new AuthticationPolicyPortable();
      AuthticationPolicy.Builder builder = AuthticationPolicy.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getName(), portable);
      } catch (Exception e) {
        logger.error("ImportAuthPolicy e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgent(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportDialogAgent");
    IMap<String, DialogAgentPortable> hzcMap = hz.getMap(HzcDA);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentPortable portable = new DialogAgentPortable();
      DialogAgent.Builder builder = DialogAgent.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getName(), portable);
      } catch (Exception e) {
        logger.error("ImportDialogAgent e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgentActivation(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportDialogAgentActivation");
    IMap<String, DialogAgentActivationInfoPortable> hzcMap = hz.getMap(HzcDAActivation);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentActivationInfoPortable portable = new DialogAgentActivationInfoPortable();
      DialogAgentActivationInfo.Builder builder = DialogAgentActivationInfo.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getDaName(), portable);
      } catch (Exception e) {
        logger.error("ImportDialogAgentActivation e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportChatbotDetail(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportChatbotDetail");
    IMap<String, ChatbotPortable> hzcMap = hz.getMap(HzcChatbotDetail);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      ChatbotPortable portable = new ChatbotPortable();
      Chatbot.Builder builder = Chatbot.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getName(), portable);
      } catch (Exception e) {
        logger.error("ImportChatbotDetail e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgentInstance(HazelcastInstance hz, JSONObject jsonObject) {
    System.out.println("ImportDialogAgentInstance");
    IMap<String, DialogAgentInstancePortable> hzcMap = hz.getMap(HzcDAInstance);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentInstancePortable portable = new DialogAgentInstancePortable();
      DialogAgentInstance.Builder builder = DialogAgentInstance.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getDaiId(), portable);
      } catch (Exception e) {
        logger.error("ImportDialogAgentInstance e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgentInstanceExec(HazelcastInstance hz,
      JSONObject jsonObject) {
    System.out.println("ImportDialogAgentInstanceExec");
    IMap<String, DialogAgentInstanceExecutionInfoPortable> hzcMap = hz.getMap(HzcDAInstExec);
    //먼저깨끗이 지우자.
    hzcMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentInstanceExecutionInfoPortable portable = new DialogAgentInstanceExecutionInfoPortable();
      DialogAgentInstanceExecutionInfo.Builder builder = DialogAgentInstanceExecutionInfo
          .newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzcMap.set(builder.getDaiId(), portable);
      } catch (Exception e) {
        logger.error("ImportDialogAgentInstanceExec e : ", e);
      }
    }

    return true;
  }

  private static void Import(HazelcastInstance hz, String importFile) {
    System.out.println("*** Import from " + importFile + " ***");
    JSONParser parser = new JSONParser();
    Object obj = null;
    try {
      obj = parser.parse(new FileReader(importFile));
    } catch (IOException e) {
      System.out.println("File reading error.");
      logger.error("Import e : ", e);
    } catch (ParseException e) {
      System.out.println("File parsing error.");
      logger.error("Import e : ", e);
    }
    if (obj == null) {
      System.err.println("import failed");
      return;
    }
    JSONObject jsonObject = (JSONObject) obj;
    JSONObject value = null;

    value = (JSONObject) jsonObject.get(HzcDAManager);
    if (value != null) {
      if (!ImportDialogAgentManager(hz, value)) {
        return;
      }
      ;
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcDA);
    if (value != null) {
      ImportDialogAgent(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcDAActivation);
    if (value != null) {
      ImportDialogAgentActivation(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcSC);
    if (value != null) {
      ImportSimpleClassifier(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcIntentFinderPolicy);
    if (value != null) {
      ImportIntentFinderPolicy(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcIntentFinderInstance);
    if (value != null) {
      ImportIntentFinderInstance(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcDAInstance);
    if (value != null) {
      ImportDialogAgentInstance(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcDAInstExec);
    if (value != null) {
      delay(1500);
      ImportDialogAgentInstanceExec(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcChatbot);
    if (value != null) {
      ImportChatbot(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcChatbotDetail);
    if (value != null) {
      ImportChatbotDetail(hz, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(HzcAuthPolicy);
    if (value != null) {
      ImportAuthPolicy(hz, value);
      delay(500);
    }
    Common.invokeBackupSignal("all");
    System.out.println("*** Import completed from " + importFile + " ***");
  }

  private static JSONObject MakeJsonString(String json) {
    //System.out.println("MakeJsonString=" + json);
    JSONParser parser = new JSONParser();
    Object obj = null;
    try {
      obj = parser.parse(json);
    } catch (Exception e) {
      logger.error("MakeJsonString e : ", e);
    }
    JSONObject jsonObject = (JSONObject) obj;

    return jsonObject;
  }

  private static JSONObject ExportIntentFinderInstance(HazelcastInstance hz) {
    System.out.println("*** ExportIntentFinderInstance ***");
    IMap<String, IntentFinderInstancePortable> itfiMap = hz.getMap(HzcIntentFinderInstance);

    JSONObject root_json = new JSONObject();

    for (String key : itfiMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(itfiMap.get(key).getProtobufObj())));
      } catch (Exception e) {
        logger.error("ExportIntentFinderInstance e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ExportIntentFinderPolicy(HazelcastInstance hz) {
    System.out.println("*** ExportIntentFinderPolicy ***");
    IMap<String, IntentFinderPolicyPortable> hzMap = hz.getMap(HzcIntentFinderPolicy);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));
      } catch (Exception e) {
        logger.error("ExportIntentFinderPolicy e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ExportSimpleClassifier(HazelcastInstance hz) {
    System.out.println("*** ExportSimpleClassifier ***");
    IMap<String, SimpleClassifierPortable> hzMap = hz.getMap(HzcSC);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));
      } catch (Exception e) {
        logger.error("ExportSimpleClassifier e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ExportChatbot(HazelcastInstance hz) {
    System.out.println("*** ExportChatbot ***");
    IMap<String, ChatbotPortable> hzMap = hz.getMap(HzcChatbot);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));
      } catch (Exception e) {
        logger.error("ExportChatbot e : ", e);
      }
    }
    return root_json;
  }


  private static JSONObject ExportDAM(HazelcastInstance hz) {
    System.out.println("*** ExportDAM ***");
    IMap<String, DialogAgentManagerPortable> hzMap = hz.getMap(HzcDAManager);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ExportDAM e : ", e);
      }
    }
    return root_json;
  }


  private static JSONObject ExportDialogAgent(HazelcastInstance hz) {
    System.out.println("*** ExportDialogAgent ***");
    IMap<String, DialogAgentPortable> hzMap = hz.getMap(HzcDA);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ExportDialogAgent e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ExportDialogAgentActivation(HazelcastInstance hz) {
    System.out.println("*** ExportDialogAgentActivation ***");
    IMap<String, DialogAgentActivationInfoPortable> hzMap = hz.getMap(HzcDAActivation);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ExportDialogAgentActivation e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ExportDialogAgentInstance(HazelcastInstance hz) {
    System.out.println("*** ExportDialogAgentInstance ***");
    IMap<String, DialogAgentInstancePortable> hzMap = hz.getMap(HzcDAInstance);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ExportDialogAgentInstance e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ExportDialogAgentInstanceExec(HazelcastInstance hz) {
    System.out.println("*** ExportDialogAgentInstanceExec ***");
    IMap<String, DialogAgentInstanceExecutionInfoPortable> hzMap = hz.getMap(HzcDAInstExec);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ExportDialogAgentInstanceExec e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ExportIntentFinderInstExec(HazelcastInstance hz) {
    System.out.println("*** ExportIntentFinderInstExec ***");
    IMap<String, IntentFinderInstancePortable> hzMap = hz.getMap(HzcIntentFinderInstExec);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key)
                .getProtobufObj())));

      } catch (Exception e) {
        logger.error("ExportIntentFinderInstExec e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ExportDialogAgentInstanceResource(HazelcastInstance hz) {
    System.out.println("*** ExportDialogAgentInstanceResource ***");
    IMap<String, DialogAgentInstanceResourcePortable> hzMap = hz.getMap(HzcDAIR);

    JSONObject root_json = new JSONObject();

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ExportDialogAgentInstanceResource e : ", e);
      }
    }
    return root_json;
  }


  private static void Export(HazelcastInstance hz, String exportFile) {
    System.out.println("*** Export to " + exportFile + " ***");
    JSONObject root_json = new JSONObject();

    ///////
    JSONObject obj = ExportDAM(hz);
    root_json.put(HzcDAManager, obj);

    obj = ExportDialogAgent(hz);
    root_json.put(HzcDA, obj);

    obj = ExportDialogAgentActivation(hz);
    root_json.put(HzcDAActivation, obj);

    obj = ExportSimpleClassifier(hz);
    root_json.put(HzcSC, obj);

    obj = ExportIntentFinderInstance(hz);
    root_json.put(HzcIntentFinderInstance, obj);

    obj = ExportIntentFinderPolicy(hz);
    root_json.put(HzcIntentFinderPolicy, obj);

    obj = ExportChatbot(hz);
    root_json.put(HzcChatbot, obj);

    obj = ExportDialogAgentInstance(hz);
    root_json.put(HzcDAInstance, obj);

    obj = ExportDialogAgentInstanceExec(hz);
    root_json.put(HzcDAInstExec, obj);
    /////////
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    JsonParser jp = new JsonParser();
    JsonElement je = jp.parse(root_json.toString());
    String prettyJsonString = gson.toJson(je);

    FileWriter file = null;
    try {
      file = new FileWriter(exportFile);
      file.write(prettyJsonString);
      file.flush();
      file.close();
      System.out.println("*** Export Completed to " + exportFile + " ***");
    } catch (IOException e) {
      logger.error("Export e : ", e);
    }
  }


  private static JSONObject ViewIntentFinderInstance(HazelcastInstance hz) {
    System.out.println("*** ViewIntentFinderInstance ***");
    IMap<String, IntentFinderInstancePortable> itfiMap = hz.getMap(HzcIntentFinderInstance);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put("proto", MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new IntentFinderInstancePortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewIntentFinderInstance e : ", e);
    }
    for (String key : itfiMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(itfiMap.get(key).getProtobufObj())));
      } catch (Exception e) {
        logger.error("ViewIntentFinderInstance e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewIntentFinderPolicy(HazelcastInstance hz) {
    System.out.println("*** ViewIntentFinderPolicy ***");
    IMap<String, IntentFinderPolicyPortable> hzMap = hz.getMap(HzcIntentFinderPolicy);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put("proto", MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new IntentFinderPolicyPortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewIntentFinderPolicy e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));
      } catch (Exception e) {
        logger.error("ViewIntentFinderPolicy e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewSimpleClassifier(HazelcastInstance hz) {
    System.out.println("*** ViewSimpleClassifier ***");
    IMap<String, SimpleClassifierPortable> hzMap = hz.getMap(HzcSC);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put("proto", MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields().print((new SimpleClassifierPortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewSimpleClassifier e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));
      } catch (Exception e) {
        logger.error("ViewSimpleClassifier e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewChatbot(HazelcastInstance hz) {
    System.out.println("*** ViewChatbot ***");

    IMap<String, ChatbotPortable> hzMap = hz.getMap(HzcChatbot);

    JSONObject root_json = new JSONObject();

    try {
      root_json.put(HzcChatbot, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields().print((new ChatbotPortable()).getProtobufObj())));
    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewChatbot e : ", e);
    }

    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));
      } catch (Exception e) {
        logger.error("ViewChatbot e : ", e);
      }
    }
    return root_json;
  }


  private static JSONObject ViewDAM(HazelcastInstance hz) {
    System.out.println("*** ViewDAM ***");
    IMap<String, DialogAgentManagerPortable> hzMap = hz.getMap(HzcDAManager);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put(HzcDAManager, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new DialogAgentManagerPortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewDAM e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ViewDAM e : ", e);
      }
    }
    return root_json;
  }


  private static JSONObject ViewDialogAgent(HazelcastInstance hz) {
    System.out.println("*** ViewDialogAgent ***");
    IMap<String, DialogAgentPortable> hzMap = hz.getMap(HzcDA);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put(HzcDA, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields().print((new DialogAgentPortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewDialogAgent e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ViewDialogAgent e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewDialogAgentActivation(HazelcastInstance hz) {
    System.out.println("*** ViewDialogAgentActivation ***");
    IMap<String, DialogAgentActivationInfoPortable> hzMap = hz.getMap(HzcDAActivation);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put(HzcDAActivation, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new DialogAgentActivationInfoPortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewDialogAgentActivation e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ViewDialogAgentActivation e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewChatbotDetail(HazelcastInstance hz) {
    System.out.println("*** ViewChatbotDetail ***");
    IMap<String, DialogAgentInstancePortable> hzMap = hz.getMap(HzcChatbotDetail);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put(HzcChatbotDetail, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new DialogAgentInstancePortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewChatbotDetail e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields().print(hzMap.get(key)
                .getProtobufObj())));

      } catch (Exception e) {
        logger.error("ViewChatbotDetail e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewDialogAgentInstance(HazelcastInstance hz) {
    System.out.println("*** ViewDialogAgentInstance ***");
    IMap<String, DialogAgentInstancePortable> hzMap = hz.getMap(HzcDAInstance);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put(HzcDAInstance, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new DialogAgentInstancePortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewDialogAgentInstance e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ViewDialogAgentInstance e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewDialogAgentInstanceExec(HazelcastInstance hz) {
    System.out.println("*** ViewDialogAgentInstanceExec ***");
    IMap<String, DialogAgentInstanceExecutionInfoPortable> hzMap = hz.getMap(HzcDAInstExec);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put(HzcDAInstExec, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new DialogAgentInstanceExecutionInfoPortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewDialogAgentInstanceExec e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ViewDialogAgentInstanceExec e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewIntentFinderInstExec(HazelcastInstance hz) {
    System.out.println("*** ViewIntentFinderInstExec ***");
    IMap<String, IntentFinderInstancePortable> hzMap = hz.getMap(HzcIntentFinderInstExec);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put(HzcIntentFinderInstExec, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new IntentFinderInstancePortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewIntentFinderInstExec e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields().print(hzMap.get(key)
                .getProtobufObj())));

      } catch (Exception e) {
        logger.error("ViewIntentFinderInstExec e : ", e);
      }
    }
    return root_json;
  }

  private static JSONObject ViewDialogAgentInstanceResource(HazelcastInstance hz) {
    System.out.println("*** ViewDialogAgentInstanceResource ***");
    IMap<String, DialogAgentInstanceResourcePortable> hzMap = hz.getMap(HzcDAIR);

    JSONObject root_json = new JSONObject();
    try {
      root_json.put(HzcDAIR, MakeJsonString(JsonFormat.printer()
          .includingDefaultValueFields()
          .print((new DialogAgentInstanceResourcePortable()).getProtobufObj())));

    } catch (InvalidProtocolBufferException e) {
      logger.error("ViewDialogAgentInstanceResource e : ", e);
    }
    for (String key : hzMap.keySet()) {
      try {
        root_json.put(key,
            MakeJsonString(JsonFormat.printer().includingDefaultValueFields()
                .print(hzMap.get(key).getProtobufObj())));

      } catch (Exception e) {
        logger.error("ViewDialogAgentInstanceResource e : ", e);
      }
    }
    return root_json;
  }


  private static void View(HazelcastInstance hz) {
    JSONObject root_json = new JSONObject();

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    JsonParser jp = new JsonParser();

    ///////
    JSONObject obj = null;
    obj = ViewChatbot(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewIntentFinderInstance(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewIntentFinderPolicy(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewSimpleClassifier(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewDAM(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewDialogAgent(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewDialogAgentActivation(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewDialogAgentInstance(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewDialogAgentInstanceExec(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }

    obj = ViewDialogAgentInstanceResource(hz);
    {
      JsonElement je = jp.parse(obj.toString());
      String prettyJsonString = gson.toJson(je);
      System.out.println(prettyJsonString);
    }
  }

  public static void main(String[] args) throws IOException {
    Properties properties = new Properties();

    String conf = getSystemConfPath("/etc/m2u.conf");
    properties.load(new FileInputStream(conf));

    String type = "";
    String path = getSystemConfPath("/etc/dump");
    String dbtable = "";

    if (args.length == 0) {
      System.err.println("Please enter options");
      System.exit(1);
    }

    // 옵션 개수 출력
    System.out.println(args.length + " options were entered.");

    System.out.println(); // 줄바꿈

    // 모든 옵션 하나씩 화면에 출력
    for (int i = 0; i < args.length; i++) {
      System.out.format("args[%d] : %s%n", i, args[i]);
      switch (i) {
        case 0:
          type = args[i];
          break;
        case 1:
          path = args[i];
          break;
        case 2:
          dbtable = args[i];
          break;
      }
    }

    try {
      List<String> addrList = new ArrayList<>();
      String address = properties.getProperty("hazelcast.server.ip.1");
      String port = properties.getProperty("hazelcast.server.port");

      if (address != null && !"".equals(address) && port != null && !"".equals(port)) {
        addrList.add(address.concat(":").concat(port));
      }

      ClientConfig clientConfig = new ClientConfig();
      clientConfig.setGroupConfig(
          new GroupConfig(properties.getProperty("hazelcast.group.name"),
              properties.getProperty("hazelcast.group.password")));
      clientConfig.getNetworkConfig().setAddresses(addrList);

      clientConfig.getSerializationConfig()
          .addPortableFactory(1, new ServiceAdminPortableFactory());
      clientConfig.getSerializationConfig()
          .addPortableFactory(2, new ServiceAdminPortableFactory());
      clientConfig.getSerializationConfig()
          .addPortableFactory(3, new ServiceAdminPortableFactory());
      HazelcastInstance hz = HazelcastClient.newHazelcastClient(clientConfig);

      System.out.println(
          "==================================================================");
      System.out
          .println("       HazelcastController " + type + " call " + dbtable);
      System.out.println(
          "==================================================================");

      if ("import".equals(type)) {
        Import(hz, path);
      } else if ("export".equals(type)) {
        Export(hz, path);
      } else if ("view".equals(type)) {
        View(hz);
      }
      System.exit(1);
    } catch (Exception e) {
      logger.error("main e : ", e);
    }
  }
}
