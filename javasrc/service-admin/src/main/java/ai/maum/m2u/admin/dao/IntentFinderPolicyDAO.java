package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.IntentFinderPolicyPortable;
import com.hazelcast.core.IMap;
import java.util.Map.Entry;
import maum.m2u.console.Classifier.IntentFinderPolicyList;
import maum.m2u.console.Da.Key;
import maum.m2u.router.v3.Intentfinder.IntentFinderPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntentFinderPolicyDAO {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderPolicyDAO.class);

  private IMap<String, IntentFinderPolicyPortable> intentFinderPolicyPortableIMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.INTENT_FINDER_POLICY);

  /**
   * Hazelcast에서 IntentFinderPolicy를 전체 조회하여 IntentFinderPolicyList에 넣는다. 조회한
   * IntentFinderPolicyList를 리턴한다.
   *
   * @return {IntentFinderPolicyList.Builder}
   */
  public IntentFinderPolicyList.Builder getIntentFinderPolicyAllList() {
    logger.info("[m2u-svcadm-dao] getIntentFinderPolicyAllList");

    IntentFinderPolicyList.Builder ifplb = IntentFinderPolicyList.newBuilder();

    for (Entry<String, IntentFinderPolicyPortable> ifp :
        this.intentFinderPolicyPortableIMap.entrySet()) {
      if (ifp != null) {
        IntentFinderPolicy.Builder ifpb = ifp.getValue().getProtobufObj().toBuilder();
        ifplb.addItfPolicyList(ifpb);
      }
    }
    return ifplb;
  }

  /**
   * HazelCast에서 Name으로 IntentFinderPolicy 정보 조회
   *
   * @param req => {Key} Key는 IntentFinderPolicy name 이다.
   * @return {IntentFinderPolicy.Builder}
   */
  public IntentFinderPolicy.Builder getIntentFinderPolicy(Key req) {
    logger.info("[m2u-svcadm-dao] getIntentFinderPolicy");
    logger.trace("[m2u-svcadm-dao] getIntentFinderPolicy = {}", req.getName());

    IntentFinderPolicyPortable ifpp = this.intentFinderPolicyPortableIMap.get(req.getName());
    if (ifpp == null) {
      return null;
    }
    return ifpp.getProtobufObj().toBuilder();
  }

  /**
   * Hazelcast에 IntentFinderPolicy를 insert
   *
   * @param req => {IntentFinderPolicy 객체}
   */
  public void insertIntentFinderPolicy(IntentFinderPolicy req) {
    logger.info("[m2u-svcadm-dao] insertIntentFinderPolicy");
    logger.trace("[m2u-svcadm-dao] insertIntentFinderPolicy = {}", req);

    IntentFinderPolicyPortable ifp = new IntentFinderPolicyPortable();
    ifp.setProtobufObj(req);

    this.intentFinderPolicyPortableIMap.set(ifp.getProtobufObj().getName(), ifp);
    Common.invokeBackupSignal(MAP.INTENT_FINDER_POLICY);
  }

  /**
   * Hazelcast에 IntentFinderPolicy를 update
   *
   * @param req => {IntentFinderPolicy 객체}
   */
  public void updateIntentFinderPolicy(IntentFinderPolicy req) {
    logger.info("[m2u-svcadm-dao] updateIntentFinderPolicy");
    logger.trace("[m2u-svcadm-dao] updateIntentFinderPolicy = {}", req);

    IntentFinderPolicyPortable ifp = new IntentFinderPolicyPortable();
    ifp.setProtobufObj(req);
    this.intentFinderPolicyPortableIMap.set(ifp.getProtobufObj().getName(), ifp);
    Common.invokeBackupSignal(MAP.INTENT_FINDER_POLICY);
  }

  /**
   * HazelCast에 Name으로 IntentFinderPolic 삭제
   *
   * @param req => {Key} Key는 IntentFinderPolicy name 이다.
   */
  public void deleteIntentFinderPolicy(Key req) {
    logger.info("[m2u-svcadm-dao] deleteIntentFinderPolicy");
    logger.trace("[m2u-svcadm-dao] deleteIntentFinderPolicy = {}", req.getName());
    
    this.intentFinderPolicyPortableIMap.delete(req.getName());
    Common.invokeBackupSignal(MAP.INTENT_FINDER_POLICY);
  }

}
