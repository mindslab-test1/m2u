package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.SimpleClassifierPortable;
import ai.maum.m2u.common.portable.SkillPortable;
import com.google.protobuf.Timestamp;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import io.grpc.Status;
import io.grpc.StatusException;
import java.util.Collection;
import java.util.Map.Entry;
import maum.m2u.console.Classifier.SimpleClassifier;
import maum.m2u.console.Classifier.SimpleClassifier.Builder;
import maum.m2u.console.Classifier.SimpleClassifier.ListSkill;
import maum.m2u.console.Classifier.SimpleClassifierList;
import maum.m2u.console.Da.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleClassifierDAO {

  static final Logger logger = LoggerFactory.getLogger(SimpleClassifierDAO.class);

  private IMap<String, SimpleClassifierPortable> SimpleClassifierMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.SIMPLE_CLASSIFIER);

  private IMap<String, SkillPortable> SkillMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.SKILL);

  public void insertSimpleClassifier(SimpleClassifier req) throws Exception {
    logger.info("[m2u-svcadm-dao] insertSimpleClassifier");
    logger.trace("[m2u-svcadm-dao] insertSimpleClassifier = {}", req);

    SimpleClassifierPortable scp = new SimpleClassifierPortable();
    SimpleClassifier.Builder sc = req.toBuilder();

    long millis = System.currentTimeMillis();

    Timestamp now = Timestamp.newBuilder().setSeconds(millis / 1000)
        .setNanos((int) ((millis % 1000) * 1000000)).build();

    sc.setCreateAt(now);
    sc.setModifyAt(now);

    for (ListSkill entry : req.getSkillListList()) {
      SkillPortable sc1 = new SkillPortable();

      sc1.setScName(req.getName());

      // sc1.setName(entry.getName());

      sc1.setProtobufObj(entry);

      // SimpleClassifier 이름 중복 체크
      Collection<SimpleClassifierPortable> scpc = this.SimpleClassifierMap
          .values(new SqlPredicate("name = '" + req.getName() + "'"));
      if (!scpc.isEmpty()) {
        logger.error("simple classifier name already exists!");
        throw new StatusException(
            Status.ALREADY_EXISTS.withDescription("simple classifier name already exists!"));
      }
      //String key = sc.getName() + "_" + sc1.getProtobufObj().getName();
      String key = sc.getName() + "_" + entry.getName();

      this.SkillMap.set(key, sc1);
    }

    scp.setProtobufObj(sc.build());
    this.SimpleClassifierMap.set(req.getName(), scp);

    Common.invokeBackupSignal(MAP.SKILL);
    Common.invokeBackupSignal(MAP.SIMPLE_CLASSIFIER);
  }

  public void updateSimpleClassifier(SimpleClassifier req) {
    logger.info("[m2u-svcadm-dao] updateSimpleClassifier");
    logger.trace("[m2u-svcadm-dao] updateSimpleClassifier = {}", req);

    SimpleClassifierPortable scp = this.SimpleClassifierMap.get(req.getName());
    SimpleClassifier.Builder sc = req.toBuilder();

    logger.debug("sc is null : {}", scp == null);

    long millis = System.currentTimeMillis();
    Timestamp now = Timestamp.newBuilder().setSeconds(millis / 1000)
        .setNanos((int) ((millis % 1000) * 1000000)).build();

    logger.debug("now : {}", now.toString());
    sc.setModifyAt(now);

    for (ListSkill entry : req.getSkillListList()) {
      SkillPortable sc1 = new SkillPortable();
      sc1.setScName(req.getName());

      sc1.setProtobufObj(entry);

      String key = req.getName() + "_" + sc1.getProtobufObj().getName();

      this.SkillMap.set(key, sc1);
    }

    scp.setProtobufObj(sc.build());
    this.SimpleClassifierMap.set(req.getName(), scp);

    Common.invokeBackupSignal(MAP.SKILL);
    Common.invokeBackupSignal(MAP.SIMPLE_CLASSIFIER);
  }

  public void delSimpleClassifier(Key req) {
    logger.info("[m2u-svcadm-dao] delSimpleClassifier");
    logger.trace("[m2u-svcadm-dao] delSimpleClassifier = {}", req.getName());

    this.SimpleClassifierMap.delete(req.getName());
    Collection<SkillPortable> result = this.SkillMap
        .values(new SqlPredicate("sc_name='" + req.getName() + "'"));

    for (SkillPortable params : result) {
      String key = params.getScName() + "_" + params.getProtobufObj().getName();

      this.SkillMap.delete(key);
    }

    Common.invokeBackupSignal(MAP.SKILL);
    Common.invokeBackupSignal(MAP.SIMPLE_CLASSIFIER);
  }

  public Builder getSimpleClassifier(Key req) {
    logger.info("[m2u-svcadm-dao] getSimpleClassifier");
    logger.trace("[m2u-svcadm-dao] getSimpleClassifier = {}", req.getName());

    Builder sc = SimpleClassifier.newBuilder();

    SimpleClassifierPortable params = this.SimpleClassifierMap.get(req.getName());

    if (params != null) {
      sc.setName(params.getProtobufObj().getName());
      sc.setLangValue(params.getProtobufObj().getLangValue());
      sc.setDescription(params.getProtobufObj().getDescription());

      Collection<SkillPortable> result = this.SkillMap
          .values(new SqlPredicate("sc_name='" + req.getName() + "%'"));

      for (SkillPortable params2 : result) {
        sc.addSkillList(params2.getProtobufObj().toBuilder());
      }

    }

    return sc;
  }

  public SimpleClassifierList.Builder getSimpleClassifierAllList() {
    logger.info("[m2u-svcadm-dao] getSimpleClassifierAllList");
    
    SimpleClassifierList.Builder scl = SimpleClassifierList.newBuilder();

    for (Entry<String, SimpleClassifierPortable> entry : this.SimpleClassifierMap.entrySet()) {
      logger.debug("entry.getKey() {}", entry.getKey());
      SimpleClassifier.Builder sc = entry.getValue().getProtobufObj().toBuilder();

      Collection<SkillPortable> result = this.SkillMap.values(
          new SqlPredicate("sc_name='" + entry.getValue().getProtobufObj().getName() + "'"));
      int regCnt = 0;
      for (SkillPortable params2 : result) {
        regCnt = regCnt + (params2.getProtobufObj().getRegexCount());

      }

      sc.setSkillCnt(result.size());
      sc.setRegCnt(regCnt);

      sc.setName(String.valueOf(entry.getKey()));

      scl.addScList(sc);
    }
    return scl;
  }
}