package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.DialogAgentDAO;
import ai.maum.m2u.admin.dao.DialogAgentInstanceDAO;
import ai.maum.m2u.admin.dao.DialogAgentManagerDAO;
import ai.maum.m2u.admin.itfc.GrpcInterfaceManager;
import com.google.protobuf.Empty;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;

import java.util.HashMap;
import java.util.HashSet;

import maum.m2u.common.Userattr.UserAttribute;
import maum.m2u.common.Userattr.UserAttributeList;
import maum.m2u.console.Da.DialogAgentActivationInfo;
import maum.m2u.console.Da.DialogAgentExecutable;
import maum.m2u.console.Da.DialogAgentExecutableList;
import maum.m2u.console.Da.DialogAgentInfo;
import maum.m2u.console.Da.DialogAgentList;
import maum.m2u.console.Da.DialogAgentManager;
import maum.m2u.console.Da.DialogAgentManagerList;
import maum.m2u.console.Da.DialogAgentManagerWithDialogAgentInstance;
import maum.m2u.console.Da.DialogAgentWithDialogAgentInstance;
import maum.m2u.console.Da.DialogAgentWithDialogAgentInstance.Builder;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.ResultList;
import maum.m2u.console.DialogAgentAdminGrpc;
import maum.m2u.da.Provider.RuntimeParameter;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.server.Dam.DialogAgentExecutables.DialogAgentSpec;
import maum.m2u.server.Dam.DialogAgentName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DialogAgentAdminImpl extends DialogAgentAdminGrpc.DialogAgentAdminImplBase {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentAdminImpl.class);

  static DialogAgentDAO daDao = new DialogAgentDAO();
  static DialogAgentManagerDAO damDao = new DialogAgentManagerDAO();
  static DialogAgentInstanceDAO daiDao = new DialogAgentInstanceDAO();

  @Override
  public void getDialogAgentActivationInfoList(Key req,
                                               StreamObserver<DialogAgentManagerList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentActivationInfoList");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentActivationInfoList = {}", req.getName());

    try {
      DialogAgentManagerList.Builder damlb = damDao.getDialogAgentManagerList(req);

      res.onNext(damlb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }

  }

  @Override
  public void getDialogAgentAllList(Empty req, StreamObserver<DialogAgentList> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentAllList");

    try {
      DialogAgentList.Builder dalb = daDao.getDialogAgentList();

      res.onNext(dalb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }
  }

  @Override
  public void getDailogAgentListToDamFilter(Key req, StreamObserver<DialogAgentList> res) {
    logger.info("[m2u-svcadm-grpc] getDailogAgentListToDamFilter");
    logger.trace("[m2u-svcadm-grpc] getDailogAgentListToDamFilter = {}", req.getName());

    try {
      DialogAgentList.Builder dailb = daDao.getDialogAgentListToDamFilter(req);

      res.onNext(dailb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }
  }

  @Override
  public void getDialogAgentInfo(Key req, StreamObserver<DialogAgentWithDialogAgentInstance> res) {
    logger.info("[m2u-svcadm-grpc] getDialogAgentInfo");
    logger.trace("[m2u-svcadm-grpc] getDialogAgentInfo = {}", req.getName());

    try {
      Builder dawdaib = daDao.getDialogAgentInfo(req);

      res.onNext(dawdaib.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }


  }

  @Override
  public void insertDialogAgentInfo(DialogAgentInfo req, StreamObserver<DialogAgentInfo> res) {
    logger.info("[m2u-svcadm-grpc] insertDialogAgentInfo");
    logger.trace("[m2u-svcadm-grpc] insertDialogAgentInfo = {}", req);

    try {
      // DA와 DAAINFO를 등록을하면 DAM에서 DAAINFO의 변경 사항을 Listen하여 DA 활성화가 진행 됨
      // DA 등록
      DialogAgentInfo.Builder daib = DialogAgentInfo.newBuilder();
      daDao.insertDialogAgent(req.getDaInfo());

      // DAAINFO 등록
      for (DialogAgentActivationInfo daai : req.getDaaiListList()) {
        DialogAgentActivationInfo.Builder daaib = daai.toBuilder();
        daDao.insertDialogAgentActivationInfo(daaib.build());
      }

      res.onNext(daib.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }
  }

  @Override
  public void updateDialogAgentInfo(DialogAgentInfo req, StreamObserver<DialogAgentInfo> res) {
    logger.info("[m2u-svcadm-grpc] updateDialogAgentInfo");
    logger.trace("[m2u-svcadm-grpc] updateDialogAgentInfo = {}", req);

    try {
      DialogAgentInfo.Builder daib = DialogAgentInfo.newBuilder();

      daDao.updateDialogAgent(req.getDaInfo());

      for (DialogAgentActivationInfo daai : req.getDaaiListList()) {
        daDao.updateDialogAgentActivationInfo(daai);
      }

      res.onNext(daib.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }
  }

  @Override
  public void deleteDialogAgent(KeyList req, StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteDialogAgent");
    logger.trace("[m2u-svcadm-grpc] deleteDialogAgent = {}", req);

    try {
      // delDialogAgentList service에서 DAAINFO의 변경을 Listen하여 DA 등록 취소가 진행됨
      // DA 삭제
      ResultList.Builder rlb = daDao.delDialogAgentList(req);

      res.onNext(rlb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }
  }

  @Override
  public void getRuntimeParameters(DialogAgentName req, StreamObserver<RuntimeParameterList> res) {
    logger.info("[m2u-svcadm-grpc] getRuntimeParameters");
    logger.trace("[m2u-svcadm-grpc] getRuntimeParameters = {}", req);

    try {

      DialogAgentManagerList.Builder damlb = damDao.getDialogAgentManagerAllList();
      RuntimeParameterList.Builder rplb = RuntimeParameterList.newBuilder();
      HashSet<RuntimeParameter> distinctData = new HashSet<RuntimeParameter>();

      for (DialogAgentManagerWithDialogAgentInstance damwdai : damlb.getDamwdListList()) {
        GrpcInterfaceManager client = new GrpcInterfaceManager(damwdai.getDamInfo().getIp(),
            damwdai.getDamInfo().getPort());

        logger.info("client : {}:{}",
            damwdai.getDamInfo().getIp(), damwdai.getDamInfo().getPort());
        RuntimeParameterList rp_list = client.getRuntimeParameters(req);
        logger.debug("rp_list : {}", rp_list);

        client.shutdown();
        if (rp_list != null) {
          for (RuntimeParameter rp : rp_list.getParamsList()) {
            if (!distinctData.contains(rp)) {
              rplb.addParams(rp);
              distinctData.add(rp);
            }
          }
        }
      }
      logger.debug("distinctData : {}", distinctData);

      res.onNext(rplb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }
  }

  @Override
  public void getUserAttributes(DialogAgentName req, StreamObserver<UserAttributeList> res) {
    logger.info("[m2u-svcadm-grpc] getUserAttributes");
    logger.trace("[m2u-svcadm-grpc] request = {}", req);

    try {

      DialogAgentManagerList.Builder damlb = damDao.getDialogAgentManagerAllList();
      UserAttributeList.Builder ualb = UserAttributeList.newBuilder();
      HashSet<UserAttribute> distinctData = new HashSet<UserAttribute>();
      logger.debug("{} {}", damlb, damlb.getDamwdListList());

      for (DialogAgentManagerWithDialogAgentInstance damwdai : damlb.getDamwdListList()) {
        GrpcInterfaceManager client = new GrpcInterfaceManager(damwdai.getDamInfo().getIp(),
            damwdai.getDamInfo().getPort());

        logger.info("client : {}:{}",
            damwdai.getDamInfo().getIp(), damwdai.getDamInfo().getPort());
        UserAttributeList ua_list = client.getUserAttributes(req);
        logger.debug("ua_list : {}", ua_list);

        client.shutdown();

        for (UserAttribute ua : ua_list.getAttrsList()) {
          if (!distinctData.contains(ua)) {
            ualb.addAttrs(ua);
            distinctData.add(ua);
          }
        }
      }
      logger.debug("distinctData : {}", distinctData);

      res.onNext(ualb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }
  }

  @Override
  public void getExecutableDA(Empty req, StreamObserver<DialogAgentExecutableList> res) {
    logger.info("[m2u-svcadm-grpc] getExecutableDA");

    try {
      DialogAgentManagerList.Builder damlb = damDao.getDialogAgentManagerAllList();
      DialogAgentExecutableList.Builder daelb = DialogAgentExecutableList.newBuilder();
      HashMap<String, String> exDa = new HashMap<String, String>();

      for (DialogAgentManagerWithDialogAgentInstance damwdai : damlb.getDamwdListList()) {
        DialogAgentManager damInfo = damwdai.getDamInfo();
        GrpcInterfaceManager client = new GrpcInterfaceManager(damInfo.getIp(), damInfo.getPort());

        for (DialogAgentSpec rp : client.getExecutableDA(req).getExecutableDasList()) {
          String daName = rp.getDaName();
          if (exDa.get(daName) == null) {
            DialogAgentExecutable.Builder daeb = DialogAgentExecutable.newBuilder();
            exDa.put(daName, damInfo.getName());

            daeb.setName(daName);
            daeb.addDistributions(damInfo.getName());
            daeb.setRuntimeEnv(rp.getRuntimeEnv());

            daelb.addDaeList(daeb.build());
          } else {
            for (DialogAgentExecutable.Builder daeb : daelb.getDaeListBuilderList()) {
              if (daeb.getName().equals(daName)) {
                daeb.addDistributions(damInfo.getName());
                daeb.setRuntimeEnv(rp.getRuntimeEnv());
              }
            }
          }
        }
        client.shutdown();
      }
      //GrpcInterfaceManager client = new GrpcInterfaceManager("10.122.65.155", 9907);

      //DialogAgentExecutables dan = client.getExecutableDA(req);

      res.onNext(daelb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(new StatusException(Status.INTERNAL
                                          .withDescription(e.getMessage())
                                          .withCause(e)));
    }
  }
}

