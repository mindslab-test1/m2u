package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.IntentFinderInstancePortable;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import io.grpc.Status;
import io.grpc.StatusException;
import java.util.Collection;
import java.util.Map.Entry;
import maum.m2u.console.Classifier.IntentFinderInstanceList;
import maum.m2u.console.Da.Key;
import maum.m2u.router.v3.Intentfinder.IntentFinderInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntentFinderInstanceDAO {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderDAO.class);

  private IMap<String, IntentFinderInstancePortable> IntentFinderInstanceMap = HazelcastConnector
      .getInstance().client
      .getMap(MAP.INTENT_FINDER_INSTANCE);

  /**
   * HazelCast에서 ITF Instance List 조회
   *
   * @return IntentFinderInstance List 객체
   */
  public IntentFinderInstanceList.Builder getIntentFinderInstanceList() {
    logger.info("[m2u-svcadm-dao] getIntentFinderInstanceList");
    
    IntentFinderInstanceList.Builder ifilb = IntentFinderInstanceList.newBuilder();

    for (Entry<String, IntentFinderInstancePortable> ifip :
        this.IntentFinderInstanceMap.entrySet()) {
      if (ifip != null) {
        IntentFinderInstance.Builder ifib = ifip.getValue().getProtobufObj().toBuilder();
        ifilb.addItfiList(ifib);
      }
    }

    return ifilb;
  }

  /**
   * HazelCast에서 Name으로 ITF Instance 정보 조회
   *
   * @param req IntentFinderInstance의 Name 값
   * @return IntentFinderInstance 객체
   */
  public IntentFinderInstance.Builder getIntentFinderInstance(Key req) {
    logger.info("[m2u-svcadm-dao] getIntentFinderInstance");
    logger.trace("[m2u-svcadm-dao] getIntentFinderInstance = {}", req.getName());

    IntentFinderInstancePortable ifip = this.IntentFinderInstanceMap.get(req.getName());
    if (ifip == null) {
      return null;
    }
    return ifip.getProtobufObj().toBuilder();
  }

  /**
   * HazelCast에 ITF Instance Insert
   *
   * @param req IntentFinderInstance 객체
   */
  public void insertIntentFinderInstance(IntentFinderInstance req) throws Exception {
    logger.info("[m2u-svcadm-dao] insertIntentFinderInstance");
    logger.trace("[m2u-svcadm-dao] insertIntentFinderInstance = {}", req);

    IntentFinderInstancePortable ifip = new IntentFinderInstancePortable();
    ifip.setProtobufObj(req);

    // IntentFinder Instance 이름 중복 체크
    Collection<IntentFinderInstancePortable> itfpc = this.IntentFinderInstanceMap
        .values(new SqlPredicate("name = '" + req.getName() + "'"));
    if (!itfpc.isEmpty()) {
      logger.error("IntentFinder Instance name already exists!");
      throw new StatusException(
          Status.ALREADY_EXISTS.withDescription("IntentFinder Instance name already exists!"));
    }

    // IntentFinder Instance Port 번호 중복 체크
    // Sever는 로컬IP로 가정하여 Port 비교대상에서 제외.
    Collection<IntentFinderInstancePortable> itfpc2 = this.IntentFinderInstanceMap
        .values(new SqlPredicate("port = '" + req.getPort() + "' and ip = '" + req.getIp() + "'"));
    if (!itfpc2.isEmpty()) {
      logger.error("The port already exists on that Ip address");
      throw new StatusException(
          Status.ALREADY_EXISTS.withDescription("The port already exists on that Ip address"));
    }

    this.IntentFinderInstanceMap.set(ifip.getProtobufObj().getName(), ifip);
    Common.invokeBackupSignal(MAP.INTENT_FINDER_INSTANCE);
  }

  /**
   * HazalCast에 ITF Instance Update
   *
   * @param req IntentFinderInstance 객체
   */
  public void updateIntentFinderInstance(IntentFinderInstance req) throws Exception {
    logger.info("[m2u-svcadm-dao] updateIntentFinderInstance");
    logger.trace("[m2u-svcadm-dao] updateIntentFinderInstance = {}", req);

    IntentFinderInstancePortable ifip = new IntentFinderInstancePortable();
    ifip.setProtobufObj(req);

    Collection<IntentFinderInstancePortable> itfp = this.IntentFinderInstanceMap
        .values(new SqlPredicate("name = '" + req.getName() + "'"));
    int req_port = req.getPort();
    int port = 0;
    String req_ip = req.getIp();
    String ip = "";
    for (IntentFinderInstancePortable i : itfp) {
      port = i.getProtobufObj().getPort();
      ip = i.getProtobufObj().getIp();
      break;
    }
    // IntentFinder Instance Port 번호 중복 체크
    // Sever는 로컬IP로 가정하여 Port 비교대상에서 제외.
    if (port != req_port || !ip.equals(req_ip)) {
      String sql = " port = '" + req_port + "' and ip = '" + req_ip + "'";
      Collection<IntentFinderInstancePortable> itfpc = this.IntentFinderInstanceMap
          .values(new SqlPredicate(sql));
      if (!itfpc.isEmpty()) {
        logger.error("The port already exists on that Ip address!");
        throw new StatusException(
            Status.ALREADY_EXISTS.withDescription("The port already exists on that Ip address!"));
      }
    }
    this.IntentFinderInstanceMap.set(ifip.getProtobufObj().getName(), ifip);
    Common.invokeBackupSignal(MAP.INTENT_FINDER_INSTANCE);
  }


  /**
   * HazelCast에 Name으로 ITF Instance 삭제
   *
   * @param req IntentFinderInstance의 Name 값
   */
  public void deleteIntentFinderInstance(Key req) {
    logger.info("[m2u-svcadm-dao] deleteIntentFinderInstance");
    logger.trace("[m2u-svcadm-dao] deleteIntentFinderInstance = {}", req.getName());
    
    this.IntentFinderInstanceMap.delete(req.getName());
    Common.invokeBackupSignal(MAP.INTENT_FINDER_INSTANCE);
  }

}
