package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.DialogAgentActivationInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceExecutionInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentManagerPortable;
import com.google.protobuf.Timestamp;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import java.util.Collection;
import java.util.Map.Entry;
import maum.m2u.console.Da.DialogAgentManager;
import maum.m2u.console.Da.DialogAgentManager.Builder;
import maum.m2u.console.Da.DialogAgentManagerList;
import maum.m2u.console.Da.DialogAgentManagerWithDialogAgentInstance;
import maum.m2u.console.Da.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DialogAgentManagerDAO {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentManagerDAO.class);

  private IMap<String, DialogAgentManagerPortable> DialogAgentManagerMap
      = HazelcastConnector.getInstance().client.getMap(MAP.DIALOG_AGENT_MANAGER);

  //DAMname, DAname, isActive
  private IMap<String, DialogAgentActivationInfoPortable> DialogAgentActivationInfoMap
      = HazelcastConnector.getInstance().client.getMap(MAP.DIALOG_AGENT_ACTIVATION);

  private IMap<String, DialogAgentInstanceExecutionInfoPortable> DialogAgentInstanceExecutionInfoMap
      = HazelcastConnector.getInstance().client.getMap(MAP.DIALOG_AGENT_INSTANCE_EXECUTION);

  public boolean checkIpPort(DialogAgentManager req) {
    logger.info("[m2u-svcadm-dao] checkIpPort");
    logger.trace("[m2u-svcadm-dao] checkIpPort = {}", req);


    Collection<DialogAgentManagerPortable> result = this.DialogAgentManagerMap
        .values(new SqlPredicate("name != '" + req.getName()
            + "' and ip = '" + req.getIp()
            + "' and port = " + req.getPort()));

    if (result.size() > 0) {
      return true;
    }
    return false;
  }

  public void insertDialogAgentManager(DialogAgentManager req) {
    logger.info("[m2u-svcadm-dao] insertDialogAgentManager");
    logger.trace("[m2u-svcadm-dao] insertDialogAgentManager = {}", req);

    DialogAgentManagerPortable damp = new DialogAgentManagerPortable();
    DialogAgentManager.Builder dam = req.toBuilder();

    long millis = System.currentTimeMillis();

    Timestamp now = Timestamp.newBuilder().setSeconds(millis / 1000)
        .setNanos((int) ((millis % 1000) * 1000000)).build();

    dam.setCreateAt(now);
    dam.setModifyAt(now);
    damp.setProtobufObj(dam.build());

    this.DialogAgentManagerMap.set(req.getName(), damp);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_MANAGER);
  }

  public Builder updateDialogAgentManager(DialogAgentManager req) {
    logger.info("[m2u-svcadm-dao] updateDialogAgentManager");
    logger.trace("[m2u-svcadm-dao] updateDialogAgentManager = {}", req);

    DialogAgentManagerPortable damp = this.DialogAgentManagerMap.get(req.getName());
    DialogAgentManager.Builder dam = req.toBuilder();

    if (damp == null) {
      return null;
    }

    long millis = System.currentTimeMillis();

    Timestamp now = Timestamp.newBuilder().setSeconds(millis / 1000)
        .setNanos((int) ((millis % 1000) * 1000000)).build();

    dam.setCreateAt(now);
    dam.setModifyAt(now);
    damp.setProtobufObj(dam.build());

    this.DialogAgentManagerMap.set(damp.getProtobufObj().getName(), damp);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_MANAGER);

    return damp.getProtobufObj().toBuilder();
  }

  public void delDialogAgentManager(Key req) {
    logger.info("[m2u-svcadm-dao] delDialogAgentManager");
    logger.trace("[m2u-svcadm-dao] delDialogAgentManager = {}", req.getName());

    this.DialogAgentManagerMap.delete(req.getName());
    Collection<DialogAgentActivationInfoPortable> result =
        this.DialogAgentActivationInfoMap.values(
            new SqlPredicate("dam_name = '" + req.getName() + "'"));

    for (DialogAgentActivationInfoPortable params : result) {
      String key = params.getProtobufObj().getDaName() + "_" + params.getProtobufObj().getDamName();

      this.DialogAgentActivationInfoMap.delete(key);
    }
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_MANAGER);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_ACTIVATION);
  }

  public Builder getDialogAgentManager(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentManager");
    logger.trace("[m2u-svcadm-dao] getDialogAgentManager = {}", req.getName());

    DialogAgentManager.Builder dam = DialogAgentManager.newBuilder();

    if (this.DialogAgentManagerMap.get(req.getName()) != null) {

      dam = this.DialogAgentManagerMap.get(req.getName()).getProtobufObj().toBuilder();

    }
    return dam;
  }

  public DialogAgentManagerList.Builder getDialogAgentManagerAllList() {
    logger.info("[m2u-svcadm-dao] getDialogAgentManagerAllList");

    DialogAgentManagerList.Builder daml = DialogAgentManagerList.newBuilder();

    for (Entry<String, DialogAgentManagerPortable> entry : this.DialogAgentManagerMap.entrySet()) {
      if (entry.getKey() != null) {
        logger.debug("entry.getKey() : {}", entry.getKey());

        DialogAgentManagerWithDialogAgentInstance.Builder damwd = DialogAgentManagerWithDialogAgentInstance
            .newBuilder();

        DialogAgentManager.Builder dam = entry.getValue().getProtobufObj().toBuilder();

        if (dam.getName() != null) {
          /*dam.setName(params.getName());*/

          Collection<DialogAgentActivationInfoPortable> result = DialogAgentActivationInfoMap
              .values(new SqlPredicate("active = true and dam_name = '" + dam.getName() + "'"));
          damwd.setDaCnt(result.size());

          Collection<DialogAgentInstanceExecutionInfoPortable> result2 = DialogAgentInstanceExecutionInfoMap
              .values(new SqlPredicate("active = true and dam_name = '" + dam.getName() + "'"));
          damwd.setDaiCnt(result2.size());
        }

        if (dam.getCreateAt() != null) {
          Timestamp create_dtm = (Timestamp) dam.getCreateAt();
          logger.debug("create_at : {}", dam.getCreateAt());

          long millis = System.currentTimeMillis();

          Timestamp now = Timestamp.newBuilder().setSeconds(millis / 1000)
              .setNanos((int) ((millis % 1000) * 1000000)).build();

          long second = (now.getSeconds() - create_dtm.getSeconds());
          long day = (second / (60 * 60)) / 24;
          long hours = (second / (60 * 60)) % 24;
          logger.debug("second {}", second);

          damwd.setDuration(String.valueOf(day) + " days " + String.valueOf(hours) + " hours");
        }
        damwd.setDamInfo(dam.build());
        daml.addDamwdList(damwd.build());
      }
    }
    return daml;
  }

  public DialogAgentManagerList.Builder getDialogAgentManagerList(Key req) {
    logger.info("[m2u-svcadm-dao] getDialogAgentManagerList");
    logger.trace("[m2u-svcadm-dao] getDialogAgentManagerList = {}", req.getName());

    DialogAgentManagerList.Builder daml = DialogAgentManagerList.newBuilder();

    Collection<DialogAgentActivationInfoPortable> daaipc =
        this.DialogAgentActivationInfoMap.values(
            new SqlPredicate("da_name='" + req.getName() + "'"));

    for (DialogAgentActivationInfoPortable daaip : daaipc) {
      DialogAgentManagerPortable damp = this.DialogAgentManagerMap.get(
          daaip.getProtobufObj().getDamName());
      DialogAgentManagerWithDialogAgentInstance.Builder damwd =
          DialogAgentManagerWithDialogAgentInstance.newBuilder();
      DialogAgentManager.Builder dam = damp.getProtobufObj().toBuilder();
      logger.debug("name = {}", damp.getProtobufObj().getName());


      /* dam.setName(params.getName());
        dam.setIp(params.getIp());
        dam.setPort(params.getPort());
        dam.setIsActive(params.getActive());*/

      if (damp.getProtobufObj().getCreateAt() != null) {
        Timestamp create_dtm = (Timestamp) damp.getProtobufObj().getCreateAt();
        logger.debug("create_dtm = {}", damp.getProtobufObj().getCreateAt());

        long millis = System.currentTimeMillis();

        Timestamp now = Timestamp.newBuilder().setSeconds(millis / 1000)
            .setNanos((int) ((millis % 1000) * 1000000)).build();

        long second = (now.getSeconds() - create_dtm.getSeconds());
        long day = (second / (60 * 60)) / 24;
        long hours = (second / (60 * 60)) % 24;

        long diffDays = second / (24 * 60 * 60 * 1000);
        logger.debug("second = {}, diffDays = {}", second, diffDays);

        // dam.setCreateAt(params.getCreateAt());
        damwd.setDuration(String.valueOf(day) + " days " + String.valueOf(hours) + " hours");
      }
      damwd.setDamInfo(dam.build());
      daml.addDamwdList(damwd.build());

    }
    return daml;
  }

  public Builder stopDialogAgentManager(Key req) {
    logger.info("[m2u-svcadm-dao] stopDialogAgentManager");
    logger.trace("[m2u-svcadm-dao] stopDialogAgentManager = {}", req.getName());

    DialogAgentManagerPortable damp = this.DialogAgentManagerMap.get(req.getName());
    if (damp == null) {
      return null;
    }
    DialogAgentManager.Builder dam = damp.getProtobufObj().toBuilder();

    dam.setActive(false);

    long millis = System.currentTimeMillis();

    Timestamp now = Timestamp.newBuilder().setSeconds(millis / 1000)
        .setNanos((int) ((millis % 1000) * 1000000)).build();

    dam.setModifyAt(now);

    damp.setProtobufObj(dam.build());

    this.DialogAgentManagerMap.set(dam.getName(), damp);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_MANAGER);

    return damp.getProtobufObj().toBuilder();
  }

  public DialogAgentManagerPortable updateActiveDialogAgentManager(Key req, boolean active) {
    logger.info("[m2u-svcadm-dao] updateActiveDialogAgentManager");
    logger.trace("[m2u-svcadm-dao] updateActiveDialogAgentManager = key : {}, active : {}", req.getName(), active);

    DialogAgentManagerPortable result = this.DialogAgentManagerMap.get(req.getName());
    long millis = System.currentTimeMillis();

    Timestamp now = Timestamp.newBuilder().setSeconds(millis / 1000)
        .setNanos((int) ((millis % 1000) * 1000000)).build();

    result.getProtobufObj().toBuilder().setModifyAt(now);
    result.getProtobufObj().toBuilder().setActive(active);

    DialogAgentManagerPortable damp = this.DialogAgentManagerMap.put(req.getName(), result);
    Common.invokeBackupSignal(MAP.DIALOG_AGENT_MANAGER);

    return damp;
  }
}
