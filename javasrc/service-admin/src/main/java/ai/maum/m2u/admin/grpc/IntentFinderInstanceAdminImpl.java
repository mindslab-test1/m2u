package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.IntentFinderInstanceDAO;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.console.Classifier.IntentFinderInstanceList;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import maum.m2u.router.v3.Intentfinder.IntentFinderInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntentFinderInstanceAdminImpl extends
    maum.m2u.console.IntentFinderInstanceAdminGrpc.IntentFinderInstanceAdminImplBase {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderInstanceAdminImpl.class);

  static IntentFinderInstanceDAO ifiDAO = new IntentFinderInstanceDAO();

  /**
   * Grpc Client로부터 ITF Instance List 조회 관련 요청 받음
   *
   * @param req com.google.protobuf.Empty
   * @param res StreamObserver<IntentFinderInstanceList>
   */
  @Override
  public void getIntentFinderInstanceList(com.google.protobuf.Empty req,
      StreamObserver<IntentFinderInstanceList> res) {
    logger.info("[m2u-svcadm-grpc] getIntentFinderInstanceList");

    try {
      IntentFinderInstanceList.Builder ifilb = ifiDAO.getIntentFinderInstanceList();

      res.onNext(ifilb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   * Grpc Client로부터 ITF Instance Name으로 Instance 정보 조회 관련 요청 받음
   *
   * @param req Key
   * @param res StreamObserver<IntentFinderInstance>
   */
  @Override
  public void getIntentFinderInstanceInfo(Key req,
      StreamObserver<IntentFinderInstance> res) {
    logger.info("[m2u-svcadm-grpc] getIntentFinderInstanceInfo");
    logger.trace("[m2u-svcadm-grpc] getIntentFinderInstanceInfo = {}", req.getName());

    try {
      IntentFinderInstance.Builder ifib = ifiDAO.getIntentFinderInstance(req);

      res.onNext(ifib.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   * Grpc Client로부터 ITF Instance Insert 관련 요청 받음
   *
   * @param req IntentFinderInstance
   * @param res StreamObserver<IntentFinderInstance>
   */
  @Override
  public void insertIntentFinderInstance(IntentFinderInstance req,
      StreamObserver<IntentFinderInstance> res) {
    logger.info("[m2u-svcadm-grpc] insertIntentFinderInstance");
    logger.trace("[m2u-svcadm-grpc] insertIntentFinderInstance = {}", req);

    try {
      try {
        ifiDAO.insertIntentFinderInstance(req);
      } catch (Exception e) {
        res.onError(e);
      }
      res.onNext(req);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   * Grpc Client로부터 ITF Instance Update 관련 요청 받음
   *
   * @param req IntentFinderInstance
   * @param res StreamObserver<IntentFinderInstance>
   */
  @Override
  public void updateIntentFinderInstance(IntentFinderInstance req,
      StreamObserver<IntentFinderInstance> res) {
    logger.info("[m2u-svcadm-grpc] updateIntentFinderInstance");
    logger.trace("[m2u-svcadm-grpc] updateIntentFinderInstance = {}", req);

    try {
      ifiDAO.updateIntentFinderInstance(req);
      res.onNext(req);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   * Grpc Client로부터 ITF Instance Delete 관련 요청 받음
   *
   * @param req KeyList
   * @param res StreamObserver<ResultList>
   */
  @Override
  public void deleteIntentFinderInstances(KeyList req, StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteIntentFinderInstances");
    logger.trace("[m2u-svcadm-grpc] deleteIntentFinderInstances = {}", req);

    try {
      ResultList.Builder kyb = ResultList.newBuilder();
      for (Key key : req.getKeyListList()) {
        Result.Builder result = Result.newBuilder();
        IntentFinderInstance.Builder ifi = ifiDAO.getIntentFinderInstance(key);
        result.setName(key.getName());
        if (ifi == null) {
          result.setResult(false);
        } else {
          ifiDAO.deleteIntentFinderInstance(key);
          result.setResult(true);
        }
        kyb.addResultList(result);
      }

      res.onNext(kyb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}
