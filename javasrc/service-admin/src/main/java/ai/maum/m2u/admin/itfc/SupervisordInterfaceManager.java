package ai.maum.m2u.admin.itfc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import maum.supervisor.Monitor.Result;
import maum.supervisor.Monitor.StdTailLog;
import maum.supervisor.Monitor.SupervisorProcess;
import maum.supervisor.Monitor.SupervisorProcessKey;
import maum.supervisor.Monitor.SupervisorServerGroup;
import maum.supervisor.Monitor.SupervisorServerGroupKey;
import maum.supervisor.Monitor.SupervisorServerGroupList;
import org.slf4j.LoggerFactory;

public class SupervisordInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(SupervisordInterfaceManager.class);

  private static SupervisordInterfaceManager instance = new SupervisordInterfaceManager();

  private static List<SupervisorXmlRpcClient> svdItfcInstances = new ArrayList<SupervisorXmlRpcClient>();
  ;

  private Map<String, Map<String, SupervisorProcess.Builder>> svdPrcsMultiMap;

  private SupervisordInterfaceManager() {
  }

  public static SupervisordInterfaceManager getInstance() {
    return SupervisordInterfaceManager.Singleton.instance;
  }

  public Map<String, Object> addSupervisordInterfaceInstance(SupervisorServerGroup req) {
    Map<String, Object> returnValue = new HashMap<String, Object>();
    boolean result = false;
    String message = "";

    String host = req.getHost();
    String ip = req.getIp();
    int port = req.getPort();
    try {
      String user = "maum";
      String password = "msl1234~";

      boolean aleadyInfrom = false;
      for (int i = 0; i < this.svdItfcInstances.size(); i++) {
        if (ip.equals(this.svdItfcInstances.get(i).getIp()) && port == this.svdItfcInstances.get(i)
            .getPort()) {
          aleadyInfrom = true;
          this.svdItfcInstances.get(i).updateLatestInformDtm(new Date());
          result = true;
          message = "UPDATE";
          break;
        }
      }

      if (!aleadyInfrom) {
        try {
          SupervisorXmlRpcClient svdItfcInstance =
              new SupervisorXmlRpcClient(host, ip, port, user, password);

          logger.debug("identification : {}", svdItfcInstance.getIdentification());

          Map<Object, Object> stateResult = svdItfcInstance.getState();
          int statecode = (int) stateResult.get("statecode");
          String statename = (String) stateResult.get("statename");
          if (statecode > -9999) {
            this.svdItfcInstances.add(svdItfcInstance);
            result = true;
            message = "ADD";
          } else {
            message = statename;
          }
        } catch (Exception e) {
          logger.error("{} => ", e.getMessage(), e);
          message = e.getMessage();
        }
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    returnValue.put("result", result);
    returnValue.put("message", message);

    return returnValue;
  }

  @SuppressWarnings("unchecked")
  public SupervisorServerGroupList getSupervisordServerGroupList(com.google.protobuf.Empty req) {
    SupervisorServerGroupList.Builder result = SupervisorServerGroupList.newBuilder();
    logger.debug("current size :: {}", this.svdItfcInstances.size());
    for (int i = 0; i < this.svdItfcInstances.size(); i++) {
      try {
        SupervisorXmlRpcClient svdc = this.svdItfcInstances.get(i);
        SupervisorServerGroup.Builder svdg = SupervisorServerGroup.newBuilder();
        svdg.setGrpName(svdc.getIdentification());
        svdg.setHost(svdc.getHostName());
        svdg.setIp(svdc.getIp());
        svdg.setPort(svdc.getPort());

        Object[] prcs = svdc.getAllProcessInfo();
        logger.info("{} > {} : {}", this.svdItfcInstances.get(i).getIdentifier(),
            this.svdItfcInstances.get(i).getHostName(), prcs.length);
        for (int j = 0; j < prcs.length; j++) {
          Map<Object, Object> pm = (Map<Object, Object>) prcs[j];
          logger.info("  -- {} : {} | {} | {}", pm.get("group"), pm.get("name"), pm.get("state"),
              pm.get("statename"));
          SupervisorProcess.Builder svproc = SupervisorProcess.newBuilder();
          svproc.setName((String) pm.get("name"));
          svproc.setGroup((String) pm.get("group"));
          svproc.setDescription((String) pm.get("description"));
          svproc.setStart((Integer) pm.get("start"));
          svproc.setStop((Integer) pm.get("stop"));
          svproc.setNow((Integer) pm.get("now"));
          svproc.setState((Integer) pm.get("state"));
          svproc.setSpawnerr((String) pm.get("spawnerr"));
          svproc.setExitstatus((Integer) pm.get("exitstatus"));
          svproc.setLogfile((String) pm.get("logfile"));
          svproc.setStdoutLogfile((String) pm.get("stdout_logfile"));
          svproc.setStderrLogfile((String) pm.get("stderr_logfile"));
          svproc.setPid((Integer) pm.get("pid"));
          svdg.addSvdProcList(svproc.build());
        }
        result.addSvdSvrGrpList(svdg.build());
      } catch (Exception e) {
        logger.info("  << Failed to get All Process Info. >>");
        logger.error("{} => ", e.getMessage(), e);
      }
    }

    return result.build();
  }

  public SupervisorServerGroup getSupervisordServerGroup(SupervisorServerGroupKey req) {
    SupervisorServerGroup.Builder result = SupervisorServerGroup.newBuilder();

    return result.build();
  }

  public SupervisorProcess getSupervisordProcess(SupervisorProcessKey req) {
    SupervisorProcess.Builder result = SupervisorProcess.newBuilder();

    return result.build();
  }

  public Result startSupervisorProcess(String ip, String grpName, String name) {
    Result.Builder result = Result.newBuilder();
    int portNo = 0;
    for (SupervisorXmlRpcClient xmlRpcClient : this.svdItfcInstances) {
      String chkIp = xmlRpcClient.getIp();
      if (chkIp.equals(ip)) {
        portNo = xmlRpcClient.getPort();
        break;
      }
    }
    if (portNo > 0) {
      SupervisorProcessKey.Builder svProcKey = SupervisorProcessKey.newBuilder();
      svProcKey.setIp(ip);
      svProcKey.setPort(portNo);
      svProcKey.setGrpName(grpName);
      svProcKey.setName(name);
      result = this.startSupervisorProcess(svProcKey.build()).toBuilder();
    }
    return result.build();
  }

  public Result startSupervisorProcess(SupervisorProcessKey req) {

    String prcsName =
        ((req.getGrpName() == null || req.getGrpName().equals("")) ? "" : req.getGrpName() + ":")
            + req.getName();
    logger.debug("> {}:{} / {}", req.getIp(), req.getPort(), prcsName);
    Result.Builder result = Result.newBuilder();

    String ip = req.getIp();
    int port = req.getPort();
    for (int i = 0; i < this.svdItfcInstances.size(); i++) {
      SupervisorXmlRpcClient client = this.svdItfcInstances.get(i);
      if (ip.equals(client.getIp()) && port == client.getPort()) {
        client.startProcess(prcsName, true);
        break;
      }
    }
    result.setResult(true);
    result.setMessage("success");

    return result.build();
  }

  public Result stopSupervisordProcess(String ip, String grpName, String name) {
    Result.Builder result = Result.newBuilder();
    int portNo = 0;
    for (int i = 0; i < this.svdItfcInstances.size(); i++) {
      String chkIp = this.svdItfcInstances.get(i).getIp();
      if (chkIp.equals(ip)) {
        portNo = this.svdItfcInstances.get(i).getPort();
        break;
      }
    }
    if (portNo > 0) {
      SupervisorProcessKey.Builder svProcKey = SupervisorProcessKey.newBuilder();
      svProcKey.setIp(ip);
      svProcKey.setPort(portNo);
      svProcKey.setGrpName(grpName);
      svProcKey.setName(name);
      result = this.stopSupervisorProcess(svProcKey.build()).toBuilder();
    }
    return result.build();
  }

  public Result stopSupervisorProcess(SupervisorProcessKey req) {
    String prcsName =
        ((req.getGrpName() == null || req.getGrpName().equals("")) ? "" : req.getGrpName() + ":")
            + req.getName();
    logger.debug("> {}:{} / {}", req.getIp(), req.getPort(), prcsName);
    Result.Builder result = Result.newBuilder();

    String ip = req.getIp();
    int port = req.getPort();
    for (int i = 0; i < this.svdItfcInstances.size(); i++) {
      SupervisorXmlRpcClient client = this.svdItfcInstances.get(i);
      if (ip.equals(client.getIp()) && port == client.getPort()) {
        client.stopProcess(prcsName, true);
        break;
      }
    }
    result.setResult(true);
    result.setMessage("success");
    return result.build();
  }

  public StdTailLog getTailProcessStdoutLog(SupervisorProcessKey req) {
    StdTailLog.Builder result = StdTailLog.newBuilder();

    return result.build();
  }

  private static class Singleton {

    private static final SupervisordInterfaceManager instance = new SupervisordInterfaceManager();
  }
}
