package ai.maum.m2u.admin.itfc;

import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.slf4j.LoggerFactory;

public class SupervisorXmlRpcClient {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(SupervisorXmlRpcClient.class);

  private String identifier;
  private int pid;
  private String hostName;
  private String ip;
  private int port;
  //  private String url;
  private Date latestInformDtm;
  private XmlRpcClient client;
  private XmlRpcClientConfigImpl config;

  public SupervisorXmlRpcClient(String host, String ip, int port, String username, String password) {

    String url = "http://" + ip + ":" + String.valueOf(port) + "/RPC2";

    this.client = new XmlRpcClient();
    this.config = new XmlRpcClientConfigImpl();
    try {
      config.setServerURL(new URL(url));
      config.setBasicUserName(username);
      config.setBasicPassword(password);
      config.setEnabledForExceptions(true);
      config.setConnectionTimeout(10 * 1000);
      config.setReplyTimeout(10 * 1000);
      client.setConfig(config);
      this.hostName = host;
      this.ip = ip;
      this.port = port;
//      this.url = url;
      this.latestInformDtm = new Date();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }

  public static void main(String[] args) {

    // FIXME(jslee88), USE CONFIG VARIABLE
    SupervisorXmlRpcClient st = new SupervisorXmlRpcClient("", "127.0.0.1", 9001, "maum", "msl1234~");
    Scanner sc = new Scanner(System.in);

    logger.info("[usage]");
    logger.info("  ##### Process Control #####");
    logger.info("  info [name] : getProcessInfo(name)");
    logger.info("  all : getAllProcessInfo()");
    logger.info("  start : startProcess(name, wait=True)");
    //logger.info("  startg : startProcessGroup(name, wait=True)");
    logger.info("  startall : startAllProcesses(wait=True)");
    logger.info("  stop : stopProcess(name, wait=True)");
    //logger.info("  stopg : stopProcessGroup(name, wait=True)");
    logger.info("  stopall : stopAllProcesses(wait=True)");
//    logger.info("  signal : signalProcess(name, signal)");
//    logger.info("  signalg : signalProcessGroup(name, signal)");
//    logger.info("  signalall : signalAllProcesses(signal)");
//    logger.info("  all : sendProcessStdin(name, chars)");
//    logger.info("  all : sendRemoteCommEvent(type, data)");
//    logger.info("  all : reloadConfig()");
//    logger.info("  all : addProcessGroup(name)");
//    logger.info("  all : removeProcessGroup(name)");
    logger.info("  ##### Process Logging #####");
    logger.info("  readstdout : readProcessStdoutLog(name, offset, length)");
    logger.info("  readstderr : readProcessStderrLog(name, offset, length)");
    logger.info("  tailstdout : tailProcessStdoutLog(name, offset, length)");
    logger.info("  tailstderr : tailProcessStderrLog(name, offset, length)");
    logger.info("  clearlog : clearProcessLogs(name)");
    logger.info("  clearlogall : clearAllProcessLogs()");
    logger.info("input : ");

    try {

      while (sc.hasNext()) {

        String inputCmd = sc.nextLine().trim();
        String cmd = "";
        int paramLen = 0;
        String[] inputArr = inputCmd.split(" ");
        if (inputArr != null && inputArr.length > 1) {
          cmd = inputArr[0].trim();
          paramLen = inputArr.length - 1;
        } else {
          cmd = inputCmd.trim();
        }
        String[] param = new String[paramLen];
        for (int i = 0; i < paramLen; i++) {
          param[i] = inputArr[i + 1];
        }

        if (cmd.equals("all")) {
          Object[] pList = st.getAllProcessInfo();
        }

        if (cmd.equals("info")) {
          logger.info(param[0]);
          st.getProcessInfo(param[0]);
        }

        if (cmd.equals("start")) {
          boolean result = st.startProcess(param[0], true);
          logger.debug("start result : {}", result);
        }
        if (cmd.equals("stop")) {
          boolean result = st.stopProcess(param[0], true);
          logger.debug("stop result : {}", result);
        }

        if (cmd.equals("startall")) {
          st.startAllProcesses(true);
        }
        if (cmd.equals("stopall")) {
          st.stopAllProcesses(true);
        }

//        if(cmd.equals("signal")){
//          st.signalProcess(param[0], param[1]);
//        }

        if (cmd.equals("readstdout")) {
          st.readProcessStdoutLog(param[0], 0, 1024);
        }
        if (cmd.equals("tailstdout")) {
          st.tailProcessStdoutLog(param[0], 0, 1024);
        }

        if (cmd.equals("clearlog")) {
          st.clearProcessLogs(param[0]);
        }
        if (cmd.equals("clearlogall")) {
          st.clearAllProcessLogs();
        }


        /**/
/*
    Supervisord test = Supervisord
        //API ADDR,Default:http://localhost:9001/RPC2
        .connect(api)
        // USERNAME AND PASSWORD
        .auth(userName, password)
        // the Identification, U shoud find it in "test.conf" default supervisor
        .namespace("supervisor")
        .proxy("HOST","PORT") //IF NEED
        ;
/**/

        if (cmd.equals("q")) {
          System.exit(0);
        } else {
          logger.info("input : ");
        }
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }

  }

  public String getIdentifier() {
    return this.identifier;
  }

  public String getHostName() {
    return this.hostName;
  }

  public String getIp() {
    return this.ip;
  }

//  public String getUrl(){
//    return this.url;
//  }

  public int getPort() {
    return this.port;
  }

  public void updateLatestInformDtm(Date curDate) {
    this.latestInformDtm = curDate;
  }

  public String getIdentification() {
    String result = "";
    try {
      Object[] params = new Object[]{};
      result = (String) this.client.execute("supervisor.getIdentification", params);
      this.identifier = result;
    } catch (XmlRpcException xre) {
      logger.error("{} => ", xre.getMessage(), xre);
    }
    return result;
  }

  public int getPID() {
    int result = -1;
    try {
      Object[] params = new Object[]{};
      result = (int) this.client.execute("supervisor.getPID", params);
      this.pid = result;
    } catch (XmlRpcException xre) {
      logger.error("{} => ", xre.getMessage(), xre);
    }
    return result;
  }

  /**
   * @return result Map<Object, Object>
   * statecode : 2,     1,        0,          -1
   * statename : FATAL, RUNNING,  RESTARTING, SHUTDOWN
   */
  @SuppressWarnings("unchecked")
  public Map<Object, Object> getState() {
    Map<Object, Object> result = new HashMap<>();
    try {
      Object[] params = new Object[]{};
      result = (Map<Object, Object>) this.client.execute("supervisor.getState", params);
//      logger.info("getState > statecode : " + result.get("statecode") + " / statename : " + result.get("statename"));
    } catch (XmlRpcException xre) {
      logger.error("{} => ", xre.getMessage(), xre);
      result.put("statecode", -9999);
      result.put("statename", "Not Available");
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public Object[] getAllProcessInfo() {
    Object[] result = null;
    try {
      Object[] params = new Object[]{};
      result = (Object[]) this.client.execute("supervisor.getAllProcessInfo", params);
//      logger.info(result[0].getClass().getName());

      for (int i = 0; i < result.length; i++) {
        Map<Object, Object> rm = (HashMap) result[i];
//        logger.info(i+" : "+rm.get("name"));
      }
//      Map<Object, Object> rm = (HashMap) result[0];
//      for (Map.Entry<Object, Object> entry : rm.entrySet()) {
//        logger.info(entry.getKey() + " : " + rm.get(entry.getKey()) + "    ["+rm.get(entry.getKey()).getClass()+"]");
//      }
    } catch (Exception e) {
      logger.info("{}.getAllProcessInfo = {}", this.getClass().getName(), e.getMessage());
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  private Object getProcessInfo(String name) {
    Object result = null;
    try {
      Object[] params = new Object[]{name};
      result = (Object) this.client.execute("supervisor.getProcessInfo", params);
      Map<Object, Object> rm = (Map<Object, Object>) result;
      for (Map.Entry<Object, Object> entry : rm.entrySet()) {
        logger.info("{} : {} [{}]",entry.getKey(), rm.get(entry.getKey()), rm.get(entry.getKey()).getClass());
      }
      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  public boolean startProcess(String name, boolean wait) {
    boolean result = false;
    try {
      Object[] params = new Object[]{name, wait};
      result = (boolean) this.client.execute("supervisor.startProcess", params);
      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  public boolean stopProcess(String name, boolean wait) {
    boolean result = false;
    try {
      Object[] params = new Object[]{name, wait};
      result = (boolean) this.client.execute("supervisor.stopProcess", params);
      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  private void startAllProcesses(boolean wait) {
    //boolean result = false;
    try {
      Object[] params = new Object[]{wait};
      Object[] result = (Object[]) this.client.execute("supervisor.startAllProcesses", params);
      for (Object obj : result) {
        Map<Object, Object> rm = (Map<Object, Object>) obj;
        for (Map.Entry<Object, Object> entry : rm.entrySet()) {
          logger.info(
              entry.getKey() + " : " + rm.get(entry.getKey()) + "    [" + rm.get(entry.getKey())
                  .getClass() + "]");
        }
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    //return result;
  }
/*

  private boolean signalProcess(String name, String signal) {
    logger.info("==== signalProcess ============================================================");
    boolean result = false;
    try {
      Object[] params = new Object[]{name, signal};
      result = (boolean) this.client.execute("supervisor.signalProcess", params);
    }catch(Exception e) {
      //e.printStackTrace();
      logger.info(e.getMessage());
    }
    return result;
  }
*/

  @SuppressWarnings("unchecked")
  private void stopAllProcesses(boolean wait) {
    try {
      Object[] params = new Object[]{wait};
      Object[] result = (Object[]) this.client.execute("supervisor.stopAllProcesses", params);
      for (Object obj : result) {
        Map<Object, Object> rm = (Map<Object, Object>) obj;
        for (Map.Entry<Object, Object> entry : rm.entrySet()) {
          logger.debug("{} : {} [ {} ]",
              entry.getKey(),
              rm.get(entry.getKey()),
              rm.get(entry.getKey()).getClass());
        }
      }
//      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
//    return result;
  }

  private void readProcessStdoutLog(String name, int offset, int length) {
    String result = "";
    try {
      Object[] params = new Object[]{name, offset, length};
      result = (String) this.client.execute("supervisor.readProcessStdoutLog", params);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.info(result);
  }

  @SuppressWarnings("unchecked")
  private void tailProcessStdoutLog(String name, int offset, int length) {
    try {
      Object[] params = new Object[]{name, offset, length};
      Object[] result = (Object[]) this.client.execute("supervisor.tailProcessStdoutLog", params);

      logger.debug("result[0] : {}", result[0]);
      logger.debug("result[0] : {1}", result[1]);
      logger.debug("result[0] : {2}", result[2]);

      for (Object obj : result) {
        Map<Object, Object> rm = (Map<Object, Object>) obj;
        for (Map.Entry<Object, Object> entry : rm.entrySet()) {
          logger.debug("{} : {} [ {} ]",
              entry.getKey(),
              rm.get(entry.getKey()),
              rm.get(entry.getKey()).getClass());
        }
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }

  private boolean clearProcessLogs(String name) {
    boolean result = false;
    try {
      Object[] params = new Object[]{name};
      result = (boolean) this.client.execute("supervisor.clearProcessLogs", params);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  private void clearAllProcessLogs() {
//    boolean result = false;
    try {
      Object[] params = new Object[]{};
      Object[] result = (Object[]) this.client.execute("supervisor.clearAllProcessLogs", params);
      for (Object obj : result) {
        Map<Object, Object> rm = (Map<Object, Object>) obj;
        for (Map.Entry<Object, Object> entry : rm.entrySet()) {
          logger.debug("{} : {} [ {} ]",
              entry.getKey(),
              rm.get(entry.getKey()),
              rm.get(entry.getKey()).getClass());
        }
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
//    return result;
  }

}
