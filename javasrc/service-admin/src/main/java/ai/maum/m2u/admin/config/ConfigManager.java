package ai.maum.m2u.admin.config;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigManager {

  static final Logger logger = LoggerFactory.getLogger(ConfigManager.class);

  private static XMLConfiguration config = null;

  private static String absoluteConfigPath = "";

  public static void setAbsoluteConfigPath(String absoluteConfigPath) {
    ConfigManager.absoluteConfigPath = absoluteConfigPath;
  }

  private static void load() {
    if (config == null) {
      logger.info("### service-admin config file load {}", ConfigManager.absoluteConfigPath);
      config = getConfig();
    }
  }

  private static XMLConfiguration getConfig() {

    //String configFilePath = getConfigPath();
    String configFileName = "conf/service-config.xml";
    String configFilePath = absoluteConfigPath + File.separator + configFileName;
    logger.info("config path = {}", configFilePath);

    if (configFilePath == null || configFilePath.length() <= configFileName.length() + 1) {
      System.err.println("=================================================================");
      System.err.println("** maum.ai M2U SERVICE ADMIN SYSTEM \"service-config.xml\" NOT FOUND! **");
      System.err.println("** [[" + configFilePath + "]] **");
      System.err.println("=================================================================");
    }

    try {
      //] [,] Parsing Block
      //] getStringArray의 오작동으로 [,]의 자동 Parsing을 막고 String.split로 구현하기 위함.
      XMLConfiguration.setDefaultListDelimiter((char) 0);
      //XMLConfiguration.setDelimiterParsingDisabled(true);
      config = new XMLConfiguration(configFilePath);
      //config.setValidating(true);

      config.load();
    } catch (ConfigurationException cex) {
      logger.error("{} => ", cex.getMessage(), cex);
    } catch (Exception ex) {
      logger.error("{} => ", ex.getMessage(), ex);
    }
    return config;
  }


  public static int getInt(String name) {
    load();
    return config.getInt(name);
  }

  public static long getLong(String name) {
    load();
    return config.getLong(name);
  }

  public static String getString(String name) {
    load();
    return config.getString(name);
  }

  public static String[] getStringArray(String name) {
    load();
    return config.getString(name).split(",");
  }

  public static List<String> getArray(String name) {
    load();
    List<String> arrayList = Arrays.asList(config.getStringArray(name));
    return arrayList;
  }

  public static boolean getBoolean(String name) {
    load();
    return config.getBoolean(name);
  }

  public static double getDouble(String name) {
    load();
    return config.getDouble(name);
  }

  public static float getFloat(String name) {
    load();
    return config.getFloat(name);
  }
}

