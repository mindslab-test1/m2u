package ai.maum.m2u.admin.grpc;

import ai.maum.m2u.admin.dao.SimpleClassifierDAO;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.util.List;
import maum.m2u.console.Classifier.SimpleClassifier;
import maum.m2u.console.Classifier.SimpleClassifierList;
import maum.m2u.console.Da.Key;
import maum.m2u.console.Da.KeyList;
import maum.m2u.console.Da.Result;
import maum.m2u.console.Da.ResultList;
import maum.m2u.server.Simpleclassifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleClassifierAdminImpl extends
    maum.m2u.console.SimpleClassifierAdminGrpc.SimpleClassifierAdminImplBase {

  static final Logger logger = LoggerFactory.getLogger(SimpleClassifierAdminImpl.class);

  static SimpleClassifierDAO scDao = new SimpleClassifierDAO();

  @Override
  public void getSimpleClassifierAllList(com.google.protobuf.Empty req,
                                         StreamObserver<SimpleClassifierList> res) {
    logger.info("[m2u-svcadm-grpc] getSimpleClassifierAllList");

    try {

      SimpleClassifierList.Builder sclb = scDao.getSimpleClassifierAllList();

      res.onNext(sclb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }

  }

  @Override
  public void getSimpleClassifierInfo(Key req,
      StreamObserver<maum.m2u.console.Classifier.SimpleClassifier> res) {
    logger.info("[m2u-svcadm-grpc] getSimpleClassifierInfo");
    logger.trace("[m2u-svcadm-grpc] getSimpleClassifierInfo = {}", req.getName());

    try {
      SimpleClassifier.Builder scb = scDao.getSimpleClassifier(req);

      res.onNext(scb.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   */
  @Override
  public void insertSimpleClassifierInfo(maum.m2u.console.Classifier.SimpleClassifier req,
      StreamObserver<maum.m2u.console.Classifier.SimpleClassifier> res) {
    logger.info("[m2u-svcadm-grpc] insertSimpleClassifierInfo");
    logger.trace("[m2u-svcadm-grpc] insertSimpleClassifierInfo = {}", req);

    try {
      Simpleclassifier.SimpleClassifierResource.Builder scrb =
          Simpleclassifier.SimpleClassifierResource.newBuilder();

      String jsonStr = generateJsonStringForSkills(req.getSkillListList());

      if ("".equals(jsonStr)) {
        throw new Exception("Skill info is null");
      }

      scrb.setScName(req.getName());
      scrb.setLang(req.getLang());
      scrb.setScJson(jsonStr);

      scDao.insertSimpleClassifier(req);

      res.onNext(req);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("insertSimpleClassifierInfo Error=> {} ", e.getMessage());
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void updateSimpleClassifierInfo(maum.m2u.console.Classifier.SimpleClassifier req,
      StreamObserver<maum.m2u.console.Classifier.SimpleClassifier> res) {
    logger.info("[m2u-svcadm-grpc] updateSimpleClassifierInfo");
    logger.trace("[m2u-svcadm-grpc] updateSimpleClassifierInfo = {}", req);

    try {
      scDao.updateSimpleClassifier(req);
      res.onNext(req);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void deleteSimpleClassifier(KeyList req, StreamObserver<ResultList> res) {
    logger.info("[m2u-svcadm-grpc] deleteSimpleClassifier");
    logger.trace("[m2u-svcadm-grpc] deleteSimpleClassifier = {}", req);

    try {

      ResultList.Builder rl = ResultList.newBuilder();
      SimpleClassifierDAO scDao = new SimpleClassifierDAO();

      for (Key key : req.getKeyListList()) {
        Result.Builder result = Result.newBuilder();
        SimpleClassifier.Builder da = scDao.getSimpleClassifier(key);
        result.setName(key.getName());
        if (da == null) {
          result.setResult(false);
        } else {
          scDao.delSimpleClassifier(key);
          result.setResult(true);
        }
        rl.addResultList(result);
      }

      res.onNext(rl.build());
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      res.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  private String generateJsonStringForSkills(List<SimpleClassifier.ListSkill> skills) {
    if (skills == null) {
      return "";
    }

    String resultStr = "";
    int skillsIdx = 1;

    for (SimpleClassifier.ListSkill skill : skills) {
      String name = skill.getName();
      List<String> regexs = skill.getRegexList();

      resultStr += "\"" + name + "\":{\"regex\":[";

      int regxsIdx = 1;
      for (String regex : regexs) {
        if (regxsIdx == regexs.size()) {
          resultStr += "\"" + regex + "\"";
        } else {
          resultStr += "\"" + regex + "\",";
        }
        regxsIdx++;
      }

      if (skillsIdx == skills.size()) {
        resultStr += "]}";
      } else {
        resultStr += "]},";
      }

      skillsIdx++;
    }

    return "{" + resultStr + "}";
  }

}

