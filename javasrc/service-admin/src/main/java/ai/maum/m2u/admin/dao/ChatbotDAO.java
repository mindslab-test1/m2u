package ai.maum.m2u.admin.dao;

import ai.maum.m2u.admin.common.model.NameSpace.MAP;
import ai.maum.m2u.admin.hzc.HazelcastConnector;
import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import java.util.Map.Entry;
import maum.m2u.console.ChatbotOuterClass.Chatbot;
import maum.m2u.console.ChatbotOuterClass.ChatbotDialogAgentInstanceInfo;
import maum.m2u.console.ChatbotOuterClass.ChatbotList;
import maum.m2u.console.Da.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChatbotDAO {

  static final Logger logger = LoggerFactory.getLogger(ChatbotDAO.class);

  private IMap<String, ChatbotPortable> ChatbotMap =
      HazelcastConnector.getInstance().client.getMap(MAP.CHATBOT);
  private IMap<String, DialogAgentInstancePortable> DialogAgentInstanceMap =
      HazelcastConnector.getInstance().client.getMap(MAP.DIALOG_AGENT_INSTANCE);

  public void insertChatbot(Chatbot req) {
    logger.info("[m2u-svcadm-dao] insertChatbot");
    logger.trace("[m2u-svcadm-dao] insertChatbot = {}", req);

    ChatbotPortable cp = new ChatbotPortable();

    cp.setProtobufObj(req);

    this.ChatbotMap.set(cp.getProtobufObj().getName(), cp);
    Common.invokeBackupSignal(MAP.CHATBOT);
  }

  public void updateChatbot(Chatbot req) {
    logger.info("[m2u-svcadm-dao] updateChatbot");
    logger.trace("[m2u-svcadm-dao] updateChatbot = {}", req);

    ChatbotPortable cp = this.ChatbotMap.get(req.getName());

    cp.setProtobufObj(req);

    this.ChatbotMap.set(cp.getProtobufObj().getName(), cp);
    Common.invokeBackupSignal(MAP.CHATBOT);
  }

  public void delChatbot(Key req) {
    logger.info("[m2u-svcadm-dao] delChatbot");
    logger.trace("[m2u-svcadm-dao] delChatbot = {}", req.getName());

    this.ChatbotMap.delete(req.getName());
    Common.invokeBackupSignal(MAP.CHATBOT);
  }

  public Chatbot.Builder getChatbot(Key req) {
    logger.info("[m2u-svcadm-dao] getChatbot");
    logger.trace("[m2u-svcadm-dao] getChatbot = {}", req.getName());

    ChatbotPortable cbp = this.ChatbotMap.get(req.getName());
    Chatbot.Builder chatbot = cbp.getProtobufObj().toBuilder();
    //chatbot.setDetail(getChatbotDetail(req));
    return chatbot;
  }

  public ChatbotList.Builder getChatbotAllList() {
    logger.info("[m2u-svcadm-dao] getChatbotAllList");

    ChatbotList.Builder cblb = ChatbotList.newBuilder();

    for (Entry<String, ChatbotPortable> cbp : this.ChatbotMap.entrySet()) {

      ChatbotDialogAgentInstanceInfo.Builder cbdaiib = ChatbotDialogAgentInstanceInfo.newBuilder();

      if (cbp != null) {
        cbdaiib.setName(cbp.getValue().getProtobufObj().getName());
        cbdaiib.setTitle(cbp.getValue().getProtobufObj().getTitle());
        cbdaiib.setIntentFinderPolicy(cbp.getValue().getProtobufObj().getIntentFinderPolicy());

        cbdaiib.setKoreanCnt(
            this.DialogAgentInstanceMap.values(
                new SqlPredicate("lang = 0 and chatbot_name = '" + cbdaiib.getName() + "'"))
                .size());
        cbdaiib.setEnglishCnt(
            this.DialogAgentInstanceMap.values(
                new SqlPredicate("lang = 1 and chatbot_name = '" + cbdaiib.getName() + "'"))
                .size());
        logger.debug("chatbot {}", cbdaiib);
      }
      cblb.addChatbotList(cbdaiib);
    }
    return cblb;
  }
}