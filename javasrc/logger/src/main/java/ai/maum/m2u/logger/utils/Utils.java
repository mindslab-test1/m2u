package ai.maum.m2u.logger.utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.FileAppender;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

  private static final Logger logger = LoggerFactory.getLogger(Utils.class);

  public static void dropBinaryDump(byte[] data, String dumpName) {
    File backupFile;
    Logger root = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    Appender fileAppender = ((ch.qos.logback.classic.Logger) root).getAppender("FILE");
    if (fileAppender != null) {
      Path path = Paths.get(((FileAppender) fileAppender).getFile()).getParent();
      backupFile = new File(path.toFile(), dumpName);
    } else {
      backupFile = Paths.get(dumpName).toFile();
    }

    DataOutputStream dump;
    try {
      dump = new DataOutputStream(new FileOutputStream(backupFile));
      dump.write(data);
      dump.close();
      logger.info("DUMP '{}' {}bytes.", dumpName, data.length);
    } catch (IOException e) {
      logger.error("DUMP '{}' failed. {}", dumpName, e.getMessage());
      logger.error("", e);
    }
  }

  public static void setSimpleLogger(String logFile) {
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

    FileAppender<ILoggingEvent> file = new FileAppender<>();
    file.setName("FILE");
    file.setFile(Paths.get(logFile).toAbsolutePath().toString());
    file.setContext(context);
    file.setAppend(true);

    ThresholdFilter thresholdFilter = new ThresholdFilter();
    thresholdFilter.setLevel("TRACE");
    thresholdFilter.setContext(context);
    thresholdFilter.start();
    file.addFilter(thresholdFilter);

    PatternLayoutEncoder patternLayoutEncoder = new PatternLayoutEncoder();
    patternLayoutEncoder.setContext(context);
    patternLayoutEncoder.setPattern("%date %level [%thread] %logger{10} %msg%n");
    patternLayoutEncoder.start();
    file.setEncoder(patternLayoutEncoder);

    file.start();

    ch.qos.logback.classic.Logger root = context.getLogger(Logger.ROOT_LOGGER_NAME);
    root.setLevel(Level.DEBUG);
    root.addAppender(file);
  }

}
