package ai.maum.m2u.logger.itfc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.concurrent.TimeUnit;

import maum.m2u.da.v3.DialogAgentProviderGrpc;
import maum.m2u.da.v3.Talk.CloseSkillRequest;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import org.slf4j.LoggerFactory;

//import maum.m2u.da.v1.DialogAgentProviderGrpc;

public class GrpcV3InterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(GrpcV3InterfaceManager.class);

  private final ManagedChannel channel;
  private final DialogAgentProviderGrpc.DialogAgentProviderBlockingStub dapStub;

  public GrpcV3InterfaceManager(String host, int port) {
    this(ManagedChannelBuilder.forAddress(host, port)
        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
        // needing certificates.
        .usePlaintext());
  }

  /**
   * Construct client for accessing RouteGuide server using the existing channel.
   */
  GrpcV3InterfaceManager(ManagedChannelBuilder<?> channelBuilder) {
    channel = channelBuilder.build();
    dapStub = DialogAgentProviderGrpc.newBlockingStub(channel);
  }

  /**
   * Greet server. If provided, the first element of {@code args} is the name to use in the
   * greeting.
   */
  public static void main(String[] args) throws Exception {

  }

  public void shutdown() {
    try {
      channel.shutdown().awaitTermination(3, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      logger.error("shutdown e : " , e);
    }
  }

  public CloseSkillResponse closeSkill(CloseSkillRequest closeSkillReq) {
    logger.debug("==========GrpcV3InterfaceManager.closeSkill [{}]", closeSkillReq);

    try {
      CloseSkillResponse result = dapStub.closeSkill(closeSkillReq);
      logger.trace("#@ result getUpdateSession [{}]", result.getSessionUpdate());
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed = {}", e.getMessage());
//      try {
//        Thread.sleep(1000);
//      } catch (InterruptedException e1) {
//        e1.printStackTrace();
//      }
      return null;
    }
  }
}
