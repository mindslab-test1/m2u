package ai.maum.m2u.logger.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBCP2Test {

  static final Logger logger = LoggerFactory.getLogger(DBCP2Test.class);

  public static void main(String[] args) {

    System.out.println("DBCP2 Test!!");

    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;

    try {
      int ctimes = 0;
      int mtimes = 1000;
      while (ctimes < mtimes) {
        conn = DBCP2Manager.getInstance().getConnection();
        stmt = conn.createStatement();
        switch(DBCP2Manager.getDbType()) {
          case DBCP2Manager.DB_TYPE_ORACLE :
            rs = stmt.executeQuery("select sysdate from dual");
            break;

          case DBCP2Manager.DB_TYPE_MYSQL :
            rs = stmt.executeQuery("select now()");
            break;

          default :
            break;
        }

        while (rs.next()) {
          System.out.println("[" + ctimes + "]" + rs.getDate(1));
        }
        try {
          if (rs != null) rs.close();
        } catch (Exception e) {
        }
        try {
          if (stmt != null) stmt.close();
        } catch (Exception e) {
        }
        try {
          if (conn != null) conn.close();
        } catch (Exception e) {
        }

        ctimes++;
        Thread.sleep(10);
      }

      Thread.sleep(10000);
    } catch (SQLException sqle) {
      logger.error("main sqle : " , sqle);
    } catch (InterruptedException e) {
      logger.error("main e : " , e);
    } finally {
      try {
        if (rs != null) rs.close();
      } catch (Exception e) {
      }
      try {
        if (stmt != null) stmt.close();
      } catch (Exception e) {
      }
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
      }
    }
  }

}
