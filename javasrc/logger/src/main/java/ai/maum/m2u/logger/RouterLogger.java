package ai.maum.m2u.logger;


import static ai.maum.m2u.logger.common.model.NameSpace.Setting;

import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.ClassificationRecordPortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceResourcePortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceResourceSkillPortable;
import ai.maum.m2u.common.portable.DialogSessionV3Portable;
import ai.maum.m2u.common.portable.SessionTalkV3Portable;
import ai.maum.m2u.logger.common.model.NameSpace.MAP;
import ai.maum.m2u.logger.config.PropertyManager;
import ai.maum.m2u.logger.db.DBCP2Manager;
import ai.maum.m2u.logger.hzc.HazelcastConnector;
import ai.maum.m2u.logger.itfc.GrpcV3InterfaceManager;
import ai.maum.m2u.logger.sql.SqlClassification;
import ai.maum.m2u.logger.sql.SqlSession;
import ai.maum.m2u.logger.sql.SqlTalk;
import ai.maum.m2u.logger.utils.Utils;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import maum.m2u.da.v3.Talk.CloseSkillRequest;
import maum.m2u.router.v3.Intentfinder.ClassificationRecord;
import maum.m2u.router.v3.Session.DialogSession;
import maum.m2u.router.v3.Session.SessionTalk;
import maum.m2u.server.Pool.DialogAgentInstanceResource;
import maum.m2u.server.Pool.DialogAgentInstanceResourceSkill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unchecked")
public class RouterLogger {

  private static final Logger logger = LoggerFactory.getLogger(RouterLogger.class);
  private static final Logger remoteDbLogger = LoggerFactory.getLogger("remote-db");

  private Boolean logSession;
  private Boolean logTalk;
  private Boolean logClassification;

  // hzc 객체 선언
  private HazelcastInstance hzc;

  private Connection conn;

  private SqlSession sqlSession;
  private SqlTalk sqlTalk;
  private SqlClassification sqlClassification;
  private int totalExpSessCnt;
  private int totalExpTalkCnt;
  private int totalExpClassificationCnt;


  public RouterLogger() {
    logSession = PropertyManager.getBoolean(Setting.LOGGER_ROUTER_SESSION);
    logTalk = PropertyManager.getBoolean(Setting.LOGGER_ROUTER_TALK);
    logClassification = PropertyManager.getBoolean(Setting.LOGGER_ROUTER_CLASSIFICATION);

    logger.info("Log Router Session: {}", logSession);
    logger.info("Log Router Talk: {}", logTalk);
    logger.info("Log Router Classification: {}", logClassification);
  }

  /**
   * 객체 초기화
   */
  public void init() throws SQLException {
    // hzc 객체 생성
    if (hzc == null) {
      this.hzc = HazelcastConnector.getInstance().client;
    }

    if (!(logSession || logTalk || logClassification)) {
      logger.debug("RouterLogger do not use DB access.");
      return;
    }

    if (conn == null) {
      // DBCP 객체 생성
      conn = DBCP2Manager.getInstance().getConnection();
      if (logTalk) {
        sqlTalk = new SqlTalk();
      }
      if (logSession) {
        sqlSession = new SqlSession();
      }
      if (logClassification) {
        sqlClassification = new SqlClassification();
      }
    }

    String dbType;
    dbType = DBCP2Manager.getDbType();

    // PreparedStatement 객체 정의 및 테이블 생성
    if (logSession) {
      sqlSession.init(conn, dbType);
      sqlSession.createTable(conn);
    }
    if (logTalk) {
      sqlTalk.init(conn, dbType);
      sqlTalk.createTable(conn);
    }
    if (logClassification) {
      sqlClassification.init(conn, dbType);
      sqlClassification.createTable(conn);
    }
  }

  /**
   * hazelcast에서 정보를 추출
   */
  public boolean hzcProcess() throws SQLException {
    boolean dbAvailable = false;
    long pT = System.currentTimeMillis();

    // DBCP 객체가 없는 경우
    try {
      if (conn == null) {
        init();
      }
      dbAvailable = conn != null;
    } catch (SQLException e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    // Infomation of hzcProcess
    // 1. chatbot 정보를 갖고 온다
    // 2. 해당 chatbot의 session_timeout 값을 갖고 온다
    // 3. sessMap에 chatbot 정보와 같은지 확인한다
    // 4. session_timeout값이 0이면 m2u.conf값을 사용한다

    // #@ hzc 로 부터 session 정보 갖고 오기 (예 : #1048579-#2)
    IMap<Long, DialogSessionV3Portable> sessMap = hzc.getMap(MAP.SESSION_V3);
    // #@ hzc 로 부터 talk 정보 갖고 오기 (예 : #1048579-#2)
    IMap<Long, SessionTalkV3Portable> talkMap = hzc.getMap(MAP.TALK_V3);
    // #@ hzc 로 부터 chatbot 정보 갖고 오기
    IMap<String, ChatbotPortable> chatbotMap = hzc.getMap(MAP.CHATBOT);
    // #@ hzc 로 부터 classification 정보 갖고 오기
    IMap<String, ClassificationRecordPortable> classificationMap = hzc.getMap(MAP.CLASSIFICATION);

    totalExpSessCnt = 0;
    totalExpTalkCnt = 0;
    totalExpClassificationCnt = 0;

    for (String tempChatbot : chatbotMap.keySet()) {
      int tempSessionTimeout = chatbotMap.get(tempChatbot).getProtobufObj().getSessionTimeout();
      logger.trace("#@ Chatbot name [{}], SessionTimeout [{}]", tempChatbot, tempSessionTimeout);

      sqlProcess(sessMap, talkMap, classificationMap, tempChatbot, tempSessionTimeout, dbAvailable);
    }

    //Chatbot 정보가 없는 경우
    //해당 경우는 실수로 관리자가 Chatbot을 삭제한 경우
    //hzc에 저장된 세션 정보를 삭제하기 위함이다
    int execcedSessionTimeout = PropertyManager.getInt(Setting.NON_CHATBOT_SESSION_EXPIRE_TIME);
    sqlProcess(sessMap, talkMap, classificationMap, "", execcedSessionTimeout, dbAvailable);

    logger.info("[ALL] remains s:{} t:{} c:{} / expired s:{} t:{} c:{} took {}ms",
        sessMap.size(), talkMap.size(), classificationMap.size(),
        totalExpSessCnt, totalExpTalkCnt, totalExpClassificationCnt,
        (System.currentTimeMillis() - pT));

    return true;
  }

  /**
   * 정보들을 RDB에 저당
   *
   * @param sessMap 세션 Map
   * @param talkMap 토크 Map
   * @param chatbot 챗봇 정보
   * @param sessionTimeout 세션 타임 아웃
   */
  @SuppressWarnings("Duplicates")
  private void sqlProcess(IMap<Long, DialogSessionV3Portable> sessMap,
      IMap<Long, SessionTalkV3Portable> talkMap,
      IMap<String, ClassificationRecordPortable> classificationMap,
      String chatbot, int sessionTimeout, boolean dbAvailable)
      throws SQLException {
    long pT = System.currentTimeMillis();

    if (sessionTimeout == 0) {
      // #@ m2u.conf에 600으로 정의 (10분)
      sessionTimeout = PropertyManager.getInt(Setting.DEFAULT_SESSION_EXPIRE_TIME);
    }

    Calendar curr = Calendar.getInstance();
    curr.add(Calendar.SECOND, -1 * sessionTimeout);
    long expBase = curr.getTimeInMillis();

    Predicate sessPredicate;
    EntryObject sessEntry = new PredicateBuilder().getEntryObject();

    if (!chatbot.isEmpty()) {
      //(chatbot 정보가 있는 경우)
      //chatbot 이름이 갖고
      //토크 만료시간이 지났거나, 유효하지 않은 세션
      sessPredicate = sessEntry.get("chatbot").equal(chatbot)
          .and(sessEntry.get("last_talked_at").lessThan(expBase / 1000)
              .or(sessEntry.get("valid").equal(false)));
    } else {
      //(chatbot 정보가 없는 경우)
      //최대토크 만료시간이 지났거나, 유효하지 않은 세션
      sessPredicate = sessEntry.get("last_talked_at").lessThan(expBase / 1000)
          .or(sessEntry.get("valid").equal(false));
    }

//    logger.trace("#@ sessPredicate [{}]", sessPredicate);

    // #@ SqlPredicate는 sql의 where의 역할
    Collection<DialogSessionV3Portable> scol = sessMap.values(sessPredicate);
    IMap<Long, Integer> talkNextMap = hzc.getMap(MAP.TALK_NEXT);
    IMap<String, Long> devicesMap = hzc.getMap(MAP.DEVICES);

    List<String> removeTalkList = new ArrayList<>();
    List<String> removeClassificationList = new ArrayList<>();

    // 만료된 세션, 토크 갯수
    int expSessCnt = 0;
    int expTalkCnt = 0;
    int expClassificationCnt = 0;

    // #@ sessMap 값이 있다면
    for (DialogSessionV3Portable dsp : scol) {
      DialogSession dialogSession = dsp.getProtobufObj();

      long nanos = dialogSession.getLastTalkedAt().getNanos();
      long msec = nanos / 1000000L;
      long sec = (dialogSession.getLastTalkedAt().getSeconds() * 1000) + msec;
      long sessSec = dialogSession.getLastTalkedAt().getSeconds();

      boolean isExpired = false;
      // 만료된 세션 ID
      long sessKey = -1;
      String deviceId = null;

      // expBase:만료 기준시간, sec:만료시간
      logger.trace("{} : {} ~ remain {} sec", expBase, sec, ((sec - expBase) / 1000));

      // 세션이 유효하지 않은 경우 : 만료
      if (!dialogSession.getValid()) {
        sessKey = dialogSession.getId();
        logger.trace("{} is invalid", sessKey);
        isExpired = true;

        // 만료 기준시간 보다 큰경우 : 만료
      } else if (expBase > sec) {
        sessKey = dialogSession.getId();
        logger.trace("{} is expired", sessKey); // 만료된 id 확인
        isExpired = true;
      }

      // 만료된 경우
      if (isExpired) {
        // 만료된 ClassificationRecord list 가져오기
        List<ClassificationRecord> expClassificationList = new ArrayList<>();
        {
          EntryObject entryObject = new PredicateBuilder().getEntryObject();
          Predicate predicate = entryObject.get("session_id").equal(sessKey);
          Collection<ClassificationRecordPortable> results = classificationMap.values(predicate);

          for (ClassificationRecordPortable crp : results) {
            ClassificationRecord cr = crp.getProtobufObj();
            expClassificationList.add(cr);
            removeClassificationList.add(cr.getOperationSyncId());
            if (!crp.isReadOk()) {
              logger.error("ClassificationRecord '{}' failed. Drop dump.", crp.getInstanceId());
              Utils.dropBinaryDump(crp.getMsg(), crp.getInstanceId());
            }
          }
        }

        // 만료된 Talk list 가져오기
        List<SessionTalk> expSessTalkList = new ArrayList<>();
        {
          EntryObject talkEntry = new PredicateBuilder().getEntryObject();
          Predicate talkPredicate = talkEntry.get("session_id").equal(sessKey);
          Collection<SessionTalkV3Portable> resultTalk = talkMap.values(talkPredicate);

          for (SessionTalkV3Portable stp : resultTalk) {
            expSessTalkList.add(stp.getProtobufObj());
          }
        }

        if (dbAvailable) {
          if (logClassification) {
            sqlClassification.make(expClassificationList);
          }
          if (logTalk) {
            sqlTalk.make(sessSec, dialogSession, expSessTalkList);
          }
          if (logSession) {
            sqlSession.make(sessSec, dialogSession, expSessTalkList.size());
          }
        }

        Map<String, Integer> skilllangs = new HashMap<>();
        // expTalkList 에 값 저장
        for (SessionTalk st : expSessTalkList) {
          String talkMapKey = "#" + sessKey + "-#" + st.getSeq();
          String sl_id = st.getSkill() + "@##@" + st.getLangValue();
          skilllangs.put(sl_id, skilllangs.get(sl_id) == null ? 0 : skilllangs.get(sl_id) + 1);
          removeTalkList.add(talkMapKey);
        }

        if (dialogSession.getValid()) {
          closeSession(dialogSession.getChatbot(), skilllangs, sessKey, dialogSession);
        }

        // Remote DB Appender에 로그객체를 전달한다.
        if (remoteDbLogger.isInfoEnabled()) {
          logger.info("Sending Talk Logs to the Appender.");
          remoteDbLogger.info("Session:{} Talks:{} Classifications:{}",
              dialogSession,
              expSessTalkList,
              expClassificationList);
          logger.info("Sent Session:{} Talks({}) Classifications({}) to the Appender.",
              sessKey,
              expSessTalkList.size(),
              expClassificationList.size());
        }

        // MAP Entry 삭제
        for (String key : removeClassificationList) {
          classificationMap.delete(key);
          logger.trace("...delete classification...[{}]", key);
        }
        classificationMap.flush();
        expClassificationCnt += removeClassificationList.size();
        removeClassificationList.clear();

        for (String key : removeTalkList) {
          talkMap.delete(key);
          logger.trace("...delete talk...[{}]", key);
        }
        talkMap.flush();
        expTalkCnt += removeTalkList.size();
        removeTalkList.clear();

        sessMap.delete(sessKey);
        talkNextMap.delete(sessKey);
        logger.trace("...delete session & talk-next...[{}]", sessKey);
        sessMap.flush();
        talkNextMap.flush();
        expSessCnt++;

        try {
          if (deviceId != null && devicesMap != null) {
            if (devicesMap.get(deviceId) != null
                && devicesMap.get(deviceId).longValue() == sessKey) {
              devicesMap.delete(deviceId);
              logger.trace("...delete device mapping...[{}]", deviceId);
              devicesMap.flush();
            } else {
              logger.trace("Device [{}] is already mapped to new Session[{}].", deviceId,
                  devicesMap.get(deviceId));
            }
          }
        } catch (Exception e) {
          logger.error("Exception with deviceID deletion ", e);
          throw e;
        }
      }
    }

    // 쿼리 수행
    if (dbAvailable) {
      if (logClassification) {
        int[] results = sqlClassification.execute();
        if (logger.isTraceEnabled()) {
          for (int result : results) {
            logger.trace("classification - insert result [{}]", result);
          }
        }
      }

      if (logTalk) {
        int[] results = sqlTalk.execute();
        if (logger.isTraceEnabled()) {
          for (int result : results) {
            logger.trace("talk - insert result [{}]", result);
          }
        }
      }

      if (logSession) {
        int[] results = this.sqlSession.execute();
        if (logger.isTraceEnabled()) {
          for (int result : results) {
            logger.trace("session - insert result [{}]", result);
          }
        }
      }
    }

    totalExpSessCnt += expSessCnt;
    totalExpTalkCnt += expTalkCnt;
    totalExpClassificationCnt += expClassificationCnt;

    if (logger.isDebugEnabled()) {
      // 현재 세션, 토크 갯수
      int sessCnt = 0;
      int talkCnt = 0;
      int classificationCnt = 0;

      if (!chatbot.isEmpty()) {
        EntryObject entryObject = new PredicateBuilder().getEntryObject();
        Collection<DialogSessionV3Portable> sessions = sessMap
            .values(entryObject.get("chatbot").equal(chatbot));
        sessCnt = sessions.size();

        for (DialogSessionV3Portable session : sessions) {
          Predicate predicate = new PredicateBuilder().getEntryObject()
              .get("session_id")
              .equal(session.getProtobufObj().getId());
          talkCnt += talkMap.values(predicate).size();
          classificationCnt += classificationMap.values(predicate).size();
        }
      }

      if (!chatbot.isEmpty() || expSessCnt > 0) {
        logger.debug("[{}] remains s:{} t:{} c:{} / expired s:{} t:{} c:{} took {}ms",
            chatbot, sessCnt, talkCnt, classificationCnt,
            expSessCnt, expTalkCnt, expClassificationCnt, (System.currentTimeMillis() - pT));
      }
    }
  }

  public boolean close() throws SQLException {

    if (sqlTalk != null) {
      sqlTalk.close();
      sqlTalk = null;
    }
    if (sqlSession != null) {
      sqlSession.close();
      sqlSession = null;
    }
    if (conn != null) {
      conn.close();
      conn = null;
    }

    return true;
  }

  /**
   * hzc remove시 DAI에 close를 호출
   *
   * @param chatbot chatbot 이름
   * @param skilllangs skilllangs 정보
   * @param sessKey 세션 키
   */
  private boolean closeSession(String chatbot, Map skilllangs, long sessKey,
      DialogSession dialogSession) {

    logger.debug("#@ closeSession chatbot [{}], key [{}]", chatbot, sessKey);

    // #@ hzc dai map 객체 생성
    IMap<String, DialogAgentInstanceResourcePortable> daiMap = hzc
        .getMap(MAP.DIALOG_AGENT_INSTANCE_RESOURCE);

    // #@ hzc dai skill map 객체 생성
    IMap<String, DialogAgentInstanceResourceSkillPortable> daiSkillMap = hzc
        .getMap(MAP.DIALOG_AGENT_INSTANCE_RESOURCE_SKILL);

    GrpcV3InterfaceManager grpcIf = null;

    // skilllang의 수만큼 삭제
    for (String skilllang : (Set<String>) skilllangs.keySet()) {
      try {
        logger.trace("#@ skilllang [{}]", skilllang);
        String skill = skilllang.split("@##@")[0];
        String lang = skilllang.split("@##@")[1];
        logger.trace("#@ skill [{}], lang [{}]", skill, lang);

        // dialog-agent-instance-resource-skill 조회를 위한 Predicate 설정
        EntryObject skillEntry = new PredicateBuilder().getEntryObject();
        Predicate skillPredicate = skillEntry.get("chatbot").equal(chatbot)
            .and(skillEntry.get("skill").equal(skill));

        Collection<DialogAgentInstanceResourceSkillPortable> skillResult = daiSkillMap
            .values(skillPredicate);

        // iterator 로 변수 설정
        Iterator<DialogAgentInstanceResourceSkillPortable> skillIt = skillResult.iterator();
        // dair 객체 생성
        DialogAgentInstanceResourceSkill skillRes = skillIt.next().getProtobufObj();
        // dairKey 추출
        String dairKey = skillRes.getDairKey();
        logger.trace("#@ dairKey [{}]", dairKey);

        // dialog-agent-instance-resource hzc 조회를 위한 Predicate 설정
        EntryObject dairEntry = new PredicateBuilder().getEntryObject();
        Predicate talkPredicate = dairEntry.get("key").equal(dairKey);

        Collection<DialogAgentInstanceResourcePortable> result = daiMap.values(talkPredicate);

        // iterator 로 변수 설정
        Iterator<DialogAgentInstanceResourcePortable> dairIt = result.iterator();

        // dair 객체 생성
        DialogAgentInstanceResource dair = dairIt.next().getProtobufObj();

        // dair ip와 port 정보 추출
        String ip = dair.getServerIp();
        int port = dair.getServerPort();

        logger.trace("#@ closeSkill session [{}], port [{}], sessKey [{}]", ip, port, sessKey);
        // dair GrpcV3InterfaceManager 객체 생성
        grpcIf = new GrpcV3InterfaceManager(ip, port);

        // dair 삭제
        CloseSkillRequest.Builder closeSkillReq = CloseSkillRequest.newBuilder()
            .setChatbot(chatbot)
            .setSkill(skill)
            .setSession(maum.m2u.common.Dialog.Session.newBuilder()
                .setContext(dialogSession.getContext()).setId(sessKey));

        grpcIf.closeSkill(closeSkillReq.build());
      } catch (Exception e) {
        logger.error("closeSession Exception {} => ", e.getMessage());
        logger.error("closeSession e : ", e);
      } finally {
        if (grpcIf != null) {
          grpcIf.shutdown();
        }
        grpcIf = null;
      }
    }

    return true;
  }

}
