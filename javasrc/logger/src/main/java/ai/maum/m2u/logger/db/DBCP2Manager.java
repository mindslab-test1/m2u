package ai.maum.m2u.logger.db;

import ai.maum.m2u.logger.config.PropertyManager;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBCP2Manager {

  private static final Logger logger = LoggerFactory.getLogger(DBCP2Manager.class);

  public static final String DB_TYPE_UNKNOWN = "_unknown_";
  public static final String DB_TYPE_MYSQL = "mysql";
  public static final String DB_TYPE_ORACLE = "oracle";

  private static DBCP2Manager instance = new DBCP2Manager();
  private DataSource ds;
  private String dbType;

  private DBCP2Manager() {
/*
    String driverClassName = "com.mysql.jdbc.Driver";
    String user = "root";
    String pass = "msl1234~";
    String url = "jdbc:mysql://m2udialogger.cqmd1puapszw.ap-northeast-2.rds.amazonaws.com:3306/m2udialogger";
    int initSize = 5;
    int maxSize = 100;
*/
    try {
      String driverClassName = PropertyManager.getString("logger.driver.classname");
      String user = PropertyManager.getDecrypt("logger.user");
      String pass = PropertyManager.getDecrypt("logger.pass");
      String url = PropertyManager.getDecrypt("logger.url");

      int initSize = PropertyManager.getInt("logger.init.size");
      int maxSize = PropertyManager.getInt("logger.max.size");

      if (driverClassName.contains(DB_TYPE_MYSQL)) {
        dbType = DB_TYPE_MYSQL;
      } else if (driverClassName.contains(DB_TYPE_ORACLE)) {
        dbType = DB_TYPE_ORACLE;
      } else {
        dbType = DB_TYPE_UNKNOWN;
      }

      logger.debug("dbType:{}, driver:{}, url:{}", dbType, driverClassName, url);

      BasicDataSource ds = new BasicDataSource();
      ds.setDriverClassName(driverClassName);
      ds.setUsername(user);
      ds.setPassword(pass);
      ds.setUrl(url);
      ds.setInitialSize(initSize);
      ds.setMaxTotal(maxSize);

      this.ds = ds;
    } catch (Exception e) {
      logger.error("DBCP2Manager e : " , e);
    }
  }

  public static String getDbType() {
    return getInstance().dbType;
  }

  public static DBCP2Manager getInstance() {
    return DBCP2Manager.Singleton.instance;
  }

  public Connection getConnection() throws SQLException {
    return ds.getConnection();
  }

  private static class Singleton {

    private static final DBCP2Manager instance = new DBCP2Manager();
  }
}
