package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableFactory;


public class LoggerPortableFactory implements PortableFactory {

  @Override
  public Portable create(int classId) {
    if (classId == PortableClassId.CHATBOT) {
      return new ChatbotPortable();
    } else if (classId == PortableClassId.DIALOG_SESSION_V3) {
      return new DialogSessionV3Portable();
    } else if (classId == PortableClassId.SESSION_TALK_V3) {
      return new SessionTalkV3Portable();
    } else if (classId == PortableClassId.DIALOG_AGENT_INSTANCE_RESOURCE) {
      return new DialogAgentInstanceResourcePortable();
    } else if (classId == PortableClassId.DIALOG_AGENT_INSTANCE_RESOURCE_SKILL) {
      return new DialogAgentInstanceResourceSkillPortable();
    } else if (classId == PortableClassId.CLASSIFICATION_RECORD) {
      return new ClassificationRecordPortable();
    }else {
      throw new IllegalArgumentException(classId + " unsupported classId");
    }
  }
}
