package ai.maum.m2u.logger.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import maum.m2u.router.v3.Session.DialogSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlSession {

  static final Logger logger = LoggerFactory.getLogger(SqlSession.class);

  private PreparedStatement ptmt = null;
  private String dbType;

  public static final String DB_TYPE_UNKNOWN = "_unknown_";
  public static final String DB_TYPE_MYSQL = "mysql";
  public static final String DB_TYPE_ORACLE = "oracle";

  public boolean init(Connection conn, String db) throws SQLException {
    dbType = db;
    String insSession = "";
    switch (dbType) {
      case DB_TYPE_ORACLE:
      case DB_TYPE_MYSQL:
        insSession = "insert into SessionV3 "
            + "(sessSec, id, userId, deviceId, deviceType, deviceVersion, channel, peer,"
            + " chatbot, intentFinderPolicyName, lastSkill, lastAgentKey, startAt, lastTalkedAt) "
            + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        break;
      default:
        insSession = "";
        break;
    }
    ptmt = conn.prepareStatement(insSession);

    return true;
  }

  public boolean createTable(Connection conn) throws SQLException {
    Statement query = null;
    ResultSet result = null;
    try {
      DatabaseMetaData dbmd = conn.getMetaData();
      query = conn.createStatement();
      String[] types = {"TABLE"};

      String tname = dbType == DB_TYPE_MYSQL ? "SessionV3" : "SESSIONV3";
      result = dbmd.getTables(conn.getCatalog(), conn.getSchema(), tname, types);
      if (!result.next()) {
        logger.debug("{} is not exists", tname);
        StringBuffer sbqy = new StringBuffer();

        switch (dbType) {
          case DB_TYPE_ORACLE:
            sbqy.append("CREATE TABLE " + tname + " ( ")
                .append("sessSec NUMBER(13) NOT NULL, ")
                .append("id NUMBER(20) NOT NULL, ")
                .append("userId VARCHAR2(1000) DEFAULT NULL, ")
                .append("deviceId VARCHAR2(1000) DEFAULT NULL, ")
                .append("deviceType VARCHAR2(1000) DEFAULT NULL, ")
                .append("deviceVersion VARCHAR2(1000) DEFAULT NULL, ")
                .append("channel VARCHAR2(1000) DEFAULT NULL, ")
                .append("peer VARCHAR2(1000) DEFAULT NULL, ")
                .append("chatbot VARCHAR2(1000) DEFAULT NULL, ")
                .append("intentFinderPolicyName VARCHAR2(1000) DEFAULT NULL, ")
                .append("lastSkill VARCHAR2(1000) DEFAULT NULL, ")
                .append("lastAgentKey VARCHAR2(1000) DEFAULT NULL, ")
                .append("startAt DATE NOT NULL, ")
                .append("lastTalkedAt DATE DEFAULT NULL, ")
                .append("CONSTRAINT PK_" + tname + " PRIMARY KEY (sessSec,id) )");
            break;

          case DB_TYPE_MYSQL:
            sbqy.append("CREATE TABLE IF NOT EXISTS `" + tname + "` ( ")
                .append("`sessSec` int(13) NOT NULL, ")
                .append("`id` bigint(20) NOT NULL, ")
                .append("`userId` varchar(1000) DEFAULT NULL, ")
                .append("`deviceId` varchar(1000) DEFAULT NULL, ")
                .append("`deviceType` varchar(1000) DEFAULT NULL, ")
                .append("`deviceVersion` varchar(1000) DEFAULT NULL, ")
                .append("`channel` varchar(1000) DEFAULT NULL, ")
                .append("`peer` varchar(1000) DEFAULT NULL, ")
                .append("`chatbot` varchar(1000) DEFAULT NULL, ")
                .append("`intentFinderPolicyName` varchar(1000) DEFAULT NULL, ")
                .append("`lastSkill` varchar(1000) DEFAULT NULL, ")
                .append("`lastAgentKey` varchar(1000) DEFAULT NULL, ")
                .append("`startAt` datetime NOT NULL, ")
                .append("`lastTalkedAt` datetime DEFAULT NULL, ")
                .append("PRIMARY KEY (`sessSec`,`id`) ) ")
                .append("ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
            break;

          default:
            break;
        }
        query.executeUpdate(sbqy.toString());
      } else {
        logger.debug("{} is exists", tname);
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    return true;
  }

  public boolean make(long sessSec, DialogSession ds, int talkCnt)
      throws SQLException {
    int i = 0;
    long stLong = ds.getStartedAt().getSeconds() * 1000;
    long etLong = ds.getLastTalkedAt().getSeconds() * 1000;
    //Long.parseLong(String.valueOf(ds.getStartedAt().getSeconds()) + "000");
    //long etLong = (new java.util.Date()).getTime();

    ptmt.setLong(++i, sessSec);
    ptmt.setLong(++i, ds.getId());
    ptmt.setString(++i, ds.getUserId());  // 사용자 UNIQUE ID
    ptmt.setString(++i, ds.getDeviceId());  // 디바이스 UNIQUE ID
    ptmt.setString(++i, ds.getDeviceType());  // 디바이스 Type
    ptmt.setString(++i, ds.getDeviceVersion());  // 디바이스 Version
    ptmt.setString(++i, ds.getChannel());  // 서비스 접속 경로
    ptmt.setString(++i, ds.getPeer());  // 원격 PEER의 정보의 정보
    ptmt.setString(++i, ds.getChatbot()); // 현재의 챗봇 정보
    ptmt.setString(++i, ds.getIntentFinderPolicy());  // 세션에서 참조한 IFP 이름
    ptmt.setString(++i, ds.getLastSkill()); // 마지막에 사용한 대화 skill
    ptmt.setString(++i, ds.getLastAgentKey());  // 현재 대화와 관련된 current agent
    ptmt.setTimestamp(++i, new Timestamp(stLong));  // 처음 대화가 시작한 시간
    ptmt.setTimestamp(++i, new Timestamp(etLong));  // 마지막 대화 시간

    if (logger.isTraceEnabled()) {
      logger.trace("#@ sessSec: {} Id: {} UserId: {}"
              + " DeviceId: {} DeviceType: {} DeviceVersion: {} Channel: {} Peer: {}"
              + " Chatbot: {} IntentFinderPolicyName: {} LastSkill: {} LastAgentKey : {}"
              + " start: {} end: {}",
          sessSec, ds.getId(), ds.getUserId(),
          ds.getDeviceId(), ds.getDeviceType(), ds.getDeviceVersion(), ds.getChannel(),
          ds.getPeer(),
          ds.getChatbot(), ds.getIntentFinderPolicy(), ds.getLastSkill(), ds.getLastAgentKey(),
          stLong, etLong);
    }

    ptmt.addBatch();
    ptmt.clearParameters();

    return true;
  }

  public int[] execute() throws SQLException {
    // sql 쿼리 수행
    return ptmt.executeBatch();
  }

  public void close() throws SQLException {
    ptmt.close();
  }

}
