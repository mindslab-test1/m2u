package ai.maum.m2u.logger;

import ai.maum.m2u.common.portable.ClassificationRecordPortable;
import ai.maum.m2u.logger.common.model.NameSpace.MAP;
import ai.maum.m2u.logger.hzc.HazelcastConnector;
import ai.maum.m2u.logger.utils.Utils;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import maum.m2u.router.v3.Intentfinder.ClassificationRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Backup {

  private static final Logger logger = LoggerFactory.getLogger(Backup.class);
  private static HazelcastInstance hzc;

  public static void main(String[] args) {
    boolean backup = false;
    Path backupPath = null;

    if (args.length == 2 && args[0].equalsIgnoreCase("backup")) {
      backupPath = Paths.get(args[1]);
      if (Files.exists(backupPath)) {
        backup = true;
      } else {
        logger.error("Backup root path '{}' is not exist.", backupPath.toAbsolutePath());
        return;
      }
    }

    if (backup) {
      logger.info("Backup into '{}'", backupPath.toAbsolutePath());
    } else {
      logger.info("Checking without backup.");
    }

    // hzc 객체 생성
    if (hzc == null) {
      hzc = HazelcastConnector.getInstance().client;
    }

    IMap<String, ClassificationRecordPortable> classificationMap = hzc.getMap(MAP.CLASSIFICATION);
    Collection<ClassificationRecordPortable> values = classificationMap.values();

    logger.info("ClassificationRecord Total: {}", values.size());
    int count = 0;

    try {
      for (ClassificationRecordPortable crp : values) {
        logger.info("ClassificationRecord check: {}", count);
        try {
          if (crp.isReadOk()) {
            ClassificationRecord cr = crp.getProtobufObj();
            logger.info("ClassificationRecord '{}' is OK.", crp.getInstanceId());
          } else {
            if (backup) {
              logger.info("ClassificationRecord '{}' failed. Dump {}bytes.",
                  crp.getInstanceId(), crp.getMsg().length);
              Utils.dropBinaryDump(crp.getMsg(), crp.getInstanceId());
            } else {
              logger.info("ClassificationRecord '{}' failed.", crp.getInstanceId());
            }
          }
        } catch (Exception e) {
          logger.error("IN LOOP: {}", e.getMessage());
          logger.error("", e);
        }
        count++;
      }
    } catch (Exception e) {
      logger.error("OUT OF LOOP: {}", e.getMessage());
      logger.error("", e);
    }

    HazelcastConnector.getInstance().release();
  }
}

