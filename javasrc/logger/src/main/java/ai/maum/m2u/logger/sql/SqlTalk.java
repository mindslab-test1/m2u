package ai.maum.m2u.logger.sql;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import maum.m2u.common.StatisticsOuterClass.Statistics;
import maum.m2u.router.v3.Session.DialogSession;
import maum.m2u.router.v3.Session.SessionTalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlTalk {

  static final Logger logger = LoggerFactory.getLogger(SqlTalk.class);
  static final int extraOutSize = 4000;

  public static final String DB_TYPE_UNKNOWN = "_unknown_";
  public static final String DB_TYPE_MYSQL = "mysql";
  public static final String DB_TYPE_ORACLE = "oracle";

  private PreparedStatement ptmt = null;
  private String dbType;

  public boolean init(Connection conn, String db) throws SQLException {
    dbType = db;
    String insTalk;
    switch (dbType) {
      case DB_TYPE_ORACLE:
      case DB_TYPE_MYSQL:
        insTalk = "insert into TalkV3 "
            + "(sessSec, seq, sessionId, opSyncId, lang, skill, intent, closeSkill"
            + ", inText, inputType, outText, speechOut, reprompt, extraOut, statusCode"
            + ", dialogResult, startTime, endTime, feedback, transitChatbot"
            + ", opinion, score, isIntervened"
            + ", category1, category2, category3, category4, category5"
            + ", sentiment, reasonOfFail, remarks) "
            + "values (?, ?, ?, ?, ?, ?, ?, ?"
            + ", ?, ?, ?, ?, ?, ?, ?"
            + ", ?, ?, ?, ?, ?"
            + ", ?, ?, ?"
            + ", ?, ?, ?, ?, ?"
            + ", ?, ?, ?)";
        break;
      default:
        insTalk = "";
        break;
    }
    ptmt = conn.prepareStatement(insTalk);

    return true;
  }

  public boolean createTable(Connection conn) throws SQLException {
    Statement query = null;
    ResultSet result = null;
    DatabaseMetaData dbmd = conn.getMetaData();
    query = conn.createStatement();
    String[] types = {"TABLE"};

    String tname = dbType == DB_TYPE_MYSQL ? "TalkV3" : "TALKV3";
    result = dbmd.getTables(conn.getCatalog(), conn.getSchema(), tname, types);
    if (!result.next()) {
      logger.debug("{} is not exists", tname);
      StringBuffer sbqy = new StringBuffer();
      switch (dbType) {
        case DB_TYPE_ORACLE:
          sbqy.append("CREATE TABLE " + tname + " ( ")
              .append("sessSec NUMBER(13) NOT NULL, ")
              .append("seq NUMBER(11) NOT NULL, ")
              .append("sessionId NUMBER (20) NOT NULL, ")
              .append("opSyncId VARCHAR2(100) DEFAULT NULL, ")
              .append("lang VARCHAR2(20) DEFAULT NULL, ")
              .append("skill VARCHAR2(20) DEFAULT NULL, ")
              .append("intent VARCHAR2(20) DEFAULT NULL, ")
              .append("closeSkill CHAR DEFAULT NULL, ")
              .append("inText VARCHAR2(300) DEFAULT NULL, ")
              .append("inputType VARCHAR2(30) DEFAULT NULL, ")
              .append("outText VARCHAR2(1000) DEFAULT NULL, ")
              .append("speechOut VARCHAR2(1000) DEFAULT NULL, ")
              .append("reprompt CHAR DEFAULT NULL, ")
              .append("extraOut VARCHAR2(" + extraOutSize + ") DEFAULT NULL, ")
              .append("statusCode NUMBER(11) DEFAULT NULL, ")
              .append("dialogResult VARCHAR2(100) DEFAULT NULL, ")
              .append("startTime DATE NOT NULL, ")
              .append("endTime DATE DEFAULT NULL, ")
              .append("feedback NUMBER(5) DEFAULT NULL, ")
              .append("accuracy NUMBER(5) DEFAULT NULL, ")
              .append("transitChatbot VARCHAR2(50) DEFAULT NULL, ")
              .append("opinion VARCHAR2(1000) DEFAULT NULL, ")
              .append("score NUMBER (5) DEFAULT NULL, ")
              .append("isIntervened CHAR DEFAULT NULL, ")
              .append("category1 VARCHAR2(100) DEFAULT NULL, ")
              .append("category2 VARCHAR2(100) DEFAULT NULL, ")
              .append("category3 VARCHAR2(100) DEFAULT NULL, ")
              .append("category4 VARCHAR2(100) DEFAULT NULL, ")
              .append("category5 VARCHAR2(100) DEFAULT NULL, ")
              .append("sentiment VARCHAR2(100) DEFAULT NULL, ")
              .append("reasonOfFail VARCHAR2(100) DEFAULT NULL, ")
              .append("remarks VARCHAR2(1000) DEFAULT NULL, ")
              .append("CONSTRAINT PK_" + tname + " ")
              .append("PRIMARY KEY ( sessSec , sessionId , seq ) ) ");
          break;

        case DB_TYPE_MYSQL:
          sbqy.append("CREATE TABLE IF NOT EXISTS `" + tname + "` ( ")
              .append("`sessSec` int(13) NOT NULL, ")
              .append("`seq` int(11) NOT NULL, ")
              .append("`sessionId` bigint(20) NOT NULL, ")
              .append("`opSyncId` varchar(100) DEFAULT NULL, ")
              .append("`lang` varchar(20) DEFAULT NULL, ")
              .append("`skill` varchar(20) DEFAULT NULL, ")
              .append("`intent` varchar(20) DEFAULT NULL, ")
              .append("`closeSkill` tinyint(1) DEFAULT NULL, ")
              .append("`inText` varchar(300) DEFAULT NULL, ")
              .append("`inputType` varchar(30) DEFAULT NULL, ")
              .append("`outText` varchar(1000) DEFAULT NULL, ")
              .append("`speechOut` varchar(1000) DEFAULT NULL, ")
              .append("`reprompt` tinyint(1) DEFAULT NULL, ")
              .append("`extraOut` varchar(" + extraOutSize + ") DEFAULT NULL, ")
              .append("`statusCode` int(11) DEFAULT NULL, ")
              .append("`dialogResult` varchar(100) DEFAULT NULL, ")
              .append("`startTime` datetime NOT NULL, ")
              .append("`endTime` datetime DEFAULT NULL, ")
              .append("`feedback` int(5) DEFAULT NULL, ")
              .append("`accuracy` int(5) DEFAULT NULL, ")
              .append("`transitChatbot` varchar(50) DEFAULT NULL, ")
              .append("`opinion` varchar(1000) DEFAULT NULL, ")
              .append("`score` int(5) DEFAULT NULL, ")
              .append("`isIntervened` tinyint(1) DEFAULT NULL, ")
              .append("`category1` varchar(100) DEFAULT NULL, ")
              .append("`category2` varchar(100) DEFAULT NULL, ")
              .append("`category3` varchar(100) DEFAULT NULL, ")
              .append("`category4` varchar(100) DEFAULT NULL, ")
              .append("`category5` varchar(100) DEFAULT NULL, ")
              .append("`sentiment` varchar(100) DEFAULT NULL, ")
              .append("`reasonOfFail` varchar(100) DEFAULT NULL, ")
              .append("`remarks` varchar(1000) DEFAULT NULL, ")
              .append("PRIMARY KEY (`sessSec`,`sessionId`,`seq`) ) ")
              .append("ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
          break;

        default:
          sbqy.append("");
          break;
      }
      query.executeUpdate(sbqy.toString());
    } else {
      logger.debug("{} is exists", tname);
    }
    return true;
  }

  private String refineExtraOut(String extraOut) {
    String refined;
    try {
      // Json object로 parsing;
      JsonObject json = new JsonParser().parse(extraOut).getAsJsonObject();

      json.getAsJsonObject().remove("speech");
      json.getAsJsonObject().remove("sessionUpdate");
      json.getAsJsonObject().remove("closeSkill");

      refined = json.toString();
    } catch (Exception e) {
      refined = extraOut;
    }
    if (refined.length() > extraOutSize) {
      logger.trace("ExtraOut length: {}", refined.length());
      refined = refined.substring(0, extraOutSize);
    }

    return refined;
  }

  public boolean make(long sessSec, DialogSession ds, List<SessionTalk> expSessTalkList) {
    // seq로 정렬
    expSessTalkList.sort((a, b) -> a.getSeq() > b.getSeq() ? 1 : -1);

    final long sid = ds.getId();
    long stLong, etLong;
    int i;

    for (SessionTalk st : expSessTalkList) {
      stLong = st.getStart().getSeconds() * 1000;
      etLong = st.getEnd().getSeconds() * 1000;
      i = 0;
      try {
        ptmt.setLong(++i, sessSec);
        ptmt.setInt(++i, st.getSeq());  // 순서
        ptmt.setLong(++i, sid); // DialogSession.id
        ptmt.setString(++i, st.getOperationSyncId()); // x-operation-sync-id
        ptmt.setString(++i, st.getLang().getValueDescriptor().getName()); // 언어
        ptmt.setString(++i, st.getSkill()); // 스킬
        ptmt.setString(++i, st.getIntent());  // 현재의 intent
        ptmt.setBoolean(++i, st.getCloseSkill()); // 현재 스킬의 마지막 대화인가?
        ptmt.setString(++i, st.getIn());  // 입력 텍스트
        ptmt.setString(++i, st.getInputType().getValueDescriptor().getName());  // 입력 유형
        ptmt.setString(++i, st.getOut()); // 출력 텍스트
        ptmt.setString(++i, st.getSpeechOut()); // TTS로 나가는 내용이 다를 때
        ptmt.setBoolean(++i, st.getReprompt()); // 재질의 상태인 경우
        ptmt.setString(++i, refineExtraOut(st.getExtraOut())); // Cards, Directives json 문자열
        ptmt.setInt(++i, st.getStatusCode()); // grpc 최종 status
        ptmt.setString(++i, st.getDialogResult().getValueDescriptor().getName()); // 내부 처리 코드
        ptmt.setTimestamp(++i, new Timestamp(stLong));  // 시작 시간
        ptmt.setTimestamp(++i, new Timestamp(etLong));  // 끝 시간
        ptmt.setInt(++i, st.getFeedback()); // *optional* 사용자 피드백
        ptmt.setString(++i, st.getTransitChatbot());  // *optional* 다른 챗봇으로 전달을 위한 챗봇 이름
        ptmt.setString(++i, st.getOpinion());
        /**  통계 자료  **/
        Statistics stat = st.getStatistics();
        // DA 에서 답변에 대한 자체 평가로 0~100로 평가한다.(-1은 평가 안 함, 0은 실패, 1~100)
        ptmt.setInt(++i, stat.getScore().getNumber());
        ptmt.setBoolean(++i, stat.getIsIntervened());  // 외부 상담사가 개입 여부
        ptmt.setString(++i, stat.getCategory1());  // DA에서 처리한 유형 1
        ptmt.setString(++i, stat.getCategory2());  // DA에서 처리한 유형 2
        ptmt.setString(++i, stat.getCategory3());  // DA에서 처리한 유형 3
        ptmt.setString(++i, stat.getCategory4());  // DA에서 처리한 유형 4
        ptmt.setString(++i, stat.getCategory5());  // DA에서 처리한 유형 5
        ptmt.setString(++i, stat.getSentiment());  // DA에서 파악한 감성
        ptmt.setString(++i, stat.getReasonOfFail());  // DA에서 답변 실패 원인
        ptmt.setString(++i, stat.getRemarks());  // DA에서 답변 시 참고 내용 기술. 실패 시는 실패 상세내역

        if (logger.isTraceEnabled()) {
          logger.trace("#@ sessSec: {}, Seq: {}, sid: {}, OperationSyncId: {}, Lang: {}, Skill: {}"
                  + " CloseSkill: {} In: {} InputType: {} Out: {} SpeechOut: {} Reprompt: {}"
                  + " StatusCode: {} DialogResult: {} #@ stLong: {} #@ etLong {} Feedback: {}"
                  + " TransitChatbot: {} Score: {} Category1: {} Category2: {}"
                  + " Sentiment: {} ReasonOfFail: {}",
              sessSec, st.getSeq(), sid, st.getOperationSyncId(),
              st.getLang().getValueDescriptor().getName(), st.getSkill(), st.getCloseSkill(),
              st.getIn(), st.getInputType().getValueDescriptor().getName(),
              st.getOut(), st.getSpeechOut(), st.getReprompt(), st.getStatusCode(),
              st.getDialogResult().getValueDescriptor().getName(), stLong, etLong, st.getFeedback(),
              st.getTransitChatbot(), stat.getScore(), stat.getCategory1(), stat.getCategory2(),
              stat.getSentiment(), stat.getReasonOfFail());
        }

        ptmt.addBatch();
        ptmt.clearParameters();

      } catch (Exception e) {
        logger.error("{} => ", e.getMessage(), e);
      }
    }
    return true;
  }

  public int[] execute() throws SQLException {
    return ptmt.executeBatch();
  }

  public void close() throws SQLException {
    ptmt.close();
  }
}
