package ai.maum.m2u.logger;

import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.DialogSessionV3Portable;
import ai.maum.m2u.logger.common.model.NameSpace.MAP;
import ai.maum.m2u.logger.common.model.NameSpace.Setting;
import ai.maum.m2u.logger.common.model.SessionStat;
import ai.maum.m2u.logger.config.PropertyManager;
import ai.maum.m2u.logger.hzc.HazelcastConnector;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * 주기적으로 세션의 접속 현황을 전달한다.
 */

public class SessionLogger {

  private static final Logger logger = LoggerFactory.getLogger(SessionLogger.class);
  private static final Logger sessionStatusLogger = LoggerFactory.getLogger("session-status");

  // hzc 객체 선언
  private HazelcastInstance hzc;

  public boolean init() throws Exception {
    // hzc 객체 생성
    this.hzc = HazelcastConnector.getInstance().client;

    return true;
  }

  public void log() throws Exception {
    try {
      IMap<Long, DialogSessionV3Portable> sessionMap = hzc.getMap(MAP.SESSION_V3);
      EntryObject sessionEntry;

      SessionStat sessionStat = new SessionStat();

      // sessionLogger process 정리
      // 1. hzc 로 부터 chatbot 목록을 조회 합니다
      // 2.m2u.conf에서 channel 목록을 조회합니다
      // 3.chatbot을 기준으로 전체 사이즈를 조회 합니다
      // 4.chatbot과 channel을 조합한 전체 사이즈를 조회합니다

      // 1. hzc 로 부터 chatbot 목록을 조회 합니다
      IMap<String, ChatbotPortable> chatbotMap = hzc.getMap(MAP.CHATBOT);
      // 2.m2u.conf에서 channel 목록을 조회합니다
      String[] channels = PropertyManager.getStringArray(Setting.LOGGER_SESSION_CHANNELS);

      for (String tempChatbot : chatbotMap.keySet()) {
        // 3.chatbot 기준으로 세션수를 조회 합니다
        sessionEntry = new PredicateBuilder().getEntryObject(); // 초기화를 안해주면 쿼리 에러

        Predicate sessionPredicate = sessionEntry.get("valid").equal(true)
            .and(sessionEntry.get("chatbot").equal(tempChatbot));

        int totalSessCnt = sessionMap.keySet(sessionPredicate).size();

        // 해당 chatbot의 총갯수를 표현할때는 channel이 없습니다
        sessionStat.put(tempChatbot, "", totalSessCnt);

        // 4.chatbot과 channel을 조합한 세션수를 조회 합니다
        for (String channel : channels) {
          sessionEntry = new PredicateBuilder().getEntryObject(); // 초기화를 안해주면 쿼리 에러

          Predicate sessionPredicate2 = sessionEntry.get("valid").equal(true)
              .and(sessionEntry.get("chatbot").equal(tempChatbot))
              .and(sessionEntry.get("channel").equal(channel));

          int totalSessCntWithChannel = sessionMap.keySet(sessionPredicate2).size();

          sessionStat.put(tempChatbot, channel, totalSessCntWithChannel);
        }
      }

      // 세션정보 출력 Appender로 로그 객체를 전달한다.
      if (sessionStatusLogger.isInfoEnabled()) {
        logger.info("Sending Session Summary to the Appender.");
        sessionStatusLogger.info("Summary: {} at {}", sessionStat, new Date());
        logger.info("Sent Session Summary({}) to the Appender.", sessionStat.size());
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }
}
