package ai.maum.m2u.logger;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import ai.maum.m2u.logger.common.model.NameSpace.Setting;
import ai.maum.m2u.logger.config.PropertyManager;
import ai.maum.m2u.logger.hzc.HazelcastConnector;
import ai.maum.rpc.ResultStatus;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import java.sql.SQLException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import maum.rpc.Status;
import maum.rpc.Status.ProcessOfM2u;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LoggerServer {

  private static final Logger logger = LoggerFactory.getLogger(LoggerServer.class);

  private static boolean isRunning = true;

  static class LoggerTask {

    private static final String LoggerLockName;
    private static final String TaskName = "logger-task";

    private final Random rand = new Random();

    private HazelcastInstance hzc;
    private boolean logRouter = false;
    private RouterLogger routerLogger;
    private Timer loggerTaskTimer;
    private long periodMillis;

    static {
      ResultStatus.setModuleAndProcess(Status.Module.M2U, ProcessOfM2u.M2U_LOGGER.getNumber());
      String lockName = PropertyManager.getString(Setting.LOGGER_LOCK_NAME);
      LoggerLockName = lockName == null ? "logger-lock" : lockName;
    }

    public void init(long startSec, long periodSec) {
      hzc = HazelcastConnector.getInstance().client;

      logRouter = PropertyManager.getBoolean(Setting.LOGGER_ROUTER_ENABLE);
      logger.info("Log Router: {}", logRouter);
      if (logRouter) {
        routerLogger = new RouterLogger();
      }

      periodMillis = periodSec * 1000;

      ILock loggerLock = hzc.getLock(LoggerLockName);

      try {
        loggerLock.lock();
        if (logRouter) {
          try {
            routerLogger.init();
          } catch (Exception e) {
            logger.error("Router Logger Init failed. {} => ", e.getMessage(), e);
          }
        }
      } finally {
        loggerLock.unlock();
      }

      loggerTaskTimer = new Timer(TaskName, false);
      loggerTaskTimer.schedule(new Task(), startSec * 1000);

      logger.info("LoggerTask::init() finished.");
    }

    private class Task extends TimerTask {

      @Override
      public void run() {
        long start = System.currentTimeMillis();

        ILock loggerLock = hzc.getLock(LoggerLockName);
        try {
          logger.trace("LoggerTask::run() try Lock");
          if (loggerLock.tryLock(periodMillis, MILLISECONDS)) {
            logger.debug("#@ run LoggerTask");

            start = System.currentTimeMillis();

            if (logRouter) {
              try {
                routerLogger.hzcProcess();
              } catch (SQLException e) {
                logger.error("{} => ", e.getMessage(), e);
                try {
                  routerLogger.close();
                } catch (SQLException e2) {
                  logger.error("{} => ", e.getMessage(), e2);
                }
              }
            }
          } else {
            logger.trace("LoggerTask::run() Lock failed. skip this time.");
          }
        } catch (InterruptedException e) {
          logger.error("LoggerTask::run() exception ", e);
        } catch (Exception e1) {
          logger.error("LoggerTask::run() exception ", e1);
        } finally {
          logger.trace("LoggerTask::run() wait to unlock.");

          long delay;

          if (loggerLock.isLockedByCurrentThread()) {
            long diff = System.currentTimeMillis() - start + 5;

            delay = periodMillis;
            if (diff < periodMillis) {
              logger.trace("LoggerTask::run() keep Lock for {}ms to fulfill {}ms period.",
                  periodMillis - diff,
                  periodMillis);
              try {
                MILLISECONDS.sleep(periodMillis - diff);
                logger.trace("LoggerTask::run() period fulfilled.");
              } catch (InterruptedException e) {
                logger.error("init e : ", e);
              }
            } else {
              delay -= diff % periodMillis;
            }
            loggerLock.unlock();
            logger.trace("LoggerTask::run() Lock unlock.");

            if (loggerLock.isLocked()) {
              if (delay < 100) {
                delay = periodMillis;
              }
              delay -= rand.nextInt(100);
            } else {
              delay = 3;
            }
          } else {
            delay = 3;
          }

          logger.trace("LoggerTask::run() set schedule for next turn.");
          loggerTaskTimer.schedule(new Task(), delay);

          logger.trace("LoggerTask::run() Locked: {}, Still have the lock: {}",
              loggerLock.getLockCount(),
              loggerLock.isLockedByCurrentThread());

          logger.debug("LoggerTask::run() next delay {} ms.", delay);
        }
      }
    }
  }

  static class SessionTask {

    private static final String SessionLockName;
    private static final String TaskName = "session-task";

    private final Random rand = new Random();

    private HazelcastInstance hzc;

    private SessionLogger sessionLogger;

    private Timer sessionTaskTimer;

    private long periodMillis;

    static {
      String lockName = PropertyManager.getString(Setting.SESSION_LOCK_NAME);
      SessionLockName = lockName == null ? "session-lock" : lockName;
    }

    public void init(long startSec, long periodSec) {
      hzc = HazelcastConnector.getInstance().client;

      periodMillis = periodSec * 1000;

      ILock sessionLock = hzc.getLock(SessionLockName);
      try {
        sessionLock.lock();
        sessionLogger = new SessionLogger();
        sessionLogger.init();
      } catch (Exception e) {
        logger.error("{} => ", e.getMessage(), e);
      } finally {
        sessionLock.unlock();
      }

      sessionTaskTimer = new Timer(TaskName, false);
      sessionTaskTimer.schedule(new Task(), startSec * 1000);

      logger.info("SessionTask::init() finished.");
    }

    private class Task extends TimerTask {

      @Override
      public void run() {
        long start = System.currentTimeMillis();

        ILock sessionLock = hzc.getLock(SessionLockName);
        try {
          logger.trace("SessionTask::run() Lock");
          if (sessionLock.tryLock(periodMillis, MILLISECONDS)) {
            logger.debug("#@ run SessionTask");

            start = System.currentTimeMillis();

            try {
              sessionLogger.log();
            } catch (Exception e) {
              logger.error("{} => ", e.getMessage(), e);
            }
          } else {
            logger.trace("SessionTask::run() Lock failed. skip this time.");
          }
        } catch (InterruptedException e) {
          logger.error("run e : ", e);
        } finally {
          long delay;

          if (sessionLock.isLockedByCurrentThread()) {
            long diff = System.currentTimeMillis() - start + 5;

            delay = periodMillis;
            if (diff < periodMillis) {
              logger.trace("SessionTask::run() keep Lock for {}ms to fulfill {}ms period.",
                  periodMillis - diff,
                  periodMillis);
              try {
                MILLISECONDS.sleep(periodMillis - diff);
                logger.trace("SessionTask::run() period fulfilled.");
              } catch (InterruptedException e) {
                logger.error("run e : ", e);
              }
            } else {
              delay -= diff % periodMillis;
            }

            sessionLock.unlock();
            logger.debug("SessionTask::run() Lock unlock");
            if (sessionLock.isLocked()) {
              if (delay < 100) {
                delay = periodMillis;
              }
              delay -= rand.nextInt(100);
            } else {
              delay = 3;
            }
          } else {
            delay = 3;
          }

          sessionTaskTimer.schedule(new Task(), delay);

          logger.debug("SessionTask::run() next delay {} ms.", delay);
        }
      }
    }
  }

  public static void main(String[] args) {
    //시작 초 설정
    long startSec = PropertyManager.getInt(Setting.LOGGER_STARTSEC);
    // Logging 주기 설정
    long periodSec = PropertyManager.getInt(Setting.LOGGER_PERIODSEC);
    // 세션정보 전송 주기 설정
    long sessionPeriodSec = PropertyManager.getInt(Setting.LOGGER_SESSION_PERIODSEC);

    logger.debug("#@ startSec = {} periodSec = {} sessionPeriodSec = {}",
        startSec,
        periodSec,
        sessionPeriodSec);

    // Logger Task 생성 및 스케줄 설정
    LoggerTask loggerTask = new LoggerTask();
    loggerTask.init(startSec, periodSec);

    // 세션정보 전송 Task 생성 및 스케줄 설정
    SessionTask sessionTask = new SessionTask();
    sessionTask.init(startSec, sessionPeriodSec);

    while (isRunning) {
      try {
        MILLISECONDS.sleep(10 * 1000L);
      } catch (InterruptedException e) {
        logger.error("{} => ", e.getMessage(), e);
      }
    }
  }


}
