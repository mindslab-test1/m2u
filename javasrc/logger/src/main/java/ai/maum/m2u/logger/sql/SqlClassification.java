package ai.maum.m2u.logger.sql;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import maum.m2u.router.v3.Intentfinder.ClassificationRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlClassification {

  static final Logger logger = LoggerFactory.getLogger(SqlClassification.class);

  private PreparedStatement ptmt = null;
  private String dbType;

  public static final String DB_TYPE_UNKNOWN = "_unknown_";
  public static final String DB_TYPE_MYSQL = "mysql";
  public static final String DB_TYPE_ORACLE = "oracle";

  public boolean init(Connection conn, String db) throws SQLException {
    dbType = db;
    String insClassification = "";
    switch (dbType) {
      case DB_TYPE_ORACLE:
      case DB_TYPE_MYSQL:
        insClassification = "insert into Classification "
            + "(sessionId, opSyncId, policyName, chatbot, inText, skill, classified, transit,"
            + " replacement, classifiedAt)"
            + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        break;
      default:
        insClassification = "";
        break;
    }
    ptmt = conn.prepareStatement(insClassification);

    return true;
  }

  public boolean createTable(Connection conn) throws SQLException {
    Statement query = null;
    ResultSet result = null;
    try {
      DatabaseMetaData dbmd = conn.getMetaData();
      query = conn.createStatement();
      String[] types = {"TABLE"};

      String tname = dbType == DB_TYPE_MYSQL ? "Classification" : "CLASSIFICATION";
      result = dbmd.getTables(conn.getCatalog(), conn.getSchema(), tname, types);
      if (!result.next()) {
        logger.debug("{} is not exists", tname);
        StringBuffer sbqy = new StringBuffer();

        switch (dbType) {
          case DB_TYPE_ORACLE:
            sbqy.append("CREATE TABLE " + tname + " ( ")
                .append("sessionId NUMBER(20) NOT NULL, ")
                .append("opSyncId VARCHAR2(100) NOT NULL, ")
                .append("policyName VARCHAR2(100) DEFAULT NULL, ")
                .append("chatbot VARCHAR2(50) DEFAULT NULL, ")
                .append("inText VARCHAR2(300) DEFAULT NULL, ")
                .append("skill VARCHAR2(20) DEFAULT NULL, ")
                .append("classified VARCHAR2(4000) DEFAULT NULL, ")
                .append("transit VARCHAR2(50) DEFAULT NULL, ")
                .append("replacement VARCHAR2(300) DEFAULT NULL, ")
                .append("classifiedAt DATE DEFAULT NULL, ")
                .append("CONSTRAINT PK_" + tname + " PRIMARY KEY (sessionId, opSyncId) )");
            break;

          case DB_TYPE_MYSQL:
            sbqy.append("CREATE TABLE IF NOT EXISTS `" + tname + "` ( ")
                .append("`sessionId` bigint(20) NOT NULL, ")
                .append("`opSyncId` varchar(100) NOT NULL, ")
                .append("`policyName` varchar(100) DEFAULT NULL, ")
                .append("`chatbot` varchar(50) DEFAULT NULL, ")
                .append("`inText` varchar(300) DEFAULT NULL, ")
                .append("`skill` varchar(20) DEFAULT NULL, ")
                .append("`classified` varchar(4000) DEFAULT NULL, ")
                .append("`transit` varchar(50) DEFAULT NULL, ")
                .append("`replacement` varchar(300) DEFAULT NULL, ")
                .append("`classifiedAt` datetime DEFAULT NULL, ")
                .append("PRIMARY KEY (`sessionId`, `opSyncId`) ) ")
                .append("ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
            break;

          default:
            break;
        }
        query.executeUpdate(sbqy.toString());
      } else {
        logger.debug("{} is exists", tname);
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    return true;
  }

  private String getJsonString(Message msg) {
    try {
      return JsonFormat.printer().print(msg);
    } catch (InvalidProtocolBufferException e) {
      return msg.toString();
    }
  }

  public boolean make(List<ClassificationRecord> records) throws SQLException {
    // 정렬
    records.sort((a, b) -> {
      if (a.getClassifiedAt().getSeconds() == b.getClassifiedAt().getSeconds()) {
        return a.getClassifiedAt().getNanos() > b.getClassifiedAt().getNanos() ? 1 : -1;
      }
      return a.getClassifiedAt().getSeconds() > b.getClassifiedAt().getSeconds() ? 1 : -1;
    });

    int i;
    long atLong;

    for (ClassificationRecord cr : records) {
      try {
        atLong = cr.getClassifiedAt().getSeconds() * 1000;
        i = 0;

        ptmt.setLong(++i, cr.getSessionId()); // 세션 ID
        ptmt.setString(++i, cr.getOperationSyncId());  // x-operation-sync-id
        ptmt.setString(++i, cr.getPolicyName());  // 의도파악 정책의 이름
        ptmt.setString(++i, cr.getChatbot()); // 호출하는 챗봇의 이름
        ptmt.setString(++i, cr.getUtter());  // 의도파악의 대상이 되는 발화
        if (cr.hasSkill()) {
          ptmt.setString(++i, cr.getSkill().getSkill());  // 의도파악의 대상이 되는 발화
          ptmt.setString(++i, getJsonString(cr.getSkill())); // 식별된 스킬 및 intent의 목록
          ptmt.setNull(++i, Types.VARCHAR); // 다른 챗봇으로 전이하는 명령어의 구조
        } else {
          ptmt.setNull(++i, Types.VARCHAR); // 의도파악의 대상이 되는 발화
          ptmt.setNull(++i, Types.VARCHAR); // 식별된 스킬 및 intent의 목록
          ptmt.setString(++i, getJsonString(cr.getTransit())); // 다른 챗봇으로 전이하는 명령어의 구조
        }
        ptmt.setString(++i, getJsonString(cr.getReplace()));  // 의도파악 과정에서 대체한 문장에 대한 기록
        ptmt.setTimestamp(++i, new Timestamp(atLong));  // 의도파악를 수행한 시간

        if (logger.isTraceEnabled()) {
          logger.trace("#@ SessId: {} OpSyncId: {} PolicyName: {} chatbot: {} InText: {} Skill: {}",
              cr.getSessionId(), cr.getOperationSyncId(), cr.getPolicyName(), cr.getChatbot(),
              cr.getUtter(), cr.getSkill().getSkill());
        }

        ptmt.addBatch();
        ptmt.clearParameters();
      } catch (Exception e) {
        logger.error("{} => ", e.getMessage(), e);
      }
    }

    return true;
  }

  public int[] execute() throws SQLException {
    // sql 쿼리 수행
    return ptmt.executeBatch();
  }

  public void close() throws SQLException {
    ptmt.close();
  }

}
