package ai.maum.m2u.service;

import com.google.protobuf.Empty;
import com.google.protobuf.Struct;
import com.google.protobuf.util.JsonFormat;
import io.grpc.stub.StreamObserver;
import maum.brain.idr.ImageRecognitionServiceGrpc.ImageRecognitionServiceImplBase;
import maum.brain.idr.Idr.ImageRecognitionResult;
import maum.brain.idr.Idr.RecognizeImageRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageRecognitionImpl extends ImageRecognitionServiceImplBase {

  static final Logger logger = LoggerFactory.getLogger(ImageRecognitionImpl.class);

  @Override
  public StreamObserver<RecognizeImageRequest> recognizeImage(
      final StreamObserver<ImageRecognitionResult> responseObserver) {
    logger.info("===== Grpc Call :: ImageRecognitionImpl.recognizeImage");

    return new StreamObserver<RecognizeImageRequest>() {

      RecognizeImageRequest.Builder param = null;
      byte[] bytes;

      @Override
      public void onNext(RecognizeImageRequest value) {
        if (value.hasParam()) {
          param = value.toBuilder();
        } else if (param != null) {
          byte[] valueByte = value.getImgContent().toByteArray();
          byte[] tempByte = null;
          if(bytes != null) {
            tempByte = bytes.clone();
            bytes = new byte[tempByte.length + valueByte.length];
            bytes = tempByte;

            logger.debug("valueByte = {}", valueByte.length);
            logger.debug("tempByte = {}", tempByte.length);
          }else{
            bytes = valueByte;
          }
        } else {
          /* exception call 해야하는데 모르겠음 */
        }
      }

      @Override
      public void onError(Throwable t) {
        logger.error("Encountered error in recordRoute", t);

      }

      @Override
      public void onCompleted() {
        try {
          /*
          * to-do
          * image transfer Recognize Module
          *
          * */
          //logger.debug("recognizeImage.onCompleted request byte :: {}", bytes);

          ImageRecognitionResult.Builder imageRecognitionResult = ImageRecognitionResult
              .newBuilder();
          Struct.Builder struct = Struct.newBuilder();
          String json = null;
          json = JsonFormat.printer().includingDefaultValueFields().print(Empty.newBuilder());
          JsonFormat.parser().ignoringUnknownFields().merge(json, struct);

          imageRecognitionResult.setImageClass("Classification001");
          imageRecognitionResult.addAnnotatedTexts("mindsLab");
          imageRecognitionResult.addAnnotatedTexts("hana");
          imageRecognitionResult.addAnnotatedTexts("bank");
          imageRecognitionResult.addAnnotatedTexts("customer");
          imageRecognitionResult.setMeta(struct);

          responseObserver.onNext(imageRecognitionResult.build());
          responseObserver.onCompleted();
        } catch (Exception e) {
          logger.error("{} => ", e.getMessage(), e);
        }
      }
    };
  }

}
