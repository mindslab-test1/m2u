package ai.maum.m2u.service;

import ai.maum.m2u.test.TestFinalValue;
import com.google.protobuf.Empty;
import com.google.protobuf.ListValue;
import com.google.protobuf.Struct;
import com.google.protobuf.Timestamp;
import com.google.protobuf.Value;
import com.google.protobuf.util.JsonFormat;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.common.UserOuterClass.User;
import maum.m2u.map.Authentication.*;
import maum.m2u.map.AuthenticationProviderGrpc.AuthenticationProviderImplBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthenticationImpl extends AuthenticationProviderImplBase {

  static final Logger logger = LoggerFactory.getLogger(AuthenticationImpl.class);


  @Override
  public void signIn(SignInPayload request, StreamObserver<SignInResultPayload> responseObserver) {
    logger.info("===== Grpc Call :: AuthenticationImpl.signIn");
    try {
      SignInResultPayload.Builder signinResultPayload = SignInResultPayload.newBuilder();

      if (TestFinalValue.FIX_USER_KEY.equals(request.getUserKey()) &&
          TestFinalValue.FIX_USER_PASS.equals(request.getPassPhrase())) {
        signinResultPayload.setAuthSuccess(
            AuthTokenPayload.newBuilder()
                .setAuthToken(TestFinalValue.FIX_AUTH_TOKEN)
                .setMeta(
                    Struct.newBuilder().putFields("data1",
                        Value.newBuilder().setStringValue("value1").build())
                ));
      } else {
        signinResultPayload.setAuthFailure(
            AuthFailurePayload.newBuilder()
                .setResCode("1111")
                .setMessage("Fail signIn :: Please Check userKey or passphrase")
                .setDetailMessage("detail message")
                .build());
      }

      responseObserver.onNext(signinResultPayload.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void multiFactorVerify(MultiFactorVerifyPayload request,
      StreamObserver<SignInResultPayload> responseObserver) {
    logger.info("===== Grpc Call :: AuthenticationImpl.signIn");

    try {
      SignInResultPayload.Builder signinResultPayload = SignInResultPayload.newBuilder();
      signinResultPayload.setAuthSuccess(
          AuthTokenPayload.newBuilder()
              .setAuthToken(TestFinalValue.FIX_AUTH_TOKEN)
              .setMultiFactor(true)
              .setMeta(
                  Struct.newBuilder().putFields("data1",
                      Value.newBuilder().setStringValue("value1").build())
              ))
          .build();
      responseObserver.onNext(signinResultPayload.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void signOut(SignOutPayload request,
      StreamObserver<SignOutResultPayload> responseObserver) {
    logger.info("===== Grpc Call :: AuthenticationImpl.signOut");

    try {
      responseObserver.onNext(null);
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void updateUserSettings(UserSettings request,
      StreamObserver<UserSettings> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.updateUserSettings");

    try {

      UserSettings.Builder userSettings = UserSettings.newBuilder();

      if (TestFinalValue.FIX_AUTH_TOKEN.equals(request.getAuthToken())) {
        // echo로 구현
        userSettings.setAuthToken(request.getAuthToken()).setSettings(request.getSettings());
      } else {
        throw new Exception("Un Valid Auth Token");
      }

      responseObserver.onNext(userSettings.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  @Override
  public void getUserSettings(UserKey request, StreamObserver<UserSettings> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.getUserSettings");

    try {

      UserSettings.Builder userSettings = UserSettings.newBuilder();

      // 임의의 데이터 return
      Struct.Builder sturct = Struct.newBuilder();
      sturct.putFields("userkey", Value.newBuilder().setStringValue("getUserSettings").build());
      sturct.putFields("passphrase", Value.newBuilder().setStringValue("1234").build());
      sturct.putFields("authParams", Value.newBuilder().setListValue(ListValue.newBuilder()).build());

      userSettings.setAuthToken(request.getAuthToken()).setSettings(sturct);

      responseObserver.onNext(userSettings.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}
