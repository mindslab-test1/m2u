package ai.maum.m2u.test;

import ai.maum.m2u.service.AuthenticationImpl;
import ai.maum.m2u.service.ImageRecognitionImpl;
import com.google.protobuf.ByteString;
import com.google.protobuf.util.JsonFormat;
import io.grpc.testing.GrpcServerRule;
import maum.brain.idr.ImageRecognitionServiceGrpc;
import maum.brain.idr.Idr.ImageFormat;
import maum.brain.idr.Idr.ImageRecognitionParam;
import maum.brain.idr.Idr.ImageRecognitionResult;
import maum.brain.idr.Idr.RecognizeImageRequest;
import maum.common.LangOuterClass.Lang;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@RunWith(JUnit4.class)
public class MapAuthServerTest {

  static final Logger logger = LoggerFactory.getLogger(MapAuthServerTest.class);

  @Rule
  public final GrpcServerRule grpcServerRule = new GrpcServerRule().directExecutor();

  @Test
  public void authenticationImpl_replyMessage() throws Exception {
    // Add the service to the in-process server.
    grpcServerRule.getServiceRegistry().addService(new AuthenticationImpl());
    grpcServerRule.getServiceRegistry().addService(new ImageRecognitionImpl());

    /*AuthenticationProviderGrpc.AuthenticationProviderBlockingStub providerBlockingStubStub;
    providerBlockingStubStub = AuthenticationProviderGrpc
        .newBlockingStub(grpcServerRule.getChannel());

    SigninResultPayload signinResultPayload = providerBlockingStubStub
        .signIn(SignInPayload.newBuilder()
            .setUserkey(TestFinalValue.FIX_USER_KEY).setPassphrase(TestFinalValue.FIX_USER_PASS).build());
    logger.debug("signinResultPayload :: {}",
        JsonFormat.printer().includingDefaultValueFields().print(signinResultPayload));

    SignOutResultPayload signOutResultPayload = providerBlockingStubStub
        .signOut(SignOutPayload.newBuilder()
            .setUserKey(TestFinalValue.FIX_USER_KEY).setAuthToken(TestFinalValue.FIX_AUTH_TOKEN).build());
    logger.debug("signOutResultPayload :: {}",
        JsonFormat.printer().includingDefaultValueFields().print(signOutResultPayload));

    SignOnResultPayload signOnResultPayload = providerBlockingStubStub
        .signOn(SignOnPayload.newBuilder().build());
    logger.debug("signOnResultPayload :: {}",
        JsonFormat.printer().includingDefaultValueFields().print(signOnResultPayload));

    IsValidResponse isValid = providerBlockingStubStub
        .isValid(IsValidRequest.newBuilder().setAuthToken(TestFinalValue.FIX_AUTH_TOKEN).build());
    logger
        .debug("isValid :: {}", JsonFormat.printer().includingDefaultValueFields().print(isValid));

    GetUserInfoResponse getUserInfoResponse = providerBlockingStubStub.getUserInfo(
        GetUserInfoRequest.newBuilder().setAccessToken(TestFinalValue.FIX_ACCESS_TOKEN).build());
    logger.debug("getUserInfoResponse :: {}",
        JsonFormat.printer().includingDefaultValueFields().print(getUserInfoResponse));*/


    ImageRecognitionServiceGrpc.ImageRecognitionServiceStub imageRecognitionServiceStub;
    imageRecognitionServiceStub = ImageRecognitionServiceGrpc.newStub(grpcServerRule.getChannel());

    // 비동기 처리를 위해서 CountDownLatch를 사용하였습니다.
    final CountDownLatch finishLatch = new CountDownLatch(1);
    final ImageRecognitionResult.Builder imageRecognitionResult = ImageRecognitionResult.newBuilder();

    // 서버에서 보내는 메시지를 처리하기 위한 옵져버를 생성합니다.
    io.grpc.stub.StreamObserver<ImageRecognitionResult> responseObserver =
        new io.grpc.stub.StreamObserver<ImageRecognitionResult>() {

          @Override
          public void onNext(ImageRecognitionResult result) {
            // 여기서는 양방향 스트림이 아니기때문에 정상적인 경우, onNext()가 한번씩만 호출됩니다.
            //logger.info("A message received from server: {}", result);
            imageRecognitionResult.setImageClass(result.getImageClass());
            imageRecognitionResult.addAllAnnotatedTexts(result.getAnnotatedTextsList());
            imageRecognitionResult.setMeta(result.getMeta());
          }

          @Override
          public void onError(Throwable t) {
            // 서버에서 onError()를 호출하거나 내부 통신장애가 발생하면 호출됩니다.
            logger.error("RecordRoute Failed: {}", io.grpc.Status.fromThrowable(t));
            finishLatch.countDown();
          }

          @Override
          public void onCompleted() {
            // 메시지 전송이 완료되면 서버에서 onCompleted()를 호출합니다.
            finishLatch.countDown();
          }

        };

    // 서버의 응답을 기다리기 위해서 비동기 stub을 사용해야 합니다.
    io.grpc.stub.StreamObserver<RecognizeImageRequest> requestObserver = imageRecognitionServiceStub
        .recognizeImage(responseObserver);
    FileInputStream fis = null;
    RecognizeImageRequest req;

    try {
      fis = new FileInputStream("/home/minds/Downloads/test.jpg");
      byte[] buffer = new byte[40960];

      ImageRecognitionParam.Builder param = ImageRecognitionParam.newBuilder();
      param.setImageFormat(ImageFormat.valueOf("JPEG"));
      param.setLang(Lang.ko_KR);
      param.setRefVertexX(0.052083f);
      param.setRefVertexY(0.046296f);
      param.setSymmCrop(true);
      param.setWidth(250);
      param.setHeight(200);

      req = RecognizeImageRequest.newBuilder().setParam(param).build();
      requestObserver.onNext(req);

      while ((fis.read(buffer)) > 0) {
        req = RecognizeImageRequest.newBuilder().setImgContent(ByteString.copyFrom(buffer)).build();
        requestObserver.onNext(req);
      }
      // 메시지 전송이 완료되면 onCompleted()를 호출하여 서버에 종료 상황을 알립니다.
      requestObserver.onCompleted();

      // finishLatch를 사용하여 최대 1분간 blocking 하도록 합니다.
      if (!finishLatch.await(1, TimeUnit.MINUTES)) {
        logger.warn("Can not finish within 1 MINUTE");
      }
      logger.debug("recognizeImage :: {}",
          JsonFormat.printer().includingDefaultValueFields().print(imageRecognitionResult));

    } catch (RuntimeException e) {
      requestObserver.onError(e);
    } catch (FileNotFoundException e) {
      logger.error("authenticationImpl_replyMessage e : " , e);
    } catch (IOException e) {
      logger.error("authenticationImpl_replyMessage e : " , e);
    }finally {
      try {
        if (fis != null) {
          fis.close();
        }
      } catch (IOException e) {
        logger.error("authenticationImpl_replyMessage e : " , e);
      }
    }
  }
}
