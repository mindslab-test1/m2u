package ai.maum.m2u.service;

import ai.maum.m2u.test.TestFinalValue;
import com.google.protobuf.Empty;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import maum.m2u.common.UserOuterClass.User;
import maum.m2u.map.Authentication.AuthTokenPayload;
import maum.m2u.map.Authentication.GetUserInfoRequest;
import maum.m2u.map.Authentication.GetUserInfoResponse;
import maum.m2u.map.Authentication.IsValidRequest;
import maum.m2u.map.Authentication.IsValidResponse;
import maum.m2u.map.AuthorizationProviderGrpc.AuthorizationProviderImplBase;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unchecked")
public class AuthorizationImpl extends AuthorizationProviderImplBase {

  static final Logger logger = LoggerFactory.getLogger(AuthorizationImpl.class);

  @Override
  public void isValid(IsValidRequest request, StreamObserver<IsValidResponse> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.isValid");

    try {

      IsValidResponse.Builder isValidResponse = IsValidResponse.newBuilder();

      if (TestFinalValue.FIX_AUTH_TOKEN.equals(request.getAuthToken())) {
        isValidResponse.setIsValid(true);
        isValidResponse.setAccessToken(TestFinalValue.FIX_ACCESS_TOKEN);

        logger.debug("System.currentTimeMillis() {}",System.currentTimeMillis());

        // 현재의 시간을 기준으로 100초를 더한 뒤에 리턴한다.
        isValidResponse.setExpiredAt(Timestamps.fromMillis(
            System.currentTimeMillis() + 100 * 1000));
        logger.debug("timestamp {}", isValidResponse.getExpiredAt());

      } else {
        throw new Exception("Un Valid Auth Token");
      }

      responseObserver.onNext(isValidResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }

  /**
   * $MAUM_ROOT/db/auth-dummy/user-meta.json 파일의 내용을 불러옵니다.
   * 파일이 존재하지 않으면 user-meta.json 파일을 생성후 내용을 리턴합니다.
   *
   * @return user-meta.json 의 내용
   */
  private String getUserMetaJson() {
    StringBuilder tempUserMeta = new StringBuilder();
    try {
      //파일 객체 생성
      File file = new File(System.getenv("MAUM_ROOT") + "/db/auth-dummy/user-meta.json");

      //입력 스트림 생성
      FileReader filereader = new FileReader(file);
      int singleCh = 0;
      while ((singleCh = filereader.read()) != -1) {
        System.out.print((char) singleCh);
        tempUserMeta.append((char) singleCh);
      }

      filereader.close();
    } catch (FileNotFoundException e) {
      logger.error("{} => ", Status.fromThrowable(e), e, null);
      createDirectoryJson();
    } catch (IOException e) {
      logger.error("{} => ", Status.fromThrowable(e), e, null);
      return "";
    }
    return tempUserMeta.toString();
  }

  private void createDirectoryJson() {
    logger.debug("#@ START createDirectoryJson");
    // 파일이 없다면 생성
    // 폴더생성
    Path dbFile = Paths.get(System.getenv("MAUM_ROOT"), "db", "auth-dummy", "user-meta.json");
    try {
      if (!Files.exists(dbFile.getParent())) {
        Files.createDirectories(dbFile.getParent());
      }
    } catch (Exception exp) {
      logger.error("#@ exp [{}]", exp);
      // throw new IOException("create Directories " + dbFile.getParent() + " Fail!");
    }
    // json 생성
    JSONObject obj1 = new JSONObject();
    obj1.put("id", "mindslab");
    obj1.put("user_no", "1");
    obj1.put("nickname", "마인즈랩");

    JSONObject obj2 = new JSONObject();
    obj2.put("id", "m2u");
    obj2.put("user_no", "2");
    obj2.put("nickname", "마음투유");

    JSONObject obj3 = new JSONObject();
    obj3.put("id", "mlt");
    obj3.put("user_no", "3");
    obj3.put("nickname", "엠엘티");

    JSONArray list = new JSONArray();
    list.add(obj1);
    list.add(obj2);
    list.add(obj3);

    JSONObject obj = new JSONObject();
    obj.put("meta_info", list);

    try {
      FileWriter file = new FileWriter(System.getenv("MAUM_ROOT") + "/db/auth-dummy/user-meta.json");
      file.write(obj.toJSONString());
      file.flush();
      file.close();
    } catch (IOException ioE) {
      logger.error("createDirectoryJson ioE : " , ioE);
    }
  }

  @Override
  public void getUserInfo(GetUserInfoRequest request,
      StreamObserver<GetUserInfoResponse> responseObserver) {
    logger.info("===== Grpc Call :: AuthorizationImpl.getUserInfo");

    try {

      GetUserInfoResponse.Builder getUserInfoResponse = GetUserInfoResponse.newBuilder();

      if (TestFinalValue.FIX_ACCESS_TOKEN.equals(request.getAccessToken())) {
        User.Builder user = User.newBuilder();

//        String json = null;
//        json = JsonFormat.printer().includingDefaultValueFields().print(Empty.newBuilder());
        /*
        Struct.Builder userMeta;
        String json = JsonFormat.printer().includingDefaultValueFields()
            .print(userMeta.putFields(""));
        */

        String json = getUserMetaJson();
        logger.debug("#@ json [{}]", json);

        // #@  사용자의 meta를 설정합니다
        Struct.Builder struct = Struct.newBuilder();
        try {
          JsonFormat.parser().ignoringUnknownFields().merge(json, struct);
        } catch (Exception e) {
          logger.error("#@ ignoringUnknownFields().merge Exception [{}]", e);
          json = JsonFormat.printer().includingDefaultValueFields().print(Empty.newBuilder());
          JsonFormat.parser().ignoringUnknownFields().merge(json, struct);
        }

        user.setUserId(TestFinalValue.FIX_USER_KEY);
        user.setName("mindsLab");
        user.setAccessToken(TestFinalValue.FIX_ACCESS_TOKEN);
        user.setMeta(struct);

        getUserInfoResponse.setUser(user);

      } else {
        throw new Exception("Un Valid Access Token");
      }

      responseObserver.onNext(getUserInfoResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      responseObserver.onError(
          new StatusException(Status.INTERNAL.withDescription(e.getMessage()).withCause(e)));
    }
  }
}
