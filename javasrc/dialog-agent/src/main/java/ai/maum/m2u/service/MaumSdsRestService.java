package ai.maum.m2u.service;

import ai.maum.m2u.maumSds.rest.AnswerResponse;
import ai.maum.m2u.maumSds.rest.UtterRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MaumSdsRestService {

  @POST("collect/run/utter")
  Call<AnswerResponse> textTalk(@Body UtterRequest request);

  @POST("collect/run/intent")
  Call<AnswerResponse> intentTalk(@Body UtterRequest request);
}
