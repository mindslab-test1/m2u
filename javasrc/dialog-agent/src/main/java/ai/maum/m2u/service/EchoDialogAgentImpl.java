package ai.maum.m2u.service;

import ai.maum.m2u.utils.ServerMetaInterceptor;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.Empty;
import com.google.protobuf.ListValue;
import com.google.protobuf.NullValue;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.text.MessageFormat;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.CardOuterClass.Card;
import maum.m2u.common.CardOuterClass.ChartCard;
import maum.m2u.common.CardOuterClass.CustomCard;
import maum.m2u.common.CardOuterClass.CustomListCard;
import maum.m2u.common.CardOuterClass.LinkCard;
import maum.m2u.common.CardOuterClass.ListCard;
import maum.m2u.common.CardOuterClass.SelectCard;
import maum.m2u.common.CardOuterClass.SelectCard.Item;
import maum.m2u.common.Dialog.Session;
import maum.m2u.common.Dialog.Speech;
import maum.m2u.common.DirectiveOuterClass.AvatarSetExpressionPayload;
import maum.m2u.common.DirectiveOuterClass.DelegateDirective;
import maum.m2u.common.DirectiveOuterClass.DelegateDirective.DelegateDirectivePayload;
import maum.m2u.common.DirectiveOuterClass.Directive;
import maum.m2u.common.DirectiveOuterClass.Directive.DirectivePayload;
import maum.m2u.common.DirectiveOuterClass.LauncherFillSlotsPayload;
import maum.m2u.common.Evaluation.Score;
import maum.m2u.common.EventOuterClass.DialogAgentForwarderParam;
import maum.m2u.common.EventOuterClass.Event;
import maum.m2u.common.Job;
import maum.m2u.common.StatisticsOuterClass.Statistics;
import maum.m2u.common.Types.DataType;
import maum.m2u.da.Provider;
import maum.m2u.da.Provider.AgentKind;
import maum.m2u.da.Provider.DialogAgentProviderParam;
import maum.m2u.da.Provider.DialogAgentState;
import maum.m2u.da.Provider.DialogAgentStatus;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.da.v3.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import maum.m2u.da.v3.Talk;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import maum.m2u.da.v3.Talk.DialogDelegate;
import maum.m2u.da.v3.Talk.OpenSkillResponse;
import maum.m2u.da.v3.Talk.SkillTransition;
import maum.m2u.da.v3.Talk.SkillTransition.TransitionType;
import maum.m2u.da.v3.Talk.SpeechResponse;
import maum.m2u.da.v3.Talk.TalkResponse;
import maum.rpc.ErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// import maum.m2u.common.Userattr.DataType;


public class EchoDialogAgentImpl extends DialogAgentProviderImplBase {

  static final private Logger logger = LoggerFactory.getLogger(EchoDialogAgentImpl.class);

  private DialogAgentProviderParam.Builder provider = DialogAgentProviderParam.newBuilder();
  private Provider.InitParameter initParam;
  private DialogAgentState state;
  private int count = 0;


  public EchoDialogAgentImpl() {
    logger.debug("DA V3 Server Constructor");
    state = DialogAgentState.DIAG_STATE_IDLE;
    logger.debug("DialogAgentState: {}", state);
  }

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때, 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는
   * 함수 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장하는 과정 수행 Init() 함수에서
   * DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중) Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다.
   * (동작중)
   */
  @Override
  public void init(Provider.InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server Init");
    logger.trace("Init request: {}", request);

    try {
      state = DialogAgentState.DIAG_STATE_INITIALIZING; // DA instance state 초기화

      provider.setName("name") // provider 정보 setting
          .setDescription("control intention return DA")
          .setVersion("3.0")
          .setSingleTurn(true)
          .setAgentKind(AgentKind.AGENT_SDS)
          .setRequireUserPrivacy(true);

      initParam = request;
      state = DialogAgentState.DIAG_STATE_RUNNING;

      //throw new Exception("exception test");

      logger.trace("Init response {}", provider);
      responseObserver.onNext(provider.build()); // provider를 response로 return
      responseObserver.onCompleted();

    } catch (Exception e) { // 예외 처리
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10001, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("init")
            .setLine(108)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("init e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수 Dialog Agent Instance 동작에 따라 상태 변경 필요 생성자에서
   * DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   */
  @Override
  public void isReady(Empty request, StreamObserver<Provider.DialogAgentStatus> responseObserver) {
    logger.info("DA V3 Server IsReady");

    try {
      DialogAgentStatus.Builder response = DialogAgentStatus.newBuilder();

      response.setState(state); // DA instance 상태 저장

      logger.debug("IsReady response: {}", response);
      responseObserver.onNext(response.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10002, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("isReady")
            .setLine(148)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("isReady e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 동작중인 DA Instance을 종료시키는 함수 Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경해줘야 합니다.
   */
  @Override
  public void terminate(Empty request, StreamObserver<Empty> responseObserver) {
    logger.info("DA V3 Server Terminate");

    try {
      state = DialogAgentState.DIAG_STATE_TERMINATED; // DA instance state 변경
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10003, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("terminate")
            .setLine(183)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("terminate e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 실제로 DA Instnace가 돌아가기 위해 필요한 paramerters를 정의 db 정보 정의
   */
  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    logger.info("DA V3 Server GetRuntimeParameters");

    try {
      RuntimeParameterList.Builder result = RuntimeParameterList.newBuilder();

      Provider.RuntimeParameter.Builder db_host = Provider.RuntimeParameter.newBuilder(); // db ip
      db_host.setName("db_host")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Host")
          .setDefaultValue("171.64.122.134")
          .setRequired(true);
      result.addParams(db_host);

      Provider.RuntimeParameter.Builder db_port = Provider.RuntimeParameter.newBuilder(); // db port
      db_port.setName("db_port")
          .setType(DataType.DATA_TYPE_INT)
          .setDesc("Database Port")
          .setDefaultValue("7701")
          .setRequired(true);
      result.addParams(db_port);

      Provider.RuntimeParameter.Builder db_user = Provider.RuntimeParameter.newBuilder(); // db user
      db_user.setName("db_user")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database User")
          .setDefaultValue("minds")
          .setRequired(true);
      result.addParams(db_user);

      Provider.RuntimeParameter.Builder db_pwd = Provider.RuntimeParameter.newBuilder(); // db pwd
      db_pwd.setName("db_pwd")
          .setType(DataType.DATA_TYPE_AUTH)
          .setDesc("Database Password")
          .setDefaultValue("minds67~")
          .setRequired(true);
      result.addParams(db_pwd);

      Provider.RuntimeParameter.Builder db_database = Provider.RuntimeParameter
          .newBuilder(); // db name
      db_database.setName("db_database")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Database name")
          .setDefaultValue("ascar")
          .setRequired(true);
      result.addParams(db_database);

      logger.trace("GetRuntimeParameters response: {}", result);
      responseObserver.onNext(result.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10004, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getRuntimeParameter")
            .setLine(262)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getRuntimeParameters e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA가 동작하는 데 필요한 여러가지 옵션 값을 처리하는 DA Provider Parameter를 반환
   */
  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server GetProviderParameter");

    try {
      // init, isReady 등 여러 함수에서 정의된 provider 값 return
      logger.trace("GetProviderParameter response {}", provider);
      responseObserver.onNext(provider.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10005, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getProviderParameter")
            .setLine(299)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getProviderParameter e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다. 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
   */
  @Override
  public void openSession(Talk.OpenSessionRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {
    logger.info("DA V3 Server OpenSession");

    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSession request: {}", request);

    ai.maum.rpc.ResultStatus status;

    try {
      final long sessionId = request.getSession().getId();
      if (sessionId == 0 || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10101,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSession")
              .setLine(339)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSession e : ", e);
          responseObserver.onError(e);
        }
        return;
      }

      Talk.TalkResponse.Builder response = Talk.TalkResponse.newBuilder();
      response
          .setStatistics(Statistics.newBuilder()
              .setScore(Score.VERY_GOOD)
              .setCategory1("openSession")
              .setCategory2("openSession-detail")
              .setSentiment("happy")
              .setRemarks("session opened"))
          .setResponse(SpeechResponse.newBuilder()
              .setSpeech(Speech.newBuilder()
                  .setLang(request.getUtter().getLang())
                  .setUtter("hi!!")) // open message
              .setSessionUpdate(Session.newBuilder()
                  .setId(request.getSession().getId()))
              // meta 정보 저장
              .setMeta(Struct.newBuilder()
                  .putFields("context_test_1", // key
                      // value, setIntValue, setStringValue 등으로 type 설정
                      Value.newBuilder().setStringValue("ctx_1").build())))
          .setNextJob(Job.newBuilder()
              .setEvent(Event.newBuilder()
                  .setInterface("DialogAgent")
                  .setOperation("Event")
                  .setPlainPayload(Struct.newBuilder()
                      .putFields("action", Value.newBuilder()
                          .setStringValue("IceBreaking").build()))
                  .setParam(DialogAgentForwarderParam.newBuilder()
                      .setChatbot(request.getChatbot())
                      .setSkill(request.getSkill())
                      .setLang(request.getUtter().getLang())
                      .setSessionId(sessionId)))
              .setRaiseAfter(10000));
      logger.trace("OpenSession response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10006, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSession")
            .setLine(378)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSession e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 해당하는 스킬을 최초로 호출 할 때 호출된다. 싱글턴 일 경우에는 호출되지 않는다. 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
   */
  @Override
  public void openSkill(Talk.OpenSkillRequest request,
      StreamObserver<Talk.OpenSkillResponse> responseObserver) {
    logger.info("DA V3 Server OpenSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSkill request: {}", request);

    OpenSkillResponse.Builder response = OpenSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      final long sessionId = request.getSession().getId();
      if (sessionId == 0 || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10102,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSkill")
              .setLine(419)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSkill e : ", e);
          responseObserver.onError(e);
        }
        return;
      }
      response.setMeta( // meta 정보 저장
          Struct.newBuilder().putFields("meta_info",
              Value.newBuilder().setStringValue("This is openSkill")
                  .build()).build())
          .setSessionUpdate( // request의 session으로 업데이트
              Session.newBuilder().setId(
                  request.getSession().getId())
                  .build()).build();

      logger.trace("OpenSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10007, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSkill")
            .setLine(453)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSkill e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 대화를 주고 응답을 생성하도록 한다. <SpeechResponse> 일반 발화시 echo "chart" 발화시 echo + chartCart return "select"
   * 발화시 echo + selectCart return "link" 발화시 echo + linkCart return "custom" 발화시 echo + linkListCart
   * return "customlist" 발화시 echo + customListCart return <DialogDelegate> "delegate slotname" 발화시
   * LauncherFillSlotsPayload 타입의 dialogDelegate return
   **/
  @Override
  public void talk(Talk.TalkRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {
    logger.info("DA V3 Server Talk");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Talk request: {}", request);
    logger.trace("Talk Utter Meta info(Replacement) : {}", request.getUtter().getMeta());

    // 응답 메세지 분류를 위한 flag
    // 0이면 response, 1이면 transit, 2이면 delegate
    int flag = 0;

    ai.maum.rpc.ResultStatus status = null;

    try {
      final long sessionId = request.getSession().getId();
      if (sessionId == 0 || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10103,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("Talk")
              .setLine(500)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("talk e : ", e);
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("error")) {
        try {
          status = new ai.maum.rpc.ResultStatus(maum.rpc.Status.ExCode.LOGICAL_ERROR,
              10106,
              "Error for Result Status.");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile =
              ErrorDetails.DebugInfoFile.newBuilder()
                  .setFunction("Talk")
                  .setLine(523)
                  .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("talk e : ", e);
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("deadline")) {
        try {
          Thread.sleep(35000);
          responseObserver.onCompleted();
        } catch (Exception e) {
          logger.error("talk e : ", e);
          responseObserver.onError(e);
        }
        return;
      }

      Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

      Speech.Builder speech = Speech.newBuilder();
      Card.Builder card = Card.newBuilder();
      Directive.Builder directive = Directive.newBuilder();
      TransitionType transitionType = TransitionType.SKILL_TRANS_UNKNOWN;
      Struct.Builder context = Struct.newBuilder();

      String transit_skill = "";
      String transit_intent = "";
      String transit_chatbot = "";
      String replaced_utter = "";

      if (request.getUtter().getUtter().equals("chart")) {
        card.setChart(setChartCard()); // 함수 호출해서 chartcard 정보 받아오기
        flag = 0;
      } else if (request.getUtter().getUtter().equals("select")) {
        card.setSelect(setSelectCard()); // 함수 호출해서 selectcard 정보 받아오기
        flag = 0;
      } else if (request.getUtter().getUtter().equals("link")) {
        card.setLink(setLinkCard()); // 함수 호출해서 linkcard 정보 받아오기
        flag = 0;
      } else if (request.getUtter().getUtter().equals("list")) {
        card.setLinkList(setListCard()); // 함수 호출해서 linklistcard 정보 받아오기
        flag = 0;
      } else if (request.getUtter().getUtter().equals("custom")) {
        card.setCustom(setCustomCard()); // 함수 호출해서 customcard 정보 받아오기
        flag = 0;
      } else if (request.getUtter().getUtter().equals("customlist")) {
        card.setCustomList(setCustomListCard()); // 함수 호출해서 customlistcard 정보 받아오기
        flag = 0;
      } else if (request.getUtter().getUtter().equals("unknown")) {
        transitionType = TransitionType.SKILL_TRANS_UNKNOWN;
        flag = 1;
      } else if (request.getUtter().getUtter().equals("goto") // 다른 skill로 완전히 이동한다.
          || request.getUtter().getUtter().equals("#멀티 discard goto")) { // discard 후 추가 goto 전환
        transitionType = TransitionType.SKILL_TRANS_GOTO;
        transit_skill = "custom"; // 이동할 skill 정의
        transit_intent = "goto_intent"; // 이동할 intent 정의
        replaced_utter = request.getUtter().getUtter() + " <utter replaced>";
        flag = 1;
      } else if (request.getUtter().getUtter().equals("push")) {
        transitionType = TransitionType.SKILL_TRANS_PUSH; // 멀티턴인 다른 skill로 갔다 온다.
        transit_skill = "custom"; // 이동할 skill 정의
        replaced_utter = request.getUtter().getUtter() + " <utter replaced>"; // 이동할 intent 정의
        flag = 1;
      } else if (request.getUtter().getUtter().equals("goto once") // 다른 skill에 한번 갔다 온다.
          || request.getUtter().getUtter().equals("#멀티 discard error")) { // discard 후 비정상 전환
        transitionType = TransitionType.SKILL_TRANS_GOTO_ONCE;
        transit_skill = "goto_once_skill"; // 이동할 skill 정의
        transit_intent = "goto_once_intent"; // 이동할 intent 정의
        replaced_utter = request.getUtter().getUtter() + " <utter replaced>";
        flag = 1;
      } else if (request.getUtter().getUtter().equals("goto default")) { // default skill로 이동한다.
        transitionType = TransitionType.SKILL_TRANS_GOTO_DEFAULT;
        transit_skill = "goto_default_skill"; // 이동할 skill 정의
        transit_intent = "goto_default_intent"; // 이동할 intent 정의
        replaced_utter = request.getUtter().getUtter() + " <utter replaced>";
        flag = 1;
      } else if (request.getUtter().getUtter().equals("discard")) { // 현재 skill을 버려라
        transitionType = TransitionType.SKILL_TRANS_DISCARD;
        replaced_utter = "#hello DISCARD performed!";
        flag = 1;
      } else if (request.getUtter().getUtter().equals("discard goto")) {
        // 현재 skill을 버리고, 새로운 멀티 DA에서 다른 Skill로 goto하라.
        transitionType = TransitionType.SKILL_TRANS_DISCARD;
        replaced_utter = "#hello discard goto";
        flag = 1;
      } else if (request.getUtter().getUtter().equals("discard error")) {
        // 현재 skill을 버리고, 새로운 멀티 DA에서 다른 Skill로 goto하라.
        transitionType = TransitionType.SKILL_TRANS_DISCARD;
        replaced_utter = "#hello discard error";
        flag = 1;
      } else if (request.getUtter().getUtter().equals("cannot understand")) {
        // 어떤 skill인지 이해 못하겠다.
        transitionType = TransitionType.SKILL_TRANS_CANNOT_UNDERSTAND;
        flag = 1;
      } else if (request.getUtter().getUtter().equals("chatbot trans goto")) {
        // 다른 chatbot으로 완전히 이동한다.
        transitionType = TransitionType.CHATBOT_TRANS_GOTO;
        transit_chatbot = "goto_chatbot"; // 이동할 chatbot 정의
        flag = 1;
      } else if (request.getUtter().getUtter().equals("delegate slotname")) {
        flag = 2;
      }

      if (flag == 0) { // SpeechResponse
        // 정상적인 대화의 응답
        speech.setLang(Lang.ko_KR) // 발화문 echo 구조
            .setUtter(MessageFormat.format("[{0}] {1}",
                request.getSkill(), request.getUtter().getUtter()))
            .setSpeechUtter("음성 출력, " + request.getUtter().getUtter())
            .setReprompt(request.getUtter().getUtter().equals("expected response"));

        directive.setInterface("Avatar") // directive 저장
            .setOperation("SetExpression")
            .setParam(DialogAgentForwarderParam.newBuilder()
                .setChatbot("echoChatbot")
                .setSkill("echo")
                .setIntent("intent")
                .setSessionId(sessionId))
            .setPayload(DirectivePayload.newBuilder()
                .setAvatarSetExpression(AvatarSetExpressionPayload.newBuilder()
                    .setExpression("smile")
                    .setDesciption("avata is happy")
                    .build()).build()).build();

        context.putFields("context1", Value.newBuilder().setNumberValue(++count).build());
        context.putFields("chair", Value.newBuilder().setNullValue(NullValue.NULL_VALUE).build());
        context.putFields("desk", Value.newBuilder().build());

        response
            .setStatistics(Statistics.newBuilder()
                .setScore(Score.VERY_GOOD)
                .setCategory1("talk")
                .setCategory2("talk-detail")
                .setSentiment("good")
                .setRemarks("Talk talk"))
            .setResponse(SpeechResponse.newBuilder()
                .setSpeech(speech)
                .addCards(card)
                .addDirectives(directive)
                .setCloseSkill(false)
                .setSessionUpdate(Session.newBuilder().setContext(context)))
            .setNextJob(Job.newBuilder()
                .setEvent(Event.newBuilder()
                    .setInterface("DialogAgent")
                    .setOperation("Event")
                    .setPlainPayload(Struct.newBuilder()
                        .putFields("action", Value.newBuilder()
                            .setStringValue("IceBreaking").build()))
                    .setParam(DialogAgentForwarderParam.newBuilder()
                        .setChatbot(request.getChatbot())
                        .setSkill(request.getSkill())
                        .setIntent(request.getIntent())
                        .setLang(request.getUtter().getLang())
                        .setSessionId(sessionId)))
                .setRaiseAfter(300000));
      } else if (flag == 1) { // SkillTransition
        // SKILL에 대한 요청을 처리할 수 없어서 다른 SKILL 또는 CHATBOT으로 전이하는 경우
        response.setTransit(SkillTransition.newBuilder()
            .setTransitionType(transitionType)
            .setSkill(transit_skill)
            .setIntent(transit_intent)
            .setChatbot(transit_chatbot)
            .setReplacedUtter(replaced_utter)
            .setMeta(Struct.newBuilder().putFields("meta1",
                Value.newBuilder().setStringValue("meta value 1").build())
                .putFields("meta2", Value.newBuilder().setBoolValue(false)
                    .build()).build())
            .setSessionUpdate(Session.newBuilder()
                .setId(request.getSession().getId())));
      } else if (flag == 2) { // DialogDelegate
        // DEVICE의 추가적인 응답을 요청하고 이를 재처리하는 경우
        response
            .setStatistics(Statistics.newBuilder()
                .setScore(Score.VERY_GOOD)
                .setCategory1("talk")
                .setCategory2("talk-detail")
                .setSentiment("negative")
                .setRemarks("Talk Delegate"))
            .setDelegate(DialogDelegate.newBuilder()
                .setDirective(DelegateDirective.newBuilder()
                    .setInterface("Launcher")
                    .setOperation("FillSlots")
                    .setParam(DialogAgentForwarderParam.newBuilder()
                        .setChatbot("echoChatbot")
                        .setSkill("echo")
                        .setIntent("intent")
                        .setSessionId(sessionId))
                    .setPayload(DelegateDirectivePayload.newBuilder()
                        .setLauncherFillSlots(LauncherFillSlotsPayload.newBuilder()
                            .addRequestingSlots("requesting_slots")))));
      }

      logger.trace("Talk response: {}", response);
      // 계속해서 정보를 전달할 때 on next를 순차적으로 추가해준다. 처음 보내는 정보는 echo
      responseObserver.onNext(response.build());
      //responseObserver.onNext(param.build()); // 카드의 종류에 따라 순차적으로 보낸다.
      responseObserver.onCompleted(); // 보낼 정보들이 끝날 때 한 번만 해주면 된다.
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10008, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("talk")
            .setLine(714)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("talk e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다. 다음과 같은 동작이 예가 될 수 있다. 로봇이 특정한 위치로 이동하라고 명명하고 수행이
   * 완료되었을 경우에 내보낸다.
   **/
  @Override
  public void event(Talk.EventRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {

    logger.info("DA V3 Server Event");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Event request: {}", request);
    logger.trace("Event Meta info : {}", request.getEvent().getMeta());

    Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      final DialogAgentForwarderParam param = request.getEvent().getParam();
      final long sessionId = request.getSession().getId();
      if (sessionId == 0 || param.getSkill() == null || param.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10104,
              "session.id or chatbot or skill is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(756)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("event e : ", e);
          responseObserver.onError(e);
        }
        return;
      }

      String action;
      Event event = request.getEvent();
      if (event.getInterface().equals("DialogAgent") && event.getOperation().equals("Event")) {
        action = event.getPlainPayload().getFieldsOrDefault("action", Value.newBuilder()
            .setStringValue("").build())
            .getStringValue();
      } else {
        action = "$$unhandled_event";
      }

      String utter;
      Lang lang;
      Job.Builder nextJob;
      switch (action) {
        case "IceBreaking":
          utter = "왜 이렇게 말이 없으세요.";
          lang = Lang.ko_KR;
          nextJob = Job.newBuilder()
              .setEvent(Event.newBuilder()
                  .setInterface("DialogAgent")
                  .setOperation("Event")
                  .setPlainPayload(Struct.newBuilder()
                      .putFields("action", Value.newBuilder()
                          .setStringValue("MoreTalk").build()))
                  .setParam(DialogAgentForwarderParam.newBuilder()
                      .setChatbot(param.getChatbot())
                      .setSkill(param.getSkill())
                      .setIntent(param.getIntent())
                      .setLang(param.getLang())
                      .setSessionId(sessionId)))
              .setRaiseAfter(0);
          break;
        case "MoreTalk":
          utter = "뭐라도 얘기 좀 해 보세요.";
          lang = Lang.ko_KR;
          nextJob = Job.newBuilder()
              .setEvent(Event.newBuilder()
                  .setInterface("DialogAgent")
                  .setOperation("Event")
                  .setPlainPayload(Struct.newBuilder()
                      .putFields("action", Value.newBuilder()
                          .setStringValue("IceBreaking").build()))
                  .setParam(DialogAgentForwarderParam.newBuilder()
                      .setChatbot(param.getChatbot())
                      .setSkill(param.getSkill())
                      .setIntent(param.getIntent())
                      .setLang(param.getLang())
                      .setSessionId(sessionId)))
              .setRaiseAfter(300000);
          break;
        case "$$unhandled_event":
          utter = "Unhandled Event: " + event.getInterface() + "/" + event.getOperation();
          lang = Lang.en_US;
          nextJob = null;
          break;
        default:
          utter = action.isEmpty() ? "Action is empty." : "Unhandled Action: " + action;
          lang = Lang.en_US;
          nextJob = null;
          break;
      }

      response
          .setStatistics(Statistics.newBuilder()
              .setScore(Score.VERY_GOOD)
              .setCategory1("event")
              .setCategory2("event-detail")
              .setSentiment("positive")
              .setRemarks("Event"))
          .setResponse(
              SpeechResponse.newBuilder()
                  // event에 대한 응답 메세지
                  .setSpeech(Speech.newBuilder()
                      .setUtter(MessageFormat.format("[{0}] {1}", param.getSkill(), utter))
                      .setLang(lang))
                  .setMeta(Struct.newBuilder().putFields("token", // 추가적인 정보 전달
                      Value.newBuilder().setStringValue(request.getEvent().getPayload()
                          .getLauncherAuthorized().getAccessToken()).build()))
                  .setSessionUpdate( // session 업데이트
                      Session.newBuilder().setId(
                          request.getSession().getId())));
      if (nextJob != null) {
        response.setNextJob(nextJob);
      }

      logger.trace("Event response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("event")
            .setLine(794)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("event e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 SKILL이 종료되었음을 알려준다. SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로 매우 주의해야 한다.
   **/
  @Override
  public void closeSkill(Talk.CloseSkillRequest request,
      StreamObserver<Talk.CloseSkillResponse> responseObserver) {
    logger.info("DA V3 Server CloseSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("CloseSkill request: {}", request);

    CloseSkillResponse.Builder response = CloseSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      final long sessionId = request.getSession().getId();
      if (sessionId == 0 || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10105,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(835)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("closeSkill e : ", e);
          responseObserver.onError(e);
        }
        return;
      }

      response.setSessionUpdate(
          Session.newBuilder()
              .setId(request.getSession().getId()) // session update
              .setContext(request.getSession().getContext()) // session context update
              .build()).build();

      logger.trace("CloseSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("closeSkill")
            .setLine(866)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("closeSkill e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * ChartCard 정의
   **/
  private ChartCard.Builder setChartCard() {
    logger.info("DA V3 Function SetChartCard");

    ChartCard.Builder chartCard = ChartCard.newBuilder();

    try {
      chartCard.setType("PieChart")
          .addData(ListValue.newBuilder()
              .addValues(Value.newBuilder()
                  .setStringValue("PieChart"))
              .build()).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.debug("setChartCard: {}", chartCard);
    return chartCard;
  }

  /**
   * SelectCard 정의
   **/
  private SelectCard.Builder setSelectCard() {
    logger.info("DA V3 Function setSelectCard");

    SelectCard.Builder selectCard = SelectCard.newBuilder();

    try {
      selectCard.setTitle("Transportation")
          .setHeader("messageId: 1234")
          .addItems(Item.newBuilder()
              .setTitle("bus")
              .setSummary("This is bus image card")
              .setImageUrl("data:image/png;base64,dhtdgedfswhftd")
              .setSelectedUtter("버스 선택할게요")
              .setExtra(Struct.newBuilder()
                  .putFields("color", Value.newBuilder().setStringValue("aquamarine").build())
                  .putFields("size", Value.newBuilder().setNumberValue(120).build())
                  .putFields("datas", Value.newBuilder().setListValue(
                      ListValue.newBuilder()
                          .addValues(Value.newBuilder().setStringValue("foo"))
                          .addValues(Value.newBuilder().setNumberValue(94))
                          .addValues(Value.newBuilder().setStringValue("bar"))
                          .addValues(Value.newBuilder().setNumberValue(23))).build())))
          .addItems(Item.newBuilder()
              .setTitle("subway")
              .setSummary("This is subway image card")
              .setImageUrl("data:image/png;base64,sgfdjhgfhkh")
              .setSelectedUtter("지하철 선택할래요").build())
          .addItems(Item.newBuilder()
              .setTitle("taxi")
              .setSummary("This is taxi image card")
              .setImageUrl("data:image/png;base64,sghdhfjgedaw")
              .setSelectedUtter("택시 탈게요").build())
          .addItems(Item.newBuilder()
              .setTitle("airplane")
              .setSummary("This is airplane image card")
              .setImageUrl("data:image/png;base64,hgdfawsad")
              .setSelectedUtter("비행기 선택할게요").build())
          .addItems(Item.newBuilder()
              .setTitle("ship")
              .setSummary("This is ship image card")
              .setImageUrl("data:image/png;base64,etdfaegdfss")
              .setSelectedUtter("배 선택할게요").build())
          .setHorizontal(true).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    logger.debug("setSelectCard: {}", selectCard);
    return selectCard;
  }

  /**
   * LinkCard 정의
   **/
  private LinkCard.Builder setLinkCard() {
    logger.info("DA V3 Function SetLinkCard");

    LinkCard.Builder linkCard = LinkCard.newBuilder();

    try {
      linkCard.setTitle("ticket_reservation")
          .setSummary("선택하면 표를 예매할 수 있는 url로 연결되는 카드")
          .setImageUrl("data:image/png;base64,diusohdkjlflaskhf")
          .setImageHref("http://ticket_reservation.com").build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.trace("setLinkCard: {}", linkCard);
    return linkCard;
  }

  /**
   * CustomCard 정의
   **/
  private CustomCard.Builder setCustomCard() {
    logger.info("DA V3 Function setCustomCard");

    CustomCard.Builder customCard = CustomCard.newBuilder();

    try {
      customCard.setType("type")
          .setCardData(Struct.newBuilder()
              .putFields("card_data", Value.newBuilder()
                  .setStringValue("This is custom card data")
                  .build()).build()).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.trace("setCustomCard: {}", customCard);
    return customCard;
  }

  /**
   * ListCard 정의
   **/
  private ListCard.Builder setListCard() {
    logger.info("DA V3 Function SetListCard");

    ListCard.Builder listCard = ListCard.newBuilder();

    try {
      listCard
          .addCards(LinkCard.newBuilder()
              .setTitle("ticket_reservation")
              .setSummary("선택하면 표를 예매할 수 있는 url로 연결되는 카드")
              .setImageUrl("data:image/png;base64,diusohdkjlflaskhf")
              .setImageHref("http://ticket_reservation.com").build())
          .addCards(LinkCard.newBuilder()
              .setTitle("ticket_reservation_cancel")
              .setSummary("예매표를 취소할 수 있는 url로 연결되는 카드")
              .setImageUrl("data:image/png;base64,fuweohoolkdqaejhsudi")
              .setImageHref("http://ticket_reservation_cancel.com").build())
          .addCards(LinkCard.newBuilder()
              .setTitle("ticket_reservation_confirm")
              .setSummary("예매한 표를 확인할 수 있는 url로 연결되는 카드")
              .setImageUrl("data:image/png;base64,kjdfnemnkas21cdwsa435")
              .setImageHref("http://ticket_reservation_confirm.com").build())
          .setHorizontal(true).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.trace("setListCard: {}", listCard);
    return listCard;
  }

  /**
   * CustomListCard 정의
   **/
  private CustomListCard.Builder setCustomListCard() {
    logger.info("DA V3 Function CustomListCard");

    CustomListCard.Builder customListCard = CustomListCard.newBuilder();

    try {
      customListCard.addCards(CustomCard.newBuilder()
          .setType("string").setCardData(Struct.newBuilder()
              .putFields("card_data", Value.newBuilder()
                  .setStringValue("This is custom card data")
                  .build()).build()).build())
          .setHorizontal(true).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.trace("customListCard: {}", customListCard);
    return customListCard;
  }
}
