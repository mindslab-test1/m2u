package ai.maum.m2u.service;

import ai.maum.m2u.maumSds.rest.AnswerResponse;
import ai.maum.m2u.maumSds.rest.AnswerResponse.ExpectedIntent;
import ai.maum.m2u.maumSds.rest.AnswerResponse.SetMemory;
import ai.maum.m2u.maumSds.rest.Utter;
import ai.maum.m2u.maumSds.rest.UtterRequest;
import ai.maum.m2u.utils.ServerMetaInterceptor;
import ai.maum.m2u.utils.converter.ProtoJsonConverterFactory;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.Empty;
import com.google.protobuf.Struct;
import com.google.protobuf.TextFormat;
import com.google.protobuf.Value;
import com.google.protobuf.util.JsonFormat;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.net.SocketTimeoutException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.CardOuterClass.Card;
import maum.m2u.common.CardOuterClass.SelectCard;
import maum.m2u.common.CardOuterClass.SelectCard.Item;
import maum.m2u.common.Dialog.Session;
import maum.m2u.common.Dialog.Speech;
import maum.m2u.common.StatisticsOuterClass.Statistics;
import maum.m2u.common.Types.DataType;
import maum.m2u.da.Provider;
import maum.m2u.da.Provider.AgentKind;
import maum.m2u.da.Provider.DialogAgentProviderParam;
import maum.m2u.da.Provider.DialogAgentState;
import maum.m2u.da.Provider.DialogAgentStatus;
import maum.m2u.da.Provider.RuntimeParameter;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.da.v3.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import maum.m2u.da.v3.Talk;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import maum.m2u.da.v3.Talk.OpenSkillResponse;
import maum.m2u.da.v3.Talk.SpeechResponse;
import maum.m2u.da.v3.Talk.TalkResponse;
import maum.rpc.ErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;


public class MaumSdsDialogAgentImpl extends DialogAgentProviderImplBase {

  private static final Logger logger = LoggerFactory.getLogger(MaumSdsDialogAgentImpl.class);
  private static final String TARGET_URL = "Target URL";
  private static final String HOST = "Host";
  private static final String URL_REGEX = "^(http|https)://.*";

  private DialogAgentProviderParam.Builder provider = DialogAgentProviderParam.newBuilder();
  private DialogAgentState state;
  // Maum SDS 서버 url
  private String maumSdsUrl = "";
  // Maum SDS host (모델 구분 값)
  private String maumSdsHost = "";

  public MaumSdsDialogAgentImpl() {
    logger.debug("DA V3 Server Constructor");
    state = DialogAgentState.DIAG_STATE_IDLE;
    logger.debug("DialogAgentState: {}", state);
  }

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때, 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는
   * 함수 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장하는 과정 수행 Init() 함수에서
   * DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중) Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다.
   * (동작중)
   */
  @Override
  public void init(Provider.InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server Init");
    logger.trace("Init request: {}", request);

    try {
      state = DialogAgentState.DIAG_STATE_INITIALIZING; // DA instance state 초기화

      provider.setName("name") // provider 정보 setting
          .setDescription("control intention return DA")
          .setVersion("3.0")
          .setSingleTurn(true)
          .setAgentKind(AgentKind.AGENT_SDS)
          .setRequireUserPrivacy(true);

      maumSdsUrl = request.getParamsMap().getOrDefault(TARGET_URL, "");
      maumSdsHost = request.getParamsMap().getOrDefault(HOST, "");

      logger.info("URL = {} HOST = {}", maumSdsUrl, maumSdsHost);
      if (maumSdsUrl.isEmpty() || !Pattern.matches(URL_REGEX, maumSdsUrl)) {
        throw new Exception(MessageFormat.format("Malformed target URL : {1}", maumSdsUrl));
      }
      if (maumSdsHost.isEmpty()) {
        throw new Exception("Host is Empty");
      }

      state = DialogAgentState.DIAG_STATE_RUNNING;

      logger.trace("Init response {}", provider);
      responseObserver.onNext(provider.build()); // provider를 response로 return
      responseObserver.onCompleted();

    } catch (Exception e) { // 예외 처리
      state = DialogAgentState.DIAG_STATE_IDLE;

      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10001, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("init")
            .setLine(108)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("init e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수 Dialog Agent Instance 동작에 따라 상태 변경 필요 생성자에서
   * DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   */
  @Override
  public void isReady(Empty request, StreamObserver<DialogAgentStatus> responseObserver) {
    logger.info("DA V3 Server IsReady");

    try {
      DialogAgentStatus.Builder response = DialogAgentStatus.newBuilder();

      response.setState(state); // DA instance 상태 저장

      logger.debug("IsReady response: {}", response);
      responseObserver.onNext(response.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10002, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("isReady")
            .setLine(148)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("isReady e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  @Override
  public void terminate(Empty request, StreamObserver<Empty> responseObserver) {
    logger.info("DA V3 Server Terminate");

    try {
      state = DialogAgentState.DIAG_STATE_TERMINATED; // DA instance state 변경
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10003, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("terminate")
            .setLine(183)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("terminate e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    logger.info("DA V3 Server GetRuntimeParameters");

    try {
      RuntimeParameterList.Builder parameters = RuntimeParameterList.newBuilder()
          .addParams(RuntimeParameter.newBuilder()
              .setName(TARGET_URL)
              .setType(DataType.DATA_TYPE_STRING)
              .setDefaultValue("http://")
              .setDesc("Maum SDS REST API url")
              .setRequired(true))
          .addParams(RuntimeParameter.newBuilder()
              .setName(HOST)
              .setType(DataType.DATA_TYPE_STRING)
              .setDefaultValue("0")
              .setDesc("Maum SDS Host")
              .setRequired(true));

      logger.trace("GetRuntimeParameters response: {}", parameters);

      responseObserver.onNext(parameters.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10004, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getRuntimeParameter")
            .setLine(262)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getRuntimeParameters e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server GetProviderParameter");

    try {
      // init, isReady 등 여러 함수에서 정의된 provider 값 return
      logger.trace("GetProviderParameter response {}", provider);
      responseObserver.onNext(provider.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10005, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getProviderParameter")
            .setLine(299)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getProviderParameter e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  @Override
  public void event(Talk.EventRequest request, StreamObserver<TalkResponse> responseObserver) {
    logger.info("DA V3 Server Event");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Event request: {}", request);

    Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10104, "session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(756)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("event e : ", e);
          responseObserver.onError(e);
        }
      }

      logger.trace("Event response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("event")
            .setLine(794)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("event e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  @Override
  public void closeSkill(Talk.CloseSkillRequest request,
      StreamObserver<CloseSkillResponse> responseObserver) {
    logger.info("DA V3 Server CloseSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("CloseSkill request: {}", request);

    CloseSkillResponse.Builder response = CloseSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10105,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(835)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("closeSkill e : ", e);
          responseObserver.onError(e);
        }
      }

      response.setSessionUpdate(
          Session.newBuilder()
              .setId(request.getSession().getId()) // session update
              .setContext(request.getSession().getContext()) // session context update
              .build()).build();

      logger.trace("CloseSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("closeSkill")
            .setLine(866)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("closeSkill e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  @Override
  public void openSession(Talk.OpenSessionRequest request,
      StreamObserver<TalkResponse> responseObserver) {

    ai.maum.rpc.ResultStatus status;
    Response<AnswerResponse> answerResponse = null;

    logger.info("DA V3 Server OpenSession");

    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSession request: {}", request);

    try {
      long sessionId = request.getSession().getId();
      if (sessionId == 0 || request.getSkill() == null || request.getChatbot() == null) {
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10101,
              "chatbot or skill or session.id is null");

          ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSession")
              .setLine(339)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSession e : {}", e.getMessage(), e);
          responseObserver.onError(e);
        }
      }

      try {
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(maumSdsUrl)
            .addConverterFactory(ProtoJsonConverterFactory.create())
            .build();
        MaumSdsRestService service = retrofit.create(MaumSdsRestService.class);

        UtterRequest utterRequest = UtterRequest.newBuilder()
            .setHost(this.maumSdsHost)
            .setSession(request.getSession().getId() + "")
            .setData(Utter.newBuilder().setUtter("처음으로"))
            .setLang("1")
            .build();

//        Call<AnswerResponse> call = service.intentTalk(utterRequest);
        Call<AnswerResponse> call = service.textTalk(utterRequest);
        answerResponse = call.execute();

      } catch (SocketTimeoutException e) {
        logger.error("OpenSession e : {}", e.getMessage(), e);
        responseObserver.onError(e);
      }

      AnswerResponse answer = answerResponse.body();
      logger.trace("Maum SDS response: {}",
          JsonFormat.printer().includingDefaultValueFields().print(answer));

      TalkResponse.Builder response = getResponseFromMSAnswer(answer);
      logger.trace("OpenSession response: {}", TextFormat.printToUnicodeString(response));

      responseObserver.onNext(response.build());
      responseObserver.onCompleted();

    } catch (Exception e) {
      logger.error("OpenSession {} ", e.getMessage(), e);

      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10006, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSession")
            .setLine(378)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);

      } catch (Exception e1) {
        logger.error("OpenSession e : {}", e1.getMessage(), e1);
        responseObserver.onError(e1);
      }
    }
  }

  /**
   * 해당하는 스킬을 최초로 호출 할 때 호출된다. 싱글턴 일 경우에는 호출되지 않는다. 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
   */
  @Override
  public void openSkill(Talk.OpenSkillRequest request,
      StreamObserver<OpenSkillResponse> responseObserver) {
    logger.info("DA V3 Server OpenSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSkill request: {}", request);

    OpenSkillResponse.Builder response = OpenSkillResponse.newBuilder();
    ai.maum.rpc.ResultStatus status;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {

        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10102,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("OpenSkill")
              .setLine(419)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("OpenSkill e : ", e);
          responseObserver.onError(e);
        }
      }

      response.setMeta( // meta 정보 저장
          Struct.newBuilder().putFields("meta_info",
              Value.newBuilder().setStringValue("This is openSkill")
                  .build()).build())
          .setSessionUpdate( // request의 session으로 업데이트
              Session.newBuilder().setId(
                  request.getSession().getId())
                  .build()).build();

      logger.trace("OpenSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);

      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10007, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("OpenSkill")
            .setLine(453)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("OpenSkill e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  @Override
  public void talk(Talk.TalkRequest request,
      StreamObserver<TalkResponse> responseObserver) {

    ai.maum.rpc.ResultStatus status;
    Response<AnswerResponse> answerResponse = null;

    logger.info("DA V3 Server Talk");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Talk request: {}", request);
    logger.trace("Talk Utter Meta info(Replacement) : {}", request.getUtter().getMeta());

    try {
      long sessionId = request.getSession().getId();
      if (sessionId == 0 || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10103,
            "chatbot or skill or session.id is null");

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("Talk")
            .setLine(500)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
        return;
      }

      Retrofit retrofit = new Retrofit.Builder()
          .baseUrl(maumSdsUrl)
          .addConverterFactory(ProtoJsonConverterFactory.create())
          .build();
      MaumSdsRestService service = retrofit.create(MaumSdsRestService.class);

      UtterRequest utterRequest = UtterRequest.newBuilder()
          .setHost(this.maumSdsHost)
          .setSession(request.getSession().getId() + "")
          .setData(Utter.newBuilder().setUtter(request.getUtter().getUtter()))
          .setLang("1")
          .build();

      boolean isIntent = request.getUtter().getMeta()
          .getFieldsOrDefault("fromSelect", Value.newBuilder().setBoolValue(false).build())
          .getBoolValue();

//      Call<AnswerResponse> call = isIntent ?
//          service.intentTalk(utterRequest) :
//          service.textTalk(utterRequest);
      Call<AnswerResponse> call = service.textTalk(utterRequest);
      answerResponse = call.execute();

      AnswerResponse answer = answerResponse.body();
      logger.trace("Maum SDS response: {}", TextFormat.printToUnicodeString(answer));

      TalkResponse.Builder response = getResponseFromMSAnswer(answer);
      logger.trace("Talk response: {}", TextFormat.printToUnicodeString(response));

      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10008, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("Talk")
            .setLine(714)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("Talk e1 : ", e1);
        responseObserver.onError(e);
      }
    }
  }

  private List<Card> getSelCardList(List<ExpectedIntent> intentList) {
    List<Card> selCardList = new ArrayList<>();
    SelectCard.Builder buttonCard = SelectCard.newBuilder();
    SelectCard.Builder carouselCard = SelectCard.newBuilder();
    SelectCard.Builder listCard = SelectCard.newBuilder();

    for (ExpectedIntent intent : intentList) {
      Item.Builder item = Item.newBuilder()
          .setTitle(intent.getIntent())
          .setSummary(intent.getDisplayText())
          .setImageUrl(intent.getDisplayUrl())
          .setSelectedUtter(intent.getIntent());

      if (("B").equals(intent.getDisplayType())) {
        buttonCard.addItems(item);
      } else if (("I").equals(intent.getDisplayType())) {
        carouselCard.addItems(item);
      } else if (("L").equals(intent.getDisplayType())) {
        listCard.addItems(item);
      }
    }

    buttonCard.addItems(Item.newBuilder()
        .setTitle("처음으로")
        .setSelectedUtter("처음으로")
        .setStyle("#0d8bff"))
        .setHorizontal(true);

    if (buttonCard.getItemsCount() != 0) {
      selCardList.add(Card.newBuilder()
          .setSelect(buttonCard.setType("Button").setHorizontal(true)).build());
    }
    if (carouselCard.getItemsCount() != 0) {
      selCardList.add(Card.newBuilder()
          .setSelect(carouselCard.setType("Carousel").setHorizontal(true)).build());
    }
    if (listCard.getItemsCount() != 0) {
      selCardList.add(Card.newBuilder()
          .setSelect(listCard.setType("List").setHorizontal(false)).build());
    }

    logger.trace("getSelCardList: {}", selCardList);

    return selCardList;
  }

  private Struct.Builder setContext(AnswerResponse answer) {

    Struct.Builder context = Struct.newBuilder();
    SetMemory setMemory = answer.getSetMemory();

    context.putFields("host", Value.newBuilder().setStringValue(setMemory.getHost()).build());
    context.putFields("session", Value.newBuilder().setStringValue(setMemory.getSession()).build());
    context.putFields("lifespan",
        Value.newBuilder().setNumberValue(setMemory.getLifespan()).build());
    context.putFields("intent",
        Value.newBuilder().setStringValue(setMemory.getIntent().getIntent()).build());
    context.putFields("entityName",
        Value.newBuilder().setStringValue(setMemory.getEntities().getEntityName()).build());
    context.putFields("entityValue",
        Value.newBuilder().setStringValue(setMemory.getEntities().getEntityValue()).build());

    logger.trace("setContext: {}", context);

    return context;
  }

  private TalkResponse.Builder getResponseFromMSAnswer(AnswerResponse answer) {
    TalkResponse.Builder response = TalkResponse.newBuilder();

    SetMemory setMemory = answer.getSetMemory();

    Statistics.Builder statistics = Statistics.newBuilder()
        .setCategory1(setMemory.getHost())
        .setCategory2(setMemory.getSession())
        .setCategory3(setMemory.getLifespan() + "")
        .setCategory4(setMemory.getIntent().getIntent())
        .setCategory5(setMemory.getEntities().getEntityName() + ":" + setMemory
            .getEntities().getEntityValue());

    String answerUtter = answer.getAnswer().getAnswer();
    answerUtter = answerUtter.replace("<a href=\"", "<br><a target='_blank' href=\"");

    Speech.Builder speech = Speech.newBuilder();
    speech.setLang(Lang.ko_KR)
        .setUtter(answerUtter)
        .setSpeechUtter(answerUtter);

    SpeechResponse.Builder speechResponse = SpeechResponse.newBuilder()
        .setSpeech(speech)
        .addAllCards(getSelCardList(answer.getExpectedIntentsList()))
        .setSessionUpdate(Session.newBuilder().setContext(setContext(answer)));

    response
        .setStatistics(statistics)
        .setResponse(speechResponse);

    return response;
  }

}
