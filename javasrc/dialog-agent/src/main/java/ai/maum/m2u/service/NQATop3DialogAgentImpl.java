package ai.maum.m2u.service;

import ai.maum.m2u.config.PropertyManager;
import ai.maum.m2u.utils.NqaOptionDocument;
import ai.maum.m2u.utils.ServerMetaInterceptor;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.Empty;
import com.google.protobuf.ListValue;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import maum.brain.qa.nqa.NQaServiceGrpc;
import maum.brain.qa.nqa.NQaServiceGrpc.NQaServiceBlockingStub;
import maum.brain.qa.nqa.Nqa.Answer;
import maum.brain.qa.nqa.Nqa.QueryTarget;
import maum.brain.qa.nqa.Nqa.QueryType;
import maum.brain.qa.nqa.Nqa.QueryUnit;
import maum.brain.qa.nqa.Nqa.Question;
import maum.brain.qa.nqa.Nqa.SearchAnswerRequest;
import maum.brain.qa.nqa.Nqa.SearchAnswerResponse;
import maum.brain.qa.nqa.Nqa.SearchQuestionRequest;
import maum.brain.qa.nqa.Nqa.SearchQuestionResponse;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.CardOuterClass.Card;
import maum.m2u.common.Dialog.Session;
import maum.m2u.common.Dialog.Speech;
import maum.m2u.common.DirectiveOuterClass.AvatarSetExpressionPayload;
import maum.m2u.common.DirectiveOuterClass.Directive;
import maum.m2u.common.DirectiveOuterClass.Directive.DirectivePayload;
import maum.m2u.common.EventOuterClass.DialogAgentForwarderParam;
import maum.m2u.common.Types.DataType;
import maum.m2u.da.Provider;
import maum.m2u.da.Provider.AgentKind;
import maum.m2u.da.Provider.DialogAgentProviderParam;
import maum.m2u.da.Provider.DialogAgentState;
import maum.m2u.da.Provider.DialogAgentStatus;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.da.v3.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import maum.m2u.da.v3.Talk;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import maum.m2u.da.v3.Talk.OpenSkillResponse;
import maum.m2u.da.v3.Talk.SpeechResponse;
import maum.m2u.da.v3.Talk.TalkResponse;
import maum.rpc.ErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NQATop3DialogAgentImpl extends DialogAgentProviderImplBase {

  static final private Logger logger = LoggerFactory.getLogger(NQATop3DialogAgentImpl.class);
  private static final String PARAMETER_CHANNEL = "channel";
  private static final String PARAMETER_DEFAULT_CHANNEL = "thezone";
  private static final String PARAMETER_CONFIG = "config";
  private static final String PARAMETER_DEFAULT_CONFIG = "m2u.conf";

  private final DialogAgentProviderParam.Builder provider = DialogAgentProviderParam.newBuilder();
  private DialogAgentState state;

  public NQATop3DialogAgentImpl() {
    logger.debug("DA V3 Server Constructor");
    state = DialogAgentState.DIAG_STATE_IDLE;
    logger.debug("DialogAgentState: {}", state);
    // Initialize Static variable
    ai.maum.rpc.ResultStatus.setModule(maum.rpc.Status.Module.M2U);
    ai.maum.rpc.ResultStatus.setProcess(6);
  }

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때, 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는
   * 함수 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장하는 과정 수행 Init() 함수에서
   * DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중) Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다.
   * (동작중)
   */
  @Override
  public void init(Provider.InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server Init");
    logger.trace("Init request: {}", request);

    try {
      state = DialogAgentState.DIAG_STATE_INITIALIZING; // DA instance state 초기화

      provider.setName("name") // provider 정보 setting
          .setDescription("control intention return DA")
          .setVersion("3.0")
          .setSingleTurn(true)
          .setAgentKind(AgentKind.AGENT_CHATBOT)
          .setRequireUserPrivacy(true);

      final String channel = request.getParamsMap().getOrDefault(PARAMETER_CHANNEL, "");
      final String config = request.getParamsMap().getOrDefault(PARAMETER_CONFIG, "");

      NqaOptionDocument.setTargetChannel(channel);
      if (!config.toLowerCase().equals(PARAMETER_DEFAULT_CONFIG)) {
        PropertyManager.setConfigFile(config);
      }

      logger.trace("Init response {}", provider);
      responseObserver.onNext(provider.build()); // provider를 response로 return
      responseObserver.onCompleted();

      state = DialogAgentState.DIAG_STATE_RUNNING;
    } catch (Exception e) { // 예외 처리
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10001, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("init")
            .setLine(108)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수 Dialog Agent Instance 동작에 따라 상태 변경 필요 생성자에서
   * DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   */
  @Override
  public void isReady(Empty request, StreamObserver<DialogAgentStatus> responseObserver) {
    logger.info("DA V3 Server IsReady");

    try {
      DialogAgentStatus.Builder response = DialogAgentStatus.newBuilder();

      response.setState(state); // DA instance 상태 저장

      logger.debug("IsReady response: {}", response);
      responseObserver.onNext(response.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10002, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("isReady")
            .setLine(148)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 동작중인 DA Instance을 종료시키는 함수 Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경해줘야 합니다.
   */
  @Override
  public void terminate(Empty request, StreamObserver<Empty> responseObserver) {
    logger.info("DA V3 Server Terminate");

    try {
      state = DialogAgentState.DIAG_STATE_TERMINATED; // DA instance state 변경
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10003, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("terminate")
            .setLine(183)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 실제로 DA Instnace가 돌아가기 위해 필요한 paramerters를 정의 db 정보 정의
   */
  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    logger.info("DA V3 Server GetRuntimeParameters");

    try {
      RuntimeParameterList.Builder result = RuntimeParameterList.newBuilder();

      Provider.RuntimeParameter.Builder channel = Provider.RuntimeParameter.newBuilder()
          .setName(PARAMETER_CHANNEL)
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("NQA Channel")
          .setDefaultValue(PARAMETER_DEFAULT_CHANNEL)
          .setRequired(false);
      result.addParams(channel);

      Provider.RuntimeParameter.Builder config = Provider.RuntimeParameter.newBuilder()
          .setName(PARAMETER_CONFIG)
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("NQA Config")
          .setDefaultValue(PARAMETER_DEFAULT_CONFIG)
          .setRequired(false);
      result.addParams(config);

      logger.trace("GetRuntimeParameters response: {}", result);
      responseObserver.onNext(result.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10004, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getRuntimeParameter")
            .setLine(262)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA가 동작하는 데 필요한 여러가지 옵션 값을 처리하는 DA Provider Parameter를 반환
   */
  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server GetProviderParameter");

    try {
      // init, isReady 등 여러 함수에서 정의된 provider 값 return
      logger.trace("GetProviderParameter response {}", provider);
      responseObserver.onNext(provider.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10005, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getProviderParameter")
            .setLine(299)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다. 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
   */
  @Override
  public void openSession(Talk.OpenSessionRequest request,
      StreamObserver<TalkResponse> responseObserver) {
    logger.info("DA V3 Server OpenSession");

    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSession request: {}", request);

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10101,
              "chatbot or skill or session.id is null");

          ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSession")
              .setLine(339)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
      }

      TalkResponse.Builder response = TalkResponse.newBuilder();
      response.setResponse(
          SpeechResponse.newBuilder()
              .setSpeech(
                  Speech.newBuilder().setLang(request.getUtter().getLang())
                      .setUtter("질문을 입력해주세요~!").build()) // open message
              .setSessionUpdate(
                  Session.newBuilder()
                      .setId(request.getSession().getId()).build())
              .setMeta( // meta 정보 저장
                  Struct.newBuilder().putFields("context_test_1", // key
                      // value, setIntValue, setStringValue 등으로 type 설정
                      Value.newBuilder().setStringValue("ctx_1")
                          .build()).build()).build());
      logger.trace("OpenSession response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10006, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSession")
            .setLine(378)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 해당하는 스킬을 최초로 호출 할 때 호출된다. 싱글턴 일 경우에는 호출되지 않는다. 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
   */
  @Override
  public void openSkill(Talk.OpenSkillRequest request,
      StreamObserver<OpenSkillResponse> responseObserver) {
    logger.info("DA V3 Server OpenSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSkill request: {}", request);

    OpenSkillResponse.Builder response = OpenSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10102,
              "chatbot or skill or session.id is null");

          ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSkill")
              .setLine(419)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
      }

      response.setMeta( // meta 정보 저장
          Struct.newBuilder().putFields("meta_info",
              Value.newBuilder().setStringValue("This is openSkill")
                  .build()).build())
          .setSessionUpdate( // request의 session으로 업데이트
              Session.newBuilder().setId(
                  request.getSession().getId())
                  .build()).build();

      logger.trace("OpenSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10007, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSkill")
            .setLine(453)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * BQA와 실질적으로 대화하는 함수. TalkRequest의 session에서 이전 계층 정보를 받아온다. 이후 생성된 계층 정보는
   * TalkResponse.SpeechResponse의 session_update에 넣어준다. 한번 답변이 완료된 경우 새로운 새션으로 대화를 시작해야함.
   **/
  @Override
  public void talk(Talk.TalkRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {
    logger.info("DA V3 Server Talk");
//    logger.trace("Talk request: {}", request);
//    logger.trace("Talk User Meta info(Replacement) : {}", request.getUtter().getMeta());

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10103,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("Talk")
              .setLine(500)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("error")) {
        try {
          status = new ai.maum.rpc.ResultStatus(maum.rpc.Status.ExCode.LOGICAL_ERROR,
              10106,
              "Error for Result Status.");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile =
              ErrorDetails.DebugInfoFile.newBuilder()
                  .setFunction("Talk")
                  .setLine(523)
                  .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("deadline")) {
        try {
          Thread.sleep(35000);
          responseObserver.onCompleted();
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
        return;
      }

      Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

      Speech.Builder speech = Speech.newBuilder();
      Card.Builder card = Card.newBuilder();
      Directive.Builder directive = Directive.newBuilder();
      Struct.Builder context = Struct.newBuilder();

      String question = request.getUtter().getUtter();  // 질문 or 계층정보

      List<String> resultQueryList = new ArrayList<>();
      Struct.Builder structBuilder = Struct.newBuilder();
      boolean closeFlag = false;

      logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      logger.info(request.getSession().getContext().toString());  // context에 담긴 정보 출력(이전 데이터)
      logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

      SearchAnswerResponse searchAnswerResponse = nqaConnect(request.getSession().getContext(),
          question, context, resultQueryList, structBuilder);  // NQA 검색
      String finalAnswer = "";
      if (searchAnswerResponse.getAnswerResultCount() > 0) {
        logger.debug("답변 {}개!", searchAnswerResponse.getAnswerResultCount());
        int count = Math.min(searchAnswerResponse.getAnswerResultCount(), 3);
        for (int i = 1; i <= count; i++) {
          finalAnswer += String.valueOf(i) + "-->" + structBuilder.getFieldsMap()
              .get("TOP_" + String.valueOf(i) + "_Value").getStructValue().getFieldsMap()
              .get("AstringView").getStringValue() + "\n";
        }
      } else {
        logger.debug("답변 없음!!");
        finalAnswer = "답변을 찾을 수 없습니다.";
      }
      context.putFields("layerNum", Value.newBuilder().setStringValue("Finish").build());
      context.putFields("Qid_Aid_Map", Value.newBuilder().setStringValue("Finish").build());

      speech.setLang(Lang.ko_KR)
          .setUtter(finalAnswer)
          .setSpeechUtter("TTS 출력: " + request.getUtter().getUtter())
          .setReprompt(request.getUtter().getUtter().equals("expected response"));

      directive.setInterface("Avatar") // directive 저장
          .setOperation("SetExpression")
          .setParam(DialogAgentForwarderParam.newBuilder()
              .setChatbot("NQAChatbot")
              .setSkill("bqa")
              .setIntent("intent")
              .setSessionId(1234).build())
          .setPayload(DirectivePayload.newBuilder()
              .setAvatarSetExpression(AvatarSetExpressionPayload.newBuilder()
                  .setExpression("smile")
                  .setDesciption("avata is happy")
//                  .setDesciption(fullQuery + fullValue)
                  .build()).build()).build();

      response.setResponse(SpeechResponse.newBuilder()
          .setSpeech(speech)
          .addCards(card)
          .addDirectives(directive)
          .setCloseSkill(closeFlag)
          .setMeta(structBuilder.build())
//          .setMeta(Struct.newBuilder().putAllFields(context.getFieldsMap().get("allResult").getStructValue().getFieldsMap()))
          .setSessionUpdate(Session.newBuilder().setContext(context.build())))
          .build();

//      logger.info("Talk response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10008, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("talk")
            .setLine(714)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  private void setNqaDocument(NqaOptionDocument nqaOptionDocument, String key, String value) {
    switch (key) {
      case "A1":
        nqaOptionDocument.getAttribute().setAttr1(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A2":
        nqaOptionDocument.getAttribute().setAttr2(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A3":
        nqaOptionDocument.getAttribute().setAttr3(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A4":
        nqaOptionDocument.getAttribute().setAttr4(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A5":
        nqaOptionDocument.getAttribute().setAttr5(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A6":
        nqaOptionDocument.getAttribute().setAttr6(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L1":
        nqaOptionDocument.getLayer().setLayer1(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L2":
        nqaOptionDocument.getLayer().setLayer2(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L3":
        nqaOptionDocument.getLayer().setLayer3(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L4":
        nqaOptionDocument.getLayer().setLayer4(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L5":
        nqaOptionDocument.getLayer().setLayer5(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L6":
        nqaOptionDocument.getLayer().setLayer6(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "TAG":
        nqaOptionDocument.getTags().add(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "Q":
      case "A":
      case "QA":
      case "6":
      case "7":
      case "SA":
      case "SQ":
      case "LA":
      case "AA":
      case "NQ":
      case "NA":
        nqaOptionDocument.getUserQuery().setPhrase(value);
        break;
      case "AID":
        nqaOptionDocument.getIds().addAll(Arrays.asList(value.split(",")));
        break;
      case "SCORE":
        nqaOptionDocument.setScore(Float.valueOf(value));
        break;
      case "WEIGHT":
        nqaOptionDocument.getUserQuery().setWeight(Float.valueOf(value));
        break;
      case "SIZE":
        nqaOptionDocument.setMaxSize(Float.valueOf(value));
        break;
    }
  }

  private SearchAnswerResponse searchAnswerTask(String userQuery, String queryT,
      NQaServiceBlockingStub stub, Struct.Builder contextOut,
      List<String> resultQueryList, Struct.Builder structBuilder) {
    String[] queryArray = queryT.split("-");
    QueryTarget queryTarget = parseQueryTarget(queryArray[0]);
    QueryType queryType = parseQueryType(queryArray[1]);
    SearchAnswerRequest.Builder searchAnswerRequestBuilder = SearchAnswerRequest.newBuilder()
        .setQueryTarget(queryTarget).setQueryType(queryType);
    fillASlot(searchAnswerRequestBuilder, userQuery);

    SearchAnswerResponse searchAnswerResponse = stub
        .searchAnswer(searchAnswerRequestBuilder.build());

    if (searchAnswerResponse.getAnswerResultCount() > 0
        && queryTarget != QueryTarget.AID) {  // 처음 답이 있을 경우 + AID검색이 아닐경우
      List<String> AidList = new ArrayList<>();
      for (Answer answer : searchAnswerResponse.getAnswerResultList()) {
        AidList.add(answer.getId());
      }
      contextOut.putFields("Answer_ids",
          Value.newBuilder().setStringValue(String.join(",", AidList)).build());
    }

    if (!(queryTarget == QueryTarget.Q) && !(queryTarget == QueryTarget.GRAMQ)
        && searchAnswerResponse.getAnswerResultCount() > 0) { // Q검색이 아니면서 처음 답이 있을경우
      putTopResult(contextOut, structBuilder, searchAnswerResponse.getAnswerResultList(), 3);
    }

    return searchAnswerResponse;
  }

  private void putTopResult(Struct.Builder contextOut, Struct.Builder structBuilder,
      List<Answer> answerList, int topNum) {
    HashMap<String, Answer> currentAnswerMap = new HashMap<>();
    for (Answer answer : answerList) {
      currentAnswerMap.put(answer.getId(), answer);
    }

    List<String> originAnsweridList = Arrays
        .asList(contextOut.getFieldsMap().get("Answer_ids").getStringValue().split(","));
    Map<String, Value> qIdInfoMap = new HashMap<>();
    if (contextOut.containsFields("Q_Info_Map")) {
      qIdInfoMap = contextOut.getFieldsMap().get("Q_Info_Map").getStructValue().getFieldsMap();
    }
    int i = 0;
    for (String aId : originAnsweridList) {
      if (currentAnswerMap.containsKey(aId)) {
        Struct.Builder topStruct = Struct.newBuilder();
        Answer answer = currentAnswerMap.get(aId);
        if (qIdInfoMap.size() > 0) {
          ListValue qValueList = qIdInfoMap.get(aId).getListValue();
          topStruct.putFields("Qscore", qValueList.getValues(3));
          topStruct.putFields("Aid", Value.newBuilder().setStringValue(aId).build());
          topStruct.putFields("Qid", qValueList.getValues(0));
          topStruct.putFields("Qstring", qValueList.getValues(1));
          topStruct.putFields("Qmorph", qValueList.getValues(2));
        } else {
          topStruct.putFields("Ascore",
              Value.newBuilder().setStringValue(String.valueOf(answer.getScore())).build());
          topStruct.putFields("Aid", Value.newBuilder().setStringValue(aId).build());
        }
        topStruct
            .putFields("Astring", Value.newBuilder().setStringValue(answer.getAnswer()).build());
        topStruct.putFields("AstringView",
            Value.newBuilder().setStringValue(answer.getAnswerView()).build());
        topStruct.putFields("Aattr1",
            Value.newBuilder().setStringValue(answer.getAttributes().getAttr1()).build());
        topStruct.putFields("Aattr2",
            Value.newBuilder().setStringValue(answer.getAttributes().getAttr2()).build());
        topStruct.putFields("Aattr3",
            Value.newBuilder().setStringValue(answer.getAttributes().getAttr3()).build());
        topStruct.putFields("Aattr4",
            Value.newBuilder().setStringValue(answer.getAttributes().getAttr4()).build());
        topStruct.putFields("Aattr5",
            Value.newBuilder().setStringValue(answer.getAttributes().getAttr5()).build());
        topStruct.putFields("Aattr6",
            Value.newBuilder().setStringValue(answer.getAttributes().getAttr6()).build());
        topStruct.putFields("Alayer1",
            Value.newBuilder().setStringValue(answer.getLayers().getLayer1()).build());
        topStruct.putFields("Alayer2",
            Value.newBuilder().setStringValue(answer.getLayers().getLayer2()).build());
        topStruct.putFields("Alayer3",
            Value.newBuilder().setStringValue(answer.getLayers().getLayer3()).build());
        topStruct.putFields("Alayer4",
            Value.newBuilder().setStringValue(answer.getLayers().getLayer4()).build());
        topStruct.putFields("Alayer5",
            Value.newBuilder().setStringValue(answer.getLayers().getLayer5()).build());
        topStruct.putFields("Alayer6",
            Value.newBuilder().setStringValue(answer.getLayers().getLayer6()).build());
        i++;
        structBuilder.putFields("TOP_" + String.valueOf(i) + "_Value",
            Value.newBuilder().setStructValue(topStruct).build());
        if (i == topNum) {  // 상위n개까지만
          break;
        }
      }
    }

  }

  // Question 검색 처리
  private void searchQuestionTask(String userQuery, String queryT, List<String> idList,
      NQaServiceBlockingStub stub, Struct.Builder contextOut,
      List<String> resultQueryList, Struct.Builder structBuilder) {
    logger.info("queryArray=>" + queryT);
    String[] queryArray = queryT.split("-");
    QueryTarget queryTarget = parseQueryTarget(queryArray[0]);
    QueryType queryType = parseQueryType(queryArray[1]);
    SearchQuestionRequest.Builder searchQuestionRequestBuilder = SearchQuestionRequest.newBuilder()
        .setQueryTarget(queryTarget).setQueryType(queryType);
    fillQSlot(searchQuestionRequestBuilder, userQuery);

    SearchQuestionResponse searchQuestionResponse = stub
        .searchQuestion(searchQuestionRequestBuilder.build());

    if (searchQuestionResponse.getIdsCount() > 0) {  // 처음으로 답이 있을경우
      idList.addAll(searchQuestionResponse.getIdsList());
      contextOut.putFields("Answer_ids",
          Value.newBuilder().setStringValue(String.join(",", searchQuestionResponse.getIdsList()))
              .build()).build();  // Score기준 answerid 저장(Top3 출력용)
      Struct.Builder qidAidMap = Struct.newBuilder();
      Struct.Builder qInfoMap = Struct.newBuilder();
      for (Question question : searchQuestionResponse.getQuestionsList()) {
        qidAidMap.putFields(question.getId(),
            Value.newBuilder().setStringValue(question.getAnswerId()).build());
        qInfoMap.putFields(question.getAnswerId(),
            Value.newBuilder().setListValue(ListValue.newBuilder()
                .addValues(0, Value.newBuilder().setStringValue(question.getId()).build())  //q_id
                .addValues(1,
                    Value.newBuilder().setStringValue(question.getQuestion()).build())  //Q_string
                .addValues(2, Value.newBuilder().setStringValue(question.getQuestionMorp())
                    .build())  //Q_morph
                .addValues(3, Value.newBuilder().setStringValue(String.valueOf(question.getScore()))
                    .build())  //Q_score
                .build()).build());
      }
      contextOut.putFields("Qid_Aid_Map",
          Value.newBuilder().setStructValue(qidAidMap).build());  // qid, aid 매칭맵
      contextOut.putFields("Q_Info_Map", Value.newBuilder().setStructValue(qInfoMap)
          .build());  // aid, qid 매칭맵(Top3출력을 위해 부가정보들 추가(string,morph,score))
    }
  }

  // QueryTarget 파싱 함수
  private QueryTarget parseQueryTarget(String queryTarget) {
    switch (queryTarget) {
      case "Q":
        return QueryTarget.Q;
      case "SQ":
        return QueryTarget.GRAMQ;
      case "NQ":
      case "NA":
        return QueryTarget.NER;
      case "A":
        return QueryTarget.A;
      case "SA":
        return QueryTarget.GRAMA;
      case "AA":
        return QueryTarget.ATTRALL;
      case "LA":
        return QueryTarget.LAYERALL;
      case "AID":
        return QueryTarget.AID;
      case "TAG":
        return QueryTarget.TAG;
    }
    return QueryTarget.Q;
  }

  // QueryType 파싱 함수
  private QueryType parseQueryType(String queryType) {
    switch (queryType) {
      case "all":
        return QueryType.ALL;
      case "or":
        return QueryType.OR;
      case "and":
        return QueryType.AND;
    }
    return QueryType.ALL;
  }

  // 모든 단일 QA 처리(Q일경우 A까지 결과 받아옴)
  // 검색 타입에 따른 검색(Q,A,LA,...)
  private SearchAnswerResponse searchNQA(String userQuery, String queryTarget,
      NQaServiceBlockingStub stub, Struct.Builder contextOut,
      List<String> resultQueryList, Struct.Builder structBuilder) {
    SearchAnswerResponse searchAnswerResponse = SearchAnswerResponse.newBuilder().build();
    List<String> idList = new ArrayList<>();
    switch (queryTarget.split("-")[0]) {
      case "Q":
      case "SQ":
      case "NQ":  // Question검색일 경우 Question검색 후 Answer까지 검색하여 결과 반환
        searchQuestionTask(userQuery, queryTarget, idList, stub, contextOut, resultQueryList,
            structBuilder);
        if (idList.size() > 0) {
          userQuery = "@AID:" + String.join(",", idList);
          searchAnswerResponse = searchAnswerTask(userQuery, "AID-all", stub, contextOut,
              resultQueryList, structBuilder);
        }
        break;
      default:  // Answer검색일 경우 A만 검색하여 결과 반환
        searchAnswerResponse = searchAnswerTask(userQuery, queryTarget, stub, contextOut,
            resultQueryList, structBuilder);
        break;
    }
    return searchAnswerResponse;
  }

  // NQA검색 실행 (queryTarget을 List형태로 받아와 순차적으로 검색 실행)
  private SearchAnswerResponse searchNQATargets(List<String> queryTargetList, String userQuery,
      List<String> resultQueryList, Struct.Builder structBuilder, Struct.Builder contextOut) {
    ManagedChannel channel = ManagedChannelBuilder
        .forAddress(NqaOptionDocument.getNqaIp(), NqaOptionDocument.getNqaPort())
        .usePlaintext(true)
        .build();
    NQaServiceBlockingStub stub = NQaServiceGrpc.newBlockingStub(channel);
    SearchAnswerResponse searchAnswerResponse = SearchAnswerResponse.newBuilder().build();

    logger.info("queryTargetList=>" + queryTargetList.toString());

    for (String queryTarget : queryTargetList) {  //순차적으로 검색
      searchAnswerResponse = searchNQA(userQuery, queryTarget, stub, contextOut, resultQueryList,
          structBuilder);
      if (searchAnswerResponse.getAnswerResultCount() > 0) {
        logger.info("searchAsnwerResponse Fisrt answer step--> " + queryTarget);
        break;
      }
    }
    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return searchAnswerResponse;
  }

  // 검색 타입, 순서를 결정
  private void setQueryList(List<String> queryTargetList, String userQuery) {
    String[] userQueryArray = userQuery.split(":");
    switch (userQueryArray[0].replace("@", "").trim()) {
      case "Q":
      case "SQ":
      case "NQ":
      case "A":
      case "SA":
      case "NA":
      case "AA":
      case "LA":
      case "TAG":
        queryTargetList.add(userQueryArray[0].replace("@", "").trim());
        break;
      case "AID":
        queryTargetList.add("AID-all");
        break;
      case "6":
        queryTargetList.addAll(Arrays.asList("NQ", "Q", "NA", "A", "AA", "LA"));
        break;
      case "7":
      case "":
      default:
//        queryTargetList.addAll(Arrays.asList("NQ","Q","NA","A","AA","LA","SQ","SA"));
//        queryTargetList.addAll(Arrays.asList("Q","A","LA","SQ","SA"));
        queryTargetList.addAll(NqaOptionDocument.getQuery());
        break;
    }
  }

  /**
   * NQA와 통신하는 함수 이전 계층 정보를 포함한 context, 질문(계층), 이후계층을 저장할 context를 입력받음. 답변 결과들을 리턴한다.
   **/
  private SearchAnswerResponse nqaConnect(Struct context, String userQuery,
      Struct.Builder contextOut,
      List<String> resultQueryList, Struct.Builder structBuilder) {
    logger.info("question ==> " + userQuery);
    SearchAnswerResponse searchAnswerResponse = SearchAnswerResponse.newBuilder().build();

    List<String> idList = new ArrayList<>();  // 답변 검색할 ID 리스트

    List<String> queryTargetList = new ArrayList<>();

    if (!context.getFieldsMap().containsKey("layerNum") || "Finish"
        .equalsIgnoreCase(context.getFieldsMap().get("layerNum").getStringValue())) {  // 첫 질문
      if (!userQuery.startsWith("@")) {  // 질문시작에 @없을때 Default로 7설정
        userQuery = "@7:" + userQuery;
      }
      setQueryList(queryTargetList, userQuery);
      searchAnswerResponse = searchNQATargets(queryTargetList, userQuery, resultQueryList,
          structBuilder, contextOut);
    }

    for (Answer answer : searchAnswerResponse.getAnswerResultList()) {
      idList.add(answer.getId());
    }
    contextOut
        .putFields("idList", Value.newBuilder().setStringValue(String.join(",", idList)).build());

    return searchAnswerResponse;
  }

  // 여러 NQA검색 옵션들 세팅
  private void fillNqaOptionDocument(NqaOptionDocument nqaOptionDocument, String query) {
    logger.info("UserQuery => " + query);
    List<String> qList = Arrays.asList(query.split("@"));
    boolean first = true;

    if (qList.size() > 0) {
      for (String slot : qList) {
        if (first) {
          first = false;
        } else {
          String[] slotList = slot.split(":");
          setNqaDocument(nqaOptionDocument, slotList[0].trim(), slotList[1].trim());
        }
      }
    }
  }

  // Answer검색을 하기위한 조건 설정
  private void fillASlot(SearchAnswerRequest.Builder searchAnswerRequestBuilder, String query) {
    NqaOptionDocument nqaOptionDocument = new NqaOptionDocument();
    fillNqaOptionDocument(nqaOptionDocument, query);

    searchAnswerRequestBuilder.setNtop(nqaOptionDocument.getNtop());
    searchAnswerRequestBuilder.addAllCategory(nqaOptionDocument.getCategory());
    searchAnswerRequestBuilder.setChannel(nqaOptionDocument.getChannel());
//    searchAnswerRequestBuilder.setQueryType(nqaOptionDocument.getQueryType());
    searchAnswerRequestBuilder.addAllIds(nqaOptionDocument.getIds());
//    searchQuestionRequestBuilder.setQueryTarget(nqaOptionDocument.getQueryTarget());
    searchAnswerRequestBuilder.setUserQuery(nqaOptionDocument.getUserQuery());
    searchAnswerRequestBuilder.setAttributes(nqaOptionDocument.getAttribute());
    searchAnswerRequestBuilder.setLayer(nqaOptionDocument.getLayer());
    searchAnswerRequestBuilder.addAllNer(nqaOptionDocument.getNer());
    searchAnswerRequestBuilder.addAllTags(nqaOptionDocument.getTags());
  }

  // Question검색을 하기위한 조건 설정
  private void fillQSlot(SearchQuestionRequest.Builder searchQuestionRequestBuilder, String query) {
    NqaOptionDocument nqaOptionDocument = new NqaOptionDocument();
    fillNqaOptionDocument(nqaOptionDocument, query);

    searchQuestionRequestBuilder.setNtop(nqaOptionDocument.getNtop());
    searchQuestionRequestBuilder.setScore(nqaOptionDocument.getScore());
    searchQuestionRequestBuilder.setMaxSize(nqaOptionDocument.getMaxSize());
    searchQuestionRequestBuilder.addAllCategory(nqaOptionDocument.getCategory());
    searchQuestionRequestBuilder.setChannel(nqaOptionDocument.getChannel());
//    searchQuestionRequestBuilder.setQueryType(nqaOptionDocument.getQueryType());
//    searchQuestionRequestBuilder.setQueryTarget(nqaOptionDocument.getQueryTarget());
    searchQuestionRequestBuilder.setUserQuery(nqaOptionDocument.getUserQuery());
    searchQuestionRequestBuilder.setAttributes(nqaOptionDocument.getAttribute());
    searchQuestionRequestBuilder.addAllNer(nqaOptionDocument.getNer());
    searchQuestionRequestBuilder.addAllTags(nqaOptionDocument.getTags());
  }

  /**
   * DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다. 다음과 같은 동작이 예가 될 수 있다. 로봇이 특정한 위치로 이동하라고 명명하고 수행이
   * 완료되었을 경우에 내보낸다.
   **/
  @Override
  public void event(Talk.EventRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {

    logger.info("DA V3 Server Event");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Event request: {}", request);

    Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10104, "session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(756)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
      }

      response.setResponse(
          SpeechResponse.newBuilder().setSpeech( // event에 대한 응답 메세지
              Speech.newBuilder().setUtter("This is event response")
                  .build())
              .setMeta(Struct.newBuilder().putFields("token", // 추가적인 정보 전달
                  Value.newBuilder().setStringValue(
                      request.getEvent().getPayload().getLauncherAuthorized().getAccessToken())
                      .build()).build())
              .setSessionUpdate( // session 업데이트
                  Session.newBuilder().setId(
                      request.getSession().getId())
                      .build()).build()).build();

      logger.trace("Event response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("event")
            .setLine(794)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 SKILL이 종료되었음을 알려준다. SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로 매우 주의해야 한다.
   **/
  @Override
  public void closeSkill(Talk.CloseSkillRequest request,
      StreamObserver<Talk.CloseSkillResponse> responseObserver) {
    logger.info("DA V3 Server CloseSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("CloseSkill request: {}", request);

    CloseSkillResponse.Builder response = CloseSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10105,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(835)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
      }

      response.setSessionUpdate(
          Session.newBuilder()
              .setId(request.getSession().getId()) // session update
              .setContext(request.getSession().getContext()) // session context update
              .build()).build();

      logger.trace("CloseSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("closeSkill")
            .setLine(866)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }
}
