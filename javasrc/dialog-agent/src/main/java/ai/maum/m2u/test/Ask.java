package ai.maum.m2u.test;


import ai.maum.m2u.client.EchoDialogAgentClient;
import ai.maum.m2u.utils.GrpcClientPool;
import ai.maum.m2u.utils.ThreadPool;
import ai.maum.m2u.utils.Utils;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.protobuf.util.JsonFormat;
import maum.m2u.da.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;


public class Ask {
  private static final Logger logger = LoggerFactory.getLogger(Ask.class);

  private static void callDaInit(final GrpcClientPool<EchoDialogAgentClient> pool) {
    EchoDialogAgentClient client = null;
    try {
      client = pool.getClient();
      final ListenableFuture<Provider.DialogAgentProviderParam> future = client.init();
      Provider.DialogAgentProviderParam param = future.get(10, TimeUnit.SECONDS);
      pool.releaseClient(client);
      client = null;
      logger.debug("DaV3 init:: {}", JsonFormat.printer().includingDefaultValueFields().print(param));

    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      logger.error("{} => ", e.getMessage(), e);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    if (client != null) {
      pool.releaseClient(client);
    }


  }

  private static void callDaEcho(final GrpcClientPool<EchoDialogAgentClient> pool) {
    EchoDialogAgentClient client = null;
    try {
      client = pool.getClient();
      final ListenableFuture<Provider.DialogAgentProviderParam> future = client.init();
      Provider.DialogAgentProviderParam param = future.get(10, TimeUnit.SECONDS);
      pool.releaseClient(client);
      client = null;
      logger.debug("DaV3 echo:: {}", JsonFormat.printer().includingDefaultValueFields().print(param));

    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      logger.error("{} => ", e.getMessage(), e);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    if (client != null) {
      pool.releaseClient(client);
    }


  }

  public static void main(String[] args) {
    ThreadPool.init(1, 5);

    GrpcClientPool<EchoDialogAgentClient> pool =
        EchoDialogAgentClient.pool.init(
            3, "127.0.0.1", 1111, ThreadPool.getExecutor());

    long startTick = System.currentTimeMillis();
    callDaInit(pool);
    System.out.println(" took " + (System.currentTimeMillis() - startTick) + "ms.");
    callDaEcho(pool);


    ThreadPool.finish(false);
    pool.finish();
  }
}
