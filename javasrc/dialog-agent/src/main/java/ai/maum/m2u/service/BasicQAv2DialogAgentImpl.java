package ai.maum.m2u.service;

import ai.maum.m2u.utils.ServerMetaInterceptor;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.Empty;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import maum.brain.qa.basicqa.BasicQaServiceGrpc;
import maum.brain.qa.basicqa.BasicQaServiceGrpc.BasicQaServiceBlockingStub;
import maum.brain.qa.basicqa.BasicQaServiceGrpc.BasicQaServiceImplBase;
import maum.brain.qa.basicqa.Basicqa.SearchAnswerResponse.Answer;
import maum.brain.qa.basicqa.Basicqa.LayerQueryUnit;
import maum.brain.qa.basicqa.Basicqa.QueryUnit;
import maum.brain.qa.basicqa.Basicqa.QueryType;
import maum.brain.qa.basicqa.Basicqa.SearchAnswerResponse;
import maum.brain.qa.basicqa.Basicqa.SearchAnswerRequest;
import maum.brain.qa.basicqa.Basicqa.SearchQuestionRequest;
import maum.brain.qa.basicqa.Basicqa.SearchQuestionResponse;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.CardOuterClass.Card;
import maum.m2u.common.CardOuterClass.SelectCard;
import maum.m2u.common.CardOuterClass.SelectCard.Item;
import maum.m2u.common.Dialog.Session;
import maum.m2u.common.Dialog.Speech;
import maum.m2u.common.DirectiveOuterClass.AvatarSetExpressionPayload;
import maum.m2u.common.DirectiveOuterClass.Directive;
import maum.m2u.common.DirectiveOuterClass.Directive.DirectivePayload;
import maum.m2u.common.EventOuterClass.DialogAgentForwarderParam;
import maum.m2u.common.Types.DataType;
import maum.m2u.da.Provider;
import maum.m2u.da.Provider.AgentKind;
import maum.m2u.da.Provider.DialogAgentProviderParam;
import maum.m2u.da.Provider.DialogAgentState;
import maum.m2u.da.Provider.DialogAgentStatus;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.da.v3.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import maum.m2u.da.v3.Talk;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import maum.m2u.da.v3.Talk.OpenSkillResponse;
import maum.m2u.da.v3.Talk.SpeechResponse;
import maum.m2u.da.v3.Talk.TalkResponse;
import maum.rpc.ErrorDetails;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicQAv2DialogAgentImpl extends DialogAgentProviderImplBase {

  static final private Logger logger = LoggerFactory.getLogger(BasicQaServiceImplBase.class);

  private DialogAgentProviderParam.Builder provider = DialogAgentProviderParam.newBuilder();
  private Provider.InitParameter initParam;
  private DialogAgentState state;
  private String skill;
  private int count = 0;
  private String db_host;
  private int db_port;
  private String question_category;

  public BasicQAv2DialogAgentImpl() {
    logger.debug("DA V3 Server Constructor");
    state = DialogAgentState.DIAG_STATE_IDLE;
    logger.debug("DialogAgentState: {}", state);
  }

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때, 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는
   * 함수 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장하는 과정 수행 Init() 함수에서
   * DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중) Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다.
   * (동작중)
   */
  @Override
  public void init(Provider.InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server Init");
    logger.trace("Init request: {}", request);

    try {
      state = DialogAgentState.DIAG_STATE_INITIALIZING; // DA instance state 초기화

      provider.setName("name") // provider 정보 setting
          .setDescription("BQA answer is returnedDA")
          .setVersion("3.0")
          .setSingleTurn(true)
          .setAgentKind(AgentKind.AGENT_SDS)
          .setRequireUserPrivacy(true);

      initParam = request;
      logger.trace("Init Param : {}", initParam);
      logger.trace("Init ParamsMap : {}", initParam.getParamsMap());

      db_host = initParam.getParamsOrDefault("db_host"," ");
      db_port = Integer.parseInt(initParam.getParamsOrDefault("db_port"," "));
      question_category = initParam.getParamsOrDefault("question_category"," ");
      logger.trace("Init Params_db_host : {}", initParam.getParamsOrDefault("db_host"," "));
      logger.trace("Init Params_db_port : {}", initParam.getParamsOrDefault("db_port"," "));
      logger.trace("Init Params_question_category : {}", initParam.getParamsOrDefault("question_category"," "));

      state = DialogAgentState.DIAG_STATE_RUNNING;

      //throw new Exception("exception test");

      logger.trace("Init response {}", provider);
      responseObserver.onNext(provider.build()); // provider를 response로 return
      responseObserver.onCompleted();

    } catch (Exception e) { // 예외 처리
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10001, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("init")
            .setLine(108)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("init e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수 Dialog Agent Instance 동작에 따라 상태 변경 필요 생성자에서
   * DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   */
  @Override
  public void isReady(Empty request, StreamObserver<Provider.DialogAgentStatus> responseObserver) {
    logger.info("DA V3 Server IsReady");

    try {
      DialogAgentStatus.Builder response = DialogAgentStatus.newBuilder();

      response.setState(state); // DA instance 상태 저장

      logger.debug("IsReady response: {}", response);
      responseObserver.onNext(response.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10002, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("isReady")
            .setLine(148)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("isReady e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 동작중인 DA Instance을 종료시키는 함수 Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경해줘야 합니다.
   */
  @Override
  public void terminate(Empty request, StreamObserver<Empty> responseObserver) {
    logger.info("DA V3 Server Terminate");

    try {
      state = DialogAgentState.DIAG_STATE_TERMINATED; // DA instance state 변경
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10003, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("terminate")
            .setLine(183)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("terminate e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 실제로 DA Instnace가 돌아가기 위해 필요한 paramerters를 정의 db 정보 정의
   */
  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    logger.info("DA V3 Server GetRuntimeParameters");

    try {
      RuntimeParameterList.Builder result = RuntimeParameterList.newBuilder();

      Provider.RuntimeParameter.Builder db_host = Provider.RuntimeParameter.newBuilder(); // db ip
      db_host.setName("db_host")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Host")
          .setDefaultValue("10.122.64.212")
          .setRequired(true);
      result.addParams(db_host);

      Provider.RuntimeParameter.Builder db_port = Provider.RuntimeParameter.newBuilder(); // db port
      db_port.setName("db_port")
          .setType(DataType.DATA_TYPE_INT)
          .setDesc("Database Port")
          .setDefaultValue("50052")
          .setRequired(true);
      result.addParams(db_port);

      Provider.RuntimeParameter.Builder question_category = Provider.RuntimeParameter.newBuilder(); // question_category
      question_category.setName("question_category")
              .setType(DataType.DATA_TYPE_QUESTION_ANSWERING_CATEGORY)
              .setDesc("Category select")
              .setDefaultValue(" ")
              .setRequired(true);
      result.addParams(question_category);

      logger.trace("GetRuntimeParameters response: {}", result);
      responseObserver.onNext(result.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10004, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getRuntimeParameter")
            .setLine(262)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getRuntimeParameters e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA가 동작하는 데 필요한 여러가지 옵션 값을 처리하는 DA Provider Parameter를 반환
   */
  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server GetProviderParameter");

    try {
      // init, isReady 등 여러 함수에서 정의된 provider 값 return
      logger.trace("GetProviderParameter response {}", provider);
      responseObserver.onNext(provider.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10005, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getProviderParameter")
            .setLine(299)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getProviderParameter e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다. 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
   */
  @Override
  public void openSession(Talk.OpenSessionRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {
    logger.info("DA V3 Server OpenSession");

    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSession request: {}", request);

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10101,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSession")
              .setLine(339)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSession e : " , e);
          responseObserver.onError(e);
        }
      }

      Talk.TalkResponse.Builder response = Talk.TalkResponse.newBuilder();
      response.setResponse(
          SpeechResponse.newBuilder()
              .setSpeech(
                  Speech.newBuilder().setLang(request.getUtter().getLang())
                      .setUtter("안녕하세요~ Basic QA 챗봇 입니다. 궁금하신 질문을 입력해주세요~!").build()) // open message
              .setSessionUpdate(
                  Session.newBuilder()
                      .setId(request.getSession().getId()).build())
              .setMeta( // meta 정보 저장
                  Struct.newBuilder().putFields("context_test_1", // key
                      // value, setIntValue, setStringValue 등으로 type 설정
                      Value.newBuilder().setStringValue("ctx_1")
                          .build()).build()).build());
      logger.trace("OpenSession response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10006, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSession")
            .setLine(378)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSession e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 해당하는 스킬을 최초로 호출 할 때 호출된다. 싱글턴 일 경우에는 호출되지 않는다. 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
   */
  @Override
  public void openSkill(Talk.OpenSkillRequest request,
      StreamObserver<Talk.OpenSkillResponse> responseObserver) {
    logger.info("DA V3 Server OpenSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSkill request: {}", request);

    OpenSkillResponse.Builder response = OpenSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10102,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSkill")
              .setLine(419)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSkill e : " , e);
          responseObserver.onError(e);
        }
      }
      skill = request.getSkill();
      response.setMeta( // meta 정보 저장
          Struct.newBuilder().putFields("meta_info",
              Value.newBuilder().setStringValue("This is openSkill")
                  .build()).build())
          .setSessionUpdate( // request의 session으로 업데이트
              Session.newBuilder().setId(
                  request.getSession().getId())
                  .build()).build();

      logger.trace("OpenSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10007, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSkill")
            .setLine(453)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSkill e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * BQA와 실질적으로 대화하는 함수.
   * TalkRequest의 session에서 이전 계층 정보를 받아온다.
   * 이후 생성된 계층 정보는 TalkResponse.SpeechResponse의 session_update에 넣어준다.
   * 한번 답변이 완료된 경우 새로운 새션으로 대화를 시작해야함.
   **/
  @Override
  public void talk(Talk.TalkRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {
    logger.info("DA V3 Server Talk");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Talk request: {}", request);
    logger.trace("Talk Utter Meta info(Replacement) : {}", request.getUtter().getMeta());

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10103,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("Talk")
              .setLine(500)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("talk e : " , e);
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("error")) {
        try {
          status = new ai.maum.rpc.ResultStatus(maum.rpc.Status.ExCode.LOGICAL_ERROR,
              10106,
              "Error for Result Status.");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile =
              ErrorDetails.DebugInfoFile.newBuilder()
                  .setFunction("Talk")
                  .setLine(523)
                  .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("talk e : " , e);
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("deadline")) {
        try {
          Thread.sleep(35000);
          responseObserver.onCompleted();
        } catch (Exception e) {
          logger.error("talk e : " , e);
          responseObserver.onError(e);
        }
        return;
      }

      Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

      Speech.Builder speech = Speech.newBuilder();
      Card.Builder card = Card.newBuilder();
      Directive.Builder directive = Directive.newBuilder();
      Struct.Builder context = Struct.newBuilder();

      String question = request.getUtter().getUtter();  // 질문 or 계층정보

      SearchAnswerResponse answerOutputs = bqaConnect(request.getSession().getContext(), question, context);  // BQA 검색
      String finalAnswer = "";
      if (answerOutputs.getAnswerResultCount() > 0) {
        if (answerOutputs.getAnswerResultCount()>1) {
          logger.debug("답변 여러개 카드 송출");
          card.setSelect(insertLayerCard(checkLayer(answerOutputs, context), context));
        } else {
          logger.debug("답변 한개 답변!");
          finalAnswer = answerOutputs.getAnswerResult(0).getAnswer();
        }
      } else {
        logger.debug("답변 없음!!");
        finalAnswer = "답변을 찾을 수 없습니다.";
      }

      speech.setLang(Lang.ko_KR)
          .setUtter(finalAnswer)
          .setSpeechUtter("TTS 출력: " + request.getUtter().getUtter())
          .setReprompt(request.getUtter().getUtter().equals("expected response"));

      directive.setInterface("Avatar") // directive 저장
          .setOperation("SetExpression")
          .setParam(DialogAgentForwarderParam.newBuilder()
              .setChatbot("BasicQAChatbot")
              .setSkill("bqa")
              .setIntent("intent")
              .setSessionId(1234).build())
          .setPayload(DirectivePayload.newBuilder()
              .setAvatarSetExpression(AvatarSetExpressionPayload.newBuilder()
                  .setExpression("smile")
                  .setDesciption("avata is happy")
                  .build()).build()).build();

      response.setResponse(SpeechResponse.newBuilder()
          .setSpeech(speech)
          .addCards(card)
          .addDirectives(directive)
          .setCloseSkill(false)
          .setSessionUpdate(Session.newBuilder().setContext(context.build())))
          .build();

      logger.info("Talk response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10008, e.getMessage());
        logger.info("status : {}", status);
        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("talk")
            .setLine(714)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("talk e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * BasicQA와 통신하는 함수
   * 이전 계층 정보를 포함한 context, 질문(계층), 이후계층을 저장할 context를 입력받음.
   * 답변 결과들을 리턴한다.
   **/
  private SearchAnswerResponse bqaConnect(Struct context, String question, Struct.Builder contextOut) {
    logger.info("question ==> " + question);
    SearchAnswerResponse answerOutputs = SearchAnswerResponse.newBuilder().build();

    String nowLayernum = "";  // 현재 입력 받아온 계층 넘버
    List<String> idList = new ArrayList<String>();  // 답변 검색할 ID 리스트

    if (!context.getFieldsMap().containsKey("layerNum")) {  // 첫 질문인지 확인
      idList = searchQuestion(question);
      contextOut.putFields("idList",Value.newBuilder().setStringValue(String.join(",",idList)).build());
    } else {  // 계층 선택인 경우
      nowLayernum = context.getFieldsMap().get("layerNum").getStringValue();
      idList = Arrays.asList(context.getFieldsMap().get("idList").getStringValue().split(","));
    }

    logger.info("IdResult ==> " + idList.toString());
    if (idList.size() > 0) {
      HashMap<String, String> layerMap = makeLayerMap(context, nowLayernum, question);
      answerOutputs = searchAnswer(idList, layerMap);
    }
    return answerOutputs;
  }

  /**
   * BQA와 통신하여 질문 검색을 하는 함수.
   * @param question 질문내용.
   * @return 질문을 검색하여 나온 답변의 ID들을 리턴.
   */
  private List<String> searchQuestion(String question) {
    logger.info("searchQuestion Start");
    logger.info("db_host {}", db_host);
    logger.info("db_port {}", db_port);
    logger.info("question {}", question);
    ManagedChannel channel = ManagedChannelBuilder.forAddress(db_host, db_port).usePlaintext(true).build();
    BasicQaServiceBlockingStub stub = BasicQaServiceGrpc.newBlockingStub(channel);

    SearchQuestionRequest searchQuestionInput = SearchQuestionRequest.newBuilder()
        .setNtop(5)  // 검색 결과로 뽑아올 최대 ID 갯수
        .setScore(6)  // 검색 결과의 최소 스코어
        .setMaxSize(2)  // 검색 결과의 최대 사이즈(형태소 분석 결과 갯수)
        .setQueryType(QueryType.ALL)  // 검색 타입
        .setUserQuery(QueryUnit.newBuilder().setPhrase(question))
        .build();

    SearchQuestionResponse questionOutput = stub.searchQuestion(searchQuestionInput);

    logger.debug("question_output_count ==> " + questionOutput.getIdsCount());

    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (Exception e) {
      logger.error("searchQuestion e : " , e);
    }
    return questionOutput.getIdsList();
  }

  /**
   * 답변 검색
   * @param idList 질문 검색 결과(답변 ID 리스트)
   * @param layerMap 계층 정보들
   * @return 답변 검색 결과들
   */
  private SearchAnswerResponse searchAnswer(List<String> idList, HashMap<String, String> layerMap) {
    ManagedChannel channel = ManagedChannelBuilder.forAddress(db_host, db_port).usePlaintext(true).build();
    BasicQaServiceBlockingStub stub = BasicQaServiceGrpc.newBlockingStub(channel);

    SearchAnswerRequest searchAnswerInput = SearchAnswerRequest.newBuilder()
        .setNtop(5)
        .addAllIds(idList)
        .setLayer(LayerQueryUnit.newBuilder()
                .setLayer1(QueryUnit.newBuilder().setPhrase(layerMap.get("layer1")).build())
                .setLayer2(QueryUnit.newBuilder().setPhrase(layerMap.get("layer2")).build())
                .setLayer3(QueryUnit.newBuilder().setPhrase(layerMap.get("layer3")).build())
                .setLayer4(QueryUnit.newBuilder().setPhrase(layerMap.get("layer4")).build())
                .build())
        .build();

    SearchAnswerResponse answerOutputs = stub.searchAnswer(searchAnswerInput);

    return answerOutputs;
  }

  /**
   * 계층 정보 확인(계층 후보가 복수인지 단일인지 확인.)
   * @param answerOutputs 답변 검색 결과들
   * @param context 세션에 저장할 context
   * @return 화면에 나타낼 카드(계층 후보) 목록
   */
  private HashSet<String> checkLayer(SearchAnswerResponse answerOutputs, Struct.Builder context) {

    ArrayList<String> layer1 = new ArrayList<String>();
    ArrayList<String> layer2 = new ArrayList<String>();
    ArrayList<String> layer3 = new ArrayList<String>();
    ArrayList<String> layer4 = new ArrayList<String>();

    List<Answer> answerResultList = answerOutputs.getAnswerResultList();

    for (Answer answerResult : answerResultList) {
      layer1.add(answerResult.getLayers().getLayer1());
      layer2.add(answerResult.getLayers().getLayer2());
      layer3.add(answerResult.getLayers().getLayer3());
      layer4.add(answerResult.getLayers().getLayer4());
    }

    HashMap<String, ArrayList<String>> layer = new HashMap<String, ArrayList<String>>();

    layer.put("layer1", layer1);
    layer.put("layer2", layer2);
    layer.put("layer3", layer3);
    layer.put("layer4", layer4);

    HashSet<String> final_set = new HashSet<String>();

    for(int i=1; i<5; i++) {
      HashSet<String> set = new HashSet<String>(layer.get("layer" + String.valueOf(i)));
      logger.debug("layer_list ==> " + set.toString());
      if(set.size() > 1) {
        final_set = set;
        context.putFields("layerNum",Value.newBuilder().setStringValue("layer" + String.valueOf(i)).build());
        break;
      } else {
        for (String temp : set) {
          context.putFields("layer" + String.valueOf(i),Value.newBuilder().setStringValue(temp).build());
        }
      }
    }
    logger.info("layer_num ==> " + context.getFieldsMap().get("layerNum").getStringValue());
    logger.info("final_set ==> " + final_set.toString());
    return final_set;
  }

  private SelectCard.Builder insertLayerCard(HashSet<String> set, Struct.Builder context) {
    SelectCard.Builder selectCard = SelectCard.newBuilder();

    try {
      selectCard.setTitle(selectLayerName(context) + "을 골라주세요")
          .setHeader("messageId: 1234").setHorizontal(true).build();
      for (String setName : set) {
        selectCard.addItems(Item.newBuilder()
            .setTitle(setName)
            .setSelectedUtter(setName)).build();
      }
    } catch (Exception e) {
      logger.error(e.toString());
    }
    return selectCard;
  }

  /**
   * 계층의 속성이름 리턴
   */
  private String selectLayerName(Struct.Builder context) {
    HashMap<String, String> layerName = new HashMap<String, String>();
    layerName.put("layer1","구분1");
    layerName.put("layer2","구분2");
    layerName.put("layer3","질문유형");
    layerName.put("layer4","주화면");

    return layerName.get(context.getFieldsMap().get("layerNum").getStringValue());
  }

  /**
   * 이전 계층 정보를 HashMap 형태로 반환
   * @param context 이전 계층 정보들
   * @param nowLayer 현 계층 넘버
   * @param layerName 현 계층 정보
   * @return 총 합한 계층 HashMap
   */
  private HashMap<String, String> makeLayerMap(Struct context, String nowLayer, String layerName) {
    HashMap<String, String> layerMap = new HashMap<String, String>();
    for (int i=1; i<5; i++) {
      if (context.getFieldsMap().containsKey("layer" + String.valueOf(i))) {
        layerMap.put("layer" + String.valueOf(i),context.getFieldsMap().get("layer" + String.valueOf(i)).getStringValue());
        logger.info("layer" + String.valueOf(i) + "==>" + context.getFieldsMap().get("layer" + String.valueOf(i)).getStringValue());
      } else if (StringUtils.equalsIgnoreCase(nowLayer, "layer" + String.valueOf(i))) {
        layerMap.put(nowLayer, layerName);
      } else {
        layerMap.put("layer" + String.valueOf(i),"");
        logger.info("layer" + String.valueOf(i) + "==>" + " ");
      }
    }
    return layerMap;
  }

  /**
   * DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다. 다음과 같은 동작이 예가 될 수 있다. 로봇이 특정한 위치로 이동하라고 명명하고 수행이
   * 완료되었을 경우에 내보낸다.
   **/
  @Override
  public void event(Talk.EventRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {

    logger.info("DA V3 Server Event");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Event request: {}", request);

    Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10104, "session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(756)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("event e : " , e);
          responseObserver.onError(e);
        }
      }

      response.setResponse(
          SpeechResponse.newBuilder().setSpeech( // event에 대한 응답 메세지
              Speech.newBuilder().setUtter("This is event response")
                  .build())
              .setMeta(Struct.newBuilder().putFields("token", // 추가적인 정보 전달
                  Value.newBuilder().setStringValue(
                      request.getEvent().getPayload().getLauncherAuthorized().getAccessToken())
                      .build()).build())
              .setSessionUpdate( // session 업데이트
                  Session.newBuilder().setId(
                      request.getSession().getId())
                      .build()).build()).build();

      logger.trace("Event response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("event")
            .setLine(794)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("event e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 SKILL이 종료되었음을 알려준다. SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로 매우 주의해야 한다.
   **/
  @Override
  public void closeSkill(Talk.CloseSkillRequest request,
      StreamObserver<Talk.CloseSkillResponse> responseObserver) {
    logger.info("DA V3 Server CloseSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("CloseSkill request: {}", request);

    CloseSkillResponse.Builder response = CloseSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10105,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(835)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("closeSkill e : " , e);
          responseObserver.onError(e);
        }
      }

      skill = null;
      response.setSessionUpdate(
          Session.newBuilder()
              .setId(request.getSession().getId()) // session update
              .setContext(request.getSession().getContext()) // session context update
              .build()).build();

      logger.trace("CloseSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("closeSkill")
            .setLine(866)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("closeSkill e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }
}
