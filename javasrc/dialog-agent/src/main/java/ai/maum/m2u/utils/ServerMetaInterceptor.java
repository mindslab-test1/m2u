package ai.maum.m2u.utils;

import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 모든 Request에 대하여 Header확인 후 sigin-in, isValid to Service, reject 처리
 */
public class ServerMetaInterceptor implements ServerInterceptor {

  private static final Logger logger = LoggerFactory.getLogger(ServerMetaInterceptor.class);

  public static final String META_OPERAION_SYNC_ID = "x-operation-sync-id";
  public static final Context.Key<String> OPERAION_SYNC_ID = Context.key(META_OPERAION_SYNC_ID);

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(
      ServerCall<ReqT, RespT> serverCall,
      Metadata metadata,
      ServerCallHandler<ReqT, RespT> serverCallHandler) {

    if (logger.isTraceEnabled()) {
      logger.trace("Metadatas:");
      for (String key : metadata.keys()) {
        logger.trace("  {}: {}", key,
            metadata.get(Metadata.Key.of(key, Metadata.ASCII_STRING_MARSHALLER)));
      }
    }

    try {
      if (metadata.containsKey(
          Metadata.Key.of(META_OPERAION_SYNC_ID, Metadata.ASCII_STRING_MARSHALLER))) {

        String operationSyncId = metadata.get(
            Metadata.Key.of(META_OPERAION_SYNC_ID, Metadata.ASCII_STRING_MARSHALLER));

        Context context = Context.current().withValue(OPERAION_SYNC_ID, operationSyncId);

        return Contexts.interceptCall(context, serverCall, metadata, serverCallHandler);
      } else {
        return serverCallHandler.startCall(serverCall, metadata);
      }
    } catch (Exception e) {
      serverCall.close(Status.INTERNAL, new Metadata());
      return null;
    }
  }
}
