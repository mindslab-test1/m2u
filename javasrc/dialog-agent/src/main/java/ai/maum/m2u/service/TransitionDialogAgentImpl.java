package ai.maum.m2u.service;


import ai.maum.m2u.utils.ServerMetaInterceptor;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.Empty;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.Dialog.Session;
import maum.m2u.common.Dialog.Speech;
// import maum.m2u.common.Userattr.DataType;
import maum.m2u.common.Types.DataType;
import maum.m2u.da.Provider;
import maum.m2u.da.Provider.AgentKind;
import maum.m2u.da.Provider.DialogAgentProviderParam;
import maum.m2u.da.Provider.DialogAgentState;
import maum.m2u.da.Provider.DialogAgentStatus;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.da.v3.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import maum.m2u.da.v3.Talk;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import maum.m2u.da.v3.Talk.OpenSkillResponse;
import maum.m2u.da.v3.Talk.SkillTransition;
import maum.m2u.da.v3.Talk.SpeechResponse;
import maum.m2u.da.v3.Talk.TalkResponse;
import maum.rpc.ErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TransitionDialogAgentImpl extends DialogAgentProviderImplBase {

  static final Logger logger = LoggerFactory.getLogger(TransitionDialogAgentImpl.class);

  private DialogAgentProviderParam.Builder provider
      = DialogAgentProviderParam.newBuilder();
  private Provider.InitParameter initParam;
  private DialogAgentState state;
  private String skill;
  private int count = 0;


  public TransitionDialogAgentImpl() {
    logger.debug("DA V3 Server Constructor");
    state = DialogAgentState.DIAG_STATE_IDLE;
    logger.debug("DialogAgentState: {}", state);
  }

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때, 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는
   * 함수 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장하는 과정 수행 Init() 함수에서
   * DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중) Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다.
   * (동작중)
   */
  @Override
  public void init(Provider.InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server Init");
    logger.trace("Init request: {}", request);

    try {
      state = DialogAgentState.DIAG_STATE_INITIALIZING; // DA instance state 초기화

      provider.setName("name") // provider 정보 setting
          .setDescription("control intention return DA")
          .setVersion("3.0")
          .setSingleTurn(true)
          .setAgentKind(AgentKind.AGENT_SDS)
          .setRequireUserPrivacy(true);

      initParam = request;
      state = DialogAgentState.DIAG_STATE_RUNNING;

      logger.trace("Init response {}", provider);
      responseObserver.onNext(provider.build()); // provider를 response로 return
      responseObserver.onCompleted();

    } catch (Exception e) { // 예외 처리
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30001, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("init")
            .setLine(58)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("init e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수 Dialog Agent Instance 동작에 따라 상태 변경 필요 생성자에서
   * DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   */
  @Override
  public void isReady(Empty request,
      StreamObserver<DialogAgentStatus> responseObserver) {
    logger.info("DA V3 Server IsReady");

    try {
      DialogAgentStatus.Builder response = DialogAgentStatus.newBuilder();

      response.setState(state); // DA instance 상태 저장

      logger.debug("IsReady response: {}", response);
      responseObserver.onNext(response.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30002, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("isReady")
            .setLine(109)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("isReady e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 동작중인 DA Instance을 종료시키는 함수 Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경해줘야 합니다.
   */
  @Override
  public void terminate(Empty request,
      StreamObserver<Empty> responseObserver) {
    logger.info("DA V3 Server Terminate");

    try {
      state = DialogAgentState.DIAG_STATE_TERMINATED; // DA instance state 변경
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30003, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("terminate")
            .setLine(149)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("terminate e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 실제로 DA Instnace가 돌아가기 위해 필요한 paramerters를 정의 db 정보 정의
   */
  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    logger.info("DA V3 Server GetRuntimeParameters");

    try {
      RuntimeParameterList.Builder result = RuntimeParameterList.newBuilder();

      Provider.RuntimeParameter.Builder db_host = Provider.RuntimeParameter.newBuilder(); // db ip
      db_host.setName("db_host")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Host")
          .setDefaultValue("171.64.122.134")
          .setRequired(true);
      result.addParams(db_host);

      Provider.RuntimeParameter.Builder db_port = Provider.RuntimeParameter.newBuilder(); // db port
      db_port.setName("db_port")
          .setType(DataType.DATA_TYPE_INT)
          .setDesc("Database Port")
          .setDefaultValue("7701")
          .setRequired(true);
      result.addParams(db_port);

      Provider.RuntimeParameter.Builder db_user = Provider.RuntimeParameter.newBuilder(); // db user
      db_user.setName("db_user")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database User")
          .setDefaultValue("minds")
          .setRequired(true);
      result.addParams(db_user);

      Provider.RuntimeParameter.Builder db_pwd = Provider.RuntimeParameter.newBuilder(); // db pwd
      db_pwd.setName("db_pwd")
          .setType(DataType.DATA_TYPE_AUTH)
          .setDesc("Database Password")
          .setDefaultValue("minds67~")
          .setRequired(true);
      result.addParams(db_pwd);

      Provider.RuntimeParameter.Builder db_database = Provider.RuntimeParameter
          .newBuilder(); // db name
      db_database.setName("db_database")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Database name")
          .setDefaultValue("ascar")
          .setRequired(true);
      result.addParams(db_database);

      logger.trace("GetRuntimeParameters response: {}", result);
      responseObserver.onNext(result.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30004, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getRuntimeParameter")
            .setLine(185)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getRuntimeParameters e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA가 동작하는 데 필요한 여러가지 옵션 값을 처리하는 DA Provider Parameter를 반환
   */
  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server GetProviderParameter");

    try {
      // init, isReady 등 여러 함수에서 정의된 provider 값 return
      logger.trace("GetProviderParameter response {}", provider);
      responseObserver.onNext(provider.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30005, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getProviderParameter")
            .setLine(264)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getProviderParameter e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다. 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
   */
  @Override
  public void openSession(Talk.OpenSessionRequest request,
      StreamObserver<TalkResponse> responseObserver) {
    logger.info("DA V3 Server OpenSession");

    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSession request: {}", request);

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 30101,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSession")
              .setLine(314)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSession e : " , e);
          responseObserver.onError(e);
        }
      }

      TalkResponse.Builder response = TalkResponse.newBuilder();
      response.setResponse(
          SpeechResponse.newBuilder()
              .setSpeech(
                  Speech.newBuilder().setLang(request.getUtter().getLang())
                      .setUtter("hi!!").build()) // open message
              .setSessionUpdate(
                  Session.newBuilder()
                      .setId(request.getSession().getId()).build())
              .setMeta( // meta 정보 저장
                  Struct.newBuilder().putFields("context_test_1", // key
                      // value, setIntValue, setStringValue 등으로 type 설정
                      Value.newBuilder().setStringValue("ctx_1")
                          .build()).build()).build());
      logger.trace("OpenSession response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30006, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSession")
            .setLine(301)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSession e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 해당하는 스킬을 최초로 호출 할 때 호출된다. 싱글턴 일 경우에는 호출되지 않는다. 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
   */
  @Override
  public void openSkill(Talk.OpenSkillRequest request,
      StreamObserver<OpenSkillResponse> responseObserver) {
    logger.info("DA V3 Server OpenSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSkill request: {}", request);

    OpenSkillResponse.Builder response = OpenSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 30102,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSkill")
              .setLine(394)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSkill e : " , e);
          responseObserver.onError(e);
        }
      }

      skill = request.getSkill();
      response.setMeta( // meta 정보 저장
          Struct.newBuilder().putFields("meta_info",
              Value.newBuilder().setStringValue("This is openSkill")
                  .build()).build())
          .setSessionUpdate( // request의 session으로 업데이트
              Session.newBuilder().setId(
                  request.getSession().getId())
                  .build()).build();

      logger.trace("OpenSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30007, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSkill")
            .setLine(380)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSkill e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 대화를 주고 응답을 생성하도록 한다. <SpeechResponse> 일반 발화시 echo "chart" 발화시 echo + chartCart return "select"
   * 발화시 echo + selectCart return "link" 발화시 echo + linkCart return "custom" 발화시 echo + linkListCart
   * return "customlist" 발화시 echo + customListCart return <DialogDelegate> "delegate slotname" 발화시
   * LauncherFillSlotsPayload 타입의 dialogDelegate return
   **/
  @Override
  public void talk(Talk.TalkRequest request,
      StreamObserver<TalkResponse> responseObserver) {
    logger.info("DA V3 Server Talk");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Talk request: {}", request);
    logger.trace("Talk Utter Meta info(Replacement) : {}", request.getUtter().getMeta());

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 30103,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("Talk")
              .setLine(471)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("talk e : " , e);
          responseObserver.onError(e);
        }
      }

      TalkResponse.Builder response = TalkResponse.newBuilder();
      int responseType = 0;

      Speech.Builder speech = Speech.newBuilder();
      boolean close_skill = false;
      String[] words = request.getUtter().getUtter().split("\\s");

      if (request.getUtter().getUtter().equals("end")) {
        close_skill = true;
      }

      if (words[0].equals("reset")) {
        response.setTransit(SkillTransition.newBuilder()
            .setTransitionType(SkillTransition.TransitionType.SKILL_TRANS_RESET)
            .setSkill(words.length > 1 ? words[1] : "final")
            .setReplacedUtter("transit reset.")
            .setMeta(Struct.newBuilder()
                .putFields("meta1", Value.newBuilder()
                    .setStringValue("meta value 1").build())
                .putFields("meta2", Value.newBuilder()
                    .setBoolValue(false).build())
                .build())
            .setSessionUpdate(Session.newBuilder()
                .setId(request.getSession().getId())
                .build())
            .build())
            .build();
        responseType = 1;
      } else {
        // reprompt
        speech.setLang(Lang.ko_KR) // 발화문 echo 구조
            .setUtter("[" + skill + "] " + request.getUtter().getUtter())
            .setSpeechUtter("TTS 출력: " + request.getUtter().getUtter())
            .setReprompt(request.getUtter().getUtter().equals("expected response"));
      }

      if (responseType == 0) {
        response.setResponse(SpeechResponse.newBuilder()
            .setSpeech(speech)
            .setCloseSkill(close_skill)
            .setSessionUpdate(Session.newBuilder()
                .setContext(Struct.newBuilder().putFields("context1",
                    Value.newBuilder().setNumberValue(++count).build()))));
      }

      logger.trace("Talk response: {}", response);
      // 계속해서 정보를 전달할 때 on next를 순차적으로 추가해준다. 처음 보내는 정보는 echo
      responseObserver.onNext(response.build());
      //responseObserver.onNext(param.build()); // 카드의 종류에 따라 순차적으로 보낸다.
      responseObserver.onCompleted(); // 보낼 정보들이 끝날 때 한 번만 해주면 된다.
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30008, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("talk")
            .setLine(458)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("talk e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다. 다음과 같은 동작이 예가 될 수 있다. 로봇이 특정한 위치로 이동하라고 명명하고 수행이
   * 완료되었을 경우에 내보낸다.
   **/
  @Override
  public void event(Talk.EventRequest request,
      StreamObserver<TalkResponse> responseObserver) {

    logger.info("DA V3 Server Event");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Event request: {}", request);

    TalkResponse.Builder response = TalkResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 30104, "session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(571)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("event e : " , e);
          responseObserver.onError(e);
        }
      }

      response.setResponse(
          SpeechResponse.newBuilder().setSpeech( // event에 대한 응답 메세지
              Speech.newBuilder().setUtter("This is event response")
                  .build())
              .setMeta(Struct.newBuilder().putFields("token", // 추가적인 정보 전달
                  Value.newBuilder().setStringValue(
                      request.getEvent().getPayload().getLauncherAuthorized().getAccessToken())
                      .build()).build())
              .setSessionUpdate( // session 업데이트
                  Session.newBuilder().setId(
                      request.getSession().getId())
                      .build()).build()).build();

      logger.trace("Event response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("event")
            .setLine(556)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("event e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 SKILL이 종료되었음을 알려준다. SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로 매우 주의해야 한다.
   **/
  @Override
  public void closeSkill(Talk.CloseSkillRequest request,
      StreamObserver<CloseSkillResponse> responseObserver) {
    logger.info("DA V3 Server CloseSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("CloseSkill request: {}", request);

    CloseSkillResponse.Builder response = CloseSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 30105,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(649)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("closeSkill e : " , e);
          responseObserver.onError(e);
        }
      }

      skill = null;
      response.setSessionUpdate(
          Session.newBuilder()
              .setId(request.getSession().getId()) // session update
              .setContext(request.getSession().getContext()) // session context update
              .build()).build();

      logger.trace("CloseSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 30009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("closeSkill")
            .setLine(635)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("closeSkill e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }
}
