package ai.maum.m2u.service;


import ai.maum.m2u.utils.ServerMetaInterceptor;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.Empty;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import io.grpc.Channel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import maum.brain.sds.Resolver.ModelGroup;
import maum.brain.sds.Resolver.ServerStatus;
import maum.brain.sds.Sds.DialogueParam;
import maum.brain.sds.Sds.Entities;
import maum.brain.sds.Sds.Intent;
import maum.brain.sds.Sds.OpenResult;
import maum.brain.sds.Sds.SdsQuery;
import maum.brain.sds.Sds.SdsUtter;
import maum.brain.sds.SdsServiceResolverGrpc;
import maum.brain.sds.SpokenDialogServiceGrpc;
import maum.common.LangOuterClass.Lang;
import maum.common.LangOuterClass.LangCode;
import maum.m2u.common.Dialog.Session;
import maum.m2u.common.Dialog.Speech;
// import maum.m2u.common.Userattr.DataType;
import maum.m2u.common.Types.DataType;
import maum.m2u.da.Provider;
import maum.m2u.da.Provider.AgentKind;
import maum.m2u.da.Provider.DialogAgentProviderParam;
import maum.m2u.da.Provider.DialogAgentState;
import maum.m2u.da.Provider.DialogAgentStatus;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.da.v3.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import maum.m2u.da.v3.Talk;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import maum.m2u.da.v3.Talk.OpenSkillResponse;
import maum.m2u.da.v3.Talk.SpeechResponse;
import maum.m2u.da.v3.Talk.TalkResponse;
import maum.rpc.ErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SdsSingleDialogAgentImpl extends DialogAgentProviderImplBase {

  static final Logger logger = LoggerFactory.getLogger(SdsSingleDialogAgentImpl.class);

  private DialogAgentProviderParam.Builder provider
      = DialogAgentProviderParam.newBuilder();
  private Provider.InitParameter initParam;
  private DialogAgentState state;
  private int count = 0;

  public SdsSingleDialogAgentImpl() {
    logger.debug("DA V3 Server Constructor");
    state = DialogAgentState.DIAG_STATE_IDLE;
    logger.debug("DialogAgentState: {}", state);
  }

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때, 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는
   * 함수 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장하는 과정 수행 Init() 함수에서
   * DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중) Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다.
   * (동작중)
   */
  @Override
  public void init(Provider.InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server Init");
    logger.trace("Init request: {}", request);

    try {
      state = DialogAgentState.DIAG_STATE_INITIALIZING; // DA instance state 초기화

      provider.setName("name") // provider 정보 setting
          .setDescription("control intention return DA")
          .setVersion("3.0")
          .setSingleTurn(true)
          .setAgentKind(AgentKind.AGENT_SDS)
          .setRequireUserPrivacy(true);

      initParam = request;
      state = DialogAgentState.DIAG_STATE_RUNNING;

      logger.trace("Init response {}", provider);
      responseObserver.onNext(provider.build()); // provider를 response로 return
      responseObserver.onCompleted();

    } catch (Exception e) { // 예외 처리
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40001, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("init")
            .setLine(70)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("init e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수 Dialog Agent Instance 동작에 따라 상태 변경 필요 생성자에서
   * DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   */
  @Override
  public void isReady(Empty request,
      StreamObserver<DialogAgentStatus> responseObserver) {
    logger.info("DA V3 Server IsReady");

    try {
      DialogAgentStatus.Builder response = DialogAgentStatus.newBuilder();

      response.setState(state); // DA instance 상태 저장

      logger.trace("IsReady response: {}", response);
      responseObserver.onNext(response.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40002, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("isReady")
            .setLine(121)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("isReady e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 동작중인 DA Instance을 종료시키는 함수 Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경해줘야 합니다.
   */
  @Override
  public void terminate(Empty request,
      StreamObserver<Empty> responseObserver) {
    logger.info("DA V3 Server Terminate");

    try {
      state = DialogAgentState.DIAG_STATE_TERMINATED; // DA instance state 변경
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40003, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("terminate")
            .setLine(161)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("terminate e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 실제로 DA Instnace가 돌아가기 위해 필요한 paramerters를 정의 db 정보 정의
   */
  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    logger.info("DA V3 Server GetRuntimeParameters");

    try {
      RuntimeParameterList.Builder result = RuntimeParameterList.newBuilder();

      Provider.RuntimeParameter.Builder db_host = Provider.RuntimeParameter.newBuilder(); // db ip
      db_host.setName("db_host")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Host")
          .setDefaultValue("171.64.122.134")
          .setRequired(true);
      result.addParams(db_host);

      Provider.RuntimeParameter.Builder db_port = Provider.RuntimeParameter.newBuilder(); // db port
      db_port.setName("db_port")
          .setType(DataType.DATA_TYPE_INT)
          .setDesc("Database Port")
          .setDefaultValue("7701")
          .setRequired(true);
      result.addParams(db_port);

      Provider.RuntimeParameter.Builder db_user = Provider.RuntimeParameter.newBuilder(); // db user
      db_user.setName("db_user")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database User")
          .setDefaultValue("minds")
          .setRequired(true);
      result.addParams(db_user);

      Provider.RuntimeParameter.Builder db_pwd = Provider.RuntimeParameter.newBuilder(); // db pwd
      db_pwd.setName("db_pwd")
          .setType(DataType.DATA_TYPE_AUTH)
          .setDesc("Database Password")
          .setDefaultValue("minds67~")
          .setRequired(true);
      result.addParams(db_pwd);

      Provider.RuntimeParameter.Builder db_database = Provider.RuntimeParameter
          .newBuilder(); // db name
      db_database.setName("db_database")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Database name")
          .setDefaultValue("ascar")
          .setRequired(true);
      result.addParams(db_database);

      logger.trace("GetRuntimeParameters response: {}", result);
      responseObserver.onNext(result.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40004, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getRuntimeParameter")
            .setLine(197)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getRuntimeParameters e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA가 동작하는 데 필요한 여러가지 옵션 값을 처리하는 DA Provider Parameter를 반환
   */
  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server GetProviderParameter");

    try {
      // init, isReady 등 여러 함수에서 정의된 provider 값 return
      logger.trace("GetProviderParameter response {}", provider);
      responseObserver.onNext(provider.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40005, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getProviderParameter")
            .setLine(276)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getProviderParameter e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다. 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
   */
  @Override
  public void openSession(Talk.OpenSessionRequest request,
      StreamObserver<TalkResponse> responseObserver) {
    logger.info("DA V3 Server OpenSession");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSession request: {}", request);

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 40101,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSession")
              .setLine(325)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSession e : " , e);
          responseObserver.onError(e);
        }
      }

      TalkResponse.Builder response = TalkResponse.newBuilder();
      response.setResponse(
          SpeechResponse.newBuilder()
              .setSpeech(
                  Speech.newBuilder().setLang(request.getUtter().getLang())
                      .setUtter("hi!!").build()) // open message
              .setSessionUpdate(
                  Session.newBuilder()
                      .setId(request.getSession().getId()).build())
              .setMeta( // meta 정보 저장
                  Struct.newBuilder().putFields("context_test_1", // key
                      // value, setIntValue, setStringValue 등으로 type 설정
                      Value.newBuilder().setStringValue("ctx_1")
                          .build()).build()).build());
      logger.trace("OpenSession response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40006, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSession")
            .setLine(312)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSession e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 해당하는 스킬을 최초로 호출 할 때 호출된다. 싱글턴 일 경우에는 호출되지 않는다. 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
   */
  @Override
  public void openSkill(Talk.OpenSkillRequest request,
      StreamObserver<OpenSkillResponse> responseObserver) {
    logger.info("DA V3 Server OpenSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSkill request: {}", request);

    OpenSkillResponse.Builder response = OpenSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 40102,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSkill")
              .setLine(405)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSkill e : " , e);
          responseObserver.onError(e);
        }
      }

      response.setMeta( // meta 정보 저장
          Struct.newBuilder().putFields("meta_info",
              Value.newBuilder().setStringValue("This is openSkill")
                  .build()).build())
          .setSessionUpdate( // request의 session으로 업데이트
              Session.newBuilder().setId(
                  request.getSession().getId())
                  .build()).build();

      logger.trace("OpenSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40007, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSkill")
            .setLine(391)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSkill e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 대화를 주고 응답을 생성하도록 한다. <SpeechResponse> 일반 발화시 echo "chart" 발화시 echo + chartCart return "select"
   * 발화시 echo + selectCart return "link" 발화시 echo + linkCart return "custom" 발화시 echo + linkListCart
   * return "customlist" 발화시 echo + customListCart return <DialogDelegate> "delegate slotname" 발화시
   * LauncherFillSlotsPayload 타입의 dialogDelegate return
   **/
  @Override
  public void talk(Talk.TalkRequest request,
      StreamObserver<TalkResponse> responseObserver) {
    logger.info("DA V3 Server Talk");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Talk request: {}", request);

    String modelName = "model_name";
    long session = request.getSession().getId();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 40103,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("Talk")
              .setLine(484)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("talk e : " , e);
          responseObserver.onError(e);
        }
      }

      TalkResponse.Builder response = TalkResponse.newBuilder();

      // resolver channel 생성
      Channel resChannel = ManagedChannelBuilder
          .forTarget("0.0.0.0:9860").usePlaintext(true).build();

      // resolver stub 생성
      SdsServiceResolverGrpc.SdsServiceResolverBlockingStub resolverStub
          = SdsServiceResolverGrpc.newBlockingStub(resChannel);

      // modelGroup 정보 저장
      ModelGroup modelGroup = ModelGroup.newBuilder()
          .setName("group_name")
          .setLang(LangCode.kor) // 영어일 경우 LangCode.eng
          .setIsExternal(true).build(); // internal이면 false

      // sub_sds 정보 받아오기
      ServerStatus serverStatus = null;
      //serverStatus = resolverStub.find(modelGroup);
      //logger.debug("<ServerStatus> {}", serverStatus);
      try {
        serverStatus = resolverStub.find(modelGroup);
        logger.debug("<ServerStatus> {}", serverStatus);
      } catch (Exception e) {
        ai.maum.rpc.ResultStatus rs = ai.maum.rpc.ResultStatusListProto.fromThrowable(e);
        ai.maum.rpc.ResultStatus resultStatus = new ai.maum.rpc.ResultStatus(rs.getResultStatusList());

        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.RUNTIME_ERROR, 40501, e.getMessage());

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("talk.find")
              .setLine(527)
              .build();

          status.addDetail(infoFile);
          status.addStackStatus(resultStatus);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e1) {
          logger.error("talk e1 : " , e1);
          responseObserver.onError(e);
        }
      }

      // 받아온 정보로 sub_sds channel 생성
      Channel sdsChannel = ManagedChannelBuilder
          .forTarget(serverStatus.getServerAddress()).usePlaintext(true).build();

      // sub_sds stub 생성
      // internal일 경우,
      // SpokenDialogServiceInternalGrpc.SpokenDialogServiceInternalBlockingStub
      // sdsStub = SpokenDialogServiceInternalGrpc.newBlockingStub(sdsChannel);
      SpokenDialogServiceGrpc.SpokenDialogServiceBlockingStub sdsStub
          = SpokenDialogServiceGrpc.newBlockingStub(sdsChannel);

      DialogueParam dialogueParam = DialogueParam.newBuilder()
          .setModel(modelName) // sds model name
          .setSessionKey(session) // session
          // 슬롯의 초기값으로 지정할 데이터를 넣어준다.
          // <슬롯명, 데이터>의 맵 형태로 사용한다.
          // 사용자 발화, DB에서 슬롯의 값을 찾을 수 없을 경우,
          // 이때 넣어준 데이터에서 값을 가져온다.
          // "<date=오늘> <city=서울> 날씨 어때?"라는 문장을 학습 시켰을 때,
          // "오늘 날씨 어때?"라고 사용자가 발화하면
          // 서울이라는 값을 가져와 "city=서울"로 인식한다.
          .putSlots("city", "서울")
          .putSlots("date", "오늘")
          // 사용자 발화가 먼저이면 true, 시스템 발화가 먼저이면 false
          .setUserInitiative(false).build();

      // 대화 세션을 생성한다.
      try {
        OpenResult openResult = sdsStub.open(dialogueParam);
        logger.debug("<OpenResult> {}", openResult);
      } catch (Exception e) {
        ai.maum.rpc.ResultStatus rs = ai.maum.rpc.ResultStatusListProto.fromThrowable(e);
        ai.maum.rpc.ResultStatus resultStatus = new ai.maum.rpc.ResultStatus(rs.getResultStatusList());

        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(maum.rpc.Status.ExCode.RUNTIME_ERROR,
              40502,
              e.getMessage());

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("talk.open")
              .setLine(582)
              .build();

          status.addDetail(infoFile);
          status.addStackStatus(resultStatus);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e1) {
          logger.error("talk e1 : " , e1);
          responseObserver.onError(e);
        }
      }

      SdsQuery sdsQuery = SdsQuery.newBuilder()
          .setModel(modelName)
          .setSessionKey(session) // session
          .setUtter(request.getUtter().getUtter()).build(); // 사용자 발화

      // 요청에 들어있는 텍스트로 대화 이해를 시도하고
      // 결과를 받아서 슬롯 정보를 채워준다.
      try {
        Intent intent = sdsStub.understand(sdsQuery);
        logger.debug("<Intent> {}", intent);
      } catch (Exception e) {
        ai.maum.rpc.ResultStatus rs = ai.maum.rpc.ResultStatusListProto.fromThrowable(e);
        ai.maum.rpc.ResultStatus resultStatus = new ai.maum.rpc.ResultStatus(rs.getResultStatusList());

        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(maum.rpc.Status.ExCode.RUNTIME_ERROR,
              40503,
              e.getMessage());

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("talk.understand")
              .setLine(618)
              .build();

          status.addDetail(infoFile);
          status.addStackStatus(resultStatus);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e1) {
          logger.error("talk e1 : " , e1);
          responseObserver.onError(e);
        }
      }

      Entities entities = Entities.newBuilder()
          .setModel(modelName)
          .setSessionKey(session).build();

      // 채워진 슬롯으로 대화를 완성한다.
      SdsUtter sdsUtter = null;
      try {
        sdsUtter = sdsStub.generate(entities);
        logger.debug("<SdsUtter> {}", sdsUtter);
      } catch (Exception e) {
        ai.maum.rpc.ResultStatus rs = ai.maum.rpc.ResultStatusListProto.fromThrowable(e);
        ai.maum.rpc.ResultStatus resultStatus = new ai.maum.rpc.ResultStatus(rs.getResultStatusList());

        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(maum.rpc.Status.ExCode.RUNTIME_ERROR,
              40504,
              e.getMessage());

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("talk.generate")
              .setLine(653)
              .build();

          status.addDetail(infoFile);
          status.addStackStatus(resultStatus);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e1) {
          logger.error("talk e1 : " , e1);
          responseObserver.onError(e);
        }
      }

      /* internal 모델을 사용할 경우 Understand, Generate 대신
      Dialog 한번을 호출해준다.
      SdsResponse sdsResponse = sdsStub.Dialog(sdsQuery);
      logger.debug("<SdsResponse> {}", sdsResponse);*/

      response.setResponse(SpeechResponse.newBuilder()
          .setSpeech(Speech.newBuilder()
              .setLang(Lang.ko_KR)
              .setUtter(sdsUtter.getResponse()).build())
          // meta 정보로 intent, slot 정보 등을 함께 보낼 수 있다.
          // 단, external 모델을 사용하여 Understand/GenerateEntities 호출했을 때만
          // intent, slot 정보를 얻을 수 있다.
          // internal 모델을 사용하여 Dialog 호출하면 나오지 않음
          //.setMeta(Struct.newBuilder()
          //    .putFields("intent", Value.newBuilder()
          //        .setStringValue(intent.getIntent()).build()).build())
          // single-turn이므로 true
          .setCloseSkill(true)
          .setSessionUpdate(Session.newBuilder()
              .setContext(Struct.newBuilder().putFields("context1",
                  Value.newBuilder().setNumberValue(++count).build()))))
          .build();
      logger.trace("Talk response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted(); // 보낼 정보들이 끝날 때 한 번만 해주면 된다.
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40008, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("talk")
            .setLine(469)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("talk e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다. 다음과 같은 동작이 예가 될 수 있다. 로봇이 특정한 위치로 이동하라고 명명하고 수행이
   * 완료되었을 경우에 내보낸다.
   **/
  @Override
  public void event(Talk.EventRequest request,
      StreamObserver<TalkResponse> responseObserver) {

    logger.info("DA V3 Server Event");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Event request: {}", request);

    TalkResponse.Builder response = TalkResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 40104, "session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(749)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("event e : " , e);
          responseObserver.onError(e);
        }
      }
      response.setResponse(
          SpeechResponse.newBuilder().setSpeech( // event에 대한 응답 메세지
              Speech.newBuilder().setUtter("This is event response")
                  .build())
              .setMeta(Struct.newBuilder().putFields("token", // 추가적인 정보 전달
                  Value.newBuilder().setStringValue(
                      request.getEvent().getPayload().getLauncherAuthorized().getAccessToken())
                      .build()).build())
              .setSessionUpdate( // session 업데이트
                  Session.newBuilder().setId(
                      request.getSession().getId())
                      .build()).build()).build();

      logger.trace("Event response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("event")
            .setLine(734)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("event e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 SKILL이 종료되었음을 알려준다. SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로 매우 주의해야 한다.
   **/
  @Override
  public void closeSkill(Talk.CloseSkillRequest request,
      StreamObserver<CloseSkillResponse> responseObserver) {
    logger.info("DA V3 Server CloseSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("CloseSkill request: {}", request);

    CloseSkillResponse.Builder response = CloseSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 40105,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(826)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("closeSkill e : " , e);
          responseObserver.onError(e);
        }
      }
      response.setSessionUpdate(
          Session.newBuilder()
              .setId(request.getSession().getId()) // session update
              .setContext(request.getSession().getContext()) // session context update
              .build()).build();

      logger.trace("CloseSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 40009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("closeSkill")
            .setLine(812)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("closeSkill e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }
}
