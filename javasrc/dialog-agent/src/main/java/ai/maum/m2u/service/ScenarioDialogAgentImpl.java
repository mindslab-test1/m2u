package ai.maum.m2u.service;

import ai.maum.m2u.utils.ServerMetaInterceptor;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.Empty;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.ListValue;
import com.google.protobuf.NullValue;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import com.google.protobuf.util.JsonFormat;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import maum.brain.sds.Scenario.DesignerScenario;
import maum.brain.sds.Scenario.Edge;
import maum.brain.sds.Scenario.Node;
import maum.brain.sds.Scenario.SystemUtter;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.CardOuterClass.Card;
import maum.m2u.common.CardOuterClass.ChartCard;
import maum.m2u.common.CardOuterClass.CustomCard;
import maum.m2u.common.CardOuterClass.CustomListCard;
import maum.m2u.common.CardOuterClass.LinkCard;
import maum.m2u.common.CardOuterClass.ListCard;
import maum.m2u.common.CardOuterClass.SelectCard;
import maum.m2u.common.CardOuterClass.SelectCard.Item;
import maum.m2u.common.Dialog.Session;
import maum.m2u.common.Dialog.Speech;
import maum.m2u.common.DirectiveOuterClass.AvatarSetExpressionPayload;
import maum.m2u.common.DirectiveOuterClass.DelegateDirective;
import maum.m2u.common.DirectiveOuterClass.DelegateDirective.DelegateDirectivePayload;
import maum.m2u.common.DirectiveOuterClass.Directive;
import maum.m2u.common.DirectiveOuterClass.Directive.DirectivePayload;
import maum.m2u.common.DirectiveOuterClass.LauncherFillSlotsPayload;
import maum.m2u.common.EventOuterClass.DialogAgentForwarderParam;
import maum.m2u.common.Types.DataType;
import maum.m2u.da.Provider;
import maum.m2u.da.Provider.AgentKind;
import maum.m2u.da.Provider.DialogAgentProviderParam;
import maum.m2u.da.Provider.DialogAgentState;
import maum.m2u.da.Provider.DialogAgentStatus;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.da.v3.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import maum.m2u.da.v3.Talk;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import maum.m2u.da.v3.Talk.DialogDelegate;
import maum.m2u.da.v3.Talk.OpenSkillResponse;
import maum.m2u.da.v3.Talk.SkillTransition;
import maum.m2u.da.v3.Talk.SkillTransition.TransitionType;
import maum.m2u.da.v3.Talk.SpeechResponse;
import maum.m2u.da.v3.Talk.TalkResponse;
import maum.rpc.ErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// import maum.m2u.common.Userattr.DataType;


public class ScenarioDialogAgentImpl extends DialogAgentProviderImplBase {

  static final private Logger logger = LoggerFactory.getLogger(ScenarioDialogAgentImpl.class);

  private DialogAgentProviderParam.Builder provider = DialogAgentProviderParam.newBuilder();
  private Provider.InitParameter initParam;
  private DialogAgentState state;
  private String skill;
  private int count = 0;


  public ScenarioDialogAgentImpl() {
    logger.debug("DA V3 Server Constructor");
    state = DialogAgentState.DIAG_STATE_IDLE;
    logger.debug("DialogAgentState: {}", state);
  }

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때, 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는
   * 함수 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장하는 과정 수행 Init() 함수에서
   * DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중) Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다.
   * (동작중)
   */
  @Override
  public void init(Provider.InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server Init");
    logger.trace("Init request: {}", request);

    try {
      state = DialogAgentState.DIAG_STATE_INITIALIZING; // DA instance state 초기화

      provider.setName("name") // provider 정보 setting
          .setDescription("control intention return DA")
          .setVersion("3.0")
          .setSingleTurn(true)
          .setAgentKind(AgentKind.AGENT_SDS)
          .setRequireUserPrivacy(true);

      initParam = request;
      state = DialogAgentState.DIAG_STATE_RUNNING;

      //throw new Exception("exception test");

      logger.trace("Init response {}", provider);
      responseObserver.onNext(provider.build()); // provider를 response로 return
      responseObserver.onCompleted();

    } catch (Exception e) { // 예외 처리
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10001, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("init")
            .setLine(108)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("init e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수 Dialog Agent Instance 동작에 따라 상태 변경 필요 생성자에서
   * DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   */
  @Override
  public void isReady(Empty request, StreamObserver<DialogAgentStatus> responseObserver) {
    logger.info("DA V3 Server IsReady");

    try {
      DialogAgentStatus.Builder response = DialogAgentStatus.newBuilder();

      response.setState(state); // DA instance 상태 저장

      logger.debug("IsReady response: {}", response);
      responseObserver.onNext(response.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10002, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("isReady")
            .setLine(148)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("isReady e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 동작중인 DA Instance을 종료시키는 함수 Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경해줘야 합니다.
   */
  @Override
  public void terminate(Empty request, StreamObserver<Empty> responseObserver) {
    logger.info("DA V3 Server Terminate");

    try {
      state = DialogAgentState.DIAG_STATE_TERMINATED; // DA instance state 변경
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10003, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("terminate")
            .setLine(183)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("terminate e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 실제로 DA Instnace가 돌아가기 위해 필요한 paramerters를 정의 db 정보 정의
   */
  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    logger.info("DA V3 Server GetRuntimeParameters");

    try {
      RuntimeParameterList.Builder result = RuntimeParameterList.newBuilder();

      Provider.RuntimeParameter.Builder db_host = Provider.RuntimeParameter.newBuilder(); // db ip
      db_host.setName("db_host")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Host")
          .setDefaultValue("171.64.122.134")
          .setRequired(true);
      result.addParams(db_host);

      Provider.RuntimeParameter.Builder db_port = Provider.RuntimeParameter.newBuilder(); // db port
      db_port.setName("db_port")
          .setType(DataType.DATA_TYPE_INT)
          .setDesc("Database Port")
          .setDefaultValue("7701")
          .setRequired(true);
      result.addParams(db_port);

      Provider.RuntimeParameter.Builder db_user = Provider.RuntimeParameter.newBuilder(); // db user
      db_user.setName("db_user")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database User")
          .setDefaultValue("minds")
          .setRequired(true);
      result.addParams(db_user);

      Provider.RuntimeParameter.Builder db_pwd = Provider.RuntimeParameter.newBuilder(); // db pwd
      db_pwd.setName("db_pwd")
          .setType(DataType.DATA_TYPE_AUTH)
          .setDesc("Database Password")
          .setDefaultValue("minds67~")
          .setRequired(true);
      result.addParams(db_pwd);

      Provider.RuntimeParameter.Builder db_database = Provider.RuntimeParameter
          .newBuilder(); // db name
      db_database.setName("db_database")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Database name")
          .setDefaultValue("ascar")
          .setRequired(true);
      result.addParams(db_database);

      logger.trace("GetRuntimeParameters response: {}", result);
      responseObserver.onNext(result.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10004, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getRuntimeParameter")
            .setLine(262)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getRuntimeParameters e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA가 동작하는 데 필요한 여러가지 옵션 값을 처리하는 DA Provider Parameter를 반환
   */
  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server GetProviderParameter");

    try {
      // init, isReady 등 여러 함수에서 정의된 provider 값 return
      logger.trace("GetProviderParameter response {}", provider);
      responseObserver.onNext(provider.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10005, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getProviderParameter")
            .setLine(299)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("getProviderParameter e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다. 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
   */
  @Override
  public void openSession(Talk.OpenSessionRequest request,
      StreamObserver<TalkResponse> responseObserver) {
    logger.info("DA V3 Server OpenSession");

    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSession request: {}", request);

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10101,
              "chatbot or skill or session.id is null");

          ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSession")
              .setLine(339)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSession e : " , e);
          responseObserver.onError(e);
        }
      }

      TalkResponse.Builder response = TalkResponse.newBuilder();
      response.setResponse(
          SpeechResponse.newBuilder()
              .setSpeech(
                  Speech.newBuilder().setLang(request.getUtter().getLang())
                      .setUtter("hi!!").build()) // open message
              .setSessionUpdate(
                  Session.newBuilder()
                      .setId(request.getSession().getId()).build())
              .setMeta( // meta 정보 저장
                  Struct.newBuilder().putFields("context_test_1", // key
                      // value, setIntValue, setStringValue 등으로 type 설정
                      Value.newBuilder().setStringValue("ctx_1")
                          .build()).build()).build());
      logger.trace("OpenSession response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();

    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10006, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSession")
            .setLine(378)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSession e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 해당하는 스킬을 최초로 호출 할 때 호출된다. 싱글턴 일 경우에는 호출되지 않는다. 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
   */
  @Override
  public void openSkill(Talk.OpenSkillRequest request,
      StreamObserver<OpenSkillResponse> responseObserver) {
    logger.info("DA V3 Server OpenSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSkill request: {}", request);

    OpenSkillResponse.Builder response = OpenSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10102,
              "chatbot or skill or session.id is null");

          ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSkill")
              .setLine(419)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("openSkill e : " , e);
          responseObserver.onError(e);
        }
      }
      skill = request.getSkill();
      response.setMeta( // meta 정보 저장
          Struct.newBuilder().putFields("meta_info",
              Value.newBuilder().setStringValue("This is openSkill")
                  .build()).build())
          .setSessionUpdate( // request의 session으로 업데이트
              Session.newBuilder().setId(
                  request.getSession().getId())
                  .build()).build();

      logger.trace("OpenSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10007, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSkill")
            .setLine(453)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("openSkill e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 대화를 주고 응답을 생성하도록 한다. <SpeechResponse> 일반 발화시 echo "chart" 발화시 echo + chartCart return "select"
   * 발화시 echo + selectCart return "link" 발화시 echo + linkCart return "custom" 발화시 echo + linkListCart
   * return "customlist" 발화시 echo + customListCart return <DialogDelegate> "delegate slotname" 발화시
   * LauncherFillSlotsPayload 타입의 dialogDelegate return
   **/
  @Override
  public void talk(Talk.TalkRequest request,
      StreamObserver<TalkResponse> responseObserver) {
    logger.info("DA V3 Server Talk");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Talk request: {}", request);
    logger.trace("Talk Utter Meta info(Replacement) : {}", request.getUtter().getMeta());

    // 응답 메세지 분류를 위한 flag
    // 0이면 response, 1이면 transit, 2이면 delegate
    int flag = 0;

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10103,
              "chatbot or skill or session.id is null");

          ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("Talk")
              .setLine(500)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("talk e : " , e);
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("error")) {
        try {
          status = new ai.maum.rpc.ResultStatus(maum.rpc.Status.ExCode.LOGICAL_ERROR,
              10106,
              "Error for Result Status.");

          ErrorDetails.DebugInfoFile infoFile =
              ErrorDetails.DebugInfoFile.newBuilder()
                  .setFunction("Talk")
                  .setLine(523)
                  .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("talk e : " , e);
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("deadline")) {
        try {
          Thread.sleep(35000);
          responseObserver.onCompleted();
        } catch (Exception e) {
          logger.error("talk e : " , e);
          responseObserver.onError(e);
        }
        return;
      }

      TalkResponse.Builder response = TalkResponse.newBuilder();

      Speech.Builder speech = Speech.newBuilder();
      Card.Builder card = Card.newBuilder();
      Directive.Builder directive = Directive.newBuilder();
      TransitionType transitionType = TransitionType.SKILL_TRANS_UNKNOWN;
      Struct.Builder context = Struct.newBuilder();

      String transit_skill = "";
      String transit_intent = "";
      String transit_chatbot = "";
      String replaced_utter = "";

      String scenarioJson = "{\n"
          + "  \"nodes\": [\n"
          + "    {\n"
          + "      \"id\": \"272f54b5-0d32-47ce-8436-8fd66534a093\",\n"
          + "      \"type\": \"start\",\n"
          + "      \"text\": \"시작\",\n"
          + "      \"left\": -1157,\n"
          + "      \"top\": -352,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"안녕하십니까. 365일 축제의 나라 에버랜드 리조트입니다.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"ARS안내는 1번, 공연시간 및 ATT 대기시간 확인은 2번, For English press three. 상담원 연결은 0번을 눌러주십시오. 번을 눌러주십시오. 다시 듣고 싶으시면 (*)를 눌러 주십시오.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"1,2,3,0,*\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"안내시 OOO 확인 필요\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"3ee43e0e-6284-4ec1-a1b9-f308abd251ca\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"ARS안내\",\n"
          + "      \"left\": -855,\n"
          + "      \"top\": -260,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"에버랜드 리조트 일반안내입니다. \"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"에버랜드는 1번, 캐리비안 베이는 2번, 연간이용권은 3번, 상담원 연결은 0번, \"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"다시 들으시려면(*), 전 단계로 이동하시려면 (#)을 눌러주십시오. \"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"1,2,3,0,*,#\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"2168c37c-17d5-405d-8cf8-f66237afa8c1\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"공연시간 ATT 대기\",\n"
          + "      \"left\": -853,\n"
          + "      \"top\": -74,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"\",\n"
          + "          \"text\": \"\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"674e6e94-2143-4e01-929f-72094a09e10b\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"영어안내\",\n"
          + "      \"left\": -833,\n"
          + "      \"top\": 65,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"\",\n"
          + "          \"text\": \"\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"408075c4-fb6f-4ca7-9be3-b4890c9e08a1\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"상담원연결\",\n"
          + "      \"left\": -863,\n"
          + "      \"top\": -362,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"\",\n"
          + "          \"text\": \"\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"b86c9e6b-124f-44ee-a935-bc743d2c2554\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"default\",\n"
          + "      \"left\": -1121,\n"
          + "      \"top\": 73,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"잘못 누르셨습니다.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"ARS안내는 1번, 공연시간 및 ATT 대기시간 확인은 2번, For English press three. 상담원 연결은 0번을 눌러주 십시오. 다시 듣고 싶으시면 (*)를 눌러 주십시오.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"1,2,3,0\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"eb145abd-00e7-4687-96df-c342d73331f8\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"에버랜드\",\n"
          + "      \"left\": -481,\n"
          + "      \"top\": -321,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"에버랜드 안내입니다.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"영업시간은 1번, 교통 및 주차안내는 2번, 반입금지물품은 3번, 상담원 연결은 0번,\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"다시 들으시려면(*), 전 단계로 이동하시려면 (#)을 눌러주십시오. \"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"1,2,3,0,*,#\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"df44054b-e257-4877-9d48-7b69b8c1b9ff\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"캐리비안 베이\",\n"
          + "      \"left\": -479,\n"
          + "      \"top\": -179,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"캐리비안 베이 안내입니다.\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"b358cfcb-bb24-4b35-83b3-fae32ab8bed4\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"연간이용권\",\n"
          + "      \"left\": -475,\n"
          + "      \"top\": -42,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"연간이용권 안내입니다.\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"595333e7-62a1-425a-bfc9-6d2898afe46f\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"영업시간\",\n"
          + "      \"left\": -105,\n"
          + "      \"top\": -449,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"에버랜드 영업시간 안내입니다.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"0,*,#\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"에버랜드는 금일 10시부터 20 시까지 운영합니다. \\\\n폐장시간은 기상여건에 따라 변동될 수 있습니다.\\\\n 홈페이지 및 모바일 어플리케이션을 참고하시면 운영시간에 대한 보다 다양한 정보를 확인하실 수 있습니다.\\\\n\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"다시 들으시려면(*), 전 단계로 이동하시려면 (#), 상담원 연결은 (0)번을 눌러주십시오. \"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"76f10a8d-612b-478f-ac23-3ab2a009c9e1\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"교통 및 주차\",\n"
          + "      \"left\": -98,\n"
          + "      \"top\": -298,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"교통 및 주차 안내입니다.\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"99283400-13c1-4483-a9db-d641fb006724\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"반입금지물품\",\n"
          + "      \"left\": -101,\n"
          + "      \"top\": -151,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"반입 금지 물품 안내입니다.\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    }\n"
          + "  ],\n"
          + "  \"edges\": [\n"
          + "    {\n"
          + "      \"source\": \"272f54b5-0d32-47ce-8436-8fd66534a093\",\n"
          + "      \"target\": \"408075c4-fb6f-4ca7-9be3-b4890c9e08a1\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"13337199-7372-4871-8697-3855acdc7c14\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"0\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"272f54b5-0d32-47ce-8436-8fd66534a093\",\n"
          + "      \"target\": \"2168c37c-17d5-405d-8cf8-f66237afa8c1\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"cf3ea76e-076c-400e-bc37-fb7d2a5c0626\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"2\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"272f54b5-0d32-47ce-8436-8fd66534a093\",\n"
          + "      \"target\": \"674e6e94-2143-4e01-929f-72094a09e10b\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"03aca6c5-e102-4a3d-9c7c-f63cf4894c32\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"3\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"272f54b5-0d32-47ce-8436-8fd66534a093\",\n"
          + "      \"target\": \"b86c9e6b-124f-44ee-a935-bc743d2c2554\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"55f182d1-e56f-48fd-98c1-76ef6dcaa72a\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"default\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"272f54b5-0d32-47ce-8436-8fd66534a093\",\n"
          + "      \"target\": \"3ee43e0e-6284-4ec1-a1b9-f308abd251ca\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"8e7d65af-d177-436e-8495-ffb472f92f79\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"1\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"3ee43e0e-6284-4ec1-a1b9-f308abd251ca\",\n"
          + "      \"target\": \"eb145abd-00e7-4687-96df-c342d73331f8\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"500925cf-f40f-4500-97d4-fd2dca247110\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"1\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"3ee43e0e-6284-4ec1-a1b9-f308abd251ca\",\n"
          + "      \"target\": \"df44054b-e257-4877-9d48-7b69b8c1b9ff\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"8ca95bf5-17ff-47e3-909f-114050c5d8d0\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"2\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"3ee43e0e-6284-4ec1-a1b9-f308abd251ca\",\n"
          + "      \"target\": \"b358cfcb-bb24-4b35-83b3-fae32ab8bed4\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"289d0f32-818d-473d-8137-24c3dbc20cf1\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"3\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"eb145abd-00e7-4687-96df-c342d73331f8\",\n"
          + "      \"target\": \"595333e7-62a1-425a-bfc9-6d2898afe46f\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"dec2860a-f951-4c58-8851-e0bb94d228f7\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"1\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"eb145abd-00e7-4687-96df-c342d73331f8\",\n"
          + "      \"target\": \"76f10a8d-612b-478f-ac23-3ab2a009c9e1\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"59294b83-771b-4063-9065-b51224caad5b\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"2\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"eb145abd-00e7-4687-96df-c342d73331f8\",\n"
          + "      \"target\": \"99283400-13c1-4483-a9db-d641fb006724\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"ff811adb-cde8-4e06-9f51-feb35cce05a5\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"3\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    }\n"
          + "  ],\n"
          + "  \"ports\": [],\n"
          + "  \"groups\": []\n"
          + "}";

      String scenarioJsonHana = "{\n"
          + "  \"nodes\": [\n"
          + "    {\n"
          + "      \"id\": \"272f54b5-0d32-47ce-8436-8fd66534a093\",\n"
          + "      \"type\": \"start\",\n"
          + "      \"text\": \"시작\",\n"
          + "      \"left\": -343,\n"
          + "      \"top\": -397,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"개인이세요. 법인이세요?\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"전산\",\n"
          + "          \"text\": \"[화면] 000 -> [항목] 고객자격\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"개인, 개인사업자, 법인, 임의단체\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"안내시 OOO 확인 필요\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"472a9eda-625a-4817-b363-86b51d9af4f9\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"프로세스설정\",\n"
          + "      \"left\": -375,\n"
          + "      \"top\": -239,\n"
          + "      \"w\": 182.24782055090364,\n"
          + "      \"h\": 68.48959010204902,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"업무안내\",\n"
          + "          \"text\": \"금융거래 한도계좌 업무 절차\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"계속 진행\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"b2fdef14-c6b3-4cb4-8340-dbe9ce617545\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"업무구분\",\n"
          + "      \"left\": -386,\n"
          + "      \"top\": -84,\n"
          + "      \"w\": 193.94265826614733,\n"
          + "      \"h\": 58.12776618832694,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"업무안내\",\n"
          + "          \"text\": \"문의하신 내용을 선택해주세요\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"거래범위 및 한도\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"등록계좌 확인방법\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"해제 방법\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"response - NORA000002(0301 거래범위 및 한도)\\n0101 창구(인출/이체)거래(콜센터 상담원 통한 이체 포함) : 1일기준 100만원 (인출+이체 합산한도) \\nATM(인출/이체)거래 : 1일기준 30만원 (인출 및 이체한도 각각 관리) \\n전자금융거래(이체) : 1일기준 30만원\\n※ 금융거래 한도계좌로 등록된 계좌에서 비대면채널을 통해 계좌 신규시에도 해당 한도범위내에서만 신규 가능합니다. \\n0803 [한도제한 거래의 예외] \\n- 대출금 자동이체 거래\\n- 공과금, 아파트, 체크카드 등 각종자동이체 거래(대량이체 포함) \\n- 지점장이 인정하는 경우\\n\\nresponse - NORA000005(0301 해제 방법)\\n0101 금융거래 한도계좌 해제방법은 해당 계좌 관리점으로 방문하여 '금융거래 목적에 따른 추가 증빙서류' 제출 후 해제 가능합니다. 해제 시 필요한 서류 안내해드리겠습니다.\\n0803 NET점 방문이 필요한 경우 해제 가능여부는 관리점과 상의할 수 있도록 안내하시기 바랍니다.\\n1403 475{수신 금융거래한도계좌}\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"e204a056-bafe-45e1-9bb4-aa8ba5320b75\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"본인확인 가능여부\",\n"
          + "      \"left\": -388,\n"
          + "      \"top\": 66,\n"
          + "      \"w\": 204.5241474739294,\n"
          + "      \"h\": 59.45045233929969,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"본인확인 진행해 주세요\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"본인확인 완료\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"버튼\",\n"
          + "          \"text\": \"본인확인 불가\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"response - NORA000003(0301 본인확인 완료)\\n0901 [전산조작] [화면] 0009 -> [거래종류] 0001 -> [항목] 주의사고및 기타등록정보\\n0803 등록내역 확인 후 안내\\n\\nresponse - NORA000004(0301 본인확인 불가)\\n0801 손님, 본인확인이 어려운 경우 상담원을 통한 확인이 어렵습니다. 해당 계좌의 통장에 관련 내용을 인자해드리오니 참고하시기 바랍니다\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"073a6693-df8b-4ba2-a7d3-ced34d576634\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"해제 방법\",\n"
          + "      \"left\": -42,\n"
          + "      \"top\": 56,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"금융거래 한도계좌 해제방법은 해당 계좌 관리점으로 방문하여 '금융거래 목적에 따른 추가 증빙서류' 제출 후 해제 가능합니다. 해제 시 필요한 서류 안내해드리겠습니다.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"최종 업무\",\n"
          + "          \"text\": \"NET점 방문이 필요한 경우 해제 가능여부는 관리점과 상의할 수 있도록 안내하시기 바랍니다.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"Go to SKILL \",\n"
          + "          \"text\": \"475{수신 금융거래한도계좌}\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"82ad2c0c-1d5d-49bd-882a-d18a036aaca8\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"거래범위 및 한도\",\n"
          + "      \"left\": -608,\n"
          + "      \"top\": 28,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"멘트\",\n"
          + "          \"text\": \"창구(인출/이체)거래(콜센터 상담원 통한 이체 포함) : 1일기준 100만원 (인출+이체 합산한도)  ATM(인출/이체)거래 : 1일기준 30만원 (인출 및 이체한도 각각 관리)  전자금융거래(이체) : 1일기준 30만원 ※ 금융거래 한도계좌로 등록된 계좌에서 비대면채널을 통해 계좌 신규시에도 해당 한도범위내에서만 신규 가능합니다.\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"최종 업무\",\n"
          + "          \"text\": \"[한도제한 거래의 예외]  - 대출금 자동이체 거래 - 공과금, 아파트, 체크카드 등 각종자동이체 거래(대량이체 포함)  - 지점장이 인정하는 경우\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"a5322439-7506-4683-a032-fe8fcf97e595\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"본인확인 완료\",\n"
          + "      \"left\": -512,\n"
          + "      \"top\": 224,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"전산안내\",\n"
          + "          \"text\": \"[전산조작] [화면] 0009 -> [거래종류] 0001 -> [항목] 주의사고및 기타등록정보\"\n"
          + "        },\n"
          + "        {\n"
          + "          \"type\": \"최종 업무\",\n"
          + "          \"text\": \"등록내역 확인 후 안내\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    },\n"
          + "    {\n"
          + "      \"id\": \"ef6374c1-102e-4b46-b220-20ef77c0597b\",\n"
          + "      \"type\": \"task\",\n"
          + "      \"text\": \"본인확인 불가\",\n"
          + "      \"left\": -237,\n"
          + "      \"top\": 234,\n"
          + "      \"w\": 100,\n"
          + "      \"h\": 70,\n"
          + "      \"attr\": [\n"
          + "        {\n"
          + "          \"type\": \"최종 답변\",\n"
          + "          \"text\": \"손님, 본인확인이 어려운 경우 상담원을 통한 확인이 어렵습니다. 해당 계좌의 통장에 관련 내용을 인자해드리오니 참고하시기 바랍니다\"\n"
          + "        }\n"
          + "      ],\n"
          + "      \"desc\": \"\"\n"
          + "    }\n"
          + "  ],\n"
          + "  \"edges\": [\n"
          + "    {\n"
          + "      \"source\": \"272f54b5-0d32-47ce-8436-8fd66534a093\",\n"
          + "      \"target\": \"472a9eda-625a-4817-b363-86b51d9af4f9\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"98f1a348-1436-465c-9875-78af75d43dd9\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"default\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"472a9eda-625a-4817-b363-86b51d9af4f9\",\n"
          + "      \"target\": \"b2fdef14-c6b3-4cb4-8340-dbe9ce617545\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"a140c8d8-3eeb-49d0-b726-979b3b06ffec\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"계속 진행\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"b2fdef14-c6b3-4cb4-8340-dbe9ce617545\",\n"
          + "      \"target\": \"e204a056-bafe-45e1-9bb4-aa8ba5320b75\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"6e1cb729-8117-4de2-ba0e-330fe81655ed\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"등록계좌 확인방법\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"b2fdef14-c6b3-4cb4-8340-dbe9ce617545\",\n"
          + "      \"target\": \"073a6693-df8b-4ba2-a7d3-ced34d576634\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"0c42209e-94b2-43fd-8447-0c9ace36a93b\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"해제 방법\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"b2fdef14-c6b3-4cb4-8340-dbe9ce617545\",\n"
          + "      \"target\": \"82ad2c0c-1d5d-49bd-882a-d18a036aaca8\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"d2048ffb-f2c3-4f13-8b5d-7e598371bb5c\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"거래범위 및 한도\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"e204a056-bafe-45e1-9bb4-aa8ba5320b75\",\n"
          + "      \"target\": \"a5322439-7506-4683-a032-fe8fcf97e595\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"3e9c368d-cf00-44ef-91db-80d8d7218201\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"본인확인 완료\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    },\n"
          + "    {\n"
          + "      \"source\": \"e204a056-bafe-45e1-9bb4-aa8ba5320b75\",\n"
          + "      \"target\": \"ef6374c1-102e-4b46-b220-20ef77c0597b\",\n"
          + "      \"data\": {\n"
          + "        \"id\": \"4301778d-dbe0-45f3-a313-e37c5d4690d7\",\n"
          + "        \"type\": \"connection\",\n"
          + "        \"label\": \"본인확인 불가\",\n"
          + "        \"attr\": [\n"
          + "          {\n"
          + "            \"text\": \"\"\n"
          + "          }\n"
          + "        ],\n"
          + "        \"desc\": \"\"\n"
          + "      }\n"
          + "    }\n"
          + "  ],\n"
          + "  \"ports\": [],\n"
          + "  \"groups\": []\n"
          + "}";

      Value nodeObj;
      Map<String, Value> contextMap = request.getSession().getContext().getFieldsMap();
      List<Value> nodeList = null;
      if ((nodeObj = contextMap.get("node_list_in_conversation")) != null) {
        nodeList = nodeObj.getListValue().getValuesList();
        // node list는 unmodifialbleList 이므로 clone을 사용
        nodeList = nodeList.stream().collect(Collectors.toList());
      } else {
        logger.debug("node_list_in_conversation is null");
        nodeList = new ArrayList<>();
      }

      // 시스템발화 출력할 노드 가져오기
      Node destNode = getNextNode(scenarioJsonHana, request.getUtter().getUtter(), nodeList);

      if (destNode == null) {
        // todo
        logger.debug("Destination node is null");
        speech.setLang(Lang.ko_KR)
            .setUtter("대화가 종료되었습니다.")
            .setSpeechUtter("TTS 출력: " + request.getUtter().getUtter())
            .setReprompt(request.getUtter().getUtter().equals("expected response"));

        SpeechResponse.Builder speechResponse = SpeechResponse.newBuilder();
        speechResponse
            .setSpeech(speech)
            .addDirectives(directive)
            .setCloseSkill(false)
            .setSessionUpdate(Session.newBuilder().setContext(context));
        response.setResponse(speechResponse).build();

      } else {
        if (flag == 0) {

          // SpeechResponse

          StringBuilder systemUtter = new StringBuilder();
          for (SystemUtter attr : destNode.getAttrList()) {
            if (!attr.getType().equals("버튼")) {
              systemUtter.append("[" + attr.getType() + "] \n" + attr.getText() + "\n");
            }
          }

          // 정상적인 대화의 응답
          speech.setLang(Lang.ko_KR)
              .setUtter(systemUtter.toString())
              .setSpeechUtter("TTS 출력: " + request.getUtter().getUtter())
              .setReprompt(request.getUtter().getUtter().equals("expected response"));

          SpeechResponse.Builder speechResponse = SpeechResponse.newBuilder();

          SelectCard.Builder selectCart = setNodeSelectCard(destNode);
          if (selectCart.getItemsCount() > 0) {
            card.setSelect(selectCart);
            speechResponse.addCards(card);
          }

          context.putFields("node_list_in_conversation",
              Value.newBuilder().setListValue(ListValue.newBuilder().addAllValues(nodeList).build())
                  .build());

          directive.setInterface("Avatar") // directive 저장
              .setOperation("SetExpression")
              .setParam(DialogAgentForwarderParam.newBuilder()
                  .setChatbot("echoChatbot")
                  .setSkill("echo")
                  .setIntent("intent")
                  .setSessionId(1234).build())
              .setPayload(DirectivePayload.newBuilder()
                  .setAvatarSetExpression(AvatarSetExpressionPayload.newBuilder()
                      .setExpression("smile")
                      .setDesciption("avata is happy")
                      .build()).build()).build();

          context.putFields("context1", Value.newBuilder().setNumberValue(++count).build());
          context.putFields("chair", Value.newBuilder().setNullValue(NullValue.NULL_VALUE).build());
          context.putFields("desk", Value.newBuilder().build());

          speechResponse
              .setSpeech(speech)
              .addDirectives(directive)
              .setCloseSkill(false)
              .setSessionUpdate(Session.newBuilder().setContext(context));
          response.setResponse(speechResponse).build();

        } else if (flag == 1) { // SkillTransition
          // SKILL에 대한 요청을 처리할 수 없어서 다른 SKILL 또는 CHATBOT으로 전이하는 경우
          response.setTransit(SkillTransition.newBuilder()
              .setTransitionType(transitionType)
              .setSkill(transit_skill)
              .setIntent(transit_intent)
              .setChatbot(transit_chatbot)
              .setReplacedUtter(replaced_utter)
              .setMeta(Struct.newBuilder().putFields("meta1",
                  Value.newBuilder().setStringValue("meta value 1").build())
                  .putFields("meta2", Value.newBuilder().setBoolValue(false)
                      .build()).build())
              .setSessionUpdate(Session.newBuilder()
                  .setId(request.getSession().getId())
                  .build()).build()).build();
        } else if (flag == 2) { // DialogDelegate
          // DEVICE의 추가적인 응답을 요청하고 이를 재처리하는 경우
          response.setDelegate(DialogDelegate.newBuilder()
              .setDirective(DelegateDirective.newBuilder()
                  .setInterface("Launcher")
                  .setOperation("FillSlots")
                  .setParam(DialogAgentForwarderParam.newBuilder()
                      .setChatbot("echoChatbot")
                      .setSkill("echo")
                      .setIntent("intent")
                      .setSessionId(request.getSession().getId())
                      .build())
                  .setPayload(DelegateDirectivePayload.newBuilder()
                      .setLauncherFillSlots(LauncherFillSlotsPayload.newBuilder()
                          .addRequestingSlots("requesting_slots")
                          .build()).build()).build()).build()).build();
        }
      }

      logger.trace("Talk response: {}", response);
      // 계속해서 정보를 전달할 때 on next를 순차적으로 추가해준다. 처음 보내는 정보는 echo
      responseObserver.onNext(response.build());
      //responseObserver.onNext(param.build()); // 카드의 종류에 따라 순차적으로 보낸다.
      responseObserver.onCompleted(); // 보낼 정보들이 끝날 때 한 번만 해주면 된다.
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10008, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("talk")
            .setLine(714)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("talk e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  Node getNextNode(String scenarioJson, String userUtter, List<Value> nodeList) {

    String currNodeId = "";
    String destNodeId = "";
    Node destNode = null;
    DesignerScenario.Builder builder = DesignerScenario.newBuilder();

    // json str to DesignerScenario struct
    try {
      JsonFormat.parser().ignoringUnknownFields().merge(scenarioJson, builder);
    } catch (InvalidProtocolBufferException e) {
      logger.error("Wrong scenario json format : {} => ", e.getMessage(), e);
    }

    currNodeId = nodeList.size() > 0 ? nodeList.get(nodeList.size() - 1).getStringValue() : "";

    logger.debug("current node id : ", currNodeId);
    logger.debug("utter : ", userUtter);

    // 세션에 저장된 node id가 없을 시,
    if (currNodeId.isEmpty()) {
      // start node를 배정
      for (Node node : builder.getNodesList()) {
        if (node.getType().equals("start")) {
          destNode = node;
          break;
        }
      }

    } else {

      // userUtter이 *이면 다시 듣기 #은 전단계
      switch (userUtter) {

        // 이전 노드 돌아가기
        case "#": {
          if (nodeList.size() > 1) {
            // current Node 삭제. 이전 노드가 current Node가 된다.
            nodeList.remove(nodeList.size() - 1);
            currNodeId = nodeList.get(nodeList.size() - 1).getStringValue();

            for (Node node : builder.getNodesList()) {
              if (node.getId().equals(currNodeId)) {
                return node;
              }
            }
            break;

          } else {
            // 이전 노드가 없는 경우에는 현재 노드를 반복
          }
        }

        // 현재 노드 반복
        case "*": {
          for (Node node : builder.getNodesList()) {
            if (node.getId().equals(currNodeId)) {
              return node;
            }
          }
          break;
        }
      }

      // node list가 비어있지 않고, *, # 이 아닐때
      String tmpNextNodeId = "";
      for (Edge edge : builder.getEdgesList()) {
        if (edge.getSource().equals(currNodeId)) {
          // edge의 label과 user input을 비교중인데, label을 쓸지는 고민해봐야
          String label = edge.getData().getLabel();
          if (label.equals(userUtter)) {
            destNodeId = edge.getTarget();
            break;
          } else if (label.equals("default")) {
            tmpNextNodeId = edge.getTarget();
          }
        }
      }

      if (destNodeId.isEmpty()) {
        if (!tmpNextNodeId.isEmpty()) {
          destNodeId = tmpNextNodeId;
        } else {
          // todo : No where to go , 종료
          return null;
        }
      }

      for (Node node : builder.getNodesList()) {
        if (node.getId().equals(destNodeId)) {
          destNode = node;
        }
      }
    }

    if (destNode == null) {
      // todo : No where to go , 종료
      return null;
    }

    nodeList.add(Value.newBuilder().setStringValue(destNode.getId()).build());

    return destNode;
  }

  /**
   * DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다. 다음과 같은 동작이 예가 될 수 있다. 로봇이 특정한 위치로 이동하라고 명명하고 수행이
   * 완료되었을 경우에 내보낸다.
   **/
  @Override
  public void event(Talk.EventRequest request,
      StreamObserver<TalkResponse> responseObserver) {

    logger.info("DA V3 Server Event");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Event request: {}", request);

    TalkResponse.Builder response = TalkResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10104, "session.id is null");

          ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(756)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("event e : " , e);
          responseObserver.onError(e);
        }
      }

      response.setResponse(
          SpeechResponse.newBuilder().setSpeech( // event에 대한 응답 메세지
              Speech.newBuilder().setUtter("This is event response")
                  .build())
              .setMeta(Struct.newBuilder().putFields("token", // 추가적인 정보 전달
                  Value.newBuilder().setStringValue(
                      request.getEvent().getPayload().getLauncherAuthorized().getAccessToken())
                      .build()).build())
              .setSessionUpdate( // session 업데이트
                  Session.newBuilder().setId(
                      request.getSession().getId())
                      .build()).build()).build();

      logger.trace("Event response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("event")
            .setLine(794)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("event e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 SKILL이 종료되었음을 알려준다. SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로 매우 주의해야 한다.
   **/
  @Override
  public void closeSkill(Talk.CloseSkillRequest request,
      StreamObserver<CloseSkillResponse> responseObserver) {
    logger.info("DA V3 Server CloseSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("CloseSkill request: {}", request);

    CloseSkillResponse.Builder response = CloseSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10105,
              "chatbot or skill or session.id is null");

          ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(835)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          logger.error("closeSkill e : " , e);
          responseObserver.onError(e);
        }
      }

      skill = null;
      response.setSessionUpdate(
          Session.newBuilder()
              .setId(request.getSession().getId()) // session update
              .setContext(request.getSession().getContext()) // session context update
              .build()).build();

      logger.trace("CloseSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("closeSkill")
            .setLine(866)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        logger.error("closeSkill e1 : " , e1);
        responseObserver.onError(e);
      }
    }
  }

  /**
   * ChartCard 정의
   **/
  private ChartCard.Builder setChartCard() {
    logger.info("DA V3 Function SetChartCard");

    ChartCard.Builder chartCard = ChartCard.newBuilder();

    try {
      chartCard.setType("PieChart")
          .addData(ListValue.newBuilder()
              .addValues(Value.newBuilder()
                  .setStringValue("PieChart"))
              .build()).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.debug("setChartCard: {}", chartCard);
    return chartCard;
  }

  /**
   * SelectCard 정의
   **/
  private SelectCard.Builder setSelectCard() {
    logger.info("DA V3 Function setSelectCard");

    SelectCard.Builder selectCard = SelectCard.newBuilder();

    try {
      selectCard.setTitle("Transportation")
          .setHeader("messageId: 1234")
          .addItems(Item.newBuilder()
              .setTitle("bus")
              .setSummary("This is bus image card")
              .setImageUrl("data:image/png;base64,dhtdgedfswhftd")
              .setSelectedUtter("버스 선택할게요").build())
          .addItems(Item.newBuilder()
              .setTitle("subway")
              .setSummary("This is subway image card")
              .setImageUrl("data:image/png;base64,sgfdjhgfhkh")
              .setSelectedUtter("지하철 선택할래요").build())
          .addItems(Item.newBuilder()
              .setTitle("taxi")
              .setSummary("This is taxi image card")
              .setImageUrl("data:image/png;base64,sghdhfjgedaw")
              .setSelectedUtter("택시 탈게요").build())
          .addItems(Item.newBuilder()
              .setTitle("airplane")
              .setSummary("This is airplane image card")
              .setImageUrl("data:image/png;base64,hgdfawsad")
              .setSelectedUtter("비행기 선택할게요").build())
          .addItems(Item.newBuilder()
              .setTitle("ship")
              .setSummary("This is ship image card")
              .setImageUrl("data:image/png;base64,etdfaegdfss")
              .setSelectedUtter("배 선택할게요").build())
          .setHorizontal(true).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    logger.debug("setSelectCard: {}", selectCard);
    return selectCard;
  }

  private SelectCard.Builder setNodeSelectCard(Node node) {
    logger.info("DA V3 Function setJsonSelectCard");
    SelectCard.Builder selectCard = SelectCard.newBuilder();

    selectCard.setHorizontal(true);
    // todo : type을 이용중인데 바꿀 것
    selectCard.setType(node.getId());

    for (SystemUtter attr : node.getAttrList()) {
      if (attr.getType().equals("버튼")) {
        String btnStr = attr.getText();
        String[] buttons = btnStr.split(",");
        for (String btn : buttons) {
          selectCard
              .addItems(Item.newBuilder()
                  .setTitle(btn)
                  .setSummary(btn)
                  .setImageUrl("data:image/png;base64,dhtdgedfswhftd")
                  .setSelectedUtter(btn).build());
        }
      }
    }

    //logger.debug("setSelectCard: {}", selectCard);
    return selectCard;
  }

  /**
   * LinkCard 정의
   **/
  private LinkCard.Builder setLinkCard() {
    logger.info("DA V3 Function SetLinkCard");

    LinkCard.Builder linkCard = LinkCard.newBuilder();

    try {
      linkCard.setTitle("ticket_reservation")
          .setSummary("선택하면 표를 예매할 수 있는 url로 연결되는 카드")
          .setImageUrl("data:image/png;base64,diusohdkjlflaskhf")
          .setImageHref("http://ticket_reservation.com").build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.trace("setLinkCard: {}", linkCard);
    return linkCard;
  }

  /**
   * CustomCard 정의
   **/
  private CustomCard.Builder setCustomCard() {
    logger.info("DA V3 Function setCustomCard");

    CustomCard.Builder customCard = CustomCard.newBuilder();

    try {
      customCard.setType("type")
          .setCardData(Struct.newBuilder()
              .putFields("card_data", Value.newBuilder()
                  .setStringValue("This is custom card data")
                  .build()).build()).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.trace("setCustomCard: {}", customCard);
    return customCard;
  }

  /**
   * ListCard 정의
   **/
  private ListCard.Builder setListCard() {
    logger.info("DA V3 Function SetListCard");

    ListCard.Builder listCard = ListCard.newBuilder();

    try {
      listCard
          .addCards(LinkCard.newBuilder()
              .setTitle("ticket_reservation")
              .setSummary("선택하면 표를 예매할 수 있는 url로 연결되는 카드")
              .setImageUrl("data:image/png;base64,diusohdkjlflaskhf")
              .setImageHref("http://ticket_reservation.com").build())
          .addCards(LinkCard.newBuilder()
              .setTitle("ticket_reservation_cancel")
              .setSummary("예매표를 취소할 수 있는 url로 연결되는 카드")
              .setImageUrl("data:image/png;base64,fuweohoolkdqaejhsudi")
              .setImageHref("http://ticket_reservation_cancel.com").build())
          .addCards(LinkCard.newBuilder()
              .setTitle("ticket_reservation_confirm")
              .setSummary("예매한 표를 확인할 수 있는 url로 연결되는 카드")
              .setImageUrl("data:image/png;base64,kjdfnemnkas21cdwsa435")
              .setImageHref("http://ticket_reservation_confirm.com").build())
          .setHorizontal(true).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.trace("setListCard: {}", listCard);
    return listCard;
  }

  /**
   * CustomListCard 정의
   **/
  private CustomListCard.Builder setCustomListCard() {
    logger.info("DA V3 Function CustomListCard");

    CustomListCard.Builder customListCard = CustomListCard.newBuilder();

    try {
      customListCard.addCards(CustomCard.newBuilder()
          .setType("string").setCardData(Struct.newBuilder()
              .putFields("card_data", Value.newBuilder()
                  .setStringValue("This is custom card data")
                  .build()).build()).build())
          .setHorizontal(true).build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.trace("customListCard: {}", customListCard);
    return customListCard;
  }
}
