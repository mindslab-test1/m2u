package ai.maum.m2u.utils.converter;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.Message.Builder;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.JsonFormat.TypeRegistry;
import java.io.IOException;
import javax.annotation.Nullable;
import okhttp3.ResponseBody;
import retrofit2.Converter;

final class ProtoJsonResponseBodyConverter<T extends Message>
    implements Converter<ResponseBody, T> {

  private final Builder builder;
  private final @Nullable
  TypeRegistry registry;

  ProtoJsonResponseBodyConverter(Builder builder, @Nullable TypeRegistry registry) {
    this.builder = builder;
    this.registry = registry;
  }

  @SuppressWarnings("unchecked")
  @Override
  public T convert(ResponseBody value) throws IOException {
    try {
      JsonFormat.Parser parser = JsonFormat.parser().ignoringUnknownFields();
      if (registry != null) {
        parser.usingTypeRegistry(registry);
      }
      parser.merge(value.string(), builder);

      return (T) builder.build();
    } catch (InvalidProtocolBufferException e) {
      throw new RuntimeException(e);
    } finally {
      value.close();
    }
  }
}
