package ai.maum.m2u.utils;

import ai.maum.m2u.config.PropertyManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import maum.brain.qa.nqa.Nqa.AttributeQueryUnit;
import maum.brain.qa.nqa.Nqa.LayerQueryUnit;
import maum.brain.qa.nqa.Nqa.QueryUnit;
//import lombok.Data;
//
//@Data

public class NqaOptionDocument {

  private static String targetChannel = "thezone";
  private static String confChannel = "thezone";

  private int ntop = PropertyManager.getInt("nqa." + confChannel + ".ntop");
  private float score = PropertyManager.getFloat("nqa." + confChannel + ".score");
  private float maxSize = PropertyManager.getFloat("nqa." + confChannel + ".maxsize");
  private String channel = targetChannel;
  //    private List<String> category = new ArrayList<>(); {
//        category.add("person");
//    }
  private List<String> category = Arrays
      .asList(PropertyManager.getStringArray("nqa." + confChannel + ".category"));
  //    private QueryType queryType = QueryType.ALL;
//    private QueryTarget queryTarget = QueryTarget.Q;
  private QueryUnit.Builder userQuery = QueryUnit.newBuilder()
      .setWeight(PropertyManager.getFloat("nqa." + confChannel + ".tagweight"));
  private List<String> ids = new ArrayList<>();
  private AttributeQueryUnit.Builder attribute = AttributeQueryUnit.newBuilder();
  private LayerQueryUnit.Builder layer = LayerQueryUnit.newBuilder();
  private List<QueryUnit> ner = new ArrayList<>();
  private List<QueryUnit> tags = new ArrayList<>();

  public static void setTargetChannel(String value) {
    if (value != null && !value.isEmpty()) {
      targetChannel = value;
      confChannel = targetChannel.toLowerCase();
    }
  }

  public static String getNqaIp() {
    return PropertyManager.getString("nqa." + confChannel + ".nqaip");
  }

  public static int getNqaPort() {
    return PropertyManager.getInt("nqa." + confChannel + ".nqaport");
  }

  public static List<String> getQuery() {
    return Arrays.asList(PropertyManager.getStringArray("nqa." + confChannel + ".query"));
  }

  public QueryUnit.Builder getUserQuery() {
    return userQuery;
  }

  public void setUserQuery(QueryUnit.Builder userQuery) {
    this.userQuery = userQuery;
  }

//    public QueryType getQueryType() { return queryType; }
//
//    public void setQueryType(QueryType queryType) {
//        this.queryType = queryType;
//    }
//
//    public QueryTarget getQueryTarget() {
//        return queryTarget;
//    }
//
//    public void setQueryTarget(QueryTarget queryTarget) {
//        this.queryTarget = queryTarget;
//    }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public List<String> getCategory() {
    return category;
  }

  public void setCategory(List<String> category) {
    this.category = category;
  }

  public List<String> getIds() {
    return ids;
  }

  public void setIds(List<String> ids) {
    this.ids = ids;
  }

  public int getNtop() {
    return ntop;
  }

  public void setNtop(int ntop) {
    this.ntop = ntop;
  }

  public float getScore() {
    return score;
  }

  public void setScore(float score) {
    this.score = score;
  }

  public float getMaxSize() {
    return maxSize;
  }

  public void setMaxSize(float maxSize) {
    this.maxSize = maxSize;
  }

  public AttributeQueryUnit.Builder getAttribute() {
    return attribute;
  }

  public void setAttribute(AttributeQueryUnit.Builder attribute) {
    this.attribute = attribute;
  }

  public LayerQueryUnit.Builder getLayer() {
    return layer;
  }

  public void setLayer(LayerQueryUnit.Builder layer) {
    this.layer = layer;
  }

  public List<QueryUnit> getNer() {
    return ner;
  }

  public void setNer(List<QueryUnit> ner) {
    this.ner = ner;
  }

  public List<QueryUnit> getTags() {
    return tags;
  }

  public void setTags(List<QueryUnit> tags) {
    this.tags = tags;
  }

}
