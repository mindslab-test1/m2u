package ai.maum.m2u.utils.converter;

import com.google.protobuf.Message;
import com.google.protobuf.Message.Builder;
import com.google.protobuf.util.JsonFormat.TypeRegistry;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import javax.annotation.Nullable;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public final class ProtoJsonConverterFactory extends Converter.Factory {

  public static ProtoJsonConverterFactory create() {
    return new ProtoJsonConverterFactory(null);
  }

  public static ProtoJsonConverterFactory createWithRegistry(@Nullable TypeRegistry registry) {
    return new ProtoJsonConverterFactory(registry);
  }

  private final @Nullable
  TypeRegistry registry;

  private ProtoJsonConverterFactory(@Nullable TypeRegistry registry) {
    this.registry = registry;
  }

  @Override
  public @Nullable
  Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations
      , Retrofit retrofit) {
    if (!(type instanceof Class<?>)) {
      return null;
    }
    Class<?> c = (Class<?>) type;
    if (!Message.class.isAssignableFrom(c)) {
      return null;
    }

    Builder builder;
    try {
      Method method = c.getDeclaredMethod("newBuilder");
      //noinspection unchecked
      builder = (Builder) method.invoke(null);
    } catch (InvocationTargetException e) {
      throw new RuntimeException(e.getCause());
    } catch (NoSuchMethodException | IllegalAccessException ignored) {
      throw new IllegalArgumentException("Found a protobuf message but "
          + c.getName()
          + " had no newBuilder() method.");
    }
    return new ProtoJsonResponseBodyConverter<>(builder, registry);
  }

  @Override
  public @Nullable
  Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations
      , Annotation[] methodAnnotations, Retrofit retrofit) {
    if (!(type instanceof Class<?>)) {
      return null;
    }
    if (!Message.class.isAssignableFrom((Class<?>) type)) {
      return null;
    }
    return new ProtoJsonRequestBodyConverter<>();
  }
}
