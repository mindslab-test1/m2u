package ai.maum.m2u.utils.converter;

import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Converter;

final class ProtoJsonRequestBodyConverter<T extends Message> implements Converter<T, RequestBody> {

  private static final MediaType MEDIA_TYPE = MediaType.get("application/json");

  @Override
  public RequestBody convert(T value) throws IOException {
    return RequestBody.create(MEDIA_TYPE
        , JsonFormat.printer().omittingInsignificantWhitespace().print(value));
  }
}
