package ai.maum.m2u.client;

import ai.maum.m2u.utils.GrpcClient;
import ai.maum.m2u.utils.GrpcClientPool;
import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.Channel;
import maum.common.LangOuterClass;
import maum.m2u.da.Provider;
import maum.m2u.da.v3.DialogAgentProviderGrpc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EchoDialogAgentClient extends GrpcClient<DialogAgentProviderGrpc.DialogAgentProviderFutureStub> {
    private static final Logger logger = LoggerFactory.getLogger(EchoDialogAgentClient.class);

    private EchoDialogAgentClient() {
    }

    /**
     * TalkServiceClient Pool
     **/
    public static final GrpcClientPool<EchoDialogAgentClient> pool = new GrpcClientPool<>(() -> new EchoDialogAgentClient());


    @Override
    protected DialogAgentProviderGrpc.DialogAgentProviderFutureStub newStub(Channel underlyingChannel) {
        return DialogAgentProviderGrpc.newFutureStub(underlyingChannel);
    }


    public ListenableFuture<Provider.DialogAgentProviderParam> init() {
        Provider.InitParameter request = Provider.InitParameter.newBuilder()
                .setChatbot("chatbot1")
                .addSkills("skill1")
                .putParams("key1", "value1")
                .putParams("key2", "value2")
                .putParams("key3", "value3")
                .setLang(LangOuterClass.LangCode.kor)
                .build();

        return stub.init(request);
    }
}