package ai.maum.m2u.map.hzc;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryExpiredListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntryEventListener implements EntryExpiredListener {

  static final Logger logger = LoggerFactory
      .getLogger(EntryEventListener.class);

  @Override
  public void entryExpired(EntryEvent event) {
    long time = System.currentTimeMillis();
    SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
    String str = dayTime.format(new Date(time));

    logger.debug("SYSTEM TIME :: {}", str);
    logger.debug("Expired Entry [KEY :: {}, VALUE :: {}", event.getKey(), event.getOldValue());
  }
}
