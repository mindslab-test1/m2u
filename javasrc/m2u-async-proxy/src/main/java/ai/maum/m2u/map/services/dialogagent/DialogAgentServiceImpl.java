package ai.maum.m2u.map.services.dialogagent;

import static ai.maum.config.PropertyManager.getString;

import ai.maum.config.Keys;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.common.user.UserParam;
import ai.maum.m2u.map.common.utils.CallLogUtills;
import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.ExceptionMsgUtills;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.common.utils.TLOLogUtills;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import maum.m2u.common.DeviceOuterClass;
import maum.m2u.common.Dialog;
import maum.m2u.common.EventOuterClass.Event;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException;
import maum.m2u.router.v3.Router;
import maum.m2u.router.v3.Router.TalkRouteResponse;
import maum.m2u.router.v3.TalkRouterGrpc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class DialogAgentServiceImpl extends Service {

  private static final int EX_INDEX = 1500;

  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  static final Logger logger = LoggerFactory.getLogger(
      DialogAgentServiceImpl.class);

  public DialogAgentServiceImpl() {
    logger.debug("#@ Create constructor of DialogAgentServiceImpl");
  }

  @Override
  public void OnDialogAgentEvent(MapEventHandler handler, EventStream event) {
    logger.debug("#@ IN OnDialogAgentEvent");

    Event.Builder eventBuilder = Event.newBuilder();
    try {
      String payload = JsonFormat.printer()
          .omittingInsignificantWhitespace()
          .print(event.getPayload());
      JsonFormat.parser().ignoringUnknownFields().merge(payload, eventBuilder);
      logger.trace("event: {}", eventBuilder);
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      String msg = MessageFormat.format("OnDialogAgentEvent event format is invalid. {0}:{1}",
          status.getCode(), status.getDescription());
      String exceptionJson = MessageFormat.format("{'exception': '{0}'}", status);

      logger.error("{}", msg);
      logger.error("Thread local {}", Dispatcher.threadLocal.get());
      logger.error("", e);

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(1),
              msg, event, exceptionJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
      return;
    }

    UserParam userParam = DialogUtils.createUserParam(event);
    Router.EventRouteRequest request = Router.EventRouteRequest.newBuilder()
        .setEvent(eventBuilder)
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get())
        )
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc.newStub(channel);

    logger.debug("#@ Router eventRoute \n [{}]", request.toString());
    Metadata operationSyncId = GrpcUtills.makeOperationSyncIdHeader(event);
    String tempOpSyncIdVal = event.getOperationSyncId();

    stub = MetadataUtils.attachHeaders(stub, operationSyncId);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .eventRoute(request, new StreamObserver<TalkRouteResponse>() {
          TalkRouteResponse ret = null;

          @Override
          public void onNext(TalkRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ eventRoute onNext [{}]", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            CallLogUtills.makeEventRouterCallLog(event, operationSyncId, request
                , userParam, null, reqDate, new Date());
            ExceptionMsgUtills.sendExceptionFromRouter(handler, t, request, reqDate, event
                , exIndex(2), MapException.StatusCode.GRPC_ROUTER_TALK_ERROR,
                "Event Route error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ eventRoute onCompleted");

            CallLogUtills.makeEventRouterCallLog(event, operationSyncId, request
                , userParam, ret, reqDate, new Date());
            TLOLogUtills.makeTLOLog(request, reqDate, new Date(), 200, exIndex(3));
            DirectiveStream dir = DirectiveStream.newBuilder()
                .setInterface(AsyncInterface.newBuilder()
                    .setInterface(MapIf.I_VIEW)
                    .setOperation(MapIf.VW_D_RENDER_TEXT)
                    .setType(OperationType.OP_DIRECTIVE)
                    .build())
                .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
                .setStreamId(UUID.randomUUID().toString())
                .setOperationSyncId(event.getOperationSyncId())
                .setPayload(DialogUtils.messageToStruct(ret))
                .build();

            handler.getDirectiveForwarder().sendStream(dir);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }
}
