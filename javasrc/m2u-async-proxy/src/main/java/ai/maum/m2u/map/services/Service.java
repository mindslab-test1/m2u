package ai.maum.m2u.map.services;

import ai.maum.m2u.map.handler.common.MapEventHandler;
import maum.brain.idr.Idr.ImageRecognitionParam;
import maum.brain.idr.Idr.ImageRecognitionResult;
import maum.brain.stt.Speech;
import maum.brain.stt.Speech.StreamingRecognizeResponse;
import maum.brain.stt.Stt.Segment;
import maum.m2u.map.Map.EventStream;

public class Service {

  /**
   * 대화 세션을 열기
   */
  public void OnDialogOpen(MapEventHandler handler, EventStream event) {
  }

  /**
   * 음성 대화, 음성 입력 및 음성 출력
   */
  public void OnDialogSpeechToSpeechTalk(MapEventHandler handler,
      EventStream event, Speech.SpeechRecognitionParam param) {
  }

  /**
   * STT 음성 인식 진행중
   */
  public void OnSpeechRecognizerRecognizing(MapEventHandler handler,
      StreamingRecognizeResponse streamingRes, EventStream callEvent) {
  }

  /**
   * STT 음성인식 완료
   */
  public void OnSpeechRecognizerRecognized(MapEventHandler handler,
      String resType,
      EventStream callEvent,
      StreamingRecognizeResponse streamingRes,
      String transcript,
      int tempLangVal) {
  }

  /**
   * STT EndpointDectected 수신후 호출
   */
  public void OnSpeechRecognizerEndpointDetected(MapEventHandler handler, Segment value) {
  }

  /**
   * 음성으로 입력하고 텍스트 형식으로 출력
   */
  public void OnDialogSpeechToTextTalk(MapEventHandler handler, EventStream event,
      Speech.SpeechRecognitionParam param) {
  }

  /**
   * 텍스트로 입력하고 텍스트 형식으로 출력
   */
  public void OnDialogTextToTextTalk(MapEventHandler handler, EventStream event) {
  }

  /**
   * 텍스트로 입력하고 음성으로 출력
   */
  public void OnDialogTextToSpeechTalk(MapEventHandler handler, EventStream event) {
  }

  /**
   * 이미지로 입력하고 음성으로 출력
   */
  public void OnDialogImageToSpeechTalk(MapEventHandler handler, EventStream event,
      ImageRecognitionParam imageRecognitionParam) {
  }

  /**
   * 이미지로 입력하고 텍스트로 출력
   */
  public void OnDialogImageToTextTalk(MapEventHandler handler, EventStream event,
      ImageRecognitionParam imageRecognitionParam) {
  }

  /**
   * 대화에 대한 답변 만족도에 대해서 리포트하기
   */
  public void OnDialogReportTalkSatispection(MapEventHandler handler,
      EventStream event) {
  }

  /**
   * 대화 도움말을 출력해준다.
   */
  public void OnDialogHelp(MapEventHandler handler, EventStream event) {
  }

  /**
   * 명시적인 대화 세션의 종료
   */
  public void OnDialogClose(MapEventHandler handler, EventStream event) {
  }

  /**
   * Dialog Agent에 이벤트를 전달하고 결과를 반환한다.
   */
  public void OnDialogAgentEvent(MapEventHandler handler, EventStream event) {
  }

  /**
   * 인증 실행결과
   */
  public void OnLauncherAuthorized(MapEventHandler handler, EventStream event) {
  }

  /**
   * 인증 실행실패
   */
  public void OnLauncherAuthorizeFailed(MapEventHandler handler, EventStream event) {
  }

  /**
   * 단말이 가지고 있는 슬롯이 채워졌다는 이벤트
   */
  public void OnLauncherSlotstFilled(MapEventHandler handler, EventStream event) {
  }

  /**
   * 앱 실행
   */
  public void OnLauncherLaunchExecuted(MapEventHandler handler, EventStream event) {
  }

  /**
   * 음성 인식 시작
   */
  public void OnSpeechRecognizerRecognize(MapEventHandler handler, EventStream event) {
  }

  /**
   * 이미지 인식 시작
   */
  public void OnImageRecognizerRecognize(MapEventHandler handler, EventStream event) {
  }

  /**
   * 이미지 인식 완료
   */
  public void OnImageRecognizerRecognized(MapEventHandler handle, ImageRecognitionResult imageRes
      , String resType, EventStream event) {
  }

  /**
   * 이미지 인식 시작
   */
  public void OnImageDocumentRecognizerRecognize(MapEventHandler handler, EventStream event) {
  }

  /**
   * 음성발화 요청
   */
  public void OnSpeechSynthesizerSpeak(MapEventHandler handler, EventStream event) {
  }

  /**
   * 음성발화 취소
   */
  public void OnSpeechSynthesizerPlayStopped(MapEventHandler handler, EventStream event) {
  }

  /**
   * 음성인식 실패
   */
  public void OnMicrophoneExpectSpeechTimedOut(MapEventHandler handler, EventStream event) {
    return;
  }

  /**
   * 음악 등의 AUDIO를 스피커를 통해서 출력
   */
  public void OnAudioPlayerEvent(MapEventHandler handler, EventStream event) {
  }

  /**
   * 동영상을 스피커와 화면에 출력
   */
  public void OnVideoPlayerEvent(MapEventHandler handler, EventStream event) {
  }

  /**
   * 현재 시스템의 상태
   */
  public void OnSystemReportDeviceState(MapEventHandler handler, EventStream event) {
  }

  /**
   * Directive로 전달된 명령의 실패
   */
  public void OnSystemCommandExecuted(MapEventHandler handler, EventStream event) {
  }

  /**
   * Directive로 전달된 명령의 실패
   */
  public void OnSystemCommandFailed(MapEventHandler handler, EventStream event) {
  }

  /**
   * 사용자 인증으로 데이터를 넘겨준다. 인증후 인증 토큰을 획득한다.
   */
  public void OnAuthSignIn(MapEventHandler handler, EventStream event) {
  }

  /**
   * MULTI FACTOR 인증에 대한 확인 요청 다중 요청에 대한 처리
   */
  public void MultiFactorVerify(MapEventHandler handler, EventStream event) {
  }

  /**
   * 로그아웃
   */
  public void OnAuthSignOut(MapEventHandler handler, EventStream event) {
  }

  /**
   * 가입처리
   */
  public void OnAuthSignOn(MapEventHandler handler, EventStream event) {
  }

  /**
   * 토큰이 정상 여부 확인
   */
  public void OnIsValid(MapEventHandler handler, EventStream event) {
  }

  /**
   * 토큰에 따른 사용자의 최종 정보를 반환한다.
   */
  public void OnGetUserInfo(MapEventHandler handler, EventStream event) {
  }

  /**
   * 사용자 속성 정의
   */
  public void OnUpdateUserSettings(MapEventHandler handler, EventStream event) {
  }

  /**
   * 사용자 속성 조회
   */
  public void OnGetUserSettings(MapEventHandler handler, EventStream event) {
  }

}
