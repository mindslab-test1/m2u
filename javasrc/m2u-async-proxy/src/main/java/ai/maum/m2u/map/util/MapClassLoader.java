package ai.maum.m2u.map.util;

import ai.maum.config.PropertyManager;
import ai.maum.m2u.map.handler.common.AnomalyMessageFilter;
import ai.maum.m2u.map.handler.common.MessageCryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MapClassLoader {

  private static final Logger logger = LoggerFactory.getLogger(MapClassLoader.class);

  /**
   * Class Loader 기능을 이용하여 암호화 클래스 객체를 생성합니다 최초 한번만 수행합니다
   *
   * @return 암복호화 객체
   */
  public static MessageCryptor loadMessageCryptor() {
    MessageCryptor cryptor;
    Class<?> tempClass;

    String tempCryptorName = PropertyManager.getString("map.forwarder.cryptor.class.name");
    logger.debug("#@ tempCryptorName [{}]", tempCryptorName);
    try {
      tempClass = Class.forName(tempCryptorName);
    } catch (ClassNotFoundException e) {
      logger.error("loadMessageCryptor e : " , e);
      return null;
    }

    logger.debug("#@ CryptorImpl's is [{}]", tempClass.getName());
    if (MessageCryptor.class.isAssignableFrom(tempClass)) {
      try {
        cryptor = (MessageCryptor) tempClass.newInstance();
      } catch (InstantiationException | IllegalAccessException e) {
        logger.error("loadMessageCryptor e : " , e);
        return null;
      }
    } else {
      logger.error("There is no CryptorImpl");
      return null;
    }

    return cryptor;
  }

  /**
   * Class Loader 기능을 이용하여 Anomaly Message Filter 객체를 생성합니다 최초 한번만 수행합니다
   *
   * @return AnomalyMessageFilter 객체
   */
  public static AnomalyMessageFilter loadAnomalyMessageFilter() {
    AnomalyMessageFilter anomalyMessageFilter;
    Class<?> tempClass;

    String tempAnomalyMessageFilterName = PropertyManager
        .getString("map.forwarder.anomaly.message.filter.class.name");
    logger.debug("#@ tempAnomalyMessageFilterName [{}]", tempAnomalyMessageFilterName);
    try {
      tempClass = Class.forName(tempAnomalyMessageFilterName);
    } catch (ClassNotFoundException e) {
      logger.error("loadAnomalyMessageFilter e : " , e);
      return null;
    }

    logger.debug("#@ AnomalyMessageFilterImpl's is [{}]", tempClass.getName());
    if (AnomalyMessageFilter.class.isAssignableFrom(tempClass)) {
      try {
        anomalyMessageFilter = (AnomalyMessageFilter) tempClass.newInstance();
      } catch (IllegalAccessException | InstantiationException e) {
        logger.error("loadAnomalyMessageFilter e : " , e);
        return null;
      }
    } else {
      logger.error("There is no anomalyMessageFilter");
      return null;
    }

    return anomalyMessageFilter;
  }

}
