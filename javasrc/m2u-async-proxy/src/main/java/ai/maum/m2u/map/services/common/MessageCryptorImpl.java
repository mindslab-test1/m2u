package ai.maum.m2u.map.services.common;

import ai.maum.m2u.map.handler.common.MessageCryptor;
import com.google.protobuf.ByteString;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageCryptorImpl implements MessageCryptor {

  static final Logger logger = LoggerFactory.getLogger(MessageCryptorImpl.class);
  static byte[] rawKey = null;
  private final static String HEX = "0123456789ABCDEF";

  public MessageCryptorImpl() {
    logger.debug("#@ Create constructor of MessageCryptorImpl");

    try {
      // 암호화 키
      rawKey = getRawKey("EncTestByKimByoungKi".getBytes());
    } catch (Exception e) {
      logger.error("MessageCryptorImpl e : " , e);
    }
  }

  //암호화
  @Override
  public ByteString encode(ByteString clearMsg) {
    logger.debug("#@ MessageCryptorImpl encode clearMsg [{}]", clearMsg);
    byte[] tempRes = null;

    try {
      SecretKeySpec skeySpec = new SecretKeySpec(rawKey, "AES");
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
      tempRes = cipher.doFinal(clearMsg.toByteArray());
    } catch (Exception e) {
      logger.error("encode e : " , e);
    }

    // ByteString copyFrom() 함수를 사용해서 리턴형을 ByteString 으로 전달합니다
    ByteString tempResult = ByteString.copyFrom(tempRes);

    return tempResult;
  }

  @Override
  public String encode(String clearMsg) {
    byte[] result = new byte[0];
    try {
      //result = encode(clearMsg.getBytes());
      SecretKeySpec skeySpec = new SecretKeySpec(rawKey, "AES");
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
      result = cipher.doFinal(clearMsg.getBytes());
    } catch (Exception e) {
      logger.error("encode e : " , e);
    }

    return toHex(result);
  }

  //복호화
  @Override
  public ByteString decode(ByteString encryptMsg) {
    logger.debug("#@ MessageCryptorImpl decode encryptMsg [{}]", encryptMsg);

    byte[] tempRes = null;
    try {
      SecretKeySpec skeySpec = new SecretKeySpec(rawKey, "AES");
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.DECRYPT_MODE, skeySpec);
      tempRes = cipher.doFinal(encryptMsg.toByteArray());
    } catch (Exception e) {
      logger.error("decode e : " , e);
    }

    // ByteString copyFrom() 함수를 사용해서 리턴형을 ByteString 으로 전달합니다
    ByteString tempResult = ByteString.copyFrom(tempRes);

    return tempResult;
  }

  @Override
  public String decode(String encryptMsg) {
    byte[] enc = toByte(encryptMsg);
    byte[] result = new byte[0];
    try {
      //result = decode(enc);
      SecretKeySpec skeySpec = new SecretKeySpec(rawKey, "AES");
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.DECRYPT_MODE, skeySpec);
      result = cipher.doFinal(enc);
    } catch (Exception e) {
      logger.error("decode e : " , e);
    }
    return new String(result);
  }

  /**
   * 암호화, 복호화 시에 사용되는 key를 생성합니다
   */
  private static byte[] getRawKey(byte[] seed) throws Exception {
    KeyGenerator kgen = KeyGenerator.getInstance("AES");
    SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
    sr.setSeed(seed);
    kgen.init(128, sr);
    SecretKey skey = kgen.generateKey();
    byte[] raw = skey.getEncoded();
    return raw;
  }


  public static byte[] toByte(String hexString) {
    int len = hexString.length()/2;
    byte[] result = new byte[len];
    for (int i = 0; i < len; i++)
      result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
    return result;
  }

  public static String toHex(byte[] buf) {
    if (buf == null)
      return "";
    StringBuffer result = new StringBuffer(2*buf.length);
    for (int i = 0; i < buf.length; i++) {
      appendHex(result, buf[i]);
    }
    return result.toString();
  }

  private static void appendHex(StringBuffer sb, byte b) {
    sb.append(HEX.charAt((b>>4)&0x0f)).append(HEX.charAt(b&0x0f));
  }

}
