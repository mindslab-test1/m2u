package ai.maum.m2u.map.services.mobile;

import ai.maum.m2u.map.MaumToYouProxyServiceImpl;
import ai.maum.m2u.map.services.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MicServiceImpl extends Service {

  static final Logger logger = LoggerFactory
      .getLogger(MaumToYouProxyServiceImpl.class);

  public MicServiceImpl() {
    logger.debug("#@ Create constructor of MicServiceImpl");
  }
}
