package ai.maum.m2u.map.common.utils;

import ai.maum.m2u.map.common.user.UserParam;
import com.google.protobuf.Value;
import io.grpc.Metadata;
import java.text.SimpleDateFormat;
import java.util.Date;
import maum.m2u.map.Authentication.MultiFactorVerifyPayload;
import maum.m2u.map.Authentication.SignInPayload;
import maum.m2u.map.Authentication.SignInResultPayload;
import maum.m2u.map.Authentication.SignOutPayload;
import maum.m2u.map.Authentication.SignOutResultPayload;
import maum.m2u.map.Map.EventStream;
import maum.m2u.router.v3.Router.CloseRouteRequest;
import maum.m2u.router.v3.Router.CloseRouteResponse;
import maum.m2u.router.v3.Router.EventRouteRequest;
import maum.m2u.router.v3.Router.OpenRouteRequest;
import maum.m2u.router.v3.Router.TalkRouteRequest;
import maum.m2u.router.v3.Router.TalkRouteResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.MarkerFactory;

public class CallLogUtills {

  static final Logger logger = LoggerFactory.getLogger(CallLogUtills.class);

  private static <ReqT, ResT> void callLog(ReqT req, ResT res) {
    if (res != null) {
      MDC.put("isSuccess", "Y");
      logger.debug(MarkerFactory.getMarker("CallLog"), "", req, res);
    } else {
      MDC.put("isSuccess", "N");
      logger.debug(MarkerFactory.getMarker("CallLog"), "", req);
    }
  }


  /**
   * OpenRouter Call Log에 기록할 정보를 MDC를 이용한다(해당 값은 Req, Res에 없는 정보이다)
   *
   * @param event event stream 객체
   * @param operationSyncId 해당 talk에 유일한 sync id 값
   */
  public static void makeOpenRouterCallLog(EventStream event, Metadata operationSyncId,
      OpenRouteRequest reqParam, UserParam userParam, TalkRouteResponse talkRouteResponse,
      Date reqDate, Date resDate) {
    logger.info("#@ START makeOpenRouterCallLog");

    CallLogUtills.setDefaultMDC(event, operationSyncId, userParam, reqDate, resDate);

    CallLogUtills.setMDCFromResParam(talkRouteResponse);
    callLog(reqParam, talkRouteResponse);
    MDC.clear();
    MDC.put("operationSyncId", event.getOperationSyncId());
  }

  /**
   * CloseRouter Call Log에 기록할 정보를 MDC를 이용한다(해당 값은 Req, Res에 없는 정보이다)
   *
   * @param event event stream 객체
   * @param operationSyncId 해당 talk에 유일한 sync id 값
   */
  public static void makeCloseRouterCallLog(EventStream event, Metadata operationSyncId,
      CloseRouteRequest reqParam, UserParam userParam, CloseRouteResponse closeRouteResponse,
      Date reqDate, Date resDate) {
    logger.info("#@ START makeCloseRouterCallLog");

    CallLogUtills.setDefaultMDC(event, operationSyncId, userParam, reqDate, resDate);

    callLog(reqParam, closeRouteResponse);

    MDC.clear();
    MDC.put("operationSyncId", event.getOperationSyncId());
  }

  /**
   * TalkRouter Call Log에 기록할 정보를 MDC를 이용한다(해당 값은 Req, Res에 없는 정보이다)
   *
   * @param event event stream 객체
   * @param operationSyncId 해당 talk에 유일한 sync id 값
   */
  public static void makeTalkRouterCallLog(EventStream event, Metadata operationSyncId,
      TalkRouteRequest reqParam, UserParam userParam, TalkRouteResponse resParam, Date reqDate,
      Date resDate) {
    logger.info("#@ START makeTalkRouterCallLog");

    CallLogUtills.setDefaultMDC(event, operationSyncId, userParam, reqDate, resDate);
    CallLogUtills.setMDCFromResParam(resParam);
    callLog(reqParam, resParam);

    MDC.clear();
    MDC.put("operationSyncId", event.getOperationSyncId());
  }

  /**
   * EventRouter Call Log에 기록할 정보를 MDC를 이용한다(해당 값은 Req, Res에 없는 정보이다)
   *
   * @param event event stream 객체
   * @param operationSyncId 해당 talk에 유일한 sync id 값
   */
  public static void makeEventRouterCallLog(EventStream event, Metadata operationSyncId,
      EventRouteRequest reqParam, UserParam userParam, TalkRouteResponse resParam, Date reqDate,
      Date resDate) {
    logger.info("#@ START makeTalkRouterCallLog");

    CallLogUtills.setDefaultMDC(event, operationSyncId, userParam, reqDate, resDate);
    CallLogUtills.setMDCFromResParam(resParam);
    callLog(reqParam, resParam);

    MDC.clear();
    MDC.put("operationSyncId", event.getOperationSyncId());
  }

  /**
   * SignIn Call Log에 기록할 정보를 MDC를 이용한다(해당 값은 Req, Res에 없는 정보이다)
   *
   * @param event event stream 객체
   * @param operationSyncId 해당 talk에 유일한 sync id 값
   */
  public static void makeSignInCallLog(EventStream event, Metadata operationSyncId,
      SignInPayload reqParam, UserParam userParam, SignInResultPayload resParam, Date reqDate,
      Date resDate) {
    logger.info("#@ START makeSignInCallLog");

    CallLogUtills.setDefaultMDC(event, operationSyncId, userParam, reqDate, resDate);

    callLog(reqParam, resParam);
    MDC.clear();
    MDC.put("operationSyncId", event.getOperationSyncId());
  }

  /**
   * MultiFactorVerifyHandler Call Log에 기록할 정보를 MDC를 이용한다(해당 값은 Req, Res에 없는 정보이다)
   *
   * @param event event stream 객체
   * @param operationSyncId 해당 talk에 유일한 sync id 값
   */
  public static void makeMultiFactorVerifyCallLog(EventStream event, Metadata operationSyncId,
      MultiFactorVerifyPayload reqParam, UserParam userParam, SignInResultPayload resParam,
      Date reqDate, Date resDate) {
    logger.info("#@ START makeMultiFactorVerifyCallLog");

    CallLogUtills.setDefaultMDC(event, operationSyncId, userParam, reqDate, resDate);
    callLog(reqParam, resParam);

    MDC.clear();
    MDC.put("operationSyncId", event.getOperationSyncId());
  }

  /**
   * SignOut Log에 기록할 정보를 MDC를 이용한다(해당 값은 Req, Res에 없는 정보이다)
   *
   * @param event event stream 객체
   * @param operationSyncId 해당 talk에 유일한 sync id 값
   */
  public static void makeSignOutCallLog(EventStream event, Metadata operationSyncId,
      SignOutPayload reqParam, UserParam userParam, SignOutResultPayload resParam, Date reqDate,
      Date resDate) {
    logger.info("#@ START makeSignOutCallLog");

    CallLogUtills.setDefaultMDC(event, operationSyncId, userParam, reqDate, resDate);

    callLog(reqParam, resParam);

    MDC.clear();
    MDC.put("operationSyncId", event.getOperationSyncId());
  }


  /**
   * Response 유무에 상관없이 설정하는 MDC 정보
   */
  private static void setDefaultMDC(EventStream event, Metadata operationSyncId,
      UserParam userParam, Date reqDate, Date resDate) {
    String tempChatbot = "";

    MDC.put("operationSyncId", event.getOperationSyncId());
    MDC.put("operation", event.getInterface().getOperation());
    MDC.put("interface", event.getInterface().getInterface());

    if (event.hasPayload()) {
      tempChatbot = event.getPayload()
          .getFieldsOrDefault("chatbot", Value.newBuilder().build()).getStringValue();
    }
    MDC.put("chatbot", tempChatbot);
    MDC.put("channel", userParam.device.getChannel());
    MDC.put("service", "MAP");

    // 호출시작 시간
    MDC.put("reqDate", getDate(reqDate.getTime()));
    // 호출종료 시간
    MDC.put("resDate", getDate(resDate.getTime()));
  }

  private static String getDate(long time) {
    Date tempDate = new Date(time);
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    String dateFormatStringTime;
    dateFormatStringTime = dateFormat.format(tempDate);

    return dateFormatStringTime;
  }

  /**
   * Router Response 가 있는 경우에만 설정되는 MDC 정보 (skill)
   */
  private static void setMDCFromResParam(TalkRouteResponse resParam) {

    if (resParam != null) {
      String tempSkill = "";
      String tempSessionId = "";

      if (resParam.getResponse().hasMeta()) {
        tempSessionId = resParam.getResponse().getMeta()
            .getFieldsOrDefault("sessionid", Value.newBuilder().build()).getStringValue();

        tempSkill = resParam.getResponse().getMeta()
            .getFieldsOrDefault("skill", Value.newBuilder().build()).getStringValue();
      }

      MDC.put("skill", tempSkill);
      MDC.put("sessionid", tempSessionId);
    }
  }
}
