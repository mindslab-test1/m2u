package ai.maum.m2u.map.handler.common;

public interface AnomalyMessageFilter {

  /**
   * SQL Injection 공격인지 판단합니다.
   * 예를 들어 mapEvent의 payload 안에 select, insert, update, delete 등이 있는지 확인합니다.
   * @param mapEvent
   * @return 공격성 message일 경우 true를 리턴합니다
   */
  public boolean isAnomalyMessage(String message);
}
