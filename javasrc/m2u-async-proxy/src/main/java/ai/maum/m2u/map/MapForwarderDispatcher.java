package ai.maum.m2u.map;

import ai.maum.config.Keys;
import ai.maum.config.PropertyManager;
import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.handler.common.MessageCryptor;
import ai.maum.m2u.map.util.MapClassLoader;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.Timestamps;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.Map.MapEvent.TestEventCase;
import maum.m2u.map.Map.MapException;
import maum.m2u.map.Map.MapException.StatusCode;
import maum.m2u.map.MapSecureForwarderGrpc;
import maum.m2u.map.MapSecureForwarderGrpc.MapSecureForwarderStub;
import maum.m2u.map.SecureForward.CipherBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Client로 부터 수신받은 Event를 SecureMap으로 전달 SecureMap으로 수신받은 Directive를 Client로 전달
 */
public class MapForwarderDispatcher {

  static final Logger logger = LoggerFactory.getLogger(MapForwarderDispatcher.class);

  private static int EX_INDEX = 1200;

  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  // cryptorImpl 객체를 생성합니다
  private static MessageCryptor cryptor = MapClassLoader.loadMessageCryptor();

  // FORWARDER to SECURE
  // sucure map에 CipherBody 전송, SMAP으로부터 CipherBody 수신 겹쳐지는 이슈가 있음
  private StreamObserver<CipherBody> forwarderToSecureObserver;
  // channel 객체
  private ManagedChannel forwarderToSecureChannel;
  // sucure map과 통신하기 위한 Client Stub (요청마다 초기화)
  private MapSecureForwarderStub forwarderToSecureStub;

  // FORWARDER to CLIENT
  // client로 Directive를 전달하기 위한 Observer
  private StreamObserver<MapDirective> forwarderToClientObserver;

  /**
   * 필요한 객체를 초기화 합니다.
   */
  public MapForwarderDispatcher(StreamObserver<MapDirective> res) {
    // 생성자에서 객체를 모두 초기화 합니다
    forwarderToClientObserver = res;

    forwarderToSecureChannel = DialogUtils
        .getChannel(PropertyManager.getString("secure.map.proxy.export"));
    forwarderToSecureStub = MapSecureForwarderGrpc.newStub(forwarderToSecureChannel);
    setHeaderInfo();

    forwarderToSecureObserver = forwarderToSecureStub
        .withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .eventStream(new SecureDirectiveRecvObserver());
  }


  /**
   * Client로 부터 onCompleted를 받으면 Secure로 명시적으로 onCompleted를 호출합니다
   */
  public void forwarderToClientOnCompleted() {
    if (forwarderToSecureObserver != null) {
      forwarderToSecureObserver.onCompleted();
    }
  }

  /**
   * client에서 수신받은 mapEvent를 암호화 하여 전송합니다
   *
   * @param mapEvent 처리해야할 Event
   */
  public void handleEvent(MapEvent mapEvent) {
//    TestEventCase oneof = mapEvent.getTestEventCase();

    // 2. 문제가 없다면 MapEvent -> Any 타입으로 변경
    Any any = Any.pack(mapEvent);
    // 3. 이후 암호화 로직으로 암호화 처리 (암호화인터페이스)
    ByteString clear = any.toByteString();
    // 암호화 수행
    //MessageCryptorImpl mci = new MessageCryptorImpl();

    // Class Loader 기능을 이용하여 암호화 클래스 객체를 생성합니다
    ByteString cipher = cryptor.encode(clear);
    CipherBody.Builder cipherBody = CipherBody.newBuilder().setCipher(cipher);

    // SECURE에서 cipherBody를 잘 받았는지 비교할 수 있습니다
    logger.trace("#@ OUT FORWARDER to SECURE cipherBody [{}]", cipherBody);
    forwarderToSecureObserver.onNext(cipherBody.build());
  }

  /**
   * Header 정보 설정 forwarderToSecureStub 객체를 초기화 하는 함수
   */
  private void setHeaderInfo() {

    // forwarderToSecureStub 객체를 초기화 (Header 정보 재설정) smapChannel 서버정보
    //forwarderToSecureStub = MapSecureForwarderGrpc.newStub(forwarderToSecureChannel);

    String authTokenValue = MapForwarderInterceptor.AUTH_TOKEN_VALUE.get();

    // m2u-auth-token Header 정보를 attach 합니다
    if (!authTokenValue.isEmpty() && !authTokenValue.equalsIgnoreCase("")) {
      Metadata fixedHeaders = new Metadata();
      Metadata.Key<String> key = Metadata.Key
          .of(Keys.I_META_M2U_AUTH_TOKEN, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(key, MapForwarderInterceptor.AUTH_TOKEN_VALUE.get());

      forwarderToSecureStub = MetadataUtils.attachHeaders(forwarderToSecureStub, fixedHeaders);
    }
    // m2u-auth-sign-in Header 정보를 attach 합니다
    if (MapForwarderInterceptor.HAS_SIGN_IN_HEADER.get()) {
      Metadata fixedHeaders = new Metadata();
      Metadata.Key<String> key = Metadata.Key
          .of(Keys.I_META_M2U_AUTH_SIGN_IN, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(key, "");

      forwarderToSecureStub = MetadataUtils.attachHeaders(forwarderToSecureStub, fixedHeaders);
    }
    // m2u-auth-internal Header 정보를 attach 합니다
    if (MapForwarderInterceptor.HAS_INTERNAL_AUTH_HEADER.get()) {
      Metadata fixedHeaders = new Metadata();
      Metadata.Key<String> key = Metadata.Key
          .of(Keys.I_META_M2U_INTERNAL_AUTH, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(key, "");

      forwarderToSecureStub = MetadataUtils.attachHeaders(forwarderToSecureStub, fixedHeaders);
    }
  }

  /**
   * 예외를 전송합니다.
   *
   * @param code 상태 코드
   * @param exIndex 예외 발생한 위치를 식별할 수 있는 정보
   * @param exMessage 예외 메시지
   */
  public void sendException(StatusCode code, int exIndex, String exMessage) {
    logger.debug("#@ sendException code = {}, exIndex = {}, exMessage = {}",
        code, exIndex, exMessage);

    MapException.Builder exb = MapException.newBuilder();

    exb.setStatusCode(code)
        .setExceptionId(UUID.randomUUID().toString())
        .setThrownAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setExIndex(exIndex)
        .setExMessage(exMessage)
        .build();

    MapDirective dir = MapDirective.newBuilder()
        .setException(exb.build())
        .build();

    forwarderToClientObserver.onNext(dir);
  }

  class SecureDirectiveRecvObserver implements StreamObserver<CipherBody> {

    @Override
    public void onNext(CipherBody cipherBody) {
      // 1.SecureMap으로 부터 Directive를 수신 합니다
      // 2.CipherBody를 MapForwarder로 전달합니다
      // 3.DirectiveStream directiveStream = value.getDirective();
      //ByteString byteString = cipherBody.getCipher();
      ByteString decrypedMsg;
      MapDirective tempMapDirective;
      // classLoader를 사용하여 복호화 클래스를 생성합니다
      decrypedMsg = cryptor.decode(cipherBody.getCipher());
      try {
        Any anyMessage = Any.newBuilder().mergeFrom(decrypedMsg.toByteArray()).build();
        tempMapDirective = anyMessage.unpack(MapDirective.class);

        // Client로 전달합니다
        forwarderToClientObserver.onNext(tempMapDirective);

      } catch (InvalidProtocolBufferException e) {
        logger.error("onNext e : " , e);
        sendException(StatusCode.MAP_PAYLOAD_ERROR, exIndex(1), e.getMessage());
        return;
      }
    }

    @Override
    public void onError(Throwable throwable) {
      logger.error("onError e : " , throwable);
      logger.debug("#@ IN Event Stream Error [{}]", throwable);
      forwarderToSecureObserver = null;

      sendException(StatusCode.FORWARDER_DISPATCHER_ERROR, exIndex(2), throwable.getMessage());
//      forwarderToClientObserver.onError(throwable);

      logger.trace("#@ Start forwarderToClientObserver onCompleted by onError");
      forwarderToClientObserver.onCompleted();
      logger.trace("#@ End forwarderToClientObserver onCompleted by onError");

      // forwarderToSecureChannel을 close 합니다
      if (forwarderToSecureChannel != null) {
        logger.trace("#@ Start forwarderToSecureChannel close by onError");
        GrpcUtills.closeChannel(forwarderToSecureChannel);
      }
    }

    @Override
    public void onCompleted() {
      forwarderToSecureObserver = null; // forwarderToSecureObserver 객체 초기화

      logger.trace("#@ Start forwarderToClientObserver onCompleted");
      forwarderToClientObserver.onCompleted();
      logger.trace("#@ End forwarderToClientObserver onCompleted");

      // forwarderToSecureChannel을 close 합니다
      if (forwarderToSecureChannel != null) {
        logger.trace("#@ Start forwarderToSecureChannel close");
        GrpcUtills.closeChannel(forwarderToSecureChannel);
      }
    }
  }
}
