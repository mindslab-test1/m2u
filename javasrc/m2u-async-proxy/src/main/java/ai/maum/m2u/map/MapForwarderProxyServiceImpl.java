package ai.maum.m2u.map;

import ai.maum.m2u.map.handler.common.AnomalyMessageFilter;
import ai.maum.m2u.map.util.MapClassLoader;
import com.google.protobuf.util.Timestamps;
import io.grpc.ServerInterceptors;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterfaceList;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.Map.MapException;
import maum.m2u.map.Map.MapException.StatusCode;
import maum.m2u.map.Map.PingRequest;
import maum.m2u.map.Map.PongResponse;
import maum.m2u.map.MaumToYouProxyServiceGrpc.MaumToYouProxyServiceImplBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * MAP Forwarder Service Implementation.
 * 1.MAP Forwarder는 CDK -> NGINX 로 부터 Client로 부터 수신 받은 MAPEvent를 그대로 받습니다.
 * 2.MAP Event payload 안에 SQL Injection CMD, 함수명이 들어 있으면 SendException 처리를 합니다.
 * 3.문제가 없다면 MapEvent -> Any 타입으로
 * 4.toByteArray()를 Serialization을 통해
 * 5. 이후 암호화 로직으로 암호화 처리(암호화인터페이스)
 * 6. SMAP Client로 전송
 *
 * - ping() - eventStream()
 */
public class MapForwarderProxyServiceImpl extends MaumToYouProxyServiceImplBase {

  private static int EX_INDEX = 1400;

  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  static final Logger logger = LoggerFactory.getLogger(MapForwarderProxyServiceImpl.class);

  // MapForwarderInterceptor 객체
  private MapForwarderInterceptor mapForwarderInterceptor;
  private AnomalyMessageFilter anomalyMessageFilter;
  private MapClassLoader mapClassLoader;

  /**
   * MapForwarderProxyServiceImpl 생성자 ServerInterceptors에 mapForwarderInterceptor 설정
   */
  public MapForwarderProxyServiceImpl() {
    mapForwarderInterceptor = new MapForwarderInterceptor();
    ServerInterceptors.intercept(this.bindService(), mapForwarderInterceptor);

    mapClassLoader = new MapClassLoader();
    anomalyMessageFilter = mapClassLoader.loadAnomalyMessageFilter();
  }

  /**
   * Client로 부터 Event 수신 수신받은 MapEvent가 무결한지 체크(SQL Injection) 암호화 하여 Secure Map으로 전송
   *
   * @param res Client로 부터 수신받은 StreamObserver
   */
  @Override
  public StreamObserver<MapEvent> eventStream(final StreamObserver<MapDirective> res) {
    return new StreamObserver<MapEvent>() {

      MapForwarderDispatcher mapForwarderDispatcher = new MapForwarderDispatcher(res);

      // Client로 부터 수신
      @Override
      public void onNext(MapEvent mapEvent) {

        logger.debug("#@ Forwarder mapEvent [{}]", mapEvent);

        // 작업중 처리
        // m2u-maintenance 헤더가 있다면 "작업중입니다" sendException 처리를 합니다
        String m2uMaintenanceValue = MapForwarderInterceptor.M2U_MAINTENANCE_VALUE.get();
        if (!m2uMaintenanceValue.isEmpty() && !m2uMaintenanceValue.equalsIgnoreCase("")) {

          String tempMaintenanceMsg = "";
          tempMaintenanceMsg = getMaintenanceText();
          logger.debug("#@ tempMaintenanceMsg [{}]", tempMaintenanceMsg);
          sendException(StatusCode.MAP_SYSTEM_MAINTENANCE, exIndex(1), tempMaintenanceMsg, res);

          res.onCompleted();
          return;
        }

        String tempPayload = mapEvent.getEvent().getPayload().toString();

        // 1.MAP Event payload 안에 SQL Injection CMD, 함수명이 들어 있으면 SendException 처리를 합니다
        if (anomalyMessageFilter.isAnomalyMessage(tempPayload)) {
          logger.debug("This event is considered an attack. Rejects this event.");
          sendException(StatusCode.MAP_PAYLOAD_ERROR, exIndex(2),
              "This event is considered an attack. Rejects this event.", res);
          res.onCompleted();
          return;
        }

        mapForwarderDispatcher.handleEvent(mapEvent);
      }

      @Override
      public void onError(Throwable t) {
        logger.error("#@ IN eventStream onError{} => ", Status.fromThrowable(t), t);
      }

      @Override
      public void onCompleted() {
        mapForwarderDispatcher.forwarderToClientOnCompleted();
      }
    };
  }

  @Override
  public void ping(PingRequest req, StreamObserver<PongResponse> res) {
    try {
      PongResponse pong = PongResponse.newBuilder()
          .setVersion("111111")
          .build();
      res.onNext(pong);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", Status.fromThrowable(e), e, Dispatcher.threadLocal.get());
      res.onError(
          new StatusException(Status.FAILED_PRECONDITION
              .withDescription(e.getMessage())
              .withCause(e)));
    }
  }

  @Override
  public void listAsyncInterfaces(AsyncInterface req, StreamObserver<AsyncInterfaceList> res) {
    try {
      AsyncInterfaceList list = AsyncInterfaceList.newBuilder()
          .addInterfaces(AsyncInterface.newBuilder()
              .setStreaming(false)
              .setInterface("Echo")
              .setOperation("Echo")
              .build())
          .build();
      res.onNext(list);
      res.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", Status.fromThrowable(e), e, Dispatcher.threadLocal.get());
      res.onError(
          new StatusException(Status.FAILED_PRECONDITION
              .withDescription(e.getMessage())
              .withCause(e)));
    }
  }

  @Override
  public StreamObserver<MapEvent> getDirectives(final StreamObserver<MapDirective> res) {
    return eventStream(res);
  }

  /**
   * 예외를 전송합니다.
   *
   * @param code 상태 코드
   * @param exIndex 예외 발생한 위치를 식별할 수 있는 정보
   * @param exMessage 예외 메시지
   */
  private void sendException(StatusCode code, int exIndex, String exMessage,
      StreamObserver<MapDirective> forwarderToClientObserver) {
    logger.debug("#@ sendException code = {}, exIndex = {}, exMessage = {}",
        code, exIndex, exMessage);

    MapException.Builder exb = MapException.newBuilder();

    exb.setStatusCode(code)
        .setExceptionId(UUID.randomUUID().toString())
        .setThrownAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setExIndex(exIndex)
        .setExMessage(exMessage)
        .build();

    MapDirective dir = MapDirective.newBuilder()
        .setException(exb.build())
        .build();

    forwarderToClientObserver.onNext(dir);
  }

  /**
   * 작업공지 문구를 외부 파일에서 불러옵니다 외부파일의 위치는 $MAUM_ROOT/etc/map/maintenance.txt 입니다
   *
   * @return 작업공지문구
   */
  private String getMaintenanceText() {
    StringBuilder tempMaintenanceText = new StringBuilder();
    try {
      //파일 객체 생성
      File file = new File(System.getenv("MAUM_ROOT") + "/etc/map/maintenance.txt");

      //입력 스트림 생성
      FileReader filereader = new FileReader(file);
      int singleCh = 0;
      while ((singleCh = filereader.read()) != -1) {
        System.out.print((char) singleCh);
        tempMaintenanceText.append((char) singleCh);
      }
      // 마지막 개행문자 제거
      tempMaintenanceText = tempMaintenanceText.deleteCharAt(tempMaintenanceText.length() - 1);

      filereader.close();
    } catch (FileNotFoundException e) {
      // Exception이 발생하더라도 명시적으로 작업중이므로 default 문자를 전송합니다
      logger.error("{} => ", Status.fromThrowable(e), e, null);
      return "Essential maintenance in progress for M2U AI service.";
    } catch (IOException e) {
      logger.error("{} => ", Status.fromThrowable(e), e, null);
      return "Essential maintenance in progress for M2U AI service.";
    }
    return tempMaintenanceText.toString();
  }
}
