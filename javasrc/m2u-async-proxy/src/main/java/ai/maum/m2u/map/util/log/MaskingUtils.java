package ai.maum.m2u.map.util.log;

import java.lang.Math;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MaskingUtils {

  public static final int PERSONAL_NUMBER = 0;
  public static final int PHONE_NUMBER = 1;
  public static final int PASSPORT_NUMBER = 2;
  public static final int BUSINESS_NUMBER = 3;
  public static final int ALL = 4;
  public static final String PATTERN_PERSONAL_NUMBER = "\\b([0-9]{2})"
      + "(0[1-9]|1[012])(0[1-9]|1[0-9]|2[0-9]|3[01])-?[012349][0-9]{6}\\b";
  public static final String PATTERN_PHONE_NUMBER = "\\b(01[0|1|7|8|9])"
      + "([-|\\s])?(\\d{3,4})([-|\\s])?\\d{4}\\b";
  public static final String PATTERN_PASSPORT_NUMBER = "\\b([a-zA-Z][a-zA-Z])"
      + "([ -])?(\\d{7})\\b";
  public static final String PATTERN_BUISINESS_NUMBER = "\\b(\\d{3})([-|\\s])?"
      + "(\\d{2})([-|\\s])?\\d{5}\\b";
  public static final String REPLACEMENT_PERSONAL_NUMBER = "$1$2$3*******";
  public static final String REPLACEMENT_PHONE_NUMBER = "$1$2$3$4****";
  public static final String REPLACEMENT_PASSPORT_NUMBER = "$1$2*******";
  //사업자번호 유효성 체크 필요한 수
  public static final int BIZ_NUMBER_CHK_IDS[] = {1, 3, 7, 1, 3, 7, 1, 3, 5, 1};
  public static final int BIZ_NUMBER_MASK_CNT = 5;
  public static final String MASK_CHAR = "*";

  public static String personalNumber(String original) {
    if (nullCheck(original)) {
      return original;
    }
    return original
        .replaceAll(PATTERN_PERSONAL_NUMBER, REPLACEMENT_PERSONAL_NUMBER);
  }

  public static String phoneNumber(String original) {
    if (nullCheck(original)) {
      return original;
    }
    return original.replaceAll(PATTERN_PHONE_NUMBER, REPLACEMENT_PHONE_NUMBER);
  }

  public static String businessNumber(String original) {
    if (nullCheck(original)) {
      return original;
    }
    return maskBusinessNumber(original);
  }

  public static String passportNumber(String original) {
    if (nullCheck(original)) {
      return original;
    }
    return original
        .replaceAll(PATTERN_PASSPORT_NUMBER, REPLACEMENT_PASSPORT_NUMBER);
  }

  /**
   * 사업자 등록 번호에 대한 마스킹 처리
   *
   * @param original 원본 문자열
   * @return 마스킹 처리가 된 경우 변환된 문자열
   */
  public static String maskBusinessNumber(String original) {

    Pattern p = Pattern.compile(PATTERN_BUISINESS_NUMBER);
    Matcher m = p.matcher(original);

    StringBuffer buffer = new StringBuffer();
    int end = 0;
    int insertIndex = 0;
    String subString = "";
    while (m.find()) {
      end = m.end();
      // 사업자 등록 번호 유효성 체크
      subString = original.substring(m.start(), end);
      if (!checkValidateBusinessNumber(subString)) {
        buffer.append(original.substring(insertIndex, end));
        insertIndex = buffer.length();
        continue;
      }
      buffer.append(original.substring(insertIndex,
          end - BIZ_NUMBER_MASK_CNT));
      for (int i = 0; i < BIZ_NUMBER_MASK_CNT; ++i) {
        buffer.append(MASK_CHAR);
      }
      insertIndex = buffer.length();
    }
    // macher가 있을 때 buffer에 origin이 모두 채워지지 않은 경우
    try {
      if (insertIndex > 0 && insertIndex < original.length()) {
        buffer.append(original.substring(insertIndex));
      }
    }
    catch (Exception e) {
      // do nothing
    }
    return buffer.length() > 0 ? buffer.toString() : original;
  }

  /**
   * 사업자 등록 번호의 유효성 체크
   *
   * @param subString 체크대상의 사업자 등록 번호
   * @return true : 유효, false : 무효
   */
  public static boolean checkValidateBusinessNumber(String subString) {

    subString = subString.replaceAll(" ", "");
    subString = subString.replaceAll("-", "");

    if (subString.length() < 10) {
      return false;
    }

    int summary = 0;

    for (int index = 0; index < BIZ_NUMBER_CHK_IDS.length - 2; ++index) {
      summary += (Character.getNumericValue(subString.charAt(index))
          * BIZ_NUMBER_CHK_IDS[index]);
    }

    String checkString =
        "0" + Character.getNumericValue(subString.charAt(8))
            * BIZ_NUMBER_CHK_IDS[8];

    int checkStringLength = checkString.length();
    String subCheckString = checkString.substring(checkStringLength - 2,
        checkStringLength);
    summary += Math.floor(Character.getNumericValue(subCheckString.charAt(0)))
        + Math.floor(Character.getNumericValue(subCheckString.charAt(1)));

    return Math.floor(Character.getNumericValue(subString.charAt(9)))
        == ((10 - (summary % 10)) % 10);
  }

  public static String masking(int type, String original) {
    String result = null;
    switch (type) {
      case PERSONAL_NUMBER:
        result = personalNumber(original);
        break;
      case PHONE_NUMBER:
        result = phoneNumber(original);
        break;
      case PASSPORT_NUMBER:
        result = passportNumber(original);
        break;
      case BUSINESS_NUMBER:
        result = businessNumber(original);
        break;
      case ALL:
        result = personalNumber(original);
        result = phoneNumber(result);
        result = passportNumber(result);
        result = businessNumber(result);
        break;
      default:
        result = original;
        break;
    }
    return result;
  }

  public static boolean nullCheck(String src) {
    boolean isReturn = true;
    if (src != null && !src.isEmpty()) {
      isReturn = false;
    }
    return isReturn;
  }
}
