package ai.maum.m2u.map;

import io.grpc.ServerInterceptors;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterfaceList;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.Map.PingRequest;
import maum.m2u.map.Map.PongResponse;
import maum.m2u.map.MaumToYouProxyServiceGrpc.MaumToYouProxyServiceImplBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;


/**
 * MAP Service Implementation.
 *
 * - ping() - eventStream()
 */
public class MaumToYouProxyServiceImpl extends MaumToYouProxyServiceImplBase {

  static final Logger logger = LoggerFactory.getLogger(MaumToYouProxyServiceImpl.class);
  ServerAuthInterceptor metaInterceptor = new ServerAuthInterceptor();

  /**
   * 생성자
   */
  public MaumToYouProxyServiceImpl() {
    ServerInterceptors.intercept(this.bindService(), metaInterceptor);
  }

  @Override
  public void ping(PingRequest req, StreamObserver<PongResponse> res) {
    try {
      PongResponse pong = PongResponse.newBuilder()
          .setVersion("111111")
          .build();
      res.onNext(pong);
      res.onCompleted();
    } catch (Exception e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      logger.error("ping e : " , e);
      res.onError(
          new StatusException(Status.FAILED_PRECONDITION
              .withDescription(e.getMessage())
              .withCause(e)));
    }
  }

  @Override
  public void listAsyncInterfaces(AsyncInterface req,
      StreamObserver<AsyncInterfaceList> res) {
    try {
      AsyncInterfaceList list = AsyncInterfaceList.newBuilder()
          .addInterfaces(AsyncInterface.newBuilder()
              .setStreaming(false)
              .setInterface("Echo")
              .setOperation("Echo")
              .build())
          .build();
      res.onNext(list);
      res.onCompleted();
    } catch (Exception e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      logger.error("listAsyncInterfaces e : " , e);
      res.onError(
          new StatusException(Status.FAILED_PRECONDITION
              .withDescription(e.getMessage())
              .withCause(e)));
    }
  }

  @Override
  public StreamObserver<MapEvent> eventStream(final StreamObserver<MapDirective> res) {
    return new StreamObserver<MapEvent>() {
      Dispatcher dispatcher = new Dispatcher(res);

      @Override
      public void onNext(MapEvent mapEvent) {

        // log에 operationSyncId를 기록하기 위해 MDC에 저장합니다
        if (!mapEvent.getEvent().getOperationSyncId().isEmpty()) {
          MDC.put("operationSyncId", mapEvent.getEvent().getOperationSyncId());
        }

        logger.trace("#@ eventStream onNext");
        dispatcher.handleEvent(mapEvent);
      }
      @Override
      public void onError(Throwable t) {
        Status status = Status.fromThrowable(t);
        logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
            t, Dispatcher.threadLocal.get());
      }

      @Override
      public void onCompleted() {
        logger.info("#@ eventStream onCompleted");
        dispatcher.waitEventShutdown();

        // res.onCompleted();
      }
    };
  }

  @Override
  public StreamObserver<MapEvent> getDirectives(final StreamObserver<MapDirective> res) {
    logger.info("#@ MaumToYouProxyServiceImpl getDirectives");
    return eventStream(res);
  }
}