package ai.maum.m2u.map.services.mobile;

import ai.maum.m2u.map.MaumToYouProxyServiceImpl;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import maum.m2u.map.Map;
import maum.m2u.map.Map.EventStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageServiceImpl extends Service {

  static final Logger logger = LoggerFactory
      .getLogger(MaumToYouProxyServiceImpl.class);

  private Map.EventStream callEvent = null;
  private MapEventHandler handler = null;

  public ImageServiceImpl() {
    logger.debug("#@ Create constructor of ImageServiceImpl");
  }

  /**
   * 이미지 인식 시작
   */
  @Override
  public void OnImageDocumentRecognizerRecognize(MapEventHandler handler, EventStream event) {
    logger.info("#@ IN OnImageDocumentRecognizerRecognize");

    this.callEvent = event;
    this.handler = handler;

    return;
  }

}