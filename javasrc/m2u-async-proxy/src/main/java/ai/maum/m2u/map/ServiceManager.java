package ai.maum.m2u.map;

import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import io.grpc.Status;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceManager {

  private static ServiceManager ourInstance = new ServiceManager();
  static final Logger logger = LoggerFactory.getLogger(Dispatcher.class);

  public static ServiceManager getInstance() {
    return ourInstance;
  }

  private HashMap<String, Class> handlers = new HashMap<>();


  private ServiceManager() {

  }

  /**
   * ServiceHandler를 추가한다.
   */
  public boolean register(String name, Class clazz) {
    if (MapEventHandler.class.isAssignableFrom(clazz)) {
      handlers.put(name, clazz);
    } else {

    }
    return true;
  }

  /**
   * 추가한 Servicehandler를 삭제한다.
   */
  public boolean unregister(String name) {
    handlers.remove(name);
    return true;
  }

  /**
   * Servicehandler를 검색한다.
   */
  public Service getServiceHandler(String name) {
    Class cl = handlers.get(name);
    try {
      return (Service) cl.newInstance();
    } catch (InstantiationException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());
      return null;
    } catch (IllegalAccessException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());
      return null;
    }
  }
}
