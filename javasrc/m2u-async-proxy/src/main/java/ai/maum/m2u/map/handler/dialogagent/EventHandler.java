package ai.maum.m2u.map.handler.dialogagent;

import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class EventHandler implements MapEventHandler {

  static Logger logger = LoggerFactory.getLogger(EventHandler.class);

  DirectiveForwarder forwarder = null;
  EventStream callEvent = null;
  Service service = null;

  @Override
  public String getInterface() {
    return MapIf.I_DIALOG_AGENT;
  }

  @Override
  public String getOperation() {
    return MapIf.DA_E_EVENT;
  }

  @Override
  public boolean isStream() {
    return false;
  }

  @Override
  public void setDirectiveForwarder(DirectiveForwarder forward) {
    forwarder = forward;
  }

  @Override
  public DirectiveForwarder getDirectiveForwarder() {
    return forwarder;
  }

  @Override
  public void begin(EventStream event) {
    logger.info("#@ begin EventHandler = {}", event.getStreamId());
    callEvent = event;
    logger.debug("begin payload = {}", event.getPayload());

    service.OnDialogAgentEvent(this, event);
  }

  @Override
  public void end(StreamEnd end) {
    logger.info("#@ EventHandler stream end = {}", end.getStreamId());

  }

  @Override
  public void cancel(StreamBreak cancel) {
    logger.error("EventHandler stream cancelled. {}", cancel.getStreamId());

  }

  @Override
  public void next(ByteString bytes) {
    logger.trace("next bytes, {}", bytes.size());

  }

  @Override
  public void next(String text) {
    logger.trace("oh no next text, {}", text.length());
  }

  @Override
  public void next(StreamMeta meta) {
    logger.trace("oh no next meta, {}", meta.getSerializedSize());
  }

  @Override
  public void setService(Service service) {
    this.service = service;
  }
}
