package ai.maum.m2u.map.services.web;

import static ai.maum.config.PropertyManager.getString;

import ai.maum.config.Keys;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.common.user.UserParam;
import ai.maum.m2u.map.common.utils.CallLogUtills;
import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.ExceptionMsgUtills;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.common.utils.TLOLogUtills;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import com.google.protobuf.util.Timestamps;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import maum.m2u.common.DeviceOuterClass;
import maum.m2u.common.Dialog;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException;
import maum.m2u.router.v3.Router;
import maum.m2u.router.v3.Router.CloseRouteResponse;
import maum.m2u.router.v3.Router.TalkRouteResponse;
import maum.m2u.router.v3.TalkRouterGrpc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class DialogWebServiceImpl extends Service {

  private static final int EX_INDEX = 900;

  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  private static final Logger logger = LoggerFactory.getLogger(DialogWebServiceImpl.class);

  public DialogWebServiceImpl() {
    logger.debug("#@ Create constructor of DialogWebServiceImpl");
  }

  @Override
  public void OnDialogOpen(MapEventHandler handler, EventStream event) {
    logger.debug("#@ IN WEB OnDialogOpen");

    UserParam userParam = DialogUtils.createUserParam(event);
    Struct.Builder meta = event.getPayload().toBuilder();
    // open시에 payload에 chatbot 정보 확인
    String chatbot = meta.getFieldsOrDefault("chatbot", Value.newBuilder().build())
        .getStringValue();

    Router.OpenRouteRequest param = Router.OpenRouteRequest.newBuilder()
        .setUtter(DialogUtils.makeOpenUtter(event, handler))
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get())
        )
        .setChatbot(chatbot)
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc
        .newStub(channel);

    logger.debug("#@ WEB Router open = {}", param.toString());
    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(event);
    String tempOpSyncIdVal = event.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .openRoute(param, new StreamObserver<TalkRouteResponse>() {
          TalkRouteResponse ret = null;

          @Override
          public void onNext(TalkRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ openRouterStub onNext = {}", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            // CallLog 호출
            CallLogUtills.makeOpenRouterCallLog(event, tempOpSyncIdMeta, param, userParam, null
                , reqDate, new Date());
            ExceptionMsgUtills.sendExceptionFromRouter(handler, t, param, reqDate, event
                , exIndex(1), MapException.StatusCode.GRPC_ROUTER_OPEN_ERROR,
                "Router error occurred");
            logger.trace("#@ dialog web send exception {}",
                MapException.StatusCode.GRPC_ROUTER_OPEN_ERROR);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ openRouterStub onCompleted");

            // openRouter CallLog 호출
            CallLogUtills.makeOpenRouterCallLog(event, tempOpSyncIdMeta, param, userParam, ret
                , reqDate, new Date());
            TLOLogUtills.makeTLOLog(param, reqDate, new Date(), 200, exIndex(2));
            DirectiveStream dir = DirectiveStream.newBuilder()
                .setInterface(AsyncInterface.newBuilder()
                    .setInterface(MapIf.I_VIEW)
                    .setOperation(MapIf.VW_D_RENDER_SPEECH)
                    .setType(OperationType.OP_DIRECTIVE)
                    .build())
                .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
                .setStreamId(UUID.randomUUID().toString())
                .setOperationSyncId(event.getOperationSyncId())
                .setPayload(DialogUtils.messageToStruct(ret))
                .build();

            handler.getDirectiveForwarder().sendStream(dir);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  @Override
  public void OnDialogClose(MapEventHandler handler, EventStream event) {
    logger.debug("#@ IN WEB OnDialogClose");

    UserParam userParam = DialogUtils.createUserParam(event);
    Router.CloseRouteRequest param = Router.CloseRouteRequest.newBuilder()
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get()))
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc
        .newStub(channel);

    logger.debug("#@ WEB Router closeRouterStub = {}", param.toString());
    Metadata operationSyncId = GrpcUtills.makeOperationSyncIdHeader(event);
    String tempOpSyncIdVal = event.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, operationSyncId);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .closeRoute(param, new StreamObserver<CloseRouteResponse>() {
          CloseRouteResponse ret = null;

          @Override
          public void onNext(CloseRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ WEB closeRouterStub onNext = {}", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            CallLogUtills
                .makeCloseRouterCallLog(event, operationSyncId, param, userParam, null, reqDate,
                    new Date());

            ExceptionMsgUtills.sendExceptionFromRouter(
                handler, t, param, reqDate, event, exIndex(3),
                MapException.StatusCode.GRPC_ROUTER_CLOSE_ERROR,
                "Close Router error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ WEB closeRouterStub onCompleted.");

            CallLogUtills.makeCloseRouterCallLog(event, operationSyncId, param, userParam, ret
                , reqDate, new Date());
            TLOLogUtills.makeTLOLog(param, reqDate, new Date(), 200, exIndex(4));
            DirectiveStream dir = DirectiveStream.newBuilder()
                .setInterface(AsyncInterface.newBuilder()
                    .setInterface(MapIf.I_VIEW)
                    .setOperation(MapIf.VW_D_RENDER_CLOSE)
                    .setType(OperationType.OP_DIRECTIVE)
                    .build())
                .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
                .setStreamId(UUID.randomUUID().toString())
                .setOperationSyncId(event.getOperationSyncId())
                .setPayload(DialogUtils.messageToStruct(ret))
                .build();

            handler.getDirectiveForwarder().sendStream(dir);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  @Override
  public void OnDialogTextToTextTalk(MapEventHandler handler, EventStream event) {
    logger.debug("#@ IN WEB OnDialogTextToTextTalk");

    UserParam userParam = DialogUtils.createUserParam(event);
    Router.TalkRouteRequest request = Router.TalkRouteRequest.newBuilder()
        .setUtter(DialogUtils.makeTalkUtter(event, handler))
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get())
        )
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc.newStub(channel);

    logger.debug("#@ WEB Router talkRoute \n [{}]", request.toString());
    Metadata operationSyncId = GrpcUtills.makeOperationSyncIdHeader(event);
    String tempOpSyncIdVal = event.getOperationSyncId();

    stub = MetadataUtils.attachHeaders(stub, operationSyncId);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .talkRoute(request, new StreamObserver<TalkRouteResponse>() {
          TalkRouteResponse ret = null;

          @Override
          public void onNext(TalkRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ talkRouterStub onNext [{}]", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            CallLogUtills.makeTalkRouterCallLog(event, operationSyncId, request
                , userParam, null, reqDate, new Date());
            ExceptionMsgUtills.sendExceptionFromRouter(handler, t, request, reqDate, event
                , exIndex(5), MapException.StatusCode.GRPC_ROUTER_TALK_ERROR,
                "Talk Route error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ talkRouterStub onCompleted");

            CallLogUtills.makeTalkRouterCallLog(event, operationSyncId, request
                , userParam, ret, reqDate, new Date());
            TLOLogUtills.makeTLOLog(request, reqDate, new Date(), 200, exIndex(6));
            DirectiveStream dir = DirectiveStream.newBuilder()
                .setInterface(AsyncInterface.newBuilder()
                    .setInterface(MapIf.I_VIEW)
                    .setOperation(MapIf.VW_D_RENDER_TEXT)
                    .setType(OperationType.OP_DIRECTIVE)
                    .build())
                .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
                .setStreamId(UUID.randomUUID().toString())
                .setOperationSyncId(event.getOperationSyncId())
                .setPayload(DialogUtils.messageToStruct(ret))
                .build();

            handler.getDirectiveForwarder().sendStream(dir);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }
}
