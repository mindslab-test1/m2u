package ai.maum.m2u.map.services.mobile;

import static ai.maum.config.PropertyManager.getString;
import static ai.maum.m2u.map.util.MapIf.E_SPEECH;
import static ai.maum.m2u.map.util.MapIf.E_TEXT;

import ai.maum.config.Keys;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.common.user.UserParam;
import ai.maum.m2u.map.common.utils.CallLogUtills;
import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.ExceptionMsgUtills;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.common.utils.TLOLogUtills;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import maum.brain.idr.Idr.ImageRecognitionParam;
import maum.brain.idr.Idr.ImageRecognitionResult;
import maum.brain.stt.Speech;
import maum.brain.stt.Speech.StreamingRecognizeResponse;
import maum.m2u.common.DeviceOuterClass;
import maum.m2u.common.Dialog;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException;
import maum.m2u.router.v3.Router;
import maum.m2u.router.v3.Router.CloseRouteResponse;
import maum.m2u.router.v3.Router.TalkRouteResponse;
import maum.m2u.router.v3.TalkRouterGrpc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class DialogServiceImpl extends Service {

  private static final int EX_INDEX = 800;

  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  static final Logger logger = LoggerFactory.getLogger(DialogServiceImpl.class);

  public DialogServiceImpl() {
    logger.debug("#@ Create constructor of DialogAgentServiceImpl");
  }

  @Override
  public void OnDialogOpen(MapEventHandler handler, EventStream event) {
    logger.info("#@ IN OnDialogOpen");

    this.openRouterStub(DialogUtils.createUserParam(event), event, handler);
  }

  private void openRouterStub(UserParam userParam, EventStream callEvent, MapEventHandler handler) {
    logger.info("#@ openRouterStub ");

    handler.getDirectiveForwarder().sendStream(DialogUtils.makeDialogProcessing(callEvent));
    Struct.Builder meta = callEvent.getPayload().toBuilder();
    // open시에 payload에 chatbot 정보 확인
    String tempChatbot = meta.getFieldsOrDefault("chatbot", Value.newBuilder().build())
        .getStringValue();

    logger.debug("#@ openRouterStub getChabot : {}", tempChatbot);

    Router.OpenRouteRequest param = Router.OpenRouteRequest.newBuilder()
        .setUtter(DialogUtils.makeOpenUtter(callEvent, handler))
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get())
        )
        .setChatbot(tempChatbot)
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc.newStub(channel);

    logger.trace("#@ Router open \n [{}]", param);

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();
    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .openRoute(param, new StreamObserver<TalkRouteResponse>() {
          TalkRouteResponse ret = null;

          @Override
          public void onNext(TalkRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ openRouterStub onNext [{}]", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            // transaction log
            CallLogUtills
                .makeOpenRouterCallLog(callEvent, tempOpSyncIdMeta, param, userParam, null, reqDate,
                    new Date());

            ExceptionMsgUtills.sendExceptionFromRouter(
                handler, t, param, reqDate, callEvent, exIndex(1),
                MapException.StatusCode.GRPC_ROUTER_OPEN_ERROR,
                "Router error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            CallLogUtills
                .makeOpenRouterCallLog(callEvent, tempOpSyncIdMeta, param, userParam, ret, reqDate,
                    new Date());
            TLOLogUtills.makeTLOLog(param, reqDate, new Date(), 200, exIndex(2));
            DialogUtils.processTalkRouterResponse(handler, ret, userParam, E_SPEECH, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  @Override
  public void OnDialogTextToTextTalk(MapEventHandler handler, EventStream event) {
    logger.info("#@ IN OnDialogTextToTextTalk");
    DialogUtils.talkRouterStub(DialogUtils.createUserParam(event), E_TEXT, event, handler);
  }

  @Override
  public void OnDialogTextToSpeechTalk(MapEventHandler handler, EventStream event) {
    logger.info("#@ IN OnDialogTextToSpeechTalk");
    DialogUtils.talkRouterStub(DialogUtils.createUserParam(event), E_SPEECH, event, handler);
  }

  @Override
  public void OnDialogSpeechToSpeechTalk(MapEventHandler handler,
      EventStream event, Speech.SpeechRecognitionParam param) {
    logger.info("#@ IN OnDialogSpeechToSpeechTalk");
  }

  @Override
  public void OnDialogSpeechToTextTalk(MapEventHandler handler, EventStream event,
      Speech.SpeechRecognitionParam param) {
    logger.info("#@ IN OnDialogSpeechToTextTalk");
  }


  @Override
  public void OnSpeechRecognizerRecognizing(MapEventHandler handler,
      StreamingRecognizeResponse streamingRes, EventStream callEvent) {
    logger.info("#@ IN OnSpeechRecognizerRecognizing");

    handler.getDirectiveForwarder()
        .sendStream(DialogUtils.makeSTTRecognizing(streamingRes, callEvent));
  }

  @Override
  public void OnSpeechRecognizerRecognized(MapEventHandler handler, String resType,
      EventStream callEvent, StreamingRecognizeResponse streamingRecognizeResponse,
      String transcript, int langVal) {
    logger.info("#@ IN OnSpeechRecognizerRecognized");

    handler.getDirectiveForwarder()
        .sendStream(DialogUtils.makeSTTRecognized(streamingRecognizeResponse, callEvent));

    DialogUtils.talkRouterStubForStt(DialogUtils.createUserParam(callEvent)
        , transcript
        , resType
        , langVal
        , callEvent
        , handler);
  }

  @Override
  public void OnDialogClose(MapEventHandler handler, EventStream event) {
    logger.info("#@ IN OnDialogClose");

    this.closeRouterStub(DialogUtils.createUserParam(event), event, handler);
  }

  @Override
  public void OnDialogImageToTextTalk(MapEventHandler handler, EventStream event,
      ImageRecognitionParam imageRecognitionParam) {
    logger.info("#@ IN OnDialogImageToTextTalk");
  }

  @Override
  public void OnImageRecognizerRecognized(MapEventHandler handler, ImageRecognitionResult imageRes,
      String resType, EventStream callEvent) {
    logger.info("#@ START OnImageRecognizerRecognized");

    handler.getDirectiveForwarder().sendStream(
        DialogUtils.makeImageRecognizerRecognized(imageRes, callEvent));
    DialogUtils
        .talkRouterStubForIdr(DialogUtils.createUserParam(callEvent), imageRes, resType, callEvent,
            handler);
  }


  private void closeRouterStub(UserParam userParam, EventStream callEvent,
      MapEventHandler handler) {
    logger.info("#@ closeRouterStub ");

    Router.CloseRouteRequest param = Router.CloseRouteRequest.newBuilder()
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get()))
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc
        .newStub(channel);

    logger.trace("#@ Router close  = \n [{}]", param.toString());
    Metadata operationSyncId = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();
    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, operationSyncId);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .closeRoute(param, new StreamObserver<CloseRouteResponse>() {
          CloseRouteResponse ret = null;

          @Override
          public void onNext(CloseRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ closeRouterStub onNext [{}]", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            CallLogUtills
                .makeCloseRouterCallLog(callEvent, operationSyncId, param, userParam, null, reqDate,
                    new Date());

            ExceptionMsgUtills.sendExceptionFromRouter(
                handler, t, param, reqDate, callEvent, exIndex(3),
                MapException.StatusCode.GRPC_ROUTER_CLOSE_ERROR,
                "Close Route error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ closeRouterStub notifyCompleted.");

            CallLogUtills
                .makeCloseRouterCallLog(callEvent, operationSyncId, param, userParam, ret, reqDate,
                    new Date());
            TLOLogUtills.makeTLOLog(param, reqDate, new Date(), 200, exIndex(4));
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }
}
