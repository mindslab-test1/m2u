package ai.maum.m2u.map.common.utils;

import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.JsonFormat.TypeRegistry;
import io.grpc.Status;
import java.util.Date;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException;
import maum.m2u.map.Map.MapException.StatusCode;
import maum.rpc.Status.ResultStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionMsgUtills {

  static final Logger logger = LoggerFactory.getLogger(ExceptionMsgUtills.class);

  /**
   * Router SendException 정리 1. ResultStatus로 부터 resStatusCode, resStatusMsg, tempPayloadJson 추출한다
   * 2. errorIndex는 9로 시작하고 나머지 2자리를 조사한다 9xxpp 3. MapException/StatusCode와 일치하는 값을 갖고 온다 4. 나머지 값을
   * 넣어준다
   */
  public static StatusCode getRouterStatusCode(ResultStatus tempRsRouter, StatusCode defaultCode) {

    StatusCode resStatusCode = defaultCode;

    String tempErrIndex = String.valueOf(tempRsRouter.getErrorIndex());

    // 맨앞자리가 9로 시작하는지 확인 (아니라면 기본 GRPC 에러처리)
    String firstNum = tempErrIndex.substring(0, 1);
    if (firstNum.equalsIgnoreCase("9")) {
      // 9xx : xx 가 무엇인지 확인
      String nextNum = tempErrIndex.substring(1, 3);

      switch (nextNum) {
        case "01":
          logger.debug("Set StatusCode : ROUTER_SESSION_NOT_FOUND");
          resStatusCode = MapException.StatusCode.ROUTER_SESSION_NOT_FOUND;
          break;
        case "02":
          logger.debug("Set StatusCode : ROUTER_SESSION_INVALID");
          resStatusCode = MapException.StatusCode.ROUTER_SESSION_INVALID;
          break;
        case "03":
          logger.debug("Set StatusCode : ROUTER_CHATBOT_NOT_FOUND");
          resStatusCode = MapException.StatusCode.ROUTER_CHATBOT_NOT_FOUND;
          break;
        case "04":
          logger.debug("Set StatusCode : ROUTER_DA_NOT_FOUND");
          resStatusCode = MapException.StatusCode.ROUTER_DA_NOT_FOUND;
          break;
        case "05":
          logger.debug("Set StatusCode : ROUTER_DA_ERROR");
          resStatusCode = MapException.StatusCode.ROUTER_DA_ERROR;
          break;
        case "06":
          logger.debug("Set StatusCode : ROUTER_ITF_NOT_FOUND");
          resStatusCode = MapException.StatusCode.ROUTER_ITF_NOT_FOUND;
          break;
        case "07":
          logger.debug("Set StatusCode : ROUTER_ITF_ERROR");
          resStatusCode = MapException.StatusCode.ROUTER_ITF_ERROR;
          break;
        default:
          logger.debug("Set StatusCode : defaultCode");
          resStatusCode = defaultCode;
      }
    } else {
      // (아니라면 기본 GRPC 에러처리)
      logger.debug("Set StatusCode : defaultCode");
      resStatusCode = defaultCode;
    }

    return resStatusCode;
  }

  /**
   * 라우터로 부터 받은 onError Throwable 정보를 ai.maum.rpc.ResultStatus에 맞게 가공하여 sendException에 실어 보냅니다
   * makeTLOLog 생성도 포함합니다
   */
  public static void sendExceptionFromRouter(
      MapEventHandler mapEventHandler,
      Throwable t,
      Object param,
      Date reqDate,
      EventStream callEvent,
      int exIndex,
      StatusCode defaultCode,
      String defaultStatusMsg) {

    // 1.ResultStatus 객체 생성
    ai.maum.rpc.ResultStatus rs = ResultStatusListProto.fromThrowable(t);
    Status status = Status.fromThrowable(t);
    // sendException에 json payload 데이터 추출
    String tempPayloadJson = "";
    StatusCode resStatusCode = null;
    String resStatusMsg = "";

    if (rs != null) {
      try {
        tempPayloadJson = JsonFormat.printer()
            .usingTypeRegistry(TypeRegistry.newBuilder()
                .add(maum.rpc.ErrorDetails.DebugInfo.getDescriptor())
                .add(maum.rpc.ErrorDetails.LocalizedMessage.getDescriptor())
                .build())
            .includingDefaultValueFields()
            .preservingProtoFieldNames()
            .print(rs.getResultStatusList());
      } catch (InvalidProtocolBufferException e) {
        Status bufferExceptionStatus = Status.fromThrowable(e);
        logger.error("error status {}:{},{}", bufferExceptionStatus.getCode(),
            bufferExceptionStatus.getDescription(),
            e, Dispatcher.threadLocal.get());
      }

      ResultStatus tempRsRouter = rs.getResultStatusList().getList(0); // 0번째가 Router로 부터 받은 정보

      // 2.resStatusCode 추출
      resStatusCode = getRouterStatusCode(tempRsRouter, defaultCode);
      logger.debug("#@ resStatusCode [{}]", resStatusCode);
      // 3.resStatusMsg 추출
      resStatusMsg = tempRsRouter.getMessage();
      logger.debug("#@ resStatusMsg [{}]", resStatusMsg);

      logger.error("error status {}:{}, Throwable {}, result {}",
          status.getCode(),
          status.getDescription(),
          t,
          rs.getResultStatusList(),
          Dispatcher.threadLocal.get());

    } else {
      resStatusCode = defaultCode;
      resStatusMsg = defaultStatusMsg;

      logger.error("error status {}:{}, Throwable {}",
          status.getCode(),
          status.getDescription(),
          t,
          Dispatcher.threadLocal.get());
    }

    TLOLogUtills.makeTLOLog(param, reqDate, new Date(), resStatusCode.getNumber(), exIndex);

    mapEventHandler.getDirectiveForwarder()
        .sendException(
            resStatusCode, exIndex, resStatusMsg, callEvent, tempPayloadJson);

    mapEventHandler.getDirectiveForwarder().notifyCompleted(mapEventHandler);
  }
}
