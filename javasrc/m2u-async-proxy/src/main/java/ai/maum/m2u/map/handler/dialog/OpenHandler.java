package ai.maum.m2u.map.handler.dialog;

import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import io.grpc.Channel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.stt.SttRealServiceGrpc.SttRealServiceStub;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpenHandler implements MapEventHandler {
  static Logger logger = LoggerFactory.getLogger(OpenHandler.class);

  DirectiveForwarder forwarder = null;
  SttRealServiceStub sttRealStub = null;
  EventStream callEvent = null;
  //  StreamObserver<Stt.Speech> sttSender = null;
  Service service = null;

  @Override
  public String getInterface() {
    return MapIf.I_DIALOG;
  }

  @Override
  public String getOperation() {
    return MapIf.DL_E_OPEN;
  }

  @Override
  public boolean isStream() {
    return false;
  }

  @Override
  public void setDirectiveForwarder(DirectiveForwarder forward) {
    forwarder = forward;
  }

  @Override
  public DirectiveForwarder getDirectiveForwarder() {
    return forwarder;
  }

  @Override
  public void begin(EventStream event) {
    logger.info("#@ begin OpenHandler = {}", event.getStreamId());
    callEvent = event;
    service.OnDialogOpen(this, event);
  }

  @Override
  public void end(StreamEnd end) {
    logger.info("#@ OpenHandler end = {}", end.getStreamId());
  }

  @Override
  public void cancel(StreamBreak cancel) {
    logger.error("OpenHandler stream cancelled = {}", cancel.getStreamId());

  }

  @Override
  public void next(ByteString bytes) {
    logger.trace("next bytes = {}", bytes.size());

  }

  @Override
  public void next(String text) {
    logger.trace("oh no next text = {}", text.length());
  }

  @Override
  public void next(StreamMeta meta) {
    logger.trace("oh no next meta = {}", meta.getSerializedSize());
  }

  @Override
  public void setService(Service service) {
    this.service = service;
  }
}
