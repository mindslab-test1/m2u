package ai.maum.m2u.map.hzc;

import ai.maum.config.Keys;
import ai.maum.m2u.common.portable.UserInfoPortable;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NameSpaceInitializer {

  static final Logger logger = LoggerFactory.getLogger(NameSpaceInitializer.class);

  public static void initializing() {

    logger.info("===== NameSpaceInitializer.initializing");

    HazelcastInstance client = HazelcastConnector.getInstance().client;

    IMap<String, UserInfoPortable> userInfoPortableIMap = client.getMap(Keys.MEM_USER_INFO);

    int userInfoCnt = userInfoPortableIMap.size();

    logger.info("===== Check Map Size ===============");
    logger.info("  userInfoCnt = {}", userInfoCnt);
    logger.info("====================================");
  }
}
