package ai.maum.m2u.map;

import io.grpc.ServerInterceptors;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import maum.m2u.map.MapSecureForwarderGrpc.MapSecureForwarderImplBase;
import maum.m2u.map.SecureForward.CipherBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * SecureMap Proxy Service Implementation.
 *
 * - ping() - eventStream()
 */
public class SecureMapProxyServiceImpl extends MapSecureForwarderImplBase {

  static final Logger logger = LoggerFactory.getLogger(SecureMapProxyServiceImpl.class);

  // SecureMapInterceptor 객체 생성
  SecureMapInterceptor secureMapInterceptor;

  /**
   * SecureMapProxyServiceImpl 생성자 ServerInterceptors에 secureMapInterceptor 설정
   */
  public SecureMapProxyServiceImpl() {
    secureMapInterceptor = new SecureMapInterceptor();
    ServerInterceptors.intercept(this.bindService(), secureMapInterceptor);
  }

  @Override
  public void ping(CipherBody request, StreamObserver<CipherBody> responseObserver) {
    super.ping(request, responseObserver);
  }

  @Override
  public void listAsyncInterfaces(CipherBody request, StreamObserver<CipherBody> responseObserver) {
    super.listAsyncInterfaces(request, responseObserver);
  }

  @Override
  public StreamObserver<CipherBody> eventStream(StreamObserver<CipherBody> responseObserver) {
    return new StreamObserver<CipherBody>() {

      //secureMapDispatcher 객체 생성
      SecureMapDispatcher secureMapDispatcher = new SecureMapDispatcher(responseObserver);

      @Override
      public void onNext(CipherBody cipherBody) {
        secureMapDispatcher.handleEvent(cipherBody);
      }

      @Override
      public void onError(Throwable throwable) {
        logger.error("#@ IN Forwarder to Secure eventStream onError{} => ", Status.fromThrowable(throwable), throwable);
      }

      @Override
      public void onCompleted() {
        secureMapDispatcher.secureToMapOnCompleted();
      }
    };
  }

  @Override
  public StreamObserver<CipherBody> getDirectives(StreamObserver<CipherBody> responseObserver) {
    return super.getDirectives(responseObserver);
  }



}