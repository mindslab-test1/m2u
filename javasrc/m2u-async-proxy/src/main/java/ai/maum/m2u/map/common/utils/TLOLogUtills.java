package ai.maum.m2u.map.common.utils;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.MarkerFactory;

public class TLOLogUtills {

  static final Logger logger = LoggerFactory.getLogger(TLOLogUtills.class);

  public static void makeTLOLog(Object request, Date reqDate, Date resDate, int resultCode, int exIndex) {
    logger.info("#@ START makeTLOLog");

    MDC.put("reqDate", Long.toString(reqDate.getTime()));
    MDC.put("resDate", Long.toString(resDate.getTime()));
    MDC.put("resultCode", "" + resultCode + exIndex);

    logger.info(MarkerFactory.getMarker("TLOLog"), "", request);

    // MDC를 clear하면 operationSyncId가 사라지기 때문에 변수에 넣어둔다
    String tempOperationSyncId = MDC.get("operationSyncId");
    MDC.clear();
    MDC.put("operationSyncId", tempOperationSyncId);
  }
}
