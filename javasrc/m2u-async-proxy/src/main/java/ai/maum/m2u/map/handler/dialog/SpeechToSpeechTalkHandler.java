package ai.maum.m2u.map.handler.dialog;

import static ai.maum.config.PropertyManager.getString;
import static com.google.protobuf.TextFormat.printToUnicodeString;

import ai.maum.config.Keys;
import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import com.google.protobuf.Duration;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Struct;
import com.google.protobuf.util.JsonFormat;
import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.Status.Code;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import maum.brain.stt.Speech;
import maum.brain.stt.Speech.SpeechContext;
import maum.brain.stt.Speech.SpeechRecognitionParam;
import maum.brain.stt.Speech.SpeechRecognitionResult;
import maum.brain.stt.Speech.StreamingRecognizeRequest;
import maum.brain.stt.Speech.StreamingRecognizeResponse;
import maum.brain.stt.Speech.StreamingRecognizeResponse.SpeechEventType;
import maum.brain.stt.Speech.WordInfo;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.SpeechToTextServiceGrpc.SpeechToTextServiceStub;
import maum.brain.stt.Stt;
import maum.brain.stt.Stt.Model;
import maum.brain.stt.Stt.ServerStatus;
import maum.brain.stt.SttModelResolverGrpc;
import maum.brain.stt.SttModelResolverGrpc.SttModelResolverStub;
import maum.common.LangOuterClass.LangCode;
import maum.m2u.common.UserOuterClass.User;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException.StatusCode;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;


@SuppressWarnings("DuplicatedCode")
public class SpeechToSpeechTalkHandler implements MapEventHandler {

  private static final int EX_INDEX = 400;

  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  private static final int[] readyDeadlines = {250, 250, 500, 2000, -1};

  private Logger logger = LoggerFactory.getLogger(SpeechToSpeechTalkHandler.class);

  private Service service;
  private DirectiveForwarder forwarder;
  private EventStream callEvent;
  private StreamObserver<Stt.Speech> sttSender;
  private final Object syncSttSender = new Object();
  private SpeechRecognitionParam speechRecParam;
  private boolean sttFinished = false;
  // FixMe: 이전 방식의 STT proto 대응 flag, 코드만 유지되고 실제 동작은 확인되지 않음.
  private StreamObserver<StreamingRecognizeRequest> sttLegacySender;
  private boolean isLegacy = false;
  private boolean paramFlag = false;


  @Override
  public String getInterface() {
    return MapIf.I_DIALOG;
  }

  @Override
  public String getOperation() {
    return MapIf.DL_E_SPEECH_TO_SPEECH_TALK;
  }


  @Override
  public boolean isStream() {
    return true;
  }

  @Override
  public void setDirectiveForwarder(DirectiveForwarder forward) {
    forwarder = forward;
  }

  @Override
  public DirectiveForwarder getDirectiveForwarder() {
    return forwarder;
  }

  @Override
  public void begin(EventStream event) {
    logger.info("#@ begin SpeechToSpeechTalkHandler = {}", event.getStreamId());

    // speech recognition param 이 없다면
    if (!event.getParam().hasSpeechRecognitionParam()) {
      forwarder.sendException(StatusCode.MAP_NO_STREAM_PARAM, exIndex(1),
          "No parameter for STS", event, null);
      return;
    }
    callEvent = event;
    if (connectSttRealStub(event.getParam().getSpeechRecognitionParam(), event)) {
      service.OnDialogSpeechToSpeechTalk(this, event, speechRecParam);
    }
  }

  @Override
  public void end(StreamEnd end) {
    logger.info("SpeechToSpeechTalkHandler stream end. = {}", end.getStreamId());
  }

  @Override
  public void cancel(StreamBreak cancel) {
    logger.debug("SpeechToSpeechTalkHandler stream cancelled. {}", cancel.getStreamId());

    if (isLegacy) {
      sttLegacySender.onError(Status.CANCELLED.asException());
    } else {
      sttSender.onError(Status.CANCELLED.asException());
    }

    forwarder.notifyCompleted(this);
  }

  @Override
  public void next(ByteString bytes) {
    logger.debug("#@ next bytes [{}]", bytes.size());

    synchronized (syncSttSender) {
      if (sttFinished) {
        logger.trace("#@ next STT is finished. discard bytes.");
        return;
      }

      if (!isLegacy) {
        if (sttSender == null) {
          logger.trace("#@ next sttSender is not ready. discard bytes.");
          return;
        }
        sttSender.onNext(Stt.Speech.newBuilder().setBin(bytes).build());
        return;
      }

      // FixMe: 이전 방식(Legacy) STT 방식, 동작하지 않을 수도 있음.
      if (sttLegacySender == null) {
        logger.trace("#@ next sttLegacySender is not ready. discard bytes.");
        return;
      }

      StreamingRecognizeRequest speech = StreamingRecognizeRequest.newBuilder()
          .setAudioContent(bytes).build();

      //////////////////////////////////////////////
      // ByteStream을 보내기 전에 SpeechParam을 전송합니다.
      // 최초 한번만 전달합니다
      if (paramFlag) {
        Struct tempMeta = Dispatcher.threadLocalUser.get().getMeta();
        logger.debug("#@ getMeta's result is [{}]", printToUnicodeString(tempMeta));

        // metaInfo가 있는 경우 전달합니다
        List<String> metaInfo = DialogUtils.getUserMetaInfo(tempMeta);
        if (metaInfo != null) {
          speechRecParam = SpeechRecognitionParam.newBuilder()
              .mergeFrom(speechRecParam)
              .addSpeechContexts(SpeechContext.newBuilder().addAllPhrases(metaInfo)).build();
        }
        logger.trace("#@ speechRecParam is [{}]", printToUnicodeString(speechRecParam));

        try {
          logger.debug("#@ START STS Send StreamingRecognizeRequest");
          StreamingRecognizeRequest speechRec = StreamingRecognizeRequest.newBuilder()
              .setSpeechParam(speechRecParam).build();

          sttLegacySender.onNext(speechRec);
          logger.debug("#@ End STS Send StreamingRecognizeRequest");
          paramFlag = false;
        } catch (Exception e) {
          logger.error("#@ StreamingRecognizeRequest STS error occurred.", e);
        }
      }
      //////////////////////////////////////////////

      sttLegacySender.onNext(speech);
    }
  }

  @Override
  public void next(String text) {
    logger.trace("oh no next text [{}]", text);
  }

  @Override
  public void next(StreamMeta meta) {
    logger.trace("oh no next meta [{}]", printToUnicodeString(meta));
  }

  @Override
  public void setService(Service service) {
    this.service = service;
  }

  private void StreamingRecognize(final String realSttRemote
      , final EventStream event
      , final MapEventHandler eventHandler
      , final Map<String, String> mdcContext
      , final String operationSyncId
      , final User user) throws IllegalStateException {
    logger.info("#@ StreamingRecognize realSttRemote [{}]", realSttRemote);

    final ManagedChannel sttRealStubChannel = DialogUtils.getChannel(realSttRemote);
    SpeechToTextServiceStub sttStub = SpeechToTextServiceGrpc.newStub(sttRealStubChannel);
    sttStub = MetadataUtils.attachHeaders(sttStub, GrpcUtills.makeOperationSyncIdHeader(event));

    for (int i : readyDeadlines) {
      if (sttRealStubChannel.getState(true) == ConnectivityState.READY) {
        break;
      }
      if (i < 0) {
        throw new IllegalStateException();
      }
      try {
        TimeUnit.MILLISECONDS.sleep(i);
      } catch (InterruptedException e) {
        logger.debug("", e);
      }
    }

    final int langValue = speechRecParam.getLangValue();

    sttLegacySender = sttStub
        .withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .streamingRecognize(new StreamObserver<Speech.StreamingRecognizeResponse>() {
          @Override
          public void onNext(StreamingRecognizeResponse recognizedRes) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);
            Dispatcher.threadLocalUser.set(user);

            logger.debug("#@ streamingRecognize type : {}", recognizedRes.getSpeechEventType());

            synchronized (syncSttSender) {
              if (sttFinished) {
                logger.trace("#@ streamingRecognize STT Finished. Discard Recognized Response.");
                return;
              }

              String transcript = "";
              List<SpeechRecognitionResult> resultList = recognizedRes.getResultsList();
              if (resultList.size() > 0) {
                for (SpeechRecognitionResult result : resultList) {
                  logger.debug("#@ transcript [{}] , final [{}]"
                      , result.getTranscript()
                      , result.getFinal());
                  if (logger.isTraceEnabled()) {
                    for (WordInfo word : result.getWordsList()) {
                      logger.trace("#@ word [{}]", word.getWord());
                    }
                  }
                  transcript = result.getTranscript();
                }
              }

              // #@ Recognizing & Recognized 전송
              service.OnSpeechRecognizerRecognizing(eventHandler, recognizedRes, event);
              service.OnSpeechRecognizerRecognized(eventHandler, MapIf.E_SPEECH,
                  event, recognizedRes, transcript, langValue);
              sttLegacySender.onCompleted();
              sttFinished = true;
            }
          }

          @Override
          public void onError(Throwable throwable) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);

            Status status = Status.fromThrowable(throwable);
            String msg = MessageFormat.format("STT error occurred. {0}:{1}",
                status.getCode(), status.getDescription());
            // 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

            logger.error("{}", msg);
            logger.error("Thread local {}", Dispatcher.threadLocal.get());
            logger.error("", throwable);

            forwarder.sendException(StatusCode.GRPC_STT_ERROR, exIndex(2),
                msg, event, tempPayloadJson);
            GrpcUtills.closeChannel(sttRealStubChannel);
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);
            Dispatcher.threadLocalUser.set(user);

            logger.info("#@ STT Completed.");
            GrpcUtills.closeChannel(sttRealStubChannel);
          }
        });
  }

  private boolean StreamRecognize(final String realSttRemote, final EventStream event) {
    logger.info("#@ StreamRecognize realSttRemote [{}]", realSttRemote);

    final ManagedChannel sttStubChannel = DialogUtils.getChannel(realSttRemote);
    SpeechToTextServiceStub sttStub = SpeechToTextServiceGrpc.newStub(sttStubChannel);
    // Stub에 Header 추가
    {
      Metadata headers = GrpcUtills.makeOperationSyncIdHeader(event);
      Metadata.Key<String> model = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> lang = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> sampleRate = Metadata.Key.of("in.samplerate"
          , Metadata.ASCII_STRING_MARSHALLER);
      headers.put(model, speechRecParam.getModel());
      LangCode langCode = LangCode.forNumber(speechRecParam.getLangValue());
      if (langCode == null) {
        langCode = LangCode.kor;
      }
      headers.put(lang, langCode.name());
      headers.put(sampleRate, Integer.toString(speechRecParam.getSampleRate()));
      sttStub = MetadataUtils.attachHeaders(sttStub, headers);
    }

    for (int i : readyDeadlines) {
      if (sttStubChannel.getState(true) == ConnectivityState.READY) {
        break;
      }
      if (i < 0) {
        Status status = Status
            .fromCode(Code.UNAVAILABLE)
            .withDescription("Model '" + speechRecParam.getModel() + "' Server is not ready.");
        String msg = MessageFormat.format("STT error occurred. {0}:{1}",
            status.getCode(), status.getDescription());
        // 에러코드를 말아서 그대로 json string으로 전달한다
        String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

        logger.error("{}", msg);
        logger.error("Thread local {}", Dispatcher.threadLocal.get());

        forwarder.sendException(StatusCode.GRPC_STT_ERROR, exIndex(6),
            msg, event, tempPayloadJson);
        return false;
      }
      try {
        TimeUnit.MILLISECONDS.sleep(i);
      } catch (InterruptedException e) {
        logger.debug("", e);
      }
    }

    final MapEventHandler eventHandler = this;
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();
    final String operationSyncId = event.getOperationSyncId();
    final User user = Dispatcher.threadLocalUser.get();

    sttSender = sttStub
        .withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .streamRecognize(new StreamObserver<Stt.Segment>() {
          private StreamingRecognizeResponse.Builder responseBuilder =
              StreamingRecognizeResponse.newBuilder();
          private String transcript = "";

          @Override
          public void onNext(Stt.Segment segment) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);
            Dispatcher.threadLocalUser.set(user);

            transcript = segment.getTxt();
            logger.debug("#@ StreamRecognize segment : {}-{} {}"
                , segment.getStart()
                , segment.getEnd()
                , transcript);

            synchronized (syncSttSender) {
              if (sttFinished) {
                logger.trace("#@ StreamRecognize STT Finished. Discard segment.");
                return;
              }
              responseBuilder
                  .setSpeechEventType(SpeechEventType.END_OF_SINGLE_UTTERANCE)
                  .addResults(SpeechRecognitionResult.newBuilder()
                      .setTranscript(transcript)
                      .setFinal(true)
                      .addWords(WordInfo.newBuilder()
                          .setStartTime(Duration.newBuilder().setSeconds(segment.getStart()))
                          .setEndTime(Duration.newBuilder().setSeconds(segment.getEnd()))
                          .setWord(transcript)))
                  .build();

              // #@ Recognizing & Recognized 전송
              service.OnSpeechRecognizerRecognizing(eventHandler, responseBuilder.build(), event);
              sttSender.onCompleted();
              sttFinished = true;
            }
          }

          @Override
          public void onError(Throwable throwable) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);

            Status status = Status.fromThrowable(throwable);
            String msg = MessageFormat.format("STT error occurred. {0}:{1}",
                status.getCode(), status.getDescription());
            // 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

            logger.error("{}", msg);
            logger.error("Thread local {}", Dispatcher.threadLocal.get());
            logger.error("", throwable);

            forwarder.sendException(StatusCode.GRPC_STT_ERROR, exIndex(3),
                msg, event, tempPayloadJson);
            GrpcUtills.closeChannel(sttStubChannel);
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);
            Dispatcher.threadLocalUser.set(user);

            logger.info("#@ StreamRecognize Completed.");
            GrpcUtills.closeChannel(sttStubChannel);
            service.OnSpeechRecognizerRecognized(eventHandler, MapIf.E_SPEECH,
                event, responseBuilder.build(), transcript, speechRecParam.getLangValue());
          }
        });

    return true;
  }

  private boolean connectSttRealStub(SpeechRecognitionParam param, EventStream event) {
    String sttRemote = getString(Keys.P_STS_SERVER);

    if (sttRemote == null || sttRemote.isEmpty()) {
      final String msg = "STT Server setting missing.";
      logger.error("{}", msg);
      forwarder.sendException(StatusCode.GRPC_STT_ERROR
          , exIndex(4)
          , msg
          , event
          , "{'exception': '" + msg + "'}");
      return false;
    }

    final String sttAddr = sttRemote.substring(0, sttRemote.indexOf(":"));
    String sttProtocolType = getString(Keys.P_STS_PROTOCOL_TYPE);
    if (sttProtocolType == null) {
      sttProtocolType = "latest";
    }
    isLegacy = sttProtocolType.equalsIgnoreCase("legacy");
    paramFlag = isLegacy;

    String sttModel = getString(Keys.P_STS_MODEL);
    Speech.SpeechRecognitionParam.Builder paramBuilder = param.toBuilder();
    if (sttModel != null && !sttModel.isEmpty()) {
      paramBuilder.setModel(sttModel);
    }
    speechRecParam = paramBuilder.build();

    logger.debug("Connect to STT Server [{}/{}/{}]", sttRemote, sttModel, param.getLang());
    logger.trace("Param Model [{}]", param.getModel());

    if (!isLegacy) {
      return StreamRecognize(sttRemote, event);
    }

    // FixMe: 이전 방식(Legacy) STT 방식, 동작하지 않을 수도 있음.
    final MapEventHandler eventHandler = this;
    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();
    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(event);
    String operationSyncId = event.getOperationSyncId();
    User tempUser = Dispatcher.threadLocalUser.get();

    final ManagedChannel sttResolverStubChannel = DialogUtils.getChannel(sttRemote);
    SttModelResolverStub sttResolverStub = SttModelResolverGrpc.newStub(sttResolverStubChannel);
    final Model model = Model.newBuilder()
        .setModel(sttModel)
        .setLangValue(param.getLang().getNumber())
        .setSampleRate(param.getSampleRate())
        .build();

    try {
      logger.trace("#@ sttResolverStub.find Model info = {}"
          , JsonFormat.printer().includingDefaultValueFields().print(model));
    } catch (InvalidProtocolBufferException e) {
      logger.trace("", e);
      logger.trace("#@ sttResolverStub.find Model info = {}", model);
    }

    // 헤더에 operationSyncId 설정
    sttResolverStub = MetadataUtils.attachHeaders(sttResolverStub, tempOpSyncIdMeta);
    sttResolverStub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .find(model, new StreamObserver<ServerStatus>() {
          ServerStatus resolved;

          @Override
          public void onNext(ServerStatus ss) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);
            Dispatcher.threadLocalUser.set(tempUser);
            logger.trace("STT ServerStatus onNext = {}", ss);
            resolved = ss;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);

            Status status = Status.fromThrowable(t);
            String msg = MessageFormat.format("STT error occurred. {0}:{1}",
                status.getCode(), status.getDescription());
            // 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

            logger.error("{}", msg);
            logger.error("Thread local {}", Dispatcher.threadLocal.get());
            logger.error("", t);

            forwarder.sendException(StatusCode.GRPC_STT_ERROR, exIndex(5),
                msg, event, tempPayloadJson);

            GrpcUtills.closeChannel(sttResolverStubChannel);
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(operationSyncId);
            Dispatcher.threadLocalUser.set(tempUser);
            logger.info("#@ ServerStatus onCompleted");

            String addr = resolved.getServerAddress();
            String port = addr.substring(addr.indexOf(':') + 1);
            String realSttRemote = sttAddr + ':' + port;
            logger.info("#@ Find completed and try connect = {}", realSttRemote);

            GrpcUtills.closeChannel(sttResolverStubChannel);

            try {
              StreamingRecognize(realSttRemote
                  , event
                  , eventHandler
                  , mdcContext
                  , operationSyncId
                  , tempUser);
            } catch (IllegalStateException e) {
              Status status = Status
                  .fromCode(Code.UNAVAILABLE)
                  .withDescription("Model '" + model.getModel() + "' Server is not ready.");
              String msg = MessageFormat.format("STT error occurred. {0}:{1}",
                  status.getCode(), status.getDescription());
              // 에러코드를 말아서 그대로 json string으로 전달한다
              String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

              logger.error("{}", msg);
              logger.error("Thread local {}", Dispatcher.threadLocal.get());

              forwarder.sendException(StatusCode.GRPC_STT_ERROR, exIndex(6),
                  msg, event, tempPayloadJson);
            }
          }
        });

    return true;
  }
}
