package ai.maum.m2u.map;

import static com.google.protobuf.TextFormat.printToUnicodeString;
import static maum.m2u.map.Map.MapException.StatusCode.MAP_EVENT_CASE_NOT_SET;

import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.common.utils.JsonUtills;
import ai.maum.m2u.map.dao.UserInfoDao;
import ai.maum.m2u.map.handler.common.EventHandlerPool;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.services.common.AuthServiceImpl;
import ai.maum.m2u.map.services.dialogagent.DialogAgentServiceImpl;
import ai.maum.m2u.map.services.mobile.AudioPlayerServiceImpl;
import ai.maum.m2u.map.services.mobile.DialogServiceImpl;
import ai.maum.m2u.map.services.mobile.ImageServiceImpl;
import ai.maum.m2u.map.services.mobile.LauncherServiceImpl;
import ai.maum.m2u.map.services.mobile.MicServiceImpl;
import ai.maum.m2u.map.services.web.DialogWebServiceImpl;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Struct;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import maum.m2u.common.UserOuterClass.User;
import maum.m2u.map.Authentication;
import maum.m2u.map.Authentication.GetUserInfoRequest;
import maum.m2u.map.Authentication.GetUserInfoResponse;
import maum.m2u.map.Authentication.IsValidRequest;
import maum.m2u.map.Authentication.IsValidResponse;
import maum.m2u.map.AuthorizationProviderGrpc;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.Map.MapEvent.TestEventCase;
import maum.m2u.map.Map.MapException;
import maum.m2u.map.Map.MapException.StatusCode;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 클라이언트의 `eventStream` 반복된 eventStream을 처리할 수 있는 구조를 가지고 있다.
 * <p> 연결된 클라이언트 장치와 사용자에 대한 정보는 채널로서 유지가 된다.
 * 인증되지 않은 사용자나 장치에 대해서도 처리할 수 있는 구조를 가지고 있다.
 * <p>
 * 이 Dispatcher는 반복되는 이벤트 요청을 받아서 처리한다.
 * <p> 동시에하나의 스트림만을 처리할 수 있다. 스트림 처리 중간에 다른 이벤트를 처리할 수 있다.
 * 더이상 이벤트가 오지 않는 경우에는 기존에 요청한 스트림에 대해서 응답이 올때까지 대기해야 한다.
 *
 * <p> TODO: 지나가는 이벤트와 스트림을 모두 관리해야할까요? `expectation`에 대해서만 관리를 해야할까요?
 */
public class Dispatcher implements DirectiveForwarder {

  private final Logger logger = LoggerFactory.getLogger(Dispatcher.class);

  /**
   * `MAP Exception Index`를 처리하기 위한 고정 변수
   */
  private static final int EX_INDEX = 100;

  /**
   * 예외 인덱스를 반환합니다.
   *
   * @param index 유일한 인덱스는 이 코드내부에서 유지되어야 합니다.
   * @return 내부에서 쓰는 유일한 인덱스 값 반환
   */
  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  private static final HashMap<String, Class> serviceMap;

  static {
    serviceMap = new HashMap<>();
    serviceMap.put(MapIf.I_DIALOG.toUpperCase(), DialogServiceImpl.class);
    serviceMap.put(MapIf.I_DIALOG_WEB.toUpperCase(), DialogWebServiceImpl.class);
    serviceMap.put(MapIf.I_DIALOG_AGENT.toUpperCase(), DialogAgentServiceImpl.class);
    serviceMap.put(MapIf.I_AUTHENTICATION.toUpperCase(), AuthServiceImpl.class);
    serviceMap.put(MapIf.I_LAUNCHER.toUpperCase(), LauncherServiceImpl.class);
    serviceMap.put(MapIf.I_SPEECH_SYNTHESIZER.toUpperCase(), LauncherServiceImpl.class);
    serviceMap.put(MapIf.I_IMAGE_DOCUMENT_RECOGNIZER.toUpperCase(), ImageServiceImpl.class);
    serviceMap.put(MapIf.I_MICROPHONE.toUpperCase(), MicServiceImpl.class);
    serviceMap.put(MapIf.I_AUDIO_PLAYER.toUpperCase(), AudioPlayerServiceImpl.class);
  }

  private Set<String> activeDirectives = new HashSet<>();
  private Set<MapEventHandler> handlers = new HashSet<>();

  private String lastStreamId = null;
  private MapEventHandler lastStreamHandler = null;
  private final Object responseSyncObj = new Object();
  private StreamObserver<MapDirective> responseObserver;
  private EventHandlerPool pool = EventHandlerPool.getInstance();
  private CountDownLatch latch = null;

  private Service service = null;
  public static InheritableThreadLocal<String> threadLocal;
  public static InheritableThreadLocal<User> threadLocalUser;

  /**
   * 유량제어를 위해서 사용되는 값, 별도의 쓰레드에서 유량제어 상황이 되면 이 값을 변환한다.
   * <p>
   * TODO: 외부 함수로 변경해야 한다.
   */
  static private boolean servicePossible = true;

  static void setServicePossible(boolean possible) {
    servicePossible = possible;
  }

  // userInfoDao 객체는 최초 한번만 생성합니다
  // 추후 userInfoDao 는 세션마다 공유하여 사용합니다
  // userInfoDao 는 hzc안에 사용자 정보 등록, 조회 기능을 제공합니다
  private static UserInfoDao userInfoDao = new UserInfoDao();

  private boolean authChecked; // false as default

  /**
   * {@link StreamObserver<MapDirective>}를 지정하여 내보내는 출력을 지정한다.
   *
   * @param res 출력을 위한 @{link StreamObserver}를 지정하도록 한다.
   */
  public Dispatcher(StreamObserver<MapDirective> res) {
    if (threadLocal == null) {
      threadLocal = new InheritableThreadLocal<>();
    }
    if (threadLocalUser == null) {
      threadLocalUser = new InheritableThreadLocal<>();
    }

    authChecked = false;
    responseObserver = res;
  }

  /**
   * 현재 처리하고 있는 스트림이 있는지 확인합니다.
   *
   * @return 현재 처리하고 있는 것이 스트림이면 `true`를 반환합니다.
   */
  private boolean hasStream() {
    return lastStreamId != null;
  }

  /**
   * 마지막에 사용 중인 스트림을 내보내도록 한다.
   *
   * @param id 스트림의 ID
   * @param handler 스트림을 처리하는 핸들러
   */
  private void setLastStream(String id, MapEventHandler handler) {
    lastStreamId = id;
    lastStreamHandler = handler;
  }

  /**
   * 마지막에 사용중인 스트림을 지우도록 한다.
   * <p>입력에 대한 처리하는 참조를 지우는 것이고 출력 쪽에서 참조하는 것은 모두 남아 있습니다.
   */
  private void clearLastStream() {
    lastStreamId = null;
    lastStreamHandler = null;
  }

  /**
   * 클라이언트에 Directive를 전달한다.
   */
  private void responseOnNext(MapDirective directive) {
    synchronized (responseSyncObj) {
      responseObserver.onNext(directive);
    }
  }

  /**
   * Event 호출을 완료한다.
   */
  private void responseOnCompleted() {
    synchronized (responseSyncObj) {
      responseObserver.onCompleted();
    }
  }

  /**
   * 출력을 위해서 사용중인 핸들러들을 모두 삭제합니다.
   *
   * @param h 지울 핸들러
   */
  private void clearHandler(MapEventHandler h) {
    if (handlers.remove(h)) {
      if (latch != null) {
        logger.trace("Class = {}, latch countDown = {}", h.getClass(), latch.getCount());
        latch.countDown();
//        logger.trace("Class = {}, latch countDown = {}", h.getClass(), latch.getCount());
      }
    }
  }

  public void notifyCompleted(MapEventHandler handler) {
    if (handler != null) {
      logger.debug("NOTIFY COMPLETED !! Class = {}, Code = {}", handler.getClass(),
          handler.hashCode());
      clearHandler(handler);
    }
  }

  public void
  sendStream(DirectiveStream stream) {
    if (logger.isTraceEnabled()) {
      logger.trace("sendStream DirectiveStream = {}", printToUnicodeString(stream));
    } else {
      logger.debug("sendStream DirectiveStream = {}", printToUnicodeString(stream.getInterface()));
    }
    MapDirective dir = MapDirective.newBuilder()
        .setDirective(stream)
        .build();
    if (stream.getInterface().getStreaming()) {
      activeDirectives.add(stream.getStreamId());
      logger.debug("total active directives count = {}", activeDirectives.size());
    }
    responseOnNext(dir);
  }

  public void sendStream(StreamEnd end) {
    logger.debug("sendStream StreamEnd = {}", printToUnicodeString(end));
    MapDirective dir = MapDirective.newBuilder()
        .setStreamEnd(end)
        .build();

    activeDirectives.remove(end.getStreamId());
    logger.debug("total active directives count = {}", activeDirectives.size());
    responseOnNext(dir);
  }

  public void sendStream(StreamBreak cancel) {
    logger.debug("sendStream StreamBreak = {}", printToUnicodeString(cancel));
    MapDirective dir = MapDirective.newBuilder()
        .setStreamBreak(cancel)
        .build();
    activeDirectives.remove(cancel.getStreamId());
    logger.debug("total active directives count = {}", activeDirectives.size());
    responseOnNext(dir);
  }

  public void sendStream(ByteString bytes) {
    logger.trace("sendStream ByteString = {}", bytes.toString());
    MapDirective dir = MapDirective.newBuilder()
        .setBytes(bytes)
        .build();
    responseOnNext(dir);
  }

  public void sendStream(String text) {
    logger.trace("sendStream String = {}", text);
    MapDirective dir = MapDirective.newBuilder()
        .setText(text)
        .build();
    responseOnNext(dir);
  }

  public void sendStream(StreamMeta meta) {
    logger.debug("sendStream StreamMeta = {}", printToUnicodeString(meta));
    MapDirective dir = MapDirective.newBuilder()
        .setMeta(meta)
        .build();
    responseOnNext(dir);
  }


  /**
   * 예외를 전송합니다.
   *
   * @param code 상태 코드
   * @param exIndex 예외 발생한 위치를 식별할 수 있는 정보
   * @param exMessage 예외 메시지
   * @param refEvent 관련 EventStream
   * @param json 페이로드 데이터
   */
  public void sendException(StatusCode code,
      int exIndex,
      String exMessage,
      EventStream refEvent,
      String json) {
    logger.debug("#@ sendException code = {}, exIndex = {}, exMessage = {}, json = {}",
        code, exIndex, exMessage, json);

    MapException.Builder exb = MapException.newBuilder();

    exb.setStatusCode(code)
        .setExceptionId(UUID.randomUUID().toString())
        .setThrownAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setExIndex(exIndex)
        .setExMessage(exMessage);

    if (null != refEvent) {
      exb.setStreamId(refEvent.getStreamId())
          .setOperationSyncId(refEvent.getOperationSyncId())
          .setCalledInterface(refEvent.getInterface());
    }

    if (json != null && json.length() > 0) {
      Struct.Builder stb = Struct.newBuilder();
      try {
        JsonFormat.parser().ignoringUnknownFields().merge(json, stb);
        exb.setPayload(stb.build());
      } catch (InvalidProtocolBufferException e) {
        Status status = Status.fromThrowable(e);
        logger.error("error status {}:{}", status.getCode(), status.getDescription());
        logger.error("Thread local {}", Dispatcher.threadLocal.get());
        logger.error("", e);
      }
    }
    exb.build();

    MapDirective dir = MapDirective.newBuilder()
        .setException(exb.build())
        .build();
    responseOnNext(dir);
  }

  private void sendNotFound(EventStream str) {
    String msg = MessageFormat.format("Interface not found [{0}/{1}]",
        str.getInterface().getInterface(),
        str.getInterface().getOperation());
    // 요청하는 AsyncInterface가 존재하지 않는 경우
    sendException(StatusCode.MAP_IF_NOT_FOUND, exIndex(1),
        msg, str, null);
  }

  private void handleEventStream(EventStream eventStream) {
    logger.debug("#@ handleEventStream [{}/{}]",
        eventStream.getInterface().getInterface(), eventStream.getInterface().getOperation());
    logger.trace("#@ EventStream = {}", printToUnicodeString(eventStream));
    // TODO 필수 입력 필드에 대한 점검
    // check hasInterface
    /*
    if (!eventStream.hasInterface()) {
      throw new
    }
    */

    AsyncInterface aif = eventStream.getInterface();
    MapEventHandler eventHandler = pool.getMapEventHandler(aif);
    if (eventHandler == null) {
      logger.debug("#@ sendNotFound");
      sendNotFound(eventStream);
      return;
    }

    logger.debug("#@ hasStream() = {}, aif.getStreaming() = {}", hasStream(), aif.getStreaming());
    if (hasStream() && (aif.getStreaming())) {
      // Streaming 처리 중에 또다른 요청이 들어온 경우
      String msg = MessageFormat.format(
          "Currently streaming {0} is processing, new streaming requested {1}/{2}/{3}",
          lastStreamId,
          eventStream.getStreamId(), aif.getInterface(), aif.getOperation());
      sendException(StatusCode.MAP_IF_DUPLICATED_STREAMING, exIndex(2),
          msg, eventStream, null);
    }

    // / 사직하기 전에 모든 처리되어야할 Event에 대해서 기록해준다.
    boolean ret = handlers.add(eventHandler);
    if (!ret) {
      logger.warn("cannot add handler = {} / {}, {}",
          eventHandler.getInterface(),
          eventHandler.getOperation(),
          Dispatcher.threadLocal.get());
    }

    // 로컬 쓰레드 스토리지에 operationSyncId 저장
    Dispatcher.threadLocal.set(eventStream.getOperationSyncId());

    // #@ Service 객체 생성
    logger.debug("#@ Create Service");
    String asyncInterface = eventStream.getInterface().getInterface().toUpperCase();
    String errorMessage = "";
    if (serviceMap.containsKey(asyncInterface)) {
      try {
        service = (Service) serviceMap.get(asyncInterface).newInstance();
      } catch (InstantiationException | IllegalAccessException e) {
        errorMessage = MessageFormat
            .format("Instantiation failed for Interface [{}].", asyncInterface);
        logger.error(errorMessage, e);
      }
    } else {
      errorMessage = MessageFormat.format("Interface [{}] is not found.", asyncInterface);
    }
    if (!errorMessage.isEmpty()) {
      sendException(StatusCode.MAP_IF_NOT_FOUND, exIndex(3), errorMessage, eventStream, null);
    }
    // 새로 시작하는 `EventHandler`에 `StreamObserver`를 등록한다.
    eventHandler.setDirectiveForwarder(this);
    eventHandler.setService(service);

    if (!servicePossible) { // CANNOT PROCESS
      // 기준 Session 수를 초과할 경우 해당 코드에서 Exception을 발생시키고 onComplete를 호출합니다
      // 해당 코드를 타게 되면 더이상의 Session은 증가되지 않습니다.
      // 즉 더이상의 hzc session수가 증가하지 않으며, onComplete 호출로 인해 정상적인 종료를 합니다
      // TODO, max last count 값을 출력해주면 좋겠네요.
      logger.warn("#@ currently service disabled {}, {}",
          SessionManager.getInstance().getThreshold(),
          SessionManager.getInstance().getLastSessionCount());

      // hzc 총 세션수가 기준범위를 초과한 경우
      sendException(StatusCode.MAP_TOTAL_SESSION_COUNT_EXCEEDED, exIndex(4),
          "The number of sessions has been exceeded.", eventStream, null);
    } else {
      // 시작한다.
      eventHandler.begin(eventStream);
    }

    if (aif.getStreaming()) {
      setLastStream(eventStream.getStreamId(), eventHandler);
    }
  }

  private void endEventStream(StreamEnd end) {
    logger.debug("endEventStream StreamEnd = {}", end.getStreamId());
    if (!hasStream()) {
      sendException(StatusCode.MAP_CURRENTLY_HAS_NO_STREAM, exIndex(5),
          "not in stream", null, null);
    }
    // check valid streamend,
    if (end.getStreamId().equals(lastStreamId)) {
      // STT 입력이라면 종료를 처리해야 한다.
      lastStreamHandler.end(end);
      clearLastStream();
    } else {
      // 현재 진행중인 StreamId와 일치하지 않는 경우
      sendException(StatusCode.MAP_STREAM_ID_NOT_MATCH, exIndex(6),
          "invalid stream end for break", null, null);
    }
  }

  private void cancelEventStream(StreamBreak cancel) {
    logger.debug("cancelEventStream StreamBreak = {}", cancel.getStreamId());
    if (!hasStream()) {
      sendException(StatusCode.MAP_CURRENTLY_HAS_NO_STREAM, exIndex(7),
          "Currently there is no stream id.", null, null);
    }
    if (cancel.getStreamId().equals(lastStreamId)) {
      lastStreamHandler.cancel(cancel);
      clearLastStream();
    } else {
      // 현재 진행중인 StreamId와 일치하지 않는 경우
      sendException(StatusCode.MAP_STREAM_ID_NOT_MATCH, exIndex(8),
          "invalid stream id for cancel", null, null);
    }
  }

  private void feedStream(ByteString bytes) {
    logger.trace("feedStream ByteString = {}", bytes.size());
    if (!hasStream()) {
      sendException(StatusCode.MAP_CURRENTLY_HAS_NO_STREAM, exIndex(9),
          "Currently there is no stream id.", null, null);
    }
    lastStreamHandler.next(bytes);
  }

  private void feedStream(String text) {
    logger.trace("feedStream String = {}", text);
    if (!hasStream()) {
      sendException(StatusCode.MAP_CURRENTLY_HAS_NO_STREAM, exIndex(10),
          "Currently there is no stream id.", null, null);
    }
    lastStreamHandler.next(text);
  }

  private void feedStream(StreamMeta meta) {
    logger.trace("feedStream StreamMeta ");
    if (!hasStream()) {
      sendException(StatusCode.MAP_CURRENTLY_HAS_NO_STREAM, exIndex(11),
          "Currently there is no stream id.", null, null);
    }
    lastStreamHandler.next(meta);
  }

  /**
   * `Directive`가 전송된 이후에 예외가 발생하여 응답으로 예외가 전송되면 이를 기록한다.
   *
   * @param ex Directive에 대한 예외 처리
   */
  private void handleException(MapException ex) {
    // 예외를 전달해야하는 경우는 AsyncInterface 에서 정의하는 것이 아니라
    // 각 HANDLER 에서 정의하도록 한다.
    logger.debug(ex.toString());
  }

  /**
   * 클라이언트에서 더 이상 MapEvent가 들어오지 않기 때문에 응답을 종료한다.
   */
  public void waitEventShutdown() {
    if (handlers.size() > 0) {
      logger.debug("waitEventShutdown handlers.size() = {}", handlers.size());
      latch = new CountDownLatch(handlers.size());
      try {
        // TODO 좀 더 멋있게 처리하는 방법은 없을까요?
        latch.await();
        responseOnCompleted();
        logger.debug("waitEventShutdown COMPLETED = {}", handlers.size());
      } catch (InterruptedException e) {
        Status status = Status.fromThrowable(e);

        logger.error("error status {}:{}", status.getCode(), status.getDescription());
        logger.error("Thread local {}", Dispatcher.threadLocal.get());
        logger.error("", e);

        logger.debug("waitEventShutdown ERROR COMPLETED = {}", handlers.size());
        responseOnCompleted();
      }
    } else {
      // authChecked가 실패한 경우는 handlers가 없으므로 예외처리를 합니다
      if (authChecked) {
        // 이미 응답을 했으므로 여기에서도 종료를 답해줍니다
        logger.debug("waitEventShutdown handlers.size() ZERO = {}", handlers.size());
        responseOnCompleted();
      } else {
        logger.debug("authChecked is false. can't waitEventShutdown");
      }
    }
  }


  /**
   * 클라이언트에서 요청한 이벤트를 분석하여 기존의 스트림 처리자에게 넘겨서 처리하도록 한다.
   *
   * @param event 처리해야할 Event
   */
  public void handleEvent(MapEvent event) {
    TestEventCase eventCase = event.getTestEventCase();
    logger.trace("[MapEvent] = {}, case = {}", event.getSerializedSize(), eventCase.toString());
    switch (eventCase) {
      case EVENT:
        // 하나의 요청에서 isValid를 통과한 경우
        if (authChecked) {
          handleEventStream(event.getEvent());
        } else {
          checkAuth(event.getEvent());

          // checkAuth 진행후 authChecked 값에따라 진행 또는 stop
          if (authChecked) {
            handleEventStream(event.getEvent());
          } else {
            // sendException은 checkAuth 에서 이미 진행하였기 때문에 auth실패시 스트림만 닫아준다.
            responseOnCompleted();
          }
        }
        break;
      case STREAM_END:
        endEventStream(event.getStreamEnd());
        break;
      case STREAM_BREAK:
        cancelEventStream(event.getStreamBreak());
        break;
      case BYTES:
        feedStream(event.getBytes());
        break;
      case TEXT:
        feedStream(event.getText());
        break;
      case META:
        feedStream(event.getMeta());
        break;
      case EXCEPTION:
        //
        // 응답으로 오는 예외에 대한 호출 인터페이스를 찾아내고
        // 이를 호출하도록 대기하는 경우가 있는 경우에는 예외를 처리하도록 한다.
        //
        handleException(event.getException());
        break;
      case TESTEVENT_NOT_SET:
        sendException(MAP_EVENT_CASE_NOT_SET, exIndex(12), "Event case not set", null, null);
        break;
      default:
        String msg = MessageFormat.format("Event case [{}} is invalid", eventCase);
        sendException(MAP_EVENT_CASE_NOT_SET, exIndex(13), msg, null, null);
        break;
    }
  }

  private void checkAuth(EventStream eventStream) {
    // EventStream일때만 인증 프로세스
    logger.debug("ServerAuthInterceptor.HAS_AUTH_TOKEN_HEADER.get() : {}",
        ServerAuthInterceptor.HAS_AUTH_TOKEN_HEADER.get());
    logger.debug("ServerAuthInterceptor.AUTH_TOKEN_VALUE.get() : {}",
        ServerAuthInterceptor.AUTH_TOKEN_VALUE.get());
    logger.debug("ServerAuthInterceptor.HAS_SIGN_IN_HEADER.get() : {}",
        ServerAuthInterceptor.HAS_SIGN_IN_HEADER.get());

    try {
      if (ServerAuthInterceptor.HAS_AUTH_TOKEN_HEADER.get() &&
          !"".equals(ServerAuthInterceptor.AUTH_TOKEN_VALUE.get())) {

        String authToken = ServerAuthInterceptor.AUTH_TOKEN_VALUE.get();
        IsValidResponse isValidResponse = callIsValid(authToken,
            eventStream.getOperationSyncId());

        // 인증 true
        if (isValidResponse.getIsValid()) {
          String accessToken = isValidResponse.getAccessToken();

          User user;
          // hzc에 사용자 정보가 이미있을경우
          if (userInfoDao.exist(accessToken)) {
            JSONObject volitileUserMeta = new JSONObject(
                JsonFormat.printer()
                    .print(isValidResponse.getVolitileUserMeta().toBuilder()));

            User.Builder hzcInfo = userInfoDao.getUserInfo(accessToken);
            user = hzcInfo.build();
            logger.trace("#@ isValid true. Current userInfo [{}]", printToUnicodeString(user));

            // 변경된 사용자 정보가 있을 경우 기존 hzc사용자 정보와 merge
            if (volitileUserMeta.length() > 0) {
              // hzc 사용자 정보
              Struct meta = hzcInfo.getMeta();

              JSONObject source = new JSONObject(JsonFormat.printer().print(
                  isValidResponse.getVolitileUserMeta().toBuilder()));
              logger.trace("#@ source meta [{}]", source.toString());
              JSONObject target = new JSONObject(JsonFormat.printer().
                  includingDefaultValueFields().print(meta.toBuilder()));
              logger.trace("#@ target meta (prev) [{}]", target.toString());
              JSONObject newJSONObject = JsonUtills.deepMerge(source, target);
              logger.trace("#@ merged new object [{}]", newJSONObject.toString());
              hzcInfo.setMeta(JsonUtills.jsonObjectToStruct(newJSONObject));

              user = hzcInfo.build();
              logger.trace("#@ updated userInfo result [{}]", printToUnicodeString(user));
              userInfoDao.insertUserInfo(accessToken, user, isValidResponse.getExpiredAt());
            }
            // hzc에 사용자 정보가 없을 경우 auth server에 사용자 정보 요청 후 put
          } else {
            GetUserInfoResponse authServerInfo = getUserInfo(accessToken,
                eventStream.getOperationSyncId());
            userInfoDao.insertUserInfo(accessToken, authServerInfo.getUser(),
                isValidResponse.getExpiredAt());
            user = authServerInfo.getUser();
            logger.debug("#@ There is no userInfoDao. So try getUserInfo result [{}]",
                printToUnicodeString(user));
          }

          logger.debug("#@ threadLocalUser userInfo [{}]", printToUnicodeString(user));
          Dispatcher.threadLocalUser.set(user);
          authChecked = true;
        } else {
          // isValid 결과 false인 경우(토큰만료)
          sendException(MapException.StatusCode.AUTH_INVALID_AUTH_TOKEN, exIndex(14),
              "Token has expired. Please retry verification.", eventStream, null);
          authChecked = false;
        }
      } else if (ServerAuthInterceptor.HAS_SIGN_IN_HEADER.get()) {
        authChecked = true;
      } else {
        // header값이 충분하지 않을경우 error
        sendException(MapException.StatusCode.AUTH_INVALID_HEADER, exIndex(15),
            "Invalid authentication header value.", eventStream, null);
        authChecked = false;
      }
    } catch (Exception e) {
      Status st = Status.fromThrowable(e);
      String msg = MessageFormat.format("checkAuth error occurred. {0}:{1}",
          st.getCode(), st.getDescription());
      sendException(StatusCode.AUTH_CHECK_AUTH_FAILED, exIndex(16), msg, eventStream, null);
      authChecked = false;
    }
  }

  /**
   * 인증토큰의 정상여부 체크
   *
   * @param authToken (sign-in service resutn token)
   * @param operationSyncId 인증 처리를 위한 OP ID
   * @return IsValidResponse 인증 토큰이 정상적인지를 나타냅니다.
   *
   * ACCESS TOKEN을 새로이 발급합니다.
   * <p>서버의 관점에서 이 토큰은 이미 존재하는 데이터일 가능성이 매우 높습니다.
   * <p>이 토큰을 기반으로 상세한 사용자 정보를 획득할 수 있습니다.
   * <code>string access_token = 2;</code>
   *
   * <p>이 ACCESS TOKEN이 말료되는 시점을 알려줍니다.
   * <code>google.protobuf.Timestamp expired_at = 3;</code>
   *
   * 이미 존재하는 데이터이더라도 늘상 업데이트가 필요한 데이터는 여기에 실어서 넣을 수 있도록 처리한다.
   * <p>기본적으로 M2U MAP은 사용자 정보를 캐쉬하도록 처리하고 있다.
   * 불가피하게 사용자 정보를 캐쉬할수 없는 조건일 경우에 아래의 GetUserInfo 요청을 통해서
   * <p>모든 정보를 모든 정보를 갱신하지 않고 새로운 정보를 갱신하도록 한다.
   * 만일 Auth Server 지금 발급한 access token이 전혀 새로운 것이라면 어차피 MAP은 아래의 GetUserInfo를 통해서
   * <p>새로운 정보를 가져와야 하므로 여기에 어떤 정보도 채워 보내지 않아도 된다.
   * <code>google.protobuf.Struct volitile_user_meta = 4;</code>
   */
  private IsValidResponse callIsValid(String authToken, String operationSyncId) {
    logger.info("#@ interceptor callIsValid = {}", authToken);
    IsValidRequest param;
    IsValidResponse ret = null;

    logger.debug("#@ callIsValid Server = {}", ServerAuthInterceptor.AUTH_SERVER_IP.get());

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());
    AuthorizationProviderGrpc.AuthorizationProviderBlockingStub stub = AuthorizationProviderGrpc
        .newBlockingStub(channel);

    Metadata fixedHeaders = new Metadata();
    Metadata.Key<String> key = Metadata.Key
        .of("x-operation-sync-id", Metadata.ASCII_STRING_MARSHALLER);
    fixedHeaders.put(key, operationSyncId);

    param = Authentication.IsValidRequest.newBuilder()
        .setAuthToken(authToken)
        .build();

    stub = MetadataUtils.attachHeaders(stub, fixedHeaders);

    logger.trace("#@ callIsValid isValidAuthStub = {}", param.toString());

    try {
      ret = stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS).isValid(param);
      logger.trace("#@ callIsValid return = {}", printToUnicodeString(ret));
    } catch (Throwable t) {
      // isValid의 Exception을 예외처리 합니다
      Status status = Status.fromThrowable(t);
      String msg = MessageFormat.format("isValid error occurred. {0}:{1}",
          status.getCode(), status.getDescription());

      logger.error("{}", msg);
      logger.error("Thread local {}", Dispatcher.threadLocal.get());
      logger.error("", t);

      // 인증서버로 부터 수신받은 Exception msg를 전달합니다
      sendException(StatusCode.AUTH_IS_VAILD_FAILED, exIndex(17),
          msg, null, null);
    }

    //grpc RuntimeException 예외 처리
    if (channel != null) {
      GrpcUtills.closeChannel(channel);
    }

    return ret;
  }

  /**
   * 사용자 정보를 반환합니다.
   *
   * @param accessToken authToken(sign-in service resutn token)
   * @param operationSyncId 요청 ID
   * @return : message GetUserInfoResponse { maum.m2u.common.User user = 1; }
   */
  private GetUserInfoResponse getUserInfo(String accessToken, String operationSyncId) {
    logger.info("#@ interceptor getUserInfo = {}", accessToken);
    GetUserInfoRequest param;
    GetUserInfoResponse ret;

    logger.debug("#@ getUserInfo Server = {}", ServerAuthInterceptor.AUTH_SERVER_IP.get());

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());
    AuthorizationProviderGrpc.AuthorizationProviderBlockingStub stub = AuthorizationProviderGrpc
        .newBlockingStub(channel);

    Metadata fixedHeaders = new Metadata();
    Metadata.Key<String> key = Metadata.Key
        .of("x-operation-sync-id", Metadata.ASCII_STRING_MARSHALLER);
    fixedHeaders.put(key, operationSyncId);

    param = GetUserInfoRequest.newBuilder()
        .setAccessToken(accessToken)
        .build();

    stub = MetadataUtils
        .attachHeaders(stub, fixedHeaders);

    logger.info("#@ getUserInfo getUserInfoStub  = {}", param.toString());

    ret = stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS).getUserInfo(param);

    //grpc RuntimeException 예외 처리
    if (channel != null) {
      GrpcUtills.closeChannel(channel);
    }

    return ret;
  }
}
