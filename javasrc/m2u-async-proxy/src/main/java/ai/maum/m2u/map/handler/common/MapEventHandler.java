package ai.maum.m2u.map.handler.common;

import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.services.Service;
import com.google.protobuf.ByteString;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;

public interface MapEventHandler {

  /**
   * 인터페이스 이름을 반환한다.
   *
   * @return 인터페이스 이름
   */
  public String getInterface();

  /**
   * 오퍼레이션 이름을 반환한다.
   */
  public String getOperation();

  /**
   * 스트림 여부를 반환한다.
   *
   * @return 스트림 여부
   */
  public boolean isStream();

  /**
   * 응답에 대한 처리를 넘길 수 있는 StreamObserver를 지정한다.
   *
   * @param forward 디렉티브를 상대방에 넘겨주도록 한다.
   */
  public void setDirectiveForwarder(DirectiveForwarder forward);


  public DirectiveForwarder getDirectiveForwarder();

  public void begin(EventStream event);

  public void end(StreamEnd end);

  public void cancel(StreamBreak cancel);

  public void next(ByteString bytes);

  public void next(String text);

  public void next(StreamMeta meta);

  public void setService(Service service);
}
