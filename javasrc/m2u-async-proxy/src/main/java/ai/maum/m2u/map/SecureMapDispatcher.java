package ai.maum.m2u.map;

import ai.maum.config.Keys;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.handler.common.MessageCryptor;
import ai.maum.m2u.map.util.MapClassLoader;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.Timestamps;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.Map.MapException;
import maum.m2u.map.Map.MapException.StatusCode;
import maum.m2u.map.MaumToYouProxyServiceGrpc;
import maum.m2u.map.MaumToYouProxyServiceGrpc.MaumToYouProxyServiceStub;
import maum.m2u.map.SecureForward.CipherBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MapForwarder로 부터 수신받은 Event를 Map으로 전달
 * Map으로 수신받은 Directive를 MapForwarder로 전달
 */
public class SecureMapDispatcher {
  static final Logger logger = LoggerFactory.getLogger(SecureMapDispatcher.class);

  private static int EX_INDEX = 1300;
  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  // cryptorImpl 객체를 생성합니다
  private static MessageCryptor cryptor = MapClassLoader.loadMessageCryptor();

  // SECURE to MAP
  // 스트림 메시지를 MAP 에게 전송, MAP으로부터 수신을 하는 Stub 을 사용하는 클래스 변수
  private StreamObserver<MapEvent> secureToMapObserver;
  private ManagedChannel secureToMapChannel;
  // MAP 과 통신하기 위한 Client Stub
  private MaumToYouProxyServiceStub secureToMapStub;

  // SECURE to FORWARDER
  // Secure to Forwarder로 Directive를 전달하기 위한 Observer
  private StreamObserver<CipherBody> secureToForwarderObserver;

  /**
   * 필요한 객체를 초기화 합니다.
   *
   * @param res
   */
  public SecureMapDispatcher(StreamObserver<CipherBody> res) {
    // 생성자에서 객체를 모두 초기화 합니다
    secureToForwarderObserver = res;

    secureToMapChannel =  InProcessChannelBuilder.forName("internal-map").build();
    secureToMapStub = MaumToYouProxyServiceGrpc.newStub(secureToMapChannel);
    setHeaderInfo();

    secureToMapObserver = secureToMapStub
        .withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .eventStream(new MapDirectiveRecvObserver());
  }

  /**
   * Forwarder로 부터 onCompleted를 받으면 Map으로 명시적으로 onCompleted를 호출합니다
   */
  public void secureToMapOnCompleted() {
    if (secureToMapObserver != null) {
      logger.debug("#@ OUT Secure to MAP onCompleted");
      secureToMapObserver.onCompleted();
    }
  }

  /**
   * client에서 수신받은 mapEvent를 암호화 하여 전송합니다
   *
   * @param cipherBody 처리해야할 cipherBody
   */
  public void handleEvent(CipherBody cipherBody) {
    MapEvent tempMapEvent;

    // FORWARDER에서 보낸 cipherBody와 비교할 수 있습니다
    logger.trace("#@ IN SECURE from FORWARDER cipherBody [{}]", cipherBody);
    // 1. MapForwarder 로 부터 cipherBody를 전달 받습니다
    // 2. 복호화를 합니다
    // 3. ANY -> MapEvent로 변경합니다
    ByteString decrypedMsg = null;
    // 4. 복호화
    decrypedMsg = cryptor.decode(cipherBody.getCipher());
    try {
      Any anyMessage = Any.newBuilder().mergeFrom(decrypedMsg.toByteArray()).build();
      tempMapEvent = anyMessage.unpack(MapEvent.class);

      logger.trace("#@ Secure to MAP MapEvent [{}]", tempMapEvent);
      // 5. MAP으로 Event를 전달합니다
      secureToMapObserver.onNext(tempMapEvent);
    } catch (InvalidProtocolBufferException e) {
      logger.error("handleEvent e : " , e);
      String msg = "handleEvent InvalidProtocolBufferException";
      sendException(StatusCode.MAP_PAYLOAD_ERROR, exIndex(1), msg);
      secureToMapObserver.onError(e);
      GrpcUtills.closeChannel(secureToMapChannel);
    } catch (Exception e) {
      sendException(StatusCode.MAP_PAYLOAD_ERROR, exIndex(2), e.getMessage());
      secureToMapObserver.onError(e);
    }
 }

  /**
   * Header 정보 설정
   * mapSecureForwarderStub 객체를 초기화 하는 함수
   */
  private void setHeaderInfo() {

    // 생성된 secureToMapChannel은 secureToMapObserver.onNext()호출 이후 명시적으로
    // secureToMapChannel.shutdown()을 호출합니다
    // secureToMapChannel.shutdown()을 호출하지 않으면 RuntimeException이 발생합니다
    // java.lang.RuntimeException: ManagedChannel allocation site
    logger.debug("START resetHeaderInfo()");

    // m2u-auth-token 정보를 attach 합니다
    if (SecureMapInterceptor.HAS_AUTH_TOKEN_HEADER.get()) {
      Metadata fixedHeaders = new Metadata();
      Metadata.Key<String> key = Metadata.Key
          .of(Keys.I_META_M2U_AUTH_TOKEN, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(key, SecureMapInterceptor.AUTH_TOKEN_VALUE.get());

      secureToMapStub = MetadataUtils.attachHeaders(secureToMapStub, fixedHeaders);
    }
    // m2u-auth-sign-in 정보를 attach 합니다
    if (SecureMapInterceptor.HAS_SIGN_IN_HEADER.get()) {
      Metadata fixedHeaders = new Metadata();
      Metadata.Key<String> key = Metadata.Key
          .of(Keys.I_META_M2U_AUTH_SIGN_IN, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(key, "");

      secureToMapStub = MetadataUtils.attachHeaders(secureToMapStub, fixedHeaders);
    }
    // m2u-auth-internal 정보를 attach 합니다
    if (SecureMapInterceptor.HAS_INTERNAL_AUTH_HEADER.get()) {
      Metadata fixedHeaders = new Metadata();
      Metadata.Key<String> key = Metadata.Key
          .of(Keys.I_META_M2U_INTERNAL_AUTH, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(key, "");

      secureToMapStub = MetadataUtils.attachHeaders(secureToMapStub, fixedHeaders);
    }
  }

  /**
   * 예외를 전송합니다.
   *
   * @param code 상태 코드
   * @param exIndex 예외 발생한 위치를 식별할 수 있는 정보
   * @param exMessage 예외 메시지
   */
  public void sendException(StatusCode code, int exIndex, String exMessage) {
    logger.debug("#@ sendException code = {}, exIndex = {}, exMessage = {}",
        code, exIndex, exMessage);

    MapException.Builder exb = MapException.newBuilder();

    exb.setStatusCode(code)
        .setExceptionId(UUID.randomUUID().toString())
        .setThrownAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setExIndex(exIndex)
        .setExMessage(exMessage)
        .build();

    MapDirective dir = MapDirective.newBuilder()
        .setException(exb.build())
        .build();

    // MapEvent -> Any 타입으로 변경
    Any any = Any.pack(dir);
    CipherBody.Builder cipherBody = null;

    ByteString clear = any.toByteString();

    // Class Loader 기능을 이용하여 암호화 클래스 객체를 생성합니다
    ByteString cipher = cryptor.encode(clear);
    cipherBody = CipherBody.newBuilder().setCipher(cipher);

    secureToForwarderObserver.onNext(cipherBody.build());
  }


  class MapDirectiveRecvObserver implements StreamObserver<MapDirective> {
    @Override
    public void onNext(MapDirective value) {
      // 1. Map으로 부터 Directive를 수신 합니다
      // 2. MapDirective를 MapForwarder로 전달합니다
      // 3. MapEvent -> Any 타입으로 변경
      Any any = Any.pack(value);
      CipherBody.Builder cipherBody = null;

      ByteString clear = any.toByteString();

      // 4. Class Loader 기능을 이용하여 암호화 클래스 객체를 생성합니다
      ByteString cipher = cryptor.encode(clear);
      cipherBody = CipherBody.newBuilder().setCipher(cipher);

      // 5. MapForwader로 전달합니다
      secureToForwarderObserver.onNext(cipherBody.build());
    }

    @Override
    public void onError(Throwable throwable) {
      logger.error("onError e : " , throwable);
      logger.debug("#@ IN Event Stream Error [{}]", throwable);
      secureToMapObserver = null;

      sendException(StatusCode.SECURE_DISPATCHER_ERROR, exIndex(3), throwable.getMessage());
//      secureToForwarderObserver.onError(throwable);

      logger.trace("#@ Start secureToForwarderObserver onCompleted by onError");
      secureToForwarderObserver.onCompleted();
      logger.trace("#@ End secureToForwarderObserver onCompleted by onError");

      // secureToMapChannel을 close 합니다
      if (secureToMapChannel != null) {
        logger.trace("#@ Start secureToMapChannel close by onError");
        GrpcUtills.closeChannel(secureToMapChannel);
      }
    }

    @Override
    public void onCompleted() {
      secureToMapObserver = null; // secureToMapObserver 객체 초기화

      logger.trace("#@ Start secureToForwarderObserver onCompleted");
      secureToForwarderObserver.onCompleted();
      logger.trace("#@ End secureToForwarderObserver onCompleted");

      // secureToMapChannel을 close 합니다
      if (secureToMapChannel != null) {
        logger.trace("#@ Start secureToMapChannel close");
        GrpcUtills.closeChannel(secureToMapChannel);
      }
    }
  }


}
