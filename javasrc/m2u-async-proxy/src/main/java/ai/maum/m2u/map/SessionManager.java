package ai.maum.m2u.map;

import ai.maum.config.Keys;
import ai.maum.config.PropertyManager;
import ai.maum.m2u.common.portable.DialogSessionV3Portable;
import ai.maum.m2u.map.hzc.HazelcastConnector;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SessionManager는 MAP의 유량을 체크하는 Thread 클래스 SessionManager는 설정된 주기별로 수행됩니다. 주기별로 hzc를 조회하여 현재 세션의
 * 총갯수를 조회 합니다. 조회 기준 : 현재 세션중 valid가 true, false인 모든 세션의 수를 조회합니다
 */
public class SessionManager extends Thread {

  static final Logger logger = LoggerFactory.getLogger(SessionManager.class);

  private int cycleTime = PropertyManager.getInt(Keys.P_MAP_SESSIONMANAGER_CYCLESEC);
  private int refValue = PropertyManager.getInt(Keys.P_MAP_SESSIONMANAGER_REFVAL);

  // hzc 객체 선언
  private HazelcastInstance hzc;
  private IMap<Long, DialogSessionV3Portable> sessMap;

  private static SessionManager instance;

  private int lastSessionCount;

  private SessionManager() {
    // hzc 객체 생성
    if (hzc == null) {
      logger.debug("#@ SessionManager create HazelcastConnector cycleTime[{}] refValue[{}]",
          cycleTime, refValue);
      this.hzc = HazelcastConnector.getInstance().client;
    }
  }

  public static SessionManager getInstance () {
    if ( instance == null )
      instance = new SessionManager();
    return instance;
  }

  public int getLastSessionCount() {
    return lastSessionCount;
  }

  public int getThreshold() {
    return refValue;
  }

  @Override
  public void run() {
    super.run();

    while (true) {
      try {
        this.hzcProcess();
      } catch (InterruptedException e) {
        logger.error("run e : " , e);
      }
    }
  }

  private void hzcProcess() throws InterruptedException {
    // hzc 조회 시간 간격 설정
    TimeUnit.MILLISECONDS.sleep(cycleTime * 1000);

    // #@ hzc 로 부터 SESSION_V3의 session 정보 갖고 오기
    sessMap = hzc.getMap(Keys.MEM_SSSION_V3);

    Predicate sessPredicate;
    EntryObject sessEntry = new PredicateBuilder().getEntryObject();

    // 총 세션수를 구합니다
    // 현재 세션중 valid가 true, false인 모든 세션의 수를 조회합니다
    sessPredicate = sessEntry.get("valid").equal(true).or(sessEntry.get("valid").equal(false));

    // 총 세션수
    int totalSessCnt = sessMap.keySet(sessPredicate).size();
    lastSessionCount = totalSessCnt;

    // 총 세션수가 기준치 이상이면 isProcessPossible 변수를 false로 변경합니다
    // isProcessPossible가 false이면 MAP에서는 더이상의 Client 요청을 진행하지 않습니다
    // SessionManager Thread는 계속해서 hzc를 체크합니다
    // 총 세션수가 기준치 이하가 되면 isProcessPossible 변수를 true로 변경합니다
    if (totalSessCnt > refValue) {
      Dispatcher.setServicePossible(false);
      logger.debug("#@ totalSessCnt[{}] disable service [{}]", totalSessCnt);
    } else {
      Dispatcher.setServicePossible(true);
    }
  }
}
