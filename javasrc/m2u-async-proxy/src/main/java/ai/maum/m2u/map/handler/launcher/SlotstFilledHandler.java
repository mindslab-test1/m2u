package ai.maum.m2u.map.handler.launcher;

import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import io.grpc.Channel;
import io.grpc.ManagedChannelBuilder;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Audio Player 관련 처리 Handler 구현
 */
public class SlotstFilledHandler implements MapEventHandler {

  static Logger logger = LoggerFactory.getLogger(SlotstFilledHandler.class);

  DirectiveForwarder forwarder = null;
  EventStream callEvent = null;
  Service service = null;

  @Override
  public String getInterface() {
    return MapIf.I_LAUNCHER;
  }

  @Override
  public String getOperation() {
    return MapIf.LC_E_LAUNCHER_SLOTSTFILLED;
  }

  @Override
  public boolean isStream() {
    return false;
  }

  @Override
  public void setDirectiveForwarder(DirectiveForwarder forward) {
    forwarder = forward;
  }

  @Override
  public DirectiveForwarder getDirectiveForwarder() {
    return forwarder;
  }

  @Override
  public void begin(EventStream event) {
    logger.trace("#@ begin SlotstFilledHandler = {}", event.getStreamId());
    callEvent = event;
    service.OnLauncherSlotstFilled(this, event);
  }

  @Override
  public void end(StreamEnd end) {
    logger.trace("#@ end SlotstFilledHandler end = {}", end.getStreamId());
  }

  @Override
  public void cancel(StreamBreak cancel) {
    logger.error("#@ SlotstFilledHandler stream cancelled = {}", cancel.getStreamId());

  }

  @Override
  public void next(ByteString bytes) {
    logger.trace("next bytes = {}", bytes.size());
  }

  @Override
  public void next(String text) {
    logger.trace("oh no next text = {}", text.length());
  }

  @Override
  public void next(StreamMeta meta) {
    logger.trace("oh no next meta = {}", meta.getSerializedSize());
  }

  @Override
  public void setService(Service service) {
    this.service = service;
  }

  private Channel getChannel(String target) {
    logger.debug("target is = {}", target);
    int pos = target.indexOf(':');
    String addr = target.substring(0, pos);
    String port = target.substring(pos + 1);
    int p = Integer.parseInt(port);

    return ManagedChannelBuilder.forAddress(addr, p).usePlaintext(true).build();
  }


}