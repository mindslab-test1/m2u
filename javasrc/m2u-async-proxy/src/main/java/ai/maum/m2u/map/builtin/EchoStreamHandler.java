package ai.maum.m2u.map.builtin;

import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import com.google.protobuf.util.Timestamps;
import java.util.UUID;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.DirectiveStream.DirectiveParam;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EchoStreamHandler implements MapEventHandler {

  static Logger logger = LoggerFactory.getLogger(EchoHandler.class);
  DirectiveForwarder forwarder = null;
  AsyncInterface aif = MapIf.newAsyncInterface(
      "Echo", "StreamAck",
      false, OperationType.OP_DIRECTIVE);
  String streamId = UUID.randomUUID().toString();

  @Override
  public String getInterface() {
    return "Echo";
  }

  @Override
  public String getOperation() {
    return "Stream";
  }

  @Override
  public boolean isStream() {
    return true;
  }

  @Override
  public void setDirectiveForwarder(DirectiveForwarder forward) {
    forwarder = forward;
  }

  @Override
  public DirectiveForwarder getDirectiveForwarder() {
    return forwarder;
  }

  @Override
  public void begin(EventStream event) {
    DirectiveStream dir = DirectiveStream.newBuilder()
        .setInterface(aif)
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(streamId)
        .setOperationSyncId(event.getOperationSyncId())
        .setParam(DirectiveParam.newBuilder()
            .setEmptyParam(Empty.newBuilder().build()).build())
        .setPayload(event.getPayload())
        .build();
    forwarder.sendStream(dir);
    forwarder.notifyCompleted(this);
  }

  @Override
  public void end(StreamEnd end) {
    StreamEnd dir = StreamEnd.newBuilder()
        .setStreamId(streamId)
        .setEndAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .build();
    forwarder.sendStream(dir);
  }

  @Override
  public void cancel(StreamBreak cancel) {
    forwarder.notifyCompleted(this);
  }

  @Override
  public void next(ByteString bytes) {
    forwarder.sendStream(bytes);
  }

  @Override
  public void next(String text) {
    forwarder.sendStream(text);
  }

  @Override
  public void next(StreamMeta meta) {
    forwarder.sendStream(meta);
  }

  @Override
  public void setService(Service service) {

  }
}
