package ai.maum.m2u.map.services.mobile;

import static ai.maum.config.PropertyManager.getString;

import ai.maum.config.Keys;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.common.user.UserParam;
import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.ExceptionMsgUtills;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import maum.m2u.common.DeviceOuterClass.Device;
import maum.m2u.common.Dialog.SystemContext;
import maum.m2u.common.EventOuterClass.DialogAgentForwarderParam;
import maum.m2u.common.EventOuterClass.Event;
import maum.m2u.common.EventOuterClass.EventPayload;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException;
import maum.m2u.router.v3.Router.EventRouteRequest;
import maum.m2u.router.v3.Router.TalkRouteResponse;
import maum.m2u.router.v3.TalkRouterGrpc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Audio Player에 관한 Service 정의 해당 Service에는 PlayFinished, PlayPaused, PlayResumed, PlayStopped 등의
 * Audio 관련 처리가 정의된다
 */
public class AudioPlayerServiceImpl extends Service {

  static int EX_INDEX = 1000;

  static int exIndex(int index) {
    return EX_INDEX + index;
  }

  static final Logger logger = LoggerFactory.getLogger(AudioPlayerServiceImpl.class);

  private String json = null;

  /**
   * AudioPlayerServiceImpl 생성자
   */
  public AudioPlayerServiceImpl() {
    logger.debug("#@ Create constructor of AudioPlayerServiceImpl");
  }

  @Override
  public void OnAudioPlayerEvent(MapEventHandler handler, EventStream event) {
    logger.info("#@ IN OnAudioPlayerEvent");
    this.eventRouterStub(DialogUtils.createUserParam(event), event, handler);

    return;
  }

  /**
   * Router 서버로 eventRouter GRPC 통신을 요청한다
   *
   * @param userParam 유저 정보
   * @param callEvent EventStream 정보
   */
  private void eventRouterStub(UserParam userParam, EventStream callEvent,
      MapEventHandler handler) {
    logger.info("#@ eventRouterStub ");

    // DialogAgentForwarderParam 이 없다면 에러처리
    if (!callEvent.getParam().hasDialogAgentForwarderParam()) {
      logger.error("#@ eventRouterStub Error : There is no DialogAgentForwarderParam");

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_NO_STREAM_PARAM, exIndex(1),
              "No parameter for eventRouter", callEvent, null);
      handler.getDirectiveForwarder().notifyCompleted(handler);

      return;
    }

    // eventRouterStub에 대한 Processing
    handler.getDirectiveForwarder().sendStream(DialogUtils.makeDialogProcessing(callEvent));

    // struct인 callEvent를 EventPayload 타입으로 바꾸기위한 로직
    EventPayload.Builder eventPayload = EventPayload.newBuilder();
    try {
      json = JsonFormat.printer().includingDefaultValueFields()
          .print(callEvent.getPayload());
      JsonFormat.parser().ignoringUnknownFields().merge(json, eventPayload);
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("eventRouter json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(2),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    // EventRouteRequest param을 생성한다
    EventRouteRequest param = EventRouteRequest.newBuilder()
        .setEvent(Event.newBuilder()
            .setInterface(callEvent.getInterface().getInterface())
            .setOperation(callEvent.getInterface().getOperation())
            .setParam(DialogAgentForwarderParam.newBuilder()
                .mergeFrom(callEvent.getParam().getDialogAgentForwarderParam()))
            .setPayload(eventPayload.build())
        )
        .setSystemContext(SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get())
        )
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));

    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc
        .newStub(channel);

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);
    Date reqDate = new Date();

    logger.debug("#@ Router event [{}]", param.toString());

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .eventRoute(param, new StreamObserver<TalkRouteResponse>() {
          TalkRouteResponse ret = null;

          @Override
          public void onNext(TalkRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ eventRouterStub onNext [{}]", value);
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            ExceptionMsgUtills.sendExceptionFromRouter(
                handler, t, callEvent, reqDate, callEvent, exIndex(3),
                MapException.StatusCode.GRPC_ROUTER_EVENT_ERROR,
                "Event Route error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ AV eventRouterStub onCompleted");

            DialogUtils
                .processTalkRouterResponse(handler, ret, userParam, MapIf.E_SPEECH, callEvent);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }
}
