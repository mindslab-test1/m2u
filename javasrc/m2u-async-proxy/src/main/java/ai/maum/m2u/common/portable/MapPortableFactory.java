package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableFactory;


public class MapPortableFactory implements PortableFactory {

  @Override
  public Portable create(int classId) {
    if (classId == PortableClassId.USER_INFO) {
      return new UserInfoPortable();
    } else {
      throw new IllegalArgumentException(classId + " unsupported classId");
    }
  }
}
