package ai.maum.m2u.map;

import ai.maum.m2u.map.handler.common.MapEventHandler;
import com.google.protobuf.ByteString;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException.StatusCode;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;

/**
 * 발생한 디렉티브를 클라이언트로 내보내도록 한다.
 *
 * 내부적으로 내보낸 디렉티브에 대한 로그를 남기거나 처리할 수 있다. `MapEventHandler`는 체계적인 이벤트 처리 중에 발생한 메시지를 단말에 내보낼 수 있다.
 */
public interface DirectiveForwarder {

  /**
   * 디렉티브 스트림을 전달한다.
   *
   * @param stream 디렉티브 스트림
   */
  void sendStream(DirectiveStream stream);

  /**
   * 기존 스트림의 종료를 전달한다.
   *
   * @param end 디렉티브를 종료한다.
   */
  void sendStream(StreamEnd end);

  void sendStream(StreamBreak cancel);

  void sendStream(ByteString bytes);

  void sendStream(String text);

  void sendStream(StreamMeta meta);

  /**
   * 예외를 전송합니다.
   *
   * @param code      상태 코드
   * @param exIndex   예외 코드
   * @param exMessage 예외 메시지
   * @param refEvent  관련 EventStream, can be null
   * @param json      페이로드 데이터 can be null
   */
  void sendException(StatusCode code,
                     int exIndex,
                     String exMessage,
                     EventStream refEvent,
                     String json);

  void notifyCompleted(MapEventHandler handler);
}