package ai.maum.m2u.map.handler.auth;

import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import io.grpc.Channel;
import io.grpc.ManagedChannelBuilder;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateUserSettingsHandler implements MapEventHandler {

  static Logger logger = LoggerFactory.getLogger(GetUserSettingsHandler.class);

  DirectiveForwarder forwarder = null;
  EventStream callEvent = null;
  Service service = null;

  @Override
  public String getInterface() {
    return MapIf.I_AUTHENTICATION;
  }

  @Override
  public String getOperation() {
    return MapIf.AU_E_UPDATE_USER_SETTINGS;
  }

  @Override
  public boolean isStream() {
    return false;
  }

  @Override
  public void setDirectiveForwarder(DirectiveForwarder forward) {
    forwarder = forward;
  }

  @Override
  public DirectiveForwarder getDirectiveForwarder() {
    return forwarder;
  }

  @Override
  public void begin(EventStream event) {
    logger.info("#@ begin UpdateUserSettingsHandler");
    callEvent = event;
    service.OnUpdateUserSettings(this, event);
  }

  @Override
  public void end(StreamEnd end) {
    logger.info("#@ UpdateUserSettingsHandler end = {}", end.getStreamId());

  }

  @Override
  public void cancel(StreamBreak cancel) {
    logger.error("UpdateUserSettingsHandler stream cancelled. {}", cancel.getStreamId());

  }

  @Override
  public void next(ByteString bytes) {
    logger.trace("next bytes, " + bytes.size());

  }

  @Override
  public void next(String text) {
    logger.trace("oh no next text, " + text.length());
  }

  @Override
  public void next(StreamMeta meta) {
    logger.trace("oh no next meta, " + meta.getSerializedSize());
  }

  @Override
  public void setService(Service service) {
    this.service = service;
  }
}
