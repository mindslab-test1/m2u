package ai.maum.m2u.map;

import ai.maum.config.Keys;
import ai.maum.config.PropertyManager;
import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 모든 Request에 대하여 Header확인 후 sigin-in, isValid to Service, reject 처리
 */
public class ServerAuthInterceptor implements ServerInterceptor {

  static final Logger logger = LoggerFactory.getLogger(Dispatcher.class);

  public static final Context.Key<String> AUTH_SERVER_IP = Context.key(Keys.CTX_AUTH_SERVER_IP);
  static final Context.Key<Boolean> HAS_SIGN_IN_HEADER = Context.key(Keys.CTX_HAS_SIGN_IN_HEADER);
  static final Context.Key<Boolean> HAS_AUTH_TOKEN_HEADER = Context.key(Keys.CTX_HAS_AUTH_TOKEN_HEADER);
  static final Context.Key<String> AUTH_TOKEN_VALUE = Context.key(Keys.CTX_AUTH_TOKEN_VALUE);

  public ServerAuthInterceptor() {
  }

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(
      ServerCall<ReqT, RespT> serverCall,
      Metadata metadata,
      ServerCallHandler<ReqT, RespT> serverCallHandler) {

    Context context;
    String authServerIp = "";
    String authTokenValue = "";

    try {

      boolean isAuthToken = metadata.containsKey(
          Metadata.Key.of(Keys.I_META_M2U_AUTH_TOKEN, Metadata.ASCII_STRING_MARSHALLER));
      boolean isAuthSignIn = metadata.containsKey(
          Metadata.Key.of(Keys.I_META_M2U_AUTH_SIGN_IN, Metadata.ASCII_STRING_MARSHALLER));
      boolean isAuthInternal = metadata.containsKey(
          Metadata.Key.of(Keys.I_META_M2U_INTERNAL_AUTH, Metadata.ASCII_STRING_MARSHALLER));

      // M2U_INTERNAL_AUTH_HEADER가 있는요청은 dummyAuth를 호출
      if (isAuthInternal) {
        authServerIp = PropertyManager.getString(Keys.P_AUTH_INTERNAL_SERVER);
      } else {
        authServerIp = PropertyManager.getString(Keys.P_AUTH_SERVER);
      }

      if (isAuthToken) {
        authTokenValue = metadata.get(Metadata.Key
            .of(Keys.I_META_M2U_AUTH_TOKEN, Metadata.ASCII_STRING_MARSHALLER));
      }

      // 필수 헤더 정보 유무 확인 로그 (헤당 로그가 없으면 인증시 매우 난감)
      logger.debug("containsKey {} :: {}", Keys.I_META_M2U_AUTH_TOKEN, isAuthToken);
      logger.debug("containsKey {} :: {}", Keys.I_META_M2U_AUTH_SIGN_IN, isAuthSignIn);
      logger.debug("containsKey {} :: {}", Keys.I_META_M2U_INTERNAL_AUTH, isAuthInternal);
      logger.debug("authTokenValue :: {}", authTokenValue);


      context = Context.current()
          .withValues(AUTH_SERVER_IP, authServerIp, HAS_SIGN_IN_HEADER, isAuthSignIn,
              HAS_AUTH_TOKEN_HEADER, isAuthToken, AUTH_TOKEN_VALUE, authTokenValue);
      return Contexts.interceptCall(context, serverCall, metadata, serverCallHandler);
    } catch (Exception e) {
      serverCall.close(Status.INTERNAL, new Metadata());
      return null;
    }
  }

}
