package ai.maum.m2u.map.handler.dialog;

import static ai.maum.config.PropertyManager.getString;

import ai.maum.config.Keys;
import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import maum.brain.idr.Idr.ImageRecognitionParam;
import maum.brain.idr.Idr.ImageRecognitionResult;
import maum.brain.idr.Idr.RecognizeImageRequest;
import maum.brain.idr.ImageRecognitionServiceGrpc;
import maum.brain.idr.ImageRecognitionServiceGrpc.ImageRecognitionServiceStub;
import maum.m2u.common.UserOuterClass.User;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException.StatusCode;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class ImageToTextTalkHandler implements MapEventHandler {

  static int EX_INDEX = 500;

  static int exIndex(int index) {
    return EX_INDEX + index;
  }

  static Logger logger = LoggerFactory.getLogger(ImageToTextTalkHandler.class);

  DirectiveForwarder forwarder = null;
  EventStream callEvent = null;
  Service service = null;
  ImageRecognitionParam imageRecognitionParam = null;
  StreamObserver<RecognizeImageRequest> requestObserver = null;
  ImageRecognitionResult.Builder imageRecognitionResult = null;

  @Override
  public String getInterface() {
    return MapIf.I_DIALOG;
  }

  @Override
  public String getOperation() {
    return MapIf.DL_E_IMAGE_TO_TEXT_TALK;
  }

  @Override
  public boolean isStream() {
    return true;
  }

  @Override
  public void setDirectiveForwarder(DirectiveForwarder forward) {
    forwarder = forward;
  }

  @Override
  public DirectiveForwarder getDirectiveForwarder() {
    return forwarder;
  }

  @Override
  public void begin(EventStream event) {
    logger.info("#@ ImageToTextTalkHandler begin = {}", event.getStreamId());

    // ImageRecognitionParam param 이 없다면
    if (!event.getParam().hasImageRecognitionParam()) {
      forwarder.sendException(StatusCode.MAP_NO_STREAM_PARAM, exIndex(1),
          "No parameter for ITT", event, null);
      return;
    }
    callEvent = event;
    // #@ imageRecognitionParam 객체 생성 (초기 Client 로 부터 생성)
    imageRecognitionParam = event.getParam().getImageRecognitionParam();
    // #@ Image 서버 연결 시도
    connectImageRealStub(imageRecognitionParam, callEvent);

    service.OnDialogImageToTextTalk(this, event, imageRecognitionParam);
  }

  @Override
  public void end(StreamEnd end) {
    logger.info("#@ ImageToTextTalkHandler end = {}", end.getStreamId());
    requestObserver.onCompleted();
  }

  @Override
  public void cancel(StreamBreak cancel) {
    logger.error("#@ ImageToTextTalkHandler cancelled = {}", cancel.getStreamId());

    requestObserver.onError(Status.CANCELLED.asException());

    logger.error("#@ Exception on ON CANCEL notifyCompleted");
    forwarder.notifyCompleted(this);
  }

  // #@ Client로 받은 값을 Image 서버로 전달한다
  @Override
  public void next(ByteString bytes) {
    logger.trace("#@ ImageToTextTalkHandler next bytes = {}", bytes.size());

    // #@ requestObserver가 생성되지 않았다면
    if (requestObserver == null) {
      logger.info("#@ There is no requestObserver. So try to connectImageRealStub");
      connectImageRealStub(imageRecognitionParam, callEvent);
      return;
    }

    // #@ byte 데이타를 그대로 전달
    requestObserver.onNext(RecognizeImageRequest.newBuilder().setImgContent(bytes).build());
  }

  @Override
  public void next(String text) {
    logger.trace("oh no next text = {}", text.length());
  }

  @Override
  public void next(StreamMeta meta) {
    logger.trace("oh no next meta = {}", meta.getSerializedSize());
  }

  @Override
  public void setService(Service service) {
    // #@ 서비스 등록
    this.service = service;
  }

  private void connectImageRealStub(ImageRecognitionParam imageRecognitionParam,
      EventStream callEvent) {
    String imageRemote = getString(Keys.P_IDR_SERVER);
    logger.info("#@ connect to IMAGE Server = {}", imageRemote);

    final MapEventHandler final_handler = this;

    final ManagedChannel channel = DialogUtils.getChannel(imageRemote);
    ImageRecognitionServiceStub imageRecognitionServiceStub = ImageRecognitionServiceGrpc
        .newStub(channel);

    imageRecognitionResult = ImageRecognitionResult.newBuilder();

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();
    User tempUser = Dispatcher.threadLocalUser.get();

    // 헤더에 operationSyncId 설정
    imageRecognitionServiceStub = MetadataUtils
        .attachHeaders(imageRecognitionServiceStub, tempOpSyncIdMeta);

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ requestObserver 객체 생성
    requestObserver = imageRecognitionServiceStub
        .withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .recognizeImage(new StreamObserver<ImageRecognitionResult>() {

          @Override
          public void onNext(ImageRecognitionResult result) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            Dispatcher.threadLocalUser.set(tempUser);
            // #@ Image 서버로 부터 result 수신
            logger.trace("Idr ImageRecognitionResult onNext = {}", result);
            imageRecognitionResult.setImageClass(result.getImageClass());
            imageRecognitionResult.addAllAnnotatedTexts(result.getAnnotatedTextsList());
            imageRecognitionResult.setMeta(result.getMeta());
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            Status status = Status.fromThrowable(t);
            logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
                t, Dispatcher.threadLocal.get());

            String msg = MessageFormat.format("IDR error occurred. {0}:{1}",
                status.getCode(), status.getDescription());
            // 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

            forwarder.sendException(StatusCode.GRPC_IDR_ERROR, exIndex(2),
                msg, callEvent, tempPayloadJson);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            Dispatcher.threadLocalUser.set(tempUser);
            // 메시지 전송이 완료되면 서버에서 onCompleted()를 호출
            logger.trace("onCompleted imageRecognitionResult = {}", imageRecognitionResult);
            service.OnImageRecognizerRecognized(final_handler,
                imageRecognitionResult.build(), MapIf.E_TEXT, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });

    // #@ ImageRecognitionParam 설정
    ImageRecognitionParam.Builder param = ImageRecognitionParam.newBuilder()
        .mergeFrom(imageRecognitionParam);

    // #@ 설정값을 Image 서버로 전송
    RecognizeImageRequest req = RecognizeImageRequest.newBuilder().setParam(param).build();
    requestObserver.onNext(req);
    logger.info("#@ Set the ImageRecognitionParam to Image Server");
  }

}
