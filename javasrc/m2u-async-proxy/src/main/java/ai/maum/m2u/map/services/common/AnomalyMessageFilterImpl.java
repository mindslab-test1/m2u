package ai.maum.m2u.map.services.common;

import ai.maum.m2u.map.handler.common.AnomalyMessageFilter;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnomalyMessageFilterImpl implements AnomalyMessageFilter {

  static final Logger logger = LoggerFactory.getLogger(AnomalyMessageFilterImpl.class);

  @Override
  public boolean isAnomalyMessage(String message) {
    /* 특수문자 공백 처리 */
    final Pattern SpecialChars = Pattern.compile("['\"\\-#()@;=*/+]");
    message = SpecialChars.matcher(message).replaceAll("").toLowerCase();

    logger.trace("#@ mapEvent.getPayload() [{}]", message);

    return message.matches("select\\s+(.*)\\s+from\\s+(.*)")
        || message.matches("select\\s+(.*)\\s+from\\s+(.*)\\s+union\\s+select\\s+(.*)")
        || message.matches("insert\\s+into\\s+(.*)")
        || message.matches("update\\s+(.*)\\s+set\\s+(.*)")
        || message.matches("delete\\s+from\\s+(.*)")
        || message.matches("eval");
  }
}
