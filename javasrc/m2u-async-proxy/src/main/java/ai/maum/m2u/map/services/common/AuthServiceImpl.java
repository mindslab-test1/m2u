package ai.maum.m2u.map.services.common;

import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.MaumToYouProxyServiceImpl;
import ai.maum.m2u.map.ServerAuthInterceptor;
import ai.maum.m2u.map.common.user.UserParam;
import ai.maum.m2u.map.common.utils.CallLogUtills;
import ai.maum.m2u.map.common.utils.DialogUtils;
import ai.maum.m2u.map.common.utils.GrpcUtills;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Struct;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import maum.m2u.map.Authentication.GetUserInfoRequest;
import maum.m2u.map.Authentication.GetUserInfoResponse;
import maum.m2u.map.Authentication.IsValidRequest;
import maum.m2u.map.Authentication.IsValidResponse;
import maum.m2u.map.Authentication.MultiFactorVerifyPayload;
import maum.m2u.map.Authentication.SignInPayload;
import maum.m2u.map.Authentication.SignInResultPayload;
import maum.m2u.map.Authentication.SignOutPayload;
import maum.m2u.map.Authentication.SignOutResultPayload;
import maum.m2u.map.Authentication.UserKey;
import maum.m2u.map.Authentication.UserSettings;
import maum.m2u.map.AuthenticationProviderGrpc;
import maum.m2u.map.AuthenticationProviderGrpc.AuthenticationProviderStub;
import maum.m2u.map.AuthorizationProviderGrpc;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class AuthServiceImpl extends Service {

  static int EX_INDEX = 700;

  static int exIndex(int index) {
    return EX_INDEX + index;
  }

  static final Logger logger = LoggerFactory.getLogger(MaumToYouProxyServiceImpl.class);

  private String json = null;

  public AuthServiceImpl() {
    logger.debug("#@ Create constructor of AuthServiceImpl");
  }

  @Override
  public void OnAuthSignIn(MapEventHandler handler, EventStream callEvent) {
    logger.info("#@ IN OnAuthSignIn");
    Struct.Builder meta = null;

    UserParam userParam = DialogUtils.createUserParam(callEvent);
    /*
     * #@ REQ : AuthSignIn 요청이 들어오면 MAP은 AuthModule로 ID, PW을 전달
     * RES : AuthModule은 MAP에게 AccessToken을 전달
     */
    try {
      meta = callEvent.getPayload().toBuilder();
      logger
          .trace("#@ OnAuthSignIn getPayload : {}",
              JsonFormat.printer().includingDefaultValueFields().print(meta));

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("authSignIn json print error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(1),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    this.signInAuthStub(meta, userParam, handler, callEvent);
  }

  // #@ AuthModule과 signIn GRPC 통신
  private void signInAuthStub(Struct.Builder payload, UserParam userParam, MapEventHandler handler,
      EventStream callEvent) {
    logger.info("#@ signInAuthStub ");

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());

    AuthenticationProviderStub stub = AuthenticationProviderGrpc.newStub(channel);
    SignInPayload.Builder param = SignInPayload.newBuilder();

    try {
      json = JsonFormat.printer().includingDefaultValueFields().print(payload);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("signIn json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(2),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .signIn(param.build(), new StreamObserver<SignInResultPayload>() {
          SignInResultPayload ret = null;

          @Override
          public void onNext(SignInResultPayload signInResponse) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger
                .trace("#@ AuthenticationProviderGrpc.signIn onNext : {}",
                    signInResponse.toString());

            ret = signInResponse;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            Status st = Status.fromThrowable(t);
            logger
                .error("#@ AuthenticationProviderGrpc.signIn onError [{}]", st, t,
                    Dispatcher.threadLocal.get());

            CallLogUtills
                .makeSignInCallLog(callEvent, tempOpSyncIdMeta, param.build(), userParam, null,
                    reqDate, new Date());

            String msg = MessageFormat.format("signIn error occurred. {0}:{1}",
                st.getCode(), st.getDescription());

            // 인증에서 준 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'authException': '" + st.toString() + "'}";

            handler.getDirectiveForwarder()
                .sendException(MapException.StatusCode.GRPC_AUTH_SIGN_IN_ERROR, exIndex(3),
                    msg, callEvent, tempPayloadJson);

            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ AuthenticationProviderGrpc.signIn onCompleted.");

            CallLogUtills
                .makeSignInCallLog(callEvent, tempOpSyncIdMeta, param.build(), userParam, ret,
                    reqDate, new Date());
            receiveSignInResponse(handler, ret, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * signIn에 대한 Response 수신 처리 루틴
   */
  private void receiveSignInResponse(MapEventHandler handler, SignInResultPayload response,
      EventStream callEvent) {
    logger.trace("#@ receiveSignInResponse" + response.toString());

    handler.getDirectiveForwarder()
        .sendStream(makeSignInResponse(response, callEvent));
    handler.getDirectiveForwarder().notifyCompleted(handler);
  }

  private DirectiveStream makeSignInResponse(SignInResultPayload response, EventStream callEvent) {

    SignInResultPayload.Builder signInResultPayload = response.toBuilder();

    DirectiveStream dir = DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_AUTHENTICATION)
            .setOperation(MapIf.AU_D_SIGN_IN_RESULT)
            .setType(AsyncInterface.OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(signInResultPayload.build()))
        .build();
    return dir;
  }

  @Override
  public void MultiFactorVerify(MapEventHandler handler, EventStream callEvent) {
    logger.info("#@ IN MultiFactorVerifyHandler");
    Struct.Builder meta = null;

    UserParam userParam = DialogUtils.createUserParam(callEvent);

    try {
      meta = callEvent.getPayload().toBuilder();
      logger.trace("#@ MultiFactorVerifyHandler param : {}",
          JsonFormat.printer().includingDefaultValueFields().print(meta));

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("multifactorverify json print error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(4),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    this.MultiFactorVerifyAuthStub(meta, userParam, handler, callEvent);
  }

  // #@ AuthModule과 MultiFactorVerifyHandler GRPC 통신
  private void MultiFactorVerifyAuthStub(Struct.Builder meta, UserParam userParam,
      MapEventHandler handler, EventStream callEvent) {
    logger.info("#@ MultiFactorVerifyAuthStub ");

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());
    AuthenticationProviderGrpc.AuthenticationProviderStub stub = AuthenticationProviderGrpc
        .newStub(channel);

    MultiFactorVerifyPayload.Builder param = MultiFactorVerifyPayload
        .newBuilder();

    try {
      json = JsonFormat.printer().includingDefaultValueFields()
          .print(meta);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("MultiFactorVerify json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(5),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }
    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .multiFactorVerify(param.build(), new StreamObserver<SignInResultPayload>() {
          SignInResultPayload ret = null;

          @Override
          public void onNext(SignInResultPayload signInResponse) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace(
                "AuthenticationProviderGrpc.multiFactorVerify onNext : {}", signInResponse
                    .toString());

            ret = signInResponse;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            Status status = Status.fromThrowable(t);
            logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
                t, Dispatcher.threadLocal.get());

            CallLogUtills
                .makeMultiFactorVerifyCallLog(callEvent, tempOpSyncIdMeta, param.build(), userParam,
                    null, reqDate, new Date());

            String msg = MessageFormat.format("multiFactorVerify error occurred. {0}:{1}",
                status.getCode(), status.getDescription());
            // 인증에서 준 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'authException': '" + status.toString() + "'}";

            handler.getDirectiveForwarder()
                .sendException(MapException.StatusCode.GRPC_AUTH_MULTIFACTOR_VERIFY_ERROR,
                    exIndex(6),
                    msg, callEvent, tempPayloadJson);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 에러 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ AuthenticationProviderGrpc.multiFactorVerifyAuth onCompleted.");

            CallLogUtills
                .makeMultiFactorVerifyCallLog(callEvent, tempOpSyncIdMeta, param.build(), userParam,
                    ret, reqDate, new Date());
            receiveMultiFactorVerifyResult(handler, ret, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * MultiFactorVerifyHandler 대한 Response 수신 처리 루틴
   */
  private void receiveMultiFactorVerifyResult(MapEventHandler handler,
      SignInResultPayload response, EventStream callEvent) {
    logger.trace("#@ receiveMultiFactorVerifyResult : {}", response.toString());

    handler.getDirectiveForwarder()
        .sendStream(makeMultiFactorVerifyResponse(response, callEvent));

    handler.getDirectiveForwarder().notifyCompleted(handler);
  }

  private DirectiveStream makeMultiFactorVerifyResponse(
      SignInResultPayload response, EventStream callEvent) {

    SignInResultPayload.Builder signInResultPayload = response.toBuilder();

    DirectiveStream dir = DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_AUTHENTICATION)
            .setOperation(MapIf.AU_D_MULTI_FACTOR_VERIFY_RESULT)
            .setType(AsyncInterface.OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(signInResultPayload.build()))
        .build();
    return dir;
  }

  @Override
  public void OnAuthSignOut(MapEventHandler handler, EventStream callEvent) {
    logger.info("#@ IN OnAuthSignOut");
    Struct.Builder meta = null;

    UserParam userParam = DialogUtils.createUserParam(callEvent);

    try {
      meta = callEvent.getPayload().toBuilder();
      logger.trace("#@ OnAuthSignOut getPayload : {}",
          JsonFormat.printer().includingDefaultValueFields().print(meta));

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("AuthSignOut json print error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(7),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    this.signOutAuthStub(meta, userParam, handler, callEvent);

    return;
  }

  // #@ AuthModule과 signOut GRPC 통신
  private void signOutAuthStub(Struct.Builder meta, UserParam userParam, MapEventHandler handler,
      EventStream callEvent) {
    logger.info("#@ signOutAuthStub ");

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());

    AuthenticationProviderGrpc.AuthenticationProviderStub stub = AuthenticationProviderGrpc
        .newStub(channel);
    SignOutPayload.Builder param = SignOutPayload.newBuilder();

    try {
      json = JsonFormat.printer().includingDefaultValueFields()
          .print(meta);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("signOut json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(8),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .signOut(param.build(), new StreamObserver<SignOutResultPayload>() {
          SignOutResultPayload ret = null;

          @Override
          public void onNext(SignOutResultPayload value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ AuthenticationProviderGrpc.signOut onNext : {}", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            Status st = Status.fromThrowable(t);
            logger
                .error("#@ AuthenticationProviderGrpc.signOut onError [{}]", st, t,
                    Dispatcher.threadLocal.get());

            CallLogUtills
                .makeSignOutCallLog(callEvent, tempOpSyncIdMeta, param.build(), userParam, null,
                    reqDate, new Date());

            String msg = MessageFormat.format("signOut error occurred. {0}:{1}",
                st.getCode(), st.getDescription());
            // 인증에서 준 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'authException': '" + st.toString() + "'}";

            handler.getDirectiveForwarder()
                .sendException(MapException.StatusCode.GRPC_AUTH_SIGN_OUT_ERROR, exIndex(9),
                    msg, callEvent, tempPayloadJson);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ AuthenticationProviderGrpc.signOut onCompleted.");

            CallLogUtills
                .makeSignOutCallLog(callEvent, tempOpSyncIdMeta, param.build(), userParam, ret,
                    reqDate, new Date());
            receiveSignOutResponse(handler, ret, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * signOut에 대한 Response 수신 처리 루틴
   */
  private void receiveSignOutResponse(MapEventHandler handler, SignOutResultPayload
      response, EventStream callEvent) {
    logger.trace("#@ receiveSignOutResponse" + response.toString());
    String tempMessage = response.getMessage();

    handler.getDirectiveForwarder()
        .sendStream(makeSignOutResponse(tempMessage, callEvent));
    handler.getDirectiveForwarder().notifyCompleted(handler);
  }

  private DirectiveStream makeSignOutResponse(String message, EventStream callEvent) {
    SignOutResultPayload signOutResultPayload =
        SignOutResultPayload.newBuilder()
            .setMessage(message)
            .build();

    DirectiveStream dir = DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_AUTHENTICATION)
            .setOperation(MapIf.AU_D_SIGN_OUT_RESULT)
            .setType(AsyncInterface.OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(signOutResultPayload))
        .build();
    return dir;
  }

  @Override
  public void OnIsValid(MapEventHandler handler, EventStream callEvent) {
    logger.info("#@ IN OnIsValid");
    Struct.Builder meta = null;

    try {
      meta = callEvent.getPayload().toBuilder();
      logger.trace("#@ OnIsValid getPayload : {}",
          JsonFormat.printer().includingDefaultValueFields().print(meta));

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("isVaild json print error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(10),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    this.isValidAuthStub(meta, handler, callEvent);
  }

  // #@ AuthModule과 isValid GRPC 통신
  private void isValidAuthStub(Struct.Builder meta, MapEventHandler handler,
      EventStream callEvent) {
    logger.info("#@ isValidAuthStub ");
    IsValidRequest.Builder param = IsValidRequest.newBuilder();

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());

    AuthorizationProviderGrpc.AuthorizationProviderStub stub = AuthorizationProviderGrpc
        .newStub(channel);

    try {
      json = JsonFormat.printer().includingDefaultValueFields()
          .print(meta);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("isValid json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(11),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .isValid(param.build(), new StreamObserver<IsValidResponse>() {
          IsValidResponse ret = null;

          @Override
          public void onNext(IsValidResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ AuthenticationProviderGrpc.isValid onNext : {}", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.debug("#@ AuthenticationProviderGrpc.isValid onError {}", t.toString());

            Status st = Status.fromThrowable(t);
            String msg = MessageFormat.format("isValid error occurred. {0}:{1}",
                st.getCode(), st.getDescription());
            // 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'exception': '" + st.toString() + "'}";

            handler.getDirectiveForwarder()
                .sendException(MapException.StatusCode.GRPC_AUTH_IS_VALID_ERROR, exIndex(12),
                    msg, callEvent, tempPayloadJson);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ AuthenticationProviderGrpc.isValid onCompleted.");

            receiveIsValidResponse(handler, ret, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * isVaild에 대한 Response 수신 처리 루틴
   */
  private void receiveIsValidResponse(MapEventHandler handler, IsValidResponse
      response, EventStream callEvent) {
    logger.trace("#@ receiveIsValidResponse" + response.toString());
    boolean tempIsVaild = response.getIsValid();
    Timestamp tempExpiredAt = response.getExpiredAt();
    String tempAccessToken = response.getAccessToken();

    handler.getDirectiveForwarder()
        .sendStream(makeIsValidResponse(tempIsVaild, tempExpiredAt, tempAccessToken, callEvent));

    handler.getDirectiveForwarder().notifyCompleted(handler);
  }

  private DirectiveStream makeIsValidResponse(boolean isVaild, Timestamp expiredAt,
      String accessToken, EventStream callEvent) {
    IsValidResponse isValidResponse =
        IsValidResponse.newBuilder()
            .setIsValid(isVaild)
            .setExpiredAt(expiredAt)
            .setAccessToken(accessToken)
            .build();

    DirectiveStream dir = DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_AUTHENTICATION)
            .setOperation(MapIf.AU_D_IS_VALID_RESPONSE)
            .setType(AsyncInterface.OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(isValidResponse))
        .build();
    return dir;
  }

  @Override
  public void OnGetUserInfo(MapEventHandler handler, EventStream callEvent) {
    logger.info("#@ IN OnGetUserInfo");
    Struct.Builder meta = null;

    try {
      meta = callEvent.getPayload().toBuilder();

      logger.trace("#@ OnGetUserInfo getPayload : {}",
          JsonFormat.printer().includingDefaultValueFields().print(meta));
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("getUserInfo json print error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(13),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    this.getUserInfoAuthStub(meta, handler, callEvent);
  }

  // #@ AuthModule과 getUserInfo GRPC 통신
  private void getUserInfoAuthStub(Struct.Builder meta, MapEventHandler handler,
      EventStream callEvent) {
    logger.info("#@ getUserInfoAuthStub ");
    GetUserInfoRequest.Builder param = GetUserInfoRequest.newBuilder();

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());

    AuthorizationProviderGrpc.AuthorizationProviderStub stub = AuthorizationProviderGrpc
        .newStub(channel);

    try {
      json = JsonFormat.printer().includingDefaultValueFields()
          .print(meta);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("getUserInfo json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(14),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);

    }

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .getUserInfo(param.build(), new StreamObserver<GetUserInfoResponse>() {
          GetUserInfoResponse ret = null;

          @Override
          public void onNext(GetUserInfoResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger
                .trace("#@ AuthenticationProviderGrpc.getUserInfoAuthStub onNext : {}",
                    value.toString());

            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            Status status = Status.fromThrowable(t);
            logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
                t, Dispatcher.threadLocal.get());

            String msg = MessageFormat.format("getUserInfo error occurred. {0}:{1}",
                status.getCode(), status.getDescription());// 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

            handler.getDirectiveForwarder()
                .sendException(MapException.StatusCode.GRPC_AUTH_GET_USER_INFO_ERROR, exIndex(15),
                    msg, callEvent, tempPayloadJson);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ AuthenticationProviderGrpc.getUserInfoAuthStub onCompleted.");
            receiveGetUserInfoResponse(handler, ret, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * getUserInfo에 대한 Response 수신 처리 루틴
   */
  private void receiveGetUserInfoResponse(MapEventHandler handler,
      GetUserInfoResponse response, EventStream callEvent) {
    logger.trace("#@ receiveGetUserInfoResponse" + response.toString());

    handler.getDirectiveForwarder()
        .sendStream(makeGetUserinfoResponse(response.getUser(), callEvent));

    handler.getDirectiveForwarder().notifyCompleted(handler);
  }


  private DirectiveStream makeGetUserinfoResponse(maum.m2u.common.UserOuterClass.User response,
      EventStream callEvent) {
    GetUserInfoResponse getUserInfoResponse =
        GetUserInfoResponse.newBuilder()
            .setUser(response)
            .build();

    DirectiveStream dir = DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_AUTHENTICATION)
            .setOperation(MapIf.AU_D_GET_USER_INFO_RESPONSE)
            .setType(AsyncInterface.OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(getUserInfoResponse))
        .build();
    return dir;
  }

  /**
   * 사용자 속성 정의 인증된 사용자만 이 호출을 진행할 수 있다. 바꾸고 싶은 값만 넣을 수 있다. 응답으로는 모든 설정정보를 다 반환한다.
   */
  @Override
  public void OnUpdateUserSettings(MapEventHandler handler, EventStream callEvent) {
    logger.info("#@ IN OnUpdateUserSettings");
    Struct.Builder meta = null;

    try {
      meta = callEvent.getPayload().toBuilder();
      logger.trace("#@ OnUpdateUserSettings getPayload : {}",
          JsonFormat.printer().includingDefaultValueFields().print(meta));
    } catch (InvalidProtocolBufferException e) {
      logger
          .error("#@ OnUpdateUserSettings InvalidProtocolBufferException exception : {} => ",
              e.getMessage(), e);

      Status st = Status.fromThrowable(e);
      String msg = MessageFormat.format("UpdateUserSettings json print error occurred. {0}:{1}",
          st.getCode(), st.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + st.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(16),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    this.updateUserSettingsStub(meta, handler, callEvent);
  }

  // #@ AuthModule과 updateUserSettings GRPC 통신
  private void updateUserSettingsStub(Struct.Builder meta, MapEventHandler handler,
      EventStream callEvent) {
    logger.info("#@ updateUserSettingsStub ");

    UserSettings.Builder param = UserSettings.newBuilder();

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());

    AuthenticationProviderGrpc.AuthenticationProviderStub stub = AuthenticationProviderGrpc
        .newStub(channel);

    try {
      json = JsonFormat.printer().includingDefaultValueFields()
          .print(meta);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("updateUserSettings json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(17),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .updateUserSettings(param.build(), new StreamObserver<UserSettings>() {
          UserSettings ret = null;

          @Override
          public void onNext(UserSettings updateUserSettingsResponse) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ AuthenticationProviderGrpc.updateUserSettings onNext : {}",
                updateUserSettingsResponse.toString());

            ret = updateUserSettingsResponse;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            Status status = Status.fromThrowable(t);
            logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
                t, Dispatcher.threadLocal.get());

            String msg = MessageFormat.format("updateUserSettings error occurred. {0}:{1}",
                status.getCode(), status.getDescription());
            // 인증에서 준 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'authException': '" + status.toString() + "'}";

            handler.getDirectiveForwarder()
                .sendException(MapException.StatusCode.GRPC_AUTH_UPDATE_USER_SETTINGS_ERROR,
                    exIndex(18),
                    msg, callEvent, tempPayloadJson);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ AuthenticationProviderGrpc.updateUserSettings onCompleted.");

            receiveUpdateUserSettingsResult(handler, ret, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * updateUserSettings 대한 Response 수신 처리 루틴
   */
  private void receiveUpdateUserSettingsResult(MapEventHandler handler, UserSettings response,
      EventStream callEvent) {
    logger.trace("#@ receiveUpdateUserSettingsResult [{}]", response.toString());

    handler.getDirectiveForwarder()
        .sendStream(makeUpdateUserSettingsResult(response, callEvent));

    handler.getDirectiveForwarder().notifyCompleted(handler);
  }

  private DirectiveStream makeUpdateUserSettingsResult(UserSettings response,
      EventStream callEvent) {
    UserSettings.Builder updateUserSettings = response.toBuilder();

    DirectiveStream dir = DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_AUTHENTICATION)
            .setOperation(MapIf.AU_D_UPDATE_USER_SETTINGS_RESULT)
            .setType(AsyncInterface.OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(updateUserSettings.build()))
        .build();
    return dir;
  }

  /**
   * 사용자 속성 조회 인증된 사용자만 이 호출을 진행할 수 있다. 설정정보를 조회한다.
   */
  @Override
  public void OnGetUserSettings(MapEventHandler handler, EventStream callEvent) {
    logger.info("#@ IN OnGetUserSettings");
    Struct.Builder meta = null;

    try {
      meta = callEvent.getPayload().toBuilder();
      logger.trace("#@ OnGetUserSettings getPayload : {}",
          JsonFormat.printer().includingDefaultValueFields().print(meta));

    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("GetUserSettings json print error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(19),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    this.getUserSettingsStub(meta, handler, callEvent);

    return;
  }

  // #@ AuthModule과 updateUserSettings GRPC 통신
  private void getUserSettingsStub(Struct.Builder meta, MapEventHandler handler,
      EventStream callEvent) {
    logger.info("#@ getUserSettingsStub ");

    UserKey.Builder param = UserKey.newBuilder();

    final ManagedChannel channel = DialogUtils
        .getChannel(ServerAuthInterceptor.AUTH_SERVER_IP.get());

    AuthenticationProviderGrpc.AuthenticationProviderStub stub = AuthenticationProviderGrpc
        .newStub(channel);

    try {
      json = JsonFormat.printer().includingDefaultValueFields()
          .print(meta);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      String msg = MessageFormat.format("getUserSettings json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(20),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ 서버에서 보내는 메시지를 처리하기 위한 옵져버 생성
    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .getUserSettings(param.build(), new StreamObserver<UserSettings>() {
          UserSettings ret = null;

          @Override
          public void onNext(UserSettings getUserSettingsResponse) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ AuthenticationProviderGrpc.getUserSettings onNext : {}",
                getUserSettingsResponse.toString());

            ret = getUserSettingsResponse;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            Status status = Status.fromThrowable(t);
            logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
                t, Dispatcher.threadLocal.get());

            String msg = MessageFormat.format("getUserSettings error occurred. {0}:{1}",
                status.getCode(), status.getDescription());
            // 인증에서 준 에러코드를 말아서 그대로 json string으로 전달한다
            String tempPayloadJson = "{'authException': '" + status.toString() + "'}";

            handler.getDirectiveForwarder()
                .sendException(MapException.StatusCode.GRPC_AUTH_GET_USER_SETTINGS_ERROR,
                    exIndex(21),
                    msg, callEvent, tempPayloadJson);
            handler.getDirectiveForwarder().notifyCompleted(handler);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ AuthenticationProviderGrpc.getUserSettings onCompleted.");

            receiveGetUserSettingsResult(handler, ret, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * getUserSettings 대한 Response 수신 처리 루틴
   */
  private void receiveGetUserSettingsResult(MapEventHandler handler, UserSettings
      response, EventStream callEvent) {
    logger.trace("#@ receiveGetUserSettingsResult [{}]", response.toString());

    handler.getDirectiveForwarder()
        .sendStream(makeGetUserSettingsResult(response, callEvent));

    handler.getDirectiveForwarder().notifyCompleted(handler);
  }

  private DirectiveStream makeGetUserSettingsResult(UserSettings response, EventStream callEvent) {
    UserSettings.Builder getUserSettings = response.toBuilder();

    DirectiveStream dir = DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_AUTHENTICATION)
            .setOperation(MapIf.AU_D_GET_USER_SETTINS_RESULT)
            .setType(AsyncInterface.OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(getUserSettings.build()))
        .build();
    return dir;
  }
}
