package ai.maum.m2u.map.util;

import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;

public final class MapIf {

  static public AsyncInterface newAsyncInterface(String ifname,
      String opname,
      boolean stream,
      OperationType type) {
    return AsyncInterface.newBuilder()
        .setInterface(ifname)
        .setOperation(opname)
        .setStreaming(stream)
        .setType(type)
        .build();
  }

  static public final String I_DIALOG = "Dialog";
  static public final String DL_E_SPEECH_TO_SPEECH_TALK = "SpeechToSpeechTalk";
  static public final String DL_E_SPEECH_TO_TEXT_TALK = "SpeechToTextTalk";
  static public final String DL_E_TEXT_TO_TEXT_TALK = "TextToTextTalk";
  static public final String DL_E_TEXT_TO_SPEECH_TALK = "TextToSpeechTalk";
  static public final String DL_E_IMAGE_TO_TEXT_TALK = "ImageToTextTalk";
  static public final String DL_E_IMAGE_TO_SPEECH_TALK = "ImageToSpeechTalk";
  static public final String DL_D_PROCESSING = "Processing";
  static public final String DL_E_REPORT_TALK_SATISFACTION = "ReportTalkSatisfaction";
  static public final String DL_E_HELP = "Help";
  static public final String DL_E_CLOSE = "Close";
  static public final String DL_E_OPEN = "Open";

  static public final String I_DIALOG_WEB = "DialogWeb";
  static public final String DLW_E_OPEN = "Open";
  static public final String DLW_E_CLOSE = "Close";
  static public final String DLW_E_TEXT_TO_TEXT_TALK = "TextToTextTalk";

  static public final String I_DIALOG_AGENT = "DialogAgent";
  static public final String DA_E_EVENT = "Event";

  static public final String I_VIEW = "View";
  static public final String VW_D_RENDER_TEXT = "RenderText";
  static public final String VW_D_RENDER_CARD = "RenderCard";
  static public final String VW_D_RENDER_CONTENT = "RenderContent";
  static public final String VW_D_RENDER_HIDDEN = "RenderHidden";
  static public final String VW_D_RENDER_CHOICE = "RenderChoice";
  static public final String VW_D_RENDER_SPEECH = "RenderSpeech";
  static public final String VW_D_RENDER_CLOSE = "RenderClose";
  static public final String VW_D_SHOW_SATISPECTION_BOX = "ShowSatispectionBox";
  static public final String VW_D_SET_BACKGROUND = "SetBackground";

  static public final String I_NEXT_JOB = "NextJob";
  static public final String NJ_D_SET = "Set";
  static public final String NJ_D_RESET = "Reset";

  static public final String I_NOTIFIER = "Notifier";
  static public final String NT_D_NOTIFY = "Notify";
  static public final String NT_D_CLEAR = "Clear";

  static public final String I_TOAST = "Toast";
  static public final String TO_D_SHOW = "Show";
  static public final String TO_D_CLEAR = "Clear";

  static public final String I_AVATAR = "Avatar";
  static public final String AT_D_SET_AVATAR = "SetAvatar";
  static public final String AT_D_SET_EXPRESSION = "SetExpression";

  static public final String I_AUTHENTICATION = "Authentication";
  static public final String AU_E_SIGN_IN = "SignIn";
  static public final String AU_D_SIGN_IN_RESULT = "SignInResult";
  static public final String AU_E_MULTI_FACTOR_VERIFY = "MultiFactorVerify";
  static public final String AU_D_MULTI_FACTOR_VERIFY_RESULT = "MultiFactorVerifyResult";
  static public final String AU_E_SIGN_OUT = "SignOut";
  static public final String AU_D_SIGN_OUT_RESULT = "SignOutResult";
  static public final String AU_E_SIGN_ON = "SignOn";
  static public final String AU_D_SIGN_ON_RESULT = "SignOnResult";
  static public final String AU_E_RESET = "Reset";
  static public final String AU_E_IS_VALID = "IsValid";
  static public final String AU_D_IS_VALID_RESPONSE = "IsValidResponse";
  static public final String AU_E_GET_USER_INFO = "GetUserInfo";
  static public final String AU_D_GET_USER_INFO_RESPONSE = "GetUserInfoResponse";
  static public final String AU_E_UPDATE_USER_SETTINGS = "UpdateUserSettings";
  static public final String AU_D_UPDATE_USER_SETTINGS_RESULT = "UpdateUserSettingsResult";
  static public final String AU_E_GET_USER_SETTINS = "GetUserSettings";
  static public final String AU_D_GET_USER_SETTINS_RESULT = "GetUserSettingsResult";

  static public final String I_LAUNCHER = "Launcher";
  static public final String LC_E_LAUNCHER_AUTHORIZED = "Authorized";
  static public final String LC_E_LAUNCHER_AUTHORIZEFAILED = "AuthorizeFailed";
  static public final String LC_E_LAUNCHER_SLOTSTFILLED = "SlotstFilled";

  static public final String I_SPEECH_RECONGNIZER = "SpeechRecognizer";
  static public final String SR_E_RECOGNIZE = "Recognize";
  static public final String SR_D_RECOGNIZING = "Recognizing";
  static public final String SR_D_RECOGNIZED = "Recognized";
  static public final String SR_D_ENDPOINT_DETECTED = "EndpointDetected";

  static public final String I_IMAGE_RECOGNIZER = "ImageRecognizer";
  static public final String IR_E_RECOGNIZE = "Recognize";
  static public final String IR_D_RECOGNIZED = "Recognized";
  static public final String IR_D_FAILED = "Failed";

  static public final String I_IMAGE_DOCUMENT_RECOGNIZER = "ImageDocumentRecognizer";
  static public final String DR_E_RECOGNIZE = "Recognize";
  static public final String DR_D_RECOGNIZED = "Recognized";
  static public final String DR_D_FAILED = "Failed";

  static public final String I_SPEECH_SYNTHESIZER = "SpeechSynthesizer";
  static public final String SS_E_SPEAK = "Speak";
  static public final String SS_D_PLAY = "Play";
  static public final String SS_E_PLAY_STOPPED = "PlayStopped";

  static public final String I_MICROPHONE = "Microphone";
  static public final String MP_D_EXPECT_SPEECH = "ExpectSpeech";
  static public final String MP_E_EXPECT_SPEECH_TIMEOUT = "ExpectSpeechTimedOut";

  static public final String I_AUDIO_PLAYER = "AudioPlayer";
  static public final String AP_D_PLAY = "Play";
  static public final String AP_E_PLAY_FINISHED = "PlayFinished";
  static public final String AP_E_PLAY_PAUSED = "PlayPaused";
  static public final String AP_E_PLAY_RESUMED = "PlayResumed";
  static public final String AP_E_PLAY_STARTED = "PlayStarted";
  static public final String AP_E_PLAY_STOPPED = "PlayStopped";
  static public final String AP_E_PROGRESS_REPORT_DELAY_PASSED = "ProgressReportDelayPassed";
  static public final String AP_E_PROGRESS_REPORT_INTERVAL_PASSED = "ProgressReportIntervalPassed";
  static public final String AP_E_PROGRESS_REPORT_POSITION_PASSED = "ProgressReportPositionPassed";
  static public final String AP_D_STREAM_DELIVER = "StreamDeliver";
  static public final String AP_E_STREAM_REQUESTED = "StreamRequested";

  static public final String I_VIDEO_PLAYER = "VideoPlayer";
  static public final String VP_D_PLAY = "Play";
  static public final String VP_E_PLAY_FINISHED = "PlayFinished";
  static public final String VP_E_PLAY_PAUSED = "PlayPaused";
  static public final String VP_E_PLAY_RESUMED = "PlayResumed";
  static public final String VP_E_PLAY_STARTED = "PlayStarted";
  static public final String VP_E_PLAY_STOPPED = "PlayStopped";
  static public final String VP_E_PROGRESS_REPORT_DELAY_PASSED = "ProgressReportDelayPassed";
  static public final String VP_E_PROGRESS_REPORT_INTERVAL_PASSED = "ProgressReportIntervalPassed";
  static public final String VP_E_PROGRESS_REPORT_POSITION_PASSED = "ProgressReportPositionPassed";
  static public final String VP_D_STREAM_DELIVER = "StreamDeliver";
  static public final String VP_D_STREAM_REQUESTED = "StreamRequested";

  static public final String I_SYSTEM = "System";
  static public final String ST_E_REPORT_DEVICE_STATE = "ReportDeviceState";
  static public final String ST_D_TURN_ON = "TurnOn";
  static public final String ST_D_TURN_OFF = "TurnOff";
  static public final String ST_D_SET_VOLUME = "SetVolume";
  static public final String ST_E_COMMAND_EXECUTED = "CommandExecuted";
  static public final String ST_E_COMMAND_FAILED = "CommandFailed";
  static public final String ST_D_SYNCHRONIZE = "Synchronize";

  static public final String E_SPEECH = "SPEECH";
  static public final String E_TEXT = "TEXT";
/*
  static public final String DL_D_PROCESSING = "Processing";


  static public final String I_VIEW = "View";
  static public final String VW_D_RENDER_TEXT = "RenderText";
  static public final String VW_O_RENDER_CONTENT = "RenderContent";


*/
}
