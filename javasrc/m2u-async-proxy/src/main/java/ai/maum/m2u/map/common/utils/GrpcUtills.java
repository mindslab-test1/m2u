package ai.maum.m2u.map.common.utils;

import ai.maum.config.PropertyManager;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import maum.m2u.map.Map.EventStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GrpcUtills {

  static final Logger logger = LoggerFactory.getLogger(GrpcUtills.class);

  public static final int grpcDeadLineSec = PropertyManager.getInt("map.grpc.deadline");

  public static Metadata makeOperationSyncIdHeader(EventStream callEvent) {
    logger.info("#@ makeOperationSyncIdHeader x-operation-sync-id");
    // GRPC 헤더에 x-operation-sync-id 추가
    Metadata fixedHeaders = new Metadata();
    Metadata.Key<String> key = Metadata.Key
        .of("x-operation-sync-id", Metadata.ASCII_STRING_MARSHALLER);
    fixedHeaders.put(key, callEvent.getOperationSyncId());

    return fixedHeaders;
  }

  public static void closeChannel(ManagedChannel channel) {
    logger.info("Call close channel shutdown awaitTermination");
    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      logger.error("#@ closeChannel Exception [{}]", e);
    }
  }
}
