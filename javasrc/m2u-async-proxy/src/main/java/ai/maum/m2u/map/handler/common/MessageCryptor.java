package ai.maum.m2u.map.handler.common;

import com.google.protobuf.ByteString;

public interface MessageCryptor {

  /**
   * 암호화를 수행합니다
   * 암호화 수행 결과를 전달합니다
   *
   * @param clearMsg 암호화 대상 원본 메시지
   * @return 암호화된 메시지
   */
  public ByteString encode(ByteString clearMsg);

  /**
   * String 평문을 입력받아 암호화 된 String 값을 리턴합니다
   * 헤더 정보를 암호화에 사용됨니다
   *
   * @param clearMsg
   * @return
   */
  public String encode(String clearMsg);

  /**
   * 복호화를 수행합니다
   * 복호화 수행 결과를 전달합니다
   *
   * @param encryptMsg 복호화 대상 암호화된 메시지
   * @return 원본 메시지
   */
  public ByteString decode(ByteString encryptMsg);

  /**
   * 암호화 된 String을 입력받아 복화된 String 평문 값을 리턴합니다
   * 헤더 정보를 복호화에 사용됨니다
   *
   * @param encryptMsg
   * @return
   */
  public String decode(String encryptMsg);
}
