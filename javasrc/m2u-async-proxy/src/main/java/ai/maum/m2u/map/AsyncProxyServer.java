package ai.maum.m2u.map;


import ai.maum.config.PropertyManager;
import ai.maum.m2u.map.builtin.EchoHandler;
import ai.maum.m2u.map.builtin.EchoStreamHandler;
import ai.maum.m2u.map.handler.audioplayer.AudioPlayerHandler;
import ai.maum.m2u.map.handler.auth.GetUserInfoHandler;
import ai.maum.m2u.map.handler.auth.GetUserSettingsHandler;
import ai.maum.m2u.map.handler.auth.IsVaildHandler;
import ai.maum.m2u.map.handler.auth.MultiFactorVerifyHandler;
import ai.maum.m2u.map.handler.auth.SignInHandler;
import ai.maum.m2u.map.handler.auth.SignOnHandler;
import ai.maum.m2u.map.handler.auth.SignOutHandler;
import ai.maum.m2u.map.handler.auth.UpdateUserSettingsHandler;
import ai.maum.m2u.map.handler.common.EventHandlerPool;
import ai.maum.m2u.map.handler.dialog.CloseHandler;
import ai.maum.m2u.map.handler.dialogagent.EventHandler;
import ai.maum.m2u.map.handler.dialog.ImageToSpeechTalkHandler;
import ai.maum.m2u.map.handler.dialog.ImageToTextTalkHandler;
import ai.maum.m2u.map.handler.dialog.OpenHandler;
import ai.maum.m2u.map.handler.dialog.SpeechToSpeechTalkHandler;
import ai.maum.m2u.map.handler.dialog.SpeechToTextTalkHandler;
import ai.maum.m2u.map.handler.dialog.TextToSpeechTalkHandler;
import ai.maum.m2u.map.handler.dialog.TextToTextTalkHandler;
import ai.maum.m2u.map.handler.launcher.AuthorizeFailedHandler;
import ai.maum.m2u.map.handler.launcher.AuthorizedHandler;
import ai.maum.m2u.map.handler.launcher.SlotstFilledHandler;
import ai.maum.m2u.map.util.MapIf;
import ai.maum.rpc.ResultStatus;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import maum.rpc.Status;
import maum.rpc.Status.ProcessOfM2u;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * map server를 main process
 */
public class AsyncProxyServer {

  static final Logger logger = LoggerFactory.getLogger(AsyncProxyServer.class);

  static {
    ResultStatus.setModuleAndProcess(Status.Module.M2U, ProcessOfM2u.M2U_MAP.getNumber());
  }

  private Server mapServer;
  private Server secureServer;
  private Server forwarderServer;
  private Server internalMapServer;

  /**
   * Server의 모드를 설정합니다
   */
  private enum ServerMode {
    MAP,
    FORWARDER,
    SECURE
  }


  /**
   * server를 시작합니다.
   *
   * MAP, FORWARDER, SECURE
   */
  private void start(ServerMode serverMode) throws IOException {
    switch (serverMode) {
      case MAP:
      default:
        processMaumToYouProxyService(serverMode);
        break;
      case FORWARDER:
        processMapForwarderProxyService();
        break;
      case SECURE:
        // Secure 모드로 동작하면 MAP과 Secure가 동시에 구동됩니다
        // Secure Port:9912, MAP Port:9911
        processSecureMapProxyService();
        processMaumToYouProxyService(serverMode);
        break;
    }
  }

  /**
   * MAP을 구동합니다 Map은 Client로 부터 수신한 Event를 Router로 전달합니다
   *
   * @throws IOException 처리 중에 IO 예외 발생
   */
  private void processMaumToYouProxyService(ServerMode serverMode) throws IOException {
    logger.info("m2u map server starting");

    //NameSpaceInitializer.initializing();
    int port = PropertyManager.getInt("map.export.port");

    EventHandlerPool pool = EventHandlerPool.getInstance();
    EchoHandler echoHandler = new EchoHandler();
    pool.register(echoHandler.getInterface(), echoHandler.getOperation(), echoHandler.getClass());
    EchoStreamHandler echoStreamHandler = new EchoStreamHandler();
    pool.register(echoStreamHandler.getInterface()
        , echoStreamHandler.getOperation()
        , echoStreamHandler.getClass());

    pool.register(MapIf.I_DIALOG, MapIf.DL_E_OPEN, OpenHandler.class);
    pool.register(MapIf.I_DIALOG, MapIf.DL_E_CLOSE, CloseHandler.class);
    pool.register(MapIf.I_DIALOG, MapIf.DL_E_SPEECH_TO_SPEECH_TALK,
        SpeechToSpeechTalkHandler.class);
    pool.register(MapIf.I_DIALOG, MapIf.DL_E_SPEECH_TO_TEXT_TALK, SpeechToTextTalkHandler.class);
    pool.register(MapIf.I_DIALOG, MapIf.DL_E_TEXT_TO_TEXT_TALK, TextToTextTalkHandler.class);
    pool.register(MapIf.I_DIALOG, MapIf.DL_E_TEXT_TO_SPEECH_TALK, TextToSpeechTalkHandler.class);
    pool.register(MapIf.I_DIALOG, MapIf.DL_E_IMAGE_TO_TEXT_TALK, ImageToTextTalkHandler.class);
    pool.register(MapIf.I_DIALOG, MapIf.DL_E_IMAGE_TO_SPEECH_TALK, ImageToSpeechTalkHandler.class);

    pool.register(MapIf.I_DIALOG_WEB, MapIf.DLW_E_OPEN, OpenHandler.class);
    pool.register(MapIf.I_DIALOG_WEB, MapIf.DLW_E_CLOSE, CloseHandler.class);
    pool.register(MapIf.I_DIALOG_WEB, MapIf.DLW_E_TEXT_TO_TEXT_TALK, TextToTextTalkHandler.class);

    pool.register(MapIf.I_DIALOG_AGENT, MapIf.DA_E_EVENT, EventHandler.class);

    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_PLAY_FINISHED, AudioPlayerHandler.class);
    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_PLAY_PAUSED, AudioPlayerHandler.class);
    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_PLAY_RESUMED, AudioPlayerHandler.class);
    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_PLAY_STARTED, AudioPlayerHandler.class);
    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_PLAY_STOPPED, AudioPlayerHandler.class);
    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_PROGRESS_REPORT_DELAY_PASSED,
        AudioPlayerHandler.class);
    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_PROGRESS_REPORT_INTERVAL_PASSED,
        AudioPlayerHandler.class);
    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_PROGRESS_REPORT_POSITION_PASSED,
        AudioPlayerHandler.class);
    pool.register(MapIf.I_AUDIO_PLAYER, MapIf.AP_E_STREAM_REQUESTED, AudioPlayerHandler.class);

    // #@ Auth
    pool.register(MapIf.I_AUTHENTICATION, MapIf.AU_E_SIGN_IN, SignInHandler.class);
    pool.register(MapIf.I_AUTHENTICATION, MapIf.AU_E_MULTI_FACTOR_VERIFY,
        MultiFactorVerifyHandler.class);
    pool.register(MapIf.I_AUTHENTICATION, MapIf.AU_E_SIGN_OUT, SignOutHandler.class);
    pool.register(MapIf.I_AUTHENTICATION, MapIf.AU_E_SIGN_ON, SignOnHandler.class);
    pool.register(MapIf.I_AUTHENTICATION, MapIf.AU_E_IS_VALID, IsVaildHandler.class);
    pool.register(MapIf.I_AUTHENTICATION, MapIf.AU_E_GET_USER_INFO, GetUserInfoHandler.class);
    pool.register(MapIf.I_AUTHENTICATION, MapIf.AU_E_UPDATE_USER_SETTINGS,
        UpdateUserSettingsHandler.class);
    pool.register(MapIf.I_AUTHENTICATION, MapIf.AU_E_GET_USER_SETTINS,
        GetUserSettingsHandler.class);

    // #@ Launcher
    pool.register(MapIf.I_LAUNCHER, MapIf.LC_E_LAUNCHER_AUTHORIZED, AuthorizedHandler.class);
    pool.register(MapIf.I_LAUNCHER, MapIf.LC_E_LAUNCHER_AUTHORIZEFAILED,
        AuthorizeFailedHandler.class);
    pool.register(MapIf.I_LAUNCHER, MapIf.LC_E_LAUNCHER_SLOTSTFILLED, SlotstFilledHandler.class);

    logger.info("#@ START NettyServerBuilder port[{}]", port);
    NettyServerBuilder sb = NettyServerBuilder.forPort(port);
    MaumToYouProxyServiceImpl impl = new MaumToYouProxyServiceImpl();
    sb.addService(impl);
    sb.addService(ProtoReflectionService.newInstance());
    sb.intercept(new ServerAuthInterceptor());
    sb.maxConnectionIdle(PropertyManager.getInt("map.server.max.connection.idlesec"),
        TimeUnit.SECONDS);
    mapServer = sb.build().start();

    if (serverMode == ServerMode.SECURE) {
      // internal server 실행
      ServerBuilder internalSb = InProcessServerBuilder.forName("internal-map");
      internalSb.addService(new MaumToYouProxyServiceImpl());
      internalSb.intercept(new ServerAuthInterceptor());
      internalMapServer = internalSb.build().start();
      logger.debug("m2u internal-map server started [{}]", internalMapServer);
    }

    logger.info("m2u map server started, listening on port = {}", port);
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      // Use stderr here since the logger may have been reset by its
      // JVM close hook.
      logger.info("*** shutting down gRPC server since JVM is shutting down");
      AsyncProxyServer.this.close();
      logger.info("*** server shut down");
    }));

    // sessionManger Thread를 시작합니다
    SessionManager sessionManager = SessionManager.getInstance();
    sessionManager.run();
  }

  /**
   * MapForwarder Proxy를 구동합니다 MapForwarder은 Secure 앞단에서 Client로 부터 수신한 Event를 암호화 하여 Secure로 전달합니다
   *
   * @throws IOException 서버 시작 중에 IO 예외 발생 가능
   */
  private void processMapForwarderProxyService() throws IOException {
    logger.info("map forwarder server starting");

    // MapForwarderProxy port 정보
    int port = PropertyManager.getInt("map.forwarder.proxy.port");
    // ServerBuilder 객체 생성
    ServerBuilder sb = ServerBuilder.forPort(port);
    // MapForwarderProxyServiceImpl 객체 생성
    MapForwarderProxyServiceImpl mapForwarderImpl = new MapForwarderProxyServiceImpl();
    sb.addService(mapForwarderImpl);
    sb.addService(ProtoReflectionService.newInstance());
    // MapForwarderInterceptor 주입
    sb.intercept(new MapForwarderInterceptor());

    forwarderServer = sb.build().start();
    logger.info("MAP Forwarder server started, listening on port = {}", port);

    // addShutdownHook 설정
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      // Use stderr here since the logger may have been reset by its
      // JVM close hook.
      logger.info("*** MAP Forwarder shutting down gRPC server since JVM is shutting down");
      AsyncProxyServer.this.close();
      logger.info("*** MAP Forwarder server shut down");
    }));
  }

  /**
   * SecureMap Proxy를 구동합니다 SecureMap은 Map 앞단에서 Forwarder로 부터 수신한 Event를 복호화 하여 MAP으로 전달합니다
   * SecureMap은 MAP과 함께 구동됩니다
   *
   * [Client] - [MapForwarder] - [SecureMap-MAP] 으로 구성되며 SecureMap에서 MAP으로 channel 생성시
   * InProcessChannelBuilder를 사용하여 전달합니다
   *
   * @throws IOException 서버 시작 중에 예외 발생 가능
   */
  private void processSecureMapProxyService() throws IOException {
    logger.info("Secure Map Proxy server starting");

    // SecureMapProxy port 정보
    int port = PropertyManager.getInt("secure.map.proxy.port");

    // ServerBuilder 객체 생성
    ServerBuilder sb = ServerBuilder.forPort(port);
    // SecureMapProxyServiceImpl 객체 생성
    SecureMapProxyServiceImpl secureMapProxyServiceImpl = new SecureMapProxyServiceImpl();
    sb.addService(secureMapProxyServiceImpl);
    sb.addService(ProtoReflectionService.newInstance());
    // SecureMapInterceptor 주입
    sb.intercept(new SecureMapInterceptor());

    secureServer = sb.build().start();
    logger.info("Secure MAP server started, listening on port = {}", port);

    // addShutdownHook 설정
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      // Use stderr here since the logger may have been reset by its
      // JVM close hook.
      logger.info("*** Secure MAP shutting down gRPC server since JVM is shutting down");
      AsyncProxyServer.this.close();
      logger.info("*** Secure MAP Forwarder server shut down");
    }));
  }

  /**
   * 명시적인 종료 처리
   */
  private void close() {
    if (mapServer != null) {
      logger.info("mapServer.close()");
      mapServer.shutdown();

      try {
        mapServer.awaitTermination(5, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        logger.error("{}", e);
      }
    }
    if (secureServer != null) {
      logger.info("secureServer.close()");
      secureServer.shutdown();

      try {
        secureServer.awaitTermination(5, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        logger.error("{}", e);
      }
    }
    if (forwarderServer != null) {
      logger.info("forwarderServer.close()");
      forwarderServer.shutdown();

      try {
        forwarderServer.awaitTermination(5, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        logger.error("{}", e);
      }
    }
    if (internalMapServer != null) {
      logger.info("internalMapServer.close()");
      internalMapServer.shutdown();

      try {
        internalMapServer.awaitTermination(5, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        logger.error("{}", e);
      }
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
    if (mapServer != null) {
      logger.info("mapServer.awaitTermination()");
      mapServer.awaitTermination();
    }
    if (secureServer != null) {
      logger.info("secureServer.awaitTermination()");
      secureServer.awaitTermination();
    }
    if (forwarderServer != null) {
      logger.info("forwarderServer.awaitTermination()");
      forwarderServer.awaitTermination();
    }
    if (internalMapServer != null) {
      logger.info("internalMapServer.awaitTermination()");
      internalMapServer.awaitTermination();
    }
  }


  /**
   * m2u map server main
   *
   * @param args arguments
   */
  public static void main(String[] args) throws IOException, InterruptedException {
    final AsyncProxyServer server = new AsyncProxyServer();

    logger.debug("#@ main start [{}]", args.length);
    logger.debug("main args length: {} values: [{}]", args.length, args);

    // default는 MAP 입니다
    String mode = "MAP";
    if (args.length >= 1) {
      mode = args[0];
    }

    ServerMode msm = ServerMode.valueOf(mode);
    logger.debug("#@ emode [{}]", msm);

    // main의 String[] args 인자로 구별합니다. args의 값이 없으면 기본 MAP으로 구동합니다
    server.start(msm);
    server.blockUntilShutdown();
  }
}
