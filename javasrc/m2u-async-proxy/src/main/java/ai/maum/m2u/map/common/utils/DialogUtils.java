package ai.maum.m2u.map.common.utils;

import static ai.maum.config.PropertyManager.getInt;
import static ai.maum.config.PropertyManager.getString;
import static com.google.protobuf.TextFormat.printToUnicodeString;

import ai.maum.config.Keys;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.common.user.UserParam;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import com.jayway.jsonpath.JsonPath;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import maum.brain.idr.Idr.ImageRecognitionResult;
import maum.brain.stt.Speech.StreamingRecognizeResponse;
import maum.brain.stt.Speech.StreamingRecognizeResponse.SpeechEventType;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.NgTtsServiceGrpc.NgTtsServiceStub;
import maum.brain.tts.Speech.SpeakRequest;
import maum.brain.tts.Speech.SpeakResponse;
import maum.brain.tts.Speech.SpeechSynthesizerParam;
import maum.brain.tts.SpeechSynthesizerServiceGrpc;
import maum.brain.tts.SpeechSynthesizerServiceGrpc.SpeechSynthesizerServiceStub;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import maum.common.Audioencoding.AudioEncoding;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.CardOuterClass;
import maum.m2u.common.CardOuterClass.Card;
import maum.m2u.common.DeviceOuterClass;
import maum.m2u.common.DeviceOuterClass.Device;
import maum.m2u.common.Dialog;
import maum.m2u.common.Dialog.Utter.InputType;
import maum.m2u.common.DirectiveOuterClass.DelegateDirective;
import maum.m2u.common.DirectiveOuterClass.Directive;
import maum.m2u.common.Job;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.DirectiveStream.DirectiveParam;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapException;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamBreak.StreamBreaker;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Payload.ExpectSpeechPayload;
import maum.m2u.map.Payload.RenderTextPayload;
import maum.m2u.router.v3.Router;
import maum.m2u.router.v3.Router.TalkRouteDialogDelegate;
import maum.m2u.router.v3.Router.TalkRouteResponse;
import maum.m2u.router.v3.Router.TalkRouteSpeechResponse;
import maum.m2u.router.v3.TalkRouterGrpc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

@SuppressWarnings("WeakerAccess")
public class DialogUtils {

  private static final Logger logger = LoggerFactory.getLogger(DialogUtils.class);

  private static final int EX_INDEX = 200;

  private static int exIndex(int index) {
    return EX_INDEX + index;
  }

  /**
   * UserParam 생성
   */
  public static UserParam createUserParam(EventStream callEvent) {
    logger.info("#@ START createUserParam");
    Device device = null;
    Location location = null;
//    callEvent.getContextsList();

    for (int i = 0; i < callEvent.getContextsCount(); i++) {
      if (callEvent.getContexts(i).hasDevice()) {
        device = DialogUtils.copyDevice(callEvent.getContexts(i).getDevice());
      } else if (callEvent.getContexts(i).hasLocation()) {
        location = DialogUtils.copyLocation(callEvent.getContexts(i).getLocation());
      }
    }

    if (device == null) {
      logger.debug("#@ device is null");
      device = Device.newBuilder().build();
    }

    if (location == null) {
      logger.debug("#@ location is null");
      location = Location.newBuilder().build();
    }

    UserParam userParam = new UserParam();

    logger.debug("#@ create userParam [{}]", device.getId());

    userParam.device = device;
    userParam.location = location;

    logger.debug("#@ device [{}], location [{}]"
        , printToUnicodeString(userParam.device)
        , printToUnicodeString(userParam.location));

    return userParam;
  }

  public static ManagedChannel getChannel(String target) {
    logger.info("#@ target is [{}]", target);
    int pos = target.indexOf(':');
    String addr = target.substring(0, pos);
    String port = target.substring(pos + 1);
    int p = Integer.parseInt(port);

    return ManagedChannelBuilder.forAddress(addr, p)
        .usePlaintext()
        .build();
  }

  public static Struct messageToStruct(Message msg) {
    Struct.Builder stb = Struct.newBuilder();

    try {
      String json = JsonFormat.printer()
          .includingDefaultValueFields()
          .omittingInsignificantWhitespace()
          .print(msg);
      logger.trace("#@ msg to struct json  = [{}]", json);
      JsonFormat.parser().ignoringUnknownFields().merge(json, stb);
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("Error status {}:{}", status.getCode(), status.getDescription());
      logger.error("Thread local {}", Dispatcher.threadLocal.get());
      logger.error("", e);
    }

    return stb.build();
  }

  /**
   * MAP.Device --> Router.Device 복사
   */
  public static Device copyDevice(Device param) {
    return Device.newBuilder()
        .mergeFrom(param)
        .build();
  }

  /**
   * MAP.Location --> Router.Location 복사
   */
  public static Location copyLocation(Location param) {
    return Location.newBuilder()
        .mergeFrom(param)
        .build();
  }

  /**
   * Stt로 부터 Recognized 수신 받은 transcript를 talkUtter에 설정해 주는 함수
   */
  public static Dialog.Utter makeTalkUtterForStt(EventStream event, String transcript, int langVal)
      throws NullPointerException {

    transcript = transcript.trim();
    logger.debug("#@ makeTalkUtterForStt trimed transcript [{}]", transcript);
    // Struct.Builder meta = event.getPayload().toBuilder();

    // STT를 통한 입력
    String inputType = "SPEECH";

    Lang lang = Lang.forNumber(langVal);
    logger.debug("#@ makeTalkUtterForStt transcript [{}], inputType [{}], lang [{}]", transcript,
        inputType, lang);

    // tempMeta 생성 (chatbot, lang, utter, skill을 제외)
    Struct tempMeta = Struct.newBuilder()
        .mergeFrom(event.getPayload())
        .removeFields("chatbot")
        .removeFields("lang")
        .removeFields("utter")
        .removeFields("skill")
        .build();

    Dialog.Utter param;

    // tempMeta가 있다면
    if (tempMeta.getFieldsCount() > 0) {
      param = Dialog.Utter.newBuilder()
          .setLang(lang)
          .setUtter(transcript)
          .setInputType(InputType.valueOf(inputType))
          .setMeta(tempMeta)
          .build();
    } else {  // chatbot, lang, utter, skill을 제외 후 tempMeta가 없다면 setMeta 하지 않는다
      param = Dialog.Utter.newBuilder()
          .setLang(lang)
          .setUtter(transcript)
          .setInputType(InputType.valueOf(inputType))
          .build();
    }

    return param;
  }

  /**
   * Talk Router 시에 전달될 Utter를 설정 IDR의 경의 기본 Utter 이외에 AltUtter를 전달할 수 있도록 매개변수 추가 meta에 idr로 부터
   * 수신받은 image_class도 추가
   *
   * @param event EventStream 객체
   */
  public static Dialog.Utter makeUtterForIdr(EventStream event,
      ImageRecognitionResult imageRecognitionResult) {

    logger.debug("#@ makeUtterForIdr imageRecognitionResult [{}]", imageRecognitionResult);

    // 1. 이미지 인식의 결과를 보내는 경우
    // TODO 지금은 IMAGE로 고정, 추후 요구사항이 들어오면 IMAGE, IMAGE_DOCUMENT 분기처리 예정
    String inputType = "IMAGE";

    // 2.Lang 설정 (전달된 Lang이 없는 경우 default:ko_KR)
    String tempLang = Lang.ko_KR.name();
    try {
      // ITS 또는 ITT 요청을 수신 받았을때 lang 값이 있다면 적용
      tempLang = event.getPayload().getFieldsOrThrow("lang").getStringValue();
    } catch (Exception e) {
      logger.debug("#@ lang getFieldsOrThrow Exception", e);
    }
    Lang lang = Lang.valueOf(tempLang);

    // 3. utter, altUtter 설정
    // String 배열로 정리한다
    // getAnnotatedTextsList의 첫번째 값을 Utter로 나머지 값은 altUtter로 사용한다
    int indexUtter = 0;
    String tempUtter = "";
    List<String> tempAltUtter = new ArrayList<>();

    java.util.Iterator<String> itr = imageRecognitionResult.getAnnotatedTextsList().iterator();

    while (itr.hasNext()) {
      if (indexUtter == 0) {
        tempUtter = itr.next();
      } else {
        tempAltUtter.add(itr.next());
      }
      indexUtter++;
    }

    tempUtter = tempUtter.trim();
    logger.debug("#@ makeUtterForIdr trimed utter [{}], inputType [{}], lang [{}]"
        , tempUtter, inputType, lang);

    // Utter의 meta에 idr로 부터 수신받은 image_class도 함께 전달한다
    Struct tempStructMeta = Struct.newBuilder()
        .mergeFrom(imageRecognitionResult.getMeta())
        .putFields("image_class",
            Value.newBuilder().setStringValue(imageRecognitionResult.getImageClass()).build())
        .build();

    // idr의 경우 meta값이 ImageRecognitionResult로 부터 전달된다
    Dialog.Utter param = Dialog.Utter.newBuilder()
        .setLang(lang)
        .setUtter(tempUtter)
        .addAllAltUtters(tempAltUtter)
        .setInputType(InputType.valueOf(inputType))
        .setMeta(tempStructMeta)
        .build();

    logger.debug("#@ makeUtterForIdr param [{}]", param);

    return param;
  }

  public static DirectiveStream makeDialogRenderText(String utter, EventStream callEvent) {
    RenderTextPayload.Builder renderTextPayload = RenderTextPayload.newBuilder().setText(utter);

    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_VIEW)
            .setOperation(MapIf.VW_D_RENDER_TEXT)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(renderTextPayload.build()))
        .build();
  }

  public static DirectiveStream makeDialogDirective(Directive directive, EventStream callEvent) {
    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(directive.getInterface())
            .setOperation(directive.getOperation())
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setParam(DirectiveParam.newBuilder().setDaForwardParam(directive.getParam()))
        .setPayload(DialogUtils.messageToStruct(directive.getPayload()))
        .build();
  }

  public static DirectiveStream makeDialogDelegateDirective(DelegateDirective directive,
      EventStream callEvent) {
    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(directive.getInterface())
            .setOperation(directive.getOperation())
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setParam(DirectiveParam.newBuilder().setDaForwardParam(directive.getParam()))
        .setPayload(DialogUtils.messageToStruct(directive.getPayload()))
        .build();
  }

  public static DirectiveStream makeDialogRenderCard(Card tempCard, EventStream callEvent) {
    CardOuterClass.Card.Builder card = tempCard.toBuilder();

    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_VIEW)
            .setOperation(MapIf.VW_D_RENDER_CARD)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(card.build()))
        .build();
  }

  // #@ renderContent (for samsung)
  public static DirectiveStream makeDialogRenderHidden(Struct payload, EventStream callEvent) {
    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_VIEW)
            .setOperation(MapIf.VW_D_RENDER_HIDDEN)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(payload))
        .build();
  }

  public static DirectiveStream makeDialogNextJob(Job job, EventStream callEvent) {
    DirectiveStream.Builder directiveBuilder = DirectiveStream.newBuilder()
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId());

    AsyncInterface.Builder interfaceBuilder = AsyncInterface.newBuilder()
        .setInterface(MapIf.I_NEXT_JOB)
        .setType(OperationType.OP_DIRECTIVE);

    if (job.getJobCase() != Job.JobCase.JOB_NOT_SET) {
      directiveBuilder.setInterface(interfaceBuilder.setOperation(MapIf.NJ_D_SET))
          .setPayload(DialogUtils.messageToStruct(job));
    } else {
      directiveBuilder.setInterface(interfaceBuilder.setOperation(MapIf.NJ_D_RESET));
    }

    return directiveBuilder.build();
  }

  public static DirectiveStream makeMicrophoneExpectSpeech(int timeout, EventStream callEvent) {
    ExpectSpeechPayload expectSpeechPayload = ExpectSpeechPayload.newBuilder()
        .setTimeoutInMilliseconds(timeout)
        .build();

    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_MICROPHONE)
            .setOperation(MapIf.MP_D_EXPECT_SPEECH)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(expectSpeechPayload))
        .build();
  }

  public static DirectiveStream makeSTTRecognizing(
      StreamingRecognizeResponse streamingRes, EventStream callEvent) {
    logger.debug("#@ OUT makeSTTRecognizing");

    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_SPEECH_RECONGNIZER)
            .setOperation(MapIf.SR_D_RECOGNIZING)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(streamingRes))
        .build();
  }

  public static DirectiveStream makeSTTRecognized(
      StreamingRecognizeResponse streamingRecognizeResponse, EventStream callEvent) {
    logger.info("#@ OUT makeSTTRecognized");

    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_SPEECH_RECONGNIZER)
            .setOperation(MapIf.SR_D_RECOGNIZED)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setPayload(DialogUtils.messageToStruct(
            StreamingRecognizeResponse.newBuilder().mergeFrom(streamingRecognizeResponse)
                .setSpeechEventTypeValue(SpeechEventType.END_OF_SINGLE_UTTERANCE_VALUE).build()))
        .build();
  }

  public static DirectiveStream makeImageRecognizerRecognized(ImageRecognitionResult imageRes,
      EventStream callEvent) {
    logger.info("#@ OUT makeImageRecognizerRecognized");

    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_IMAGE_RECOGNIZER)
            .setOperation(MapIf.IR_D_RECOGNIZED)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .setParam(DirectiveStream.DirectiveParam.newBuilder())
        .setPayload(DialogUtils.messageToStruct(imageRes))
        .build();
  }

  public static DirectiveStream makeSpeechRecognizerEndPointDetected(EventStream callEvent) {
    logger.info("#@ OUT makeSpeechRecognizerEndPointDetected");
    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_SPEECH_RECONGNIZER)
            .setOperation(MapIf.SR_D_ENDPOINT_DETECTED)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .build();
  }

  public static DirectiveStream makeDialogProcessing(EventStream callEvent) {
    logger.info("#@ OUT makeDialogProcessing");
    return DirectiveStream.newBuilder()
        .setInterface(AsyncInterface.newBuilder()
            .setInterface(MapIf.I_DIALOG)
            .setOperation(MapIf.DL_D_PROCESSING)
            .setType(OperationType.OP_DIRECTIVE)
            .build())
        .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
        .setStreamId(UUID.randomUUID().toString())
        .setOperationSyncId(callEvent.getOperationSyncId())
        .build();
  }

  /**
   * Open Router 시에 전달될 Utter를 설정
   *
   * @param callEvent EventStream 객체
   */
  public static Dialog.Utter makeOpenUtter(EventStream callEvent, MapEventHandler handler)
      throws NullPointerException {
    Struct.Builder meta = callEvent.getPayload().toBuilder();

    try {
      String utter = meta.getFieldsOrThrow("utter").getStringValue();
      utter = utter.trim();
      logger.debug("#@ OnDialogOpen getUtter : {}", utter);
      // #@ utter에 공백을 제거한다
      meta.putFields("utter", Value.newBuilder().setStringValue(utter).build());
    } catch (NullPointerException e) {
      // open에는 utter 값이 optional 입니다
      logger.trace("#@ OnDialogOpen utter(optional) is empty.");
    }

    Dialog.Utter.Builder param = Dialog.Utter.newBuilder();
    try {
      String json = JsonFormat.printer().includingDefaultValueFields().print(meta);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      String msg = MessageFormat.format("json merge error occurred. {0}:{1}"
          , status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      logger.error("{}", msg);
      logger.error("Thread local {}", Dispatcher.threadLocal.get());
      logger.error("", e);

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(1),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    return param.build();
  }

  /**
   * Talk Router 시에 전달될 Utter를 설정
   *
   * @param callEvent EventStream 객체
   */
  public static Dialog.Utter makeTalkUtter(EventStream callEvent, MapEventHandler handler)
      throws NullPointerException {
    Struct.Builder meta = callEvent.getPayload().toBuilder();

    try {
      String inputType = meta.getFieldsOrThrow("inputType").getStringValue();
      logger.debug("#@ OnDialogTextToTextTalk getInputType : {}", inputType);
    } catch (NullPointerException e) {
      Status status = Status.fromThrowable(e);
      String msg = MessageFormat.format("getFieldsOrThrow error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      logger.error("{}", msg);
      logger.error("Thread local {}", Dispatcher.threadLocal.get());
      logger.error("", e);

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(2),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    try {
      String utter = meta.getFieldsOrThrow("utter").getStringValue();
      utter = utter.trim();
      logger.debug("#@ OnDialogTextToTextTalk getUtter : {}", utter);

      // #@ utter에 공백을 제거한다
      meta.putFields("utter", Value.newBuilder().setStringValue(utter).build());
    } catch (NullPointerException e) {
      Status status = Status.fromThrowable(e);
      String msg = MessageFormat.format("getFieldsOrThrow error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      logger.error("{}", msg);
      logger.error("Thread local {}", Dispatcher.threadLocal.get());
      logger.error("", e);

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(3),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    try {
      String lang = meta.getFieldsOrThrow("lang").getStringValue();
      logger.debug("#@ OnDialogTextToTextTalk getLang : {}", lang);
    } catch (NullPointerException e) {
      Status status = Status.fromThrowable(e);
      String msg = MessageFormat.format("getFieldsOrThrow error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      logger.error("{}", msg);
      logger.error("Thread local {}", Dispatcher.threadLocal.get());
      logger.error("", e);

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(4), msg, callEvent,
              tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    Dialog.Utter.Builder param = Dialog.Utter.newBuilder();
    try {
      String json = JsonFormat.printer().includingDefaultValueFields().print(meta);
      JsonFormat.parser().ignoringUnknownFields().merge(json, param);
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      String msg = MessageFormat.format("json merge error occurred. {0}:{1}",
          status.getCode(), status.getDescription());
      // 에러코드를 말아서 그대로 json string으로 전달한다
      String tempPayloadJson = "{'exception': '" + status.toString() + "'}";

      logger.error("{}", msg);
      logger.error("Thread local {}", Dispatcher.threadLocal.get());
      logger.error("", e);

      handler.getDirectiveForwarder()
          .sendException(MapException.StatusCode.MAP_PAYLOAD_ERROR, exIndex(5),
              msg, callEvent, tempPayloadJson);
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }

    return param.build();
  }

  private static void processSpeech(MapEventHandler handler, TalkRouteSpeechResponse response,
      UserParam userParam, EventStream callEvent) {
    // 1.TTS를 이용하여 PCM 을 발송
    // 2.getReprompt가 true이면 ExpectSpeech 발송

    Dialog.Speech speech = response.getSpeech();

    if (speech.getReprompt()) {
      //map은 자동으로 ExepectSpeech Directive를 자동
      DirectiveStream expectspeech = DialogUtils
          .makeMicrophoneExpectSpeech(getInt(Keys.P_DEFAULT_EXPECT_SPEECH), callEvent);
      handler.getDirectiveForwarder().sendStream(expectspeech);
    }

    // #@ resType 이 E_SPEECH인 경우에만 TTS 작동
    //TTS 출력을 위한 데이터
    String ifc = getString(Keys.P_TTS_INTERFACE);
    String utter = !speech.getSpeechUtter().isEmpty() ?
        speech.getSpeechUtter() :
        speech.getUtter();
    if (ifc == null) {
      ifc = "ng";
    }

    logger.debug("#@ utter [{}] interface [{}]", utter, ifc);

    if (ifc.equalsIgnoreCase("legacy")) {
      DialogUtils.speakLegacyTtsStub(handler, utter, userParam, callEvent);
    } else {
      DialogUtils.speakTtsStub(handler, utter, speech.getSpeechTone(), callEvent);
    }
  }

  /**
   * TalkRouter에 대한 Response 수신 처리 루틴
   */
  public static void processTalkRouterResponse(MapEventHandler handler, TalkRouteResponse response,
      UserParam userParam, String resType, EventStream callEvent) {
    logger.trace("#@ processTalkRouterResponse [{}]", printToUnicodeString(response));

    boolean isSpeech = false;

    if (response.hasResponse()) {
      try {
        // #@ 값이 있는지 확인
        TalkRouteSpeechResponse res = response.getResponse();
        List<Card> tempCardList = res.getCardsList();
        List<Directive> tempDirectives = res.getDirectivesList();

        logger.debug(
            "#@ hasSpeech [{}], cardListSize [{}], directivesSize [{}], hasMeta [{}], resType [{}]",
            res.hasSpeech(), tempCardList.size(), tempDirectives.size(), res.hasMeta(), resType);

        // RenderText와 TTS Byte Stream은 따로 전달되야 한다
        // Directive Speech에 대한 RenderText를 전달
        // RenderText는 RenderCard 보다 먼저 전달되어야 한다
        if (res.hasSpeech() && userParam.device.getSupport().getSupportRenderText()) {
          Dialog.Speech speech = res.getSpeech();
          if (!res.getSpeech().getUtter().isEmpty()) {
            handler.getDirectiveForwarder()
                .sendStream(DialogUtils.makeDialogRenderText(speech.getUtter(), callEvent));
          }
        }

        // #@ Card
        if (tempCardList.size() > 0 && userParam.device.getSupport().getSupportRenderCard()) {
          // 1.cardsList만큼 RenderCard로 Directive
          for (Card tempCard : tempCardList) {
            handler.getDirectiveForwarder().sendStream(
                DialogUtils.makeDialogRenderCard(tempCard, callEvent));
          }
        }

        // #@ Directive
        if (tempDirectives.size() > 0) {
          for (Directive directive : tempDirectives) {
            handler.getDirectiveForwarder().sendStream(
                DialogUtils.makeDialogDirective(directive, callEvent));
          }
        }

        // #@ Meta
        if (res.hasMeta()) {
          handler.getDirectiveForwarder().sendStream(
              DialogUtils.makeDialogRenderHidden(res.getMeta(), callEvent));
        }

        // #@ NextJob
        handler.getDirectiveForwarder().sendStream(
            DialogUtils.makeDialogNextJob(response.getNextJob(), callEvent));

        // #@ Directive Speech에 대한 TTS ByteStream 전달
        if (res.hasSpeech()
            && userParam.device.getSupport().getSupportSpeechSynthesizer()) {
          Dialog.Speech speech = res.getSpeech();

          isSpeech = resType.equalsIgnoreCase(MapIf.E_SPEECH)
              && !(speech.getUtter().isEmpty() || speech.getSpeechMute());

          if (isSpeech) {
            DialogUtils.processSpeech(handler, res, userParam, callEvent);
          } else {
            // utter가 없는 경우 TTS 연동 안하도록 예외처리
            logger.debug("Utter is empty or ResType is not 'SPEECH'. Don't connect to TTS Server");
            if (callEvent.getInterface().getStreaming()) {
              handler.getDirectiveForwarder().sendStream(StreamEnd.newBuilder()
                  .setStreamId(UUID.randomUUID().toString())
                  .setEndAt(Timestamps.fromMillis(System.currentTimeMillis()))
                  .build());
            }
          }
        }

      } catch (Exception e) {
        Status status = Status.fromThrowable(e);
        logger.error("error status {}:{}", status.getCode(), status.getDescription());
        logger.error("Thread local {}", Dispatcher.threadLocal.get());
        logger.error("", e);
      }

    } else if (response.hasDelegate()) {
      // #@ 값이 있는지 확인
      TalkRouteDialogDelegate resDelegate = response.getDelegate();
      DelegateDirective delegateDirective = resDelegate.getDirective();
      handler.getDirectiveForwarder().sendStream(DialogUtils
          .makeDialogDelegateDirective(delegateDirective, callEvent));
      // #@ NextJob
      handler.getDirectiveForwarder().sendStream(
          DialogUtils.makeDialogNextJob(response.getNextJob(), callEvent));
    }

    /*
     * SpeechProcess를 타지 않은 경우 notifyCompleted 처리
     * SpeechProcess가 진행되는 경우 SpeechProcess가 내부에서 notifyCompleted 처리 한다
     */
    if (!isSpeech) {
      handler.getDirectiveForwarder().notifyCompleted(handler);
    }
  }

  /**
   * MAP에서 Router로 TalkRouteRequest를 요청하는 함수
   */
  public static void talkRouterStub(UserParam userParam, String resType, EventStream callEvent,
      MapEventHandler handler) {
    //final MapEventHandler final_handler = handler;

    handler.getDirectiveForwarder()
        .sendStream(DialogUtils.makeDialogProcessing(callEvent));

    Router.TalkRouteRequest request = Router.TalkRouteRequest.newBuilder()
        .setUtter(makeTalkUtter(callEvent, handler))
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get())
        )
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc.newStub(channel);

    logger.debug("#@ MAP to Router talkRouterStub = \n [{}]",
        printToUnicodeString(request));
    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .talkRoute(request, new StreamObserver<TalkRouteResponse>() {
          TalkRouteResponse ret = null;

          @Override
          public void onNext(TalkRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ talkRouterStub onNext [{}]", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            CallLogUtills
                .makeTalkRouterCallLog(callEvent, tempOpSyncIdMeta, request, userParam, null,
                    reqDate, new Date());

            ExceptionMsgUtills.sendExceptionFromRouter(
                handler, t, request, reqDate, callEvent, exIndex(6),
                MapException.StatusCode.GRPC_ROUTER_TALK_ERROR,
                "Talk Route error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ talkRouterStub onCompleted");

            CallLogUtills
                .makeTalkRouterCallLog(callEvent, tempOpSyncIdMeta, request, userParam, ret,
                    reqDate, new Date());
            TLOLogUtills.makeTLOLog(request, reqDate, new Date(), 200, exIndex(7));
            processTalkRouterResponse(handler, ret, userParam, resType, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * <p> 1. m2u.conf에 등록되어 있는 map.user.export.speechcontext 값을 이용</p>
   * <p> 2. jsonPath를 사용하여 user의 meta 정보에서 원하는 값을 추출</p>
   **/
  public static List<String> getUserMetaInfo(Struct meta) {
    // 최종 전달
    List<String> resJsonPath = new ArrayList<>();

    // meta 정보가 없다면 중지
    if (meta == null) {
      logger.debug("#@ getUserMetaInfo meta is null");
      return null;
    }

    // 1. User의 meta(google struct) 정보를 순수한 json 형태로 변환합니다
    // jsonPath는 순수한 json 형태만 조회 가능
    String metaToJson;
    try {
      metaToJson = JsonFormat.printer().includingDefaultValueFields().print(meta);
      logger.debug("#@ msg to struct json  = [{}] length [{}]", metaToJson, metaToJson.length());
      // tempMetaTojson의 내용이 비어있다면 중지
      if (metaToJson.isEmpty()) {
        logger.error("#@ getUserMetaInfo tempMetaTojson is null");
        return null;
      }
    } catch (InvalidProtocolBufferException e) {
      logger.error("#@ getUserMetaInfo Exception", e);
      return null;
    }

    // 3.예를 들어 map.user.export.speechcontext = $.rcv_info[*].rcv_a,$.rcv_info[*].b,$.user[*].b
    // 위의 경우, 3번 jsonpath 호출을 하고, 그 결과를 모두 묶어서 하나의 배열로 넘겨준다.
    String tempRawSpeechContext = getString("map.user.export.speechcontext");
    logger.info("#@ Total speechcontext is [{}]", tempRawSpeechContext);
    // map.user.export.speechcontext이 없으면 중지
    if (tempRawSpeechContext == null) {
      logger.debug("#@ getUserMetaInfo map.user.export.speechcontext is null");
      return null;
    }

    String[] tempSpeechContexts = tempRawSpeechContext.split("/");
    // 등록된 speechcontext 만큼 jsonPath를 조회합니다
    for (String speechContext : tempSpeechContexts) {
      logger.debug("#@ Current speechContext is [{}]", speechContext);
      try {
        // List<String> resJsonPathRead = JsonPath.read(metaToJson, "$.rcv_info[*].rcv_acct_alrm");
        List<String> resJsonPathRead = JsonPath.read(metaToJson, speechContext);
        logger.debug("#@ JsonPathRead's Result is [{}]", resJsonPathRead);

        resJsonPath.addAll(resJsonPathRead);
      } catch (Exception e) {
        logger.error("#@ getUserMetaInfo JsonPath.read Exception", e);
        return null;
      }
    }

    logger.debug("#@ Final resJsonPath is [{}]", resJsonPath);
    return resJsonPath;
  }

  /**
   * talkRouterStubForStt는 talkRouterStub과 달리 STT로 부터 수신받은 transcript값을 TalkRouteRequest의 Utter에
   * 설정해준다
   */
  public static void talkRouterStubForStt(UserParam userParam, String transcript, String resType,
      int langVal, EventStream callEvent, MapEventHandler handler) {
    logger.debug("#@ talkRouterStubForStt transcript [{}] resType[{}] langVal[{}]", transcript,
        resType, langVal);

    //final MapEventHandler final_handler = handler;

    // STT로 수신받은 transcript가 빈문자열 또는 없는 경우 SendException 발생
    if (transcript.equalsIgnoreCase("") || transcript.isEmpty()) {
      handler.getDirectiveForwarder().sendException(
          MapException.StatusCode.GRPC_STT_TRANSCRIPT_NULL_ERROR, exIndex(8),
          "There is no stt's transcript", callEvent, null);
      return;
    }

    handler.getDirectiveForwarder().sendStream(DialogUtils.makeDialogProcessing(callEvent));

    Router.TalkRouteRequest request = Router.TalkRouteRequest.newBuilder()
        .setUtter(DialogUtils.makeTalkUtterForStt(callEvent, transcript, langVal))
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get())
        )
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc.newStub(channel);

    logger.debug("#@ MAP to Router talkRouterStubForStt = \n [{}]",
        printToUnicodeString(request));
    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .talkRoute(request, new StreamObserver<TalkRouteResponse>() {
          TalkRouteResponse ret = null;

          @Override
          public void onNext(TalkRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ talkRouterStub onNext [{}]", printToUnicodeString(value));
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            CallLogUtills
                .makeTalkRouterCallLog(callEvent, tempOpSyncIdMeta, request, userParam, null,
                    reqDate, new Date());

            ExceptionMsgUtills.sendExceptionFromRouter(
                handler, t, request, reqDate, callEvent, exIndex(9),
                MapException.StatusCode.GRPC_ROUTER_TALK_ERROR,
                "Talk Route error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ talkRouterStub onCompleted");

            CallLogUtills.makeTalkRouterCallLog(callEvent, tempOpSyncIdMeta,
                request, userParam, ret, reqDate, new Date());
            processTalkRouterResponse(handler, ret, userParam, resType, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  /**
   * talkRouterStubForIdr는 talkRouterStub과 달리 ImageRecognized 의 인식 결과로 수신받은 annotated_texts 배열의 값을
   * talkRouter로 전달 할 수 있도록 예외처리
   */
  public static void talkRouterStubForIdr(UserParam userParam,
      ImageRecognitionResult imageRecognitionResult, String resType, EventStream callEvent,
      MapEventHandler handler) {

    logger.info("#@ talkRouterStubForIdr imageRecognitionResult [{}]", imageRecognitionResult);

    //final MapEventHandler final_handler = handler;

    handler.getDirectiveForwarder()
        .sendStream(DialogUtils.makeDialogProcessing(callEvent));

    Router.TalkRouteRequest request = Router.TalkRouteRequest.newBuilder()
        .setUtter(DialogUtils.makeUtterForIdr(callEvent, imageRecognitionResult))
        .setSystemContext(Dialog.SystemContext.newBuilder()
            .setLocation(Location.newBuilder().mergeFrom(userParam.location))
            .setDevice(DeviceOuterClass.Device.newBuilder().mergeFrom(userParam.device))
            .setUser(Dispatcher.threadLocalUser.get())
        )
        .build();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_ROUTER_SERVER));
    TalkRouterGrpc.TalkRouterStub stub = TalkRouterGrpc.newStub(channel);

    logger
        .debug("#@ Router talkRouterStubForIdr \n [{}]", printToUnicodeString(request));
    Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // 헤더에 operationSyncId 설정
    stub = MetadataUtils.attachHeaders(stub, tempOpSyncIdMeta);
    Date reqDate = new Date();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    stub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .talkRoute(request, new StreamObserver<TalkRouteResponse>() {
          TalkRouteResponse ret = null;

          @Override
          public void onNext(TalkRouteResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.trace("#@ talkRouterStubForIdr onNext [{}]", value.toString());
            ret = value;
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            CallLogUtills
                .makeTalkRouterCallLog(callEvent, tempOpSyncIdMeta, request, userParam, null,
                    reqDate, new Date());

            ExceptionMsgUtills.sendExceptionFromRouter(
                handler, t, request, reqDate, callEvent, exIndex(10),
                MapException.StatusCode.GRPC_ROUTER_TALK_ERROR,
                "Talk Router error occurred");

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ talkRouterStubForIdr onCompleted");

            CallLogUtills
                .makeTalkRouterCallLog(callEvent, tempOpSyncIdMeta, request, userParam, ret,
                    reqDate, new Date());
            processTalkRouterResponse(handler, ret, userParam, resType, callEvent);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  private static void speakTtsStub(MapEventHandler handler, String utter, final int speaker,
      EventStream callEvent) {
    logger.info("#@ speakTtsStub utter [{}]", utter);

    String langSetting = getString(Keys.P_TTS_LANG);
    if (langSetting == null) {
      logger.trace("#@ ttsLang is empty, set to default `ko_KR`");
      langSetting = "ko_KR";
    }

    // #@ TTS의 encoding, sampleRate 값을 m2u.conf에서 참조
    logger.debug("#@ TTS Server [{}] [{}/{}/{}/{}]"
        , getString(Keys.P_TTS_SERVER)
        , getString(Keys.P_TTS_ENCODING)
        , getInt(Keys.P_TTS_SAMPLE_RATE)
        , langSetting
        , speaker);

    final Lang lang = Lang.valueOf(langSetting);
    // #@#@ req param 작성
    TtsRequest req = TtsRequest.newBuilder()
        .setLang(lang)
        .setSpeaker(speaker)
        .setText(utter)
        .build();

    // #@ tts로 전달할 text가 없는 경우
    if (req.getText().isEmpty()) {
      req = req.toBuilder().setText("다시 말씀해주세요.").build();
    }

    logger.debug("#@ TTS call [{}]", req.getText());
    final MapEventHandler ref = handler;
    final Metadata opSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    final String opSyncIdVal = callEvent.getOperationSyncId();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    // #@ TTS의 encoding, sampleRate 값을 m2u.conf에서 참조
    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_TTS_SERVER));
    NgTtsServiceStub ttsStub = NgTtsServiceGrpc.newStub(channel);

    // 헤더에 operationSyncId 설정
    ttsStub = MetadataUtils.attachHeaders(ttsStub, opSyncIdMeta);
    ttsStub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .speakWav(req, new StreamObserver<TtsMediaResponse>() {
          // on next 시점에 내려보낼 dir을 미리 만들어 놓는다.
          DirectiveStream dir = DirectiveStream.newBuilder()
              .setInterface(AsyncInterface.newBuilder()
                  .setInterface(MapIf.I_SPEECH_SYNTHESIZER)
                  .setOperation(MapIf.SS_D_PLAY)
                  .setType(OperationType.OP_DIRECTIVE)
                  .setStreaming(true)
                  .build())
              .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
              .setStreamId(UUID.randomUUID().toString())
              .setOperationSyncId(callEvent.getOperationSyncId())
              .build();

          boolean ttsFlag = true;

          @Override
          public void onNext(TtsMediaResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(opSyncIdVal);
            if (ttsFlag) {
              logger.trace("#@ onNext tts returns value == param [{}]", value.toString());

              ref.getDirectiveForwarder().sendStream(
                  dir.toBuilder()
                      // DirectiveParam.newBuilder().setSpeechSynthesizerParam(value.getParam()))
                      // #@#@ SpeechSynthesizerParam 일단 하드코딩
                      .setParam(DirectiveParam.newBuilder().setSpeechSynthesizerParam(
                          SpeechSynthesizerParam.newBuilder()
                              .setSampleRate(getInt(Keys.P_TTS_SAMPLE_RATE))
                              .setLang(lang)
                              .setEncoding(AudioEncoding.LINEAR16)))
                      .setPayload(Struct.newBuilder().putFields("speechTone",
                          Value.newBuilder().setNumberValue(speaker).build()))
                      .build());
              // #@#@ 최초 한번만 SpeechSynthesizerParam 전달
              ttsFlag = false;
            } else {
              logger.trace("#@ onNext tts returns bytes [{}]", value.getMediaData().size());
              ref.getDirectiveForwarder().sendStream(value.getMediaData());
            }
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(opSyncIdVal);

            Status status = Status.fromThrowable(t);
            logger.error("error status {}:{},{},{}"
                , status.getCode(), status.getDescription(), t, Dispatcher.threadLocal.get());

            ref.getDirectiveForwarder().sendStream(StreamBreak.newBuilder()
                .setBreaker(StreamBreaker.BREAK_BY_EXCEPTION)
                .setStreamId(dir.getStreamId())
                .setBrokenAt(Timestamps.fromMillis(System.currentTimeMillis()))
                .setReason("TTS Error - " + t.getLocalizedMessage())
                .build());

            ref.getDirectiveForwarder().notifyCompleted(ref);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(opSyncIdVal);
            logger.info("#@ TTS SpeechSynthesizerServiceStub.speak onCompleted");
            ref.getDirectiveForwarder().sendStream(StreamEnd.newBuilder()
                .setStreamId(dir.getStreamId())
                .setEndAt(Timestamps.fromMillis(System.currentTimeMillis()))
                .build());

            //ref.getDirectiveForwarder().sendStream(makeMicrophoneExpectSpeech(10));
            ref.getDirectiveForwarder().notifyCompleted(ref);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }

  private static void speakLegacyTtsStub(MapEventHandler handler, String utter, UserParam userParam,
      EventStream callEvent) {
    logger.info("#@ speakLegacyTtsStub utter [{}]", utter);

    // #@ TTS의 encoding, sampleRate 값을 m2u.conf에서 참조
    logger.debug("#@ TTS Server [{}] [{}/{}/{}]"
        , getString(Keys.P_TTS_SERVER)
        , getString(Keys.P_TTS_ENCODING)
        , getInt(Keys.P_TTS_SAMPLE_RATE)
        , Lang.forNumber(userParam.lang));

    SpeakRequest req = SpeakRequest.newBuilder()
        .setEncoding(AudioEncoding.valueOf(getString(Keys.P_TTS_ENCODING)))
        .setSampleRate(getInt(Keys.P_TTS_SAMPLE_RATE))
        .setLangValue(userParam.lang)
        .setText(utter)
        .build();

    if (req.getText().isEmpty()) {
      req = req.toBuilder().setText("다시 말씀해주세요.").build();
    }

    logger.debug("#@ TTS call [{}]", req.getText());
    final MapEventHandler ref = handler;
    final Metadata tempOpSyncIdMeta = GrpcUtills.makeOperationSyncIdHeader(callEvent);
    final String tempOpSyncIdVal = callEvent.getOperationSyncId();

    // stub call 이후 callback에서 MDC값이 유지되지 않아, MDC context를 저장
    final Map<String, String> mdcContext = MDC.getCopyOfContextMap();

    final ManagedChannel channel = DialogUtils.getChannel(getString(Keys.P_TTS_SERVER));
    SpeechSynthesizerServiceStub ttsStub = SpeechSynthesizerServiceGrpc.newStub(channel);

    // 헤더에 operationSyncId 설정
    ttsStub = MetadataUtils.attachHeaders(ttsStub, tempOpSyncIdMeta);

    ttsStub.withDeadlineAfter(GrpcUtills.grpcDeadLineSec, TimeUnit.SECONDS)
        .speak(req, new StreamObserver<SpeakResponse>() {
          // on next 시점에 내려보낼 dir을 미리 만들어 놓는다.
          DirectiveStream dir = DirectiveStream.newBuilder()
              .setInterface(AsyncInterface.newBuilder()
                  .setInterface(MapIf.I_SPEECH_SYNTHESIZER)
                  .setOperation(MapIf.SS_D_PLAY)
                  .setType(OperationType.OP_DIRECTIVE)
                  .setStreaming(true)
                  .build())
              .setBeginAt(Timestamps.fromMillis(System.currentTimeMillis()))
              .setStreamId(UUID.randomUUID().toString())
              .setOperationSyncId(callEvent.getOperationSyncId())
              .build();

          @Override
          public void onNext(SpeakResponse value) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ TTS SpeechSynthesizerServiceStub.speak onNext");
            if (value.hasParam()) {
              logger.trace("#@ tts returns value == param [{}]", value.toString());
              ref.getDirectiveForwarder().sendStream(
                  dir.toBuilder()
                      .setParam(DirectiveParam.newBuilder()
                          .setSpeechSynthesizerParam(value.getParam()))
                      .setPayload(Struct.newBuilder().putFields("tts.interface.type",
                          Value.newBuilder().setStringValue("legacy").build()))
                      .build());
            } else {
              logger.trace("#@ tts returns bytes [{}]", value.getAudioContent().size());
              ref.getDirectiveForwarder().sendStream(value.getAudioContent());
            }
          }

          @Override
          public void onError(Throwable t) {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);

            Status status = Status.fromThrowable(t);
            logger.error("error status {}:{},{},{}"
                , status.getCode(), status.getDescription(), t, Dispatcher.threadLocal.get());

            ref.getDirectiveForwarder().sendStream(StreamBreak.newBuilder()
                .setBreaker(StreamBreaker.BREAK_BY_EXCEPTION)
                .setStreamId(dir.getStreamId())
                .setBrokenAt(Timestamps.fromMillis(System.currentTimeMillis()))
                .setReason(t.getMessage())
                .build());
            ref.getDirectiveForwarder().notifyCompleted(ref);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            MDC.setContextMap(mdcContext);
            Dispatcher.threadLocal.set(tempOpSyncIdVal);
            logger.info("#@ TTS SpeechSynthesizerServiceStub.speak onCompleted");
            ref.getDirectiveForwarder().sendStream(StreamEnd.newBuilder()
                .setStreamId(dir.getStreamId())
                .setEndAt(Timestamps.fromMillis(System.currentTimeMillis()))
                .build());
            //ref.getDirectiveForwarder().sendStream(makeMicrophoneExpectSpeech(10));
            ref.getDirectiveForwarder().notifyCompleted(ref);

            //grpc RuntimeException 예외 처리
            if (channel != null) {
              GrpcUtills.closeChannel(channel);
            }
          }
        });
  }
}
