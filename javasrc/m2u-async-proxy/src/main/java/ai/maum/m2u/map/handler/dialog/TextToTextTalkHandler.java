package ai.maum.m2u.map.handler.dialog;

import ai.maum.m2u.map.DirectiveForwarder;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.handler.common.MapEventHandler;
import ai.maum.m2u.map.services.Service;
import ai.maum.m2u.map.util.MapIf;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.Struct;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import io.grpc.Status;
import java.util.UUID;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.DirectiveStream;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.StreamBreak;
import maum.m2u.map.Map.StreamEnd;
import maum.m2u.map.Map.StreamMeta;
import maum.m2u.map.Payload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import maum.m2u.map.Dialog.*;

public class TextToTextTalkHandler implements MapEventHandler {

  static Logger logger = LoggerFactory.getLogger(TextToTextTalkHandler.class);

  DirectiveForwarder forwarder = null;
  EventStream callEvent = null;
  Service service = null;

  @Override
  public String getInterface() {
    return MapIf.I_DIALOG;
  }

  @Override
  public String getOperation() {
    return MapIf.DL_E_TEXT_TO_TEXT_TALK;
  }

  @Override
  public boolean isStream() {
    return false;
  }

  @Override
  public void setDirectiveForwarder(DirectiveForwarder forward) {
    forwarder = forward;
  }

  @Override
  public DirectiveForwarder getDirectiveForwarder() {
    return forwarder;
  }

  @Override
  public void begin(EventStream event) {
    logger.info("#@ begin TextToTextTalkHandler = {}", event.getStreamId());
    callEvent = event;
    // speech recognition param 이 없다면
    String utter = event.getPayload().getFieldsMap().get("utter").getStringValue();
    logger.debug("begin utter=" + utter);

    service.OnDialogTextToTextTalk(this, event);
  }

  @Override
  public void end(StreamEnd end) {
    logger.info("#@ TextToTextTalkHandler stream end = {}", end.getStreamId());

  }

  @Override
  public void cancel(StreamBreak cancel) {
    logger.error("TextToTextTalkHandler stream cancelled. {}", cancel.getStreamId());

  }

  @Override
  public void next(ByteString bytes) {
    logger.trace("next bytes, {}", bytes.size());

  }

  @Override
  public void next(String text) {
    logger.trace("oh no next text, {}", text.length());
  }

  @Override
  public void next(StreamMeta meta) {
    logger.trace("oh no next meta, {}", meta.getSerializedSize());
  }

  @Override
  public void setService(Service service) {
    this.service = service;
  }
}
