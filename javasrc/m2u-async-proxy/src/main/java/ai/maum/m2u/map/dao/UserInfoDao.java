package ai.maum.m2u.map.dao;

import ai.maum.config.Keys;
import ai.maum.m2u.common.portable.UserInfoPortable;
import ai.maum.m2u.map.Dispatcher;
import ai.maum.m2u.map.hzc.EntryEventListener;
import ai.maum.m2u.map.hzc.HazelcastConnector;
import com.google.protobuf.Timestamp;
import com.hazelcast.core.IMap;
import io.grpc.Status;
import java.util.concurrent.TimeUnit;
import maum.m2u.common.UserOuterClass.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserInfoDao {

  static final Logger logger = LoggerFactory.getLogger(UserInfoDao.class);

  private IMap<String, UserInfoPortable> userIMap = HazelcastConnector
      .getInstance().client.getMap(Keys.MEM_USER_INFO);

  private EntryEventListener entryEventListener = new EntryEventListener();

  public UserInfoDao() {
    this.userIMap.addEntryListener(this.entryEventListener, true);
    logger.info("#@ UserInfoDao addEntryListener userIMap [{}]", userIMap);
  }

  /**
   * 기존 사용자 정보가 있는지 확인
   */
  public boolean exist(String accessToken) {
    logger.info("===== request UserInfoDao.getUserInfo");
    return this.userIMap.containsKey(accessToken);
  }

  /**
   * 사용자정보 조회
   *
   * @return message User { // 사용자의 UNIQUE ID string user_id = 1;
   *
   * // 사용자 이름 // *optional* string name = 2; // Opt
   *
   * // *optional*, 추가적인 인증 요구에 필요할 수도 있음. string access_token = 3;
   *
   * // *optional* 사용자의 부가 정보 // musicProvider // shoppingProvider google.protobuf.Struct meta = 11;
   * }
   */
  public User.Builder getUserInfo(String accessToken) {
    logger.info("===== request UserInfoDao.getUserInfo");

    UserInfoPortable app = this.userIMap.get(accessToken);
    User.Builder ap = User.newBuilder();
    if (app != null) {
      ap = app.getProtobufObj().toBuilder();
    }
    return ap;
  }

  /**
   * 사용자정보 등록
   */
  public void insertUserInfo(String accessToken, User req, Timestamp expiredAt) {
    logger.info("===== request UserInfoDao.insertUserInfo");

    try {
      long expiredTime = expiredAt.getSeconds();
      long systemTime = System.currentTimeMillis();

      //전달된 long 타입 expiredTime의 자릿수를 확인
      logger.debug("#@ expiredTime[{}] systemTime[{}]", expiredTime, systemTime);
      logger.debug("#@ expiredTimeLengh[{}] systemTimeLenght[{}]", getNumberLength(expiredTime),
          getNumberLength(systemTime));

      // 자릿수 길이 유효성 체크
      if (getNumberLength(expiredTime) == 10 || getNumberLength(expiredTime) == 13) {
        // mill 자릿수(3자리)가 부족한 경우 1000을 곱해 자릿수를 맞춘다
        if (getNumberLength(expiredTime) == 10) {
          expiredTime = expiredTime * 1000;
          logger.debug("#@ expiredTime's length is 10. So make as milli second [{}]", expiredTime);
        }

        if (getNumberLength(expiredTime) == 13) {
          boolean compare = systemTime < expiredTime;
          long ttl = expiredTime - systemTime;
          logger.debug("#@ systemTime < expiredTime = {}, ttl = {}", compare, ttl);

          if (compare) {
            UserInfoPortable uip = new UserInfoPortable();
            uip.setProtobufObj(req);
            this.userIMap.set(accessToken, uip, ttl, TimeUnit.MILLISECONDS);
          } else {
            logger.warn("#@ AccessToken: {} has expired! at {}. do not insert into hzc",
                accessToken, expiredAt);
            return;
          }
        }
      } else {
        logger.error("#@ AccessToken: {} not valid expired length {}", accessToken, expiredAt);
        throw new Exception("AccessToken:" + accessToken + " not valid expired at " + expiredAt);
      }
    } catch (Exception e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());
    }
  }

  /**
   * long의 자릿수 출력 함수
   *
   * @param number 시간 값
   * @return 숫자의 10의 자리 수
   */
  private int getNumberLength(long number) {
    // UNIX Epoch으로 보고 10을 반환한다.
    if (number == 0L) {
      return 10;
    }
    return ((int) Math.log10(number > 0L ? number : -number)) + 1;
  }
}
