package ai.maum.m2u.map;

import static ai.maum.config.Keys.I_META_M2U_AUTH_SIGN_IN;
import static ai.maum.config.Keys.I_META_M2U_AUTH_TOKEN;
import static ai.maum.config.Keys.I_META_M2U_INTERNAL_AUTH;

import ai.maum.config.Keys;
import ai.maum.m2u.map.handler.common.MessageCryptor;
import ai.maum.m2u.map.util.MapClassLoader;
import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 모든 Request에 대하여 Header확인 후 sigin-in, isValid to Service, reject 처리
 */
public class SecureMapInterceptor implements ServerInterceptor {

  static final Logger logger = LoggerFactory.getLogger(SecureMapInterceptor.class);
  // cryptorImpl 객체를 생성합니다
  private MessageCryptor cryptor;
  private MapClassLoader mapClassLoader;

  static final Context.Key<String> AUTH_TOKEN_VALUE
      = Context.key(Keys.CTX_AUTH_TOKEN_VALUE);
  static final Context.Key<Boolean> HAS_SIGN_IN_HEADER
      = Context.key(Keys.CTX_HAS_SIGN_IN_HEADER);
  static final Context.Key<Boolean> HAS_AUTH_TOKEN_HEADER
      = Context.key(Keys.CTX_HAS_AUTH_TOKEN_HEADER);
  static final Context.Key<Boolean> HAS_INTERNAL_AUTH_HEADER
      = Context.key(Keys.CTX_HAS_INTERNAL_AUTH_HEADER);

  public SecureMapInterceptor() {
    mapClassLoader = new MapClassLoader();
    cryptor = mapClassLoader.loadMessageCryptor();
  }

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(
      ServerCall<ReqT, RespT> serverCall,
      Metadata metadata,
      ServerCallHandler<ReqT, RespT> serverCallHandler) {

    Context context;
    String authTokenValue = "";

    try {
      // #@ Header 정보 유무 확인
      boolean isAuthToken = metadata
          .containsKey(Metadata.Key.of(I_META_M2U_AUTH_TOKEN, Metadata.ASCII_STRING_MARSHALLER));
      boolean isAuthSignIn = metadata
          .containsKey(Metadata.Key.of(I_META_M2U_AUTH_SIGN_IN, Metadata.ASCII_STRING_MARSHALLER));
      boolean isAuthInternal = metadata
          .containsKey(Metadata.Key.of(I_META_M2U_INTERNAL_AUTH, Metadata.ASCII_STRING_MARSHALLER));

      if (isAuthToken) {
        authTokenValue = metadata.get(Metadata.Key
            .of(I_META_M2U_AUTH_TOKEN, Metadata.ASCII_STRING_MARSHALLER));
        if (authTokenValue != null && !authTokenValue.isEmpty())
          authTokenValue = cryptor.decode(authTokenValue);
      }
      //ByteString decrypedMsg = cryptorImpl.decode(ByteString.copyFrom(authTokenValue.getBytes()));

      // 정리 :
      // authToken은 유무, 값 전달
      // authSignIn은 유무 전달
      // internalAuth는 유무 전달
      context = Context.current()
          .withValues(
              HAS_AUTH_TOKEN_HEADER, isAuthToken
              , AUTH_TOKEN_VALUE, authTokenValue
              , HAS_SIGN_IN_HEADER, isAuthSignIn
              , HAS_INTERNAL_AUTH_HEADER, isAuthInternal
          );

      return Contexts.interceptCall(context, serverCall, metadata, serverCallHandler);
    } catch (Exception e) {
      serverCall.close(Status.INTERNAL, new Metadata());
      return null;
    }
  }
}
