package ai.maum.m2u.map.common.utils;

import ai.maum.m2u.map.Dispatcher;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Struct;
import com.google.protobuf.util.JsonFormat;
import io.grpc.Status;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonUtills {

  static final Logger logger = LoggerFactory.getLogger(JsonUtills.class);

  /**
   * Merge "source" into "target". If fields have equal name, merge them recursively.
   * @return the merged object (target).
   */
  public static JSONObject deepMerge(JSONObject source, JSONObject target) throws JSONException {
    if (source.length() < 1){
      return target;
    } else {
      for (String key: JSONObject.getNames(source)) {
        Object value = source.get(key);
        if (!target.has(key)) {
          // new value for "key":
          target.put(key, value);
        } else {
          // existing value for "key" - recursively deep merge:
          if (value instanceof JSONObject) {
            JSONObject valueJson = (JSONObject)value;
            deepMerge(valueJson, target.getJSONObject(key));
          } else {
            target.put(key, value);
          }
        }
      }
      return target;
    }
  }

  public static Struct jsonObjectToStruct(JSONObject msg) {
    Struct.Builder stb = Struct.newBuilder();
    try {
      String json = msg.toString();
      JsonFormat.parser().ignoringUnknownFields().merge(json, stb);
    } catch (InvalidProtocolBufferException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());

      return stb.build();
    }
    return stb.build();
  }

}
