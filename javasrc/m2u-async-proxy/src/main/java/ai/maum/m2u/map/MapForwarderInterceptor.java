package ai.maum.m2u.map;

import ai.maum.config.Keys;
import ai.maum.m2u.map.handler.common.MessageCryptor;
import ai.maum.m2u.map.util.MapClassLoader;
import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MapForwarder 기능
 * 1.Client로 부터 수신 받은 EventStream을 SQL Injection 공격인지 분석후 암호화 하여 전달합니다
 * 2."작업중" 공지기능을 제공합니다.
 * nginx.conf에 grpc_set_header m2u-maintenance 12345; 와 같이 설정해 주면 헤더값을 읽고
 * sendException을 통해 작업중 공지 메시지를 전달합니다
 */
public class MapForwarderInterceptor implements ServerInterceptor {
  static final Logger logger = LoggerFactory.getLogger(MapForwarderInterceptor.class);
  // cryptorImpl 객체를 생성합니다
  private MessageCryptor cryptor;
  private MapClassLoader mapClassLoader;

  static final Context.Key<String> AUTH_TOKEN_VALUE
      = Context.key(Keys.CTX_AUTH_TOKEN_VALUE);
  static final Context.Key<Boolean> HAS_SIGN_IN_HEADER
      = Context.key(Keys.CTX_HAS_SIGN_IN_HEADER);
  static final Context.Key<Boolean> HAS_AUTH_TOKEN_HEADER
      = Context.key(Keys.CTX_HAS_AUTH_TOKEN_HEADER);
  static final Context.Key<Boolean> HAS_INTERNAL_AUTH_HEADER
      = Context.key(Keys.CTX_HAS_INTERNAL_AUTH_HEADER);
  static final Context.Key<String> M2U_MAINTENANCE_VALUE
      = Context.key(Keys.CTX_M2U_MAINTENANCE_VALUE);

  public MapForwarderInterceptor() {
    mapClassLoader = new MapClassLoader();
    cryptor = mapClassLoader.loadMessageCryptor();
  }

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(
      ServerCall<ReqT, RespT> serverCall,
      Metadata metadata,
      ServerCallHandler<ReqT, RespT> serverCallHandler) {

    Context context;

    // 전달해야할 header 정보
    // I_META_M2U_AUTH_TOKEN
    String authTokenValue = "";
    String encryptedAuthToken = "";

    try {
      boolean isAuthToken = metadata.containsKey(Metadata.Key.of(
          Keys.I_META_M2U_AUTH_TOKEN, Metadata.ASCII_STRING_MARSHALLER));
      boolean isAuthSignIn = metadata.containsKey(Metadata.Key.of(
          Keys.I_META_M2U_AUTH_SIGN_IN, Metadata.ASCII_STRING_MARSHALLER));
      boolean isM2uMaintenance = metadata.containsKey(Metadata.Key.of(
          Keys.I_META_M2U_MAINTENANCE, Metadata.ASCII_STRING_MARSHALLER));
      boolean isAuthInternal = metadata.containsKey(Metadata.Key.of(
          Keys.I_META_M2U_INTERNAL_AUTH, Metadata.ASCII_STRING_MARSHALLER));

      // authTokenValue를 확인합니다
      // authTokenValue가 있으면 암호화를 진행합니다
      authTokenValue = metadata.get(Metadata.Key
          .of(Keys.I_META_M2U_AUTH_TOKEN, Metadata.ASCII_STRING_MARSHALLER));

      if (isAuthToken && authTokenValue!=null && !authTokenValue.isEmpty()) {
        // authTokenValue를 암호화 합니다
        encryptedAuthToken = cryptor.encode(authTokenValue);
      }

      String m2uMaintenanceValue = "";
      if (isM2uMaintenance) {
        m2uMaintenanceValue = metadata.get(Metadata.Key
            .of(Keys.I_META_M2U_MAINTENANCE, Metadata.ASCII_STRING_MARSHALLER));
      }

      // 정리 :
      // authToken은 값 전달
      // m2uMaintenance은 값 전달
      // authSignIn은 유무 전달
      // internalAuth는 유무 전달
      context = Context.current()
          .withValues(
              AUTH_TOKEN_VALUE, encryptedAuthToken,
              M2U_MAINTENANCE_VALUE, m2uMaintenanceValue,
              HAS_SIGN_IN_HEADER, isAuthSignIn,
              HAS_INTERNAL_AUTH_HEADER, isAuthInternal
          );

      return Contexts.interceptCall(context, serverCall, metadata, serverCallHandler);
    } catch (Exception e) {
      serverCall.close(Status.INTERNAL, new Metadata());
      return null;
    }
  }
}
