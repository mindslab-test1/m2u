package ai.maum.m2u.map.common.user;

import maum.m2u.common.DeviceOuterClass.Device;
import maum.m2u.common.LocationOuterClass.Location;

public class UserParam {

  public Device device;
  public Location location;
  public int lang;
}

