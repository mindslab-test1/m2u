package ai.maum.m2u.map.handler.common;

import ai.maum.m2u.map.Dispatcher;
import io.grpc.Status;
import java.util.HashMap;
import maum.m2u.map.Map.AsyncInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 모든 처리 가능한 EventHandler들을 모아 놓은 POOL 객체
 *
 * 키는 "interface/operation" 형식으로 유지된다.
 */
public class EventHandlerPool {

  static final Logger logger = LoggerFactory.getLogger(EventHandlerPool.class);

  private static EventHandlerPool ourInstance = new EventHandlerPool();

  public static EventHandlerPool getInstance() {
    return ourInstance;
  }

  private EventHandlerPool() {
  }

  private HashMap<String, Class> handlers = new HashMap<>();

  /**
   * 키를 생성한다.
   *
   * @param asyncIf AsyncInterface 로 동작한다.
   * @return 키 문자열
   */
  private String makeKey(final AsyncInterface asyncIf) {
    return makeKey(asyncIf.getInterface(), asyncIf.getOperation());
  }

  /**
   * 키를 생성한다.
   *
   * @param intf 인터페이스 이름
   * @param operation 오퍼레이션 이름
   * @return "인터페이스/오퍼레이션"을 반환한다.
   */
  private String makeKey(final String intf, final String operation) {
    return intf + "/" + operation;
  }

  /**
   * AsyncInterface 객체에 맞는 AsyncInterface를 반환한다.
   *
   * @param asyncIf AsyncInterface 객체, 원격에서 전송된다.
   * @return AsyncInterface 객체
   */
  public MapEventHandler getMapEventHandler(AsyncInterface asyncIf) {
    try {
      String key = makeKey(asyncIf);
      if (handlers.containsKey(key)) {
        return (MapEventHandler) handlers.get(key).newInstance();
      }
    } catch (InstantiationException | IllegalAccessException e) {
      Status status = Status.fromThrowable(e);
      logger.error("error status {}:{},{},{}", status.getCode(), status.getDescription(),
          e, Dispatcher.threadLocal.get());
    }
    return null;
  }

  /**
   * MapEventHandler 객체를 이용해서 처리한다.
   *
   * MapEventHandler를 구현하고 있는지를 검사한 이후에 처리하도록 한다.
   *
   * @param clazz Class 객체
   */
  public void register(String aif, String operation, Class clazz) {
    if (MapEventHandler.class.isAssignableFrom(clazz)) {
      handlers.put(makeKey(aif, operation), clazz);
    } else {
      // TODO
      // throw exception or logging
    }
  }

  /**
   * 등록 해제 한다.
   *
   * @param intf 인터페이스 이름
   * @param operation 오퍼레이션 이름
   */
  public void unregister(String intf, String operation) {
    handlers.remove(makeKey(intf, operation));
  }

  /**
   * 핸들러 각체를 이용해서 자기자신을 삭
   */
  public void unregister(MapEventHandler handler) {
    handlers.remove(makeKey(handler.getInterface(), handler.getOperation()));
  }
}
