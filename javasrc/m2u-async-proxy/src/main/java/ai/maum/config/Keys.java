package ai.maum.config;

public interface Keys {

  public static final String I_META_M2U_INTERNAL_AUTH = "m2u-auth-internal";
  public static final String I_META_M2U_AUTH_TOKEN = "m2u-auth-token";
  public static final String I_META_M2U_AUTH_SIGN_IN = "m2u-auth-sign-in";
  public static final String I_META_M2U_MAINTENANCE = "m2u-maintenance";

  public static final String CTX_AUTH_SERVER_IP = "authServerIp";
  public static final String CTX_HAS_SIGN_IN_HEADER = "hasSignInHeader";
  public static final String CTX_HAS_AUTH_TOKEN_HEADER = "hasAuthTokenHeader";
  public static final String CTX_AUTH_TOKEN_VALUE = "authTokenValue";
  public static final String CTX_M2U_MAINTENANCE_VALUE = "m2uMaintenanceValue";
  public static final String CTX_HAS_INTERNAL_AUTH_HEADER = "hasInternalAuthHeader";

  //
  // PROPERTIES
  //
  public static final String P_AUTH_SERVER = "auth.export.ip";
  public static final String P_AUTH_INTERNAL_SERVER = "internal.auth.export.ip";
  public static final String P_ROUTER_SERVER = "routed.export";
  public static final String P_MAP_SESSIONMANAGER_CYCLESEC = "map.sessionmanager.cyclesec";
  public static final String P_MAP_SESSIONMANAGER_REFVAL = "map.sessionmanager.refval";
  public static final String P_STS_SERVER = "brain-stt.sttd.front.remote";
  public static final String P_STS_MODEL = "brain-stt.sttd.default.model";
  public static final String P_STS_LANG = "brain-stt.sttd.default.lang";
  public static final String P_STS_PROTOCOL_TYPE = "brain-stt.sttd.protocol.type";

  public static final String P_DEFAULT_EXPECT_SPEECH = "map.default.expectspeech";
  public static final String P_TTS_SERVER = "speech-synthesizer.export";
  public static final String P_TTS_INTERFACE = "speech-synthesizer.interface";
  public static final String P_TTS_ENCODING = "speech-synthesizer.encoding";
  public static final String P_TTS_SAMPLE_RATE = "speech-synthesizer.sample-rate";
  public static final String P_TTS_LANG = "speech-synthesizer.lang";
  public static final String P_IDR_SERVER = "image.export.ip";

  //
  // HZC
  //
  public static final String MEM_SSSION_V3 = "session-v3";
  public static final String MEM_USER_INFO = "user-info";

}
