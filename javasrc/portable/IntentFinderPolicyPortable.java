package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.router.v3.Intentfinder.IntentFinderPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
message IntentFinderPolicy {
  // 의도파악 정책의 이름
  string name = 1;
  // 의도파악 정책에 대한 설명
  string description = 2;
  // 기존 규칙들
  maum.common.Lang lang = 3;
  // 기본 스킬
  // 만일 각 스텝을 돈 결과 제대로된 의도 파익이 실패한 경우에는 이 값으로 채워야 한다.
  // 인텐트 파인더가 아예 동작하지 않거나 하는 경우에 동작을 해야할 수도 있습니다.
  string default_skill = 4;
  // 의도파악 단계들
  repeated IntentFindingStep steps = 5;
}
 */

public class IntentFinderPolicyPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderPolicyPortable.class);

  private IntentFinderPolicy protobufObj;

  public IntentFinderPolicyPortable() {
    protobufObj = IntentFinderPolicy.newBuilder().build();
  }

  public IntentFinderPolicy getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(IntentFinderPolicy protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return ServiceAdminPortableFactory.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.INTENT_FINDER_POLICY;
  }


  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj().toByteArray() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(IntentFinderPolicy.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
