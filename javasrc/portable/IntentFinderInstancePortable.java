package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.router.v3.Intentfinder.IntentFinderInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
message IntentFinderInstance {
  // IntentFinderInstance의 이름으로 중복되어서는 안된다.
  string name = 1;
  // intentFinder 에 대한 설명
  string description = 2;
  // intentFinder instance의 ip주소
  string ip = 4;
  // intentFinder instance의 port
  int32  port = 5;
  // 의도 파악 정책의 이름
  string policy_name = 6;
}
 */

public class IntentFinderInstancePortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderInstancePortable.class);

  private IntentFinderInstance protobufObj;

  public IntentFinderInstancePortable() {
    protobufObj = IntentFinderInstance.newBuilder().build();
  }

  public IntentFinderInstance getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(IntentFinderInstance protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.INTENT_FINDER_INSTANCE;
  }


  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }

    if (this.getProtobufObj().toByteArray() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if(this.getProtobufObj().getIp() != null){
      writer.writeUTF("ip", this.getProtobufObj().getIp());
    }
    if(this.getProtobufObj().getPort() != 0){
      writer.writeInt("port", this.getProtobufObj().getPort());
    }
    if(this.getProtobufObj().getPolicyName() != null){
      writer.writeUTF("policy_name", this.getProtobufObj().getPolicyName());
    }
    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(IntentFinderInstance.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
