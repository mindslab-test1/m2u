package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.console.Da.DialogAgentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
message DialogAgentManager {
  string name = 1;
  string description = 2;
  string ip = 3;
  int32 port = 4;
  bool active = 5;
  bool default = 6;
  google.protobuf.Timestamp create_at = 7;
  google.protobuf.Timestamp modify_at = 8;
}
 */

public class DialogAgentManagerPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentManagerPortable.class);

  private DialogAgentManager protobufObj;

  public DialogAgentManagerPortable() {
    protobufObj = DialogAgentManager.newBuilder().build();
  }

  public DialogAgentManager getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogAgentManager protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_AGENT_MANAGER;
  }


  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getDescription() != null) {
      writer.writeUTF("description", this.getProtobufObj().getDescription());
    }
    if (this.getProtobufObj().getIp() != null) {
      writer.writeUTF("ip", this.getProtobufObj().getIp());
    }
    writer.writeInt("port", this.getProtobufObj().getPort());
    writer.writeBoolean("active", this.getProtobufObj().getActive());
    writer.writeBoolean("default", this.getProtobufObj().getDefault());
    if (this.getProtobufObj().getCreateAt() != null) {
      writer.writeLong("create_at", this.getProtobufObj().getCreateAt().getSeconds());
    }
    if (this.getProtobufObj().getModifyAt() != null) {
      writer.writeLong("modify_at", this.getProtobufObj().getModifyAt().getSeconds());
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogAgentManager.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
