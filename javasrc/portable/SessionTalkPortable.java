package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.server.Session.SessionTalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
message SessionTalk {
  // 순서
  uint32 seq = 1;
  // 언어
  maum.common.LangCode lang = 11;
  // 음성 여부
  bool audio = 12;
  // Sample Rate
  int32 sample_rate = 13;
  // 이미지 여부
  bool image = 14;
  // 입력 텍스트
  string in = 21;
  string in_mangled = 22;
  // 출력 텍스트
  string out = 23;
  // 출력 음성
  string audio_record_file = 24;
  // 출력 메타 데이터
  map<string, string> meta = 31;
  // 출력 메타 데이터
  map<string, string> context = 32;
  // grpc 최종 status
  int32 status_code = 41;
  // 내부 처리 코드
  maum.m2u.facade.DialogResult dialog_result = 42;
  // 텍스트 분석, 이건 정말 들어갈 것은 아닌 것 같군요. 제거
  // NamedEntityAnalysis named_entity_analysis = 50;
  // 복수의 대화 에이전트 정보
  repeated TalkAgent agents = 61;
  // 최종 도메인
  string skill = 62;
  // 최종 의도
  string intent = 63;
  // 시작 시간
  google.protobuf.Timestamp start = 71;
  // 끝 시간
  google.protobuf.Timestamp end = 72;
  // 각 구간별 시간
  map<string, google.protobuf.Duration> elapsed_time = 73;
  // 각 세부 동작별 CL
  ConfidenceLevel cl = 81;
  float cl_result = 82;
  // CL 안정구간이 아닌 경우 표시
  int32 warned = 83;
}
 */

public class SessionTalkPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(SessionTalkPortable.class);

  private SessionTalk protobufObj;

  public SessionTalkPortable() {
    protobufObj = SessionTalk.newBuilder().build();
  }

  public SessionTalk getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(SessionTalk protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.SESSION_TALK;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    writer.writeLong("seq", this.getProtobufObj().getSeq());
    writer.writeLong("session_id", this.getProtobufObj().getSessionId());
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getLang() != null) {
      writer.writeInt("lang", this.getProtobufObj().getLangValue());
    }
    writer.writeBoolean("audio", this.getProtobufObj().getAudio());
    writer.writeBoolean("image", this.getProtobufObj().getImage());
    if (this.getProtobufObj().getIn() != null) {
      writer.writeUTF("_in", this.getProtobufObj().getIn());
    }
    if (this.getProtobufObj().getInMangled() != null) {
      writer.writeUTF("in_mangled", this.getProtobufObj().getInMangled());
    }
    if (this.getProtobufObj().getOut() != null) {
      writer.writeUTF("out", this.getProtobufObj().getOut());
    }
    writer.writeInt("status_code", this.getProtobufObj().getStatusCode());
    writer.writeInt("dialog_result", this.getProtobufObj().getDialogResult().getNumber());
    if (this.getProtobufObj().getSkill() != null) {
      writer.writeUTF("skill", this.getProtobufObj().getSkill());
    }
    if (this.getProtobufObj().getIntent() != null) {
      writer.writeUTF("intent", this.getProtobufObj().getIntent());
    }
    if (this.getProtobufObj().getStart() != null) {
      writer.writeLong("start", this.getProtobufObj().getStart().getSeconds());
    }
    if (this.getProtobufObj().getEnd() != null) {
      writer.writeLong("end", this.getProtobufObj().getEnd().getSeconds());
    }

//    속도 너무 느려지셔 주석 처리
//    try {
//      logger.debug("writePortable : {}", this.toString());
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(SessionTalk.parseFrom(reader.readByteArray("_msg")));

//    속도 너무 느려지셔 주석 처리
//    try {
//      logger.debug("readPortable : {}", this.toString());
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
