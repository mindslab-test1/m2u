package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableFactory;

public class ServiceAdminPortableFactory implements PortableFactory {

  public static final int FACTORY_ID = 1;

  @Override
  public Portable create(int classId) {
    if (classId == PortableClassId.DIALOG_AGENT_MANAGER) {
      return new DialogAgentManagerPortable();
    } else if (classId == PortableClassId.DIALOG_AGENT) {
      return new DialogAgentPortable();
    } else if (classId == PortableClassId.DIALOG_AGENT_ACTIVATION_INFO) {
      return new DialogAgentActivationInfoPortable();
    } else if (classId == PortableClassId.SIMPLE_CLASSIFIER) {
      return new SimpleClassifierPortable();
    } else if (classId == PortableClassId.SKILL) {
      return new SkillPortable();
    } else if (classId == PortableClassId.CHATBOT) {
      return new ChatbotPortable();
    } else if (classId == PortableClassId.AUTHTICATION_POLICY) {
      return new AuthticationPolicyPortable();
    } else if (classId == PortableClassId.INTENT_FINDER) {
      return new IntentFinderPortable();
    } else if (classId == PortableClassId.DIALOG_AGENT_INSTANCE) {
      return new DialogAgentInstancePortable();
    } else if (classId == PortableClassId.DIALOG_AGENT_INSTANCE_EXECUTION_INFO) {
      return new DialogAgentInstanceExecutionInfoPortable();
    } else if (classId == PortableClassId.DIALOG_AGENT_INSTANCE_RESOURCE) {
      return new DialogAgentInstanceResourcePortable();
    } else if (classId == PortableClassId.DIALOG_SESSION) {
      return new DialogSessionPortable();
    } else if (classId == PortableClassId.SESSION_TALK) {
      return new SessionTalkPortable();
    } else if (classId == PortableClassId.DIALOG_SESSION_V3) {
      return new DialogSessionV3Portable();
    } else if (classId == PortableClassId.SESSION_TALK_V3) {
      return new SessionTalkV3Portable();
    } else if (classId == PortableClassId.USER_INFO) {
      return new UserInfoPortable();
    } else if (classId == PortableClassId.INTENT_FINDER_INSTANCE) {
      return new IntentFinderInstancePortable();
    } else if (classId == PortableClassId.INTENT_FINDER_POLICY) {
      return new IntentFinderPolicyPortable();
    } else if (classId == PortableClassId.CLASSIFICATION_RECORD) {
      return new ClassificationRecordPortable();
    } else if (classId == PortableClassId.DIALOG_AGENT_INSTANCE_RESOURCE_SKILL) {
      return new DialogAgentInstanceResourceSkillPortable();
    } else {
      throw new IllegalArgumentException(classId + " unsupported classId");
    }
  }
}
