package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import java.io.IOException;
import maum.m2u.console.DaInstance.DialogAgentInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
message DialogAgentInstance {
  string dai_id = 1;
  string chatbot_name = 2;
  string da_name = 4;
  string skill_name = 5;
  string name = 6;
  LangCode lang = 7;
  string description = 8;
  string exec_param = 9;

  int32 version = 10;
  string state = 11;

  repeated DialogAgentInstanceExecutionInfo da_instance_exec_info = 12;
}
 */

public class DialogAgentInstancePortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentInstancePortable.class);

  private DialogAgentInstance protobufObj;

  public DialogAgentInstancePortable() {
    protobufObj = DialogAgentInstance.newBuilder().build();
  }

  public DialogAgentInstance getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogAgentInstance protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_AGENT_INSTANCE;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getDaiId() != null) {
      writer.writeUTF("dai_id", this.getProtobufObj().getDaiId());
    }
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getChatbotName() != null) {
      writer.writeUTF("chatbot_name", this.getProtobufObj().getChatbotName());
    }
    if (this.getProtobufObj().getDaName() != null) {
      writer.writeUTF("da_name", this.getProtobufObj().getDaName());
    }
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj().getLang() != null) {
      writer.writeInt("lang", this.getProtobufObj().getLang().getNumber());
    }
    if (this.getProtobufObj().getDescription() != null) {
      writer.writeUTF("description", this.getProtobufObj().getDescription());
    }
    if (this.getProtobufObj().getDaProdSpec() != null) {
      writer.writeInt("da_prod_spec", this.getProtobufObj().getDaProdSpec().getNumber());
    }
    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogAgentInstance.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
