package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.console.Classifier.SimpleClassifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
message SimpleClassifier {
  string name = 1;
  LangCode lang = 2;
  string description = 3;

  int32 skill_cnt = 4;
  int32 reg_cnt = 5;

  google.protobuf.Timestamp create_at = 6;
  google.protobuf.Timestamp modify_at = 7;

  map<string, ListRegex> skill = 8;

  message ListRegex {
    repeated string regex = 1;
  }
}
 */

public class SimpleClassifierPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(SimpleClassifierPortable.class);

  private SimpleClassifier protobufObj;

  public SimpleClassifierPortable() {
    protobufObj = SimpleClassifier.newBuilder().build();
  }

  public SimpleClassifier getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(SimpleClassifier protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return ServiceAdminPortableFactory.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.SIMPLE_CLASSIFIER;
  }


  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj().toByteArray() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getLang() != null) {
      writer.writeInt("lang", this.getProtobufObj().getLang().getNumber());
    }
    //writer.writeBoolean("pos_tagging", this.getProtobufObj().getPosTagging());
    //writer.writeBoolean("named_entity", this.getProtobufObj().getNamedEntity());
    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(SimpleClassifier.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
