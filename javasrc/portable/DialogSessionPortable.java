package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.server.Session.DialogSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
// 세션 정보
message DialogSession {
  int64 id = 1;
  string chatbot = 2;
  string user_key = 3;
  map<string, string> contexts = 4;
  google.protobuf.Duration duration = 5;
  bool human_assisted = 6;
  string name = 7;
  Agent current_agent = 8;
  maum.m2u.facade.MediaType output = 9;
  bool valid = 15;
  bool active = 16;
  google.protobuf.Timestamp started_at = 20;
  google.protobuf.Timestamp talked_at = 21;
  maum.m2u.facade.Caller caller = 30;
  string peer = 31;
  int32 warned_talk_count = 41;
  ConfidenceLevel cl = 42;
  float last_cl_result = 43;
  bool interfered = 44;
  repeated AgentUserAttribute agent_user_attrs = 50;
}
 */

public class DialogSessionPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(DialogSessionPortable.class);

  private DialogSession protobufObj;

  public DialogSessionPortable() {
    protobufObj = DialogSession.newBuilder().build();
  }

  public DialogSession getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogSession protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_SESSION;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    writer.writeLong("id", this.getProtobufObj().getId());
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getChatbot() != null) {
      writer.writeUTF("chatbot", this.getProtobufObj().getChatbot());
    }
    if (this.getProtobufObj().getUserKey() != null) {
      writer.writeUTF("user_key", this.getProtobufObj().getUserKey());
    }
    if (this.getProtobufObj().getDuration() != null) {
      writer.writeLong("duration", this.getProtobufObj().getDuration().getSeconds());
    }
    writer.writeBoolean("human_assisted", this.getProtobufObj().getHumanAssisted());
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj().getOutput() != null) {
      writer.writeLong("output", this.getProtobufObj().getOutput().getNumber());
    }
    writer.writeBoolean("valid", this.getProtobufObj().getValid());
    writer.writeBoolean("active", this.getProtobufObj().getActive());
    writer.writeLong("started_at", this.getProtobufObj().getStartedAt().getSeconds());
    writer.writeLong("talked_at", this.getProtobufObj().getTalkedAt().getSeconds());
    if (this.getProtobufObj().getPeer() != null) {
      writer.writeUTF("peer", this.getProtobufObj().getPeer());
    }
    writer.writeInt("warned_talk_count", this.getProtobufObj().getWarnedTalkCount());

//    속도 너무 느려지셔 주석 처리
//    try {
//      logger.debug("writePortable : {}", this.toString());
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogSession.parseFrom(reader.readByteArray("_msg")));

//    속도 너무 느려지셔 주석 처리
//    try {
//      logger.debug("readPortable : {}", this.toString());
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
