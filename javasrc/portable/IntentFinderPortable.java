package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.console.Classifier.IntentFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
message IntentFinder{
  string name = 1;
  string chatbot_name = 2;
  string description = 3;
  repeated string sc_list = 4;
  repeated string dnn_list =5;
  repeated string mapped = 8;
}
 */

public class IntentFinderPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderPortable.class);

  private IntentFinder protobufObj;

  public IntentFinderPortable() {
    protobufObj = IntentFinder.newBuilder().build();
  }

  public IntentFinder getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(IntentFinder protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return ServiceAdminPortableFactory.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.INTENT_FINDER;
  }


  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj().toByteArray() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getDescription() != null) {
      writer.writeUTF("description", this.getProtobufObj().getDescription());
    }
    if (this.getProtobufObj().getMappedList() != null) {
      writer.writeUTFArray("mapped", this.getProtobufObj().getMappedList()
          .toArray(new String[this.getProtobufObj().getMappedCount()]));
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(IntentFinder.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
