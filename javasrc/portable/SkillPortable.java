package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.console.Classifier.SimpleClassifier.ListSkill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
//old facade
//message Skill {
//  string name = 1;
//  string lang = 2;
//  string description = 3;
//  string version = 4;
//  string skill = 5;
//  map<string, string> properties = 6;
//  string key = 7;
//}
 */

public class SkillPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(SkillPortable.class);

  private String scName = "NIL";
  private ListSkill protobufObj;

  public SkillPortable() {
    protobufObj = ListSkill.newBuilder().build();
  }


  public String getScName() {
    return scName;
  }

  public void setScName(String scName) {
    this.scName = scName;
  }


  public ListSkill getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(ListSkill protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return ServiceAdminPortableFactory.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.SKILL;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getScName() != null) {
      writer.writeUTF("sc_name", this.getScName());
    }
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getRegexList() != null) {
      writer.writeUTFArray("regex", this.getProtobufObj().getRegexList()
          .toArray(new String[this.getProtobufObj().getRegexCount()]));
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(ListSkill.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
