package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.console.ChatbotOuterClass.AuthticationPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/*
message AuthticationPolicy {
  string name = 1;
  string description = 2;
  ProtocalType protocol = 3;
  string server_ip = 4;
  int32 server_port = 5;
  string api_key = 6;
  google.protobuf.Timestamp create_at = 7;
  google.protobuf.Timestamp modify_at = 8;

  string providerName = 9;
  repeated string mapped = 10;
}
 */

public class AuthticationPolicyPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(AuthticationPolicyPortable.class);

  private AuthticationPolicy protobufObj;

  public AuthticationPolicyPortable() {
    protobufObj = AuthticationPolicy.newBuilder().build();
  }

  public AuthticationPolicy getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(AuthticationPolicy protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return ServiceAdminPortableFactory.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.AUTHTICATION_POLICY;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getDescription() != null) {
      writer.writeUTF("description", this.getProtobufObj().getDescription());
    }
    if (this.getProtobufObj().getProtocol() != null) {
      writer.writeInt("protocol", this.getProtobufObj().getProtocol().getNumber());
    }
    if (this.getProtobufObj().getServerIp() != null) {
      writer.writeUTF("server_ip", this.getProtobufObj().getServerIp());
    }
    writer.writeInt("server_port", this.getProtobufObj().getServerPort());
    if (this.getProtobufObj().getApiKey() != null) {
      writer.writeUTF("api_key", this.getProtobufObj().getApiKey());
    }
    if (this.getProtobufObj().getCreateAt() != null) {
      writer.writeLong("create_at", this.getProtobufObj().getCreateAt().getSeconds());
    }
    if (this.getProtobufObj().getModifyAt() != null) {
      writer.writeLong("modify_at", this.getProtobufObj().getModifyAt().getSeconds());
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(AuthticationPolicy.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
