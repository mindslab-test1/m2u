
package ai.maum.m2u.common.portable;

public final class PortableClassId {

  public static final int FACTORY_ID = 1;


  public static final int DIALOG_AGENT_MANAGER = 1;
  public static final int DIALOG_AGENT = 2;
  public static final int DIALOG_AGENT_ACTIVATION_INFO = 3;

  public static final int SIMPLE_CLASSIFIER = 4;
  public static final int SKILL = 5;

  public static final int CHATBOT = 6;
  public static final int CHATBOT_DETAIL = 7;

  public static final int AUTHTICATION_POLICY = 8;
  public static final int INTENT_FINDER = 9;

  public static final int DIALOG_AGENT_INSTANCE = 10;
  public static final int DIALOG_AGENT_INSTANCE_EXECUTION_INFO = 11;
  public static final int DIALOG_AGENT_INSTANCE_RESOURCE = 12;
  public static final int DIALOG_AGENT_INSTANCE_RESOURCE_SKILL = 13;

  public static final int DIALOG_SESSION = 21;
  public static final int SESSION_TALK = 22;
  // Router Sesion 관리용으로 추가함.
  public static final int DIALOG_SESSION_V3 = 24;
  public static final int SESSION_TALK_V3 = 25;
  public static final int DEVICE = 26;

  public static final int INTENT_FINDER_INSTANCE = 30;
  public static final int INTENT_FINDER_POLICY = 31;
  public static final int CLASSIFICATION_RECORD = 32;

  public static final int USER_INFO = 90;


  //
  // MAP NAME
  //
  public static final String NsAuthPolicy = "auth-policy";
  public static final String NsChatbotDetail = "chatbot-detail";
  public static final String NsDAManager = "dialog-agent-manager";
  public static final String NsDA = "dialog-agent";
  public static final String NsDAActivation = "dialog-agent-activation";
  public static final String NsDAInstance = "dialog-agent-instance";
  public static final String NsDAInstExec = "dialog-agent-instance-exec";
  public static final String NsIntentFinderInstExec = "intent-finder-instance-exec";
  public static final String NsSC = "simple-classifier";
  public static final String NsSkill = "skill";
  public static final String NsChatbot = "chatbot";
  public static final String NsIntentFinderInstance = "intent-finder-instance";
  public static final String NsIntentFinderPolicy = "intent-finder-policy";
  public static final String NsClassificationRecord = "classification-record";

  // Topic Name
  public static final String NsBackupSignal = "$$backup_signal";
}
