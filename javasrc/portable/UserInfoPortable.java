package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import maum.m2u.common.UserOuterClass.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserInfoPortable implements Portable {

  static final private Logger logger = LoggerFactory.getLogger(UserInfoPortable.class);

  private User protobufObj;
  private String updatedAt;

  public UserInfoPortable() {
    protobufObj = User.newBuilder().build();
  }

  public User getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(User protobufObj) {
    this.protobufObj = protobufObj;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getUpdatedAt() {
    return this.updatedAt;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.USER_INFO;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());

      try {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        writer.writeUTF("updated_at", df.format(new Date()));
      } catch (Exception e) {
        logger.error("writePortable e : " , e);
      }
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(User.parseFrom(reader.readByteArray("_msg")));
    this.setUpdatedAt(reader.readUTF("updated_at"));

    try {
      logger.debug("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String.format("[%s] [%s], [%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
        this.getProtobufObj().toString(), "updatedAt", this.getUpdatedAt());
  }
}
