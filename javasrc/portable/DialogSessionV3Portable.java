package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import java.io.IOException;
import maum.m2u.router.v3.Session.DialogSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
// 세션 정보
//
// 세션에서 하지 말아야할 관리 대상
//  - 장치 정보는 매번 바뀌고 장치가 initiator 이므로 중간에 캐시하지 않는다.
//    대신 device_id는 검색을 위해서 유지 한다.
//  - 사용자 정보도 매번 바뀌고 MAP에서 새롭게 유지된다. 중간에 캐시하지 않는다.
//    대신 user_id를 유지한다.
message DialogSession {

  // ID: 세션을 구분짓는 유일한 정보들

  // ID
  // 세션별로 UNIQUE한 ID값 유지
  // IMDG 서버가 새로 시작하면 time_t 를 구하여 `<< 30` 처리한 값을 초기값을
  // 초기화한다.
  int64 id = 1;

  // 사용자 UNIQUE ID
  // 이 값은 IMDG에서 검색이 가능해야 한다.
  string user_id = 2;

  // 디바이스 UNIQUE ID
  // 이 값은 IMDG에서 검색이 가능해야 한다.
  string device_id = 3;
  // 디바이스 버전
  string device_version = 4;
  // 챗봇 접근경로(M2U Admin, MAP-cli, 스피커, 모바일 App, 서비스 등)
  string channel = 5;
  // 원격 PEER의 정보의 정보
  string peer = 6;

  // 대화 진행에 관련된 정보들

  // 현재의 챗봇 정보
  // 챗봇이 바뀌면 세션을 종료하고 새롭게 다시 만들어야 한다.
  // 챗봇은 세션 동안에만 유일하다.
  string chatbot = 11;

  // 이 세션에서 참조하는 INTENT FINDER POLICY의 이름
  // Router는 이 정책의 이름을 IMDG에서 query를 통해서 구해오고
  // round robin 방식으로 ITF에 접근할 수 있다.
  string intent_finder_policy = 12;

  // 대화 진행에 필요한 컨텍스트
  // 이 컨텍스트는 대화가 종료하면 동시에 사려져야 한다.
  // 참고로 meta는 매 대화마다 새롭게 기록되는 정보이므로
  // 로컬 변수 및 Talk에 유지되어야 한다.
  google.protobuf.Struct context = 13;

  // 마지막에 사용한 대화 skill
  // 만일 대화 호출 후에 `closeSkill = true`인 조건이 발생하게 되면
  // 이 값은 비어있어야 한다. 그래야만 다음 대화에서 ITF를 호출하여 새로운 스킬을
  // 찾을 수 있습니다.
  string last_skill = 21;

  // 현재 대화와 관련된 current agent
  // 대화 에이젠트 인스턴스의 리소스 키, UUID로서
  // 대화 에이전트를 구분해주는 하나의 키이다.
  // 멀티턴 대화의 경우에는 이 키를 사용해서 이전 대화를 찾아야 합니다.
  // 이전 대화를 찾을 때 last_agent_key가 사라지고 없을 경우에는
  // 같은 서비스를 제공하는 다른 DAIR를 찾아야 합니다.
  // 이때 (chatbot, skill, lang) 튜플을 사용해야 합니다.
  string last_agent_key = 22;

  // 세션의 상태

  // 세션을 무효화 시킨다.
  // 관리자에 의해서 이 세션을 강제로 무효화 할 수 있다.
  // logger는 무효화된 세션을 바로 종료시키고 RDB로 저장한다.
  bool valid = 31;

  // 현재 대화가 진행 중인지를 표시한다.
  // Router는 내부적으로 매 대화마다 소요시간을 측정하도록 하고
  // 측정을 시작할 때 talking을 켜고, 응답을 내보내면 끈다.
  bool talking = 32;

  //
  // TODO: CONFINENCE LEVEL 정보는 추후에 더 구체적으로 정의한다.
  // TODO: dilaog trigger 기능 은 ARCHITECTURE 재구성 후 구현

  // 대화 통게 관련 데이터

  // 누적된 대화 시간을 기록합니다.
  // 여기에는 실지로 대화 호출에 사용된 누적된 시간입니다.
  // 함수 호출, 응답대기를 포함한 시간입니다.
  google.protobuf.Duration accumulated_time = 101;

  // 처음 대화가 시작한 시간
  google.protobuf.Timestamp started_at = 102;

  // 마지막 대화 시간
  // 이 값은 세션 만료시간을 판단하는 데 중요한 기준이 된다.
  // logger는 무효화된 세션을 바로 종료시키고 RDB로 저장한다.
  google.protobuf.Timestamp last_talked_at = 103;

  //
  // 문제가 있는 대화에 대한 관리 사항은 추후에 진행한다.
  // 그래서 여기에서 정의하지 않는다.
}
*/

public class DialogSessionV3Portable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(DialogSessionV3Portable.class);

  private DialogSession protobufObj;

  public DialogSessionV3Portable() {
    protobufObj = DialogSession.newBuilder().build();
  }

  public DialogSession getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogSession protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_SESSION_V3;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    DialogSession protobufObj = getProtobufObj();
    writer.writeLong("id", protobufObj.getId());
    if (protobufObj != null) {
      writer.writeByteArray("_msg", protobufObj.toByteArray());
    }
    if (protobufObj.getUserId() != null) {
      writer.writeUTF("user_id", protobufObj.getUserId());
    }
    if (protobufObj.getDeviceId() != null) {
      writer.writeUTF("device_id", protobufObj.getDeviceId());
    }
    if (protobufObj.getDeviceType() != null) {
      writer.writeUTF("device_type", protobufObj.getDeviceType());
    }
    if (protobufObj.getDeviceVersion() != null) {
      writer.writeUTF("device_version", protobufObj.getDeviceVersion());
    }
    if (protobufObj.getChannel() != null) {
      writer.writeUTF("channel", protobufObj.getChannel());
    }
    if (protobufObj.getChatbot() != null) {
      writer.writeUTF("chatbot", protobufObj.getChatbot());
    }
    writer.writeBoolean("valid", protobufObj.getValid());
    writer.writeBoolean("talking", protobufObj.getTalking());
    writer.writeLong("started_at", protobufObj.getStartedAt().getSeconds());
    writer.writeLong("last_talked_at", protobufObj.getLastTalkedAt().getSeconds());

// 속도 너무 느려지셔 주석 처리
//    try {
//      logger.debug("writePortable : {}", this.toString());
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogSession.parseFrom(reader.readByteArray("_msg")));

//    속도 너무 느려지셔 주석 처리
//    try {
//      logger.debug("readPortable : {}", this.toString());
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
