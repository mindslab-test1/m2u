package ai.maum.m2u.hazelcast;

import ai.maum.m2u.common.portable.PortableClassId;
import ai.maum.m2u.hazelcast.mapstore.FileDBConnector;
import ai.maum.m2u.hazelcast.mapstore.MapStorePrepareFileDB;
import ai.maum.m2u.hazelcast.mapstore.MapStoreBackupHandler;
import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import com.hazelcast.core.ITopic;
import java.io.IOException;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HazelcastNodeLauncher {

  static final private Logger logger = LoggerFactory.getLogger(HazelcastNodeLauncher.class);


  void loadFileDb(String id) {
    FileDBConnector.setId(id);
    MapStorePrepareFileDB fileDb = new MapStorePrepareFileDB();

    try {
      fileDb.createDB();
      fileDb.createDBSession();
      fileDb.createDBTalk();
      fileDb.createDBSessionV3();
      fileDb.createDBTalkV3();
      fileDb.createDBClassification();
      fileDb.createDBUserInfo();
    } catch (SQLException e) {
      logger.info("Hazelcast Run without FileDB(for MapStore) ID: {}", id);
      logger.info("Hazelcast File DB, error {}", e);
    } catch (IOException e) {
      logger.info("Hazelcast Run without FileDB(for MapStore) ID: {}", id);
      logger.info("Hazelcast File DB, error {}", e);
    }

    logger.info("Hazelcast Run with File DB(for MapStore) ID: {}", id);
  }


  private void printHzcInfo(HazelcastInstance hz, int port) {
    Config config = hz.getConfig();
    logger.info("==================================================================");
    logger.info("Hazelcast Member - Group Name       : {}", config.getGroupConfig().getName());
    logger.info("Hazelcast Member - Instance Name    : {}", config.getInstanceName());
    logger.info("Hazelcast Member - Service PID[{}] Port[{}]", getPID(), port);
    logger.info("==================================================================");
  }

  public void setInitialSessionId(HazelcastInstance hz) {
    long startId = (System.currentTimeMillis() / 1000L) << 30;
    logger.info("FIRST member {}, session init with start id {}"
        , hz.getLocalEndpoint().getUuid(), startId);
    hz.getAtomicLong("session").set(startId);
  }

  public static long getPID() {
    String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
    if (processName != null && processName.length() > 0) {
      try {
        return Long.parseLong(processName.split("@")[0]);
      } catch (Exception e) {
        return 0;
      }
    }

    return 0;
  }

  /**
   * @return
   */
  public void launch() {
    loadFileDb(HazelcastClientConfig.getMemberId());
    HazelcastInstance hz = Hazelcast.newHazelcastInstance(new XmlConfigBuilder().build());
    int port = hz.getCluster().getLocalMember().getAddress().getPort();

    printHzcInfo(hz, port);

    // 새로 시작한 경우, 오로지 자기만 member에 속해 있는 경우 일때
    // 세션의 번호를 초기화 한다.
    if (hz.getCluster().getMembers().size() == 1) {
      setInitialSessionId(hz);
    }

    ITopic<String> topic = hz.getTopic(PortableClassId.NsBackupSignal);
    topic.publish("all");
    topic.addMessageListener(new MapStoreBackupHandler());
  }
}