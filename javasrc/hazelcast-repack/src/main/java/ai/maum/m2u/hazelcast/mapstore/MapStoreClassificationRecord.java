package ai.maum.m2u.hazelcast.mapstore;

import static java.lang.String.format;

import ai.maum.m2u.common.portable.ClassificationRecordPortable;
import com.hazelcast.core.MapStore;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
maum.m2u.router.v3
// 의도파악 기록 저장 객체
message ClassificationRecord {
  // 세션 ID
  int64 session_id = 1;
  // x-operation-sync-id
  string operation_sync_id = 2;
  // 의도 파악 정책의 이름
  string policy_name = 3;
  // 호출하는 챗봇의 이름
  string chatbot = 4;
  // 의도파악하기 위한 발화
  string utter = 5;

  oneof classified {
    // 식별된 스킬 및 intent의 목록
    FoundIntent skill = 11;
    // 다른 챗봇으로 전이하는 명령어의 구조
    ChatbotTransition transit = 12;
  }

  // 의도파악 과정에서 대체한 문장에 대한 기록
  Replacement replace = 21;

  // 의도파악를 수행한 시간
  google.protobuf.Timestamp classified_at = 100;
}
*/

public class MapStoreClassificationRecord implements MapStore<String, ClassificationRecordPortable> {

  static final Logger logger = LoggerFactory.getLogger(MapStoreClassificationRecord.class);

  public synchronized void store(String key, ClassificationRecordPortable value) {
    PreparedStatement pstmt = null;
    StringBuffer sqlStatment = new StringBuffer();
    sqlStatment.append("select key")
        .append(" from classification_record")
        .append(" where key = '%s'");
    String sql = format(sqlStatment.toString(), key);

    try {
      ResultSet resultSet = FileDBConnector.getConnectionClassification().createStatement().executeQuery(sql);
      if (!resultSet.next()) {
        sqlStatment = new StringBuffer();
        sqlStatment.append("insert into classification_record")
            .append(" (key, bin) ")
            .append(" values")
            .append(" (?, ?)");

        pstmt = FileDBConnector.getConnectionClassification().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, key);
        pstmt.setBytes(2, value.getProtobufObj().toByteArray());
        pstmt.executeUpdate();
      } else {
        sqlStatment = new StringBuffer();
        sqlStatment.append("update classification_record set")
            .append(" bin = ?")
            .append(" where key = ?");
        pstmt = FileDBConnector.getConnectionClassification().prepareStatement(sqlStatment.toString());
        pstmt.setBytes(1, value.getProtobufObj().toByteArray());
        pstmt.setString(2, key);
        pstmt.executeUpdate();
      }
    } catch (SQLException e1) {
      logger.error("{} Exception at store(serialize) {} {}",
          new Object[]{value.getClass().getSimpleName(),
              value.getProtobufObj().getSerializedSize(),
              value.getProtobufObj().toString()}
      );
      logger.error("store e1 : " , e1);
    } finally {
      try {
        pstmt.close();
      } catch (SQLException e1) {
        logger.error("store e1 : " , e1);
      }
    }
  }

  public synchronized void storeAll(Map<String, ClassificationRecordPortable> map) {
    for (Map.Entry<String, ClassificationRecordPortable> entry : map.entrySet()) {
      store(entry.getKey(), entry.getValue());
    }
  }

  public synchronized void delete(String key) {
    try {
      String sql = format("delete from classification_record where key = '%s'", key);
      FileDBConnector.getConnectionClassification().createStatement().executeUpdate(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void deleteAll(Collection<String> keys) {
    for (String key : keys) {
      delete(key);
    }
  }

  public synchronized ClassificationRecordPortable load(String key) {
    try {
      StringBuffer sqlStatment = new StringBuffer();
      sqlStatment.append("select key, bin")
          .append(" from classification_record")
          .append(" where key = '%s'");
      String sql = format(sqlStatment.toString(), key);
      ResultSet resultSet = FileDBConnector.getConnectionClassification().createStatement().executeQuery(sql);
      try {
        if (!resultSet.next()) {
          return null;
        }
        ClassificationRecordPortable loadedObj = new ClassificationRecordPortable();
        try {
          loadedObj.setProtobufObj(
              loadedObj.getProtobufObj().toBuilder().mergeFrom(resultSet.getBinaryStream(2))
                  .build()
          );
        } catch (IOException e) {
          logger.error(
              "{} Exception at load(deserialize) {}",
              this.getClass().getSimpleName(),
              resultSet.getString(2)
          );
          logger.error("load e : " , e);
        }
        return loadedObj;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized Map<String, ClassificationRecordPortable> loadAll(Collection<String> keys) {
    Map<String, ClassificationRecordPortable> result = new HashMap<String, ClassificationRecordPortable>();
    for (String key : keys) {
      result.put(key, load(key));
    }
    return result;
  }

  public Iterable<String> loadAllKeys() {
    try {
      ResultSet resultSet = FileDBConnector.getConnectionClassification().createStatement().executeQuery(
          format("select key from classification_record"));
      try {
        Collection<String> keys = new ArrayList<>();
        while (resultSet.next()) {
          keys.add(resultSet.getString(1));
        }
        return keys;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}