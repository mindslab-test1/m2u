package ai.maum.m2u.hazelcast.mapstore;

import static java.lang.String.format;

import ai.maum.m2u.common.portable.DialogSessionPortable;
import com.hazelcast.core.MapStore;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
// 세션 정보
message DialogSession {
  int64 id = 1;
  string chatbot = 2;
  string user_key = 3;
  map<string, string> contexts = 4;
  google.protobuf.Duration duration = 5;
  bool human_assisted = 6;
  string name = 7;
  Agent current_agent = 8;
  maum.m2u.facade.MediaType output = 9;
  bool valid = 15;
  bool active = 16;
  google.protobuf.Timestamp started_at = 20;
  google.protobuf.Timestamp talked_at = 21;
  maum.m2u.facade.Caller caller = 30;
  string peer = 31;
  int32 warned_talk_count = 41;
  ConfidenceLevel cl = 42;
  float last_cl_result = 43;
  bool interfered = 44;
  repeated AgentUserAttribute agent_user_attrs = 50;
}

RDB Table Schema
create table session (
key long,
msg text,
primary key(key desc));
 */

public class MapStoreDialogSession implements MapStore<Long, DialogSessionPortable> {

  static final Logger logger = LoggerFactory.getLogger(MapStoreDialogSession.class);

  public synchronized void store(Long key, DialogSessionPortable value) {
    PreparedStatement pstmt = null;
    StringBuffer sqlStatment = new StringBuffer();
    sqlStatment.append("select key")
        .append(" from session")
        .append(" where key = %d");
    String sql = format(sqlStatment.toString(), key);
    try {
      ResultSet resultSet = FileDBConnector.getConnectionSession().createStatement().executeQuery(sql);
      if (!resultSet.next()) {
        sqlStatment = new StringBuffer();
        sqlStatment.append("insert into session")
            .append(" (key, bin) ")
            .append(" values")
            .append(" (?, ?)");
        pstmt = FileDBConnector.getConnectionSession().prepareStatement(sqlStatment.toString());
        pstmt.setLong(1, key);
        pstmt.setBytes(2, value.getProtobufObj().toByteArray());
        pstmt.executeUpdate();
      } else {
        sqlStatment = new StringBuffer();
        sqlStatment.append("update session set")
            .append(" bin = ?")
            .append(" where key = ?");
        pstmt = FileDBConnector.getConnectionSession().prepareStatement(sqlStatment.toString());
        pstmt.setBytes(1, value.getProtobufObj().toByteArray());
        pstmt.setLong(2, key);
        pstmt.executeUpdate();
      }
    } catch (SQLException e) {
      logger.error("{} Exception at store(serialize) {} {}",
          new Object[]{value.getClass().getSimpleName(),
              value.getProtobufObj().getSerializedSize(),
              value.getProtobufObj().toString()}
      );
      logger.error("store e : " , e);
    } finally {
      try {
        pstmt.close();
      } catch (SQLException e1) {
        logger.error("store e1 : " , e1);
      }
    }
  }

  public synchronized void storeAll(Map<Long, DialogSessionPortable> map) {
    for (Map.Entry<Long, DialogSessionPortable> entry : map.entrySet()) {
      store(entry.getKey(), entry.getValue());
    }
  }

  public synchronized void delete(Long key) {
    try {
      String sql = format("delete from session where key = %d", key);
      FileDBConnector.getConnectionSession().createStatement().executeUpdate(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void deleteAll(Collection<Long> keys) {
    for (Long key : keys) {
      delete(key);
    }
  }

  public synchronized DialogSessionPortable load(Long key) {
    try {
      StringBuffer sqlStatment = new StringBuffer();
      sqlStatment.append("select key, bin")
          .append(" from session")
          .append(" where key = %d");
      String sql = format(sqlStatment.toString(), key);
      ResultSet resultSet = FileDBConnector.getConnectionSession().createStatement().executeQuery(sql);
      try {
        if (!resultSet.next()) {
          return null;
        }
        DialogSessionPortable loadedObj = new DialogSessionPortable();
        try {
          loadedObj.setProtobufObj(
              loadedObj.getProtobufObj().toBuilder().mergeFrom(resultSet.getBinaryStream(2))
                  .build()
          );
        } catch (IOException e) {
          logger.error(
              "{} Exception at load(deserialize) {}",
              this.getClass().getSimpleName(),
              resultSet.getLong(1)
          );
          logger.error("load e : " , e);
        }
        return loadedObj;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized Map<Long, DialogSessionPortable> loadAll(Collection<Long> keys) {
    Map<Long, DialogSessionPortable> result = new HashMap<Long, DialogSessionPortable>();
    for (Long key : keys) {
      result.put(key, load(key));
    }
    return result;
  }

  public Iterable<Long> loadAllKeys() {
    try {
      ResultSet resultSet = FileDBConnector.getConnectionSession().createStatement().executeQuery(
          format("select key from session "));
      try {
        Collection<Long> keys = new ArrayList<>();
        while (resultSet.next()) {
          keys.add(resultSet.getLong(1));
        }
        return keys;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}