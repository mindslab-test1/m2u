package ai.maum.m2u.hazelcast.mapstore;

import static java.lang.String.format;

import ai.maum.m2u.common.portable.SessionTalkV3Portable;
import com.hazelcast.core.MapStore;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
// 한턴의 대화에 대한 상세 기록
message SessionTalk {
    // 순서
    uint32 seq = 1;
    // DialogSession.id
    int64 session_id = 2;
    // 언어
    maum.common.Lang lang = 3;

    // 스킬
    string skill = 11;
    // 현재의 intent
    string intent = 12;
    // 현재 스킬의 마지막 대화인가?
    bool close_skill = 13;

    // 입력
    // 입력 텍스트
    string in = 21;
    // 입력 유형
    maum.m2u.common.Utter.InputType input_type = 22;
    // 입력 메타, IMAGE와 같은 데이터일 경우에 필요함.
    google.protobuf.Struct input_meta = 23;

    // 출력

    // 출력 텍스트
    string out = 31;
    // TTS로 나가는 내용이 다를 때
    string speech_out = 32;
    // 재질의 상태인 경우
    bool reprompt = 33;

    // 출력 메타 데이터
    map<string, string> output_meta = 41;

    // 출력 context 업데이트
    google.protobuf.Struct update_context = 42;

    // grpc 최종 status
    int32 status_code = 51;
    // 내부 처리 코드
    DialogResult dialog_result = 52;

    // 텍스트 분석, 이건 정말 들어갈 것은 아닌 것 같군요. 제거
    // 복수의 대화 에이전트 정보
    repeated TalkAgent agents = 61;

    // 시작 시간
    google.protobuf.Timestamp start = 71;
    // 끝 시간
    google.protobuf.Timestamp end = 72;
    // 각 구간별 시간
    map<string, google.protobuf.Duration> elapsed_time = 73;

    // *optional* 사용자 피드백
    // 사용자가 매기는 점수, 5점척도 또는 10점 척도, 2점 척도 등 다양하게 정의할 수 있다.
    // 매기지지 않으면 -1 이다.
    // 통계에서 뺀다.
    int32 feedback = 91;

    // *optional* 대화 정확도
    // 관리자가 큐레이션 과정에서 매기는 점수이다.
    int32 accuracy = 92;

    // *optional* 다른 챗봇으로 전달을 위한 챗봇 이름
    // FIXME: 다른 챗봇으로 이전하는 경우에만 사용
    string transit_chatbot = 101;
}

RDB Table Schema
create table talk_v3 (
key long,
msg text,
primary key(key desc));
 */

public class MapStoreSessionTalkV3 implements MapStore<String, SessionTalkV3Portable> {

  static final Logger logger = LoggerFactory.getLogger(MapStoreSessionTalkV3.class);

  public synchronized void store(String key, SessionTalkV3Portable value) {
    PreparedStatement pstmt = null;
    StringBuffer sqlStatment = new StringBuffer();
    sqlStatment.append("select key")
        .append(" from talk_v3")
        .append(" where key = '%s'");
    String sql = format(sqlStatment.toString(), key);
    try {
      ResultSet resultSet = FileDBConnector.getConnectionTalkV3().createStatement().executeQuery(sql);
      if (!resultSet.next()) {
        sqlStatment = new StringBuffer();
        sqlStatment.append("insert into talk_v3")
            .append(" (key, bin) ")
            .append(" values")
            .append(" (?, ?)");
        pstmt = FileDBConnector.getConnectionTalkV3().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, key);
        pstmt.setBytes(2, value.getProtobufObj().toByteArray());
        pstmt.executeUpdate();
      } else {
        sqlStatment = new StringBuffer();
        sqlStatment.append("update talk_v3 set")
            .append(" bin = ?")
            .append(" where key = ?");
        pstmt = FileDBConnector.getConnectionTalkV3().prepareStatement(sqlStatment.toString());
        pstmt.setBytes(1, value.getProtobufObj().toByteArray());
        pstmt.setString(2, key);
        pstmt.executeUpdate();
      }
    } catch (SQLException e1) {
      logger.error("{} Exception at store(serialize) {} {}",
          new Object[]{value.getClass().getSimpleName(),
              value.getProtobufObj().getSerializedSize(),
              value.getProtobufObj().toString()}
      );
      logger.error("store e1 : " , e1);
    } finally {
      try {
        pstmt.close();
      } catch (SQLException e1) {
        logger.error("store e1 : " , e1);
      }
    }
  }

  public synchronized void storeAll(Map<String, SessionTalkV3Portable> map) {
    for (Map.Entry<String, SessionTalkV3Portable> entry : map.entrySet()) {
      this.store(entry.getKey(), entry.getValue());
    }
  }

  public synchronized void delete(String key) {
    try {
      String sql = format("delete from talk_v3 where key = '%s'", key);
      FileDBConnector.getConnectionTalkV3().createStatement().executeUpdate(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void deleteAll(Collection<String> keys) {
    for (String key : keys) {
      this.delete(key);
    }
  }

  public synchronized SessionTalkV3Portable load(String key) {
    try {
      StringBuffer sqlStatment = new StringBuffer();
      sqlStatment.append("select key, bin")
          .append(" from talk_v3")
          .append(" where key = '%s'");
      String sql = format(sqlStatment.toString(), key);
      ResultSet resultSet = FileDBConnector.getConnectionTalkV3().createStatement().executeQuery(sql);
      try {
        if (!resultSet.next()) {
          return null;
        }
        SessionTalkV3Portable loadedObj = new SessionTalkV3Portable();
        try {
          loadedObj.setProtobufObj(
              loadedObj.getProtobufObj().toBuilder().mergeFrom(resultSet.getBinaryStream(2))
                  .build()
          );
        } catch (IOException e) {
          logger.error(
              "{} Exception at load(deserialize) {}",
              this.getClass().getSimpleName(),
              resultSet.getString(1)
          );
          logger.error("SessionTalkV3Portable e : " , e);
        }
        return loadedObj;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized Map<String, SessionTalkV3Portable> loadAll(Collection<String> keys) {
    Map<String, SessionTalkV3Portable> result = new HashMap<String, SessionTalkV3Portable>();
    for (String key : keys) {
      result.put(key, load(key));
    }
    return result;
  }

  public Iterable<String> loadAllKeys() {
    try {
      ResultSet resultSet = FileDBConnector.getConnectionTalkV3().createStatement().executeQuery(
          format("select key from talk_v3 "));
      try {
        Collection<String> keys = new ArrayList<>();
        while (resultSet.next()) {
          keys.add(resultSet.getString(1));
        }
        return keys;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}