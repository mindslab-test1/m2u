package ai.maum.m2u.hazelcast.mapstore;

import static java.lang.String.format;

import ai.maum.m2u.common.portable.DialogSessionV3Portable;
import com.hazelcast.core.MapStore;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
// 세션 정보
//
// 세션에서 하지 말아야할 관리 대상
//  - 장치 정보는 매번 바뀌고 장치가 initiator 이므로 중간에 캐시하지 않는다.
//    대신 device_id는 검색을 위해서 유지 한다.
//  - 사용자 정보도 매번 바뀌고 MAP에서 새롭게 유지된다. 중간에 캐시하지 않는다.
//    대신 user_id를 유지한다.
message DialogSession {

    // ID: 세션을 구분짓는 유일한 정보들

    // ID
    // 세션별로 UNIQUE한 ID값 유지
    // IMDG 서버가 새로 시작하면 time_t 를 구하여 `<< 30` 처리한 값을 초기값을
    // 초기화한다.
    int64 id = 1;

    // 사용자 UNIQUE ID
    // 이 값은 IMDG에서 검색이 가능해야 한다.
    // INDEX로 걸려 있어야 한다.
    string user_id = 2;

    // 디바이스 UNIQUE ID
    // 이 값은 IMDG에서 검색이 가능해야 하며
    // INDEX로 걸려 있어야 한다.
    string device_id = 3;
    // 원격 PEER의 정보의 정보
    string peer = 4;

    // 대화 진행에 관련된 정보들

    // 현재의 챗봇 정보
    // 챗봇이 바뀌면 세션을 종료하고 새롭게 다시 만들어야 한다.
    // 챗봇은 세션 동안에만 유일하다.
    string chatbot = 11;

    // 이 세션에서 참조하는 INTENT FINDER POLICY의 이름
    // Router는 이 정책의 이름을 IMDG에서 query를 통해서 구해오고
    // round robin 방식으로 ITF에 접근할 수 있다.
    string intent_finder_policy_name = 12;

    // 대화 진행에 필요한 컨텍스트
    // 이 컨텍스트는 대화가 종료하면 동시에 사려져야 한다.
    // 참고로 meta는 매 대화마다 새롭게 기록되는 정보이므로
    // 로컬 변수 및 Talk에 유지되어야 한다.
    google.protobuf.Struct context = 13;


    // 마지막에 사용한 대화 skill
    // 만일 대화 호출 후에 `closeSkill = true`인 조건이 발생하게 되면
    // 이 값은 비어있어야 한다. 그래야만 다음 대화에서 ITF를 호출하여 새로운 스킬을
    // 찾을 수 있습니다.
    string last_skill = 21;

    // 현재 대화와 관련된 current agent
    // 대화 에이젠트 인스턴스의 리소스 키, UUID로서
    // 대화 에이전트를 구분해주는 하나의 키이다.
    // 멀티턴 대화의 경우에는 이 키를 사용해서 이전 대화를 찾아야 합니다.
    // 이전 대화를 찾을 때 last_agent_key가 사라지고 없을 경우에는
    // 같은 서비스를 제공하는 다른 DAIR를 찾아야 합니다.
    // 이때 (chatbot, skill, lang) 튜플을 사용해야 합니다.
    string last_agent_key = 22;

    // 세션의 상태

    // 세션을 무효화 시킨다.
    // 관리자에 의해서 이 세션을 강제로 무효화 할 수 있다.
    // logger는 무효화된 세션을 바로 종료시키고 RDB로 저장한다.
    bool valid = 31;

    // 현재 대화가 진행 중인지를 표시한다.
    // Router는 내부적으로 매 대화마다 소요시간을 측정하도록 하고
    // 측정을 시작할 때 talking을 켜고, 응답을 내보내면 끈다.
    bool talking = 32;

    //
    // TODO: CONFINENCE LEVEL 정보는 추후에 더 구체적으로 정의한다.
    // TODO: dilaog trigger 기능 은 ARCHITECTURE 재구성 후 구현

    // 대화 통게 관련 데이터

    // 누적된 대화 시간을 기록합니다.
    // 여기에는 실지로 대화 호출에 사용된 누적된 시간입니다.
    // 함수 호출, 응답대기를 포함한 시간입니다.
    google.protobuf.Duration accumulated_time = 101;

    // 처음 대화가 시작한 시간
    google.protobuf.Timestamp started_at = 102;

    // 마지막 대화 시간
    // 이 값은 세션 만료시간을 판단하는 데 중요한 기준이 된다.
    // logger는 무효화된 세션을 바로 종료시키고 RDB로 저장한다.
    google.protobuf.Timestamp last_talked_at = 103;


    //
    // 문제가 있는 대화에 대한 관리 사항은 추후에 진행한다.
    // 그래서 여기에서 정의하지 않는다.
}

RDB Table Schema
create table session_v3 (
key long,
msg text,
primary key(key desc));
 */

public class MapStoreDialogSessionV3 implements MapStore<Long, DialogSessionV3Portable> {

  static final Logger logger = LoggerFactory.getLogger(MapStoreDialogSessionV3.class);

  public synchronized void store(Long key, DialogSessionV3Portable value) {
    PreparedStatement pstmt = null;
    StringBuffer sqlStatment = new StringBuffer();
    sqlStatment.append("select key")
        .append(" from session_v3")
        .append(" where key = %d");
    String sql = format(sqlStatment.toString(), key);
    try {
      ResultSet resultSet = FileDBConnector.getConnectionSessionV3().createStatement()
          .executeQuery(sql);
      if (!resultSet.next()) {
        sqlStatment = new StringBuffer();
        sqlStatment.append("insert into session_v3")
            .append(" (key, bin) ")
            .append(" values")
            .append(" (?, ?)");
        pstmt = FileDBConnector.getConnectionSessionV3().prepareStatement(sqlStatment.toString());
        pstmt.setLong(1, key);
        pstmt.setBytes(2, value.getProtobufObj().toByteArray());
        pstmt.executeUpdate();
      } else {
        sqlStatment = new StringBuffer();
        sqlStatment.append("update session_v3 set")
            .append(" bin = ?")
            .append(" where key = ?");
        pstmt = FileDBConnector.getConnectionSessionV3().prepareStatement(sqlStatment.toString());
        pstmt.setBytes(1, value.getProtobufObj().toByteArray());
        pstmt.setLong(2, key);
        pstmt.executeUpdate();
      }
    } catch (SQLException e) {
      logger.error("{} Exception at store(serialize) {} {}",
          new Object[]{value.getClass().getSimpleName(),
              value.getProtobufObj().getSerializedSize(),
              value.getProtobufObj().toString()}
      );
      logger.error("store e : " , e);
    } finally {
      try {
        pstmt.close();
      } catch (SQLException e1) {
        logger.error("store e1 : " , e1);
      }
    }
  }

  public synchronized void storeAll(Map<Long, DialogSessionV3Portable> map) {
    for (Map.Entry<Long, DialogSessionV3Portable> entry : map.entrySet()) {
      store(entry.getKey(), entry.getValue());
    }
  }

  public synchronized void delete(Long key) {
    try {
      String sql = format("delete from session_v3 where key = %d", key);
      FileDBConnector.getConnectionSessionV3().createStatement().executeUpdate(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void deleteAll(Collection<Long> keys) {
    for (Long key : keys) {
      delete(key);
    }
  }

  public synchronized DialogSessionV3Portable load(Long key) {
    try {
      StringBuffer sqlStatment = new StringBuffer();
      sqlStatment.append("select key, bin")
          .append(" from session_v3")
          .append(" where key = %d");
      String sql = format(sqlStatment.toString(), key);
      ResultSet resultSet = FileDBConnector.getConnectionSessionV3().createStatement()
          .executeQuery(sql);
      try {
        if (!resultSet.next()) {
          return null;
        }
        DialogSessionV3Portable loadedObj = new DialogSessionV3Portable();
        try {
          loadedObj.setProtobufObj(
              loadedObj.getProtobufObj().toBuilder().mergeFrom(resultSet.getBinaryStream(2))
                  .build()
          );
        } catch (IOException e) {
          logger.error(
              "{} Exception at load(deserialize) {}",
              this.getClass().getSimpleName(),
              resultSet.getLong(1)
          );
          logger.error("load e : " , e);
        }
        return loadedObj;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized Map<Long, DialogSessionV3Portable> loadAll(Collection<Long> keys) {
    Map<Long, DialogSessionV3Portable> result = new HashMap<Long, DialogSessionV3Portable>();
    for (Long key : keys) {
      result.put(key, load(key));
    }
    return result;
  }

  public Iterable<Long> loadAllKeys() {
    try {
      ResultSet resultSet = FileDBConnector.getConnectionSessionV3().createStatement().executeQuery(
          format("select key from session_v3 "));
      try {
        Collection<Long> keys = new ArrayList<>();
        while (resultSet.next()) {
          keys.add(resultSet.getLong(1));
        }
        return keys;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}