package ai.maum.m2u.hazelcast.mapstore;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MapStorePrepareFileDB {

  private static final Logger logger = LoggerFactory.getLogger(MapStorePrepareFileDB.class);
  private static final String PRAGMA_AUTO_VACUUM_FULL = "PRAGMA auto_vacuum=1;";
  private static final String PRAGMA_SYNCHRONOUS_EXTRA = "PRAGMA synchronous=EXTRA;";
  private static final String PRAGMA_SYNCHRONOUS_NORMAL = "PRAGMA synchronous=NORMAL;";
  private static final String PRAGMA_SYNCHRONOUS_OFF = "PRAGMA synchronous=OFF;";
  private static final String PRAGMA_JOURNAL_MODE_MEMORY = "PRAGMA journal_mode=MEMORY;";
  private static final String PRAGMA_JOURNAL_MODE_WAL = "PRAGMA journal_mode=WAL;";

  private void createTable(Connection conn, String table) throws SQLException {
    Statement stmt = conn.createStatement();
    StringBuilder sqlStatement = new StringBuilder();
    try {
      sqlStatement
          .append("CREATE TABLE IF NOT EXISTS ").append(table).append(" (")
          .append("key text,")
          .append("msg text,")
          .append("bin blob,")
          .append("PRIMARY KEY(key asc));");
      stmt.execute(sqlStatement.toString());
    } catch (SQLException e) {
      logger.error("CREATE Mapstore Table {} Fail!", table);
      logger.error("sqlStatement : {}", sqlStatement.toString());
      throw new SQLException("CREATE Mapstore Table Fail!");
    } finally {
      try {
        stmt.close();
      } catch (SQLException e) {
        logger.warn("Statement close Fail!");
      }
    }
  }

  private void createTableLongLog(Connection conn, String table) throws SQLException {
    Statement stmt = conn.createStatement();
    StringBuilder sqlStatement = new StringBuilder();
    try {
      sqlStatement
          .append("CREATE TABLE IF NOT EXISTS ").append(table).append(" (")
          .append("key long,")
          .append("bin blob,")
          .append("PRIMARY KEY(key asc));");
      stmt.execute(sqlStatement.toString());
    } catch (SQLException e) {
      logger.error("CREATE Mapstore Table {} Fail!", table);
      logger.error("sqlStatement : {}", sqlStatement.toString());
      throw new SQLException("CREATE Mapstore Table Fail!");
    } finally {
      try {
        stmt.close();
      } catch (SQLException e) {
        logger.warn("Statement close Fail!");
      }
    }
  }

  private void createTableTextLog(Connection conn, String table) throws SQLException {
    Statement stmt = conn.createStatement();
    StringBuilder sqlStatement = new StringBuilder();
    try {
      sqlStatement
          .append("CREATE TABLE IF NOT EXISTS ").append(table).append(" (")
          .append("key text,")
          .append("bin blob,")
          .append("PRIMARY KEY(key asc));");
      stmt.execute(sqlStatement.toString());
    } catch (SQLException e) {
      logger.error("CREATE Mapstore Table {} Fail!", table);
      logger.error("sqlStatement : {}", sqlStatement.toString());
      throw new SQLException("CREATE Mapstore Table Fail!");
    } finally {
      try {
        stmt.close();
      } catch (SQLException e) {
        logger.warn("Statement close Fail!");
      }
    }
  }

  public void createDB() throws SQLException, IOException {
    String mindsRoot = "";

    Path dbFile = Paths.get(mindsRoot, "db", FileDBConnector.getDbPath(), "m2u.db");

    try {
      if (!Files.exists(dbFile.getParent())) {
        Files.createDirectories(dbFile.getParent());
      }
    } catch (IOException e) {
      throw new IOException("create Directories " + dbFile.getParent() + " Fail!");
    }

    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      logger.warn("Thread.sleep Occur InterruptedException!");
    }

    Connection conn = FileDBConnector.getConnection();

    createTable(conn, "auth_policy");
    createTable(conn, "chatbot");
    createTable(conn, "dialog_agent");
    createTable(conn, "dialog_agent_activation");
    createTable(conn, "dialog_agent_instance");
    createTable(conn, "dialog_agent_instance_exec");
    createTable(conn, "dialog_agent_instance_resource");
    createTable(conn, "dialog_agent_instance_resource_skill");
    createTable(conn, "dialog_agent_manager");
    createTable(conn, "intent_finder");
    createTable(conn, "intent_finder_instance");
    createTable(conn, "intent_finder_policy");
    createTable(conn, "simple_classifier");
    createTable(conn, "skill");

    Statement stmt = conn.createStatement();
    stmt.execute(PRAGMA_SYNCHRONOUS_NORMAL + PRAGMA_JOURNAL_MODE_WAL);

    try {
      if (!stmt.isClosed()) {
        stmt.close();
      }
    } catch (SQLException e) {
      logger.warn("Statement close Fail!");
    }
  }

  public void createDBSession() throws SQLException, IOException {
    String mindsRoot = "";

    Path dbFile = Paths.get(mindsRoot, "db", FileDBConnector.getDbPath(), "m2u-session.db");

    try {
      if (!Files.exists(dbFile.getParent())) {
        Files.createDirectories(dbFile.getParent());
      }
    } catch (IOException e) {
      throw new IOException("create Directories " + dbFile.getParent() + " Fail!");
    }

    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      logger.warn("Thread.sleep Occur InterruptedException!");
    }

    Connection conn = FileDBConnector.getConnectionSession();
    createTableLongLog(conn, "session");

    Statement stmt = conn.createStatement();
    stmt.execute(
        PRAGMA_AUTO_VACUUM_FULL + PRAGMA_SYNCHRONOUS_OFF + PRAGMA_JOURNAL_MODE_MEMORY);

    try {
      if (!stmt.isClosed()) {
        stmt.close();
      }
    } catch (SQLException e) {
      logger.warn("Statement close Fail!");
    }
  }

  public void createDBTalk() throws SQLException, IOException {
    String mindsRoot = "";

    Path dbFile = Paths.get(mindsRoot, "db", FileDBConnector.getDbPath(), "m2u-talk.db");

    try {
      if (!Files.exists(dbFile.getParent())) {
        Files.createDirectories(dbFile.getParent());
      }
    } catch (IOException e) {
      throw new IOException("create Directories " + dbFile.getParent() + " Fail!");
    }

    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      logger.warn("Thread.sleep Occur InterruptedException!");
    }

    Connection conn = FileDBConnector.getConnectionTalk();
    createTableTextLog(conn, "talk");

    Statement stmt = conn.createStatement();
    stmt.execute(
        PRAGMA_AUTO_VACUUM_FULL + PRAGMA_SYNCHRONOUS_OFF + PRAGMA_JOURNAL_MODE_MEMORY);

    try {
      if (!stmt.isClosed()) {
        stmt.close();
      }
    } catch (SQLException e) {
      logger.warn("Statement close Fail!");
    }
  }

  public void createDBSessionV3() throws SQLException, IOException {
    String mindsRoot = "";

    Path dbFile = Paths.get(mindsRoot, "db", FileDBConnector.getDbPath(), "m2u-session-v3.db");

    try {
      if (!Files.exists(dbFile.getParent())) {
        Files.createDirectories(dbFile.getParent());
      }
    } catch (IOException e) {
      throw new IOException("create Directories " + dbFile.getParent() + " Fail!");
    }

    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      logger.warn("Thread.sleep Occur InterruptedException!");
    }

    Connection conn = FileDBConnector.getConnectionSessionV3();
    createTableLongLog(conn, "session_v3");

    Statement stmt = conn.createStatement();
    stmt.execute(
        PRAGMA_AUTO_VACUUM_FULL + PRAGMA_SYNCHRONOUS_OFF + PRAGMA_JOURNAL_MODE_MEMORY);

    try {
      if (!stmt.isClosed()) {
        stmt.close();
      }
    } catch (SQLException e) {
      logger.warn("Statement close Fail!");
    }
  }

  public void createDBTalkV3() throws SQLException, IOException {
    String mindsRoot = "";

    Path dbFile = Paths.get(mindsRoot, "db", FileDBConnector.getDbPath(), "m2u-talk-v3.db");

    try {
      if (!Files.exists(dbFile.getParent())) {
        Files.createDirectories(dbFile.getParent());
      }
    } catch (IOException e) {
      throw new IOException("create Directories " + dbFile.getParent() + " Fail!");
    }

    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      logger.warn("Thread.sleep Occur InterruptedException!");
    }

    Connection conn = FileDBConnector.getConnectionTalkV3();
    createTableTextLog(conn, "talk_v3");

    Statement stmt = conn.createStatement();
    stmt.execute(
        PRAGMA_AUTO_VACUUM_FULL + PRAGMA_SYNCHRONOUS_OFF + PRAGMA_JOURNAL_MODE_MEMORY);

    try {
      if (!stmt.isClosed()) {
        stmt.close();
      }
    } catch (SQLException e) {
      logger.warn("Statement close Fail!");
    }
  }

  public void createDBClassification() throws SQLException, IOException {
    String mindsRoot = "";

    Path dbFile = Paths.get(mindsRoot,
        "db", FileDBConnector.getDbPath(), "m2u-classification.db");

    try {
      if (!Files.exists(dbFile.getParent())) {
        Files.createDirectories(dbFile.getParent());
      }
    } catch (IOException e) {
      throw new IOException("create Directories " + dbFile.getParent() + " Fail!");
    }

    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      logger.warn("Thread.sleep Occur InterruptedException!");
    }

    Connection conn = FileDBConnector.getConnectionClassification();
    createTableTextLog(conn, "classification_record");

    Statement stmt = conn.createStatement();
    stmt.execute(
        PRAGMA_AUTO_VACUUM_FULL + PRAGMA_SYNCHRONOUS_OFF + PRAGMA_JOURNAL_MODE_MEMORY);

    try {
      if (!stmt.isClosed()) {
        stmt.close();
      }
    } catch (SQLException e) {
      logger.warn("Statement close Fail!");
    }
  }

  public void createDBUserInfo() throws SQLException, IOException {
    String mindsRoot = "";

    Path dbFile = Paths.get(mindsRoot, "db", FileDBConnector.getDbPath(), "m2u-user-info.db");

    try {
      if (!Files.exists(dbFile.getParent())) {
        Files.createDirectories(dbFile.getParent());
      }
    } catch (IOException e) {
      throw new IOException("create Directories " + dbFile.getParent() + " Fail!");
    }

    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      logger.warn("Thread.sleep Occur InterruptedException!");
    }

    Connection conn = FileDBConnector.getConnectionUserInfo();
    createTableTextLog(conn, "user_info");

    Statement stmt = conn.createStatement();
    stmt.execute(PRAGMA_AUTO_VACUUM_FULL + PRAGMA_SYNCHRONOUS_OFF + PRAGMA_JOURNAL_MODE_MEMORY);

    try {
      if (!stmt.isClosed()) {
        stmt.close();
      }
    } catch (SQLException e) {
      logger.warn("Statement close Fail!");
    }
  }
}