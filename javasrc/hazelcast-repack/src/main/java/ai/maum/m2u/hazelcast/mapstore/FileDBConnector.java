package ai.maum.m2u.hazelcast.mapstore;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileDBConnector {

  private static final Logger logger = LoggerFactory.getLogger(FileDBConnector.class);
  private static final String SqlitePrefix = "jdbc:sqlite:";

  private static String id;
  private static String dbPath;

  /**
   * 프로세스 시작 시점에 임의대로 결정된 PORT를 유지한다.
   *
   * @param id Hazelcast node의 Id
   */
  public static void setId(String id) {
    if (FileDBConnector.id == null) {
      FileDBConnector.id = id;
    }
  }

  public static String getDbPath() {
    if (dbPath == null) {
      dbPath = "hazelcast-" + id;
    }

    return dbPath;
  }

  private static class Singleton {
    private static final Connection conn = new FileDBConnector().connect();
  }

  public static Connection getConnection() {
    return Singleton.conn;
  }

  private Connection connect() {
    String mindsRoot = "";
    Path dbFileConfig = Paths.get(mindsRoot, "db", getDbPath(), "m2u.db");
    String urlConfig = SqlitePrefix + dbFileConfig.toString();
    //create Connection
    try {
      Connection initConn = DriverManager.getConnection(urlConfig);
      logger.debug("FileDB connection {} Established.", urlConfig);
      return initConn;
    } catch (SQLException e) {
      logger.error("FileDB connection {} Failed!!!", urlConfig);
      return null;
    }
  }

  private static class SingletonSession {
    private static final Connection conn = new FileDBConnector().connectSession();
  }

  public static Connection getConnectionSession() {
    return SingletonSession.conn;
  }

  private Connection connectSession() {
    String mindsRoot = "";
    Path dbFileConfig = Paths.get(mindsRoot, "db", getDbPath(), "m2u-session.db");
    String urlConfig = SqlitePrefix + dbFileConfig.toString();
    //create Connection
    try {
      Connection initConn = DriverManager.getConnection(urlConfig);
      logger.debug("FileDBSession connection {} Established.", urlConfig);
      return initConn;
    } catch (SQLException e) {
      logger.error("FileDBSession connection {} Failed!!!", urlConfig);
      return null;
    }
  }

  private static class SingletonTalk {
    private static final Connection conn = new FileDBConnector().connectTalk();
  }

  public static Connection getConnectionTalk() {
    return SingletonTalk.conn;
  }

  private Connection connectTalk() {
    String mindsRoot = "";
    Path dbFileConfig = Paths.get(mindsRoot, "db", getDbPath(), "m2u-talk.db");
    String urlConfig = SqlitePrefix + dbFileConfig.toString();
    //create Connection
    try {
      Connection initConn = DriverManager.getConnection(urlConfig);
      logger.debug("FileDBTalk connection {} Established.", urlConfig);
      return initConn;
    } catch (SQLException e) {
      logger.error("FileDBTalk connection {} Failed!!!", urlConfig);
      return null;
    }
  }

  private static class SingletonSessionV3 {
    private static final Connection conn = new FileDBConnector().connectSessionV3();
  }

  public static Connection getConnectionSessionV3() {
    return SingletonSessionV3.conn;
  }

  private Connection connectSessionV3() {
    String mindsRoot = "";
    Path dbFileConfig = Paths.get(mindsRoot, "db", getDbPath(), "m2u-session-v3.db");
    String urlConfig = SqlitePrefix + dbFileConfig.toString();
    //create Connection
    try {
      Connection initConn = DriverManager.getConnection(urlConfig);
      logger.debug("FileDBSessionV3 connection {} Established.", urlConfig);
      return initConn;
    } catch (SQLException e) {
      logger.error("FileDBSessionV3 connection {} Failed!!!", urlConfig);
      return null;
    }
  }

  private static class SingletonTalkV3 {
    private static final Connection conn = new FileDBConnector().connectTalkV3();
  }

  public static Connection getConnectionTalkV3() {
    return SingletonTalkV3.conn;
  }

  private Connection connectTalkV3() {
    String mindsRoot = "";
    Path dbFileConfig = Paths.get(mindsRoot, "db", getDbPath(), "m2u-talk-v3.db");
    String urlConfig = SqlitePrefix + dbFileConfig.toString();
    //create Connection
    try {
      Connection initConn = DriverManager.getConnection(urlConfig);
      logger.debug("FileDBTalkV3 connection {} Established.", urlConfig);
      return initConn;
    } catch (SQLException e) {
      logger.error("FileDBTalkV3 connection {} Failed!!!", urlConfig);
      return null;
    }
  }

  private static class SingletonClassification {
    private static final Connection conn = new FileDBConnector().connectClassification();
  }

  public static Connection getConnectionClassification() {
    return SingletonClassification.conn;
  }

  private Connection connectClassification() {
    String mindsRoot = "";
    Path dbFileConfig = Paths.get(mindsRoot, "db", getDbPath(), "m2u-classification.db");
    String urlConfig = SqlitePrefix + dbFileConfig.toString();
    //create Connection
    try {
      Connection initConn = DriverManager.getConnection(urlConfig);
      logger.debug("FileDBClassification connection {} Established.", urlConfig);
      return initConn;
    } catch (SQLException e) {
      logger.error("FileDBFileDBClassification connection {} Failed!!!", urlConfig);
      return null;
    }
  }

  private static class SingletonUserInfo {
    private static final Connection conn = new FileDBConnector().connectUserInfo();
  }

  public static Connection getConnectionUserInfo() {
    return SingletonUserInfo.conn;
  }

  private Connection connectUserInfo() {
    String mindsRoot = "";
    Path dbFileConfig = Paths.get(mindsRoot, "db", getDbPath(), "m2u-user-info.db");
    String urlConfig = SqlitePrefix + dbFileConfig.toString();
    //create Connection
    try {
      Connection initConn = DriverManager.getConnection(urlConfig);
      logger.debug("FileDBUserInfo connection {} Established.", urlConfig);
      return initConn;
    } catch (SQLException e) {
      logger.error("FileDBUserInfo connection {} Failed!!!", urlConfig);
      return null;
    }
  }

}
