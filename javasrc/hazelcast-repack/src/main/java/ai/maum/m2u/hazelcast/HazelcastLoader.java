package ai.maum.m2u.hazelcast;

import ai.maum.rpc.ResultStatus;
import maum.rpc.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HazelcastLoader {

  static final Logger logger = LoggerFactory.getLogger(HazelcastLoader.class);

  static {
    ResultStatus.setModuleAndProcess(Status.Module.M2U, Status.ProcessOfM2u.M2U_HZC.getNumber());
  }

  public static void main(String[] args) {

    try {
      HazelcastClientConfig.loadConfig();

      logger.info("*****************************");
      logger.info("    Cluster Member ID: {}", HazelcastClientConfig.getMemberId());
      logger.info("*****************************");

      new HazelcastNodeLauncher().launch();
    } catch (Exception e) {
      logger.error("<<< Stop to launch a Hazelcast node. >>>");
    }
  }

}

