package ai.maum.m2u.hazelcast.mapstore;

import ai.maum.m2u.common.portable.PortableClassId;
import ai.maum.m2u.common.portable.ServiceAdminPortableFactory;
import ai.maum.m2u.hazelcast.HazelcastClientConfig;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;


public class MapStoreBackupHandler implements MessageListener<String> {

  static final private Logger logger = LoggerFactory.getLogger(MapStoreBackupHandler.class);

  private ClientConfig clientConfig;

  public MapStoreBackupHandler() {
    clientConfig = HazelcastClientConfig.newConfig();

    clientConfig.getSerializationConfig()
        .addPortableFactory(1, new ServiceAdminPortableFactory());
    clientConfig.getSerializationConfig()
        .addPortableFactory(2, new ServiceAdminPortableFactory());
    clientConfig.getSerializationConfig()
        .addPortableFactory(3, new ServiceAdminPortableFactory());
  }

  private synchronized void backupSettings(String target) {
    logger.debug("target: {}", target);
    HazelcastInstance hzc = null;

    try {
      boolean backupAll = target.equals("all");
      hzc = HazelcastClient.newHazelcastClient(clientConfig);

      if (backupAll || target.equals(PortableClassId.NsAuthPolicy)) {
        new MapStoreAuthticationPolicy().backupAll(hzc.getMap(PortableClassId.NsAuthPolicy));
      }
      if (backupAll || target.equals(PortableClassId.NsChatbot)) {
        new MapStoreChatbot().backupAll(hzc.getMap(PortableClassId.NsChatbot));
      }
      if (backupAll || target.equals(PortableClassId.NsDA)) {

        new MapStoreDialogAgent().backupAll(hzc.getMap(PortableClassId.NsDA));
      }
      if (backupAll || target.equals(PortableClassId.NsDAActivation)) {
        new MapStoreDialogAgentActivationInfo()
            .backupAll(hzc.getMap(PortableClassId.NsDAActivation));
      }
      if (backupAll || target.equals(PortableClassId.NsDAInstance)) {
        new MapStoreDialogAgentInstance().backupAll(hzc.getMap(PortableClassId.NsDAInstance));
      }
      if (backupAll || target.equals(PortableClassId.NsDAInstExec)) {
        new MapStoreDialogAgentInstanceExecutionInfo()
            .backupAll(hzc.getMap(PortableClassId.NsDAInstExec));
      }
      if (backupAll || target.equals(PortableClassId.NsDAManager)) {
        new MapStoreDialogAgentManager().backupAll(hzc.getMap(PortableClassId.NsDAManager));
      }
      if (backupAll || target.equals(PortableClassId.NsIntentFinderInstance)) {
        new MapStoreIntentFinderInstance()
            .backupAll(hzc.getMap(PortableClassId.NsIntentFinderInstance));
      }
      if (backupAll || target.equals(PortableClassId.NsIntentFinderPolicy)) {
        new MapStoreIntentFinderPolicy()
            .backupAll(hzc.getMap(PortableClassId.NsIntentFinderPolicy));
      }
      if (backupAll || target.equals(PortableClassId.NsSC)) {
        new MapStoreSimpleClassifier().backupAll(hzc.getMap(PortableClassId.NsSC));
      }
      if (backupAll || target.equals(PortableClassId.NsSkill)) {
        new MapStoreSkill().backupAll(hzc.getMap(PortableClassId.NsSkill));
      }
    } catch (Exception e) {
      logger.error("backup failed", e);
    } finally {
      if (hzc != null) {
        hzc.shutdown();
      }
    }
  }

  /**
   * Invoked when a message is received for the added topic. Note that topic guarantees message
   * ordering. Therefore there is only one thread invoking onMessage. The user should not keep the
   * thread busy, but preferably should dispatch it via an Executor. This will increase the
   * performance of the topic.
   *
   * @param message the message that is received for the added topic
   */
  @Override
  public void onMessage(Message<String> message) {
    String target = message.getMessageObject();
    logger.trace("Got Message {}", target);
    new Thread(() -> backupSettings(target)).start();
  }

}
