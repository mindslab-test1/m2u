package ai.maum.m2u.hazelcast;

import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.GroupConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class HazelcastClientConfig {

  private static final Logger logger = LoggerFactory.getLogger(HazelcastClientConfig.class);

  private static final String absoluteMindsPath = System.getenv("MAUM_ROOT");
  private static final String absoluteEtcPath = "etc";
  private static final String absoluteConfigFileName = "m2u.conf";

  private static volatile String memberId;
  private static List<String> addrList = new ArrayList<>();
  private static String name;
  private static String password;

  private static boolean loaded = false;


  public static synchronized String getMemberId() {
    loadConfig();
    return memberId;
  }

  public static synchronized ClientConfig newConfig() {
    loadConfig();

    ClientConfig clientConfig = new ClientConfig();
    clientConfig.setGroupConfig(new GroupConfig(name, password));
    clientConfig.getNetworkConfig().setAddresses(addrList);

    return clientConfig;
  }

  public static String getSystemConfPath(String fileName) {
    Path path = Paths.get(absoluteMindsPath, absoluteEtcPath, fileName);
    return path.toString();
  }

  public static synchronized void loadConfig() throws IllegalArgumentException {
    if (loaded) {
      return;
    }

    String id = System.getProperty("cluster.member.id");

    if (id == null || id.isEmpty()) {
      logger.error("*****************************************");
      logger.error("*   Cluster Member ID is not assigned.  *");
      logger.error("*****************************************");
      throw new IllegalArgumentException("Cluster Member ID is not assigned.");
    }
    memberId = id;

    Properties props = new Properties();
    String path = getSystemConfPath(absoluteConfigFileName);

    try {
      FileInputStream input = new FileInputStream(path);
      props.load(input);
    } catch (IOException e) {
      logger.error("loadConfig e : " , e);
      System.exit(300);
    }

    name = props.getProperty("hazelcast.group.name");
    password = props.getProperty("hazelcast.group.password");
    String port = props.getProperty("hazelcast.server.port");

    String countVal = props.getProperty("hazelcast.server.count");
    int count = Integer.parseInt(countVal);
    logger.debug("group count: {}", count);

    for (int i = 0; i < count; i++) {
      String key = String.format("hazelcast.server.ip.%d", i + 1);
      String ip = props.getProperty(key);
      if (ip != null) {
        String addr = ip.concat(":").concat(port);
        addrList.add(addr);
        logger.debug("group {}: {}", i, addr);
      }
    }

    loaded = true;
  }

}
