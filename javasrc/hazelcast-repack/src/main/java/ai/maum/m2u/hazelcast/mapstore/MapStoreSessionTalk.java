package ai.maum.m2u.hazelcast.mapstore;

import static java.lang.String.format;

import ai.maum.m2u.common.portable.SessionTalkPortable;
import com.hazelcast.core.MapStore;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
message SessionTalk {
  // 순서
  uint32 seq = 1;
  // 언어
  minds.LangCode lang = 11;
  // 음성 여부
  bool audio = 12;
  // Sample Rate
  int32 sample_rate = 13;
  // 이미지 여부
  bool image = 14;
  // 입력 텍스트
  string in = 21;
  string in_mangled = 22;
  // 출력 텍스트
  string out = 23;
  // 출력 음성
  string audio_record_file = 24;
  // 출력 메타 데이터
  map<string, string> meta = 31;
  // 출력 메타 데이터
  map<string, string> context = 32;
  // grpc 최종 status
  int32 status_code = 41;
  // 내부 처리 코드
  maum.m2u.facade.DialogResult dialog_result = 42;
  // 텍스트 분석, 이건 정말 들어갈 것은 아닌 것 같군요. 제거
  // NamedEntityAnalysis named_entity_analysis = 50;
  // 복수의 대화 에이전트 정보
  repeated TalkAgent agents = 61;
  // 최종 도메인
  string skill = 62;
  // 최종 의도
  string intent = 63;
  // 시작 시간
  google.protobuf.Timestamp start = 71;
  // 끝 시간
  google.protobuf.Timestamp end = 72;
  // 각 구간별 시간
  map<string, google.protobuf.Duration> elapsed_time = 73;
  // 각 세부 동작별 CL
  ConfidenceLevel cl = 81;
  float cl_result = 82;
  // CL 안정구간이 아닌 경우 표시
  int32 warned = 83;
}

RDB Table Schema
create table talk (
key long,
msg text,
primary key(key desc));
 */

public class MapStoreSessionTalk implements MapStore<String, SessionTalkPortable> {

  static final Logger logger = LoggerFactory.getLogger(MapStoreSessionTalk.class);

  public synchronized void store(String key, SessionTalkPortable value) {
    PreparedStatement pstmt = null;
    StringBuffer sqlStatment = new StringBuffer();
    sqlStatment.append("select key")
        .append(" from talk")
        .append(" where key = '%s'");
    String sql = format(sqlStatment.toString(), key);
    try {
      ResultSet resultSet = FileDBConnector.getConnectionTalk().createStatement().executeQuery(sql);
      if (!resultSet.next()) {
        sqlStatment = new StringBuffer();
        sqlStatment.append("insert into talk")
            .append(" (key, bin) ")
            .append(" values")
            .append(" (?, ?)");
        pstmt = FileDBConnector.getConnectionTalk().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, key);
        pstmt.setBytes(2, value.getProtobufObj().toByteArray());
        pstmt.executeUpdate();
      } else {
        sqlStatment = new StringBuffer();
        sqlStatment.append("update talk set")
            .append(" bin = ?")
            .append(" where key = ?");
        pstmt = FileDBConnector.getConnectionTalk().prepareStatement(sqlStatment.toString());
        pstmt.setBytes(1, value.getProtobufObj().toByteArray());
        pstmt.setString(2, key);
        pstmt.executeUpdate();
      }
    } catch (SQLException e1) {
      logger.error("{} Exception at store(serialize) {} {}",
          new Object[]{value.getClass().getSimpleName(),
              value.getProtobufObj().getSerializedSize(),
              value.getProtobufObj().toString()}
      );
      logger.error("store e1 : " , e1);
    } finally {
      try {
        pstmt.close();
      } catch (SQLException e1) {
        logger.error("store e1 : " , e1);
      }
    }
  }

  public synchronized void storeAll(Map<String, SessionTalkPortable> map) {
    for (Map.Entry<String, SessionTalkPortable> entry : map.entrySet()) {
      this.store(entry.getKey(), entry.getValue());
    }
  }

  public synchronized void delete(String key) {
    try {
      String sql = format("delete from talk where key = '%s'", key);
      FileDBConnector.getConnectionTalk().createStatement().executeUpdate(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void deleteAll(Collection<String> keys) {
    for (String key : keys) {
      this.delete(key);
    }
  }

  public synchronized SessionTalkPortable load(String key) {
    try {
      StringBuffer sqlStatment = new StringBuffer();
      sqlStatment.append("select key, bin")
          .append(" from talk")
          .append(" where key = '%s'");
      String sql = format(sqlStatment.toString(), key);
      ResultSet resultSet = FileDBConnector.getConnectionTalk().createStatement().executeQuery(sql);
      try {
        if (!resultSet.next()) {
          return null;
        }
        SessionTalkPortable loadedObj = new SessionTalkPortable();
        try {
          loadedObj.setProtobufObj(
              loadedObj.getProtobufObj().toBuilder().mergeFrom(resultSet.getBinaryStream(2))
                  .build()
          );
        } catch (IOException e) {
          logger.error(
              "{} Exception at load(deserialize) {}",
              this.getClass().getSimpleName(),
              resultSet.getString(1)
          );
          logger.error("load e : " , e);
        }
        return loadedObj;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized Map<String, SessionTalkPortable> loadAll(Collection<String> keys) {
    Map<String, SessionTalkPortable> result = new HashMap<String, SessionTalkPortable>();
    for (String key : keys) {
      result.put(key, load(key));
    }
    return result;
  }

  public Iterable<String> loadAllKeys() {
    try {
      ResultSet resultSet = FileDBConnector.getConnectionTalk().createStatement().executeQuery(
          format("select key from talk "));
      try {
        Collection<String> keys = new ArrayList<>();
        while (resultSet.next()) {
          keys.add(resultSet.getString(1));
        }
        return keys;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}