package ai.maum.m2u.hazelcast.mapstore;

import static java.lang.String.format;

import ai.maum.m2u.common.portable.SimpleClassifierPortable;
import com.hazelcast.core.MapStore;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
message SimpleClassifier {
  string name = 1;
  LangCode lang = 2;
  string description = 3;

  int32 skill_cnt = 4;
  int32 reg_cnt = 5;

  google.protobuf.Timestamp create_at = 6;
  google.protobuf.Timestamp modify_at = 7;

  map<string, ListRegex> skill = 8;

  message ListRegex {
    repeated string regex = 1;
  }
}

RDB Table Schema
create table simple_classifier (
key text,
msg text,
primary key(key asc));
 */

public class MapStoreSimpleClassifier implements
    MapStore<String, SimpleClassifierPortable> {

  static final Logger logger = LoggerFactory.getLogger(MapStoreSimpleClassifier.class);

  public synchronized void store(String key, SimpleClassifierPortable value) {
    PreparedStatement pstmt = null;
    StringBuffer sqlStatment = new StringBuffer();
    sqlStatment.append("select key")
        .append(" from simple_classifier")
        .append(" where key = '%s'");
    String sql = format(sqlStatment.toString(), key);
    try {
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(sql);
      if (!resultSet.next()) {
        sqlStatment = new StringBuffer();
        sqlStatment.append("insert into simple_classifier")
            .append(" (key, msg, bin) ")
            .append(" values")
            .append(" (?, ?, ?)");
        pstmt = FileDBConnector.getConnection().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, key);
        pstmt.setString(2, value.getProtobufObj().toString());
        pstmt.setBytes(3, value.getProtobufObj().toByteArray());
        pstmt.executeUpdate();
      } else {
        sqlStatment = new StringBuffer();
        sqlStatment.append("update simple_classifier set")
            .append(" msg = ?")
            .append(",bin = ?")
            .append(" where key = ?");
        pstmt = FileDBConnector.getConnection().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, value.getProtobufObj().toString());
        pstmt.setBytes(2, value.getProtobufObj().toByteArray());
        pstmt.setString(3, key);
        pstmt.executeUpdate();
      }
    } catch (SQLException e1) {
      logger.error("{} Exception at store(serialize) {} {}",
          new Object[]{value.getClass().getSimpleName(),
              value.getProtobufObj().getSerializedSize(),
              value.getProtobufObj().toString()}
      );
      logger.error("store e1 : " , e1);
    } finally {
      try {
        pstmt.close();
      } catch (SQLException e1) {
        logger.error("store e1 : " , e1);
      }
    }
  }

  public synchronized void storeAll(Map<String, SimpleClassifierPortable> map) {
    for (Map.Entry<String, SimpleClassifierPortable> entry : map.entrySet()) {
      store(entry.getKey(), entry.getValue());
    }
  }

  public synchronized void delete(String key) {
    try {
      String sql = format("delete from simple_classifier where key = '%s'", key);
      FileDBConnector.getConnection().createStatement().executeUpdate(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void deleteAll(Collection<String> keys) {
    for (String key : keys) {
      delete(key);
    }
  }

  public synchronized SimpleClassifierPortable load(String key) {
    try {
      StringBuffer sqlStatment = new StringBuffer();
      sqlStatment.append("select key, msg, bin")
          .append(" from simple_classifier")
          .append(" where key = '%s'");
      String sql = format(sqlStatment.toString(), key);
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(sql);
      try {
        if (!resultSet.next()) {
          return null;
        }
        SimpleClassifierPortable loadedObj = new SimpleClassifierPortable();
        try {
          loadedObj.setProtobufObj(
              loadedObj.getProtobufObj().toBuilder().mergeFrom(resultSet.getBinaryStream(3))
                  .build()
          );
        } catch (IOException e) {
          logger.error(
              "{} Exception at load(deserialize) {}",
              this.getClass().getSimpleName(),
              resultSet.getString(2)
          );
          logger.error("load e : " , e);
        }
        return loadedObj;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized Map<String, SimpleClassifierPortable> loadAll(
      Collection<String> keys) {
    Map<String, SimpleClassifierPortable> result = new HashMap<String, SimpleClassifierPortable>();
    for (String key : keys) {
      result.put(key, load(key));
    }
    return result;
  }

  public Iterable<String> loadAllKeys() {
    try {
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(
          format("select key from simple_classifier "));
      try {
        Collection<String> keys = new ArrayList<>();
        while (resultSet.next()) {
          keys.add(resultSet.getString(1));
        }
        return keys;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public void backupAll(Map<String, SimpleClassifierPortable> map) {
    storeAll(map);

    Iterable<String> keys = loadAllKeys();
    Collection<String> deleteKeys = new ArrayList<>();

    for (String key : keys) {
      if (!map.containsKey(key)) {
        deleteKeys.add(key);
      }
    }
    deleteAll(deleteKeys);
  }
}