package ai.maum.m2u.hazelcast.mapstore;

import static java.lang.String.format;

import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import com.hazelcast.core.MapStore;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
message DialogAgentInstance {
  string key = 1;
  string chatbot_name = 2;
  string sc_name = 3;
  string da_name = 4;
  string skill_name = 5;
  string name = 6;
  LangCode lang = 7;
  string description = 8;
  string exec_param = 9;

  int32 version = 10;
  string state = 11;

  repeated DialogAgentInstanceExecutionInfo da_instance_exec_info = 12;
}

RDB Table Schema
create table dialog_agent_instance (
key text,
msg text,
primary key(key asc));
 */

public class MapStoreDialogAgentInstance implements
    MapStore<String, DialogAgentInstancePortable> {

  static final Logger logger = LoggerFactory.getLogger(MapStoreDialogAgentInstance.class);

  public synchronized void store(String key, DialogAgentInstancePortable value) {
    PreparedStatement pstmt = null;
    StringBuffer sqlStatment = new StringBuffer();
    sqlStatment.append("select key")
        .append(" from dialog_agent_instance")
        .append(" where key = '%s'");
    String sql = format(sqlStatment.toString(), key);
    try {
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(sql);
      if (!resultSet.next()) {
        sqlStatment = new StringBuffer();
        sqlStatment.append("insert into dialog_agent_instance")
            .append(" (key, msg, bin) ")
            .append(" values")
            .append(" (?, ?, ?)");
        pstmt = FileDBConnector.getConnection().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, key);
        pstmt.setString(2, value.getProtobufObj().toString());
        pstmt.setBytes(3, value.getProtobufObj().toByteArray());
        pstmt.executeUpdate();
      } else {
        sqlStatment = new StringBuffer();
        sqlStatment.append("update dialog_agent_instance set")
            .append(" msg = ?")
            .append(",bin = ?")
            .append(" where key = ?");
        pstmt = FileDBConnector.getConnection().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, value.getProtobufObj().toString());
        pstmt.setBytes(2, value.getProtobufObj().toByteArray());
        pstmt.setString(3, key);
        pstmt.executeUpdate();
      }
    } catch (SQLException e1) {
      logger.error("{} Exception at store(serialize) {} {}",
          new Object[]{value.getClass().getSimpleName(),
              value.getProtobufObj().getSerializedSize(),
              value.getProtobufObj().toString()}
      );
      logger.error("store e1 : " , e1);
    } finally {
      try {
        pstmt.close();
      } catch (SQLException e1) {
        logger.error("store e1 : " , e1);
      }
    }
  }

  public synchronized void storeAll(Map<String, DialogAgentInstancePortable> map) {
    for (Map.Entry<String, DialogAgentInstancePortable> entry : map.entrySet()) {
      store(entry.getKey(), entry.getValue());
    }
  }

  public synchronized void delete(String key) {
    try {
      String sql = format("delete from dialog_agent_instance where key = '%s'", key);
      FileDBConnector.getConnection().createStatement().executeUpdate(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void deleteAll(Collection<String> keys) {
    for (String key : keys) {
      delete(key);
    }
  }

  public synchronized DialogAgentInstancePortable load(String key) {
    try {
      StringBuffer sqlStatment = new StringBuffer();
      sqlStatment.append("select key, msg, bin")
          .append(" from dialog_agent_instance")
          .append(" where key = '%s'");
      String sql = format(sqlStatment.toString(), key);
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(sql);
      try {
        if (!resultSet.next()) {
          return null;
        }
        DialogAgentInstancePortable loadedObj = new DialogAgentInstancePortable();
        try {
          loadedObj.setProtobufObj(
              loadedObj.getProtobufObj().toBuilder().mergeFrom(resultSet.getBinaryStream(3))
                  .build()
          );
        } catch (IOException e) {
          logger.error(
              "{} Exception at load(deserialize) {}",
              this.getClass().getSimpleName(),
              resultSet.getString(2)
          );
          logger.error("load e : " , e);
        }
        return loadedObj;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized Map<String, DialogAgentInstancePortable> loadAll(
      Collection<String> keys) {
    Map<String, DialogAgentInstancePortable> result = new HashMap<String, DialogAgentInstancePortable>();
    for (String key : keys) {
      result.put(key, load(key));
    }
    return result;
  }

  public Iterable<String> loadAllKeys() {
    try {
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(
          format("select key from dialog_agent_instance "));
      try {
        Collection<String> keys = new ArrayList<>();
        while (resultSet.next()) {
          keys.add(resultSet.getString(1));
        }
        return keys;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public void backupAll(Map<String, DialogAgentInstancePortable> map) {
    storeAll(map);

    Iterable<String> keys = loadAllKeys();
    Collection<String> deleteKeys = new ArrayList<>();

    for (String key : keys) {
      if (!map.containsKey(key)) {
        deleteKeys.add(key);
      }
    }
    deleteAll(deleteKeys);
  }
}