package ai.maum.m2u.hazelcast.mapstore;

import static java.lang.String.format;

import ai.maum.m2u.common.portable.DialogAgentManagerPortable;
import com.hazelcast.core.MapStore;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
message DialogAgentManager {
  string name = 1;
  string description = 2;
  string ip = 3;
  int32 port = 4;
  bool is_active = 5;
  bool is_default = 6;
  google.protobuf.Timestamp create_at = 7;
  google.protobuf.Timestamp modify_at = 8;
}

RDB Table Schema
create table dialog_agent_manager (
key text,
msg text,
primary key(name asc));
 */

public class MapStoreDialogAgentManager implements
    MapStore<String, DialogAgentManagerPortable> {

  static final Logger logger = LoggerFactory.getLogger(MapStoreDialogAgentManager.class);

  public synchronized void store(String key, DialogAgentManagerPortable value) {
    PreparedStatement pstmt = null;
    StringBuffer sqlStatment = new StringBuffer();
    sqlStatment.append("select key")
        .append(" from dialog_agent_manager")
        .append(" where key = '%s'");
    String sql = format(sqlStatment.toString(), key);
    try {
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(sql);
      if (!resultSet.next()) {
        sqlStatment = new StringBuffer();
        sqlStatment.append("insert into dialog_agent_manager ")
            .append(" (key, msg, bin) ")
            .append(" values")
            .append(" (?, ?, ?)");
        pstmt = FileDBConnector.getConnection().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, key);
        pstmt.setString(2, value.getProtobufObj().toString());
        pstmt.setBytes(3, value.getProtobufObj().toByteArray());
        pstmt.executeUpdate();
      } else {
        sqlStatment = new StringBuffer();
        sqlStatment.append("update dialog_agent_manager set")
            .append(" msg = ?")
            .append(",bin = ?")
            .append(" where key = ?");
        pstmt = FileDBConnector.getConnection().prepareStatement(sqlStatment.toString());
        pstmt.setString(1, value.getProtobufObj().toString());
        pstmt.setBytes(2, value.getProtobufObj().toByteArray());
        pstmt.setString(3, key);
        pstmt.executeUpdate();
      }
    } catch (SQLException e) {
      logger.error("{} Exception at store(serialize) {} {}",
          new Object[]{value.getClass().getSimpleName(),
              value.getProtobufObj().getSerializedSize(),
              value.getProtobufObj().toString()}
      );
      logger.error("store e : " , e);
    } finally {
      try {
        pstmt.close();
      } catch (SQLException e1) {
        logger.error("store e1 : " , e1);
      }
    }
  }

  public synchronized void storeAll(Map<String, DialogAgentManagerPortable> map) {
    for (Map.Entry<String, DialogAgentManagerPortable> entry : map.entrySet()) {
      store(entry.getKey(), entry.getValue());
    }
  }

  public synchronized void delete(String key) {
    try {
      String sql = format("delete from dialog_agent_manager where key = '%s'", key);
      FileDBConnector.getConnection().createStatement().executeUpdate(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void deleteAll(Collection<String> keys) {
    for (String key : keys) {
      delete(key);
    }
  }

  public synchronized DialogAgentManagerPortable load(String key) {
    try {
      StringBuffer sqlStatment = new StringBuffer();
      sqlStatment.append("select key, msg, bin")
          .append(" from dialog_agent_manager")
          .append(" where key = '%s'");
      String sql = format(sqlStatment.toString(), key);
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(sql);
      try {
        if (!resultSet.next()) {
          return null;
        }
        DialogAgentManagerPortable loadedObj = new DialogAgentManagerPortable();
        try {
          loadedObj.setProtobufObj(
              loadedObj.getProtobufObj().toBuilder().mergeFrom(resultSet.getBinaryStream(3))
                  .build()
          );
        } catch (IOException e) {
          logger.error(
              "{} Exception at load(deserialize) {}",
              this.getClass().getSimpleName(),
              resultSet.getString(2)
          );
          logger.error("load e : " , e);
        }
        return loadedObj;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized Map<String, DialogAgentManagerPortable> loadAll(
      Collection<String> keys) {
    Map<String, DialogAgentManagerPortable> result
        = new HashMap<String, DialogAgentManagerPortable>();
    for (String key : keys) {
      result.put(key, load(key));
    }
    return result;
  }

  public Iterable<String> loadAllKeys() {
    try {
      ResultSet resultSet = FileDBConnector.getConnection().createStatement().executeQuery(
          format("select key from dialog_agent_manager "));
      try {
        Collection<String> keys = new ArrayList<>();
        while (resultSet.next()) {
          keys.add(resultSet.getString(1));
        }
        return keys;
      } finally {
        resultSet.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public void backupAll(Map<String, DialogAgentManagerPortable> map) {
    storeAll(map);

    Iterable<String> keys = loadAllKeys();
    Collection<String> deleteKeys = new ArrayList<>();

    for (String key : keys) {
      if (!map.containsKey(key)) {
        deleteKeys.add(key);
      }
    }
    deleteAll(deleteKeys);
  }
}