package ai.maum.appender;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import maum.m2u.map.Authentication.SignInPayload;
import maum.m2u.map.Authentication.SignInResultPayload;
import maum.m2u.map.Authentication.SignOutPayload;
import maum.m2u.map.Authentication.SignOutResultPayload;
import maum.m2u.router.v3.Router.OpenRouteRequest;
import maum.m2u.router.v3.Router.TalkRouteRequest;
import maum.m2u.router.v3.Router.TalkRouteResponse;

/**
 * MAP에서 CallLogEncoder로 전달되는 정보는 Router연동(OPEN, TALK), 인증 정보이다
 * 연동, 인증 정보의 형태는 EVENT(요청)/DELEGATE(응답)로 구성된다
 * 해당 정보는 Arguments, MDC 를 이용하여 전달된다
 * 규격서에 명시된 Data Set을 구성하기 위한 방법의 예는 makeEventCallLog, makeDelegateCallLog 를 참고한다
 * 추가적으로 필요한 정보는 MDC 를 이용하여 추가한다
 */
public class CallLogEncoder extends LayoutWrappingEncoder<ILoggingEvent> {

  // 변수 선언
  private static final String OPEN_ROUTER = "OPEN_ROUTER";
  private static final String TALK_ROUTER = "TALK_ROUTER";
  private static final String SIGN = "SIGN";

  private OpenRouteRequest openRouteRequest = null;
  private TalkRouteRequest talkRouteRequest = null;
  private TalkRouteResponse talkRouteResponse = null;

  private SignInPayload signInPayload = null;
  private SignInResultPayload signInResultPayload = null;

  private SignOutPayload signOutPayload = null;
  private SignOutResultPayload signOutResultPayload = null;

  private String tempOperationSyncId = "";
  private String tempInterface = "";
  private String tempOperation = "";
  private String tempStreamId = "";
  private String tempExceptionCode = "";
  private String tempExceptionMsg = "";

  private Charset charset;

  /**
   * 문자열을 charset에 따라 byte[]로 변환한다.
   */
  private byte[] convertToBytes(String s) {
    if (charset == null) {
      return s.getBytes();
    } else {
      return s.getBytes(charset);
    }
  }

  /**
   * Encoder에 사용할 Character set 설정
   */
  @Override
  public void setCharset(Charset charset) {
    super.setCharset(charset);
    this.charset = getCharset();
  }

  /**
   * event로 전달된 Marker, Message, Arguments, MDC Map 등을 이용하여 로그를 조합한다.
   */
  @Override
  public byte[] encode(ILoggingEvent event) {

    // 변수 초기화
    StringBuffer buffer = new StringBuffer();
    tempOperationSyncId = "";
    boolean isOpenRouterEvent = false;
    boolean isOpenRouterDelegate = false;
    boolean isTalkRouterEvent = false;
    boolean isTalkRouterDelegate = false;
    boolean isSignEvent = false;
    boolean isSignDelegate = false;
    boolean isSignOutEvent = false;
    boolean isSignOutDelegate = false;

    tempExceptionCode = "";
    tempExceptionMsg = "";

    buffer.append("MARKER=");
    buffer.append(event.getMarker().toString());
    buffer.append(",");

    buffer.append("MESSAGE=");
    buffer.append(event.getMessage());
    buffer.append(",");

    buffer.append("ARGs=");
    buffer.append(String.valueOf(event.getArgumentArray().length));
    buffer.append('\n');

    // START Check MDC
    Map<String, String> map = event.getMDCPropertyMap();

    buffer.append("MDCs=");
    buffer.append(String.valueOf(map.size()));
    buffer.append(',');

    for (String key : map.keySet()) {
      buffer.append(key);
      buffer.append('=');
      buffer.append(map.get(key));

      // MDC로 부터 전달받은 변수 설정 (Key, Value)
      // Argument로 전달하지 못하는 값은 Kay, Value 형식의 MDC로 전달된다
      // 지정된 Key값으로 Value 값을 획득한다 (추후 필요한 데이터를 MDC를 통해 추가 가능)
      if (key.equalsIgnoreCase("OperationSyncId")) {
        tempOperationSyncId = map.get(key);
      } else if (key.equalsIgnoreCase("interface")) {
        tempInterface = map.get(key);
      } else if (key.equalsIgnoreCase("operation")) {
        tempOperation = map.get(key);
      } else if (key.equalsIgnoreCase("streamId")) {
        tempStreamId = map.get(key);
      } else if (key.equalsIgnoreCase("exceptionCode")) {
        tempExceptionCode = map.get(key);
      } else if (key.equalsIgnoreCase("exceptionMsg")) {
        tempExceptionMsg = map.get(key);
      }
      buffer.append(',');
    }

    //각 param 초기화
    openRouteRequest = null;
    talkRouteResponse = null;
    signInPayload = null;

    // 전달받은 Argument의 객수를 출력
    System.out.println("#@ event.getArgumentArray().length :" + event.getArgumentArray().length);

    for (Object object : event.getArgumentArray()) {

      System.out.println("#@ object.getClass().getTypeName() :" + object.getClass().getTypeName());

      // 전달된 Object에 맞는 Class 객체를 맵핑한다
      // 현재 Object가 어떤 Class인지 찾고(Type Name이 일치하는지 판단), Data Set을 구성하기 위해 객체를 주입한다
      if (object.getClass().getTypeName().equals(OpenRouteRequest.class.getTypeName())) {
        System.out.println("#@ START Make Request OpenRouteRequest");
        openRouteRequest = (OpenRouteRequest) object;
        isOpenRouterEvent = true;
      } else if (object.getClass().getTypeName().equals(TalkRouteRequest.class.getTypeName())) {
        System.out.println("#@ START Make Request TalkRouteRequest");
        talkRouteRequest = (TalkRouteRequest) object;
        isTalkRouterEvent = true;
      } else if (object.getClass().getTypeName().equals(TalkRouteResponse.class.getTypeName())) {
        System.out.println("#@ START Make Response TalkRouteResponse");
        talkRouteResponse = (TalkRouteResponse) object;
        isTalkRouterDelegate = true;
        if (isOpenRouterEvent == true) {
          // Open인 경우 param은 OpenRouteRequest지만 결과는 TalkRouterResponse
          isTalkRouterDelegate = false;
          // Open인 경우엔 request param이 openRouterRequest 이므로 아래와 같이 셋팅한다
          isOpenRouterDelegate = true;
        }
      } else if (object.getClass().getTypeName().equals(SignInPayload.class.getTypeName())) {
        System.out.println("#@ START Make Response SignInPayload");
        signInPayload = (SignInPayload) object;
        isSignEvent = true;
      } else if (object.getClass().getTypeName().equals(SignInResultPayload.class.getTypeName())) {
        System.out.println("#@ START Make Response SignInResultPayload");
        signInResultPayload = (SignInResultPayload) object;
        isSignDelegate = true;
      } else if (object.getClass().getTypeName().equals(SignOutPayload.class.getTypeName())) {
        System.out.println("#@ START Make Response SignOutPayload");
        signOutPayload = (SignOutPayload) object;
        isSignOutEvent = true;
      } else if (object.getClass().getTypeName().equals(SignOutResultPayload.class.getTypeName())) {
        System.out.println("#@ START Make Response SignOutResultPayload");
        signOutResultPayload = (SignOutResultPayload) object;
        isSignOutDelegate = true;
      } else {
        System.out.println("#@ There is no corresponding class Type.");
      }
      buffer.append('|');
    }

    buffer.append('\n');

    // Call Log에 남길 메시지 셋팅을 수행한다
    if (isOpenRouterEvent == true) {
      buffer.append(this.makeEventCallLog(OPEN_ROUTER));
      buffer.append('\n');
    }
    if (isOpenRouterDelegate == true) {
      buffer.append(this.makeDelegateCallLog(OPEN_ROUTER));
      buffer.append('\n');
    }
    if (isTalkRouterEvent == true) {
      buffer.append(this.makeEventCallLog(TALK_ROUTER));
      buffer.append('\n');
    }
    if (isTalkRouterDelegate == true) {
      buffer.append(this.makeDelegateCallLog(TALK_ROUTER));
      buffer.append('\n');
    }
    if (isSignEvent == true) {
      buffer.append(this.makeEventCallLog(SIGN));
      buffer.append('\n');
    }
    if (isSignDelegate == true) {
      buffer.append(this.makeDelegateCallLog(SIGN));
      buffer.append('\n');
    }
    if (isSignOutEvent == true) {
      buffer.append(this.makeEventCallLog(SIGN));
      buffer.append('\n');
    }
    if (isSignOutDelegate == true) {
      buffer.append(this.makeDelegateCallLog(SIGN));
      buffer.append('\n');
    }

    return convertToBytes(buffer.toString());
  }

  /**
   * Event CallLog 생성 함수 Event Stream에 대한 Call Log Data Set을 담당한다 규격에 맞춰 Data Set을 변경할 수 있다
   */
  String makeEventCallLog(String oprationType) {
    System.out.println("#@ START makeEventCallLog :" + oprationType);

    // 날짜 생성
    Date nowDate = new Date();
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");

    // 랜덤 생성 (SEQ_ID = YYYYMMDDHHmmSSsss+{랜덤4자리})
    Random rnd = new Random();
    int rndInt = rnd.nextInt(9999);

    // reqCallLog 변수 선언
    String reqCallLog = "";

    // OpenRouter/TalkRouter 분기처리 (Device Info)
    String tempDevInfo = "";
    if (oprationType.equalsIgnoreCase(OPEN_ROUTER)) {
      openRouteRequest.getSystemContext().getDevice().getType();
    } else if (oprationType.equalsIgnoreCase(TALK_ROUTER)) {
      talkRouteRequest.getSystemContext().getDevice().getType();
    }

    // OpenRouter/TalkRouter 분기처리 (User Id)
    String tempUserId = "";
    if (oprationType.equalsIgnoreCase(OPEN_ROUTER)) {
      openRouteRequest.getSystemContext().getUser().getUserId();
    } else if (oprationType.equalsIgnoreCase(TALK_ROUTER)) {
      talkRouteRequest.getSystemContext().getUser().getUserId();
    }

    // OpenRouter/TalkRouter 분기처리 (AccessToken)
    String tempAccessToken = "";
    if (oprationType.equalsIgnoreCase(OPEN_ROUTER)) {
      openRouteRequest.getSystemContext().getUser().getAccessToken();
    } else if (oprationType.equalsIgnoreCase(TALK_ROUTER)) {
      talkRouteRequest.getSystemContext().getUser().getAccessToken();
    }

    // reqCallLog의 DataSet은 규격에 맞춘다
    reqCallLog += "SEQ_ID=" + sdf1.format(nowDate) + rndInt + "|";
    reqCallLog += "LOG_TIME=" + sdf2.format(nowDate) + "|";
    reqCallLog += "LOG_TYPE=" + "EVENT" + "|";
    reqCallLog += "SID=" + "|";
    reqCallLog += "RESULT_CODE=" + "|";
    reqCallLog += "REQ_TIME=" + sdf2.format(nowDate) + "|";
    reqCallLog += "RSP_TIME=" + "|";
    reqCallLog += "CLIENT_IP=" + "|";
    reqCallLog += "DEV_INFO=" + tempDevInfo + "|";
    reqCallLog += "OS_INFO=" + "|";
    reqCallLog += "NW_INFO=" + "|";
    reqCallLog += "SVC_NAME=" + "|";
    reqCallLog += "CARRIER_TYPE=" + "|";
    reqCallLog += "TR_ID=" + tempOperationSyncId + "|";
    reqCallLog += "MSG_ID=" + "|";
    reqCallLog += "FROM_SVC_NAME=" + "|";
    reqCallLog += "TO_SVC_NAME=" + "|";
    reqCallLog += "SVC_TYPE=" + "|";
    reqCallLog += "DEV_TYPE=" + "|";
    reqCallLog += "DEVICE_TOKEN=" + "|";
    reqCallLog += "SESSION_ID=" + "|";
    reqCallLog += "USER_ID=" + tempUserId + "|";
    reqCallLog += "ACCESS_TOKEN=" + tempAccessToken + "|";
    reqCallLog += "EVENT_DIRECTIVE=" + tempOperation + "|";
    reqCallLog += "STREAM_ID=" + tempStreamId + "|";
    reqCallLog += "EXCEPTION_ID=" + "|";
    reqCallLog += "STATUS_CODE=" + "|";
    reqCallLog += "EX_CODE=" + tempExceptionCode + "|";
    reqCallLog += "EX_MESSAGE=" + tempExceptionMsg + "|";
    reqCallLog += "STT_TIME=" + "|";
    reqCallLog += "STT_RESULT=" + "|";
    reqCallLog += "SVC_TIME=" + "|";
    reqCallLog += "OUTPUT_SPEECH_TYPE=" + "|";
    reqCallLog += "SHOULD_END_SESSION=" + "|";
    reqCallLog += "SPEECH_ERROR_TEXT=" + "|";
    reqCallLog += "SPEECH_ERROR_REASON=";

    System.out.println("#@ reqCallLog :" + reqCallLog);

    return reqCallLog;
  }

  /**
   * Delegate CallLog 생성 함수 Delegate Stream에 대한 Call Log Data Set을 담당한다 규격에 맞춰 Data Set을 변경할 수 있다
   */
  String makeDelegateCallLog(String oprationType) {
    System.out.println("#@ START makeDelegateCallLog :" + oprationType);
    // 날짜 생성
    Date nowDate = new Date();
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");

    // 랜덤 생성 (SEQ_ID = YYYYMMDDHHmmSSsss+{랜덤4자리})
    Random rnd = new Random();
    int rndInt = rnd.nextInt(9999);

    // resCallLog 변수 선언
    String resCallLog = "";

    // OpenRouter/TalkRouter 분기처리 (Device Info)
    String tempDevInfo = "";
    if (oprationType.equalsIgnoreCase(OPEN_ROUTER)) {
      openRouteRequest.getSystemContext().getDevice().getType();
    } else if (oprationType.equalsIgnoreCase(TALK_ROUTER)) {
      talkRouteRequest.getSystemContext().getDevice().getType();
    }

    // OpenRouter/TalkRouter 분기처리 (User Id)
    String tempUserId = "";
    if (oprationType.equalsIgnoreCase(OPEN_ROUTER)) {
      openRouteRequest.getSystemContext().getUser().getUserId();
    } else if (oprationType.equalsIgnoreCase(TALK_ROUTER)) {
      talkRouteRequest.getSystemContext().getUser().getUserId();
    }

    // OpenRouter/TalkRouter 분기처리 (AccessToken)
    String tempAccessToken = "";
    if (oprationType.equalsIgnoreCase(OPEN_ROUTER)) {
      openRouteRequest.getSystemContext().getUser().getAccessToken();
    } else if (oprationType.equalsIgnoreCase(TALK_ROUTER)) {
      talkRouteRequest.getSystemContext().getUser().getAccessToken();
    }

    // resCallLog DataSet은 규격에 맞춘다
    resCallLog += "SEQ_ID=" + sdf1.format(nowDate) + rndInt + "|";
    resCallLog += "LOG_TIME=" + sdf2.format(nowDate) + "|";
    resCallLog += "LOG_TYPE=" + "DIRECTIVE" + "|";
    resCallLog += "SID=" + "|";
    resCallLog += "RESULT_CODE=" + "|";
    resCallLog += "REQ_TIME=" + "|";
    resCallLog += "RSP_TIME=" + sdf2.format(nowDate) + "|";
    resCallLog += "CLIENT_IP=" + "|";
    resCallLog += "DEV_INFO=" + tempDevInfo + "|";
    resCallLog += "OS_INFO=" + "|";
    resCallLog += "NW_INFO=" + "|";
    resCallLog += "SVC_NAME=" + "|";
    resCallLog += "CARRIER_TYPE=" + "|";
    resCallLog += "TR_ID=" + tempOperationSyncId + "|";
    resCallLog += "MSG_ID=" + "|";
    resCallLog += "FROM_SVC_NAME=" + "|";
    resCallLog += "TO_SVC_NAME=" + "|";
    resCallLog += "SVC_TYPE=" + "|";
    resCallLog += "DEV_TYPE=" + "|";
    resCallLog += "DEVICE_TOKEN=" + "|";
    resCallLog += "SESSION_ID=" + "|";
    resCallLog += "USER_ID=" + tempUserId + "|";
    resCallLog += "ACCESS_TOKEN=" + tempAccessToken + "|";
    resCallLog += "EVENT_DIRECTIVE=" + tempOperation + "|";
    resCallLog += "STREAM_ID=" + tempStreamId + "|";
    resCallLog += "EXCEPTION_ID=" + "|";
    resCallLog += "STATUS_CODE=" + "|";
    resCallLog += "EX_CODE=" + tempExceptionCode + "|";
    resCallLog += "EX_MESSAGE=" + tempExceptionMsg + "|";
    resCallLog += "STT_TIME=" + "|";
    resCallLog += "STT_RESULT=" + "|";
    resCallLog += "SVC_TIME=" + "|";
    resCallLog += "OUTPUT_SPEECH_TYPE=" + "|";
    resCallLog += "SHOULD_END_SESSION=" + "|";
    resCallLog += "SPEECH_ERROR_TEXT=" + "|";
    resCallLog += "SPEECH_ERROR_REASON=";

    System.out.println("#@ resCallLog :" + resCallLog);

    return resCallLog;
  }
}