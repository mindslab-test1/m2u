package ai.maum.caller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;


public class Caller {

  private static final Logger logger = LoggerFactory.getLogger(Caller.class);

  public static void main(String[] args) {
    Marker sampleMarker = MarkerFactory.getMarker("CallLog");

    logger.error("{} {}", sampleMarker, sampleMarker.contains("CallLog"));

    boolean loop = true;
    int count = 0;

    while (loop) {
      ++count;
      if (count > 10 && count < 60) {
        MDC.put("count", String.valueOf(count));
        MDC.put("count2", String.valueOf(count));
        logger.error(sampleMarker, "hi:{} {}", loop, count);
        logger.info("Log without Marker.");
      } else {
        logger.info("Testing zero-sized log file. {}", count);
      }
      try {
        Thread.sleep(3333);
      } catch (Exception e) {
      }
    }
  }
}
