package ai.maum.m2u.common.portable;

import static ai.maum.m2u.common.portable.PortableClassId.DIALOG_AGENT;
import static ai.maum.m2u.common.portable.PortableClassId.DIALOG_AGENT_INSTANCE;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableFactory;

public class DialogAgentInstanceSvcPortableFactory implements PortableFactory {

  @Override
  public Portable create(int classId) {
    switch (classId) {
      case DIALOG_AGENT_INSTANCE:
        return new DialogAgentInstancePortable();
      case DIALOG_AGENT:
        return new DialogAgentPortable();
      default:
        throw new IllegalArgumentException(classId + " unsupported classId");
    }
  }
}
