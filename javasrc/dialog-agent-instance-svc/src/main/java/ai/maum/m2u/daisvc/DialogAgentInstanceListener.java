package ai.maum.m2u.daisvc;

import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Dialog Agent Instance Listener
 *
 * entryAdded, entryRemoved, entryUpdated를 처리한다.
 */
public class DialogAgentInstanceListener implements
    EntryAddedListener<String, DialogAgentInstancePortable>,
    EntryRemovedListener<String, DialogAgentInstancePortable>,
    EntryUpdatedListener<String, DialogAgentInstancePortable> {

  private static final Logger logger = LoggerFactory.getLogger(DialogAgentInstanceListener.class);

  private final Object lockEvent = new Object();
  private int eventCount = 0;

  /**
   * 이벤트를 처리할 지 판단한다.
   *
   * 중첩된 이벤트를 제외하고 마지막 이벤트로 모두 처리하도록 한다.
   *
   * @return true: 이벤트를 처리한다. false: 이벤트가 여러개 이거나 없으면 처리하지 않는다.
   */
  public boolean shouldProcessEvent() {
    synchronized (lockEvent) {
      logger.trace("dai shouldProcessEvent eventCount {}", eventCount);
      return eventCount == 1;
    }
  }

  /**
   * 중첩된 이벤트가 있으면 한 개를 제거한다.
   *
   * @return true: 이벤트 한 개를 제거한 후 남은 것이 있으면 sleep 하지 않는다. false: 이벤트가 없으면 sleep 한다.
   */
  public boolean fetchEvent() {
    synchronized (lockEvent) {
      if (eventCount > 1) {
        eventCount--;
      }
      logger.trace("dai fetchEvent eventCount {}", eventCount);
      return eventCount > 0;
    }
  }

  /**
   * 이벤트를 처리할 지 값을 설정한다.
   *
   * @param value true: 이벤트 카운트가 1 보다 작으면 1로 설정한다. false: 이벤트 카운트를 1 감소 시킨다.
   */
  public void setHasEvent(boolean value) {
    synchronized (lockEvent) {
      if (value && eventCount < 1) {
        eventCount = 1;
      } else if (!value && eventCount > 0) {
        eventCount--;
      }
      logger.trace("dai setHasEvent eventCount {}", eventCount);
    }
  }

  /**
   * Hzc에 dai entry가 추가될 경우 호출됩니다.
   *
   * @param event 변경된 event
   */
  @Override
  public void entryAdded(
      EntryEvent<String, DialogAgentInstancePortable> event) {
    logger.info("dai entryAdded = {}/{}", event.getName(), event.getKey());
    logger.trace("dai entryAdded = {}", event.toString());
    //Dailog Agent Instance가 추가되면 해당 entryAdded Lisener가 호출됩니다
    //추가된 Dailog Agent Instance의 정보가 넘어옵니다
    synchronized (lockEvent) {
      eventCount++;
      logger.trace("dai entryAdded eventCount {}", eventCount);
    }
  }

  /**
   * Hzc에 dai entry가 삭제될 경우 호출됩니다
   *
   * @param event 삭제된 event
   */

  @Override
  public void entryRemoved(
      EntryEvent<String, DialogAgentInstancePortable> event) {
    logger.debug("dai entryRemoved = {}", event.toString());
    synchronized (lockEvent) {
      eventCount++;
      logger.trace("dai entryRemoved eventCount {}", eventCount);
    }
  }

  /**
   * Hzc의 entry가 변경될 경우 호출됩니다
   *
   * @param event 변경된 event
   */

  @Override
  public void entryUpdated(
      EntryEvent<String, DialogAgentInstancePortable> event) {
    logger.debug("dai entryUpdated = {}", event.toString());
    synchronized (lockEvent) {
      eventCount++;
      logger.trace("dai entryUpdated eventCount {}", eventCount);
    }
  }

}
