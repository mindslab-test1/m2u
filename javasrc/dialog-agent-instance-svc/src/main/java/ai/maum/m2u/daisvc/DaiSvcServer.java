package ai.maum.m2u.daisvc;

import ai.maum.m2u.config.ConfigManager;
import ai.maum.m2u.hzc.HazelcastHelper;
import ai.maum.rpc.ResultStatus;
import com.hazelcast.client.HazelcastClientNotActiveException;
import java.text.SimpleDateFormat;
import java.util.Date;
import maum.rpc.Status;
import maum.rpc.Status.ProcessOfM2u;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DaiSvc Server 초기화 및 실행
 */
public class DaiSvcServer extends Thread {

  static final Logger logger = LoggerFactory.getLogger(DaiSvcServer.class);

  final static int NEXT_EVENT = 60 * 1000;

  static {
    ResultStatus.setModuleAndProcess(Status.Module.M2U, ProcessOfM2u.M2U_DAM.getNumber());
  }

  private ConfigManager config = new ConfigManager();
  private DialogAgentInstanceSvctl daiSvctl = new DialogAgentInstanceSvctl();


  /**
   * DaiSvc Server를 실행한다.
   */
  @Override
  public void run() {
    logger.info("#@ DaiSvcServer run");

    //최초 hasEvent는 true로 설정합니다
    DialogAgentInstanceListener daiEventListener = new DialogAgentInstanceListener();
    daiEventListener.setHasEvent(true);

    //hzc로 부터 응답을 받기 위한 listener를 등록합니다
    HazelcastHelper.AddListener(daiEventListener);

    long lastCall = System.currentTimeMillis();

    // DaiSvcServer는 15초마다 reload할 항목이 있는지 조회를 합니다
    while (true) {
      long curr = System.currentTimeMillis();

      // 매 1분 단위로 무조건 다시 호출하도록 한다.
      // 시스템 설정이 오류로 인해서 잘못된 경우에는 무한 재시도가 될 수도 있다.
      // 정상적인 경우라 하더라도 오류가 있는지 확인을 해야 한다.
      boolean timeExceed = (curr - lastCall) > NEXT_EVENT;
      try {
        final boolean hasEvent = daiEventListener.shouldProcessEvent();
        if (hasEvent || timeExceed) {
          logger.info("try to reload hasEvent {} or timeExceeded {}", hasEvent, timeExceed);
          // hasEvent가 있다면 reload를 통해 서비스를 시작합니다
          daiSvctl.reload(HazelcastHelper.getDaiInstances(), hasEvent);
          lastCall = System.currentTimeMillis();
          daiEventListener.setHasEvent(false);

          if (daiSvctl.getRetry()) {
            daiSvctl.setRetry(false);
            daiEventListener.setHasEvent(true);
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            logger.warn("#@ DaiSvcServer next retry![{}] after 10 SEC", timeStamp);
            Thread.sleep(3000);
          }
        } else {
          if (!daiEventListener.fetchEvent()) {
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            logger.trace("#@ DaiSvcServer sleep [{}]", timeStamp);
            Thread.sleep(3000);
          }
        }
      } catch (InterruptedException e) {
        logger.error("InterruptedException {} => {}", e.getMessage(), e);
      } catch (HazelcastClientNotActiveException hzcCliExce) {
        logger.error("{} => ", hzcCliExce.getMessage(), hzcCliExce);
        // 해당 에러가 발생하면 daisvc를 svctl에서 stop 시킵니다
        // 이후 daisvc는 재시작 되면 정상적으로 동작합니다
        logger.debug("#@ stopProcess m2u-daisvc");
        daiSvctl.connectSupervisord().stopProcess("m2u-daisvc", true);
      } catch (Exception e) {
        logger.error("{} => ", e.getMessage(), e);
      }
    }
  }

  /**
   * DaiSvcServer를 초기화 합니다
   */
  public void init() {
    config.load();

    logger.info("#@ hzcHelper.connectDaiHzc");
    HazelcastHelper.connect(config.getProperties());
  }

  public static void main(String[] args) {
    logger.info("#@ DaiSvcServer start...");
    DaiSvcServer server = new DaiSvcServer();
    server.init();
    server.start();

    try {
      server.join();
    } catch (InterruptedException e) {
      logger.error("{} => {}", e.getMessage(), e);
    }
  }
}
