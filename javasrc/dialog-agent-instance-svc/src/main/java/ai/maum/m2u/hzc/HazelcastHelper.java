package ai.maum.m2u.hzc;

import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceSvcPortableFactory;
import ai.maum.m2u.common.portable.DialogAgentPortable;
import ai.maum.m2u.common.portable.PortableClassId;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.map.listener.MapListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Hazelcast 연동을 위한 Helper 클래스
 */
public class HazelcastHelper {

  static final Logger logger = LoggerFactory.getLogger(HazelcastHelper.class);

  public static final String HzcAuthPolicy = "auth-policy";
  public static final String HzcChatbotDetail = "chatbot-detail";
  public static final String HzcDAManager = "dialog-agent-manager";
  public static final String HzcDA = "dialog-agent";
  public static final String HzcDAActivation = "dialog-agent-activation";
  public static final String HzcDAInstance = "dialog-agent-instance";
  public static final String HzcDAInstExec = "dialog-agent-instance-exec";
  public static final String HzcIntentFinderInstExec = "intent-finder-instance-exec";
  public static final String HzcSC = "simple-classifier";
  public static final String HzcChatbot = "chatbot";
  public static final String HzcIntentFinderInstance = "intent-finder-instance";
  public static final String HzcIntentFinderPolicy = "intent-finder-policy";

  private static HazelcastInstance daiHazelcastInstance = null;
  private static HazelcastInstance daHazelcastInstance = null;


  /**
   * HazelcastInstance를 리턴한다.
   *
   * @return HazelcastInstance의 인스턴스
   */
  public static HazelcastInstance getDaiHzcInstance() {
    return daiHazelcastInstance;
  }

  /**
   * HazelcastInstance를 리턴한다.
   *
   * @return HazelcastInstance의 인스턴스
   */
  public static HazelcastInstance getDaHzcInstance() {
    return daHazelcastInstance;
  }


  /**
   * hazelcast server에 연결합니다
   *
   * @param properties 설정 파일의 Properties
   * @return 성공여부
   */
  public static boolean connect(Properties properties) {
    logger.debug("#@ HazelcastHelper connect properties");
    List<String> addrList = new ArrayList<>();

    String count = properties.getProperty("hazelcast.server.count");
    String port = properties.getProperty("hazelcast.server.port");

    for (int i = 1; i < (Integer.parseInt(count) + 1); i++) {
      addrList.add(properties.getProperty("hazelcast.server.ip." + i) + ':' + port);
    }

    ClientConfig clientConfig = new ClientConfig();
    clientConfig.setGroupConfig(
        new GroupConfig(properties.getProperty("hazelcast.group.name"),
            properties.getProperty("hazelcast.group.password")));
    clientConfig.getNetworkConfig().setAddresses(addrList);

    clientConfig.getSerializationConfig()
        .addPortableFactory(PortableClassId.FACTORY_ID,
            new DialogAgentInstanceSvcPortableFactory());

    if (daiHazelcastInstance != null) {
      daiHazelcastInstance.shutdown();
    }
    daiHazelcastInstance = HazelcastClient.newHazelcastClient(clientConfig);
    if (daHazelcastInstance != null) {
      daHazelcastInstance.shutdown();
    }
    daHazelcastInstance = HazelcastClient.newHazelcastClient(clientConfig);

    return true;
  }

  /**
   * DialogAgentInstance의 IMAP을 가져옵니다
   *
   * @return DialogAgentInstancePortable의 IMAP
   */
  public static IMap<String, DialogAgentInstancePortable> getDaiInstances() {
    return getDaiHzcInstance().getMap(HazelcastHelper.HzcDAInstance);
  }

  /**
   * DialogAgentInstance의 IMAP을 가져옵니다
   *
   * @return DialogAgentInstancePortable의 IMAP
   */
  public static IMap<String, DialogAgentPortable> getDaInstances() {
    return getDaHzcInstance().getMap(HazelcastHelper.HzcDA);
  }

  /**
   * Map이벤트에 대한 Listener를 등록합니다
   *
   * @param listener 이벤트를 받을 listener
   */
  public static void AddListener(MapListener listener) {
    // Hzc로 부터 DAInstance가 추가되었을때 응답을 받기위해서 listener를 등록합니다
    IMap<String, DialogAgentInstancePortable> daiMap = getDaiHzcInstance()
        .getMap(HazelcastHelper.HzcDAInstance);
    daiMap.addEntryListener(listener, true);
  }
}
