package ai.maum.m2u.daisvc;

import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import ai.maum.m2u.common.portable.DialogAgentPortable;
import ai.maum.m2u.config.ConfigManager;
import ai.maum.m2u.config.PropertyManager;
import ai.maum.m2u.hzc.HazelcastHelper;
import ai.maum.m2u.svd.SupervisordXmlRpc;
import com.google.protobuf.ProtocolStringList;
import com.hazelcast.core.IMap;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import maum.m2u.console.Da.DialogAgent;
import maum.m2u.console.DaInstance.DialogAgentInstance;
import maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IntentFinder관련 Supervisord control 클래스
 */
public class DialogAgentInstanceSvctl {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentInstanceSvctl.class);

  private boolean retry = false;

  /**
   * 내부 IP를 먼저 구하고 일치된 IP를 처리하도록 한다.
   */
  public DialogAgentInstanceSvctl() {
  }

  /**
   * Supervisord에 연결한다.
   *
   * @return XmlRpc 객체를 리턴한다. 실패할경우 null
   */
  protected SupervisordXmlRpc connectSupervisord() {
    SupervisordXmlRpc st = new SupervisordXmlRpc("", "127.0.0.1", 9799, "maum",
        "msl1234~");
    return st;
  }

  /**
   * supervisord에  reloadconfig 수행한다.
   *
   * @param added config에 추가된 dai list
   * @param changed config에 변경된 dai list
   * @param removed config에  삭제된 dai list
   * @return 성공여부
   */
  protected boolean reloadConfig(List<String> added, List<String> changed, List<String> removed) {
    SupervisordXmlRpc st = connectSupervisord();
    Object[] result = st.reloadConfig();

    Object[] subObj = (Object[]) result[0];
    for (int i = 0; i < subObj.length; i++) {
      Object[] names = (Object[]) subObj[i];

      for (Object name1 : names) {
        String name = (String) name1;
        switch (i) {
          case 0:
            logger.debug("#@ added {}", name);
            added.add(name);
            break;
          case 1:
            logger.debug("#@ changed {}", name);
            changed.add(name);
            break;
          case 2:
            logger.debug("#@ removed {}", name);
            removed.add(name);
            break;
        }
      }
    }

    return true;
  }

  private static String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  @SuppressWarnings("unchecked")
  private void makeM2uDaiConf(DialogAgentInstance daInstance, String damName, String key,
      StringWriter writer, DialogAgentInstanceExecutionInfo daiExInfo) {
    logger.debug("#@ START makeM2uDaiConf key [{}] daiExInfo [{}]", key, daiExInfo);

    // daInstance와 daiExInfo의 상태를 확인합니다
    // daInstance의 name에 공백이 있으면 dai conf 파일에 저장하지 않습니다
    if (daInstance.getName().contains(" ")) {
      logger.error("#@ DAI name requires NO space. DROP !!!", daInstance.getName());
      return;
    }
    // daInstance의 chatbotName에 공백이 있으면 dai conf 파일에 저장하지 않습니다
    if (daInstance.getChatbotName().contains(" ")) {
      logger.error("#@ chatbot name requires NO space. DROP !!!", daInstance.getChatbotName());
      return;
    }
    // daiExInfo 의 Active 값이 true가 아니면 dai conf 파일에 저장하지 않습니다
    if (daiExInfo.getActive() != true) {
      logger.info("#@ This daiExInfo is not active {}, So pass make dai conf"
          , daiExInfo.getDaiId());
      return;
    }
    // daiExInfo 의 KeyList 값이 1개 이상이 아니면 dai conf 파일에 저장하지 않습니다
    if (daiExInfo.getKeyList().size() < 1) {
      logger.info("#@ There is no keyList {}, So pass make dai conf", daiExInfo.getDaiId());
      return;
    }

    IMap<String, DialogAgentPortable> daPortable = HazelcastHelper.getDaInstances();
    logger.debug("#@ daPortable [{}]", daPortable);
    logger.debug("#@ daPortable.size [{}]", daPortable.size());
    logger.debug("#@ daPortable.keySet [{}]", daPortable.keySet());

    String daName = daInstance.getDaName();
    logger.debug("#@ daName [{}]", daName);
    // DialogAgent에서 Version, DaExecutable 정보를 조회합니다

    // DAI 정보를 가져왔지만, DA 정보를 가져오지 못했다면 이는 HZC의 정보 불일치 문제입니다.
    // ADMIN UI가 문제가 없다면, HZC가 재기동하는 동안에 발생할 개연성이 높습니다.
    DialogAgentPortable tempDaPort = daPortable.get(daName);
    if (tempDaPort == null) {
      logger.error("#@ cannot get da info {}, SO RETRY NEXT!", daName);
      this.retry = true;

      return;
    }

    DialogAgent daMap = tempDaPort.getProtobufObj();
    logger.trace("#@ daMap [{}]", daMap);

    // $MAUM_ROOT path를 갖고 옵니다
    String maumRoot = ConfigManager.getSystemConfPath("");
    logger.debug("#@ setConfig path [{}]", maumRoot);
    String logsDir = PropertyManager.getString("logs.dir");
    logger.debug("#@ logs Dir [{}]", logsDir);

    // 해당 DialogAgentInstance의 daiExInfo에 key가 여러개인 경우
    // 파일 생성시 [program:dai-DAI이름-1], [program:dai-DAI이름-2] 와 같은 방법으로 생성됩니다
    int programNum = 1;
    for (String tempDaiExInfoKey : daiExInfo.getKeyList()) {
      String tempChatbotName = daInstance.getChatbotName();
      String tempDaiName = daInstance.getName();
      logger.info("[STEP - 1] chatbotName [{}]", tempChatbotName);

      // skill은 등록한 만큼 지정해 줍니다 (예:-s skill1 skill2 skill3)
      ProtocolStringList tempSkillList = daInstance.getSkillNamesList();
      StringBuilder sbSkill = new StringBuilder();
      boolean isSetSkillFirst = true;
      for (String tempSkill : tempSkillList) {
        logger.trace("#@ tempSkill [{}]", tempSkill);
        if (isSetSkillFirst == true) {
          sbSkill.append(tempSkill);
        } else {
          sbSkill.append(" ");
          sbSkill.append(tempSkill);
        }
        isSetSkillFirst = false;
      }
      logger.info("[STEP - 2] daiMap.key [{}], tempSkill [{}]", key, sbSkill);

      String tempDaSpec = daMap.getDaProdSpec().name();
      logger.debug("#@ [STEP - 3] daVersion [{}]", tempDaSpec);
      String daiId = daInstance.getDaiId();
      logger.debug("#@ [STEP - 4] daiId [{}]", daiId);
      // 포트정보는 python에서 지정해 줍니다 (m2u.conf에 max port, min port 값이 설정되어 있습니다)
      logger.debug("#@ [STEP - 5] key [{}]", tempDaiExInfoKey);

      JSONObject tempObject = new JSONObject();
      tempObject.putAll(daInstance.getParamsMap());
      logger.debug("#@ [STEP - 6] param [{}]", tempObject);
      logger.debug("#@ [STEP - 7] damName [{}]", damName);

      String fullCommand = "";
      // 실행 파일명(jar, python)
      String daExecutable = daMap.getDaExecutable();
      logger.debug("#@ [STEP - 8] daExecutable [{}]", daExecutable);

      String tempLang = daInstance.getLang().name();
      logger.debug("#@ [STEP - 9] tempLang [{}]", tempLang);

      // da가 python인 경우
      switch (daMap.getRuntimeEnv()) {
        case RE_NATIVE: {
          fullCommand = daExecutable;
          break;
        }
        case RE_PYTHON: {
          // m2u.conf에 python 경로가 있는지 확인합니다.
          String tempPython = PropertyManager.getString("dam-svcd.python.path");

          if (tempPython == null) {
            tempPython = "/usr/bin/python";
          }
          logger.debug("#@ [STEP - 10] pythonPath [{}]", tempPython);
          fullCommand = String.format("%s %s/da/%s", tempPython, maumRoot, daExecutable);
          break;
        }
        case RE_JAVA: {
          // m2u.conf에 java 경로가 있는지 확인합니다.
          String tempJava = PropertyManager.getString("dam-svcd.java.path");

          if (tempJava == null) {
            // 없다면 /usr/bin/java 설정합니다
            tempJava = "/usr/bin/java";
          }
          logger.debug("#@ [STEP - 10] javaPath [{}]", tempJava);
          // ExtraCommand 유무를 확인합니다
          String tempExtraCommand = daMap.getExtraCommand();
          logger.debug("#@ [STEP - 11] extraCommand [{}]", tempExtraCommand);
          if (!tempExtraCommand.isEmpty()) {
            // ExtraComman에 '\n', '\r', '\f', '\t'가 있는 경우 ' '로 변경합니다
            fullCommand = String.format("%s -cp %s/da/%s %s",
                tempJava, maumRoot, daExecutable,
                tempExtraCommand.replaceAll("\n|\r|\f|\t", " "));
          } else {
            fullCommand = String.format("%s -jar %s/da/%s",
                tempJava, maumRoot, daExecutable);
          }
          break;
        }
        default: {
          logger.debug("not supported format. fail to makeM2uDaiConf");
          fullCommand = "UNKONWN TYPE";
          return;
        }
      }

      String fmt = String.join(System.lineSeparator(),
          "[program:dai-%s-%s-%d]",
          "command = %s/bin/m2u-dareg",
          "   -c %s", // chatbot
          "   -s %s", // skills
          "   -la %s", // lang
          "   -v %s", // version
          "   -i %s", // dai
          "   -k %s", // key
          "   -pa '%s'", // param object
          "   -d %s", // dam name
          "   '%s'", // full cmd
          "directory = %s/da",  // dir
          "autostart = true",
          "autorestart = true",
          "stopasgroup = true",
          "stdout_logfile = %s/supervisor/dai-%s-%s-%d.out",
          "stderr_logfile = %s/supervisor/dai-%s-%s-%d.err",
          "environment = MAUM_ROOT=\"%s\",LD_LIBRARY_PATH=\"%s/lib\"",
          System.lineSeparator()
      );

      String program = String.format(fmt,
          tempChatbotName, tempDaiName, programNum,
          maumRoot,
          tempChatbotName,
          sbSkill,
          tempLang,
          tempDaSpec,
          daiId,
          tempDaiExInfoKey,
          tempObject,
          damName,
          fullCommand,
          maumRoot,
          logsDir, tempChatbotName, tempDaiName, programNum,
          logsDir, tempChatbotName, tempDaiName, programNum,
          maumRoot, maumRoot
      );
      logger.trace("PROGRAM = [{}], {}", program, programNum);
      writer.write(program);

      //tempDaiExInfoKey가 더 존재하는 경우 다음 program name을 위해 증가시킵니다
      programNum += 1;
    }
    logger.debug("#@ End makeM2uDaiConf key [{}] daiExInfo [{}]", key, daiExInfo);
  }

  /**
   * da 실행을 위한 supervisor conf를 만듭니다
   *
   * @param daiMap 등록된 dialogAgentInstance의 portable list
   */
  protected void setConfig(IMap<String, DialogAgentInstancePortable> daiMap, boolean forced) {
    logger.debug("#@ START setConfig process daiMap.size() : {}", daiMap.size());

    //List<String> daNames = new ArrayList<>();

    StringWriter writer = new StringWriter();

    // hzc로 부터 전달받은 dai의 map 갯수만큼 동작합니다
    for (String key : daiMap.keySet()) {
      logger.debug("daiMap.key = {}, {}", key, daiMap.get(key));

      // 해당된 key로 Dai 객체를 생성합니다
      DialogAgentInstance daInstance = daiMap.get(key).getProtobufObj();

      //daiExInfo객체 생성시 해당 dam_name으로 생성합니다
      //dai_exec_info안에는 여러개의 key를 갖을 수 있습니다
      //DAI이름에 DAI-1, DAI-2 식으로 이름을 붙여야 합니다

      // 내 dam 네임을 갖고와서 일치하는 daiExInfo 값을 갖고 옵니다
      // 내 dam 네임은 ~/maum/run/dam.state.json에 있습니다
      String tempMyDamName = ConfigManager.findDamNameFromJson("dam_name");
      logger.debug("#@ tempMyDamName [{}]", tempMyDamName);

      if (tempMyDamName == null || tempMyDamName.isEmpty()) {
        logger.info("#@ There is no dam_name from dam.state.json. So end of setConfig()");

        return;
      }

      for (DialogAgentInstanceExecutionInfo daiExInfo : daInstance.getDaiExecInfoList()) {
        // 내 damName과 일치하면 bw 작성을 시작합니다
        String tempCurrentDamName = daiExInfo.getDamName();
        logger.debug("#@ tempCurrentDamName [{}]", tempCurrentDamName);

        if (tempCurrentDamName.equalsIgnoreCase(tempMyDamName)) {
          logger.debug("START call makeM2uDaiConf key [{}]", key);
          makeM2uDaiConf(daInstance, tempMyDamName, key, writer, daiExInfo);
        } else {
          logger.info(
              "#@ key[{}]'s CurrentDamName[{}] is not equal MyDamName[{}]. So end of setConfig()",
              key, tempCurrentDamName, tempMyDamName);
        }
      }
    }

    try {
      // bw close 호출
      writer.write(System.lineSeparator());
      writer.close();

      // m2u-dai.conf에 새로 등록된 dai 정보를 기입합니다
      // $MAUM_ROOT/bin/m2u-dareg 을 동작시키기 위해 m2u-dai.conf 파일을 생성합니다
      String confDir = PropertyManager.getString("daisvc.conf.dir");
      File svdConf = new File(ConfigManager.getSystemConfPath(confDir));
      String updatedConf = writer.toString();
      boolean update = true;

      if (!forced) {
        byte[] encoded = Files.readAllBytes(svdConf.toPath());
        String prev = new String(encoded, StandardCharsets.UTF_8);
        update = !prev.equals(updatedConf);
      }
      if (update) {
        FileWriter fileWriter = new FileWriter(svdConf);
        fileWriter.write(updatedConf);
        fileWriter.close();
        logger.info("daisvc supervisor conf updated.");
      }
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    logger.debug("#@ END setConfig process");
  }

  /**
   * hazelcast의 DialogAgentInstancePortable를 supervisord에 등록후 실행합니다.
   *
   * @param daiMap hzc의 dai의 portable IMAP
   * @return 성공여부
   */
  public boolean reload(IMap<String, DialogAgentInstancePortable> daiMap, boolean forced) {

    logger.info("[DAI-SVC] reload = {}", daiMap.size());

    // config를 만들어 추가합니다
    setConfig(daiMap, forced);

    // config를 reload한다.
    List<String> added = new ArrayList<>();
    List<String> changed = new ArrayList<>();
    List<String> removed = new ArrayList<>();
    reloadConfig(added, changed, removed);

    for (String obj : removed) {
      connectSupervisord().stopProcess(obj, true);
      connectSupervisord().removeProcessGroup(obj);
    }

    for (String obj : added) {
      connectSupervisord().addProcessGroup(obj);
    }

    for (String obj : changed) {
      connectSupervisord().stopProcess(obj, true);
      connectSupervisord().removeProcessGroup(obj);
      connectSupervisord().addProcessGroup(obj);
    }

    return true;
  }

  public boolean getRetry() {
    return retry;
  }

  public void setRetry(boolean retry) {
    this.retry = retry;
  }
}
