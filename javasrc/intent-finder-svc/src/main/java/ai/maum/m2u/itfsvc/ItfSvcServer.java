package ai.maum.m2u.itfsvc;

import ai.maum.m2u.common.portable.IntentFinderInstancePortable;
import ai.maum.m2u.config.ConfigManager;
import ai.maum.m2u.hzc.HazelcastHelper;
import ai.maum.rpc.ResultStatus;
import com.hazelcast.client.HazelcastClientNotActiveException;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import maum.rpc.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IntentFinderSupervisorControl Server
 */
public class ItfSvcServer extends Thread implements
    EntryAddedListener<String, IntentFinderInstancePortable>,
    EntryRemovedListener<String, IntentFinderInstancePortable>,
    EntryUpdatedListener<String, IntentFinderInstancePortable> {

  static final Logger logger = LoggerFactory.getLogger(ItfSvcServer.class);

  static {
    ResultStatus.setModuleAndProcess(Status.Module.M2U, Status.ProcessOfM2u.M2U_ITFM.getNumber());
  }


  private ConfigManager config = new ConfigManager();
  private HazelcastHelper hzcHelper = new HazelcastHelper();
  private IntentFinderSvctl itfSvctl = new IntentFinderSvctl();

  private boolean hasEvent = false;
  private DateFormat timestampFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss");

  /**
   * ItfSvcServer를 실행한다.
   */
  @Override
  public void run() {
    logger.info("#@ ItfSvcServer run");

    hzcHelper.AddListener(this);

    int count = 0;
    while (true) {
      try {
        if (hasEvent) {
          itfSvctl.reload(hzcHelper.getItfInstances());
          hasEvent = false;
          count = 0;
        } else {
          if (count == 0) {
            logger.info("#@ ItfSvcServer sleep {}", timestampFormat.format(new Date()));
          } else {
            logger.debug("#@ ItfSvcServer sleep {}", timestampFormat.format(new Date()));
          }
          Thread.sleep(5000);
          count++;
          count %= 120;
        }
      } catch (HazelcastClientNotActiveException hzcCliExce) {
        logger.error("{} => ", hzcCliExce.getMessage(), hzcCliExce);
        // 해당 에러가 발생하면 m2u-itfsvc를 svctl에서 stop 시킵니다
        // 이후 m2u-itfsvc는 재시작 되면 정상적으로 동작합니다
        logger.debug("#@ stopProcess m2u-itfsvc");
        itfSvctl.connectSupervisord().stopProcess("m2u-itfsvc", true);

      } catch (InterruptedException e) {
        logger.error("{} => ", e.getMessage(), e);
      }
    }

  }

  /**
   * ItfSvcServer를 초기화한다.
   */
  public void init() {
    hasEvent = true;
    config.load();
    logger.info("#@ hzcHelper.connect");
    hzcHelper.connect(config.getProperties());
  }

  public static void main(String[] args) throws IOException {
    logger.info("#@ ItfSvcServer start...");
    ItfSvcServer server = new ItfSvcServer();
    server.init();
    server.start();

    try {
      server.join();
    } catch (InterruptedException e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }

  /**
   * Hazelcaster의 entry가 추가될경우 호출된다.
   *
   * @param event 변경된 event
   */
  @Override
  public void entryAdded(
      EntryEvent<String, IntentFinderInstancePortable> event) {
    logger.debug("entryAdded = {}", event.toString());
    hasEvent = true;
  }

  /**
   * Hazelcaster의 entry가 삭제될경우 호출된다.
   *
   * @param event 삭제된 event
   */

  @Override
  public void entryRemoved(
      EntryEvent<String, IntentFinderInstancePortable> event) {
    logger.debug("entryRemoved = {}", event.toString());
    hasEvent = true;
  }

  /**
   * Hazelcaster의 entry가 변경될경우 호출된다.
   *
   * @param event 변경된 event
   */

  @Override
  public void entryUpdated(
      EntryEvent<String, IntentFinderInstancePortable> event) {
    logger.debug("entryUpdated = {}", event.toString());
    hasEvent = true;
  }

}
