package ai.maum.m2u.itfsvc;

import ai.maum.m2u.common.portable.IntentFinderInstancePortable;
import ai.maum.m2u.config.ConfigManager;
import ai.maum.m2u.svd.SupervisordXmlRpc;
import com.hazelcast.core.IMap;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import maum.m2u.router.v3.Intentfinder.IntentFinderInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IntentFinder관련 Supervisord control 클래스
 */
public class IntentFinderSvctl {

  static final Logger logger = LoggerFactory.getLogger(IntentFinderSvctl.class);

  static private final String[] localIps = {"localhost", "127.0.0.1", "0.0.0.0"};

  private ArrayList<String> localIpList = new ArrayList<>();

  private void getLocalIpAddress() {
    try {
      logger.info("Scanning Local IP Addresses...");
      Collections.addAll(localIpList, localIps);
      Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
      int index = Integer.MAX_VALUE;
      String primaryIp = "";

      while (interfaces.hasMoreElements()) {
        NetworkInterface networkInterface = interfaces.nextElement();
        Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();

        while (inetAddresses.hasMoreElements()) {
          InetAddress inetAddress = inetAddresses.nextElement();

          logger.info(" >> {}({}) {}"
              , networkInterface.getDisplayName()
              , networkInterface.getIndex()
              , inetAddress.getHostAddress());

          if (inetAddress instanceof Inet4Address && !inetAddress.isLoopbackAddress()) {
            localIpList.add(inetAddress.getHostAddress());
            if (index > networkInterface.getIndex()) {
              index = networkInterface.getIndex();
              primaryIp = inetAddress.getHostAddress();
            }
          }
        }
      }

      logger.info("Local IP List: {}", localIpList);
      if (index < Integer.MAX_VALUE) {
        logger.info("Primary IP is {}.", primaryIp);
      }
    } catch (SocketException e) {
      logger.error("getLocalIpAddress: socket error", e);
    }
  }

  /**
   * 내부 IP를 먼저 구하고 일치된 IP를 처리하도록 한다.
   */
  public IntentFinderSvctl() {
    getLocalIpAddress();
  }

  /**
   * Supervisord에 연결한다.
   *
   * @return XmlRpc 객체를 리턴한다. 실패할경우 null
   */
  protected SupervisordXmlRpc connectSupervisord() {
    SupervisordXmlRpc st = new SupervisordXmlRpc("", "127.0.0.1", 9799, "maum",
        "msl1234~");
    return st;
  }

  /**
   * supervisord에  reloadconfig 수행한다.
   *
   * @param added config에 추가된 itf list
   * @param changed config에 변경된 itf list
   * @param removed config에  삭제된 itf list
   * @return 성공여부
   */
  protected boolean reloadConfig(List<String> added, List<String> changed,
      List<String> removed) {
    SupervisordXmlRpc st = connectSupervisord();
    Object[] result = st.reloadConfig();

    Object[] subObj = (Object[]) result[0];
    for (int i = 0; i < subObj.length; i++) {
      Object[] names = (Object[]) subObj[i];

      for (Object name1 : names) {
        String name = (String) name1;
        switch (i) {
          case 0:
            logger.debug("added {}", name);
            added.add(name);
            break;
          case 1:
            logger.debug("changed {}", name);
            changed.add(name);
            break;
          case 2:
            logger.debug("removed {}", name);
            removed.add(name);
            break;
        }
      }

    }

    return true;
  }

  /**
   * itf 실행을 위한 supervisor conf를 만든다.
   *
   * @param itfiMap 등록된 intentfinderinstance의 protable list
   * @return 성공여부
   */
  protected boolean setConfig(IMap<String, IntentFinderInstancePortable> itfiMap) {
    boolean result = false;

    logger.info("setConfig ");

    if (itfiMap.size() == 0) {
      return false;
    }

    File svdConf = new File(
        ConfigManager.getSystemConfPath("/etc/supervisor/conf.d/m2u-itf.conf"));
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(svdConf);
    } catch (FileNotFoundException e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

    String path = ConfigManager.getSystemConfPath("");

    List<String> itfNames = new ArrayList<String>();
    for (String key : itfiMap.keySet()) {
      try {
        logger.debug("key = {}, {}", key, itfiMap.get(key));
        IntentFinderInstance itfInstance = itfiMap.get(key).getProtobufObj();

        if (!localIpList.contains(itfInstance.getIp())) {
          logger.warn("SKIP: IP '{}' is not belong to this server.", itfInstance.getIp());
          continue;
        }

        bw.write(String.format("[program:itf-%s]\n", itfInstance.getName()));
        bw.write(String.format("command = %s/bin/m2u-itfd\n", path));
        bw.write(String.format("   -n %s\n", itfInstance.getName()));
        bw.write(String.format("   -i %s:%d\n", itfInstance.getIp(), itfInstance.getPort()));

        bw.write(String.format("directory = %s/bin\n", path));
        bw.write("autostart = true\n");
        bw.write("autorestart = true\n");
        bw.write("stopasgroup = true\n");
        bw.write(String.format("stdout_logfile = %s/logs/supervisor/itf-%s.out\n",
            path, itfInstance.getName()));
        bw.write(String.format("stderr_logfile = %s/logs/supervisor/itf-%s.err\n",
            path, itfInstance.getName()));
        bw.write(String.format(
            "environment = MAUM_ROOT=\"%s\",LD_LIBRARY_PATH=\"%s/lib\",GRPC_ENABLE_FORK_SUPPORT=\"False\"\n",
            path, path));

        bw.newLine();
      } catch (Exception e) {
        logger.error("{} => ", e.getMessage(), e);
      }
    }

    try {
      bw.close();
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    return result;
  }

  /**
   * hazelcast의 IntentFinderInstancePortable를 supervisord에 등록후 실행한다.
   *
   * @param itfiMap hazelcast의 intentFinderInstance의 portable IMAP
   * @return 성공여부
   */
  public boolean reload(IMap<String, IntentFinderInstancePortable> itfiMap) {

    logger.info("[ITF-SVC] reload = {}", itfiMap.size());
    //
    // config를 만들어 추가한다.
    // added 정보를 config를 만든다.
    setConfig(itfiMap);

    //
    // config를 reload한다.
    List<String> added = new ArrayList<String>();
    List<String> changed = new ArrayList<String>();
    List<String> removed = new ArrayList<String>();
    reloadConfig(added, changed, removed);

    for (String obj : removed) {
      connectSupervisord().stopProcess(obj, true);
      connectSupervisord().removeProcessGroup(obj);
    }

    for (String obj : added) {
      connectSupervisord().addProcessGroup(obj);
      connectSupervisord().startProcess(obj, true);
    }

    for (String obj : changed) {
      connectSupervisord().stopProcess(obj, true);
      connectSupervisord().removeProcessGroup(obj);
      connectSupervisord().addProcessGroup(obj);
    }

    return true;
  }

}
