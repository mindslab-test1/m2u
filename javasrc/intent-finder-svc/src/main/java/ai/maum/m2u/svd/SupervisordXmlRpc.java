package ai.maum.m2u.svd;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * supervisord XML-RPC 연동 class
 */
public class SupervisordXmlRpc {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(SupervisordXmlRpc.class);

  private String identifier;
  private int pid;
  private String hostName;
  private String ip;
  private int port;
  private Date latestInformDtm;
  private XmlRpcClient client;
  private XmlRpcClientConfigImpl config;

  public SupervisordXmlRpc(String host, String ip, int port, String username, String password) {

    String url = "http://" + ip + ":" + String.valueOf(port) + "/RPC2";
    logger.debug("XmlRpc {}", url);
    this.client = new XmlRpcClient();
    this.config = new XmlRpcClientConfigImpl();
    try {
      config.setServerURL(new URL(url));
      config.setBasicUserName(username);
      config.setBasicPassword(password);
      config.setEnabledForExceptions(true);
      config.setConnectionTimeout(10 * 1000);
      config.setReplyTimeout(10 * 1000);
      client.setConfig(config);
      this.hostName = host;
      this.ip = ip;
      this.port = port;
      //      this.url = url;
      this.latestInformDtm = new Date();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }

  /**
   * supervisord의 identifier를 가져온다.
   *
   * @return identifier값
   */
  public String getIdentifier() {
    return this.identifier;
  }

  /**
   * supervisord의 hostName을 가져온다.
   *
   * @return hostName
   */
  public String getHostName() {
    return this.hostName;
  }

  /**
   * supervisord의 접속 IP가져온다.
   *
   * @return IP리턴
   */
  public String getIp() {
    return this.ip;
  }

  /**
   * supervisord의 접속 Port가져온다.
   *
   * @return port리턴
   */
  public int getPort() {
    return this.port;
  }

  /**
   * update시간을 설정한다.
   *
   * @param curDate 현재시간
   */
  public void updateLatestInformDtm(Date curDate) {
    this.latestInformDtm = curDate;
  }

  /**
   * supervisord의 identifying string 리턴한다.
   *
   * @return Identification
   */
  public String getIdentification() {
    String result = "";
    try {
      Object[] params = new Object[] {};
      result = (String) this.client.execute("supervisor.getIdentification", params);
      this.identifier = result;
    } catch (XmlRpcException e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.debug("supervisor.getIdentification result={}", result);

    return result;
  }

  /**
   * supervisord의 PID 가져온다.
   *
   * @return pid
   */
  public int getPID() {
    int result = -1;
    try {
      Object[] params = new Object[] {};
      result = (int) this.client.execute("supervisor.getPID", params);
      this.pid = result;
    } catch (XmlRpcException e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    logger.debug("supervisor.getPID result={}", result);
    return result;
  }

  /**
   * supervisord의 상태를 읽어온다.
   *
   * @return result Map<Object, Object> statecode : 2,  1,  0, -1
   * statename : FATAL, RUNNING,  RESTARTING, SHUTDOWN
   */
  @SuppressWarnings("unchecked")
  public Map<Object, Object> getState() {
    Map<Object, Object> result = new HashMap<Object, Object>();
    try {
      Object[] params = new Object[] {};
      result = (Map<Object, Object>) this.client.execute("supervisor.getState", params);
      logger.info("getState > statecode = {} / statename = {}", result.get("statecode"), result.get("statename"));
    } catch (XmlRpcException xre) {
      result.put("statecode", -9999);
      result.put("statename", "Not Available");
    }
    return result;
  }

  /**
   * 사용가능한  method names 가져오기
   *
   * @return array result An array of method names available (strings).
   */
  @SuppressWarnings("unchecked")
  public Object[] listMethods() {
    Object[] result = null;
    try {
      Object[] params = new Object[] {};
      result = (Object[]) this.client.execute("system.listMethods", params);
      logger.info("listMethods result={}", result);
    } catch (XmlRpcException e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  /**
   * 모든 processInfo 가져오기
   *
   * @return array result An array of process status results
   */
  @SuppressWarnings("unchecked")
  public Object[] getAllProcessInfo() {
    Object[] result = null;
    try {
      Object[] params = new Object[] {};
      result = (Object[]) this.client.execute("supervisor.getAllProcessInfo", params);
      logger.info(result[0].getClass().getName());

      for (int i = 0; i < result.length; i++) {
        Map<Object, Object> rm = (HashMap<Object, Object>) result[i];
        logger.info("{} : name = {}", i, rm.get("name"));
      }
      Map<Object, Object> rm = (HashMap<Object, Object>) result[0];
      for (Map.Entry<Object, Object> entry : rm.entrySet()) {
        logger.info("{} : {}    [{}]", entry.getKey(), rm.get(entry.getKey()), rm.get(entry.getKey()).getClass());
      }
    } catch (Exception e) {
      logger.info("{}.getAllProcessInfo = {}", this.getClass().getName(), e.getMessage());
    }
    return result;
  }

  /**
   * process 정보를 가져 온다.
   *
   * @param name 프로세스이름
   * @return struct result A structure containing data about the process
   */
  @SuppressWarnings (value="unchecked")
  public Object getProcessInfo(String name) {
    logger.info("supervisor.getProcessInfo = {}", name);
    Object result = null;
    try {
      Object[] params = new Object[] {name};
      result = (Object) this.client.execute("supervisor.getProcessInfo", params);
      Map<Object, Object> rm = (Map<Object, Object>) result;
      for (Map.Entry<Object, Object> entry : rm.entrySet()) {
        logger.info("{} : {} [{}]", entry.getKey(), rm.get(entry.getKey()), rm.get(entry.getKey()).getClass());
      }
      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  /**
   * config를 다시 가져온다.
   *
   * @return add, change, remove 그룹을 리턴하낟.
   */
  public Object[] reloadConfig() {
    Object[] result = null;
    try {
      Object[] params = new Object[] {};
      result = (Object[]) this.client.execute("supervisor.reloadConfig", params);
      logger.info("supervisor.reloadConfig result = {}", result);

      Object[] subObj = (Object[]) result[0];
      for (int i = 0; i < subObj.length; i++) {

        Object[] names = (Object[]) subObj[i];

        for (int j = 0; j < names.length; j++) {
          logger.info("  + [{}, {}] {}", i, j, names[j]);
        }

      }

    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  /**
   * process group을 시작한다.
   *
   * @param name 프로세스 이름
   * @param wait 처리될때까지 대기 여부
   * @return 성공여부
   */
  public Object[] startProcessGroup(String name, boolean wait) {
    Object[] result = null;
    try {
      Object[] params = new Object[] {name, wait};
      result = (Object[]) this.client.execute("supervisor.startProcessGroup", params);
      logger.info("supervisor.startProcessGroup name={}, result = {}", name, result);
      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  /**
   * process group을 멈춘다..
   *
   * @param name 프로세스 이름
   * @param wait 처리될때까지 대기 여부
   * @return 성공여부
   */
  public boolean stopProcessGroup(String name, boolean wait) {
    boolean result = false;
    try {
      Object[] params = new Object[] {name, wait};
      result = (boolean) this.client.execute("supervisor.stopProcessGroup", params);
      logger.info("supervisor.stopProcessGroup name={}, result = {}", name, result);
      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  /**
   * process를 시작한다.
   *
   * @param name 프로세스 이름
   * @param wait 처리될때까지 대기 여부
   * @return 성공여부
   */
  public boolean startProcess(String name, boolean wait) {
    boolean result = false;
    try {
      Object[] params = new Object[] {name, wait};
      result = (boolean) this.client.execute("supervisor.startProcess", params);
      logger.info("supervisor.startProcess name={}, result = {}", name, result);
      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  /**
   * process를 중지한다.
   *
   * @param name 프로세스 이름
   * @param wait 처리될때까지 대기 여부
   * @return 성공여부
   */
  public boolean stopProcess(String name, boolean wait) {
    boolean result = false;
    try {
      Object[] params = new Object[] {name, wait};
      result = (boolean) this.client.execute("supervisor.stopProcess", params);
      logger.info("supervisor.stopProcess name={}, result = {}", name, result);
      return result;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  /**
   * 모든 process를 시작한다.
   *
   * @param wait 처리될때까지 대기 여부
   * @return 성공여부
   */
  @SuppressWarnings("unchecked")
  public void startAllProcesses(boolean wait) {
    //boolean result = false;
    try {
      Object[] params = new Object[] {wait};
      Object[] result = (Object[]) this.client.execute("supervisor.startAllProcesses", params);
      for (Object obj : result) {
        Map<Object, Object> rm = (Map<Object, Object>) obj;
        for (Map.Entry<Object, Object> entry : rm.entrySet()) {
          logger.info("{} : {} [{}]", entry.getKey(), rm.get(entry.getKey()), rm.get(entry.getKey()).getClass());
        }
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    //return result;
  }

  /**
   * 모든 process를 정지한다.
   *
   * @param wait 처리될때까지 대기 여부
   * @return 성공여부
   */
  @SuppressWarnings (value="unchecked")
  public void stopAllProcesses(boolean wait) {
    try {
      Object[] params = new Object[] {wait};
      Object[] result = (Object[]) this.client.execute("supervisor.stopAllProcesses", params);
      for (Object obj : result) {
        Map<Object, Object> rm = (Map<Object, Object>) obj;
        for (Map.Entry<Object, Object> entry : rm.entrySet()) {
          logger.info("{} : {}  [{}]", entry.getKey(), rm.get(entry.getKey()), rm.get(entry.getKey()).getClass());
        }
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }


  /**
   * supervisor에 프로세스를 추가한다.
   *
   * @param name 프로세스 이름
   * @return 성공여부
   */
  @SuppressWarnings (value="unchecked")
  public boolean addProcessGroup(String name) {
    boolean result = false;
    try {
      Object[] params = new Object[] {name};
      result = (boolean) this.client.execute("supervisor.addProcessGroup", params);
      logger.info("supervisor.readProcessStdoutLog name={}, result = {}", name, result);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }


  /**
   * supervisor에 프로세스를 제거한다.
   *
   * @param name 프로세스 이름
   * @return 성공여부
   */
  @SuppressWarnings (value="unchecked")
  public boolean removeProcessGroup(String name) {
    boolean result = false;
    try {
      Object[] params = new Object[] {name};
      result = (boolean) this.client.execute("supervisor.removeProcessGroup", params);
      logger.info("supervisor.readProcessStdoutLog name={}, result = {}", name, result);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return result;
  }

  /**
   * supervisor process를 재시작한다.
   */
  @SuppressWarnings (value="unchecked")
  public void restart() {
    boolean result = false;
    try {
      Object[] params = new Object[] {};
      result = (boolean) this.client.execute("supervisor.restart", params);
      logger.info("supervisor.restart, result = {}", result);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
  }
}
