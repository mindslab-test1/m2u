package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableFactory;


public class IntentFinderManagerPortableFactory implements PortableFactory {

  @Override
  public Portable create(int classId) {
    if (classId == PortableClassId.INTENT_FINDER_INSTANCE) {
      return new IntentFinderInstancePortable();
    } else {
      throw new IllegalArgumentException(classId + " unsupported classId");
    }
  }
}
