package ai.maum.m2u.hzc;

import ai.maum.m2u.common.portable.IntentFinderInstancePortable;
import ai.maum.m2u.common.portable.IntentFinderManagerPortableFactory;
import ai.maum.m2u.common.portable.PortableClassId;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.map.listener.MapListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Hazelcast 연동을 위한 Helper 클래스
 */
public class HazelcastHelper {

  static final Logger logger = LoggerFactory.getLogger(HazelcastHelper.class);

  public static final String HzcAuthPolicy = "auth-policy";
  public static final String HzcChatbotDetail = "chatbot-detail";
  public static final String HzcDAManager = "dialog-agent-manager";
  public static final String HzcDA = "dialog-agent";
  public static final String HzcDAActivation = "dialog-agent-activation";
  public static final String HzcDAInstance = "dialog-agent-instance";
  public static final String HzcDAInstExec = "dialog-agent-instance-exec";
  public static final String HzcIntentFinderInstExec = "intent-finder-instance-exec";
  public static final String HzcSC = "simple-classifier";
  public static final String HzcChatbot = "chatbot";
  public static final String HzcIntentFinderInstance = "intent-finder-instance";
  public static final String HzcIntentFinderPolicy = "intent-finder-policy";

  private HazelcastInstance hazelcastInstance = null;

  /**
   * HazelcastInstance를 리턴한다.
   *
   * @return HazelcastInstance의 인스턴스
   */
  public HazelcastInstance getHzcInstance() {
    return hazelcastInstance;
  }

  /**
   * hazelcaster server에 연결한다.
   *
   * @param properties 설정 파일의 Properties
   * @return 성공여부
   */
  public boolean connect(Properties properties) {
    List<String> addrList = new ArrayList<String>();

    String count = properties.getProperty("hazelcast.server.count");
    String port = properties.getProperty("hazelcast.server.port");

    for (int i = 1; i < (Integer.parseInt(count) + 1); i++) {
      addrList.add(properties.getProperty("hazelcast.server.ip." + i) + ':' + port);
    }

    ClientConfig clientConfig = new ClientConfig();

    clientConfig.setGroupConfig(
        new GroupConfig(properties.getProperty("hazelcast.group.name"),
            properties.getProperty("hazelcast.group.password")));
    clientConfig.getNetworkConfig().setAddresses(addrList);

    try {
      String timeout = properties.getProperty("hazelcast.invocation.timeout");
      if (Integer.parseInt(timeout) > 0) {
        clientConfig.setProperty("hazelcast.client.invocation.timeout.seconds", timeout);
      }
    } catch (Exception e) {
      logger.warn("Hazelcast connect {} => ", e.getMessage(), e);
    }

    clientConfig.getSerializationConfig()
        .addPortableFactory(PortableClassId.FACTORY_ID,
            new IntentFinderManagerPortableFactory());
    hazelcastInstance = com.hazelcast.client.HazelcastClient
        .newHazelcastClient(clientConfig);

    return true;
  }

  /**
   * IntentFinderInstance의 IMAP을 가져온다.
   *
   * @return IntentFinderInstancePortable의 IMAP
   */
  public IMap<String, IntentFinderInstancePortable> getItfInstances() {
    IMap<String, IntentFinderInstancePortable> itfiMap =
        getHzcInstance().getMap(HazelcastHelper.HzcIntentFinderInstance);
    return itfiMap;
  }

  /**
   * Map이벤트에 대한 Listener를 등록한다.
   *
   * @param listener 이벤트를 받을 listener
   */
  public void AddListener(MapListener listener) {
    IMap<String, IntentFinderInstancePortable> itfiMap =
        getHzcInstance().getMap(HazelcastHelper.HzcIntentFinderInstance);
    itfiMap.addEntryListener(listener, true);

  }


}
