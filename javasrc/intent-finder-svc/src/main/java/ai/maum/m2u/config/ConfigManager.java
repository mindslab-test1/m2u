package ai.maum.m2u.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * M2U Config를 관리하는 클래스
 */
public class ConfigManager {

  static final Logger logger = LoggerFactory.getLogger(ConfigManager.class);

  Properties properties = null;

  public ConfigManager() {
  }


  /**
   * properties 객체를 리턴한다.
   *
   * @return properties instance
   */
  public Properties getProperties() {
    return properties;
  }

  /**
   * MAUM_ROOT 포함하여 paramPath 위치를 리턴한다.
   *
   * @param paramPath 추가할 path
   * @return 전체 path
   */
  public static String getSystemConfPath(String paramPath) {
    String path = System.getenv("MAUM_ROOT");
    path += paramPath;
    return path;
  }

  /**
   * m2u.conf를 load한다.
   *
   * @return 성공여부
   */
  public boolean load() {
    boolean result = false;
    properties = new Properties();

    String conf = getSystemConfPath("/etc/m2u.conf");
    try {
      properties.load(new FileInputStream(conf));
      result = true;
    } catch (IOException e) {
      logger.error("{} => ", e.getMessage(), e);
    }

    return result;
  }


}
