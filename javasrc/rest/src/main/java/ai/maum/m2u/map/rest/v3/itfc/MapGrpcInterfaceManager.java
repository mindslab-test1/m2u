package ai.maum.m2u.map.rest.v3.itfc;

import ai.maum.m2u.map.rest.common.config.Keys;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Metadata.Key;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.MaumToYouProxyServiceGrpc;
import maum.m2u.map.MaumToYouProxyServiceGrpc.MaumToYouProxyServiceStub;
import org.slf4j.LoggerFactory;


public class MapGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(MapGrpcInterfaceManager.class);

  private ManagedChannel channel;

  public MapGrpcInterfaceManager(String host, int port) {
    this(ManagedChannelBuilder.forAddress(host, port)
        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
        // needing certificates.
        .usePlaintext(true));
  }

  MapGrpcInterfaceManager(ManagedChannelBuilder<?> channelBuilder) {
    channel = channelBuilder.build();
  }

  public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }


  /**
   * Map의 eventStream를 호출하는 서비스입니다.
   */
  public MapDirective callEvent(MapEvent req, String authToken, String internalAuthHeader)
      throws Exception {
    logger.trace("==========TestGrpcInterfaceManager.callEvent = {}", req);
    try {
      // 헤더가 중첩으로 쌓이는 현상이 발생하여 stub 초기화
      MaumToYouProxyServiceStub mapStub = MaumToYouProxyServiceGrpc.newStub(channel);

      // GRPC 헤더에  추가
      Metadata fixedHeaders = new Metadata();

      if (!"".equals(authToken)) {
        // authToken토큰을 발급받은 이후에는 모든 Service 호출시 M2U_AUTH_TOKEN_HEADER에 토큰값을 할당해야 합니다.
        fixedHeaders.put(Key.of(Keys.M2U_AUTH_TOKEN_HEADER, Metadata.ASCII_STRING_MARSHALLER)
            , authToken);
      }
      if (!"".equals(internalAuthHeader)) {
        // m2u-auth-internal있을경우 그대로 토스
        fixedHeaders.put(Key.of(Keys.M2U_AUTH_INTERNAL_HEADER, Metadata.ASCII_STRING_MARSHALLER)
            , internalAuthHeader);
      }
      // Sigin Service 호출시 M2U_AUTH_SIGN_IN_HEADER가 추가되어야 합니다.
      // M2U_AUTH_SIGN_IN_HEADER에 값이 없어도 되지만 OperationSyncId를 할당해 주었습니다.
      fixedHeaders.put(Key.of(Keys.M2U_AUTH_SIGN_IN_HEADER, Metadata.ASCII_STRING_MARSHALLER)
          , req.getEvent().getOperationSyncId());

      mapStub = MetadataUtils.attachHeaders(mapStub, fixedHeaders);

      // 비동기 처리를 위해서 CountDownLatch를 사용하였습니다.
      final CountDownLatch finishLatch = new CountDownLatch(1);
      HashMap<String, MapDirective> result = new HashMap<>();

      StreamObserver<MapDirective> responseObserver = new StreamObserver<MapDirective>() {
        @Override
        public void onNext(MapDirective res) {
          logger.trace("#@ callEvent onNext :" + res.toString());
          result.put("result", res);
        }

        @Override
        public void onError(Throwable t) {
          logger.error("#@ callEvent onError [{}]", t.toString());
          finishLatch.countDown();
        }

        @Override
        public void onCompleted() {
          logger.info("#@ callEvent Complete.");
          finishLatch.countDown();
        }
      };

      StreamObserver<MapEvent> requestObserver = mapStub.eventStream(responseObserver);

      requestObserver.onNext(req);
      requestObserver.onCompleted();

      // finishLatch를 사용하여 최대 1분간 blocking 하도록 합니다.
      if (!finishLatch.await(1, TimeUnit.MINUTES)) {
        logger.warn("Can not finish within 1 MINUTE");
      }

      return result.get("result");
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus(), e);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("{} => ", e.getMessage(), e);
      } finally {
        this.shutdown();
        throw e;
      }
    } catch (InterruptedException e) {
      logger.error("{} => ", e.getMessage(), e);
      this.shutdown();
      throw e;
    }
  }
}
