package ai.maum.m2u.map.rest.v3.dialog;

import ai.maum.m2u.map.rest.common.config.Keys;
import ai.maum.m2u.map.rest.v3.itfc.MapWebSocketInterfaceManager;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

@Component
@SuppressWarnings("unchecked")
public class TextToSpeechTalkController extends AbstractWebSocketHandler {

  private static final Logger logger = LoggerFactory.getLogger(TextToSpeechTalkController.class);

  // sessionId, RestMapDispatcher를 저장하기 위한 HashMap
  // ByteStream 전송시 HashMap에 저장된 restMapDispatcher로 Map에게 전송한다
  private static HashMap<String, MapWebSocketInterfaceManager> sessionSocketMap = new HashMap();
  ;

  /**
   * Client와 연결이 완료된 상태
   */
  @Override
  public void afterConnectionEstablished(WebSocketSession session) throws Exception {
    String sessionId = session.getId();

    logger.info("#@ IN Client to Rest afterConnectionEstablished [{}]", sessionId);

    if (!sessionSocketMap.containsKey(sessionId)) {
      // Client로 부터 STS 요청이 들어오면 WSTextToSpeechTalkManager 객체를 생성하여 HashMap에 저장
      sessionSocketMap.put(session.getId(), new MapWebSocketInterfaceManager(session));
    }
  }

  /**
   * Client로 부터 TestMessage를 수신합니다(예 : MapEvent)
   */
  @Override
  protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
    String sessionId = session.getId();
    logger.debug("#@ IN Client to Rest handleTextMessage id [{}], sessCnt [{}]",
        sessionId, sessionSocketMap.size());
    logger.trace("#@ handleTextMessage id [{}], payload [{}]", sessionId, message.getPayload());

    // sessionList에 없는 session인 경우 MapWebSocketInterfaceManager 객체를 생성
    if (sessionSocketMap.containsKey(sessionId)) {
      sessionSocketMap.get(sessionId)
          .handleEvent(sessionId, Keys.DL_E_TEXT_TO_SPEECH_TALK, message);
    } else {
      // 해당 sessionId가 없는 경우
      logger.info("#@ handleTextMessage There is no sessionId [{}]", sessionId);
    }
  }

  /**
   * Client로 부터 websocket afterConnectionClosed를 전달 받으면 MAP으로 onCompleted 호출, GRPC closeChannel
   */
  @Override
  public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    String sessionId = session.getId();
    logger.info("#@ IN Client to Rest afterConnectionClosed [{}]", sessionId);

    // HashMap에서 해당 sessionId 조회
    if (sessionSocketMap.containsKey(sessionId)) {
      // 해당 tempSessId를 HashMap에서 삭제
      sessionSocketMap.remove(sessionId).websocketClosed();
    } else {
      // 해당 sessionId가 없는 경우
      logger.info("#@ afterConnectionClosed There is no sessionId [{}]", sessionId);
    }
  }
}
