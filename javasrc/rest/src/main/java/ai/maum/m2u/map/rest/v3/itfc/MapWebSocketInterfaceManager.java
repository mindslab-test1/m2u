package ai.maum.m2u.map.rest.v3.itfc;

import ai.maum.m2u.map.rest.common.config.Keys;
import ai.maum.m2u.map.rest.common.config.PropertyManager;
import ai.maum.m2u.map.rest.common.utils.GrpcUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Struct;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import maum.brain.stt.Speech.SpeechRecognitionParam;
import maum.common.Audioencoding.AudioEncoding;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.DeviceOuterClass.Device;
import maum.m2u.common.Dialog.Utter;
import maum.m2u.common.Dialog.Utter.InputType;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.EventStream.EventContext;
import maum.m2u.map.Map.EventStream.EventParam;
import maum.m2u.map.Map.MapEvent;
import maum.m2u.map.MaumToYouProxyServiceGrpc;
import maum.m2u.map.MaumToYouProxyServiceGrpc.MaumToYouProxyServiceStub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

/**
 * WSTextToSpeechTalkManager 설명 Client로 부터 수신받은 Event를 Map으로 전달 Map으로 수신받은 Directive를 Client로 전달 비동기
 * GRPC 통신을 위해 Dispacher 클래스를 생성
 */
@SuppressWarnings("unchecked")
public class MapWebSocketInterfaceManager {

  private final Logger logger = LoggerFactory.getLogger(MapWebSocketInterfaceManager.class);

  // Rest to MAP
  // 스트림 메시지를 MAP 에게 전송, MAP으로부터 수신을 하는 Stub 을 사용하는 클래스 변수
  private StreamObserver<MapEvent> mapSender;
  private ManagedChannel mapChannel;
  // MAP 과 통신하기 위한 Client Stub
  private MaumToYouProxyServiceStub mapStub;
  // Client에게 SendMessage 전달용 객체
  private WebSocketSession wbSession;

  /**
   * 필요한 객체를 초기화 합니다.
   */
  public MapWebSocketInterfaceManager(WebSocketSession wbSession) {
    // 생성자에서 객체를 모두 초기화 합니다
    this.wbSession = wbSession;
    this.mapChannel = GrpcUtils.getChannel(PropertyManager.getString("map.export"));
    this.mapStub = MaumToYouProxyServiceGrpc.newStub(this.mapChannel);
  }

  public void websocketClosed() {
    logger.info("#@ websocketClosed");
    mapSender.onCompleted();

    if (!mapChannel.isShutdown()) {
      logger.trace("#@ GrpcUtils.closeChannel");
      // Channel close
      GrpcUtils.closeChannel(mapChannel);
    }
  }

  /**
   * client에서 수신받은 TextMessage를 MapEvent로 가공하여 MAP으로 전달합니다
   */
  public void handleEvent(String sessionId, String opType, TextMessage message) throws IOException {
    logger.info("#@ handleEvent [{}] {}", sessionId, opType);

    // Header 정보를 설정
    this.setHeaderInfo(message);
    // #@ rest 와 Map과 통신을 위한 Observer를 등록
    this.mapSender = mapStub.eventStream(new MapDirectiveRecvObserver());

    MapEvent.Builder mapEventParam = this.getMapEvent(opType, message);
    if (mapEventParam == null) {
      String msg = "failed to construct a MapEvent";
      logger.error(msg);
      throw new NullPointerException(msg);
    }

    logger.trace("#@ OUT Rest to MapEvent talkParam [{}]", JsonFormat.printer()
        .includingDefaultValueFields()
        .print(mapEventParam));

    // 4.Rest toMAP으로 Event를 전달
    this.mapSender.onNext(mapEventParam.build());
  }

  /**
   * client에서 수신받은 Binary(ByteStream)을 MAP으로 전달합니다
   */
  public void handleBinary(BinaryMessage message) {
    // logger.info("#@ handleBinary BinaryMessage [{}]", message);

    if (this.mapSender != null) {
      // Client로 부터 수신받은 BinaryMessage를 MapEvent ByteStream로 변경
      MapEvent byteStreamParam = MapEvent.newBuilder().setBytes(
          ByteString.copyFrom(message.getPayload().array())).build();

      // logger.info("#@ OUT restToMap byteStreamParam [{}]", byteStreamParam); // 모든 byteStream
      this.mapSender.onNext(byteStreamParam);
    } else {
      logger.info("#@ restToMapObserver is null, so this request was rejected");
    }
  }

  /**
   * Header 정보 설정 mapSecureForwarderStub 객체를 초기화 하는 함수
   */
  private void setHeaderInfo(TextMessage message) {
    logger.info("START resetHeaderInfo message.getPayload() [{}]", message.getPayload());

    ObjectMapper mapper = new ObjectMapper();
    Map<String, Object> map = new HashMap<>();

    // convert JSON string to Map
    try {
      map = mapper.readValue(message.getPayload(), new TypeReference<Map<String, Object>>() {
      });
      logger.info("#@ setHeaderInfo authToken [{}]", map.get("authToken"));
    } catch (IOException e) {
      logger.error("setHeaderInfo e : ", e);
    }

    // 인증정보를 헤더에 정보를 추가한다.
    Metadata fixedHeaders = new Metadata();
    if (map.containsKey("authToken")) {
      Metadata.Key<String> key = Metadata.Key
          .of(Keys.M2U_AUTH_TOKEN_HEADER, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(key, map.get("authToken").toString());
    }
    if (map.containsKey("authInternal")) {
      Metadata.Key<String> key = Metadata.Key
          .of(Keys.M2U_AUTH_INTERNAL_HEADER, Metadata.ASCII_STRING_MARSHALLER);
      fixedHeaders.put(key, map.get("authInternal").toString());
    }

    if (fixedHeaders.keys().size() > 0) {
      this.mapStub = MetadataUtils.attachHeaders(this.mapStub, fixedHeaders);
    }
  }

  /**
   * MAP으로 부터 수신받은 Directive 처리
   */
  class MapDirectiveRecvObserver implements StreamObserver<maum.m2u.map.Map.MapDirective> {

    @Override
    public void onNext(maum.m2u.map.Map.MapDirective value) {
      // 1. Map으로 부터 Directive를 수신 합니다
      // 2. MapDirective를 Client로 전달합니다
      String directive = "";
      try {
        directive = JsonFormat.printer().includingDefaultValueFields().print(value);
        logger.trace("#@ IN MapToRest onNext[{}]", directive);
      } catch (InvalidProtocolBufferException e) {
        logger.error("onNext e : ", e);
      }

      try {
        wbSession.sendMessage(new TextMessage(directive));
      } catch (IOException e) {
        logger.error("onNext e : ", e);
      }
    }

    @Override
    public void onError(Throwable throwable) {
      logger.error("#@ IN MAP to Rest onError", throwable);

      // Client에게 에러 전달
      // TODO 에러 메시지는 무엇으로 해야 할까요
      try {
        wbSession.sendMessage(new TextMessage("Error, grpc error"));
      } catch (IOException e) {
        logger.error("onError e : ", e);
      }

      if (mapChannel != null) {
        logger.info("#@ End restToMapChannel close by onError");
        // _restToMapChannel을 close 합니다
        GrpcUtils.closeChannel(mapChannel);
        mapChannel = null;
      }
    }

    @Override
    public void onCompleted() {
      logger.info("#@ IN MAP to Rest onCompleted..");
      if (mapChannel != null) {
        GrpcUtils.closeChannel(mapChannel);
        mapChannel = null;
      }
    }
  }

  /**
   * MAP으로 보낼 MapEvent를 생성 opType으로 MapEvent를 생성 (SpeechToSpeechTalk, TextToSpeechTalk,
   * SpeechToTextTalk)
   */
  private MapEvent.Builder getMapEvent(String opType, TextMessage message) {
    ObjectMapper mapper = new ObjectMapper();
    MapEvent.Builder talkParam;

    switch (opType) {
      case Keys.DL_E_SPEECH_TO_SPEECH_TALK:
      case Keys.DL_E_SPEECH_TO_TEXT_TALK:
        try {
          // convert JSON string to Map
          Map<String, Object> map = mapper
              .readValue(message.getPayload(), new TypeReference<Map<String, Object>>() {
              });

          logger.info("#@ payload [{}], device [{}]", map.get("payload"), map.get("device"));
          HashMap<String, Object> payload = (HashMap) map.get("payload");
          HashMap<String, Object> device = (HashMap) map.get("device");
          HashMap<String, Object> location = (HashMap) map.get("location");
          HashMap<String, Object> param = (HashMap) map.get("param");
          Map<String, Object> speechParam = (HashMap) param.get("speech_recognition_param");

          if (device.size() > 1 && location.size() > 1) {
            // 1.Timestamp 생성
            Timestamp.Builder timestamp = Timestamp.newBuilder()
                .setSeconds(System.currentTimeMillis() / 1000);

            // single_utterance 설정 (true, false)
            boolean isSingleUtter = true;
            if (speechParam.get("single_utterance").toString().equalsIgnoreCase("false")) {
              isSingleUtter = false;
            }

            talkParam = MapEvent.newBuilder().setEvent(EventStream.newBuilder()
                .setInterface(AsyncInterface.newBuilder()
                    .setInterface(Keys.I_DIALOG)
                    .setOperation(opType)
                    .setStreaming(true))
                .setStreamId(GrpcUtils.getUUID())
                .setOperationSyncId(GrpcUtils.getUUID())
                .setParam(EventParam.newBuilder()
                    .setSpeechRecognitionParam(SpeechRecognitionParam.newBuilder()
                        .setEncoding(AudioEncoding.valueOf(speechParam.get("encoding").toString()))
                        .setSampleRate(Integer.parseInt(speechParam.get("sample_rate").toString()))
                        .setModel(speechParam.get("model").toString())
                        .setLang(Lang.valueOf(payload.get("lang").toString()))
                        .setSingleUtterance(isSingleUtter)))
                .addContexts(EventContext.newBuilder()
                    .setDevice(Device.newBuilder()
                        .setId(device.get("id").toString())
                        .setType(device.get("type").toString())
                        .setVersion(device.get("version").toString())
                        .setChannel(device.get("channel").toString())
                        .setSupport(GrpcUtils.getCapabilities(device))
                        .setTimestamp(timestamp).setTimezone("KST+9")))
                .addContexts(EventContext.newBuilder()
                    .setLocation(Location.newBuilder()
                        .setLatitude(Float.parseFloat(location.get("latitude").toString()))
                        .setLongitude(Float.parseFloat(location.get("longitude").toString()))
                        .setLocation(location.get("location").toString()))));

            return talkParam;
          }
        } catch (Exception e) {
          logger.error("#@ getMapEvent", e);
          try {
            wbSession.sendMessage(
                new TextMessage("Error, Required param is missing, please check required param"));
          } catch (IOException e1) {
            logger.error("getMapEvent e1 : ", e1);
          }
        }
      case Keys.DL_E_TEXT_TO_SPEECH_TALK:
        try {
          // convert JSON string to Map
          Map<String, Object> map = mapper
              .readValue(message.getPayload(), new TypeReference<Map<String, Object>>() {
              });

          logger.info("#@ payload [{}], device [{}]", map.get("payload"), map.get("device"));
          HashMap<String, Object> payload = (HashMap) map.get("payload");
          HashMap<String, Object> device = (HashMap) map.get("device");
          HashMap<String, Object> location = (HashMap) map.get("location");

          if (device.size() > 1 && location.size() > 1) {
            // S-1.Timestamp 생성
            Timestamp.Builder timestamp = Timestamp.newBuilder()
                .setSeconds(System.currentTimeMillis() / 1000);

            // S-2.Utter 생성
            Struct.Builder stb = Struct.newBuilder();
            Utter.Builder taklPayload = Utter.newBuilder()
                .setUtter(payload.get("utter").toString())
                .setLang(Lang.valueOf(payload.get("lang").toString()))
                .setInputType(InputType.valueOf("KEYBOARD"))
                .setMeta(GrpcUtils.hashMapToStruct((HashMap) payload.get("meta")));

            String json = JsonFormat.printer().includingDefaultValueFields().print(taklPayload);
            JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

            // S-3.MapEvent 생성
            talkParam = MapEvent.newBuilder().setEvent(EventStream.newBuilder()
                .setInterface(AsyncInterface.newBuilder()
                    .setInterface(Keys.I_DIALOG)
                    .setOperation(Keys.DL_E_TEXT_TO_SPEECH_TALK)
                    .setType(OperationType.OP_EVENT)
                    .setStreaming(true))
                .setStreamId(GrpcUtils.getUUID())
                .setOperationSyncId(GrpcUtils.getUUID())
                .addContexts(EventContext.newBuilder()
                    .setDevice(Device.newBuilder()
                        .setId(device.get("id").toString())
                        .setType(device.get("type").toString())
                        .setVersion(device.get("version").toString())
                        .setChannel(device.get("channel").toString())
                        .setSupport(GrpcUtils.getCapabilities(device))
                        .setTimestamp(timestamp).setTimezone("KST+9")))
                .addContexts(EventContext.newBuilder()
                    .setLocation(Location.newBuilder()
                        .setLatitude(Float.parseFloat(location.get("latitude").toString()))
                        .setLongitude(Float.parseFloat(location.get("longitude").toString()))
                        .setLocation(location.get("location").toString())))
                .setPayload(stb));

            return talkParam;
          }
        } catch (Exception e) {
          logger.error("#@ getMapEvent", e);
          try {
            wbSession.sendMessage(
                new TextMessage("Error, Required param is missing, please check required param"));
          } catch (IOException e1) {
            logger.error("getMapEvent e1 : ", e1);
          }
        }
      default:
        // 처리 할수 없는 opType 에러 처리
        return null;
    }
  }
}
