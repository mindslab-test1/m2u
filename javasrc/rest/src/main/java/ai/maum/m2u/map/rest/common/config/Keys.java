package ai.maum.m2u.map.rest.common.config;

import java.util.HashMap;

public final class Keys {

  public static final String M2U_AUTH_SIGN_IN_HEADER = "m2u-auth-sign-in";
  public static final String M2U_AUTH_TOKEN_HEADER = "m2u-auth-token";
  public static final String M2U_AUTH_INTERNAL_HEADER = "m2u-auth-internal";
  public static final String I_DIALOG = "Dialog";
  public static final String I_DIALOG_WEB = "DialogWeb";
  public static final String DLW_E_OPEN = "Open";
  public static final String DLW_E_CLOSE = "Close";
  public static final String DLW_E_TEXT_TO_TEXT_TALK = "TextToTextTalk";
  public static final String DLW_E_EVENT_TO_TEXT_TALK = "EventToTextTalk";
  public static final String DL_E_SPEECH_TO_SPEECH_TALK = "SpeechToSpeechTalk";
  public static final String DL_E_SPEECH_TO_TEXT_TALK = "SpeechToTextTalk";
  public static final String DL_E_TEXT_TO_SPEECH_TALK = "TextToSpeechTalk";

  public static final HashMap<String, String> operations = new HashMap<>();

  static {
    operations.put("/api/v3/ws/speechToSpeechTalk", DL_E_SPEECH_TO_SPEECH_TALK);
    operations.put("/api/v3/ws/speechToTextTalk", DL_E_SPEECH_TO_TEXT_TALK);
    operations.put("/api/v3/ws/textToSpeechTalk", DL_E_TEXT_TO_SPEECH_TALK);
  }
}
