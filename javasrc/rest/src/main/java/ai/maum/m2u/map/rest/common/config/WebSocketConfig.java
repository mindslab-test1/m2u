package ai.maum.m2u.map.rest.common.config;

import ai.maum.m2u.map.rest.v3.dialog.SpeechToSpeechTalkController;
import ai.maum.m2u.map.rest.v3.dialog.TextToSpeechTalkController;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);

  public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
    // WebSocketController 등록
    registry.addHandler(new SpeechToSpeechTalkController(), "/api/v3/ws/speechToSpeechTalk")
        .setAllowedOrigins("*");
    registry.addHandler(new SpeechToSpeechTalkController(), "/api/v3/ws/speechToTextTalk")
        .setAllowedOrigins("*");
    registry.addHandler(new TextToSpeechTalkController(), "/api/v3/ws/textToSpeechTalk")
        .setAllowedOrigins("*");
    logger.info("#@ START registerWebSocketHandlers [{}]", registry);
  }

  /**
   * websocket binary 데이터의 buffer 크기 조절
   */
  @Bean
  public ServletServerContainerFactoryBean createWebSocketContainer() {
    ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
    container.setMaxTextMessageBufferSize(8 * 1024);
    container.setMaxBinaryMessageBufferSize(1024 * 1024);
    return container;
  }
}
