package ai.maum.m2u.map.rest.v3.dialog;

import ai.maum.m2u.map.rest.common.config.Keys;
import ai.maum.m2u.map.rest.common.config.PropertyManager;
import ai.maum.m2u.map.rest.common.utils.GrpcUtils;
import ai.maum.m2u.map.rest.v3.itfc.MapGrpcInterfaceManager;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import java.util.HashMap;
import java.util.UUID;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v3/auth")
public class AuthController {

  private static final Logger logger = LoggerFactory.getLogger(DialogController.class);

  private MapGrpcInterfaceManager mapGrpcInterfaceManager;

  public AuthController() {
    String[] addrArr = PropertyManager.getString("map.export").split(":");
    this.mapGrpcInterfaceManager = new MapGrpcInterfaceManager(addrArr[0],
        Integer.parseInt(addrArr[1]));
//    this.mapGrpcInterfaceManager = new MapGrpcInterfaceManager("54.180.59.113", 9911);
  }

  @RequestMapping(
      value = "/signIn",
      method = RequestMethod.POST)
  public ResponseEntity<?> signIn(
      @RequestHeader(value = Keys.M2U_AUTH_INTERNAL_HEADER, defaultValue = "") String authInternal,
      @RequestBody HashMap<String, Object> req) {
    logger.info("[m2u-rest] IN call /signIn");
    logger.trace("[m2u-rest] /signIn = {}", req.toString());
    try {
      Timestamp.Builder timestamp = Timestamp.newBuilder()
          .setSeconds(System.currentTimeMillis() / 1000);

      MapEvent.Builder signInParam = MapEvent.newBuilder()
          .setEvent(
              EventStream.newBuilder().setInterface(
                  AsyncInterface.newBuilder().setInterface("Authentication")
                      .setOperation("SignIn")
                      .setType(OperationType.OP_EVENT).setStreaming(false))
                  .setStreamId(getUUID())
                  .setOperationSyncId(getUUID())
                  .setPayload(GrpcUtils.hashMapToStruct(req))
                  .setBeginAt(timestamp));

      MapDirective signInResult = mapGrpcInterfaceManager
          .callEvent(signInParam.build(), "", authInternal);
      String result = JsonFormat.printer().includingDefaultValueFields()
          .print(signInResult);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(e.getMessage(), HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @RequestMapping(
      value = "/signOut",
      method = RequestMethod.POST)
  public ResponseEntity<?> signOut(
      @RequestHeader(value = Keys.M2U_AUTH_INTERNAL_HEADER, defaultValue = "") String authInternal,
      @RequestBody HashMap<String, Object> req) {
    logger.info("[m2u-rest] IN call /signOut");
    logger.trace("[m2u-rest] /signOut = {}", req.toString());
    try {
      Timestamp.Builder timestamp = Timestamp.newBuilder()
          .setSeconds(System.currentTimeMillis() / 1000);
      String authToken = req.get("auth_token").toString();

      MapEvent.Builder signOutParam = MapEvent.newBuilder()
          .setEvent(EventStream.newBuilder().setInterface(
              AsyncInterface.newBuilder().setInterface("Authentication").setOperation("SignOut")
                  .setType(OperationType.OP_EVENT).setStreaming(false))
              .setStreamId(getUUID())
              .setOperationSyncId(getUUID())
              .setPayload(GrpcUtils.hashMapToStruct(req))
              .setBeginAt(timestamp));

      MapDirective signOutResult = mapGrpcInterfaceManager
          .callEvent(signOutParam.build(), authToken, authInternal);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(e.getMessage(), HttpStatus.FAILED_DEPENDENCY);
    }
  }

  private static String getUUID() {
    return UUID.randomUUID().toString();
  }
}
