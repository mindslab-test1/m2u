package ai.maum.m2u.map.rest.v3.dialog;

import ai.maum.m2u.map.rest.common.config.Keys;
import ai.maum.m2u.map.rest.v3.itfc.MapWebSocketInterfaceManager;
import java.net.URI;
import java.text.MessageFormat;
import java.util.HashMap;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

/**
 * MAP과의 Stream을 처리하기 위해 spring websoket으로 구현
 *
 * Client 입력시 device, location, authToken 정보 필수
 *
 * Client 예 : { "payload":{ "lang":"ko_KR", "meta":{ "debug":true } }, "param":{
 * "speech_recognition_param":{ "encoding":"LINEAR16", "sample_rate":16000, "model":"baseline",
 * "single_utterance":true } }, "device":{ "id":"ID001", "type":"WEB", "version":"0.1",
 * "channel":"CHANNEL01", "support":{ "support_render_text":true, "support_render_card":true,
 * "support_speech_synthesizer":true, "support_action":true, "support_expect_speech":true } },
 * "location":{ "latitude":10.3, "longitude":20.5, "location":"mindslab" },
 * "authToken":"1602a950-e651-11e1-84be-00145e76c700" }
 */
@Component
//@SuppressWarnings("unchecked")
public class SpeechToSpeechTalkController extends AbstractWebSocketHandler {

  private final org.slf4j.Logger logger = LoggerFactory
      .getLogger(SpeechToSpeechTalkController.class);

  // sessionId, RestMapDispatcher를 저장하기 위한 HashMap
  // ByteStream 전송시 HashMap에 저장된 restMapDispatcher로 Map에게 전송
  private static HashMap<String, MapWebSocketInterfaceManager> sessionSocketMap = new HashMap<>();

  /**
   * Client와 연결이 완료된 상태
   */
  @Override
  public void afterConnectionEstablished(WebSocketSession session) throws Exception {
    super.afterConnectionEstablished(session);
    logger.info("#@ FROM Client afterConnectionEstablished [{}]", session.getId());

    String sessionId = session.getId();
    if (!sessionSocketMap.containsKey(sessionId)) {
      // Client로 부터 STS 요청이 들어오면 WSTextToSpeechTalkManager 객체를 생성 후 HashMap에 저장
      sessionSocketMap.put(session.getId(), new MapWebSocketInterfaceManager(session));
    }
  }

  /**
   * Client로 부터 TestMessage를 수신합니다(예 : MapEvent)
   */
  @Override
  protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
    String sessionId = session.getId();
    logger.debug("#@ FROM Client handleTextMessage id [{}], sessCnt [{}]"
        , sessionId, sessionSocketMap.size());
    logger.trace("#@ handleTextMessage id [{}], payload [{}]", sessionId, message.getPayload());

    // sessionList에 없는 session인 경우 에러
    if (!sessionSocketMap.containsKey(sessionId)) {
      String msg = MessageFormat
          .format("#@ handleTextMessage There is no sessionId [{}]", sessionId);
      logger.error(msg);
      throw new Exception(msg);
    }

    URI uri = session.getUri();
    if (uri == null) {
      String msg = MessageFormat.format("URI is null. Session:{}", sessionId);
      logger.error(msg);
      throw new NullPointerException(msg);
    }

    String operation = Keys.operations.get(uri.getPath());
    if (operation == null) {
      String msg = MessageFormat.format("Unhandled path: {}", uri.getPath());
      logger.error(msg);
      throw new Exception(msg);
    }

    sessionSocketMap.get(sessionId).handleEvent(sessionId, operation, message);
  }

  /**
   * Client로 부터 TestMessage를 수신합니다(예 : MapEvent)
   */
  @Override
  protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message)
      throws Exception {
    String sessionId = session.getId();

    logger.trace("#@ IN Client to Rest handleBinaryMessage session.id[{}] message[{}]", sessionId,
        message.getPayload());

    // HashMap에서 해당 sessionId 조회
    if (sessionSocketMap.containsKey(sessionId)) {
      MapWebSocketInterfaceManager mapWebSocketInterfaceManager = sessionSocketMap.get(sessionId);
      mapWebSocketInterfaceManager.handleBinary(message);
    } else {
      // 해당 sessionId가 없는 경우
      logger.info("#@ handleBinaryMessage There is no sessionId [{}]", sessionId);
    }
  }


  /**
   * Client로 부터 websocket afterConnectionClosed를 전달 받으면 MAP으로 onCompleted 호출, GRPC closeChannel
   */
  @Override
  public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    String sessionId = session.getId();

    logger.info("#@ FROM Client afterConnectionClosed [{}] {}", sessionId, this);

    // HashMap에서 해당 sessionId 조회
    if (sessionSocketMap.containsKey(sessionId)) {
      // 해당 sessionId를 HashMap에서 삭제
      sessionSocketMap.remove(sessionId).websocketClosed();
    } else {
      // 해당 sessionId가 없는 경우
      logger.info("#@ afterConnectionClosed There is no sessionId [{}]", sessionId);
    }
  }
}
