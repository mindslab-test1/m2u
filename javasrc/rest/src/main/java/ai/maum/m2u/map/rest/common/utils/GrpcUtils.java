package ai.maum.m2u.map.rest.common.utils;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Struct;
import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import maum.m2u.common.DeviceOuterClass.Device.Capability;
import maum.m2u.common.DeviceOuterClass.Device.Capability.Builder;
import maum.m2u.map.Map.EventStream;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GrpcUtils {

  static final Logger logger = LoggerFactory.getLogger(GrpcUtils.class);

  public static Metadata makeOperationSyncIdHeader(EventStream callEvent) {
    logger.info("#@ makeOperationSyncIdHeader x-operation-sync-id");
    // GRPC 헤더에 x-operation-sync-id 추가
    Metadata fixedHeaders = new Metadata();
    Metadata.Key<String> key = Metadata.Key
        .of("x-operation-sync-id", Metadata.ASCII_STRING_MARSHALLER);
    fixedHeaders.put(key, callEvent.getOperationSyncId());

    return fixedHeaders;
  }

  public static void closeChannel(ManagedChannel channel) {
    logger.info("Call close channel shutdown awaitTermination");
    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      logger.error("#@ closeChannel Exception [{}]", e);
    }
  }

  public static ManagedChannel getChannel(String target) {
    logger.info("#@ GRPC target is [{}]", target);
    int pos = target.indexOf(':');
    String addr = target.substring(0, pos);
    String port = target.substring(pos + 1);
    int p = Integer.parseInt(port);

    ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(addr, p)
        .usePlaintext()
        .build();

    return managedChannel;
  }

  public static Struct hashMapToStruct(HashMap hashMap) {
    Struct.Builder builder = Struct.newBuilder();
    try {
      JSONObject jsonObject = new JSONObject(hashMap);
      JsonFormat.parser().merge(jsonObject.toString(), builder);
      logger.trace("struct: {}", JsonFormat.printer().print(builder));
    } catch (InvalidProtocolBufferException e) {
      logger.error("getMetaStruct e : ", e);
    }

    return builder.build();
  }

  // 간단하게 UUID로 구현하였습니다. 중복이 우려될 경우 해당부분 수정하셔서 사용하시면 됩니다.
  public static String getUUID() {
    return UUID.randomUUID().toString();
  }

  /**
   * device의 support 항목을 조회하여 설정한다
   *
   * @param device client에게 수신받은 device 정보
   */
  @SuppressWarnings("unchecked")
  public static Builder getCapabilities(HashMap<String, Object> device) {
    logger.info("#@ is support = {}", device.containsKey("support"));

    // 임시 변수 선언
    boolean tempSupportRenderText = false;
    boolean tempSupportRenderCard = false;
    boolean tempSupportSpeechSynthesizer = false;
    boolean tempSupportPlayAudio = false;
    boolean tempSupportPlayVideo = false;
    boolean tempSupportAction = false;
    boolean tempSupportMove = false;
    boolean tempSupportExpectSpeech = false;

    if (device.containsKey("support") == true) {  // device의 support 항목이 있다면
      Map<String, Boolean> support = (HashMap) device.get("support");
      Iterator<String> keys = support.keySet().iterator();
      while (keys.hasNext()) {
        String key = keys.next();

        if (key.equalsIgnoreCase("support_render_text")
            && support.get(key).booleanValue() == true) {
          tempSupportRenderText = true;
        } else if (key.equalsIgnoreCase("support_render_card")
            && support.get(key).booleanValue() == true) {
          tempSupportRenderCard = true;
        } else if (key.equalsIgnoreCase("support_speech_synthesizer")
            && support.get(key).booleanValue() == true) {
          tempSupportSpeechSynthesizer = true;
        } else if (key.equalsIgnoreCase("support_play_audio")
            && support.get(key).booleanValue() == true) {
          tempSupportPlayAudio = true;
        } else if (key.equalsIgnoreCase("support_play_video")
            && support.get(key).booleanValue() == true) {
          tempSupportPlayVideo = true;
        } else if (key.equalsIgnoreCase("support_action")
            && support.get(key).booleanValue() == true) {
          tempSupportAction = true;
        } else if (key.equalsIgnoreCase("support_move")
            && support.get(key).booleanValue() == true) {
          tempSupportMove = true;
        } else if (key.equalsIgnoreCase("support_expect_speech")
            && support.get(key).booleanValue() == true) {
          tempSupportExpectSpeech = true;
        }
      }
    } else {
      logger.error("There is no support value");
    }

    return Capability.newBuilder()
        .setSupportRenderText(tempSupportRenderText)
        .setSupportRenderCard(tempSupportRenderCard)
        .setSupportSpeechSynthesizer(tempSupportSpeechSynthesizer)
        .setSupportPlayAudio(tempSupportPlayAudio)
        .setSupportPlayVideo(tempSupportPlayVideo)
        .setSupportAction(tempSupportAction)
        .setSupportMove(tempSupportMove)
        .setSupportExpectSpeech(tempSupportExpectSpeech);
  }
}
