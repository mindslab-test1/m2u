package ai.maum.m2u.map.rest.v3.dialog;

import ai.maum.m2u.map.rest.common.config.Keys;
import ai.maum.m2u.map.rest.common.config.PropertyManager;
import ai.maum.m2u.map.rest.common.utils.GrpcUtils;
import ai.maum.m2u.map.rest.v3.itfc.MapGrpcInterfaceManager;
import com.google.protobuf.Struct;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import java.util.HashMap;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.DeviceOuterClass.Device;
import maum.m2u.common.Dialog.OpenUtter;
import maum.m2u.common.Dialog.Utter;
import maum.m2u.common.Dialog.Utter.InputType;
import maum.m2u.common.LocationOuterClass.Location;
import maum.m2u.map.Map.AsyncInterface;
import maum.m2u.map.Map.AsyncInterface.OperationType;
import maum.m2u.map.Map.EventStream;
import maum.m2u.map.Map.EventStream.EventContext;
import maum.m2u.map.Map.MapDirective;
import maum.m2u.map.Map.MapEvent;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v3/dialog")
public class DialogController {

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DialogController.class);

  private MapGrpcInterfaceManager mapGrpcInterfaceManager;

  public DialogController() {
    String[] addrArr = PropertyManager.getString("map.export").split(":");
    this.mapGrpcInterfaceManager = new MapGrpcInterfaceManager(addrArr[0],
        Integer.parseInt(addrArr[1]));
  }

  @SuppressWarnings("unchecked")
  @RequestMapping(value = "/open", method = RequestMethod.POST)
  public ResponseEntity<?> open(
      @RequestHeader(value = Keys.M2U_AUTH_INTERNAL_HEADER, defaultValue = "") String authInternal,
      @RequestBody HashMap<String, Object> req) {
    logger.info("[m2u-rest] IN call /open");
    logger.trace("[m2u-rest] /open = {}", req.toString());

    try {
      HashMap<String, Object> payload = (HashMap) req.get("payload");
      HashMap<String, Object> device = (HashMap) req.get("device");
      HashMap<String, Object> location = (HashMap) req.get("location");

      if (payload.size() > 1 && device.size() > 1 && location.size() > 1) {
        Timestamp.Builder timestamp = Timestamp.newBuilder()
            .setSeconds(System.currentTimeMillis() / 1000);
        Struct.Builder stb = Struct.newBuilder();

        OpenUtter.Builder openPayload = OpenUtter.newBuilder()
            .setUtter(payload.get("utter").toString())
            .setLang(Lang.valueOf(payload.containsKey("lang") ?
                payload.get("lang").toString() :
                "ko_KR"))
            .setChatbot(payload.get("chatbot").toString())
            .setMeta(GrpcUtils.hashMapToStruct((HashMap) payload.get("meta")));

        String json = JsonFormat.printer()
            .includingDefaultValueFields()
            .omittingInsignificantWhitespace()
            .print(openPayload);
        JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

        MapEvent.Builder openParam = MapEvent.newBuilder().setEvent(EventStream.newBuilder()
            .setInterface(AsyncInterface.newBuilder()
                .setInterface(Keys.I_DIALOG_WEB)
                .setOperation(Keys.DLW_E_OPEN)
                .setType(OperationType.OP_EVENT)
                .setStreaming(false))
            .setStreamId(GrpcUtils.getUUID())
            .setOperationSyncId(GrpcUtils.getUUID())
            .addContexts(EventContext.newBuilder()
                .setDevice(Device.newBuilder()
                    .setId(device.get("id").toString())
                    .setType(device.get("type").toString())
                    .setVersion(device.get("version").toString())
                    .setChannel(device.get("channel").toString())
                    .setSupport(GrpcUtils.getCapabilities(device))
                    .setTimestamp(timestamp)
                    .setTimezone("KST+9")))
            .addContexts(EventContext.newBuilder()
                .setLocation(Location.newBuilder()
                    .setLatitude(Float.parseFloat(location.get("latitude").toString()))
                    .setLongitude(Float.parseFloat(location.get("longitude").toString()))
                    .setLocation(location.get("location").toString())))
            .setPayload(stb)
            .setBeginAt(timestamp));

        MapDirective openResult = mapGrpcInterfaceManager
            .callEvent(openParam.build(), req.get("authToken").toString(), authInternal);
        String result = JsonFormat.printer().includingDefaultValueFields().print(openResult);

        return new ResponseEntity<>(result, HttpStatus.OK);
      } else {
        return new ResponseEntity<>("Invalid argument", HttpStatus.BAD_REQUEST);
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(e.getMessage(), HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @SuppressWarnings("unchecked")
  @RequestMapping(value = "/close", method = RequestMethod.POST)
  public ResponseEntity<?> close(
      @RequestHeader(value = Keys.M2U_AUTH_INTERNAL_HEADER, defaultValue = "") String authInternal,
      @RequestBody HashMap<String, Object> req) {
    logger.info("[m2u-rest] IN call /close");

    try {
      HashMap<String, Object> device = (HashMap<String, Object>) req.get("device");
      HashMap<String, Object> location = (HashMap<String, Object>) req.get("location");

      if (device.size() > 1 && location.size() > 1) {
        Timestamp.Builder timestamp = Timestamp.newBuilder()
            .setSeconds(System.currentTimeMillis() / 1000);

        MapEvent.Builder closeParam = MapEvent.newBuilder().setEvent(EventStream.newBuilder()
            .setInterface(AsyncInterface.newBuilder()
                .setInterface(Keys.I_DIALOG_WEB)
                .setOperation(Keys.DLW_E_CLOSE)
                .setType(OperationType.OP_EVENT)
                .setStreaming(false))
            .setStreamId(GrpcUtils.getUUID())
            .setOperationSyncId(GrpcUtils.getUUID())
            .addContexts(EventContext.newBuilder()
                .setDevice(Device.newBuilder()
                    .setId(device.get("id").toString())
                    .setType(device.get("type").toString())
                    .setVersion(device.get("version").toString())
                    .setChannel(device.get("channel").toString())
                    .setSupport(GrpcUtils.getCapabilities(device))
                    .setTimestamp(timestamp)
                    .setTimezone("KST+9")))
            .addContexts(EventContext.newBuilder()
                .setLocation(Location.newBuilder()
                    .setLatitude(Float.parseFloat(location.get("latitude").toString()))
                    .setLongitude(Float.parseFloat(location.get("longitude").toString()))
                    .setLocation(location.get("location").toString()))));

        MapDirective closeResult = mapGrpcInterfaceManager
            .callEvent(closeParam.build(), req.get("authToken").toString(), authInternal);

        return new ResponseEntity<>(HttpStatus.OK);
      } else {
        throw new Exception();
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(e.getMessage(), HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @SuppressWarnings("unchecked")
  @RequestMapping(value = "/textToTextTalk", method = RequestMethod.POST)
  public ResponseEntity<?> textToTextTalk(
      @RequestHeader(value = Keys.M2U_AUTH_INTERNAL_HEADER, defaultValue = "") String authInternal,
      @RequestBody HashMap<String, Object> req) {
    logger.info("[m2u-rest] IN call /textToTextTalk");
    logger.info("[m2u-rest] /textToTextTalk = {}", req.toString());

    try {
      HashMap<String, Object> payload = (HashMap) req.get("payload");
      HashMap<String, Object> device = (HashMap) req.get("device");
      HashMap<String, Object> location = (HashMap) req.get("location");

      if (device.size() > 1 && location.size() > 1) {
        Timestamp.Builder timestamp = Timestamp.newBuilder()
            .setSeconds(System.currentTimeMillis() / 1000);
        Struct.Builder stb = Struct.newBuilder();

        Utter.Builder taklPayload = Utter.newBuilder()
            .setUtter(payload.get("utter").toString())
            .setLang(Lang.valueOf(payload.containsKey("lang") ?
                payload.get("lang").toString() :
                "ko_KR"))
            .setInputType(InputType.valueOf("KEYBOARD"))
            .setMeta(GrpcUtils.hashMapToStruct((HashMap) payload.get("meta")));

        String json = JsonFormat.printer()
            .includingDefaultValueFields()
            .omittingInsignificantWhitespace()
            .print(taklPayload);
        JsonFormat.parser().ignoringUnknownFields().merge(json, stb);

        MapEvent.Builder talkParam = MapEvent.newBuilder().setEvent(EventStream.newBuilder()
            .setInterface(AsyncInterface.newBuilder()
                .setInterface(Keys.I_DIALOG_WEB)
                .setOperation(Keys.DLW_E_TEXT_TO_TEXT_TALK)
                .setType(OperationType.OP_EVENT)
                .setStreaming(false))
            .setStreamId(GrpcUtils.getUUID())
            .setOperationSyncId(GrpcUtils.getUUID())
            .addContexts(EventContext.newBuilder()
                .setDevice(Device.newBuilder()
                    .setId(device.get("id").toString())
                    .setType(device.get("type").toString())
                    .setVersion(device.get("version").toString())
                    .setChannel(device.get("channel").toString())
                    .setSupport(GrpcUtils.getCapabilities(device))
                    .setTimestamp(timestamp)
                    .setTimezone("KST+9")))
            .addContexts(EventContext.newBuilder()
                .setLocation(Location.newBuilder()
                    .setLatitude(Float.parseFloat(location.get("latitude").toString()))
                    .setLongitude(Float.parseFloat(location.get("longitude").toString()))
                    .setLocation(location.get("location").toString())))
            .setPayload(stb)
            .setBeginAt(timestamp));

        MapDirective talkResult = mapGrpcInterfaceManager
            .callEvent(talkParam.build(), req.get("authToken").toString(), authInternal);
        String result = JsonFormat.printer().includingDefaultValueFields().print(talkResult);

        return new ResponseEntity<>(result, HttpStatus.OK);
      } else {
        return new ResponseEntity<>("Invalid argument", HttpStatus.BAD_REQUEST);
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(e.getMessage(), HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @SuppressWarnings("unchecked")
  @RequestMapping(value = "/eventToTextTalk", method = RequestMethod.POST)
  public ResponseEntity<?> eventToTextTalk(
      @RequestHeader(value = Keys.M2U_AUTH_INTERNAL_HEADER, defaultValue = "") String authInternal,
      @RequestBody HashMap<String, Object> req) {
    logger.info("[m2u-rest] IN call /eventToTextTalk");
    logger.info("[m2u-rest] /eventToTextTalk = {}", req.toString());

    try {
      HashMap<String, Object> payload = (HashMap) req.get("payload");
      HashMap<String, Object> device = (HashMap) req.get("device");
      HashMap<String, Object> location = (HashMap) req.get("location");

      if (device.size() > 1 && location.size() > 1) {
        Struct event = GrpcUtils.hashMapToStruct(payload);
        Timestamp.Builder timestamp = Timestamp.newBuilder()
            .setSeconds(System.currentTimeMillis() / 1000);

        MapEvent.Builder talkParam = MapEvent.newBuilder().setEvent(EventStream.newBuilder()
            .setInterface(AsyncInterface.newBuilder()
                .setInterface(event.getFieldsOrThrow("interface").getStringValue())
                .setOperation(event.getFieldsOrThrow("operation").getStringValue())
                .setType(OperationType.OP_EVENT)
                .setStreaming(false))
            .setStreamId(GrpcUtils.getUUID())
            .setOperationSyncId(GrpcUtils.getUUID())
            .addContexts(EventContext.newBuilder()
                .setDevice(Device.newBuilder()
                    .setId(device.get("id").toString())
                    .setType(device.get("type").toString())
                    .setVersion(device.get("version").toString())
                    .setChannel(device.get("channel").toString())
                    .setSupport(GrpcUtils.getCapabilities(device))
                    .setTimestamp(timestamp)
                    .setTimezone("KST+9")))
            .addContexts(EventContext.newBuilder()
                .setLocation(Location.newBuilder()
                    .setLatitude(Float.parseFloat(location.get("latitude").toString()))
                    .setLongitude(Float.parseFloat(location.get("longitude").toString()))
                    .setLocation(location.get("location").toString())))
            .setPayload(event)
            .setBeginAt(timestamp));

        MapDirective talkResult = mapGrpcInterfaceManager
            .callEvent(talkParam.build(), req.get("authToken").toString(), authInternal);
        String result = JsonFormat.printer().includingDefaultValueFields().print(talkResult);

        return new ResponseEntity<>(result, HttpStatus.OK);
      } else {
        return new ResponseEntity<>("Invalid argument", HttpStatus.BAD_REQUEST);
      }
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(e.getMessage(), HttpStatus.FAILED_DEPENDENCY);
    }
  }

}
