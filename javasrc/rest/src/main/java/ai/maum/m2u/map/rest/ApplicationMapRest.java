package ai.maum.m2u.map.rest;

import ai.maum.rpc.ResultStatus;
import maum.rpc.Status;
import maum.rpc.Status.ProcessOfM2u;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = "file:${MAUM_ROOT}/etc/m2u.conf", encoding = "UTF-8")
public class ApplicationMapRest {

  static {
    ResultStatus.setModuleAndProcess(Status.Module.M2U, ProcessOfM2u.M2U_REST.getNumber());
  }

  public static void main(String[] args) {
    SpringApplication.run(ApplicationMapRest.class, args);

  }

  @Primary
  @SpringBootConfiguration
  @ConfigurationProperties(prefix = "front.rest.server")
  @EnableConfigurationProperties(ServerPropertiesExtend.class)
  public class ServerPropertiesExtend extends ServerProperties {

  }
}
