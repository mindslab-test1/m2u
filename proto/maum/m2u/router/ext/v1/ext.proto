/**
 * maum.ai M2U 외부 시스템 연동 규격
 */

syntax = "proto3";

import "google/protobuf/struct.proto";
import "google/protobuf/empty.proto";
import "maum/common/lang.proto";
import "maum/m2u/common/dialog.proto";
import "maum/m2u/common/directive.proto";

package maum.m2u.router.ext.v1;

option java_package = "maum.m2u.router.ext.v1";
option java_multiple_files = true;

service ExternalSystem {
  // 사용자인증은 이미 되어 있는 상태이다.
  // Client의 Chatbot open 요청 처리 후 전달된다.
  // 외부시스템에서는 대화 시작 처리, 모니터링, 시작 답변 변경 개입 등을 할 수 있다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc OpenExternal (OpenExternalRequest) returns (stream OpenExternalResponse);
  //  Client의 사용자 발화를 STT 처리 후 DA의 답변 생성 전에 외부시스템에 전달된다.
  //  외부시스템에서는 모니터링, 발화 변경/직접 답변 개입 등을 할 수 있다.
  //  직접 답변 개입 시에는 DA를 거치지 않고 곧바로 Client에 응답한다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc TalkExternal (TalkExternalRequest) returns (stream TalkExternalResponse);
  //  DA에서 답변을 생성 후에 외부시스템에 전달된다.
  //  외부시스템에서는 모니터링, 답변 변경 개입 등을 할 수 있다.
  //  최종 답변은 TTS를 거처 Client에 응답한다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc AnswerExternal (AnswerExternalRequest) returns (stream AnswerExternalResponse);
  //  Client의 Chatbot close 요청 처리 시 외부시스템에 전달된다.
  //  외부시스템에서는 대화 종료 처리를 한다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc CloseExternal (maum.m2u.common.Session) returns (google.protobuf.Empty);
}


message OpenExternalRequest {
  // 사용자 발화
  maum.m2u.common.Utter utter = 1;
  // 세션 open 시 DA에서 생성한 시스템 답변
  maum.m2u.common.Speech answer = 2;

  // open 한 챗봇의 이름
  string chatbot = 11;

  // 지정 시간(ms 단위) 동안 API 호출이 없으면
  // 외부시스템 내부적으로 CloseExternal 처리한다.
  int32 session_timeout = 21;

  // 현재 세션 정보를 보냅니다.
  maum.m2u.common.Session session = 101;
}

message OpenExternalResponse {
  // 외부시스템에서 개입하여 변경한 답변, 개입하지 않았을 때는 지정하지 않는다.
  maum.m2u.common.Speech intervened_answer = 1;
  // DEVICE가 수행할 지시를 내려보낸다. [reserved]
  repeated maum.m2u.common.Directive directives = 2;

  // talk, answer 시 timeout 요청값, 단위는 ms
  int32 requested_timeout = 11;

  // 외부시스템에서 개입한 결과 업데이트된 세션 컨텍스트가 있을 경우 지정한다.
  maum.m2u.common.Session session_update = 101;
  // 외부시스템에서 개입한 결과에 추가 정보가 있을 경우에 지정한다.
  google.protobuf.Struct meta = 102;
}

message TalkExternalRequest {
  // client 에서 전달된 사용자 발화
  maum.m2u.common.Utter utter = 1;

  // 현재 세션 정보를 보냅니다.
  maum.m2u.common.Session session = 101;
}

message TalkExternalResponse {
  oneof intervention {
    // 외부시스템에서 개입하여 변경된 사용자 발화, 개입하지 않았을 때는 지정하지 않는다.
    maum.m2u.common.Utter altered_utter = 1;
    // 외부시스템에서 개입하여 작성된 답변, 개입하지 않았을 때는 지정하지 않는다.
    maum.m2u.common.Speech intervened_answer = 2;
  }
  // DEVICE가 수행할 지시를 내려보낸다. [reserved]
  repeated maum.m2u.common.Directive directives = 3;

  // 외부시스템에서 개입한 결과 업데이트된 세션 컨텍스트가 있을 경우 지정한다.
  maum.m2u.common.Session session_update = 101;
  // 외부시스템에서 개입한 결과에 추가 정보가 있을 경우에 지정한다.
  google.protobuf.Struct meta = 102;
}

message AnswerExternalRequest {
  // DA에서 생성한 시스템 답변
  maum.m2u.common.Speech system_answer = 1;
  // DA에서 생성한 시스템 지시 [reserved]
  repeated maum.m2u.common.Directive directives = 2;

  // 답변 신뢰도, 0~1의 값, 1에 가까울수록 적절한 답변임
  float assessment = 11;
  // 의도파악 결과 사용자가 개입을 요청했는지 여부
  bool intervention_requested = 12;

  // 현재 세션 정보를 보냅니다.
  maum.m2u.common.Session session = 101;
}

message AnswerExternalResponse {
  // 외부시스템에서 개입하여 작성된 답변, 개입하지 않았을 때는 지정하지 않는다.
  maum.m2u.common.Speech intervened_answer = 1;
  // DEVICE가 수행할 지시를 내려보낸다. [reserved]
  repeated maum.m2u.common.Directive directives = 2;

  // 외부시스템에서 개입한 결과 업데이트된 세션 컨텍스트가 있을 경우 지정한다.
  maum.m2u.common.Session session_update = 101;
  // 외부시스템에서 개입한 결과에 추가 정보가 있을 경우에 지정한다.
  google.protobuf.Struct meta = 102;
}
