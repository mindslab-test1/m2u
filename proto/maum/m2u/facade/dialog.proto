syntax = "proto3";

import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/duration.proto";
import "maum/m2u/facade/userattr.proto";
import "maum/m2u/facade/types.proto";
import "google/protobuf/struct.proto";
import "maum/m2u/common/mediatype.proto";

package maum.m2u.facade;

message ChatbotDetail {
  bool auth = 1;
  string auth_provider = 2;

  string greeting = 11;
  string advertising = 12;
  map<string, string> icons = 13;
  string bg_color = 14;
  string bg_image = 15;
  repeated string tags = 16;
  repeated string categories = 17;
  string created_at = 18;
  string modified_at = 19;
  string vendor = 20;
}

message Chatbot {
  string name = 1;
  string title = 2;
  string description = 3;
  string version = 4;
  string howto = 5;
  maum.m2u.common.MediaType output = 6;
  maum.m2u.common.MediaType input = 7;

  repeated Skill skills = 100;
  ChatbotDetail detail = 200;
}

message ChatbotKey {
  string name = 1;
}

message ChatbotFindParams {
  bool all = 1;
  repeated string names = 2;
}

message ChatbotList {
  repeated Chatbot chatbots = 1;
}

message SkillUserAttributes {
  string skill = 1; // skill specified by admin
  string description = 2; // description by admin
  UserAttributeList user_attrs = 11;
}

message ChatbotUserAttributes {
  string chatbot = 1; // chatbot name specified by admin
  repeated SkillUserAttributes skill_user_attrs = 11;
}

//
// Chatbot Finder
//
// Get information on service groups
// If you want to use authenticated mode,
// you can provide meta data with
// 'x-m2u-authentication-token' and
// 'x-m2u-authentication-provider'
service ChatbotFinder {
  // Get chatbot list
  rpc GetAllChatbots (ChatbotFindParams) returns (ChatbotList) {
  }

  // Get chatbot list
  rpc GetChatbots (google.protobuf.Empty) returns (ChatbotList) {
  }

  // Get a service group.
  rpc GetChatbot (ChatbotKey) returns (Chatbot) {
  }

  // Get attributes on service group
  rpc GetUserAttributes (ChatbotKey) returns (ChatbotUserAttributes) {
  }
}

///
/// DIALOG SERVICE
///

enum AccessFrom {
  ANONYMOUS = 0;
  WEB_BROWSER = 1;
  TEXT_MESSENGER = 2;
  SPEAKER = 20;
  TELEPHONE = 30;
  SMS = 40;

  // 1xx는 모바일
  MOBILE = 100;
  MOBILE_ANDROID = 101;
  MOBILE_IPHONE = 102;

  // 2xx는 SNS(카톡, 페북, ... )연동
  SNS = 200;
  SNS_KAKAOTALK = 201;
  SNS_FACEBOOK = 202;

  EXT_API = 1000;
}

message Caller {
  string chatbot = 1;
  string name = 2;
  string email = 3;
  string phone_no = 4;
  map<string, string> properties = 5;
  AccessFrom accessFrom = 6;
}

message Skill {
  string name = 1;
  string lang = 2;
  string description = 3;
  string version = 4;
  string skill = 5;
  map<string, string> properties = 6;
  string key = 7;
}

/**
 *
 */
message Session {
  int64 id = 1;
  string chatbot = 2;
  string user_key = 3;
  AccessFrom accessFrom = 4;
  google.protobuf.Struct contexts = 5;
  google.protobuf.Timestamp start_time = 6;
  google.protobuf.Duration duration = 7;
  bool human_assisted = 8;
  string intent_finder = 9;
}

message SessionKey {
  int64 session_id = 1;
}

message AudioUtter {
  bytes utter = 1;
}

message TextUtter {
  string utter = 1;
}

message Image {
  bytes body = 1;
}

enum DialogResult {
  OK = 0;
  USER_INAVLIDATED = 101;
  SERVICE_GROUP_NOT_FOUND = 102;
  SESSION_NOT_FOUND = 103;
  SESSION_INVALIDATED = 104;

  EMPTY_IN = 201;

  SKILL_CLASSIFIER_FAILED = 301;
  DIALOG_AGENT_NOT_FOUND = 302;
  DIALOG_AGENT_INVALIDATED = 303;
  LAST_DIALG_AGENT_LOST = 304;
  CHATBOT_NOT_FOUND = 305;
  HUMAN_AGENT_NOT_FOUND = 306;
  SECONDARY_DIALOG_CONFLICT = 311;
  LAST_AGENT_NOT_FOUND = 312;
  DUPLICATED_SKILL_RESOLVED = 313;
  SECONDARY_DIALOG_AGENT_IS_MULTI_TURN = 314;

  TRANSIT_CHATBOT = 401;

  INTERNAL_ERROR = 500;
}

enum LangCode {
  kor = 0;
  eng = 1;
}

message SessionStat {
  int32 result_code = 1;
  string full_message = 2;
}

// 과거 세션 조회 조건
message PastSessionQuery {
  string user = 1;
  int64 id = 2;
  string chatbot = 3;
  AccessFrom access_from = 4;
  string peer_ip = 5;
  string sdate = 6;
  string edate = 7;

  int32 limit = 31;
  string page = 32;
}

// 과거 세션
message PastSession {
  int64 id = 1;
  int32 access_from = 2;
  string peer_ip = 3;
  string sdate = 4;
  string stime = 5;
  string edate = 6;
  string etime = 7;
  string chatbot = 8;
  string user = 9;
  int32 talk_count = 10;
}

// 과거 세션 리스트
message PastSessionList {
  repeated PastSession sessions = 1;
  string page_state = 2;
}

// 과거 대화 조건
message PastTalkQuery {
  string user = 1;
  int64 sid = 2;
  string sdate = 3;
  string edate = 4;
  string skill = 5;
  int32 start_cl = 6;
  int32 end_cl = 7;
  string text = 8;

  int32 limit = 31;
  string page = 32;
}

// 과거 대화
message PastTalk {
  int64 sid = 1;
  int32 seq = 2;
  bool audio = 3;
  string audio_record_file = 4;
  int32 sample_rate = 5;
  bool image = 6;
  int32 lang = 7;
  string skill = 8;
  string in_mangled = 9;
  string text = 10;
  string user = 11;
  string date = 12;
  string time = 13;
  map<string, string> meta = 14;
  map<string, string> intention = 15;
  map<string, string> context = 16;
  repeated string agent = 17;
  int32 type = 18;
  float cl_result = 19;
  int32 warned = 20;
}

// 과거 대화 리스트
message PastTalkList {
  repeated PastTalk talks = 1;
  string page_state = 2;
}

message DialFeedbackReq {
  int64 session_id = 1;
  int32 talk_id = 2;
  int32 satisfaction = 3;
}

message DialFeedbackRes {
  string res_code = 2;
}

// Talk service with endpoint client
service DialogService {
  // Open a new session
  rpc Open (Caller) returns (Session) {
  }

  rpc OpenWithAuth (Caller) returns (Session) {
  }

  rpc AudioTalk (stream AudioUtter) returns (stream AudioUtter) {
  }

  rpc AudioToTextTalk (stream AudioUtter) returns (stream TextUtter) {
  }

  rpc TextTalk (stream TextUtter) returns (stream TextUtter) {
  }

  rpc SimpleTextTalk (TextUtter) returns (TextUtter) {
  }

  rpc TextToAudioTalk (stream TextUtter) returns (stream AudioUtter) {
  }

  rpc ImageToTextTalk (stream Image) returns (stream TextUtter) {
  }

  rpc ImageToAudioTalk (stream Image) returns (stream AudioUtter) {
  }
  // update current session. If not exist, NOT_FOUND returns.
  rpc UpdateSession (SessionKey) returns (Session) {
  }

  // close session
  rpc Close (SessionKey) returns (SessionStat) {
  }

  rpc GetPastSessions (PastSessionQuery) returns (PastSessionList);
  rpc GetPastTalks (PastTalkQuery) returns (PastTalkList);

  rpc Feedback (DialFeedbackReq) returns (DialFeedbackRes);
}
