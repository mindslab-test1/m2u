/**
 * 이 파일은 Clova CIC에서 maum.ai M2U 플랫폼에 접속하여 인공지능 기반 대화 서비스를
 * 사용할 수 있는 연동 규격을 정의하고 있습니다.
 *
 * 여기에서 정의된 인터페이스와 메시지는 모두 map.proto에서 정의된 이벤트와
 * 디렉티브 구조에 담겨서 전송되게 됩니다.
 *
 * 아래는 인터페이스와 오퍼레이션을 정의한 것입니다.
 * ------------------------------------------
 * INTERFACE: ClovaToMaum
 * ------------------------------------------
 * OPERATIONs
 * ------------------------------------------
 * - (E) Open
 *   ---: description: init session
 *   ---: paylod type - OpenRequest
 * - (E) SpeechTalk(stream)
     ---: description: start speech talk stream
 *   ---: event param:  SpeechRecognitionParam
 *   ---: stream: bytes
 * - (E) TextTalk (stream)
     ---: description: start text talk stream
 *   ---: event param: empty_param
 *   ---: stream: text
 * - (E) Close
 *   ---: description: close clova-to-maum session
 *   ---: event param or payload: none
 * - (D) SpeechRecognizing
 *   ---: description: display stt temporary result
 *   ---: payload: maum.common.SpeechRecognitionResult
 * - (D) SpeechCaptured
 *   ---: description: stop audio input
 *   ---: payload: maum.common.SpeechRecognitionResult
 * - (D) SpeechRecognitionFailed
 *   ---: description: failure in stt
 *   ---: payload: SpeechError
 * - (D) TalkProcessing
 *   ---: description: AI server working
 *   ---: none
 * - (D) TalkResponse
 *   ---: description: system response on user utterance
 *   ---: payload: ClovaExtensionResponse
 * - (D) CloseResponse
 *   ---: description: closed session
 *   ---: payload: CloseResponse
 *
 * ------------------------------------------
 * Event & Directive Matrix
 * ------------------------------------------
 * 1. (E) Open
 *  ---> (D) TalkProcessing
 *  ---> (D) TalkResponse
 * 2. (E) SpeechTalk(stream)
 *  ---> (D) SpeechRecognizing
 *  ---> (D) SpeechCaptured or (D) SpeechRecognitionFailed
 *  ---> (D) TalkProcessing
 *  ---> (D) TalkResponse
 * 3. (E) TextTalk(stream)
 *  ---> (D) TalkProcessing
 *  ---> (D) TalkResponse
 * 4. (E) Close()
 *  ---> (D) CloseResponse
 *
 * namespace: maum.m2u.ext
 */
syntax = "proto3";

import "google/protobuf/struct.proto";

package maum.m2u.ext;


/**
 * User from Clova
 */
message User {
  string user_id = 1;
  string access_token = 2;
}

/**
 * User from Device
 */
message Device {
  string device_id = 1;
}

/**
 * Session
 */
message Session {
  // 새로운 세션인가?
  bool new = 1;
  // 세션 속성
  google.protobuf.Struct session_attributes = 2;
  // 세션 ID
  string session_id = 3;
  // 사용자 속성
  User user = 4;
}

/**
 * 컨텍스트 정보 from Clova
 */
message Context {
  message SystemContext {
    // 사용자 속성
    User user = 1;
    // 디바이스 속성
    Device device = 2;
  }
  // 시스템 컨텍스트
  SystemContext System = 1;
}

/**
 * Open Event에 대한 메시지 규격
 *
 * 영어 모드로 전환할 때 전달된 메시지 구조
 */
message OpenRequest {
  // 세션
  Session session = 1;
  // 컨텍스트
  Context context = 2;
}

/**
 * SpeechError, 음성인식 에러 메시지
 */
message SpeechError {
  // 에러 상황에서 처리해줘야 할 메시지, STT 텍스트
  string text = 1;
  // 에러 상황 발생한 상세원인
  string reason = 2;
}

/**
 * Clova의 다양한 카드 형식
 */
message Card {
  // TODO
}

// 전달할 Directive 메시지 형식
message Directive {
  // Directive 헤더
  message DirectiveHeader {
    string namespace = 1;
    string name = 2;
    string message_id = 3;
  }
  // 디렉티브 헤더
  DirectiveHeader header = 1;
  // 디렉티브 페이로드
  google.protobuf.Struct payload = 2;
}

// 개벌 출력 메시지
message Speech {
  // 언어
  string lang = 1; // en, ko, ""
  // 스피치 타입
  enum SpeechType {
    PlainText = 0;
    URL = 1;
  }
  // 스피치 타입
  SpeechType type = 2;
  // 값
  string value = 3;
}

// 출력 메시지
message OutputSpeech {
  // 출력 형식
  enum OutputSpeechType {
    // 단순 출력
    SimpleSpeech = 0;
    // 출력 배열
    SpeechList = 1;
    // 출력 집합
    SpeechSet = 2;
  }
  // 출력의 형식
  OutputSpeechType type = 1;
  // 요약
  Speech brief = 2;
  // LIST 형식? (value의 경우에는 values만 됩니다.)
  repeated Speech values = 3;
  message Verbose {
    OutputSpeechType type = 1;
    repeated Speech values = 2;
  }
  // 더 복잡한 출력
  Verbose verbose = 4;
}

/**
 * 대화 응답 구조
 */
message TalkResponse {
  // content template 현태의 데이터 전달
  google.protobuf.Struct card = 1;
  // CIC를 통해서 전달할 directive 구조
  repeated Directive directives = 2;
  // 출력 SPEECH
  OutputSpeech output_speech = 3;
  // 종료 형태
  bool should_end_session = 4;
}

/**
 * CEK Extension 응답 메시지
 */
message ClovaExtensionResponse {
  // 대화 응답
  TalkResponse response = 1;
  // 세션 속성
  google.protobuf.Struct session_attributes = 2;
  // 버전 정보
  string version = 3;
}

/**
 * Close Response
 */
message CloseResponse {
  // 대화 응답
  bool closed = 1;
  // 세션 속성
  google.protobuf.Struct session_attributes = 2;
  // 버전 정보
  string version = 3;
}

