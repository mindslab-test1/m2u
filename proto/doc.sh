#!/bin/bash

#
# MAUM_ROOT=~/maum sudo -E ./doc.sh
#

maum_root=${MAUM_ROOT}
if [ "${maum_root}" = "" ]; then
  echo MAUM_ROOT is not defined!
  exit 1
fi

test -d ${HOME}/temp || mkdir -p ${HOME}/temp

IMAGE=pseudomuto/protoc-gen-doc
if [[ "$(docker images -q ${IMAGE} 2> /dev/null)" == "" ]]; then
  docker pull pseudomuto/protoc-gen-doc
fi

docker run \
--rm \
-v ${HOME}/temp:/out \
-v ${MAUM_ROOT}/include:/protos \
${IMAGE} \
--doc_opt=html,maumai-m2u-map.html \
--proto_path=/protos \
google/protobuf/struct.proto \
maum/common/lang.proto \
maum/common/audioencoding.proto \
maum/brain/stt/speech.proto \
maum/brain/tts/speech.proto \
maum/brain/idr/idr.proto \
maum/m2u/common/device.proto \
maum/m2u/common/location.proto \
maum/m2u/common/event.proto \
maum/m2u/map/map.proto \
maum/m2u/common/user.proto \
maum/m2u/map/authentication.proto \
maum/m2u/map/payload.proto

docker run \
--rm \
-v ${HOME}/temp:/out \
-v ${MAUM_ROOT}/include:/protos \
${IMAGE} \
--doc_opt=html,maumai-m2u-dav3.html \
--proto_path=/protos \
google/protobuf/struct.proto \
maum/common/lang.proto \
maum/m2u/common/user.proto \
maum/m2u/common/device.proto \
maum/m2u/common/location.proto \
maum/m2u/common/dialog.proto \
maum/m2u/common/card.proto \
maum/m2u/common/event.proto \
maum/m2u/common/directive.proto \
maum/brain/nlp/nlp.proto \
maum/brain/sds/sds.proto \
maum/brain/sds/resolver.proto \
maum/m2u/router/v3/intentfinder.proto \
maum/m2u/common/userattr.proto \
maum/m2u/common/types.proto \
maum/m2u/da/provider.proto \
maum/m2u/da/v3/talk.proto \


docker run \
--rm \
-v ${HOME}/temp:/out \
-v ${MAUM_ROOT}/include:/protos \
${IMAGE} \
--doc_opt=html,maumai-m2u-router.html \
--proto_path=/protos \
google/protobuf/struct.proto \
maum/m2u/common/dialog.proto \
maum/m2u/common/card.proto \
maum/m2u/common/event.proto \
maum/m2u/common/directive.proto \
maum/m2u/router/v3/router.proto \
maum/m2u/router/v3/intentfinder.proto


docker run \
--rm \
-v ${HOME}/temp:/out \
-v ${MAUM_ROOT}/include:/protos \
${IMAGE} \
--doc_opt=html,maumai-m2u-map-auth.html \
--proto_path=/protos \
google/protobuf/struct.proto \
maum/m2u/common/user.proto \
maum/m2u/common/device.proto \
maum/m2u/map/authentication.proto


docker run \
--rm \
-v ${HOME}/temp:/out \
-v ${MAUM_ROOT}/include:/protos \
${IMAGE} \
--doc_opt=html,maumai-m2u-brain-stt.html \
--proto_path=/protos \
maum/common/lang.proto \
maum/common/types.proto \
maum/brain/stt/speech.proto \
maum/brain/stt/stt.proto
